<?php

namespace xc;

class RecordingData
{

    public $genus;

    public $species;

    public $isMystery;

    public $ssp;

    public $commonName;

    public $family;

    public $soundTypeExtra;

    public $quality;

    public $recordist;

    public $recordingDate;

    public $time;

    public $country;

    public $location;

    public $lng;

    public $lat;

    public $elevation;

    public $remarks;

    public $bgSpecies;

    public $bgExtra;

    public $filename;

    public $speciesNr;

    public $speciesOrderNr;

    public $americas;

    public $africa;

    public $asia;

    public $europe;

    public $australia;

    public $uploadDate;

    public $discussionStatus;

    public $license;

    public $id;

    public $device;

    public $microphone;

    public $temperature;

    public $automatic;

    public $specimen;

    public $collectionDate;

    public $soundProperties;

    public $animalSeen;

    public $playbackUsed;

    public $groupId;
}
