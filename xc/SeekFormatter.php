<?php

namespace xc;

class SeekFormatter
{

    public $recording;

    public function __construct($recording)
    {
        $this->recording = $recording;
    }

    public function formatPlayLink($matches)
    {
        if (count($matches) < 3) {
            return $matches[0];
        }

        $text    = $matches[0];
        $minutes = intval($matches[1]);
        $seconds = floatval($matches[2]);

        $time = 60 * $minutes + $seconds;

        if ($time > $this->recording->length()) {
            return $matches[0];
        }

        return "<a onclick='xc.playFrom({$this->recording->xcid()}, $time);'>$text</a>";
    }
}
