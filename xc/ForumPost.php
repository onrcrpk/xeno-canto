<?php

namespace xc;

require_once 'basic_stuff.php';

use function PHP81_BC\strftime;

function getXCNumbers($para)
{
    $stripped = strip_tags($para);
    $xcnrs = [];
    preg_match_all('/\bXC\s?(\d+)/i', $stripped, $found);
    if ($found) {
        $xcnrs = array_merge($xcnrs, $found[1]);
    }
    preg_match_all('/\bxeno-canto.org\/(\d+)/i', $stripped, $found);
    if ($found) {
        $xcnrs = array_merge($xcnrs, $found[1]);
    }

    if (!$xcnrs) {
        return null;
    }

    return array_unique($xcnrs);
}

function getMLNumbers($para)
{
    $stripped = strip_tags($para);
    $mlnrs = [];
    preg_match_all('/\bML\s?(\d+)/i', $stripped, $found);
    $mlnrs = array_merge($mlnrs, $found[1]);
    preg_match_all('/\bLNS\s?(\d+)/i', $stripped, $found);
    $mlnrs = array_merge($mlnrs, $found[1]);
    preg_match_all('/\bmacaulaylibrary.org\/audio\/(\d+)/i', $stripped, $found);
    $mlnrs = array_merge($mlnrs, $found[1]);

    if (!$mlnrs) {
        return [];
    }

    return array_unique($mlnrs);
}

function create_inline_sonos($para)
{
    $xcnrs = getXCNumbers($para);
    $mlnrs = getMLNumbers($para);

    if (!$xcnrs && !$mlnrs) {
        return;
    }

    $sonos = '';
    for ($i = 0; $i < count((array)$xcnrs); $i++) {
        if (isset($xcnrs[$i])) {
            $rec = Recording::load($xcnrs[$i]);
            if ($rec) {
                $sonos .= $rec->player(
                    ['showLocation' => true, 'linkFields' => true]
                );
            }
        }
    }
    for ($i = 0; $i < count((array)$mlnrs); $i++) {
        $sonos .= "<iframe class='ml-audio' width='100%' height='200' src='//macaulaylibrary.org/audio/{$mlnrs[$i]}/play/280' frameborder=0 allowfullscreen></iframe>";
    }
    return $sonos;
}

class ForumPost
{

    public static $threadTypeIcons;

    public static $threadTypeDescriptions;

    public static $threadTypeLevel1;

    public static $threadTypeLevel2;

    public static $threadTypeLevel3;

    private $m_row;

    private $m_table;

    public function __construct($db_row, $db_table)
    {
        $this->m_row = $db_row;
        $this->m_table = $db_table;
    }

    public static function getThreadDescriptions($level = false)
    {
        if (!$level || !is_int($level) || intval($level) < 1 || intval($level) > 3) {
            $level = 1;
        }
        $keys = self::${"threadTypeLevel{$level}"};
        return array_intersect_key(self::$threadTypeDescriptions, array_fill_keys($keys, '1'));
    }

    public static function init()
    {
        self::$threadTypeIcons = [
            ThreadType::NONE => '',
            ThreadType::ID_QUESTIONED => 'ID?',
            ThreadType::ID_RESOLVED => 'ID!',
            ThreadType::MYSTERY => 'Mys?',
            ThreadType::MYSTERY_RESOLVED => 'Mys!',
            ThreadType::ID_UNCONFIRMED => 'ID~',
            ThreadType::RECORDING_DISCUSS => 'dis',

            ThreadType::ADMIN => 'adm',
            ThreadType::TAXONOMY => 'taxo',
            ThreadType::REQUEST => 'req',
            ThreadType::REQUEST_HANDLED => 'req!',
            ThreadType::BUG => 'bug',
            ThreadType::BUG_RESOLVED => 'bug!',
            ThreadType::MAP => 'map',
            ThreadType::MAP_RESOLVED => 'map!',
            ThreadType::HARDWARE => 'hw',
            ThreadType::SOFTWARE => 'sw',
            ThreadType::ID_GENERAL => '#ID',
            //ThreadType::OTHER => "-"
        ];

        self::$threadTypeDescriptions = [
            ThreadType::NONE => 'No category (default)',
            ThreadType::ID_QUESTIONED => 'Recording ID questioned, unresolved',
            ThreadType::ID_RESOLVED => 'Recording ID resolved',
            ThreadType::MYSTERY => 'Mystery recording, ID unresolved',
            ThreadType::MYSTERY_RESOLVED => 'Mystery ID resolved',
            ThreadType::ID_UNCONFIRMED => 'ID Requires Confirmation',
            ThreadType::RECORDING_DISCUSS => 'Recording discussed, but not ID',

            ThreadType::ADMIN => 'Admin',
            ThreadType::TAXONOMY => 'Taxonomy',
            ThreadType::REQUEST => 'Request',
            ThreadType::REQUEST_HANDLED => 'Request handled',
            ThreadType::BUG => 'Bug',
            ThreadType::BUG_RESOLVED => 'Bug resolved',
            ThreadType::MAP => 'Map issue',
            ThreadType::MAP_RESOLVED => 'Map issue resolved',
            ThreadType::HARDWARE => 'Hardware',
            ThreadType::SOFTWARE => 'Software',
            ThreadType::ID_GENERAL => 'General ID question',
            //ThreadType::OTHER => "Other",
        ];

        // Used to tag forum posts for any user
        self::$threadTypeLevel1 = [
            ThreadType::NONE,
            ThreadType::TAXONOMY,
            ThreadType::REQUEST,
            ThreadType::BUG,
            ThreadType::MAP,
            ThreadType::HARDWARE,
            ThreadType::SOFTWARE,
            ThreadType::ID_GENERAL,
        ];

        // Used to display categories in forum index
        // (includes types that can be set by admins only)
        self::$threadTypeLevel2 = [
            ThreadType::NONE,
            ThreadType::ID_QUESTIONED,
            ThreadType::ID_RESOLVED,
            ThreadType::MYSTERY,
            ThreadType::MYSTERY_RESOLVED,
            ThreadType::ADMIN,
            ThreadType::TAXONOMY,
            ThreadType::REQUEST,
            ThreadType::REQUEST_HANDLED,
            ThreadType::BUG,
            ThreadType::BUG_RESOLVED,
            ThreadType::MAP,
            ThreadType::MAP_RESOLVED,
            ThreadType::HARDWARE,
            ThreadType::SOFTWARE,
            ThreadType::ID_GENERAL,
            //ThreadType::OTHER,
        ];

        // Full set of thread types, available to admins only
        self::$threadTypeLevel3 = [
            ThreadType::NONE,
            ThreadType::ID_QUESTIONED,
            ThreadType::ID_RESOLVED,
            ThreadType::MYSTERY,
            ThreadType::MYSTERY_RESOLVED,
            ThreadType::ID_UNCONFIRMED,
            ThreadType::RECORDING_DISCUSS,
            ThreadType::ADMIN,
            ThreadType::TAXONOMY,
            ThreadType::REQUEST,
            ThreadType::REQUEST_HANDLED,
            ThreadType::BUG,
            ThreadType::BUG_RESOLVED,
            ThreadType::MAP,
            ThreadType::MAP_RESOLVED,
            ThreadType::HARDWARE,
            ThreadType::SOFTWARE,
            ThreadType::ID_GENERAL,
            //ThreadType::OTHER,
        ];
    }

    public static function create($type, $subject, $body, $xcid = null, $category = null)
    {
        if (empty(User::current())) {
            return false;
        }

        $name = User::current()->userName();
        $escapedSubject = escape(htmlspecialchars($subject, ENT_NOQUOTES));
        $escapedName = escape($name);
        $dir = User::current()->userId();
        $xcid = intval($xcid);
        $sndStatus = intval($type);

        if ($xcid) {
            $res = query_db(
                "SELECT snd_nr, C.branch, discussed from birdsounds LEFT JOIN world_country_list C USING(country) where snd_nr=$xcid"
            );
            $row = $res->fetch_object();

            // just ensure it's an integer before using it in sql...
            if (!$category) {
                $category = $row->branch;

                if (!$category) {
                    $category = 'world';
                }
            }

            // don't let a new forum topic clear the status for questioned
            // recordings
            if (
                $row->discussed == ThreadType::ID_QUESTIONED
                || $row->discussed == ThreadType::ID_UNCONFIRMED
                || $row->discussed == ThreadType::MYSTERY
            ) {
                $sndStatus = $row->discussed;
            }
        }
        $sql = "insert into forum_world values ('$type', '$escapedSubject', '$escapedName', NOW(), '$category', $xcid, NULL, '$dir', 0, NOW())";

        if (query_db($sql)) {
            $res = query_db('select LAST_INSERT_ID() as topic_nr');
            $row = $res->fetch_object();
            $topic_nr = $row->topic_nr;

            if ($sndStatus && $xcid) {
                query_db("update birdsounds set discussed = $sndStatus where snd_nr = $xcid");
            }

            $t = date('H:i');
            $escapedText = strip($body, true, true);
            $sql = "INSERT INTO discussion_forum_world VALUES ('$escapedName','$escapedText',CURDATE(),'$t',
                '','$dir','$topic_nr', NULL, NOW())";
            query_db($sql);

            $wrapped = wordwrap($body, 60);
            $commonMailBody = <<<EOT
Topic: $subject

$wrapped
EOT;

            if ($xcid) {
                $rec = Recording::load($xcid, false);
                // no need to send a notification to yourself if you're uploading a
                // mystery recording...
                if ($rec && ($rec->recordistID() != User::current()->userId())) {
                    $dir = $rec->recordistID();

                    $sql = "SELECT email, dir, username FROM users WHERE dir='$dir'";
                    $res = query_db($sql);
                    $row = $res->fetch_object();

                    $mailBody = <<<EOT
                        Dear $row->username,
                        
                        xeno-canto user $name has started a discussion on
                        the xeno-canto forum regarding your recording XC$xcid:
                        
                            {$rec->commonName()} ({$rec->fullScientificName()})
                            {$rec->location()}, {$rec->country()}
                            https://www.xeno-canto.org/$xcid
                        
                        $commonMailBody
                        EOT;
                    $text = self::formatNotificationEmail($topic_nr, $mailBody);
                    (new XCMail($row->email, "[xeno-canto] Your recording XC$xcid", $text))->send();
                }
            }

            // for admin notifications, add a custom prefix
            $text = "ADMIN NOTIFICATION:\n\n$name has opened a new topic on the forum\n\n" .
                self::formatNotificationEmail($topic_nr, $commonMailBody);

            (new XCMail(XC_adminMailAddresses(), "[xeno-canto] New forum thread $topic_nr", $text))->send();
        } else {
            // couldn't insert into db
            $failedSubmission = true;
            $errorMessages[] = _('Unable to save forum post');
            XC_logger()->logError("DB error, unable to save forum post for user $name: [$subject]");
        }

        (new XCRedis())->clearForumCache();
        return $topic_nr;
    }

    public static function formatNotificationEmail($topicnr, $body)
    {
        $topicUrl = XC_forumTopicURL($topicnr);
        return <<<EOT
*** PLEASE DO NOT REPLY TO THIS EMAIL. SEE BELOW FOR A LINK
TO THE URL WHERE YOU WILL BE ABLE TO REPLY ON THE XENO-CANTO
FORUM. ***

--------------------------------------------------------------
$body
--------------------------------------------------------------

You can respond to this message by visiting the forum at the
following address:

    $topicUrl

With best wishes,

The xeno-canto.org team
EOT;
    }

    public function generateHTML($last = false)
    {
        $row = $this->m_row;
        $output = '';
        $username = $row->name;

        $userStats = '';
        $nameText = $username;
        $nPosts = intval($row->ncomments);
        $nRecordings = intval($row->nrecordings);

        // There are several cases where the user's name has changed, making it
        // quite difficult to map from the forum post user to the actual user in the
        // users table.  When this happens 'dir' will be null, so we just show a
        // generic avatar, not link to a profile, etc
        if ($row->dir) {
            $nameText = "<a href='" . getUrl('recordist', ['id' => $row->dir]
                ) . "' title='public profile'>$username</a>";
            $userStats = "
                <ul>
                <li><img class='icon' src='/static/img/discuss-light.png' title='Number of forum posts'/>$nPosts</li>
                <li><img class='icon' src='/static/img/audio.png' title='Number of recordings' />$nRecordings</li>
                </ul>
                ";
        }

        if (
            !$row->dir || (!@file_exists(
                $_SERVER['DOCUMENT_ROOT'] . "/graphics/memberpics/$row->dir" . '_tb.png'
            ))
        ) {
            $avatarText = "<img class='avatar' src='/static/img/avatar-default-32.png'/>";
        } else {
            $avatarThumb = "/graphics/memberpics/{$row->dir}_tb.png";
            $avatarFull = "/graphics/memberpics/{$row->dir}.png";
            $avatarText = "<a href='$avatarFull' class='fancybox'><img class='avatar' src='$avatarThumb'/></a>";
        }

        $adminBadge = '';
        if ($row->admin) {
            $adminBadge = "<span class='admin-badge'><img class='icon' src='/static/img/admin-16.png' title='xeno-canto administrator'/>" . _(
                    'XC administrator'
                ) . '</span>';
        }
        $deleteLink = '';
        if (User::current() && User::current()->isAdmin()) {
            $deleteLink = "<img class='icon' src='/static/img/admin-16.png' title='xeno-canto administrator'/> <a href='" . getUrl(
                    'admin-delete-comment',
                    ['t' => $this->m_table, 'id' => $row->mes_nr]
                ) . "'>(delete this comment)</a>";
        }
        $timestamp = $this->getTimestamp();
        $dt = strftime('%B %e, %Y', $timestamp);
        $tm = date('H:i', $timestamp);
        /// translators: a date and time stamp for forum posts
        $timestamp = sprintf(_('posted on %s at %s'), $dt, $tm);
        // use 'forum.name' here rather than 'users.username', because in several
        // instances it appears that the users name has changed, and so the forum
        // database tables are inconsistent.
        $message = self::formatBodyText($row->text);

        // add an anchor for the last post
        if ($last) {
            $output .= "<a name='last'></a>";
        }

        $output .= "
      <article class='forum-post'>
      <footer>
        <div class='userbox'>
          <div class='avatar-container'>
            $avatarText
          </div>
          <h1>$nameText $adminBadge</h1>
          $userStats
        </div>
        <div class='date'>$timestamp $deleteLink</div>
      </footer>
      $message
      </article>";

        return $output;
    }

    public function getTimestamp()
    {
        return strtotime($this->m_row->datetime);
    }

    public static function formatBodyText($text)
    {
        $body = XC_formatUserText($text);
        $sonos = '<div>' . create_inline_sonos($text) . '</div>';

        return $body . $sonos;
    }

}

ForumPost::init();

function XC_forumTopicURL($topicnr)
{
    return getUrl('discuss_forum', ['topic_nr' => $topicnr], true);
}
