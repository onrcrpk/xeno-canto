<?php

namespace xc;

class SortDirection
{

    public const NORMAL = 0;

    public const REVERSE = 1;
}
