<?php
/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace xc;

class Token
{

    public const TYPE_WORD = 0;

    public const TYPE_TAG = 1;

    public const TYPE_STRING = 2;

    public const TYPE_OPERATOR = 3;

    public $type;

    public $value;

    public function __construct($type, $value = null)
    {
        $this->type = $type;
        $this->value = $value;
    }
}

class TaggedQuery
{

    public const DEFAULT_OPERATOR = 0;

    public const MATCHES_OPERATOR = 1;

    public const GREATER_THAN_OPERATOR = 2;

    public const LESS_THAN_OPERATOR = 3;

    public $tagName;

    public $searchTerm;

    public $operator;

    public function __construct($name, $val, $operator = self::DEFAULT_OPERATOR)
    {
        $this->tagName = strtolower($name);
        $this->searchTerm = $val;
        $this->operator = $operator;
    }
}

class ParsedQuery
{

    public $basicTerms;

    public $taggedQueries;

    public function __construct()
    {
        $this->basicTerms = [];
        $this->taggedQueries = [];
    }
}

// parses a query string down into tokens, then constructs those tokens into
// query terms
class QueryParser
{

    public const MATCHES_OPERATOR = '=';

    public const GREATER_THAN_OPERATOR = '>';

    public const LESS_THAN_OPERATOR = '<';

    public static $tagNames = [
        'also',
        'alsospec',
        'area',
        'box',
        'cnt',
        'dir',
        'dis',
        'gen',
        'lat',
        'lic',
        'loc',
        'lon',
        'max',
        'net',
        'nr',
        'or',
        'q',
        'q_lt',
        'q_gt',
        'rec',
        'rmk',
        'since',
        'until',
        'sp',
        'ssp',
        'type',
        'month',
        'year',
        'set',
        'len',
        'len_gt',
        'len_lt',
        'auto',
        'sex',
        'stage',
        'seen',
        'playback',
        'method',
        'colyear',
        'colmonth',
        'temp',
        'regnr',
        'grp',
        'smp',
        'othertype',
        'dvc',
        'mic',
    ];

    public static array $operatorMapping = [
        QueryParser::MATCHES_OPERATOR => TaggedQuery::MATCHES_OPERATOR,
        QueryParser::GREATER_THAN_OPERATOR => TaggedQuery::GREATER_THAN_OPERATOR,
        QueryParser::LESS_THAN_OPERATOR => TaggedQuery::LESS_THAN_OPERATOR,
    ];

    private $query;

    private $pos;

    private $tokens;

    public function __construct($queryString)
    {
        $this->query = $queryString;
        $this->pos = 0;
        $this->tokens = [];
    }

    public static function taggedQueryOperators(): array
    {
        return array_values(self::$operatorMapping);
    }

    public function parse()
    {
        /*
        This has become one of the most compilcated pieces of code in XC.

        Parsing involves checking tokens that were extraced from the query string. These
        are not parsed in tuplets/triplets, but sequentially. This would have been manageable
        if we wouldn't have added operators to the query string instead of using
        dedicated tags, but yeah.

        If the user has added an operator, the method should not skip two steps for the next
        tag:term combo but *three* steps, as the operator in this case is a token by itself.

        To further complicate matters, in theory rmk:> or rmk:">" should be considered valid
        queries, even if the results disappoint (mainly <a href=..> results). In this case, the
        operator becomes the search term and the operator is switched to the default operstor.

        Are you still with me?

        Right, this too is reflected in Query->invalidTaggedQueries().
        */

        $tokens = $this->tokenize();
        $pq = new ParsedQuery();
        for ($i = 0; $i < count($tokens);) {
            $token = $tokens[$i];
            if (($token->type === Token::TYPE_TAG) && ($i + 1 < count($tokens))) {
                $valToken = $tokens[$i + 1];
                if (in_array($valToken->type, [Token::TYPE_WORD, Token::TYPE_STRING, Token::TYPE_OPERATOR])) {
                    // Currently, TYPE_OPERATOR is parsed as a separate entity, check this first.
                    // We need to take the *next* token as the value in this case and skip accordingly.
                    if ($valToken->type == Token::TYPE_OPERATOR && isset($tokens[$i + 2])) {
                        $realValToken = $tokens[$i + 2];
                        // rmk:> query
                        if ($realValToken->value == '') {
                            $realValToken->value = array_search($valToken->value, self::$operatorMapping);
                            $valToken->value = TaggedQuery::DEFAULT_OPERATOR;
                        }
                        $pq->taggedQueries[] = new TaggedQuery($token->value, $realValToken->value, $valToken->value);
                        $i += 3;
                    } else {
                        $pq->taggedQueries[] = new TaggedQuery(
                            $token->value, $valToken->value, TaggedQuery::DEFAULT_OPERATOR
                        );
                        $i += 2;
                    }
                    continue;
                }
            }

            $pq->basicTerms[] = $token->value;
            $i++;
        }
        return $pq;
    }

    private function tokenize()
    {
        $tokens = [];
        $token = null;

        $i = 0;
        for (; ; $i++) {
            $lastType = null;
            if ($token) {
                $lastType = $token->type;
            }

            $token = $this->nextToken($lastType);

            if (!$token) {
                break;
            } elseif ($this->usesOperator($token)) {
                $tokens[] = new Token(Token::TYPE_OPERATOR, $this->getTermAndOperator($token)->operator);
                $tokens[] = new Token(Token::TYPE_STRING, $this->getTermAndOperator($token)->term);
            } else {
                $tokens[] = $token;
            }
        }

        return $tokens;
    }

    public function nextToken($lastTokenType)
    {
        $this->eatWhitespace();
        $c = $this->peek();
        if ($c === null) {
            return null;
        }

        if ($c == '"') {
            $str = $this->parseString();
            return new Token(Token::TYPE_STRING, $str);
        } else {
            if ($lastTokenType != Token::TYPE_TAG) {
                $tag = $this->parseTag();
                if ($tag) {
                    return new Token(Token::TYPE_TAG, $tag);
                }
            }

            // no tag, try to parse it as a word or operator
            $str = $this->parseWord();
            $type = $this->isOperator($str) ? Token::TYPE_OPERATOR : Token::TYPE_WORD;
            return new Token($type, $str);
        }
        return null;
    }

    public function eatWhitespace()
    {
        while (true) {
            $c = $this->peek();
            if ($c !== ' ') {
                return;
            }
            $this->read();
        }
    }

    public function peek($n = 0)
    {
        if (($this->pos + $n) >= strlen($this->query)) {
            return null;
        }

        return $this->query[$this->pos + $n];
    }

    public function read($n = 1)
    {
        if ($this->pos >= strlen($this->query)) {
            return null;
        }

        $v = substr($this->query, $this->pos, $n);
        $this->pos += $n;
        return $v;
    }

    public function parseString()
    {
        if ($this->read() != '"') {
            $this->back();
            return null;
        }

        $v = $this->readUntil('"');
        if ($v) {
            // throw away the closing quote
            $this->read();
        } else {
            $v = $this->readToEnd();
        }
        return $v;
    }

    public function back($n = 1)
    {
        $this->pos -= $n;
    }

    public function readUntil($c)
    {
        $v = null;
        $end = strpos($this->query, $c, $this->pos);
        if ($end !== false) {
            $v = $this->read($end - $this->pos);
        }
        return $v;
    }

    public function readToEnd()
    {
        $n = strlen($this->query) - $this->pos;
        return $this->read($n);
    }

    public function parseTag()
    {
        $candidate = $this->peekUntil(':');
        if ($candidate && in_array($candidate, self::$tagNames)) {
            // advance the internal position
            $this->readUntil(':');
            // move past the ':'
            $this->read();
            return $candidate;
        }

        return null;
    }

    public function peekUntil($c)
    {
        $v = null;
        $end = strpos($this->query, $c, $this->pos);
        if ($end !== false) {
            $v = substr($this->query, $this->pos, $end - $this->pos);
        }
        return $v;
    }

    public function parseWord()
    {
        $str = '';
        $c = $this->read();
        for (; $c !== null && $c !== ' ' && $c !== '"'; $c = $this->read()) {
            $str .= $c;
        }
        // put the last read character back
        if ($c) {
            $this->back();
        }

        return $str;
    }

    private function isOperator($str)
    {
        return in_array($str, self::operators());
    }

    public static function operators(): array
    {
        return array_keys(self::$operatorMapping);
    }

    private function usesOperator($term)
    {
        if ($term instanceof Token && $term->type == Token::TYPE_STRING) {
            return in_array(substr($term->value, 0, 1), self::operators());
        }
        return false;
    }

    private function getTermAndOperator($term)
    {
        if ($term instanceof Token && $term->type == Token::TYPE_STRING) {
            foreach (self::operators() as $operator) {
                if (substr($term->value, 0, strlen($operator)) == $operator) {
                    return (object)[
                        'operator' => self::$operatorMapping[$operator],
                        'term' => str_replace($operator, '', $term->value)
                    ];
                }
            }
        }
        return false;
    }
}
