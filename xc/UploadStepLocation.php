<?php

namespace xc;

use Exception;
use getID3;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadStepLocation extends UploadStep
{

    private $m_map;

    private $previousDevice;

    private $previousMicrophone;

    public function __construct(&$data)
    {
        parent::__construct(UploadStep::LOCATION, $data);

        $dir = User::current()->userId();
        $q = new Query("dir:$dir");
        $this->m_map = new LocationMap('map-canvas', $q->getLocations(), false);

        $this->setPreviousEquipment();
    }

    private function setPreviousEquipment()
    {
        $q = "
            SELECT device, microphone 
            FROM birdsounds 
            WHERE dir = '" . User::current()->userId() . "'
            ORDER BY snd_nr DESC LIMIT 1";
        $res = query_db($q);
        $row = $res->fetch_object();
        if ($row) {
            $this->previousDevice = $row->device;
            $this->previousMicrophone = $row->microphone;
        }
    }

    public function validate($request)
    {
        $valid = true;
        $data = $this->m_data;

        // Check if third party license has been selected (only once!)
        if (User::current()->thirdPartyLicense() == -1) {
            notifyError(
                _(
                    "A metadata sharing license is not selected yet. <u>You will only need to select this license once</u>. Click the link below to open your account preferences in a new window and to set the license. Once you have updated your settings, you can return to this page to proceed with the upload.<br><br><a target='_blank' href='/account/license'>Adjust your account preferences</a>"
                )
            );
            $valid = false;
        }

        if (!$request->request->count()) {
            notifyError('Form submission was unsuccessful.  Perhaps the file you uploaded was too large?');
            // don't try to save posted values, since nothing was posted.  just
            // return immediately
            return false;
        }

        $data->hideMap = !is_null($request->request->get('loc-checkbox'));

        $data->location = $request->request->get('loc-title');
        if (self::validateCountryString($request->request->get('loc-country'))) {
            $data->country = $request->request->get('loc-country');
        } else {
            $data->country = self::getCountryByCode($request->request->get('loc-country-code'));
        }
        $data->elevation = $request->request->get('loc-elevation');
        $data->lat = $request->request->get('loc-latitude');
        // don't need ~10 digits of accuracy here...
        if (is_numeric($data->lat)) {
            $data->lat = round(floatval($data->lat), 4);
        }
        $data->lng = $request->request->get('loc-longitude');
        if (is_numeric($data->lng)) {
            $data->lng = round(floatval($data->lng), 4);
        }
        $data->existingLocation = $request->request->get('loc-existing-location');

        $data->device = $request->request->get('recording-device');
        $data->microphone = $request->request->get('recording-microphone');

        // if we're revising an existing recording, don't bother checking file upload errors
        if (!$data->id) {
            // check for file upload errors
            // FIXME: figure out what to do the second time through if the user is
            // editing the data after having already uploaded a file
            $f = $request->files->get('recording-file');
            if ($f && $f->isValid()) {
                $oldFile = $data->filename;
                if (!empty($oldFile)) {
                    unlink($oldFile);
                }
                $data->filename = null;

                $maxFileNameLength = 175;
                $length = strlen($f->getClientOriginalName());
                if ($length >= $maxFileNameLength) {
                    $msg = sprintf(
                        _('File name length is %d characters. The maximum length is %d characters.'),
                        $length,
                        $maxFileNameLength
                    );
                    notifyError($msg);
                    return false;
                }

                $getID3 = new getID3();
                $info = $getID3->analyze($f);

                if (!isset($info['fileformat']) || !in_array($info['fileformat'], ['mp3', 'wav'])) {
                    $valid = false;
                    $mime = $info['mime_type'] ?? 'unknown';
                }

                if (!$valid) {
                    $msg = sprintf(
                            _(
                                "Uploaded file does not appear to be a valid mp3 or wav audio file. Detected file type is '%s'."
                            ),
                            "<strong>$mime</strong>"
                        ) . ' ' .
                        $this->fileRequirements();
                    $msg .= _(
                        " If you feel that this error is incorrect, please email us at <a href='mailto:" .
                        CONTACT_EMAIL
                        . "'>" .
                        CONTACT_EMAIL
                        . '</a> so that we can investigate.'
                    );
                    notifyError($msg);
                } else {
                    $userdir = app()->soundsStagingDir(User::current()->userId());
                    if (!(is_dir($userdir) || mkdir($userdir, 0755, true))) {
                        notifyError(_('Internal Error') . ": couldn't create staging directory.");
                        $valid = false;
                    }

                    $destFile = null;
                    try {
                        $destFile = $f->move($userdir, $f->getClientOriginalName());
                    } catch (Exception $e) {
                        notifyError(_('Internal Error') . ": couldn't move uploaded file.");
                        $valid = false;
                    }

                    if ($valid) {
                        $data->filename = $destFile->getPathname();
                    }
                }
            } else {
                // if we've come through the process once and then gone back to edit, we
                // might have already uploaded the file, so check whether it exists or
                // not.
                $existingFile = null;
                if ($data->filename) {
                    $existingFile = new File($data->filename);
                }
                if (!($existingFile && $existingFile->isFile())) {
                    $valid = false;
                    $ferr = UPLOAD_ERR_NO_FILE;
                    if ($f) {
                        $ferr = $f->getError();
                        $fmes = $f->getErrorMessage();
                        XC_logger()->logError("Could not upload file, error code: $ferr ($fmes)");
                    }
                    switch ($ferr) {
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            notifyError(
                                htmlspecialchars(
                                    sprintf(_("File '%s' was too large."), $f->getClientOriginalName())
                                ) . ' ' . $this->fileRequirements()
                            );
                            break;
                        case UPLOAD_ERR_PARTIAL:
                            notifyError(
                                htmlspecialchars(
                                    sprintf(
                                        _("File '%s' was only partially uploaded.  Please try again."),
                                        $f->getClientOriginalName()
                                    )
                                )
                            );
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            notifyError(
                                _('No file was uploaded.  Please select a file.')
                            );
                            break;
                        default:
                            notifyError(_("Couldn't upload file: internal error."));
                    }
                }
            }
        }

        // validate the date field
        $datestr = $request->request->get('recording-date');
        if (!$data->id || ($data->recordingDate != $datestr)) {
            if (!self::validateDateString($datestr)) {
                $valid = false;
            }
        }
        $data->recordingDate = $request->request->get('recording-date');

        // validate the selected license
        if (!$data->id || ($data->license != $request->request->get('recording-license'))) {
            if (!$request->request->get('recording-license')) {
                notifyError(_('Recording license was not specified.'));
                $valid = false;
            } elseif (
                License::lookupById(
                    $request->request->get('recording-license')
                ) === null
            ) {
                notifyError(
                    sprintf(
                        _("The license '%s' is not a valid option."),
                        $request->request->get('recording-license')
                    )
                );
                $valid = false;
            }
        }
        $data->license = $request->request->get('recording-license');

        // validate the recordist name
        if (!$data->id || ($data->recordist != $request->request->get('recordist'))) {
            if (!$request->request->get('recordist')) {
                notifyError(_('Recordist name was not specified.'));
                $valid = false;
            }
        }
        $data->recordist = $request->request->get('recordist');

        // validate the location title.  If the user chose a previous location,
        // there will be no 'loc-title' field submitted
        if ($request->request->get('loc-title')) {
            if (!$data->id || ($data->location != $request->request->get('loc-title'))) {
                if (!$request->request->get('loc-title')) {
                    notifyError(_('Location name was not specified.'));
                    $valid = false;
                }
            }
            $data->location = $request->request->get('loc-title');
        }

        if (!$data->id || ($data->country != $request->request->get('loc-country'))) {
            if (!self::validateCountryString($request->request->get('loc-country'))) {
                notifyError(_('Country was not specified.'));
                $valid = false;
            }
        }
        $data->country = $request->request->get('loc-country');

        if (!$data->id || ($data->time != $request->request->get('recording-time'))) {
            if (!self::validateTimeString($request->request->get('recording-time'))) {
                $valid = false;
            }
        }
        $data->time = $request->request->get('recording-time');

        $elevStr = trim($request->request->get('loc-elevation'));
        if (!$data->id || ($data->elevation != $elevStr)) {
            // check for valid elevation
            if ($elevStr === '') {
                notifyError(_("Elevation was not specified. If elevation is unknown, simply use '?'."));
                $valid = false;
            }
        }
        $data->elevation = $elevStr;

        $data->recordingIdType = $request->request->get('recording-id-type');

        $automatic = $request->request->get('automatic-recording');
        if (!$data->id || ($data->automatic != $automatic)) {
            if (!$automatic) {
                notifyError(_("Not specified if recording was supervised ('Automatic')."));
                $valid = false;
            }
        }
        $data->automatic = $request->request->get('automatic-recording');

        return $valid;
    }

    private function fileRequirements()
    {
        return sprintf(
            _('Audio files may be in mp3 or wav format. Size should be %sMB max.'),
            intval(UploadedFile::getMaxFilesize() / (1024 * 1024))
        );
    }

    public function pageId()
    {
        return 'upload-location';
    }

    public function content($request)
    {
        $data = $this->m_data;

        $license = $data->license;
        if (empty($license)) {
            $res = query_db("SELECT license from users where dir='" . User::current()->userId() . "'");
            $row = $res->fetch_object();
            if ($row) {
                $license = $row->license;
            }
        }
        if (empty($license)) {
            $license = 'CC-by-nc-sa-4.0';
        }

        $licenses = License::uploadOptions();
        if ($license && !in_array($license, $licenses)) {
            $licenses[] = $license;
        }
        $licenseSelected = array_fill_keys($licenses, '');
        $licenseSelected[$license] = 'selected';
        $licenseInput = "<select name='recording-license' id='recording-license'>'";

        for ($i = 0; $i < count($licenses); $i++) {
            $lic = License::lookupById($licenses[$i]);
            $licenseInput .= "<option value='{$lic->id}' {$licenseSelected[$lic->id]}>{$lic->name}</option>";
        }
        $licenseInput .= '</select>';

        $recordist = sanitize($data->recordist) ?: User::current()->userName();

        $existingFile = '';
        $fname = sanitize($data->filename);
        if (!empty($fname)) {
            $f = basename($fname);
            $existingFile = '<p>' . sprintf(_('%s already uploaded'), "<em>$f</em>") . '</p>';
        }

        // if they're revising this recording, don't allow them to upload a new
        // file, just allow them to edit file details
        $fileInput = '';
        if (!$data->id) {
            $fileInput = "<input id='recording-file' class='required' type='file' name='recording-file' placeholder='" .
                _('Choose an mp3 or wav file to upload...') . "'/>";
        }

        $loctitle = sanitize($data->location);
        $country = sanitize($data->country);
        $date = sanitize($data->recordingDate);
        $time = sanitize($data->time);
        $elevation = sanitize($data->elevation);

        $device = sanitize($data->device) ?: sanitize($this->previousDevice);
        $microphone = sanitize($data->microphone) ?: sanitize($this->previousMicrophone);

        $automaticSelected = [
            'unknown' => '',
            'no' => '',
            'yes' => '',
        ];
        if (!empty($data->automatic)) {
            $automaticSelected[$data->automatic] = 'checked';
        }

        $recordingIdTypeSelected = [
            'species' => '',
            'envrec' => '',
        ];
        if (!empty($data->recordingIdType)) {
            $recordingIdTypeSelected[$data->recordingIdType] = 'checked';
        } elseif ($data->speciesNr == Species::soundscapeSpeciesNumber()) {
            $recordingIdTypeSelected['envrec'] = 'checked';
        } else {
            $recordingIdTypeSelected['species'] = 'checked';
        }

        $output = "
            <div>
            <p class='important'> " .
            _(
                "Please make sure you understand the <a target='_blank' href='" . getUrl(
                    'termsofuse'
                ) . "'>terms of use</a> before uploading a recording."
            ) . ' ' .
            sprintf(
                _(
                    "Check out <a target='_blank' href='%s'>this upload guide</a> to make sure that your recording is as valuable as possible to the wider community."
                ),
                getUrl('feature-view', ['blognr' => 117])
            ) . "
            </p>

            <form id='map-search'></form>
            
            <form id='details-form' method='post' enctype='multipart/form-data'>
            <table class='form-table'>
            <tbody>
            <tr><th colspan='2'>" . _('Recording file') . "</th></tr>
            <tr>
            <td>
            <span class='required'>" . _('Recording file') . "</span>
            <img data-qtip-header='" . sanitize(_('File requirements')) . "'data-qtip-content='" .
            sanitize($this->fileRequirements()) . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            $existingFile
            $fileInput
            </td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table'>
            <tbody>
            <tr><th colspan='2'>" . _('Location') . "</th></tr>
            </tbody>
            </table>
            </div>

            <div id='map-container' " . ($data->hideMap ? 'style="display: none;"' : '') . ">
                <section class='column' id='map-column'>
                <div>
                <table id='search-form'>
                <tr>
                <td>
                <input form='map-search' autofocus type='text' id='map-search-input' placeholder='" . htmlspecialchars(
                _('Search for a location...')
            ) . "'/>
                </td>
                <td>
                <input type='submit' id='map-search-submit' value='" . htmlspecialchars(_('Search')) . "' form='map-search'/>
                </td>
                </tr>
                </table>
                </div>
                <div id='map-canvas'></div>
                </section>
                <section class='column'>
                <p>" . _(
                'Your previous recording locations are shown on the map.  New locations can be defined by clicking anywhere within the map, by searching for a particular place name, or by entering the coordinates (latitude, longitude in decimal degrees) directly in the field below.'
            ) . "</p>
                <div id='map-coords'>
                <input type='text' id='input-map-coords' placeholder='" . htmlspecialchars(
                _('Latitude, longitude...')
            ) . "'/>            
                <input type='button' id='map-coords-display' value='" . htmlspecialchars(_('Display')) . "'/>
                </div>
                <section class='tip'><h1>" . _('Choose an existing location') . '</h1>
                <p>' . _(
                'If you zoom in far enough, you will be able to see other locations that are already in the Xeno-canto database.'
            ) . "</p>
                </section>
                <section class='map-legend'>
                <h3>" . _('Map legend') . "</h3>
                <ul>
                    <li>
                        <span class='marker-image'><img class='icon' src='/static/img/markers/e-14.png'></span> 
                        <span class='marker-text'>" . _('Personal location') . "</span>
                    </li>
                    <li>
                        <span class='marker-image'><img class='icon' src='/static/img/markers/u-14.png'></span> 
                        <span class='marker-text'>" . _('Location of a nearby Xeno-canto recording') . "</span>
                    </li>
                    <li>
                        <span class='marker-image'><img class='icon' src='/static/img/markers/q-14.png'></span> 
                        <span class='marker-text'>" . _('Search result') . "</span>
                    </li>
                    <li>
                        <span class='marker-image'><img class='icon' src='/static/img/markers/a-14.png'></span> 
                        <span class='marker-text'>" . _('Selected location') . "</span>
                    </li>
                </ul>
                </section>
                </section>
            </div>
            
            <input type='checkbox' id='loc-checkbox' name='loc-checkbox' " . ($data->hideMap ? "checked" : "") . " />
            <label for='loc-checkbox'>" . _("I don't know the exact location where this recording was made") . "</label>
               
            <table class='form-table'>
            <tbody>
            <tr>
            <td>
            <span class='required'>" . _('Country') . '</span>
            </td>
            <td>';

        $selectedCountry = $country;

        if (!$this->validateCountryString($country)) {
            $code = $request->request->get('loc-country-code');
            foreach (getCountryList() as $key => $val) {
                if ($code === $key) {
                    $selectedCountry = $val;
                    break;
                }
            }
        }
        $output .= HtmlUtil::countrySelect(
            'loc-country',
            $selectedCountry,
            'loc-country',
            'required',
            false
        );

        $output .= "
            </td>
            </tr>           
            
            <tr>
            <td>
            <span class='required'>" . _('Location name') . "</span> 
            <img data-qtip-header='" . htmlspecialchars(
                _('Default location name'),
                ENT_QUOTES
            ) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    'The location name may have been filled in automatically by data from Google Maps.  Please change the name to something more descriptive if appropriate.'
                ),
                ENT_QUOTES
            )
            . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            <input id='loc-title' class='required' type='text' name='loc-title' placeholder='"
            . htmlspecialchars(_('Location name'), ENT_QUOTES) . "' value='$loctitle' />
            </td>
            </tr>
            
            <tr>
            <td>
            <span class='required'>" . _('Elevation (m)') . "</span>
            <img data-qtip-header='" . htmlspecialchars(
                _('Default elevation'),
                ENT_QUOTES
            ) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    'The elevation may have been filled in automatically using data from Google Maps.  In areas with high elevational variation, this may be very inaccurate. Please verify and change as necessary.'
                ),
                ENT_QUOTES
            )
            . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            <input id='loc-elevation' class='required' type='text' name='loc-elevation' placeholder='"
            . htmlspecialchars(_('Elevation (m)'), ENT_QUOTES) . "' value='$elevation' />
            </td>
            </tr>  
            </tbody>
            </table>              
            
            <table class='form-table'>
            <tbody>
            <tr><th colspan='2'>" . _('Date and time') . "</th></tr>
            <tr>
            <td>
            <span class='required'>" . _('Recording date') . "</span>
            </td>
            <td>
            <input id='recording-date' class='required' type='text' name='recording-date' placeholder='" . htmlspecialchars(
                _('yyyy-mm-dd'),
                ENT_QUOTES
            ) . "' value='$date' />
            </td>
            </tr>
            <tr>
            <td>
            <span class='required'>" . _('Time of day') . "</span>
            </td>
            <td>
            <input id='recording-time' class='required' type='text' name='recording-time' placeholder='" . htmlspecialchars(
                _('hh:mm (24-hour clock)'),
                ENT_QUOTES
            ) . "' value='$time' />
            </td>
            </tr>
            </tbody>
            </table>  

            <table class='form-table'>
            <tbody>
            <tr><th colspan='2'>" . _('Attribution') . "</th></tr>
            <tr>
            <td>
            <span class='required'>" . _('License') . "</span>
            <img data-qtip-header='" . _('License') . "'
            data-qtip-content='" . sanitize(
                sprintf(
                    _(
                        "You can change your default license on <a href='%s' target='_blank'>your account page</a>. Make sure you understand the <a href='%s' target=_blank >licenses</a> before uploading."
                    ),
                    getUrl('mypage', ['p' => 'prefs']),
                    getUrl('termsofuse')
                )
            )
            . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            $licenseInput
            </td>
            </tr>            
            <tr>
            <td>
            <span class='required'>" . _('Recordist name') . "</span>
            <img data-qtip-header='" . _('Recordist name') . "'
            data-qtip-content='" . sanitize(
                _(
                    "Only change this value if you are uploading another person's recording and have the appropriate rights to do so."
                )
            )
            . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            <input id='recordist' class='required' type='text' name='recordist' placeholder='" . htmlspecialchars(
                _('The name of the recordist'),
                ENT_QUOTES
            ) . "' value='$recordist'/>
            </td>
            </tr>           
            </tbody>
            </table>              
            
            <table class='form-table'>
            <tbody>
            <tr><th colspan='2'>" . _('Devices used') . "</th></tr></th></tr>
            <tr>
            <td> " . _('Recording device') . "
            </td>
            <td>
            <input id='recording-device' type='text' name='recording-device' placeholder='" .
            htmlspecialchars(_('Recording device'), ENT_QUOTES) . "' value='$device' />
            </td>
            </tr>
            
            <tr>
            <td>" . _('Microphone') . "
            </td>
            <td>
            <input id='recording-microphone' type='text' name='recording-microphone' placeholder='" .
            htmlspecialchars(_('Microphone'), ENT_QUOTES) . "' value='$microphone' />
            </td>
            </tr>
            </tbody>
            </table>
            
            
            <table class='form-table'>
            <tbody>
            <tr><th colspan='2'>" . _('Categorization') . "</th></tr></th></tr>
            
            <tr>
            <td><span class='required'>" . _('Type') . "</span></td>
            <td>
            <span class='upload-input'>
                <input type='radio' name='recording-id-type' value='species' id='recording-id-type-species' {$recordingIdTypeSelected['species']} />
                <label for='recording-id-type-species'>" . _('single species') . "</label>
            </span>
            <span class='upload-input'>
                <input type='radio' name='recording-id-type' value='envrec' id='recording-id-type-envrec' {$recordingIdTypeSelected['envrec']} />
                <label for='recording-id-type-envrec'>" . _('soundscape') . " <img data-qtip-header='" .
            _('What is a soundscape?') . "'data-qtip-content='" .
            sanitize(
                _('Soundscapes are longer recordings with multiple species vocalizing.') . " <a href='" .
                getUrl('FAQ') . "#soundscape' target='_blank'>" . _('See more details') . '</a>.'
            )
            . "' class='tooltip' class='icon' src='/static/img/question-icon.png' /></label>
            </span>
            </td>
            </tr>
            
            <tr>
            <td><span class='required'>" . _('Automatic recording') .
            "</span> <img data-qtip-header='" . _('Automatic recording') . "'
            data-qtip-content='" . sanitize(
                _(
                    "Please indicate if the recording was obtained with an unsupervised, automatic recording device."
                )
            )
            . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            <span class='upload-input'>
                <input type='radio' name='automatic-recording' value='yes' id='automatic-yes' {$automaticSelected['yes']}/>
                <label for='automatic-yes'>" . _('yes') . "</label>
            </span>
            <span class='upload-input'>
                <input type='radio' name='automatic-recording' value='no' id='automatic-no' {$automaticSelected['no']} />
                <label for='automatic-no'>" . _('no') . "</label>
            </span>
            <span class='upload-input'>
                <input type='radio' name='automatic-recording' value='unknown' id='automatic-unknown' {$automaticSelected['unknown']} />
                <label for='automatic-unknown'>" . _('unknown') . "</label>
            </span>
            </td>
            </tr>
            </tbody>
            </table>              
            
            <input type='hidden' name='recording-id' value='{$data->id}' />
            <input id='loc-country-code' type='hidden' name='loc-country-code' value='{$request->request->get('loc-country-code', '')}' />
            <input id='loc-latitude' type='hidden' name='loc-latitude' value='{$data->lat}' />
            <input id='loc-longitude' type='hidden' name='loc-longitude' value='{$data->lng}' />
            <input id='loc-existing-location' type='hidden' name='loc-existing-location' value='" .
            sanitize($data->existingLocation) . " '/>

            <div class='button-box'>
                <input id='upload-step-submit' type='submit' value='" . sanitize(_('Next')) . " &raquo;'/>
            </div>
            
            </form>";
        $output .= $this->script();

        return $output;
    }

    public function script()
    {
        $initialPointJs = '';
        if (is_numeric($this->m_data->lat) && is_numeric($this->m_data->lng)) {
            $escapedLocation = htmlspecialchars($this->m_data->location, ENT_QUOTES);
            // This can happen when we've gone through the upload process and then
            // come back to edit data that wasn't correct. Delay the initial
            // location setting slightly because otherwise it will get overridden by
            // the fitBounds() call that happens when the user's personal locations
            // are added.
            $initialPointJs = "
                <script type='text/javascript'>
                    window.setTimeout(function() {
                        var p = new google.maps.LatLng({$this->m_data->lat}, {$this->m_data->lng})
                        newLocationMarker = newMarker({position: p});
                        selectLocation(newLocationMarker, '$escapedLocation', true);
                        var b = new google.maps.LatLngBounds();
                        b.extend(p);
                        xcmap.setCenter(p);
                    }, 500);
                </script>";
        }

        $maxSizeMb = 128;
        $sizeErrorMessage = sprintf(_("Maximum file size is %sMB! Sound file has been unselected."), $maxSizeMb);
        $jsFileSize = "
            <script>
                jQuery('#recording-file').on('change', function() {
                    if (this.files[0].size > ($maxSizeMb * Math.pow(1024, 2))) {
                        xc.showErrorMessage('$sizeErrorMessage');
                        jQuery('#recording-file').val('');
                        
                    }
                });
            </script>";

        $script = self::jsVariables();
        $script .= $this->m_map->getJS(false);
        $script .= '
            <script type="text/javascript" src="/static/js/xc-upload-form-location.js"></script>
            <script type="text/javascript" src="/static/js/jquery.timePicker.min.js"></script>
            <script type="text/javascript" src="/static/js/zebra_datepicker.js"></script>
            <link rel="stylesheet" type="text/css" href="/static/js/zebra_datepicker.css">';
        $script .= $initialPointJs . $jsFileSize;

        return $script;
    }

}