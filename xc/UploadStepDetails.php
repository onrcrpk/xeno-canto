<?php

namespace xc;

class UploadStepDetails extends UploadStepDetailsBase
{

    public function content($request)
    {
        $output = $this->showUploadDetailsForm($request, sanitize(_('Next')) . ' &raquo;');
        $output .= $this->script();

        return $output;
    }
}
