<?php

namespace xc;

class Marker
{

    public const Foreground = 0;

    public const Background = 1;

    public const Location = 2;

    public static $icons =
        [
            '/static/img/markers/a-14.png',
            '/static/img/markers/b-14.png',
            '/static/img/markers/c-14.png',
            '/static/img/markers/d-14.png',
            '/static/img/markers/e-14.png',
            '/static/img/markers/f-14.png',
            '/static/img/markers/g-14.png',
            '/static/img/markers/h-14.png',
            '/static/img/markers/i-14.png',
            '/static/img/markers/j-14.png',
            '/static/img/markers/k-14.png',
            '/static/img/markers/l-14.png',
            '/static/img/markers/m-14.png',
            '/static/img/markers/n-14.png',
            '/static/img/markers/o-14.png',
            '/static/img/markers/p-14.png',
            '/static/img/markers/q-14.png',
            '/static/img/markers/r-14.png',
            '/static/img/markers/s-14.png',
            '/static/img/markers/t-14.png',
            '/static/img/markers/u-14.png',
            '/static/img/markers/v-14.png',
            '/static/img/markers/w-14.png',
        ];

    public static $backgroundIcon = '/static/img/markers/bg-12.png';

    public static $unspecifiedIcon = '/static/img/markers/unspec-14.png';

    public $spec;

    public $specID;

    public $title;

    public $zIndex;

    public $animate;

    public $latitude;

    public $longitude;

    public $id;

    public $contentType;

    public $restricted;

    public function __construct()
    {
        $this->contentType = self::Foreground;
        $this->animate = true;
    }

    public function isRestricted()
    {
        return $this->restricted == 1;
    }

    public function json()
    {
        return json_encode($this->asArray());
    }

    public function asArray()
    {
        $markerOptions = [];
        $markerOptions['m'] = intval($this->specID);
        $markerOptions['lat'] = floatval($this->latitude);
        $markerOptions['lon'] = floatval($this->longitude);
        $markerOptions['animate'] = $this->animate;
        $markerOptions['z'] = intval($this->zIndex);
        $markerOptions['title'] = $this->title;
        $markerOptions['id'] = $this->id;
        $markerOptions['ct'] = $this->contentType;

        return $markerOptions;
    }
}
