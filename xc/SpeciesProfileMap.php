<?php

namespace xc;

class SpeciesProfileMap extends RecordingMap
{

    private $m_unspecMarkerSpec;

    private $m_bgMarkerSpec;

    private $m_sspMarkerSpec;

    private $m_species;

    public function __construct($mapCanvasId, $speciesNumber, $fgResults, $bgResults, $kmlURL = '')
    {
        $this->m_species = Species::load($speciesNumber);
        parent::__construct($mapCanvasId);

        // first add the foreground results to the map, ordered by subspecies
        $this->addResults($fgResults);

        // now add the background results to the map.
        if ($bgResults) {
            $this->addResults($bgResults);
        }

        if ($kmlURL) {
            $this->m_kmlURL = $kmlURL;
        }

        // create default marker for unspecified ssp
        $spec = new MarkerSpec();
        $spec->url = Marker::$unspecifiedIcon;
        $spec->legendText = 'unspecified';
        $this->m_unspecMarkerSpec = $spec;
        $this->m_sspMarkerSpec = [];

        // create default marker for background ssp
        $spec = new MarkerSpec();
        $spec->url = Marker::$backgroundIcon;
        $spec->height = 12;
        $spec->width = 12;
        $spec->anchorX = 6;
        $spec->anchorY = 6;
        $spec->legendText = 'background';
        $this->m_bgMarkerSpec = $spec;
    }

    public function getLegendVfunc()
    {
        $html = "
            <div class=\"map-legend\">";

        if ($this->m_sspMarkerSpec) {
            $sql = "SELECT ssp, author FROM taxonomy_ssp WHERE species_nr='" . $this->m_species->speciesNumber() . "'";
            $res = query_db($sql);
            $validSsp = [];
            while ($row = $res->fetch_object()) {
                $validSsp[$row->ssp] = $row->author;
            }

            $html .= '
                <h4>' . _('Subspecies') . '</h4>
                <ul>';

            foreach ($this->m_sspMarkerSpec as $spec) {
                $ssp = $spec->legendText;
                $queryText = urlencode("\"$ssp\"");
                $legendText = "<a href='{$this->m_species->profileURL()}?query=ssp:{$queryText}'>{$ssp}</a>";
                if (array_key_exists($ssp, $validSsp)) {
                    $legendText .= " &middot; <span class='authority'>{$validSsp[$ssp]}</span>";
                }
                $html .= "
                    <li><span class=\"marker-image\"><img class=\"icon\" src=\"{$spec->url}\" /></span> 
                    <span class=\"marker-text\">$legendText</span></li>";
            }
            $html .= '</ul>';
        }

        if ($this->m_bgMarkerSpec || $this->m_unspecMarkerSpec) {
            $html .= '<h4>' . _('Unclassified') . '</h4>
                <ul>';
            if ($this->m_unspecMarkerSpec) {
                $html .= "
                    <li><span class=\"marker-image\"><img class=\"icon\" src=\"{$this->m_unspecMarkerSpec->url}\" /></span> 
                    <span class=\"marker-text\">" . _('No subspecies specified') . '</span></li>';
                if ($this->m_bgMarkerSpec) {
                    $html .= "
                        <li><span class=\"marker-image\"><img class=\"icon\" src=\"{$this->m_bgMarkerSpec->url}\" /></span> 
                        <span class=\"marker-text\">" . _('In background of another recording') . '</span></li>';
                }
                $html .= '
                    </ul>';
            }
        }

        $html .= '
            </div>';

        return $html;
    }

    protected function createMarkerForRecording($rec)
    {
        if ($rec->speciesNumber() == $this->m_species->speciesNumber()) {
            $marker = new Marker();
            $ssp = $rec->subspecies();
            if (!empty($ssp)) {
                $spec = $this->getMarkerForSsp($ssp);
                // markers with specified subspecies should be visible on top of
                // those without a specified subspecies
                $marker->zIndex = 50;
            } else {
                $spec = $this->m_unspecMarkerSpec;
                $marker->zIndex = 40;
            }

            $spec->height = 14;
            $spec->width = 14;
            $spec->anchorX = 7;
            $spec->anchorY = 7;

            $marker->spec = $spec;
            $marker->animate = true;
            if ($rec->isPseudoSpecies()) {
                $marker->title = "XC{$rec->xcid()}: {$rec->commonName()} ({$rec->recordist()})";
            } else {
                $marker->title = "XC{$rec->xcid()}: {$rec->fullScientificName()} ({$rec->recordist()})";
            }
        } else {
            $marker = new Marker();
            $marker->spec = $this->m_bgMarkerSpec;
            $marker->zIndex = 10;
            $marker->animate = false;
            if ($rec->isPseudoSpecies()) {
                $marker->title = "Background of XC{$rec->xcid()}: {$rec->commonName()}";
            } else {
                $marker->title = "Background of XC{$rec->xcid()}: {$rec->scientificName()}";
            }
            $marker->contentType = Marker::Background;
        }
        $marker->latitude = $rec->latitude();
        $marker->longitude = $rec->longitude();
        $marker->id = $rec->xcid();

        return $marker;
    }

    private function getMarkerForSsp($ssp)
    {
        $ssp = trim(strtolower($ssp));
        if (!array_key_exists($ssp, $this->m_sspMarkerSpec)) {
            $spec = new MarkerSpec();
            // always assign the first marker to the nominate ssp
            if ($ssp === $this->m_species->species()) {
                $spec->url = Marker::$icons[0];
            } else {
                $i = (count($this->m_sspMarkerSpec) + 1) % count(Marker::$icons);
                if ($i === 0) {
                    $i++;
                }
                $spec->url = Marker::$icons[$i];
            }
            $spec->legendText = $ssp;
            $this->m_sspMarkerSpec[$ssp] = $spec;
        } else {
            $spec = $this->m_sspMarkerSpec[$ssp];
        }

        return $spec;
    }
}
