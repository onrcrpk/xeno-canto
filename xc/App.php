<?php

namespace xc;

interface App
{

    public function settings();

    public function db();
}
