<?php

namespace xc;

class HtmlUtil
{

    // $options should be an array of the form (submitvalue => displayname)
    public static function regionSelect($name, $selected = null, $id = '', $class = '')
    {
        $options = ['world' => _('World')];
        foreach (WorldArea::all() as $area) {
            $options[$area->branch] = $area->desc;
        }
        if (!$selected) {
            $selected = 'world';
        }

        return self::selectInput($options, $name, $selected, $id, $class);
    }

    public static function selectInput($options, $name, $selected = null, $id = '', $class = '')
    {
        $html = "<select name='$name' class='$class' id='$id'>";
        foreach ($options as $val => $display) {
            if ($val == html_entity_decode($selected)) {
                $selectedAttr = 'selected';
            } else {
                $selectedAttr = '';
            }
            $html .= "<option value='$val' $selectedAttr>$display</option>";
        }
        $html .= '</select>';
        return $html;
    }

    public static function countrySelect($name, $selected = null, $id = '', $classes = '', $prependEquals = true)
    {
        $list = getCountryList();
        $options = ['' => _('Please choose...')];

        // When used for searching, it's better to prepend an =, so the matches option will be used
        $values = !$prependEquals ? array_values($list) :
            $values = preg_filter('/^/', '=', array_values($list));

        // yes, we use country name as the key...
        $options = array_merge(
            $options,
            array_combine($values, array_values($list))
        );

        return self::selectInput($options, $name, $selected, $id, $classes);
    }

    public static function threadTypeSelect($type, $name, $selected = null, $id = null, $class = null): string
    {
        $level = $type == 'admin' ? 3 : 1;
        $threadTypes = ForumPost::getThreadDescriptions($level);
        // Rename none option for non-admin select
        if ($level == 1) {
            $threadTypes[ThreadType::NONE] = _('No category');
        }
        return self::selectInput($threadTypes, $name, $selected, $id, $class);
    }
}
