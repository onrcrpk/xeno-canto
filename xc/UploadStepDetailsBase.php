<?php

namespace xc;

class UploadStepDetailsBase extends UploadStep
{
    public function __construct(&$data)
    {
        parent::__construct(UploadStep::DETAILS, $data);
        $this->setFormProperties();
    }

    public function validate($request)
    {
        $valid = true;
        $data = $this->m_data;

        // check if third party license has been selected (only once!)
        if (User::current()->thirdPartyLicense() == -1) {
            notifyError(
                _(
                    "A metadata sharing license is not selected yet. <u>You will only need to select this license once</u>. Click the link below to open your account preferences in a new window and to set the license. Once you have updated your settings, you can return to this page to proceed with the upload.<br><br><a target='_blank' href='/account/license'>Adjust your account preferences</a>"
                )
            );
            $valid = false;
        }

        $data->groupId = $request->request->get('group-id');
        $data->soundProperties = $request->request->get('sound-properties');

        $speciesText = $request->request->get('species');

        if ($this->isSoundscape()) {
            $species_nr = Species::soundscapeSpeciesNumber();
        } elseif ($request->request->get('recording-id-type') == 'mystery') {
            $species_nr = Species::mysterySpeciesNumber();
        } else {
            $species_nr = Species::getSpeciesNumberForString($speciesText);
        }

        // check whether a valid species was specified
        if (!$data->id || ($data->speciesNr != $species_nr)) {
            if (empty($species_nr)) {
                if (empty($speciesText)) {
                    notifyError(_('The species was not specified'));
                    $valid = false;
                } else {
                    notifyError(sprintf(_("The specified species name '%s' is invalid."), sanitize($speciesText)));
                    $valid = false;
                }
            } else {
                $res = query_db(
                    "SELECT count(*) from taxonomy WHERE species_nr='$species_nr'"
                );
                if ($res->num_rows != 1) {
                    notifyError(_('No valid species was specified.'));
                    $valid = false;
                }
            }
        }
        $data->speciesNr = $species_nr;
        $data->species = $request->request->get('species');
        $data->ssp = trim($request->request->get('subspecies'));

        $data->animalSeen = $request->request->get('animal-seen');
        if (!$this->isSoundscape() && !$data->animalSeen) {
            notifyError(_("'Animal seen?' was not specified."));
            $valid = false;
        }

        // Background species don't need any validation
        if (!$data->id && $this->isSoundscape()) {
            if (!$request->request->get('background-species')) {
                notifyError(_('Soundscape recordings should have at least one identified background species'));
                $valid = false;
            }
        }
        $data->bgSpecies = $request->request->get('background-species');

        if (!$data->id && ($data->speciesNr == Species::mysterySpeciesNumber())) {
            if (!$request->request->get('remarks')) {
                notifyError(
                    _(
                        "Please leave some comments in the 'Remarks' field so that we can help you solve your mystery."
                    )
                );
                $valid = false;
            }
        }

        // Recording method is required but generated dynamically
        $submittedProperties = $data->soundProperties[$data->groupId] ?? [];
        if (!$submittedProperties) {
            $validRecordingMethod = false;
        } else {
            $recordingMethodProperties = array_keys($this->properties[$data->groupId]['recording method']);
            $validRecordingMethod = !empty(array_intersect($recordingMethodProperties, $submittedProperties));
        }
        if (!$this->isSoundscape() && !$validRecordingMethod) {
            notifyError(_("Recording method was not specified."));
            $valid = false;
        }

        // Collection date is required for 'in studio' recording method
        $data->collectionDate = $request->request->get('collection-date');
        if (in_array(self::inStudioInputId(), $submittedProperties)) {
            if (!$data->collectionDate) {
                notifyError(sprintf(_("No collection date specified for recording method '%s'"), _('in studio')));
                $valid = false;
            } else {
                if (!self::validateDateString($data->collectionDate)) {
                    $valid = false;
                } elseif ($data->collectionDate > $data->recordingDate) {
                    notifyError(_("Collection date must be earlier than recording date."));
                    $valid = false;
                }
            }
        }

        $data->playbackUsed = $request->request->get('playback-used');
        if (!$this->isSoundscape() && !$data->playbackUsed) {
            notifyError(_("'Playback used?' was not specified."));
            $valid = false;
        }

        // New: quality is also required. Do not bother users when they are editing an existing sound though
        $data->quality = $request->request->get('quality');
        if (!$data->id && $data->quality == 0) {
            notifyError(_('Sound quality of your recording was not rated.'));
            $valid = false;
        }

        $data->remarks = $request->request->get('remarks');

        $tempEntered = $request->request->get('temperature');
        if ($tempEntered && $tempEntered != '0') {
            $temp = floatval($tempEntered);
            if ($temp == 0 || $temp < -90 || $temp > 60) {
                notifyError(
                    sprintf(_("'%s' is not a valid temperature. Please enter in degrees Celsius."), $tempEntered)
                );
                $valid = false;
            }
        }
        $data->temperature = $tempEntered;

        $data->specimen = $request->request->get('specimen');

        $data->soundTypeExtra = $request->request->get('sound-type-extra');

        return $valid;
    }

    public function script()
    {
        return "<script>
                var inStudioInputId = " . $this->inStudioInputId() . ";
                var groupSwitchWarning = '" . _(
                'You are selecting a species from a different group. All group-related values will be cleared!'
            ) . "';
            </script>\n" . self::jsVariables() . '
                <script type="text/javascript" src="/static/js/xc-upload-form-details.js"></script>
                <script type="text/javascript" src="/static/js/zebra_datepicker.js"></script>
                <link rel="stylesheet" type="text/css" href="/static/js/zebra_datepicker.css">';
    }

    public function pageId()
    {
        return 'upload-details';
    }

    public function showUploadDetailsForm($request, $buttonText, $includeBackButton = false)
    {
        $data = $this->m_data;

        $backButton = '';
        if ($includeBackButton) {
            $backButton = "<input type='submit' name='back' value='&laquo; " . _('Edit Basic data') . "'/>";
        }

        $idTypeSelected = array_fill_keys(
            ['species', 'mystery'],
            ''
        );
        if ($data->speciesNr == Species::mysterySpeciesNumber()) {
            $idTypeSelected['mystery'] = 'checked';
        } else {
            $idTypeSelected['species'] = 'checked';
        }

        $species = '';
        if ($data->speciesNr != Species::soundscapeSpeciesNumber()) {
            $species = sanitize($data->species);
        }
        $species_nr = $data->speciesNr;

        // check which of the animal-seen and playback-used fields are selected
        $remarks = sanitize($data->remarks);

        $animalSeenSelected = [
            'unknown' => '',
            'yes' => '',
            'no' => '',
        ];
        if (!empty($data->animalSeen)) {
            $animalSeenSelected[$data->animalSeen] = 'checked';
        }

        $playbackUsedSelected = [
            'unknown' => '',
            'yes' => '',
            'no' => '',
        ];
        if (!empty($data->playbackUsed)) {
            $playbackUsedSelected[$data->playbackUsed] = 'checked';
        }

        $qualitySelected = ['', '', '', '', '', ''];
        $q = $data->quality;
        if (!empty($q)) {
            $qualitySelected[$q] = 'checked';
        }

        $selectedGroupId = $data->groupId;
        if (!$selectedGroupId && $species_nr) {
            $selectedGroupId = Species::load($species_nr)->groupId();
        }
        $groupInput = "
            <select id='group-id' name='group-id'>
            <option value=''>" . _('Please choose...') . "</option>";
        foreach ($this->groups as $gid => $name) {
            $selected = $gid == $selectedGroupId ? 'selected' : '';
            $groupInput .= "<option value='$gid' $selected>" . ucfirst(_($name)) . "</option>";
        }
        $groupInput .= '</select>';

        $subspecies = sanitize($data->ssp);
        $soundTypeExtra = sanitize($data->soundTypeExtra);
        $background = sanitize($data->bgSpecies);

        $soundscapeCommonName = Species::load(Species::soundscapeSpeciesNumber())->commonName();
        $mysteryCommonName = Species::load(Species::mysterySpeciesNumber())->commonName();

        // NEW!
        $collectionDate = sanitize($data->collectionDate);
        $temperature = sanitize($data->temperature);
        $specimen = sanitize($data->specimen);

        $batsWarning = _(
            "For bats, XC only admits recordings that are recorded in real-time. Heterodyne, zero-crossing, or time-expanded (slowed down) recordings are not welcome. See the <a href='/help/FAQ#bats' target='_blank'>FAQ</a> for an explanation, and for help to convert your time-expanded recordings to real-time."
        );

        $output = "
            <style>
                .group-data, #collection-date-row, .button-box, #group-id-row {
                    display: none;
                }
                .autocomplete {
                    min-width: 300px;
                }
            </style>

            <form id='details-form' method='post' class='species-completion'>
            <input type='hidden' name='i' value='{$request->query->get('i')}'/>
            <input type='hidden' name='upload-step' value='" . UploadStep::DETAILS . "'/>
            <input type='hidden' name='recording-id' value='$data->id'/>
   
            <p id='bats-warning' class='important' style='display: none;'>$batsWarning</p>
            
            <table class='form-table'>
            <tbody>
            <tr><th colspan='2'>" . _('Species') . "</th></tr>
            <tr>
            <td style='vertical-align: top;'>
            <span class='required'>" . _('Species') . "</span>
            </td>
            <td id='recording-id-type-container'>";

        if (!$this->isSoundscape()) {
            $output .= "
                <div id='species-input-container'>
                <input type='radio' name='recording-id-type' value='species' id='recording-id-type-species' {$idTypeSelected['species']} />
                <input id='species' class='species-input flex-main' data-onselect='primarySpeciesSelected' type='text' name='species' 
                placeholder='" . sanitize(_('Start typing a species name...')) . "' value='$species'/>
                </div>
                
                <div id='mystery-input-container'>
                <input type='radio' name='recording-id-type' value='mystery' id='recording-id-type-mystery' {$idTypeSelected['mystery']} /> 
                <label for='recording-id-type-mystery'>$mysteryCommonName
                <img data-qtip-header='" . _('Mystery recordings') . "' data-qtip-content='" . sanitize(
                    _(
                        'When a mystery recording is uploaded, a new forum topic will be created so that others can comment on your recording.'
                    )
                ) . "' class='tooltip' class='icon' src='/static/img/question-icon.png' />
                </label>
                </div>";
        } else {
            $output .= "
                <div id='soundscape-input-container'>
                <input type='radio' name='recording-id-type' value='envrec' id='recording-id-type-envrec' checked /> 
                <label for='recording-id-type-envrec'>$soundscapeCommonName</label>
                </div>";
        }

        $output .= "
            </td>
            </tr>
            <tr id='group-id-row'>
            <td><span class='required'>" . _('Group') . "</span>
            <img data-qtip-header='" . _('Automatic recording') . "'
            data-qtip-content='" . sanitize(_("Please choose the appropriate animal group.")) . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>$groupInput</td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table group-data'>
            <tbody>
            <tr><th colspan='2'>" . _('Animal details') . "</th></tr>
            
            <tr>
            <td>" . _('Subspecies') . "
            <img data-qtip-header='" . _('Subspecies guidelines') . "'
            data-qtip-content='" . sanitize(
                sprintf(
                    _(
                        "The subspecies field should be left blank if there is any uncertainty. See <a href='%s' target=_blank >this article</a> for more information."
                    ),
                    getUrl('feature-view', ['blognr' => 147])
                )
            ) . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            <input id='subspecies' type='text' name='subspecies' placeholder='" . sanitize(
                _('Start typing a subspecies name...')
            ) . "' value='$subspecies'/>
            </td>
            </tr>";

        foreach ($this->groups as $gid => $groupName) {
            $output .= "
                <tr class='group-row group-data-$gid'>
                    <td>" . _('Sex') . "</td>
                    <td>" . $this->groupCategoryInput($gid, 'sex') . "</td>
                </tr>
                <tr class='group-row group-data-$gid'>
                    <td>" . _('Life stage') . "</td>
                    <td>" . $this->groupCategoryInput($gid, 'life stage') . "</td>
                </tr>";
        }

        $output .= "
            <tr>
            <td><span class='required'>" . _('Animal seen?') . "</span></td>
            <td>
            <span class='upload-input'>
                <input type='radio' name='animal-seen' value='yes' id='animal-seen-yes' {$animalSeenSelected['yes']} />
                <label for='animal-seen-yes'>" . _('yes') . "</label>
            </span>
            <span class='upload-input'>
                <input type='radio' name='animal-seen' value='no' id='animal-seen-no' {$animalSeenSelected['no']} />
                <label for='animal-seen-no'>" . _('no') . "</label>
            </span>
            <span class='upload-input'>
                <input type='radio' name='animal-seen' value='unknown' id='animal-seen-unknown' {$animalSeenSelected['unknown']} />
                <label for='animal-seen-unknown'>" . _('unknown') . "</label>
            </span>
            </td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table group-data'>
            <tbody>
            <tr><th colspan='2'>" . _('Sound type(s)') . "</th></tr>";

        foreach ($this->groups as $gid => $groupName) {
            $output .= "
                <tr class='group-row group-data-$gid'>
                    <td>" . _('Sound type(s)') . " <img data-qtip-header='" . htmlspecialchars(
                    _('Sound type selection'),
                    ENT_QUOTES
                ) . "'
                    data-qtip-content='" . htmlspecialchars(
                    _(
                        'Choose the types of sound that are made by the foreground species in this recording. Try to choose one of the standard sound types if possible and give more detail in your remarks if necessary.'
                    ),
                    ENT_QUOTES
                ) . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" /></td>
                    <td>" . $this->groupCategoryInput($gid, 'sound type') . "</td>
                </tr>";
        }

        $output .= "
            <tr>
            <td>" . _('Other sound type(s)') . "</td>
            <td><input type='text' id='sound-type-extra' name='sound-type-extra' placeholder='" . htmlspecialchars(
                _('Comma-separated list of non-default sound types'),
                ENT_QUOTES
            ) . "' value='$soundTypeExtra'/></td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table group-data'>
            <tbody>
            <tr><th colspan='2'>" . _('Recording conditions') . "</th></tr>";

        foreach ($this->groups as $gid => $groupName) {
            $output .= "
                <tr class='group-row group-data-$gid'>
                    <td><span class='required'>" . _('Recording method') . "</span></td>
                    <td>" . $this->groupCategoryInput($gid, 'recording method') . "</td>
                </tr>";
        }

        $output .= "
            <tr id='collection-date-row'>
            <td>
            <span class='required'>" . _('Collection date') . "</span>
            <img data-qtip-header='" . _('Collection date') . "'
            data-qtip-content='" . sanitize(
                _(
                    "If this is a recording of an animal that has been collected, please mention the collection date here."
                )
            ) . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" /></td>
            <td>
            <input id='collection-date' type='text' name='collection-date' placeholder='" . htmlspecialchars(
                _('yyyy-mm-dd'),
                ENT_QUOTES
            ) . "' value='$collectionDate' />
            </td>
            </tr>

            <tr>
            <td><span class='required'>" . _('Playback used?') . "</span></td>
            <td>
            <span class='upload-input'>
                <input type='radio' name='playback-used' value='yes' id='playback-used-yes' {$playbackUsedSelected['yes']} />
                <label for='playback-used-yes'>" . _('yes') . "</label>
            </span>
            <span class='upload-input'>
                <input type='radio' name='playback-used' value='no' id='playback-used-no' {$playbackUsedSelected['no']} />
                <label for='playback-used-no'>" . _('no') . "</label>
            </span>
            <span class='upload-input'>
                <input type='radio' name='playback-used' value='unknown' id='playback-used-unknown' {$playbackUsedSelected['unknown']} />
                <label for='playback-used-unknown'>" . _('unknown') . "</label>
            </span>
            </td>
            </tr>
           
            <tr class='group-row group-data-2'>
            <td>
            <span>" . _('Temperature') . "  (&deg;C)</span>
            </td>
            <td>
            <input id='temperature' type='text' name='temperature' placeholder='" . htmlspecialchars(
                _('In degrees Celsius, e.g. "25.2"'),
                ENT_QUOTES
            ) . "' value='$temperature' />
            </td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table group-data soundscape-data'>
            <tbody>
            <tr><th colspan='2'>" . _('Background') . "</th></tr>
            
            <tr>
            <td><span " . ($this->isSoundscape() ? 'class="required"' : '') . "> " . _('Background species') . "</span></td>
            <td>
            <input id='background-species' class='species-input multiple' type='text' name='background-species' placeholder='" . htmlspecialchars(
                _(
                    'Start typing a species name... Separate multiple species with a comma (,)'
                ),
                ENT_QUOTES
            ) . "' value='$background' />
            </td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table group-data'>
            <tbody>
            <tr><th colspan='2'>" . _('Collection specimen') . "</th></tr>
            <tr>
            <td>
            <span>" . _('Registration number') . "</span>
            <img data-qtip-header='" . _('Collection specimen reference') . "'
            data-qtip-content='" . sanitize(
                _(
                    "If this is a recording of an animal that has been collected, please mention the collection reference number here."
                )
            ) . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" /></td>
            <td>
            <input id='specimen' type='text' name='specimen' placeholder='" . htmlspecialchars(
                _('Collection registration number'),
                ENT_QUOTES
            ) . "' value='$specimen' />
            </td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table group-data soundscape-data'>
            <tbody>
            <tr><th colspan='2'>" . _('Sound quality') . "</th></tr>
                       
            <tr>
            <td>
            <span class='required'>" . _('Quality') . "</span>
            <img data-qtip-header='" . htmlspecialchars(_('Sound quality selection'), ENT_QUOTES) . "'
            data-qtip-content='" . '
                <strong>A</strong>: ' . htmlspecialchars(_('Loud and Clear'), ENT_QUOTES) . '<br>
                <strong>B</strong>: ' . htmlspecialchars(
                _(
                    'Clear, but animal a bit distant, or some interference with other sound sources'
                ),
                ENT_QUOTES
            ) . '<br>
                <strong>C</strong>: ' . htmlspecialchars(
                _('Moderately clear, or quite some interference'),
                ENT_QUOTES
            ) . '<br>
                <strong>D</strong>: ' . htmlspecialchars(
                _('Faint recording, or much interference'),
                ENT_QUOTES
            ) . '<br>
                <strong>E</strong>: ' . htmlspecialchars(_('Barely audible'), ENT_QUOTES) . "' class=\"tooltip\" class=\"icon\" src=\"/static/img/question-icon.png\" />
            </td>
            <td>
            <span class='upload-input'>
            <input type='radio' name='quality' value='1' id='quality-1' {$qualitySelected[1]} />
            <label for='quality-1' class='tooltip' data-qtip-content='" . htmlspecialchars(
                _('Loud and Clear'),
                ENT_QUOTES
            ) . "'>A</label>
            </span>
            <span class='upload-input'>            
            <input type='radio' name='quality' value='2' id='quality-2' {$qualitySelected[2]} />
            <label for='quality-2' class='tooltip' data-qtip-content='" . htmlspecialchars(
                _('Clear, but animal a bit distant, or some interference with other sound sources'),
                ENT_QUOTES
            ) . "'>B</label>
            </span>
            <span class='upload-input'>  
            <input type='radio' name='quality' value='3' id='quality-3' {$qualitySelected[3]} />
            <label for='quality-3' class='tooltip'  data-qtip-content='" . htmlspecialchars(
                _('Moderately clear, or quite some interference'),
                ENT_QUOTES
            ) . "'>C</label>
            </span>
            <span class='upload-input'>  
            <input type='radio' name='quality' value='4' id='quality-4' {$qualitySelected[4]} />
            <label for='quality-4' class='tooltip'  data-qtip-content='" . htmlspecialchars(
                _('Faint recording, or much interference'),
                ENT_QUOTES
            ) . "'>D</label>
            </span>
            <span class='upload-input'>  
            <input type='radio' name='quality' value='5' id='quality-5' {$qualitySelected[5]} />
            <label for='quality-5' class='tooltip' data-qtip-content='" . htmlspecialchars(
                _('Barely audible'),
                ENT_QUOTES
            ) . "'>E</label>
            </span>
            </td>
            </tr>
            </tbody>
            </table>
            
            <table class='form-table group-data soundscape-data'>
            <tbody>
            <tr><th colspan='2'>" . _('Remarks') . "</th></tr>

            <tr>
            <td style='vertical-align: top;'>" . _('Remarks') . "</td>
            <td>
            <div id='remarks-directions'>
            <p>" . _(
                'Please add as much detail as you can about this recording. The following information is particularly valuable:'
            ) . '
            <ul>
            <li>' . _(
                'Was the recording modified significantly? Note filtering, shortened intervals between vocalizations, etc.'
            ) . '</li>
            <li>' . _(
                "Habitat: note vegetation type, animal's position with respect to the ground or the canopy, etc."
            ) . '</li>
            <li>' . _(
                'Behavior: note interactions, exaggerated posturing, flight displays, etc.'
            ) . '</li>
            <li>' . _(
                'Add extra information regarding distinguishing physical characteristics, age, sex, condition, etc.'
            ) . '</li>
            <li>' . _(
                "Anything else that may have affected the animal's singing behavior (light conditions, weather, etc.)"
            ) . '</li>
            </ul>
            </p>
            <p>' . sprintf(
                _(
                    'You can format your text using the %s text formatting syntax.'
                ),
                "<a target='_blank' href='" . getUrl('markdown') . "'>Markdown</a>"
            ) . ' ' . _(
                'Timestamps (such as <b>0:42</b>) will be converted to clickable links that will start playing from that point in the recording. References to other recordings (such as <b>XC20464</b>) will be converted to clickable links.'
            ) . "
            </p>
            </div>
            <textarea id='remarks' name='remarks'>$remarks</textarea>
            </td>
            </tr>

            </tbody>
            </table>

            <div class='button-box'>
            <input id='upload-step-submit' type='submit' value='$buttonText'/>
            $backButton
            </div>

            <input type='hidden' id='species-nr' name='recording-species-nr' value='$species_nr'/>
  
            </form>";

        return $output;
    }

    private function groupCategoryInput($groupId, $category)
    {
        $type = $this->categories[$groupId][$category] == 1 ? 'checkbox' : 'radio';
        $selectedProperties = $this->m_data->soundProperties ?? [];

        $select = "<div class='" . str_replace(' ', '-', $category) . "'>";
        foreach ($this->properties[$groupId][$category] as $propertyId => $property) {
            $id = str_replace(' ', '-', $category) . "-$propertyId-$groupId";
            $checked = '';
            if (isset($selectedProperties[$groupId]) && in_array($propertyId, $selectedProperties[$groupId])) {
                $checked = 'checked';
            }
            $uncertain = 'data-uncertain=0';
            if ($property == 'uncertain') {
                $uncertain = 'data-uncertain=1';
            }

            $select .= "
                <span class='upload-input'>
                    <input $checked id='$id' type='$type' $uncertain name='sound-properties[$groupId][]' value='$propertyId'>
                    <label for='$id'>" . _($property) . "</label>
                </span>";
        }
        $select .= "</div>";

        return $select;
    }

}
