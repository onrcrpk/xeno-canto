<?php

namespace xc;

class EmbedTemplate extends Template
{

    public function render($content, $params = [])
    {
        $title = '';
        if (isset($params['title'])) {
            $title = $params['title'];
        }

        $bodyClass = '';
        if (isset($params['bodyClass'])) {
            $bodyClass = $params['bodyClass'];
        }

        $gaq = GOOGLE_ANALYTICS_TRACKING_ID;
        if ($gaq) {
            $content .= "
                <script type='text/javascript'>
                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', '$gaq']);
                    _gaq.push(['_trackPageview']);
    
                    (function() {
                        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                    })();
                </script>";
        }

        $gtmq = GOOGLE_TAG_MANAGER_TRACKING_ID;
        if ($gtmq) {
            $content .= "
            <script async src=\"https://www.googletagmanager.com/gtag/js?id=$gtmq\"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '$gtmq');
            </script>";
        }

        return "
            <!doctype html>
            <html>
                <head>
                    <title>$title</title>
                    <link rel='stylesheet' href='/static/css/embed.css' type='text/css'>
                    <link href='//fonts.googleapis.com/css?family=Ubuntu:400,700|Ubuntu+Condensed' rel='stylesheet' type='text/css'>
                    <link rel='stylesheet' href='/static/js/jplayer/xcplayer.css' type='text/css'>
                    <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js'></script>
                    <script type='text/javascript' src='/static/js/jplayer/jquery.jplayer.min.js'></script>
                    <script type='text/javascript' src='/static/js/jplayer/xcplayer.js'></script>
                    <script type='text/javascript'>
                    jQuery(document).ready(function() {
                    jQuery('.xc-audio, .xc-button-audio').each(function()
                    {
                    xcCreatePlayerForControls(this);
                    });
                    });
                    </script>
                    <base target='_parent' />
                </head>
                <body class='$bodyClass' id='embed'>
                    $content
                 </body>
            </html>";
    }
}
