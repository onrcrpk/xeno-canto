<?php

namespace xc;

class DeletedRecording extends Recording
{

    public static function load($xcid, $localized = true)
    {
        $localName = 'english';
        if ($localized) {
            $localName = localNameLanguage();
        }

        $xcid = escape($xcid);
        $res  = query_db(
            "SELECT B.*, cnames.$localName as localName, audio_info.length FROM birdsounds_trash B LEFT JOIN taxonomy_multilingual cnames USING(species_nr) LEFT JOIN audio_info USING(snd_nr) where snd_nr = $xcid"
        );
        if ($res) {
            $row = $res->fetch_object();
            if ($row) {
                return new DeletedRecording($row);
            }
        }
    }

    public function deleteDate()
    {
        return $this->m_row->delete_date;
    }

    public function deleteUser()
    {
        return $this->m_row->delete_user;
    }

    public function deleteReason()
    {
        return $this->m_row->delete_reason;
    }
}
