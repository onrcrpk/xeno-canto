<?php

namespace xc;

class MarkerMap extends Map
{

    public function __construct($mapCanvasId, $locations, $kml = null)
    {
        parent::__construct($mapCanvasId);

        $spec                  = new MarkerSpec();
        $spec->url             = Marker::$icons[0];
        $spec->height          = 14;
        $spec->width           = 14;
        $spec->anchorX         = 7;
        $spec->anchorY         = 7;
        $this->m_markerSpecs[] = $spec;

        for ($i = 0; $i < count($locations); $i++) {
            $loc            = $locations[$i];
            $marker         = new Marker();
            $marker->spec   = $spec;
            $marker->specID = 0;
            // avoid double-quotes and single-quotes which will cause
            // problems with the generated javascript
            $marker->animate   = true;
            $marker->latitude  = $loc['latitude'];
            $marker->longitude = $loc['longitude'];
            $marker->title     = $loc['title'];
            $marker->zIndex    = 50;
            if (isset($loc['id'])) {
                $marker->id = $loc['id'];
            }
            $marker->contentType = Marker::Location;

            $this->m_markers[] = $marker;
        }

        $this->m_kmlURL = $kml;
    }

    protected function createMarker($row)
    {
    }
}
