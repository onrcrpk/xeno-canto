<?php

namespace xc;

use Exception;
use Predis\Client;
use Predis\Connection\ConnectionException;
use Symfony\Component\HttpFoundation\Request as Request;

class XCRedis
{

    public const MAX_REDIS_EXPIRATION = 86400;

    private $client;

    private $key;

    private $enabled = true;

    public function __construct($key = false)
    {
        $options = [
            'scheme' => 'tcp',
            'host' => 'redis',
            'port' => 6379,
            'password' => REDIS_PASSWORD,
        ];
        try {
            $this->client = new Client($options);
            $this->client->connect();
        } catch (ConnectionException $e) {
            XC_logger()->logError('Cannot connect to Redis: ' . $e->getMessage());
        }
        if ($key) {
            $this->setKey($key);
        }
        // Secret option to disable cache for develop purpose
        $this->enabled = getenv('REDIS_DISABLED') != 1;
    }

    public function setKey($key)
    {
        $this->key = $key . '-' . (User::current() ? (User::current()->isAdmin() ? 2 : 1) : 0);
    }

    public function clearRecordingCache()
    {
        $this->deleteAll('recording*');
    }

    private function deleteAll($pattern)
    {
        $keys = $this->client->keys($pattern);
        foreach ($keys as $key) {
            $this->client->del($key);
        }
    }

    public function clearAllCache()
    {
        $this->deleteAll('*');
    }

    public function clearArticleCache()
    {
        $this->deleteAll('article*');
    }

    public function del()
    {
        if ($this->client->exists($this->key())) {
            $this->client->del($this->key());
        }
    }

    public function key()
    {
        if (!$this->key) {
            throw new Exception('Redis key fetched, but not set yet');
        }
        return $this->key;
    }

    public function clearDailyStatsCache()
    {
        $this->deleteAll('daily-stats*');
    }

    public function clearHourlyStatsCache()
    {
        $this->deleteAll('hourly-stats*');
    }

    public function clearForumCache()
    {
        $this->deleteAll('forum*');
    }

    public function clearMysteryCache()
    {
        $this->deleteAll('mystery*');
    }

    public function get()
    {
        return $this->enabled ? $this->client->get($this->key()) : false;
    }

    public function incr()
    {
        return $this->client->incr($this->key());
    }

    public function set($value, $expire = self::MAX_REDIS_EXPIRATION)
    {
        $this->client->set($this->key(), $value);
        $this->client->expire($this->key(), intval($expire));
    }

    public function setKeyByRequest($key, $request = false)
    {
        $this->setKey($key);
        if (!($request instanceof Request)) {
            throw new Exception('Redis key cannot be set: invalid Request object');
        }
        $params = $request->query->all();
        ksort($params);
        $this->key .= '-' . http_build_query($params, '', '-');
        return $this->key;
    }

}
