<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Recording;
use xc\Set;
use xc\User;

use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\sanitize;

class SetEdit extends LoggedInController
{

    public function save($id)
    {
        if ($this->validateForm()) {
            $name = $this->request->request->get('name');
            $description = $this->request->request->get('description');
            $visibility = $this->request->request->get('visibility');
            $userid = User::current()->userId();
            $redirect = $this->request->request->get(
                'redirect',
                getUrl(
                    'mypage',
                    ['p' => 'sets']
                )
            );

            if ($id) {
                $set = Set::load($id);
                if (!$set) {
                    return $this->badRequest();
                }

                if ($set->update($name, $description, $visibility)) {
                    notifySuccess(sprintf(_("Set '%s' updated"), $set->name()));
                    return $this->seeOther($redirect);
                } else {
                    notifyError(_('Unable to save the set'));
                }
            } else {
                $set = Set::create($name, $description, $userid, $visibility);
                if ($set) {
                    notifySuccess(
                        sprintf(_("New set '%s' added"), $set->name())
                    );
                    return $this->seeOther($redirect);
                } else {
                    notifyError(_('Unable to add the set'));
                }
            }
        }

        if ($id) {
            return $this->editForm($id);
        } else {
            return $this->newForm();
        }
    }

    private function validateForm()
    {
        if (!$this->request->request->get('name')) {
            notifyError(_('Please specify a name for the set'));
            return false;
        }
        return true;
    }

    public function editForm($id)
    {
        $title = _('Edit Set Details');
        $output = "
            <p><a href='" . getUrl('mypage', ['p' => 'sets']
            ) . "'>&laquo; " . _(
                'Your Sets'
            ) . "</a></p>
            <h1>$title</h1>
            " . $this->form($id);

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'set-edit']
        );
    }

    private function form($id)
    {
        $set = null;
        if ($id) {
            $set = Set::load($id);
        }
        $defaults = [
            'name' => '',
            'description' => '',
            'visibility' => Set::VISIBILITY_PRIVATE,
        ];
        $checked = '';
        if ($set) {
            $defaults['name'] = $set->name();
            $defaults['description'] = $set->description();
            $defaults['visibility'] = $set->visibility();
        }
        return "
            <form action='" . getUrl('set-save', ['id' => $id]) . "' method='post'>
            <div>
            <input type='text' name='name' placeholder='" . sanitize(
                _(
                    'Name your set...'
                )
            ) . "' value='" .
            sanitize(
                $this->request->request->get('name', $defaults['name'])
            ) . "'/>
            </div>
            <div>
            <p class='note'>" .
            sprintf(
                _(
                    'You can format your text using the %s text formatting syntax.'
                ),
                "<a target='_blank' href='" . getUrl(
                    'markdown'
                ) . "'>Markdown</a>"
            )
            . "</p>
            <textarea placeholder='" . sanitize(
                _(
                    'Enter a short description for this set...'
                )
            ) . "' name='description'>" .
            sanitize(
                $this->request->request->get(
                    'description',
                    $defaults['description']
                )
            ) . "</textarea>
            </div>
            <div>
            <label for='visibility-select'>" . _('Visibility') . ':</label>
            </div>
            <div>
            ' .
            HtmlUtil::selectInput(
                Set::$visibilityOptions,
                'visibility',
                $this->request->request->get(
                    'visibility',
                    $defaults['visibility']
                ),
                'visibility-select'
            ) . "
            </div>
            <div>
            <input type='hidden' name='redirect' value='" . sanitize(
                $this->request->headers->get(
                    'Referer'
                )
            ) . "'/>
            <input type='submit' value='" . sanitize(_('Save')) . "'/>
            </div>
            </form>";
    }

    public function newForm()
    {
        $title = _('Create a New Set');
        $output = "
            <p><a href='" . getUrl('mypage', ['p' => 'sets']
            ) . "'>&laquo; " . _(
                'Your Sets'
            ) . "</a></p>
            <h1>$title</h1>
            " . $this->form(null);

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'set-edit']
        );
    }

    public function delete($id)
    {
        $set = Set::load($id);
        if (!$set) {
            return $this->badRequest();
        }
        $set->delete();
        notifySuccess(sprintf(_("Removed set '%s'"), $set->name()));
        return $this->seeOther(
            $this->request->request->get(
                'redirect',
                getUrl(
                    'mypage',
                    ['p' => 'sets']
                )
            )
        );
    }

    public function deleteForm($id)
    {
        $set = Set::load($id);
        if (!$set) {
            return $this->badRequest();
        }

        $output = "
            <p><a href='" . getUrl('mypage', ['p' => 'sets']
            ) . "'>&laquo; " . _(
                'Your Sets'
            ) . '</a></p>
            <h1>' . _('Remove Set?') . '</h1>
            <p>' . sprintf(
                _(
                    "Are you sure you wish to remove the set '%s'? You cannot undo this action."
                ),
                "<b>{$set->name()}</b>"
            ) . "</p>
            <form action='" . getUrl('set-delete-post', ['id' => $id]) . "' method='post'>
            <input type='submit' value='" . sanitize(_('Remove')) . "' class='delete'/>
            <input type='hidden' name='redirect' value='{$this->request->headers->get('Referer')}'/>
            </form>";

        return $this->template->render($output);
    }

    /** @noinspection SyntaxError */
    public function removeRecordingForm($id)
    {
        $set = Set::load($id);
        if (!$set) {
            return $this->badRequest();
        }

        $title = _('Remove Recordings from Set');

        if ($this->request->query->get('xcid')) {
            $rec = Recording::load($this->request->query->get('xcid'));
            if ($rec) {
                $output = "<h1>$title</h1>
                    <p>" . sprintf(
                        _('Would you like to remove recording %s from the set %s?'),
                        "XC{$rec->xcid()} {$rec->htmlDisplayName(false)}}",
                        "<strong>{$set->name()}</strong>"
                    ) . "</p>
                        <form action='" . getUrl(
                        'set-remove-recording-post',
                        ['id' => $id]
                    ) . "' method='post'>
                        <input type='hidden' name='xcids[]' value='{$rec->xcid()}'/>
                        <input type='hidden' name='redirect' value='" . sanitize(
                        $this->request->headers->get(
                            'Referer'
                        )
                    ) . "'/>
                        <p><input class='delete' type='submit' value='" . sanitize(
                        'Remove'
                    ) . "'/></p>
                        </form>";

                return $this->template->render($output, ['title' => $title]);
            }
        }

        $output = "<h1>$title</h1>
            <p>" . sprintf(
                _(
                    "Select which recordings you would like to remove from the set '%s'."
                ),
                $set->name()
            ) . "</p>
            <form action='" . getUrl(
                'set-remove-recording-post',
                ['id' => $id]
            ) . "' method='post'>";

        foreach ($set->recordings() as $rec) {
            $output .= "
                <div>
                    <input type='checkbox' name='xcids[]' id='checkbox-{$rec->xcid()}' value='{$rec->xcid()}' />
                    <label for='checkbox-{$rec->xcid()}'><a href='" . getUrl('recording', ['xcid' => $rec->xcid()]) . "'>XC{$rec->xcid()}</a>
                    {$rec->htmlDisplayName(false)} &mdash; {$rec->recordist()} &mdash; {$rec->country()}
                    </label>
                </div>";
        }

        $output .= "
            <input type='hidden' name='redirect' value='" . sanitize(
                $this->request->headers->get(
                    'Referer'
                )
            ) . "'/>
            <p><input class='delete' type='submit' value='" . sanitize(
                'Remove'
            ) . "'/></p>
            </form>";

        return $this->template->render($output, ['title' => $title]);
    }

    public function removeRecording($id)
    {
        $set = Set::load($id);
        if (!$set) {
            return $this->badRequest();
        }

        $xcids = $this->request->request->get('xcids');
        $set->removeRecordings($xcids);
        notifySuccess(
            sprintf(
                ngettext(
                    "Removed %s recording from set '%s'",
                    "Removed %s recordings from set '%s'",
                    count($xcids)
                ),
                count($xcids),
                $set->name()
            )
        );
        return $this->seeOther(
            $this->request->request->get(
                'redirect',
                getUrl(
                    'set',
                    ['id' => $set->id()]
                )
            )
        );
    }

}
