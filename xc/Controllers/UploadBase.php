<?php

namespace xc\Controllers;

use xc\AudioUtil;
use xc\ForumPost;
use xc\RecordingData;
use xc\Species;
use xc\ThreadType;
use xc\UploadStep;
use xc\XCRedis;

use function xc\escape;
use function xc\notifyError;
use function xc\strip;
use function xc\XC_acceptableErrors;
use function xc\XC_logger;
use function xc\XC_rateRecording;

class UploadBase extends LoggedInController
{

    // presumably accepts a POSTed array
    public static function finishUpload($formData, $userid)
    {
        $d = self::prepareSubmittedValuesForUpload($formData);
        $redis = new XCRedis();

        if ($d->discussionStatus != ThreadType::MYSTERY) {
            // check to see if this user can upload uncomfirmed recordings for this
            // country. If the user has more than one sigma deviation from the
            // expected accuracy for this country (currently 99%), then all of their
            // recordings will be set to ID_UNCOMFIRMED and require confirmation by
            // another user/admin
            // Mystery recordings and unconfirmed recordings don't count toward the
            // total for this calculation.
            $sql = '
                SELECT 
                    SUM(IF(discussed!=' . ThreadType::MYSTERY . ',1,0)) as N, 
                    SUM(IF(discussed=' . ThreadType::ID_QUESTIONED . ' OR discussed=' . ThreadType::ID_UNCONFIRMED . ", 1, 0)) AS NQ
                FROM birdsounds 
                WHERE dir='$userid' AND country='{$d->country}'";
            $res = query_db($sql);
            $row = $res->fetch_object();
            if ($row) {
                $n = $row->N;
                $acceptableErrors = XC_acceptableErrors($n);
                if ($row->NQ > $acceptableErrors) {
                    $d->discussionStatus = ThreadType::ID_UNCONFIRMED;
                }
            }
        }

        $sql_insert = "
            INSERT INTO birdsounds (
                 `genus`, `species`, `ssp`, `eng_name`, `family`, 
                `length_snd`, `volume_snd`, `speed_snd`, `pitch_snd`, `number_notes_snd`, `variable_snd`,
                `songtype`, `song_classification`, `quality`, `recordist`, 
                `olddate`, `date`, `time`,
                `country`, `location`, `longitude`, `latitude`, `elevation`,
                `remarks`, 
                `background`, `back_nrs`, `back_english`, `back_latin`, `back_families`,  `back_extra`,
                `path`, `species_nr`, `order_nr`, `dir`,
                `neotropics`, `africa`, `asia`, `europe`, `australia`,
                `datetime`, `modified`,
                `discussed`,
                `license`,
                `snd_nr`, 
                `group_id`,
                `temperature`,
                `collection_specimen`, `collection_date`,
                `observed`, 
                `playback`,
                `automatic`,
                `device`, `microphone`, `form_data`               
            ) VALUES (
                '{$d->genus}', '{$d->species}',  '{$d->ssp}', '{$d->commonName}', '{$d->family}',
                -1, -1, -1, -1, -1, -1,
                '{$d->soundTypeExtra}', 0, {$d->quality}, '{$d->recordist}',
                '{$d->recordingDate}', '{$d->recordingDate}', '{$d->time}',
                '{$d->country}', '{$d->location}', {$d->lng}, {$d->lat}, '{$d->elevation}',
                '{$d->remarks}',
                '', '', '', '', '', '{$d->bgExtra}',
                '{$d->filename}', '{$d->speciesNr}', '{$d->speciesOrderNr}', '{$userid}',
                '{$d->americas}', '{$d->africa}', '{$d->asia}', '{$d->europe}', '{$d->australia}',
                '{$d->uploadDate}', '{$d->uploadDate}',
                '{$d->discussionStatus}',
                '{$d->license}',
                null,
                $d->groupId,
                {$d->temperature},  
                '{$d->specimen}', {$d->collectionDate},   
                $d->animalSeen, 
                $d->playbackUsed, 
                $d->automatic, 
               '{$d->device}', '{$d->microphone}', '" . self::setHistoryData($d) . "'
            );";

        $stagingFile = $formData->filename;

        if (!$stagingFile || !is_file($stagingFile)) {
            notifyError(_('Unable to publish file (1)'));
            XC_logger()->logError(
                "Missing filename'\n\tform data: " . var_export($formData) . "\n\tprepared data: " . var_export($d)
            );
            return false;
        } elseif (query_db($sql_insert)) {
            $res = query_db('select LAST_INSERT_ID() as soundnr');
            if (!$res) {
                return false;
            }
            $row = $res->fetch_object();
            $xcid = $row->soundnr;

            // Abort if background species or sound properties can't be set
            if (!self::updateBackgroundSpecies($xcid, $d->bgSpecies) ||
                !self::updateSoundProperties($xcid, $d->soundProperties)) {
                query_db("DELETE FROM birdsounds WHERE snd_nr = $xcid");
                return false;
            }

            $uploadDir = app()->soundsUploadDir($userid);
            if (!(is_dir($uploadDir) || mkdir($uploadDir, 0755, true))) {
                notifyError(_('Unable to publish file (2)'));
                XC_logger()->logError("Unable to create upload directory '$uploadDir'");
                return false;
            } else {
                // prepending the XC number allows us to identify the file
                // more easily after downloading, and also guarantees that
                // the filename is unique and doesn't overwrite an existing
                // file with the same filename from the same user.  Don't
                // use $d->filename here because that variable is
                // sql-escaped
                // Ruud 09-08-22: just get rid of those weird file names and only accept
                // A-Z and 0-9 as valid characters.
                $file = basename($stagingFile);
                $fn = str_replace(
                    ['\\', '/', ':', '*', '?', '"', "'", '<', '>', '|', ' ', '#'],
                    '-',
                    pathinfo($file, PATHINFO_FILENAME)
                );
                $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $xcfname = "XC$xcid-$fn.$ext";
                $publishedFile = "$uploadDir/$xcfname";

                if (!copy($stagingFile, $publishedFile)) {
                    notifyError(_('Unable to publish file (3)'));
                    XC_logger()->logError(
                        "Unable to rename '$stagingFile' to '$publishedFile'\n\tform data: " . var_export(
                            $formData
                        ) . "\n\tprepared data: " . var_export($d)
                    );
                    if ($xcid) {
                        query_db("DELETE FROM birdsounds WHERE snd_nr = $xcid");
                    }

                    return false;
                }

                $xcfname = escape($xcfname);
                // fix the file path in the DB now that we've named it
                query_db("UPDATE birdsounds SET path = '$xcfname' WHERE snd_nr = $xcid");

                if ($d->quality != 0) {
                    XC_rateRecording($xcid, $d->quality, $userid);
                }

                if (!AudioUtil::updateAudioProperties($xcid)) {
                    XC_logger()->logError('Could not update audio properties for ' . $xcfname);
                }

                // Tags are only written for mp3 files
                AudioUtil::writeId3($xcid);

                // Sono generation has its own logging case of errors
                AudioUtil::generateSonos($xcid);

                // Creation of full length sono is combined with generation of mp3 derivatives
                if (!AudioUtil::scheduleFullLengthSono($xcid)) {
                    XC_logger()->logError(
                        'Could not schedule full length sonogram creation and mp3 derivatives for ' . $xcfname
                    );
                }

                if ($d->isMystery) {
                    $subject = "ID Unknown from {$d->country} (XC$xcid)";
                    ForumPost::create($d->discussionStatus, $subject, $formData->remarks, $xcid);
                    $redis->clearMysteryCache();
                }
                $redis->clearRecordingCache();
            }
        } else {
            notifyError('Database write error');
            XC_logger()->logError(
                "Database write error for sql statement '$sql_insert': " . mysqli()->error
            );
            return false;
        }

        return $xcid;
    }

    public static function prepareSubmittedValuesForUpload($formData)
    {
        $d = new RecordingData();

        // get species info from database
        $species_nr = $formData->speciesNr;
        $sp = Species::load($species_nr, false);

        $d->uploadDate = date('Y-m-d h:i:s');
        $d->recordingDate = str_replace('?', '0', $formData->recordingDate);

        $d->discussionStatus = ThreadType::NONE;
        if ($sp->isMystery()) {
            $d->discussionStatus = ThreadType::MYSTERY;
        }

        // figure out which 'branch' this recording applies to
        $d->americas = 0;
        $d->africa = 0;
        $d->asia = 0;
        $d->europe = 0;
        $d->australia = 0;

        $l = $formData->lat;
        if (empty($l) || !is_numeric($l)) {
            $d->lat = 'null';
        } else {
            $d->lat = floatval($l);
        }
        $l = $formData->lng;
        if (empty($l) || !is_numeric($l)) {
            $d->lng = 'null';
        } else {
            $d->lng = floatval($l);
        }
        $d->country = strip($formData->country);
        $res = query_db(
            "SELECT branch FROM world_country_list where country='{$d->country}'"
        );
        $row = $res->fetch_object();
        if ($row) {
            $branch = $row->branch;
            // special case for carribean island territories
            if ($branch == 'europe' && $d->lat > 0 && $d->lng > -100.0 && $d->lng < -50.0) {
                $branch = 'america';
            }
            // special case for asian island territories
            if ($branch == 'europe' && $d->lat < 0 && ($d->lng < -110.0 || $d->lng > 120.0)) {
                $branch = 'australia';
            }
            // special case for atlantic islands (tristan da cunha, etc)
            if ($branch == 'europe' && $d->lat < -30 && ($d->lng < 15.0 || $d->lng > -25.0)) {
                $branch = 'world';
            }
            // special case for falkland islands, etc
            if ($branch == 'europe' && $d->lat < -50 && ($d->lng < -50.0 || $d->lng > -65.0)) {
                $branch = 'america';
            }
            // special case for african island territories (e.g. reunion island)
            if ($branch == 'europe' && $d->lat < 0 && $d->lng > 35.0 && $d->lng < 70.0) {
                $branch = 'africa';
            }
            // special case for european Russia. Arbitrarily use 60 longitude
            if ($d->country == 'Russian Federation' && $d->lng < 60.0) {
                $branch = 'europe';
            }

            if ($branch == 'asia') {
                $d->asia = 'Y';
            }
            if ($branch == 'africa') {
                $d->africa = 'Y';
            }
            if ($branch == 'america') {
                $d->americas = 'Y';
            }
            if ($branch == 'europe') {
                $d->europe = 'Y';
            }
            if ($branch == 'australia') {
                $d->australia = 'Y';
            }

            // special-case
            if ($d->country == 'Papua New Guinea') {
                $d->australia = 'Y';
                $d->asia = 'Y';
            }
        }

        $d->remarks = escape($formData->remarks);
        // These two are not stored in remarks anymore
        $d->animalSeen = $formData->animalSeen == 'yes' ? 1 : ($formData->animalSeen == 'no' ? 0 : -1);
        $d->playbackUsed = $formData->playbackUsed == 'yes' ? 1 : ($formData->playbackUsed == 'no' ? 0 : -1);

        $stagingFile = $formData->filename;
        $filename = basename($stagingFile);

        // make sure all user-entered data is sanitized
        $d->genus = escape($sp->genus());
        $d->species = escape($sp->species());
        $d->isMystery = $sp->isMystery();
        $d->family = escape($sp->family());
        $d->speciesOrderNr = $sp->order_nr();
        $d->commonName = strip($sp->commonName());
        $d->ssp = strip($formData->ssp);
        $d->soundTypeExtra = strip($formData->soundTypeExtra);
        $d->quality = intval($formData->quality);
        $d->recordist = strip($formData->recordist);
        $d->time = strip($formData->time);
        $d->location = strip($formData->location);
        $d->elevation = strip($formData->elevation);

        $bgData = self::populateBackgroundSpecies($formData->bgSpecies);
        $d->bgSpecies = $bgData['species'];
        $d->bgExtra = escape($bgData['extra']);

        $d->filename = escape($filename);
        $d->speciesNr = strip($species_nr);
        $d->license = strip($formData->license);
        $d->id = intval($formData->id);
        $d->groupId = intval($formData->groupId);

        // Temperature hard-coded for grasshoppers atm
        $d->temperature = $formData->temperature != '' && $d->groupId == 2 ?
            floatval(str_replace(',', '.', $formData->temperature)) : 'null';
        $d->device = strip($formData->device);
        $d->microphone = strip($formData->microphone);
        $d->specimen = strip($formData->specimen);
        $d->automatic = $formData->automatic == 'yes' ? 1 : ($formData->automatic == 'no' ? 0 : -1);

        // Either a valid date between quotes or null
        // Test if recording method is studio recording, otherwise clear (XC-670)
        $collectionDate = (string)str_replace('?', '0', $formData->collectionDate);
        $d->collectionDate = ($collectionDate != ''
            && in_array(
                UploadStep::inStudioInputId(),
                $formData->soundProperties[$d->groupId]
            )) ? "'$collectionDate'" : 'null';

        $d->soundProperties = $formData->soundProperties[$d->groupId] ?? [];

        return $d;
    }

    public static function populateBackgroundSpecies($bgText)
    {
        $backgroundSpecies = $bgNrs = $bgExtra = [];

        if (!empty($bgText)) {
            $names = array_map('trim', explode(',', $bgText));
            foreach ($names as $name) {
                $spnr = Species::getSpeciesNumberForString($name);
                if (empty($spnr)) {
                    $bgExtra[] = $name;
                } else {
                    $bgNrs[] = $spnr;
                }
            }

            foreach ($bgNrs as $spNr) {
                $bg = [];
                $s = Species::load($spNr, false);
                if (!$s) {
                    continue;
                }
                $bg['species_nr'] = $spNr;
                $bg['scientific'] = $s->scientificName();
                $bg['english'] = $s->commonName();
                $bg['family'] = $s->family();

                $backgroundSpecies[] = (object)$bg;
            }
        }

        return [
            'species' => $backgroundSpecies,
            'extra' => implode(', ', $bgExtra)
        ];
    }

    protected static function setHistoryData($d)
    {
        // Remove upload date, otherwise even updates without changes are logged
        unset($d->uploadDate);
        return escape(json_encode($d));
    }

    protected static function updateBackgroundSpecies($sndNr, $backgroundSpecies)
    {
        $sndNr = intval($sndNr);
        if (!$sndNr) {
            return false;
        }

        query_db("DELETE FROM birdsounds_background WHERE snd_nr = $sndNr");
        foreach ($backgroundSpecies as $sp) {
            $scientific = escape($sp->scientific);
            $english = escape($sp->english);
            $family = escape($sp->family);

            $insert = "
                INSERT INTO `birdsounds_background` 
                (`snd_nr`, `species_nr`, `scientific`, `english`, `family`)
                VALUES 
                ($sndNr, '{$sp->species_nr}', '$scientific', '$english', '$family')";
            if (!query_db($insert)) {
                notifyError('Database write error background species');
                XC_logger()->logError(
                    "Database write error for sql statement '$insert': " . mysqli()->error
                );
                return false;
            }
        }

        (new XCRedis())->clearHourlyStatsCache();
        return true;
    }

    protected static function updateSoundProperties($sndNr, $soundProperties)
    {
        $sndNr = intval($sndNr);
        if (!$sndNr) {
            return false;
        }

        query_db("DELETE FROM birdsounds_properties WHERE snd_nr = $sndNr");
        foreach ($soundProperties as $propertyId) {
            $res = query_db('SELECT category_id, property FROM sound_properties WHERE id = ' . $propertyId);
            $row = $res->fetch_object();
            $property = escape($row->property);

            $insert = "
                INSERT INTO `birdsounds_properties` 
                (`snd_nr`, `property_id`, `category_id`, `property`)
                VALUES 
                ($sndNr, $propertyId, $row->category_id, '$property')";
            if (!query_db($insert)) {
                notifyError('Database write error sound properties');
                XC_logger()->logError(
                    "Database write error for sql statement '$insert': " . mysqli()->error
                );
                return false;
            }
        }

        return true;
    }
}
