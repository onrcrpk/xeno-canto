<?php

namespace xc\Controllers;

use xc\ForumPost;
use xc\HtmlUtil;
use xc\Language;
use xc\License;
use xc\NotificationPeriod;
use xc\Set;
use xc\Species;
use xc\ThreadType;
use xc\User;
use xc\XCMail;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\notifyWarning;
use function xc\sanitize;
use function xc\strip;
use function xc\XC_acceptableErrors;
use function xc\XC_formatUserText;

class Account extends LoggedInController
{
    public $pages;

    public function __construct($request)
    {
        parent::__construct($request);

        $this->pages = [
            '' => _('Account Overview'),
            'personal' => _('Personal Information'),
            'password' => _('Change Password'),
            'email' => _('Email Address'),
            'image' => _('User Photo'),
            'prefs' => _('Preferences'),
            'license' => _('Licenses'),
            'notifications' => _('Notifications'),
            'locations' => _('Locations'),
            'sets' => _('Recording Sets'),
            'forum' => _('Forum Topics'),
            'idquestion' => _('Identification Questions'),
        ];
    }

    public function handlePost($page)
    {
        if (!array_key_exists($page, $this->pages)) {
            return $this->notFound();
        }

        if ($page === 'personal') {
            if ($this->mypageProcessPersonalInformation()) {
                return $this->seeSubPage($page);
            }
        } elseif ($page === 'email') {
            if ($this->mypageProcessEmailForm()) {
                return $this->seeSubPage($page);
            }
        } elseif ($page === 'password') {
            if ($this->mypageProcessPasswordForm()) {
                return $this->seeSubPage($page);
            }
        } elseif ($page === 'image') {
            if ($this->mypageProcessUserPhotoForm()) {
                return $this->seeSubPage($page);
            }
        } elseif ($page === 'prefs') {
            if ($this->mypageProcessPreferencesForm()) {
                return $this->seeSubPage($page);
            }
        } elseif ($page === 'notifications') {
            if ($this->mypageProcessNotificationsForm()) {
                return $this->seeSubPage($page);
            }
        } elseif ($page === 'license') {
            if ($this->mypageProcessLicensesForm()) {
                return $this->seeSubPage($page);
            }
        }

        return $this->handleRequest($page);
    }

    private function mypageProcessPersonalInformation()
    {
        $warning = null;
        $blurb = strip($this->request->request->get('blurb'), true, true);
        $username = strip(trim($this->request->request->get('username')));
        $id = User::current()->userId();
        if (!$this->userCanUpdateBlurb() && $blurb) {
            $sql = "UPDATE users SET username='$username' where dir='$id'";
            $warning = $this->blurbRequirementsMessage();
            $blurb = '';
        } else {
            $sql = "UPDATE users SET username='$username', blurb='$blurb' where dir='$id'";
        }
        if (query_db($sql)) {
            notifySuccess(_('Successfully updated your personal information'));
            if ($warning) {
                notifyWarning($warning);
            }

            User::current()->reloadFromDatabase();

            if ($blurb) {
                $url = getUrl('recordist', ['id' => $id], true);
                $body = wordwrap(
                    "User $username ($url) updated their blurb to:\n\n$blurb",
                    68
                );
                (new XCMail(
                    WEBMASTER_EMAIL, '[xeno-canto] User blurb updated', $body
                ))->send();
            }

            return true;
        }

        notifyError(
            _(
                'Internal error while trying to update your personal information'
            )
        );
        return false;
    }

    private function userCanUpdateBlurb()
    {
        return (User::current()->isVerified() && (User::current()->age() > $this->blurbRequiredAge()));
    }

    private function blurbRequiredAge()
    {
        return 1;
    }

    private function blurbRequirementsMessage()
    {
        return sprintf(
                ngettext(
                    'In order to reduce spam, new users must verify their email address and wait at least %s hour before adding a user description to their account.',
                    'In order to reduce spam, new users must verify their email address and wait at least %s hours before adding a user description to their account.',
                    $this->blurbRequiredAge()
                ),
                $this->blurbRequiredAge()
            ) . ' ' . _('We apologize for the inconvenience.') . ' ' . sprintf(
                _(
                    "To re-send a verification code, please visit <a href='%s'>your account page</a>."
                ),
                getUrl('mypage')
            );
    }

    protected function seeSubPage($p)
    {
        return $this->seeOther(getUrl('mypage', ['p' => $p]));
    }

    private function mypageProcessEmailForm()
    {
        $email2 = $this->request->request->get('email2');
        $email3 = $this->request->request->get('email3');

        if ($email2 !== $email3) {
            notifyError(_("Emails don't match"));
            return false;
        }

        $error = null;
        if (!User::current()->updateEmailAddress($email2, $error)) {
            notifyError($error);
            return false;
        }

        notifySuccess(_('You have successfully changed your email address!'));
        return true;
    }

    private function mypageProcessPasswordForm()
    {
        $oldPass = $this->request->request->get('old-password');
        $newPass = $this->request->request->get('new-password');
        $newPassConfirm = $this->request->request->get('new-password-confirm');

        $userid = User::current()->userId();
        $res = query_db(
            "select salt, password from users where dir='$userid'"
        );
        $row = $res->fetch_object();
        $salt = $row->salt;
        $hashed = User::hashPassword($oldPass, $salt);

        // Circumvent old password check when masquerading as an admin
        if (!User::current()->isMasquerade()) {
            if (!$oldPass) {
                notifyError(_('Your current password was empty'));
                return false;
            }

            if ($hashed !== $row->password) {
                notifyError(_('Your current password was invalid.'));
                return false;
            }
        }

        if ($newPass !== $newPassConfirm) {
            notifyError(_("New passwords don't match"));
            return false;
        }
        if (strlen($newPass) < 5) {
            notifyError(_('Your password must be at least 5 characters long.'));
            return false;
        }

        $newHashedPass = User::hashPassword($newPass, $salt);
        $res = query_db(
            "UPDATE users set password='$newHashedPass' WHERE dir='$userid'"
        );

        if (!$res) {
            notifyError(_("Internal Error: couldn't update password"));
            return false;
        }

        $email = User::current()->emailAddress();
        User::current()->logout();
        app()->session()->migrate();
        User::loginWithPassword($email, $newPass);

        notifySuccess(_('You have successfully changed your password!'));
        return true;
    }

    private function mypageProcessUserPhotoForm()
    {
        $dir = User::current()->userId();
        $delete = !empty($this->request->request->get('delete'));

        // check if a file was uploaded
        if (!$delete && $_FILES['avatar']['error'] == UPLOAD_ERR_NO_FILE) {
            notifyError(_('No file selected. Try again.'));
            return false;
        }

        $absolute_path = "{$_SERVER['DOCUMENT_ROOT']}/graphics/memberpics";

        if ($delete) {
            if (User::current()->deleteAvatar()) {
                notifySuccess(_('Successfully deleted your user photo'));
                return true;
            }
            notifyError(_('Unable to delete user photo'));
            return false;
        }

        if ($_FILES['avatar']['error'] != UPLOAD_ERR_OK) {
            switch ($_FILES['avatar']['error']) {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    $errorMsg = _('Uploaded file was too large');
                    break;
                default:
                    $errorMsg = _('Internal Error');
            }
            notifyError(sprintf(_('Error uploading file: %s'), $errorMsg));
            return false;
        }

        $f = $_FILES['avatar'];

        if (!in_array($f['type'], ['image/jpeg', 'image/png', 'image/gif'])) {
            notifyError(
                _(
                    'Only jpeg, png or gif file types are supported. Please try a different image.'
                )
            );
            return false;
        }

        $rv = null;
        system(
            "convert -strip -quality 100 -resize 500x500 '{$f['tmp_name']}' 'PNG24:$absolute_path/$dir.png'",
            $rv
        );
        if ($rv) {
            notifyError(
                _(
                    'Internal error while creating user photo.  Please try again.'
                )
            );
            return false;
        }
        system(
            "convert -thumbnail 100x100 '{$f['tmp_name']}' '$absolute_path/$dir" . "_tb.png'",
            $rv
        );
        if ($rv) {
            notifyError(
                _(
                    'Internal error while creating thumbnail.  Please try again.'
                )
            );
            return false;
        }
        system("chmod ugo+rw '$absolute_path/$dir.png'");
        system("chmod ugo+rw '$absolute_path/$dir" . "_tb.png'");

        User::current()->getAvatar();

        notifySuccess(_('Successfully updated your user photo'));
        return true;
    }

    private function mypageProcessPreferencesForm()
    {
        $lang = escape($this->request->request->get('language'));
        $view = intval($this->request->request->get('table_view'));
        $numResults = intval($this->request->request->get('table_num_per_page'));
        $contact = intval($this->request->request->get('contact_form'));
        $searchLanguages = $this->request->request->get('search-languages');

        $dir = User::current()->userId();

        $res = query_db(
            "UPDATE users SET language='$lang', num_per_page=$numResults, view=$view, contactform=$contact WHERE dir='$dir'"
        );
        if (!$res) {
            notifyError(_('Internal error while saving preferences'));
            return false;
        }

        query_db("DELETE FROM user_search_languages WHERE userid='$dir'");
        foreach ($searchLanguages as $language) {
            query_db("INSERT INTO user_search_languages (userid, language) VALUES ('$dir', '$language')");
        }

        User::current()->reloadFromDatabase();
        app()->setLanguage(Language::lookup($lang), true);
        notifySuccess(_('Successfully updated your user preferences'));
        return true;
    }

    private function mypageProcessNotificationsForm()
    {
        $dir = User::current()->userId();

        if ($this->request->request->get('update')) {
            $str = escape($this->request->request->get('match_string'));
            $branch = escape($this->request->request->get('branch'));
            $timeperiod = intval($this->request->request->get('timeperiod'));
            $id = intval($this->request->request->get('id'));
            if (empty($str)) {
                notifyError(_('No search string specified'));
                return false;
            }

            $res = query_db(
                "update notifications set match_string='$str',
                branch='$branch', timeperiod=$timeperiod where id_nr = '$id'"
            );
            if (!$res) {
                notifyError(_('Unable to save the changes to the database'));
                return false;
            }

            notifySuccess(_('Successfully updated notification'));
        } elseif ($this->request->request->get('delete')) {
            $id = $this->request->request->get('id');
            $res = query_db(
                "delete from notifications where dir='$dir' and id_nr='$id'"
            );
            if (!$res) {
                notifyError(
                    _('Unable to delete notification from the database')
                );
                return false;
            }

            notifySuccess(_('Successfully deleted notification'));
        } elseif ($this->request->request->get('add')) {
            $str = escape($this->request->request->get('match_string'));
            $branch = escape($this->request->request->get('branch'));
            $timeperiod = intval($this->request->request->get('timeperiod'));

            if (empty($str)) {
                notifyError(_('No search string specified'));
                return false;
            }

            $res = query_db(
                "insert into notifications values ('$dir', $timeperiod, '$str', '$branch', null)"
            );
            if (!$res) {
                notifyError(
                    _('Unable to add the notification to the database')
                );
                return false;
            }

            notifySuccess(_('Successfully added notification'));
        }

        return true;
    }

    private function mypageProcessLicensesForm()
    {
        $action = escape($this->request->request->get('action'));
        $dir = User::current()->userId();

        if ($action == 'default') {
            $default = escape($this->request->request->get('default'));
            $query = "UPDATE users SET license='$default'";
        } elseif ($action == 'third_party') {
            $thirdParty = escape($this->request->request->get('third_party'));
            $query = "UPDATE users SET third_party_license='$thirdParty'";
            /* XC-294
            } else if ($action == 'bulk_update') {
            $bulkUpdate = \xc\escape($this->request->request->get('bulk_update'));
            $query = "UPDATE birdsounds SET license='$bulkUpdate'";
            */
        } else {
            return false;
        }
        $query .= " WHERE dir='$dir'";

        $res = query_db($query);
        if (!$res) {
            notifyError(_('Internal error while saving preferences'));
            return false;
        }

        User::current()->reloadFromDatabase();
        notifySuccess(_('Successfully updated your license'));
        return true;
    }

    public function handleRequest($page)
    {
        if (!array_key_exists($page, $this->pages)) {
            return $this->notFound();
        }

        $title = _('Your Account');
        $content = '';
        if ($page === 'personal') {
            $content .= $this->mypagePersonalInformationForm();
        } elseif ($page === 'password') {
            $content .= $this->mypagePasswordForm();
        } elseif ($page === 'email') {
            $content .= $this->mypageEmailForm();
        } elseif ($page === 'image') {
            $content .= $this->mypageUserPhotoForm();
        } elseif ($page === 'prefs') {
            $content .= $this->mypagePreferencesForm();
        } elseif ($page === 'notifications') {
            $content .= $this->mypageNotificationsForm();
        } elseif ($page === 'locations') {
            $content .= $this->mypageLocations();
        } elseif ($page === 'sets') {
            $content .= $this->mypageSets();
        } elseif ($page === 'forum') {
            $content .= $this->mypageForumTopics();
        } elseif ($page === 'idquestion') {
            $content .= $this->mypageIdQuestioned();
        } elseif ($page === 'license') {
            $content .= $this->mypageLicensesForm();
        } else {
            $content .= $this->mypageOverview();
        }

        $output = "
            <h1>$title</h1>
            <p>" . _(
                'This is your own corner on xeno-canto. There are various settings below that you can customize to your needs.'
            ) . "</p>
            <table>
            <tr>
            <td id='my-page-menu'>
            <ul class='simple'>";

        foreach ($this->pages as $slug => $desc) {
            $selected = '';
            if ($slug === $page) {
                $selected = "class='selected'";
            }

            $url = getUrl('mypage', ['p' => $slug]);
            $output .= "
            <li $selected>
            <a href='$url'>$desc</a>
            </li>";
        }

        $output .= "</ul>
            </td>
            <td id='my-page-content'>
            $content
            </tbody>
            </table>";

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'my-page']);
    }

    private function mypagePersonalInformationForm()
    {
        $user = User::current();

        if (!$user) {
            return '';
        }

        // fetch the blurb from the DB rather than accessing the session user object
        // because we want to be sure that we're editing what's curently in the db.
        $res = query_db(
            "SELECT username, blurb FROM users WHERE dir='{$user->userId()}'"
        );
        $blurb = $username = '';
        if ($row = $res->fetch_object()) {
            $blurb = $row->blurb;
            $username = $row->username;
        }

        if ($this->userCanUpdateBlurb()) {
            $blurbInput = '
            <p>' . _(
                    'Write a paragraph or two about yourself so that other xeno-canto members can learn a bit more about you.'
                ) . ' ' . sprintf(
                    _('You can format your text using the %s text formatting syntax.'),
                    "<a target='_blank' href='" . getUrl('markdown') . "'>Markdown</a>"
                ) . "</p>
            <textarea  name='blurb' placeholder='" . htmlspecialchars(
                    _('Introduce yourself...'),
                    ENT_QUOTES
                ) . "'>$blurb</textarea>";
        } else {
            $blurbInput = "<div class='warning'><p>" . $this->blurbRequirementsMessage() . '</p></div>';
        }

        return '
            <h2>' . _('Edit your personal information') . "</h2>
                
            <form method='post'>
            <input type='hidden' name='p' value='personal' />
            <p>" . _('Name') . ":</p>
            <input type='text' id='input-username' name='username' value='" . htmlspecialchars($username, ENT_QUOTES) . "'/>
            $blurbInput
            <p>
            <input type='submit' value='" . htmlspecialchars(_('Update'), ENT_QUOTES) . "'/>
            </p>
            </form>
        ";
    }

    private function mypagePasswordForm()
    {
        $html = '';
        $user = User::current();

        if (!$user) {
            return $html;
        }

        $res = query_db(
            "SELECT email FROM users where dir='{$user->userId()}'"
        );
        if ($row = $res->fetch_object()) {
            $html = '
                <h2>' . _('Change Password') . '</h2>
                <p>' . _(
                    'To change your password, please fill out the form below'
                ) . "</p>
                    
                <form method='post'>
                <input type='hidden' name='p' value='password'>
                <table class='settings-form'>
                <tr><td>" . _('Current Password') . ":</td>
                <td><input type='password' name='old-password' /></td></tr>
                    
                <tr><td>" . _('New Password') . ":</td><td>
                <input type='password' name='new-password' /></tr>
                    
                <tr><td>" . _('Confirm New Password') . ":</td><td>
                <input type='password' name='new-password-confirm' /></tr>
                <tr>
                <td></td>
                <td>
                <input type='submit' value='" . htmlspecialchars(
                    _(
                        'Change Password'
                    ),
                    ENT_QUOTES
                ) . "'>
                </td>
                </tr>
                </table>
                </form>
            ";
        }

        return $html;
    }

    private function mypageEmailForm()
    {
        $html = '';
        $user = User::current();

        if (!$user) {
            return $html;
        }

        $email2 = $this->request->request->get('email2');
        $email3 = $this->request->request->get('email3');
        $res = query_db(
            "SELECT email FROM users where dir='{$user->userId()}'"
        );
        if ($row = $res->fetch_object()) {
            $html = '
                <h2>' . _('Change Email Address') . '</h2>
                <p>' . _(
                    'To change your email address, please fill out the form below'
                ) . "</p>
                    
                <form method='post'>
                <input type='hidden' name='p' value='email'>
                <table class='settings-form'>
                <tr><td>" . _(
                    'Old Email Address'
                ) . ":</td><td><input type='text' disabled value='" . sanitize(
                    $row->email
                ) . "'/></td></tr>
                <tr><td>" . _('New Email Address') . ":</td><td>
                <input type='text' name='email2'  value='$email2'></tr>
                <tr><td>" . _('Confirm New Email Address') . ":</td><td>
                <input type='text' name='email3'  value='$email3'></tr>
                <tr>
                <td></td>
                <td>
                <input type='submit' value='" . sanitize(_('Update')) . "'>
                </td>
                </tr>
                </table>
                </form>
            ";
        }

        return $html;
    }

    private function mypageUserPhotoForm()
    {
        $user = User::current();

        if (!$user) {
            return null;
        }

        $random = bin2hex(random_bytes(5));

        $html = '
            <h2>' . _('User Photo') . '</h2>
            <p>' . _(
                'This image will be used on your recordist profile page and in various other places around the site.'
            ) . "</p>
            <div>
            <img class='avatar' src='{$user->getAvatar()}?{$random}' />
            <img class='avatar' src='{$user->getThumbnail()}?{$random}}' />
            </div>
            <p>" . _(
                'To change your user photo, upload a new image below. The image can be in JPEG, PNG, or GIF format.'
            ) . "</p>
            <form enctype='multipart/form-data' method='post'>
            <input type='hidden' name='p' value='image'>
            <input type='file' name='avatar' />
            <p>
            <input type='submit' value='" . (!$user->hasAvatar() ? _(
                'Upload'
            ) : _(
                'Replace'
            )) . "'>";
        if ($user->hasAvatar()) {
            $html .= "
                <input type='submit' name='delete' value='" . _(
                    'Delete'
                ) . "' />";
        }
        $html .= '
            </p>
            </form>';

        return $html;
    }

    private function mypagePreferencesForm()
    {
        $user = User::current();
        if (!$user) {
            return null;
        }

        $res = query_db(
            "SELECT language, view, num_per_page, license, contactform FROM users WHERE dir='{$user->userId()}'"
        );
        if (!$res) {
            return null;
        }
        $row = $res->fetch_object();

        $html = '
            <h2>' . _('Preferences') . "</h2>
            <p>" . _(
                'Customize the behavior of the site to your preferences by changing the values below.'
            ) . "</p>
            <form method='post'>
            <input type='hidden' name='p' value='prefs'>
            <table class='settings-form'>
            <tr>
            <td>" . _('Language') . ":</td>
            <td>
            <select name='language'>";
        foreach (Language::all() as $locale) {
            $html .= '<option ';
            if ($locale->code == $row->language) {
                $html .= ' selected ';
            }
            $html .= " value='$locale->code'>$locale->name</option>";
        }
        $html .= "</select>
            </td>
            <td>
            <img data-qtip-header='" . htmlspecialchars(_('Language'), ENT_QUOTES) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    'Your preferred language. Please note that not all languages have full translations at the moment.'
                ),
                ENT_QUOTES
            ) . "' class='tooltip' class='icon' src='/static/img/question-icon.png' />
            </td>
            </tr>
            <tr>
            <td style='vertical-align: top;'>" . _('Search languages') . ' (' . _("for common names") . "):</td>
            <td>";
        foreach (Language::all() as $locale) {
            $html .= "
            <div>
            <input type='checkbox' id='search-$locale->code' ";
            if (array_key_exists($locale->code, $user->searchLanguages()) || $locale->code == 'en') {
                $html .= ' checked ' . ($locale->code == 'en' ? 'disabled ' : '');
            }
            $html .= " name='search-languages[]' value='$locale->code'>
                <label for='search-$locale->code'>$locale->name</label>
            </div>";
        }
        $html .= "</select>
            </td>
            <td style='vertical-align: top;'>
            <img data-qtip-header='" . htmlspecialchars(_('Search languages'), ENT_QUOTES) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    'Search for common names in the selected languages. Common names in other languages but English currently are available almost exclusively for birds. Note that the more languages you select, the longer the search will take and the more duplicate results you are likely to get, so try to be selective!'
                ),
                ENT_QUOTES
            ) . "' class='tooltip' class='icon' src='/static/img/question-icon.png' />
            </td>
            </tr>
            
            <tr>
            <td>" . _('Table view') . ":</td>
            <td>
                
            <select name='table_view'>
        ";

        $views = [
            0 => _('detailed'),
            1 => _('concise'),
            3 => _('sonograms'),
        ];

        foreach ($views as $k => $v) {
            $html .= '<option ';
            if ($k === intval($row->view)) {
                $html .= ' selected ';
            }
            $html .= " value='$k'>$v</option>
        ";
        }

        $html .= "</select></td>
            <td>
            <img data-qtip-header='" . htmlspecialchars(_('Table view'), ENT_QUOTES) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    'The tables with recordings on Identify, Browse, Random and Revise pages can be viewed in four different ways. Concise views are fastest, Detailed views have most information, Sonogram views show the sonograms for each recording, and Codes shows the sound characteristics.'
                ),
                ENT_QUOTES
            ) . "' class='tooltip' class='icon' src='/static/img/question-icon.png' />
            </td>
            </tr>
            <tr>
            <td>" . _('Number of results per page') . ":
            </td>
            <td>
                
            <select name='table_num_per_page'>
        ";

        $nums[] = 9;
        $txt_nums[] = '9';
        $nums[] = 15;
        $txt_nums[] = '15';
        $nums[] = 30;
        $txt_nums[] = '30';
        $nums[] = 45;
        $txt_nums[] = '45';
        $nums[] = 60;
        $txt_nums[] = '60';

        for ($k = 0; $k < count($nums); $k++) {
            $html .= '<option ';
            if ($nums[$k] === intval($row->num_per_page)) {
                $html .= ' selected ';
            }
            $html .= " value='" . $nums[$k] . "'>" . $txt_nums[$k] . '</option>
            ';
        }
        $html .= "</select></td>
            <td>
            <img data-qtip-header='" . htmlspecialchars(_('Number of results'), ENT_QUOTES) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    'The number of search results displayed in each table in Identify, Browse, Random or Revise pages. Choose lower values to get smaller pages that download faster.'
                ),
                ENT_QUOTES
            ) . "' class='tooltip'  class='icon' src='/static/img/question-icon.png' />
            </td>
            </tr>";

        $html .= '
                <tr>
                <td>' . _('Contact form') . ":</td>
                <td>
                    
                <select name='contact_form'>
        ";

        $opts = [
            0 => _('no'),
            1 => _('yes'),
        ];

        foreach ($opts as $k => $v) {
            $html .= '<option ';
            if ($k === intval($row->contactform)) {
                $html .= ' selected ';
            }
            $html .= " value='$k'>$v</option>
             ";
        }

        $html .= "</select>
            </td>
            <td>
            <img data-qtip-header='" . htmlspecialchars(_('Contact form'), ENT_QUOTES) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    'Allow other registered xeno-canto users to contact you directly via a form on this website. Enabling this option will not reveal your email address to anyone.'
                ),
                ENT_QUOTES
            ) . "' class='tooltip' class='icon' src='/static/img/question-icon.png' />
            </td>
            </tr>
            <tr>
            <td>
            </td>
            <td>
            <input type='submit' value='" . htmlspecialchars(_('Update'), ENT_QUOTES) . "'>
            </td>
            </tr>
            </table>
                
            </form>
        ";

        return $html;
    }

    private function mypageNotificationsForm()
    {
        $user = User::current();

        if (!$user) {
            return null;
        }

        $dir = User::current()->userId();
        $res = query_db(
            "select * from notifications where dir = '$dir' order by id_nr ASC"
        );
        $pageTitle = _('Notifications');
        $html = "
            <section id='notifications'>
            <h2>$pageTitle</h2>
            <p>" . _(
                'Do you want to be notified when interesting recordings are added to
            xeno-canto? Then add your own notifications here. A daily, weekly or monthly
            digest will be sent with an overview of the recordings matching your search
            criteria.'
            ) . '
            </p>
            <p>' . sprintf(
                _(
                    'These search criteria are given using the same keywords as in normal xeno-canto searches. So for example, use %s to select only recordings made in Peru, or %s to keep abreast on recordings of songtype "call" of crows of the genus %s. For a complete overview of all search critera, see the <a href="%s">search tips</a>.'
                ),
                '<tt>cnt:Peru</tt>',
                '<tt>Corvus type:call</tt>',
                '<i>Corvus</i>',
                getUrl('tips')
            ) . '
            </p>
            <p>' . _(
                'If you simply want a list of all recordings (be prepared for about 50 new
            additions per day), choose <tt>idae</tt> as the search string, and choose a
            branch of your choice.  This search string matches any family name (they all
            end in -idae), and hence any recording.'
            ) . '
            </p>
                
            <h2>' . _('Your Notifications') . "</h2>
            <div id='notification-form'>
            <div class='row'>
            <div>" . _('Search string') . '</div>
            <div>' . _('Time period') . '</div>
            <div>' . _('Region') . '</div>
            <div></div>
            </div>
        ';

        while ($row = $res->fetch_object()) {
            $html .= $this->mypageNotificationsFormRow(
                $row->id_nr,
                $row->match_string,
                $row->timeperiod,
                $row->branch
            );
        }

        // now add an extra row for adding a new notification
        $html .= $this->mypageNotificationsFormRow() . '</section>';

        return $html;
    }

    private function mypageNotificationsFormRow(
        $id = null,
        $match_string = null,
        $timeperiod = null,
        $branch = null
    ) {
        $html = "<form method='post' class='species-completion row'>
            <div>
            <input type='hidden' name='p' value='notifications' />
            <input type='hidden' name='id' value='$id' />
            <input type='text' class='species-input' name='match_string' placeholder='" . htmlspecialchars(
                _(
                    'Add a new notification'
                ),
                ENT_QUOTES
            ) . "...' value='" . htmlspecialchars(
                $match_string,
                ENT_QUOTES
            ) . "' /></div><div>" . $this->timePeriodSelect(
                'timeperiod',
                $timeperiod
            ) . '</div>
            <div>' . HtmlUtil::regionSelect('branch', $branch) . '</div>';

        if ($id) {
            $html .= "<div><input type='submit' name='update' value='" . _(
                    'Update'
                ) . "' /><input type='submit' name='delete' value='" . _(
                    'Delete'
                ) . "' /></div>";
        } else {
            $html .= "<div><input type='submit' name='add' value='" . _(
                    'Add'
                ) . "' /></div>";
        }

        $html .= '</form>';

        return $html;
    }

    private function timePeriodSelect(
        $name,
        $selected = null,
        $id = '',
        $class = ''
    ) {
        $options = [
            NotificationPeriod::DAILY => _('Daily'),
            NotificationPeriod::WEEKLY => _('Weekly'),
            NotificationPeriod::MONTHLY => _('Monthly'),
        ];

        if (!$selected) {
            $selected = NotificationPeriod::DAILY;
        } else {
            $selected = intval($selected);
        }

        return HtmlUtil::selectInput($options, $name, $selected, $id, $class);
    }

    private function mypageLocations()
    {
        $html = '<h2>' . _('Locations') . "</h2>
            <ul class='simple'>
            ";
        $userid = User::current()->userId();
        $res = query_db(
            "SELECT location, country, count(*) as nrecs, COUNT(latitude AND longitude) as ncoords from birdsounds WHERE dir='$userid' GROUP BY location, country ORDER BY country ASC, TRIM(location) ASC"
        );

        $missing = false;
        if ($res->num_rows > 0) {
            $lastCountry = null;
            while ($row = $res->fetch_object()) {
                if ($lastCountry != $row->country) {
                    if ($lastCountry !== null) {
                        $html .= '</ul></li>';
                    }
                    $html .= "<li>{$row->country}
                    <ul>";
                }
                $lastCountry = $row->country;
                if (!$row->location) {
                    $loc = _('unnamed');
                } else {
                    $loc = htmlspecialchars($row->location, ENT_QUOTES);
                }
                $missingText = '';
                if ($row->ncoords < $row->nrecs) {
                    $missing = true;
                    $missingText = "<span title='" . sprintf(
                            _(
                                '%d of %d recordings are missing coordinates'
                            ),
                            ($row->nrecs - $row->ncoords),
                            $row->nrecs
                        ) . "'>*</span>";
                }
                // The number of recordings: for example "12 recordings"
                $html .= "<li><strong>$loc</strong>: " . sprintf(
                        _(
                            '%s recordings'
                        ),
                        $row->nrecs
                    ) . "$missingText (<a href='" . getUrl(
                        'location-edit', ['loc' => $row->location, 'cnt' => $row->country]
                    ) . "'>" . _(
                        'edit'
                    ) . '</a>)</li>';
            }
        } else {
            $html .= "<li class='unspecified'>" . _('None') . '</li>';
        }

        $html .= '</ul>
            </li>
            </ul>';
        if ($missing) {
            $html .= '<p><strong>*</strong> ' . _(
                    'These locations contain recordings with missing coordinates'
                ) . '</a></p>';
        }

        return $html;
    }

    private function mypageSets()
    {
        $html = '<h2>' . $this->pages['sets'] . '</h2>
            <p>' . _(
                "Sets allow you to create and curate groups of recordings. Perhaps you want to bookmark your favorite recordings, or create a study list for an upcoming trip.  You can create as many sets as you'd like, for any purpose you'd like. Sets can be edited or deleted at any time."
            ) . '
            <p>
            <p>' . sprintf(
                _(
                    'In order to add recordings to a set, you can click this icon (%s) wherever you see it on the site. After clicking that icon, you will be prompted to choose a set to which you wish to add the recording.'
                ),
                "<img class='icon' src='/static/img/bookmark-add.png' alt='icon'/>"
            ) . '
            </p>
            <p>' . _(
                'You can choose from 3 different visibility levels for each set:'
            ) . "
            </p>
            <ul class='simple'>
            <li>" . _(
                '<strong>Private</strong> sets are only visible to you.'
            ) . '
            </li>
            <li>' . _(
                '<strong>Unlisted</strong> sets are visible to anybody that knows
            the URL of the set, but will not be displayed on your public profile
            or in search results. This allows you to send a link to friends
            without making the set fully public.'
            ) . '
            </li>
            <li>' . _(
                '<strong>Public</strong> sets are visible to anyone and are also
            displayed on your public profile and may show up in search results.'
            ) . '
            </li>
            </ul>
            <h3>' . _('Your Sets') . "</h3>
            <ul class='simple'>";

        $userid = User::current()->userId();
        $sets = Set::loadUserSets($userid, true);

        if ($sets) {
            foreach ($sets as $set) {
                $class = '';
                if ($set->visibility() != Set::VISIBILITY_PUBLIC) {
                    $class = 'class="draft"';
                }
                $html .= "<li $class><a href='{$set->url()}'>{$set->name()}</a> &mdash; " . sprintf(
                        ngettext(
                            '%d recording',
                            '%d recordings',
                            intval($set->numRecordings())
                        ),
                        $set->numRecordings()
                    ) . "
                <a href='" . getUrl('set-edit', ['id' => $set->id()]
                    ) . "'><img src='/static/img/edit.png' title='" . sanitize(
                        _(
                            'Edit Set Details'
                        )
                    ) . "' class='icon'/></a>" . "<a href='" . getUrl('set-delete', ['id' => $set->id()]
                    ) . "'><img src='/static/img/delete.png' title=" . sanitize(
                        _(
                            'Delete Set'
                        )
                    ) . "'' class='icon'/></a>
            </li>";
            }
        } else {
            $html .= "<li class='unspecified'>" . _('None') . '</li>';
        }

        $html .= "
        <li><a href='" . getUrl('set-new') . "'>" . _('Create a new set') . '</a></li>
        </ul>
        ';

        return $html;
    }

    private function mypageForumTopics()
    {
        $user = User::current();

        if (!$user) {
            return null;
        }

        $html = '
            <h2>' . _('Forum Topics') . '</h2>
            <p>' . _(
                'The following forum topics are related to recordings that you have uploaded:'
            ) . '</p>';

        $userid = $user->userId();

        // Get a list all forum topics related to this recordists' recordings
        $sqlForumPosts = "select forum_world.subject, forum_world.topic_nr from forum_world left join birdsounds on birdsounds.snd_nr = forum_world.snd_nr WHERE birdsounds.dir='$userid' ORDER BY topic_nr DESC";
        $resForum = query_db($sqlForumPosts);
        $numRows = $resForum->num_rows;
        if ($numRows >= 1) {
            $html .= '
                <ul>';
            while ($row = $resForum->fetch_object()) {
                $html .= "
                    <li><a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]) . "'>$row->subject</a></li>
                    ";
            }
            $html .= ' </ul>';
        } else {
            $html .= "<p class='unspecified'>" . _('None') . '</p>';
        }

        // a list of all forum topics started by the user
        $html .= '
            <p>' . _(
                'The following are forum topics that you initiated:'
            ) . '</p>';

        $sqlForumPosts = "select forum_world.subject, forum_world.topic_nr from forum_world WHERE forum_world.userid='$userid' ORDER BY topic_nr DESC";
        $resForum = query_db($sqlForumPosts);
        $numRows = $resForum->num_rows;
        if ($numRows >= 1) {
            $html .= '
                <ul>';
            while ($row = $resForum->fetch_object()) {
                $html .= "
                    <li><a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]) . "'>$row->subject</a></li>
                    ";
            }
            $html .= ' </ul>';
        } else {
            $html .= "<p class='unspecified'>" . _('None') . '</p>';
        }

        // a list of all forum topics the user has participated in
        $html .= '
            <p>' . _(
                'The following are additional forum topics that you participated in:'
            ) . '</p>';

        $sqlForumPosts = "select forum_world.subject, forum_world.topic_nr from forum_world INNER JOIN discussion_forum_world USING(topic_nr) WHERE forum_world.userid!='$userid' AND discussion_forum_world.dir='$userid' GROUP BY topic_nr ORDER BY mes_nr DESC";
        $resForum = query_db($sqlForumPosts);
        $numRows = $resForum->num_rows;
        if ($numRows >= 1) {
            $html .= '
                <ul>';
            while ($row = $resForum->fetch_object()) {
                $html .= "
                    <li><a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]) . "'>$row->subject</a></li>
                    ";
            }
            $html .= ' </ul>';
        } else {
            $html .= "<p class='unspecified'>" . _('None') . '</p>';
        }

        return $html;
    }

    private function mypageIdQuestioned()
    {
        $user = User::current();

        if (!$user) {
            return null;
        }

        $userid = $user->userId();

        $sql = 'SELECT dir, U.username, country, SUM(IF(discussed!=' . ThreadType::MYSTERY . ',1,0)) as N, SUM(IF(discussed=' . ThreadType::ID_QUESTIONED . ', 1, 0)) AS NQ,
            SUM(IF(discussed=' . ThreadType::ID_UNCONFIRMED . ",1,0)) as NU
            FROM birdsounds INNER JOIN users U USING(dir) WHERE dir='$userid' GROUP BY dir, country ORDER BY country ASC";
        $res = query_db($sql);
        $html = '';

        if ($res->num_rows > 0) {
            $html .= '<h2>' . _('Questioned Recordings by Country') . '</h2>
                    <p>' . _(
                    "To protect the integrity of the collection, certain upload restrictions will be enforced when the number of questioned recordings in a given country exceeds a calculated limit. The limit is based on the total number of recordings you've uploaded for that country. The table below shows you how close you are to these limits."
                ) . "
                    <a href='" . getUrl('discuss_forum', ['topic_nr' => 6743]) . "'>" . _(
                    'See this page for more details'
                ) . '</a>.</p>
                ';
            $html .= "<table class='results'>
                    <thead>
                    <tr>
                    <th>" . _('Country') . '</th>
                    <th>' . _('Total Recordings') . '</th>
                    <th>' . _('Errors Allowed') . '</th>
                    <th>' . _('Errors') . '</th>
                    <th>' . _('Status') . '</th>
                    </tr>
                    </thead>
                ';
            while ($row = $res->fetch_object()) {
                $n = $row->N;
                $acceptableErrors = XC_acceptableErrors($n);

                $totalErrors = $row->NQ + $row->NU;
                $html .= "<tr>
                        <td>{$row->country}</td>
                        <td>{$row->N}</td>
                        <td>$acceptableErrors</td>
                        <td>$totalErrors
                        (<a href='" . getUrl('recordist', [
                        'id' => $userid,
                        'query' => "cnt:\"{$row->country}\" dis:" . ThreadType::ID_QUESTIONED,
                    ]) . "'>{$row->NQ} " . ForumPost::$threadTypeIcons[ThreadType::ID_QUESTIONED] . "</a> /
                        <a href='" . getUrl('recordist', [
                        'id' => $userid,
                        'query' => "cnt:\"{$row->country}\" dis:" . ThreadType::ID_UNCONFIRMED,
                    ]) . "'>{$row->NU} " . ForumPost::$threadTypeIcons[ThreadType::ID_UNCONFIRMED] . '</a>)</td>
                        <td>';
                if ($totalErrors > $acceptableErrors) {
                    $html .= "<strong class='error'>" . _(
                            'Restricted'
                        ) . '</strong>';
                }

                $html .= '
                        </td>
                        </tr>';
            }

            $html .= '</table>';
        }

        $html .= '
                <h2>' . _("Recordings with 'ID Questioned' status") . '</h2>
                <p>' . _(
                'The following recordings that you have uploaded are currently flagged for Identification'
            ) . ':</p>';

        // Get a list of all of the user's recordings that have 'ID Questioned' status
        $questioned = ThreadType::ID_QUESTIONED;
        $needsConfirmation = ThreadType::ID_UNCONFIRMED;
        $mystery = ThreadType::MYSTERY;
        $mystNum = Species::mysterySpeciesNumber();
        $sqlQuestioned = "SELECT * from birdsounds WHERE dir='$userid' AND (discussed=$questioned OR discussed=$needsConfirmation OR (discussed=$mystery && species_nr!='$mystNum'))";
        $resQuestioned = query_db($sqlQuestioned);
        $numRows = $resQuestioned->num_rows;
        if ($numRows >= 1) {
            $html .= '
                    <ul>';
            while ($row = $resQuestioned->fetch_object()) {
                $rlink = getUrl('recording', ['xcid' => $row->snd_nr]);
                $html .= "
                    <li><a href='$rlink'>XC{$row->snd_nr}</a>: {$row->eng_name} ({$row->genus} {$row->species}), {$row->country}</li>
                    ";
            }
            $html .= '
                    </ul>';
        } else {
            $html .= "<p class='unspecified'>" . _('None') . '</p>';
        }
        return $html;
    }

    private function mypageLicensesForm()
    {
        $user = User::current();
        $referer = $this->request->query->get('referer');

        if (!$user) {
            return null;
        }

        $res = query_db(
            "SELECT license, third_party_license FROM users WHERE dir='{$user->userId()}'"
        );
        if (!$res) {
            return null;
        }

        $row = $res->fetch_object();

        $res = query_db(
            "SELECT COUNT(1) AS num_rec FROM birdsounds WHERE dir='{$user->userId()}'"
        )->fetch_object();
        $numRecordings = $res->num_rec;

        if ($numRecordings > 0) {
            $res = query_db(
                "SELECT DISTINCT license, COUNT(license) AS num FROM birdsounds WHERE dir = '{$user->userId()}' GROUP BY license ORDER BY num DESC"
            );
            $currentLicenses = [];
            while ($r = $res->fetch_object()) {
                $currentLicenses[] = "$r->license ($r->num)";
            }
            $currentLicenseHtml = sprintf(
                    _(
                        'For your %d recording(s), the following license(s) are currently in use'
                    ),
                    $numRecordings
                ) . ': ' . implode(', ', $currentLicenses) . '.';
        }

        $html = '<h2>' . _('Licenses') . '</h2>';

        if ($referer == 'login' || $row->third_party_license == -1) {
            $html .= "<p class='prominent-button'>" . _(
                    "You've been redirected here because you haven't told us your metadata sharing preferences yet. If you have shared sounds already, or are planning to, please take a minute to read through the information below, and make sure you indicate your preference in both cases."
                ) . '</p>';
        }

        $html .= '<p>' . _(
                "You can only share recordings on Xeno-canto under a Creative Commons (\"CC\") license. We've chosen a default license (CC BY-NC-SA), but you are invited to read up on the various types of CC licenses and choose the one you want from the list. Please read the relevant bits on: <a href='https://www.xeno-canto.org/about/terms'>www.xeno-canto.org/about/terms</a>. You'll see that the licenses vary in the restrictions they place on use and on attribution. Please note that the license, once chosen, will remain with the recording. You can change your choice of default license, but it will only be applied to recordings uploaded after that change. You can choose to apply a different license to a specific recording during upload if you want."
            ) . '<p></p>' . _(
                'Please note that the terms state that <strong>the recordist</strong> is the copyright holder. That means that XC is <strong>not</strong> the copyright holder. This is an important characteristic of XC. Except in cases that are clearly covered by the licenses, requests about the use of particular recordings that reach XC will always be transferred to you.'
            ) . "</p>
            <form method='post'>
            <input type='hidden' name='p' value='prefs'>
            <table class='settings-form'>
            <tr>
            <td>" . _('Default recording license') . ":
            </td>
            <td>
            <select name='default'>'
        ";

        $licenseOptions = '';
        $licenses = License::uploadOptions();
        if ($row->license && !in_array($row->license, $licenses)) {
            $licenses[] = $row->license;
        }
        $licenseSelected = array_fill_keys($licenses, '');
        $licenseSelected[$row->license] = 'selected';

        for ($i = 0; $i < count($licenses); $i++) {
            $lic = License::lookupById($licenses[$i]);
            $licenseOptions .= "<option value='{$lic->id}' {$licenseSelected[$lic->id]}>{$lic->name}</option>";
        }
        $html .= $licenseOptions . "</select>
            <img data-qtip-header='" . htmlspecialchars(
                _('Default License'),
                ENT_QUOTES
            ) . "'
            data-qtip-content='" . htmlspecialchars(
                _(
                    "The license that you'd like to apply by default to all uploaded recordings from now on. This does not affect recordings that you've already uploaded, and can be changed on a per-recording basis."
                ),
                ENT_QUOTES
            ) . "' class='tooltip'  class='icon' src='/static/img/question-icon.png' />
            </td>
            </tr>
            <tr>
            <td>
            </td>
            <td>
            <button type='submit' class='prominent-button' name='action' value='default'>" . htmlspecialchars(
                _(
                    'Update default'
                ),
                ENT_QUOTES
            ) . '</button>
            </td>
            </tr>
            </table>
            
            <h2>' . _('Sharing metadata with GBIF') . '</h2>
            ' . _(
                'In principle, the CC licenses allow XC to share the metadata of the recordings (such as time, sound type, remarks, location etc.) with other institutions/websites under your chosen default license.'
            ) . '</p><p>' . _(
                "However, one particular institution that we would like to share your metadata with, the <a href='https://www.gbif.org'>Global Biodiversity Information Facility</a> (\"GBIF\") has a policy to only accept metadata shared under a set of CC licenses that sometimes differ from the ones that XC has allowed in the past. Read about their reasoning here: <a href='https://www.gbif.org/terms'>www.gbif.org/terms</a>."
            ) . "</p><p class='important' style ='width: 75%; margin: 20px;'>" . _(
                "XC would like that \"our\" metadata appear in searches on www.gbif.org that can combine results from all kinds of biodiversity archives from around the world. We are confident this will increase the impact of your recordings on XC. We've asked some XC contributors and they all agreed to share their metadata under the CC BY-NC license. So, we ask your permission to include the metadata of your recordings in a single set that will appear credited to Xeno-canto on GBIF with a CC BY-NC license. Please note that this license covers only the set of metadata, <strong>not</strong> the actual sound files. The individual recordings will always appear on the GBIF site in a way that honours the CC license that you've chosen for a particular recording. Also, if similar opportunities might arise in the future, we'd like to be able to act upon them in the same way."
            ) . '</p><p>' . _(
                "If you agree to the above, please indicate \"Yes\" in the menu below, if not, indicate \"No\"."
            ) . "</p>
            <table class='settings-form'>
            <tr>
            <td>" . _('Permission to share metadata of recordings') . ":</td>
            <td>
            <select name='third_party'>";

        // Delete blank option if anything has been chosen
        $thirdParty = License::thirdParty();
        if ($row->third_party_license > -1) {
            unset($thirdParty[-1]);
        }
        foreach ($thirdParty as $k => $v) {
            $html .= '<option ';
            if ($k == $row->third_party_license) {
                $html .= ' selected ';
            }
            $html .= " value='$k'>" . _($v) . '</option>';
        }
        $html .= "</select>
            </td>
            </tr>
            <tr>
            <td>
            </td>
            <td>
            <button type='submit' class='prominent-button' name='action' value='third_party'>" . htmlspecialchars(
                _(
                    'Update sharing'
                ),
                ENT_QUOTES
            ) . '</button>
            </td>
            </tr>
            </table>';

        /* XC-294 disabled for the time being
        if ($numRecordings > 0) {
            $html .= "
            <h2>" . _("Update the license for all your recordings") . "</h2>
            <p>" . _("An explanation here.") . "</p>
            <p>$currentLicenseHtml</p>
            <table class='settings-form'>
            <tr>
            <td>" . _("New recording license") . ":
            </td>
            <td>
            <select name='bulk_update'>'" . $licenseOptions . "</select>
            </td>
            </tr>
            <tr>
            <td>
            </td>
            <td>
            <button type='submit' class='prominent-button' name='action' id='bulk_update' value='bulk_update'>" .
                htmlspecialchars(_("Update recordings"), ENT_QUOTES) . "</button>
            </td>
            </tr>
            </table>

            <script>
                var warning = '" . _('Are you sure you want to change the license for all your recordings? This action is irreversible!') . "';
                jQuery('#bulk_update').click(function() {
                    if (!confirm(warning)) {
                        return false;
                    }
                });
            </script>";
        }
        */

        $html .= '</form>';

        return $html;
    }

    private function mypageOverview()
    {
        $user = User::current();

        if (!$user) {
            return null;
        }

        $res = query_db(
            "SELECT username, email, blurb, verified FROM users WHERE dir='{$user->userId()}'"
        );
        if ($row = $res->fetch_object()) {
            $resendVerification = '';
            if (!$row->verified) {
                $resendVerification = "
                    <div class='important'>
                    <form method='post' action='" . getUrl(
                        'verify-email-resend'
                    ) . "'>
                    <p>" . _('Email address not verified.') . "
                    <input type='submit' value='" . sanitize(
                        _(
                            'Re-send verification code'
                        )
                    ) . "'/>
                    </p>
                    </form>
                    </div>
                    ";
            }
            if ($row->blurb) {
                $blurb = XC_formatUserText($row->blurb);
            } else {
                $blurb = "<p class='unspecified'>" . _(
                        'Introduce yourself...'
                    ) . '</p>';
            }

            return "
                    <div>
                    <img class='avatar' src='{$user->getAvatar()}' />
                    </div>
                    <h4>
                    " . _('Name') . "
                    </h4>
                    <p>
                    {$row->username}
                    <p>
                    <h4>
                    " . _('Email') . "
                    </h4>
                    <p>
                    {$row->email}
                    $resendVerification
                    <p>
                    <h4>" . _('Your Recordings') . "</h4>
                    <p>
                    <a href='" . getUrl('recordist', ['id' => $user->userId()]) . "'>" . _(
                    'View your public profile'
                ) . '</a>
                    </p>
                    <h4>
                    ' . _('About yourself') . "
                    </h4>
                    <div>
                    $blurb
                    <div>
                    ";
        }

        return '';
    }
}
