<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\License;

use function xc\getUrl;

class Search extends Controller
{
    private $properties = [];

    private $groupProperties = [];

    private $searchFields = [
        'search-ssp' => 'ssp',
        'search-genus' => 'gen',
        'search-bg' => 'also',
        'search-type' => 'type',
        'search-location' => 'loc',
        'search-country' => 'cnt',
        'search-remarks' => 'rmk',
        'search-recordist' => 'rec',
        'search-license' => 'lic',
        'search-seen' => 'seen',
        'search-playback' => 'playback',
        'search-group' => 'grp',
        'search-sex' => 'sex',
        'search-stage' => 'stage',
        'search-method' => 'method',
        'search-quality' => 'q',
        'search-auto' => 'auto',
        'search-type-other' => 'othertype',
    ];

    public function handlePost()
    {
        return $this->seeOther(getUrl('browse', ['query' => $this->getQueryString()]));
    }

    private function getQueryString()
    {
        $query = $this->request->request->get('search-term') . ' ';
        foreach ($this->searchFields as $field => $tag) {
            $value = $this->request->request->get($field);
            if ($value) {
                $query .= "$tag:\"$value\" ";
            }
        }
        return trim($query);
    }

    public function handleRequest()
    {
        $completionUrl = getUrl('api-completion-locations');
        $extrajs = "
            <script type='text/javascript'>
            jQuery('document').ready(function() {
                jQuery('input[name=search-location]').autocomplete({
                    serviceUrl: '$completionUrl',
                    minChars: 3,
                    deferRequestBy: 200,
                    onSelect: function (suggestion) {
                        localStorage.setItem('search-location', suggestion);
                    }
                })

                // Set group-specific options on load (either from form or local storage)
                var group = jQuery('select[name=search-group]').val() || localStorage.getItem('search-group') || '';
                setOptions(group);
                
                // Replace options on group change
                jQuery('select[name=search-group]').change(function() { 
                    setOptions(jQuery(this).val());
                })
                
                // Add handlers to store search values in local storage for all form fields, except search-term and 
                // search-location. The first is excluded because it's used elsewhere and we don't want to store it 
                // permanently; the latter is processed in the location autocomplete.
                var excludedFields = ['search-term', 'search-location'];
                jQuery('input[type=text],select').each(function() {
                     if (this.name.includes('search-') && !excludedFields.includes(this.name)) {
                        jQuery(this).change(function() { 
                             localStorage.setItem(this.name, this.value);
                        });
                    }
                    // If value has been set, restore
                    this.value = localStorage.getItem(this.name) || '';
                })
                // Clear local storage when clearing form
                jQuery('input[type=reset]').click(function() {
                    localStorage.clear();
                });
             });
            
            function selectHasOption (selectName, option) {
                var options = jQuery('select[name='+ selectName +'] option').map(function() { 
                    return this.value; 
                }).get();
                return options.includes(option);
            }
            
            function setOptions(group) {
                jQuery('div#options-' + group).find('div').each(function() {        
                    var selectName = 'search-' + this.id;
                    var select = jQuery('select[name='+ selectName +']');
                    select.html(jQuery(this).html());
                    var previousOption = localStorage.getItem(selectName);
                    // Select value if option exists; otherwise clear local storage for this select
                    if (previousOption && selectHasOption(selectName, previousOption)) {
                        select.val(previousOption);
                    } else {
                        localStorage.setItem(selectName, '');
                    }
                });
            }
            </script>";

        $title = _('Advanced Search');
        $output = $this->hiddenGroupProperties() . "<h1>$title</h1>
            <form class='species-completion' method='post'>
            <div id='main-search-box'>
            <input autofocus type='text' name='search-term' class='species-input' placeholder='" . htmlspecialchars(
                _('Search recording, e.g., by typing a species name…'),
                ENT_QUOTES
            ) . "' />
            </div>
            <p> " . _(
                "Searches are case-insensitive. Wildcards such as '*' are not necessary. Where possible, we will search for your query term as a substring of the full field."
            ) . ' ' . sprintf(
                _(
                    "Occasionally, a wildcard search does not deliver the best results. You can prepend an '=' to most search terms to force an exact match, or (in case of sound quality) an '&lt;' or '&gt;' to search for less or greater than."
                ),
                '<tt>=male</tt>'
            ) . '
            </p>
            <p>' . sprintf(
                _(
                    'Not all search tags are available in this form. See %s for more additional search options and instructions.'
                ),
                '<a href="' . getUrl('tips') . '">' . _('Search tips') . '</a>'
            ) . " " . _('Limit results by matching the following fields:') . "
            </p>
            
            <table id='advanced-search'>
            <tr>
                <td>" . _('Group') . "</td>
                <td> " . $this->groupSelect() . "</td>
            </tr>
            <tr>
                <td>" . _('Genus') . "</td>
                <td><input type='text' name='search-genus' /></td>
            </tr>
             <tr>
                <td> " . _('Subspecies') . " </td>
                <td><input type='text' name='search-ssp' /></td>
            </tr>
             
            <tr><th colspan='2'></th></tr>
           
            <tr> 
                <td>" . _('Country') . " </td>
                <td>" . HtmlUtil::countrySelect('search-country') . "</td>
            </tr>
             <tr>
                <td>" . _('Location') . "</td>
                <td><input type='text' name='search-location' /></td>
            </tr>
            <tr>
                <td>" . _('License') . "</td>
                <td>" . $this->licenseSelect() . " </td>
            </tr>   
            <tr>
                <td>" . _('Recordist name') . "</td>
                <td><input type='text' name='search-recordist' /></td>
            </tr>
            <tr>
                <td>" . _('Automatic') . " </td>
                <td>" . $this->yesNoSelect('search-auto') . "</td>
            </tr> 
            
            <tr><th colspan='2'></th></tr>
            
            <tr>
                <td>" . _('Predefined sound type') . "</td>
                <td><select name='search-type'></select></td>
            </tr> 
            <tr>
                <td>" . _('Other sound type') . "</td>
                <td><input type='text' name='search-type-other' placeholder='" . _(
                'Any type not included under predefined options'
            ) . "'/></td>
            </tr> 
           
            <tr><th colspan='2'></th></tr>
            
            <tr>
                <td>" . _('Sex') . "</td>
                <td><select name='search-sex'></select></td> 
            </tr> 
            <tr>
                <td>" . _('Life stage') . "</td>
                <td><select name='search-stage'></select></td>
            </tr> 
            <tr>
                <td>" . _('Animal seen') . "</td>
                <td>" . $this->yesNoSelect('search-seen') . "</td>
            </tr> 
            <tr>
                <td>" . _('Playback used') . "</td>
                <td>" . $this->yesNoSelect('search-playback') . "</td>
            </tr> 
            <tr>
                <td>" . _('Recording method') . "</td>
                <td><select name='search-method'></select></td>
            </tr>
            <tr>
                <td>" . _('Background species') . "</td>
                <td><input type='text' class='species-input' name='search-bg' placeholder='" . _(
                'Start typing a species name...'
            ) . "'/></td>
            </tr>
            <tr>
                <td>" . _('Remarks') . "</td>
                <td><input type='text' name='search-remarks' /></td>
            </tr>
            <tr>
                <td>" . _('Sound quality') . "</td>
                <td><input type='text' name='search-quality' placeholder='" . _(
                'Letters A to E. Prepend &gt; or &lt; for better or worse than, e.g. &gt;C for A and B.'
            ) . "' /></td>
            </tr> 
            
            <tr><th colspan='2'></th></tr>
            
            <tr>
                <td></td>
                <td>
                    <input type='submit' name='submit' value='" . _('Search') . "'/>
                    <input type='reset' value='" . _('Clear') . "'/>
                </td>
            </tr>
            </table>
            </form>
            
            <div>
            </div>
            
            $extrajs
            ";

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'search']);
    }

    private function hiddenGroupProperties()
    {
        $output = "<div style='display: none;'>";
        foreach ($this->groupProperties() as $group => $tagProperties) {
            $output .= "<div id='options-$group' >";
            foreach ($tagProperties as $tag => $properties) {
                $options = "<option value=''>" . _("Please choose...") . "</option>";
                foreach ($properties as $value) {
                    $options .= "<option value='$value'>" . ucfirst(_($value)) . "</option>";
                }
                $output .= "<div id='$tag'>" . $options . "</div>";
            }
            $output .= "</div>";
        }
        $output .= "<div id='options-'>";
        foreach ($this->allProperties() as $tag => $properties) {
            $options = "<option value=''>" . _("Please choose...") . "</option>";
            foreach ($properties as $value) {
                $options .= "<option value='$value'>" . ucfirst(_($value)) . "</option>";
            }
            $output .= "<div id='$tag'>" . $options . "</div>";
        }
        return $output . "</div></div>";
    }

    private function groupProperties()
    {
        if (!$this->groupProperties) {
            $res = query_db(
                "select t4.name, t3.tag, t2.property 
                from group_sound_properties as t1
                left join sound_properties as t2 on t1.property_id = t2.id
                left join sound_property_categories as t3 on t2.category_id = t3.id
                left join groups as t4 on t1.group_id = t4.id
                where t2.property not in ('uncertain', 'unknown')
                order by t3.sort_order, t2.property"
            );
            while ($row = $res->fetch_object()) {
                $this->groupProperties[$row->name][$row->tag][] = $row->property;
            }
        }
        return $this->groupProperties;
    }

    private function allProperties()
    {
        if (!$this->properties) {
            $res = query_db(
                "select distinct t3.tag, t2.property 
                from group_sound_properties as t1
                left join sound_properties as t2 on t1.property_id = t2.id
                left join sound_property_categories as t3 on t2.category_id = t3.id
                where t2.property not in ('uncertain', 'unknown')
                order by t3.sort_order, t2.property"
            );
            while ($row = $res->fetch_object()) {
                $this->properties[$row->tag][] = $row->property;
            }
        }
        return $this->properties;
    }

    private function groupSelect()
    {
        $options = "
            <option value=''>" . _("Please choose...") . "</option>";
        $res = query_db('SELECT name FROM groups');
        while ($row = $res->fetch_object()) {
            $options .= "<option value='$row->name'>" . ucfirst(_($row->name)) . "</option>";
        }
        return "<select name='search-group'>" . $options . "</select>";
    }

    private function licenseSelect()
    {
        $options = "<option value=''>" . _("Please choose...") . "</option>";
        $licenses = License::uploadOptions();

        for ($i = 0; $i < count($licenses); $i++) {
            $lic = License::lookupById($licenses[$i]);
            $options .= "<option value='" . implode('-', $lic->attrs) . "'}>$lic->name</option>";
        }

        return "<select name='search-license'>" . $options . "</select>";
    }

    private function yesNoSelect($name)
    {
        return "
            <select name='$name'>
            <option value=''>" . _("Please choose...") . "</option>
            <option value='yes'>" . _("Yes") . "</option>
            <option value='no'>" . _("No") . "</option>
            </select>";
    }
}
