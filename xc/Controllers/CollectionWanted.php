<?php

namespace xc\Controllers;

use xc\Species;
use xc\XCRedis;

use function xc\escape;
use function xc\getUrl;

class CollectionWanted extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
        $this->redis->setKeyByRequest($this->redisKey('stats-wanted-species'), $request);
    }

    /** @noinspection SyntaxError
     * @noinspection SyntaxError
     */
    public function handleRequest()
    {
        $title = _('Wanted Species');
        $output = $this->redis->get();

        if (!$output) {
            $resultsTable = '';

            $cnt = $this->request->query->get('cnt');

            $selectForm = "
            <form>
            <select onchange=\"window.open(this.options[this.selectedIndex].value,'_top')\">
            <option value=''>" . _('Choose a country') . '</option>
            ';

            $sql = 'select country, country_code from world_country_list INNER JOIN checklists_country USING(country_code) GROUP BY country_code ORDER BY country';
            $rescnts = query_db($sql);
            while ($rowcnts = $rescnts->fetch_object()) {
                if ($cnt == $rowcnts->country_code) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }
                $selectForm .= "<option $selected value='" . getUrl(
                        'missing',
                        ['cnt' => $rowcnts->country_code]
                    ) . "'>$rowcnts->country</option>
                ";
            }

            $selectForm .= '
            </select>
            </form>';
            if ($cnt) {
                $cnt = escape($cnt);
                $rescnt = query_db(
                    "SELECT country from world_country_list where country_code='$cnt'"
                );
                $row = $rescnt->fetch_object();
                $countryName = $row->country;

                // find total number of species in this checklist
                $restotal = query_db(
                    "SELECT COUNT(species_nr) as N FROM checklists_country WHERE country_code='$cnt'"
                );
                $rowtotal = $restotal->fetch_object();
                $nrin = $rowtotal->N;

                $sqlnotin = "SELECT C.species_nr, T.genus, T.species, T.eng_name, T.extinct FROM checklists_country C INNER JOIN taxonomy T USING(species_nr) WHERE country_code='$cnt' and C.species_nr NOT IN (SELECT species_nr FROM taxonomy WHERE recordings) ORDER BY T.IOC_order_nr";
                $resnotin = query_db($sqlnotin);
                $nrnotin = $resnotin->num_rows;

                $sqlnotrecordedin = "SELECT C.species_nr, T.genus, T.species, T.eng_name, T.extinct FROM checklists_country C INNER JOIN taxonomy T USING(species_nr) WHERE country_code='$cnt' and C.species_nr NOT IN (SELECT species_nr FROM taxonomy INNER JOIN birdsounds USING(species_nr) INNER JOIN world_country_list USING(country) WHERE taxonomy.recordings AND country_code='$cnt') AND C.species_nr IN (SELECT species_nr from taxonomy WHERE recordings) ORDER BY T.IOC_order_nr";
                $resnotrecordedin = query_db($sqlnotrecordedin);
                $nrnotrecordedin = $resnotrecordedin->num_rows;

                $perc = round($nrnotin / $nrin * 100) . '%';
                $tablenotin = "
                <table class='results'>
                <thead>
                <th>" . _('Common name') . '</th>
                <th>' . _('Scientific name') . '</td>
                </thead>
                <tbody>';
                while ($row = $resnotin->fetch_object()) {
                    $sp = new Species(
                        $row->genus,
                        $row->species,
                        $row->eng_name,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        $row->extinct
                    );
                    $url = $sp->profileURL();
                    $class = '';
                    if ($sp->isExtinct()) {
                        $class = "class='extinct'";
                    }
                    $tablenotin .= "
                    <tr $class><td><a href='$url'>{$sp->commonName()}</a></td>
                    <td>{$sp->scientificName()}</td>
                    </tr>
                    ";
                }
                $tablenotin .= '
                </tbody>
                </table>';

                $tablenotrecordedin = "
                <table class='results'>
                <thead>
                <th>" . _('Common name') . '</th>
                <th>' . _('Scientific name') . '</th>
                </thead>
                <tbody>';
                while ($row = $resnotrecordedin->fetch_object()) {
                    $sp = new Species(
                        $row->genus,
                        $row->species,
                        $row->eng_name,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        $row->extinct
                    );
                    $class = '';
                    if ($sp->isExtinct()) {
                        $class = "class='extinct'";
                    }
                    $url = $sp->profileURL();
                    $tablenotrecordedin .= "
                    <tr $class>
                    <td><a href='$url'>{$sp->commonName()}</a></td>
                    <td>{$sp->scientificName()}</td>
                    </tr>
                    ";
                }

                $tablenotrecordedin .= '
                </tbody>
                </table>';

                $resultsTable = '

                <p>' . _(
                        "Base checklists courtesy of <a href='http://www.printablebirdchecklists.com'>www.printablebirdchecklists.com</a> as of March 2013."
                    ) . '
                </p>
                <p>' . sprintf(
                        _(
                            '%s total species in the checklist for %s.  Note that this list may contain rare or vagrant species.'
                        ),
                        "<b>$nrin</b>",
                        "<b>$countryName</b>"
                    ) . "
                    </p>
                    <table id='main-table'>
                    <tr>
                    <td>
                    <p>" .
                    sprintf(
                        _(
                            'The following %s species (%s of the total) from %s are not represented in xeno-canto.'
                        ),
                        "<b>$nrnotin</b>",
                        "$perc",
                        "<b>$countryName</b>"
                    ) . "
                        </p>
                        $tablenotin
                        </td>
                        <td>
                        <p>" .
                    sprintf(
                        _(
                            'The following additional %s species have been recorded in other countries, but have not been recorded in %s.'
                        ),
                        "<b>$nrnotrecordedin</b>",
                        "<b>$countryName</b>"
                    ) . "
                            </p>
                            $tablenotrecordedin
                            </td>
                            </tr>
                            <td>
                            </td>
                            </tr>
                            </table>
                            ";
            } else {
                $resultsTable = '<p>' . _(
                        'Select a country above to see a list of species from that country that are still missing from xeno-canto.'
                    ) .
                    '</p>';
            }

            $output = "
            <ul class='breadcrumbs'>
            <li><a href='" . getUrl('collection-details') . "'>" . _(
                    'Collection Details'
                ) . "</a></li>
            <li>$title</li>
            <li class='current'>$selectForm</li>
            </ul>";
            $output .= $resultsTable;

            $this->redis->set($output);
        }

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'wanted']
        );
    }
}
