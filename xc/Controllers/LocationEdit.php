<?php

namespace xc\Controllers;

use xc\LocationMap;
use xc\User;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;

class LocationEdit extends LoggedInController
{

    public function handlePost()
    {
        $loc = $this->request->query->get('loc');
        $cnt = $this->request->query->get('cnt');

        if (!($loc && $cnt)) {
            return $this->badRequest();
        }

        $userid   = User::current()->userId();
        $coordStr = $this->request->request->get('new-coord-val');
        if ($coordStr === 'use-map') {
            $coordStr = $this->request->request->get('map-coords');
        }

        $newname = escape($this->request->request->get('newname'));
        $ecnt    = escape($cnt);
        $eloc    = escape($loc);

        $coords = [];
        if ($coordStr) {
            $coords = explode(',', $coordStr);
        }

        $res = null;
        if (count($coords) == 2) {
            $lat = round(floatval($coords[0]), 4);
            $lng = round(floatval($coords[1]), 4);

            $res = query_db(
                "UPDATE birdsounds SET location='$newname', latitude=$lat, longitude=$lng where dir='$userid' AND location='$eloc' AND country='$ecnt'"
            );
        } elseif ($newname !== $loc) {
            $res = query_db(
                "UPDATE birdsounds SET location='$newname' where dir='$userid' AND location='$eloc' AND country='$ecnt'"
            );
        }

        if (!$res) {
            notifyError(_('Unable to Update Location'));
            return $this->seeOther(
                getUrl('location-edit', ['loc' => $loc, 'cnt' => $cnt])
            );
        } else {
            notifySuccess(_('Updated Location'));
            return $this->seeOther(
                getUrl(
                    'location-edit',
                    [
                        'loc' => $this->request->request->get('newname'),
                        'cnt' => $cnt,
                    ]
                )
            );
        }
    }

    public function handleRequest()
    {
        $loc = $this->request->query->get('loc');
        $cnt = $this->request->query->get('cnt');

        if (!($loc && $cnt)) {
            return $this->badRequest();
        }

        $title  = _('Edit Location Coordinates');
        $userid = User::current()->userId();
        $script = '';

        if (empty($loc)) {
            notifyError(_('No location specified'));
        } else {
            $escapedLoc = escape($loc);
            $ecnt       = escape($cnt);
            $res        = query_db(
                "SELECT COUNT(*) as nrecs from birdsounds WHERE dir='$userid' AND location='$escapedLoc' AND country='$ecnt'"
            );
            $row        = $res->fetch_object();
            $nrecs      = $row->nrecs;
            if (!$nrecs) {
                return $this->notFound();
            }

            $body = "<h1>$title</h1>
                <p><a href='" . getUrl(
                    'mypage',
                    ['p' => 'locations']
                ) . "'>&laquo; " . htmlspecialchars(
                        _(
                            'All Locations'
                        )
                    ) . "</a></p>
                <form method='post' >
                <p>
                <label>" . _(
                        'Location Name'
                    ) . ":</label> <input type='text' name='newname' value='" . htmlspecialchars(
                        $loc,
                        ENT_QUOTES
                    ) . "'/>
                </p>
                ";
            $res  = query_db(
                "SELECT location, latitude, longitude, COUNT(*) as nrecs from birdsounds WHERE dir='$userid' AND location='$escapedLoc' AND country='$ecnt' GROUP BY latitude, longitude"
            );
            $map  = new LocationMap('map-canvas', $res, false);

            $body .= '
                <p>' . _(
                    'Assign all recordings at this location to the following coordinates:'
                ) . "</p>
                <ul class='simple'>
                ";

            $i = 0;
            while ($row = $res->fetch_object()) {
                if (is_null($row->latitude) || is_null($row->longitude)) {
                    continue;
                }
                $i++;
                $body .= "<li><input type='radio' name='new-coord-val' id='coord-$i' value='{$row->latitude},{$row->longitude}'/> <label for='coord-$i'>{$row->latitude}, {$row->longitude} (" . sprintf(
                        _(
                            '%s recordings'
                        ),
                        $row->nrecs
                    ) . ')</label></li>';
            }
            $body .= "
                <li><input type='radio' name='new-coord-val' value='use-map' id='coord-use-map'/> <label for='coord-use-map'>" . _(
                    'Specify new coordinates by clicking on the map below or typing them here:'
                ) . "
                <input type='text' name='map-coords' id='input-map-coords' placeholder='" . _(
                         'latitude,longitude'
                     ) . "'/></label>
                </li>
                </ul>

                <div id='map-canvas'></div>
                <p><input type='submit' name='update-coords' value='" . htmlspecialchars(
                         _(
                             'Update Location'
                         ),
                         ENT_QUOTES
                     ) . "'/></p>
                ";

            $script = <<<EOT
{$map->getJS(false)}
<script type='text/javascript'>
var locMarker = null;
var markerIcon = new google.maps.MarkerImage('/static/img/markers/a-14.png', new google.maps.Size(14,14), null, new google.maps.Point(7,7));
var xcmap = null;

function locationMapClicked(event)
{
    var pos = event.latLng;
    if (locMarker)
    {
        locMarker.setMap(null);
        locMarker = null;
    }

    var options = {
        icon: markerIcon,
        zIndex: 50,
        map: this,
        position: pos
    };
    locMarker = new google.maps.Marker(options);

    jQuery("#input-map-coords").val(pos.lat().toFixed(4) + "," + pos.lng().toFixed(4));
    jQuery("#coord-use-map").attr("checked", "checked");
}

jQuery(document).ready(function() {
    xcmap = xc.initMap();

    google.maps.event.addListener(xcmap, "click", locationMapClicked);

    jQuery("#input-map-coords").change(function() {
        var strCoords = jQuery(this).val();
        var coords = strCoords.split(',');
        if (coords.length == 2)
        {
            // synthesize a 'click' event
            var event = { latLng: new google.maps.LatLng(coords[0], coords[1]) };
            locationMapClicked.call(xcmap, event);
            xcmap.panTo(event.latLng);
        }
    });
});
</script>

EOT;
        }
        $body .= $script;

        return $this->template->render(
            $body,
            ['title' => $title, 'bodyId' => 'location-edit']
        );
    }

}
