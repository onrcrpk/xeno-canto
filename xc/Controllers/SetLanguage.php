<?php

namespace xc\Controllers;

use xc\Language;

class SetLanguage extends Controller
{
    public function set($code)
    {
        $language = Language::lookup($code);
        if ($language) {
            app()->setLanguage($language);
            app()->session()->set('language', $language);
            app()->setCookie('language', $language->code);
        }

        return $this->seeOther($this->request->headers->get('Referer', '/'));
    }
}
