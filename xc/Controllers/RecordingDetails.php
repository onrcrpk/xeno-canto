<?php

namespace xc\Controllers;

use xc\DeletedRecording;
use xc\HtmlUtil;
use xc\License;
use xc\MarkerMap;
use xc\Recording;
use xc\SonoSize;
use xc\Species;
use xc\ThreadType;
use xc\User;

use function xc\getUrl;
use function xc\printErrorMessages;
use function xc\sanitize;
use function xc\XC_formatDuration;
use function xc\XC_ratingWidget;

class RecordingDetails extends Controller
{

    public function redirect()
    {
        $id = $this->request->query->get('XC', $this->request->query->get('nr'));
        return $this->movedPermanently(getUrl('recording', ['xcid' => $id]));
    }

    public function show($xcid)
    {
        $output = '';

        $rec = Recording::load($xcid);
        $user = User::current();

        $idAsterisk = $idnote = $forumtopics = $idtopics = '';
        if ($rec) {
            $mysteryForumTopic = null;

            // Get a list of related forum topics and check whether this recording has
            // had its ID questioned
            $res = query_db("SELECT topic_nr,subject, type FROM forum_world WHERE snd_nr = $xcid");
            if ($res && $res->num_rows > 0) {
                $topics = '';
                while ($row = $res->fetch_object()) {
                    $subj = sanitize($row->subject);
                    if ($row->type == ThreadType::ID_QUESTIONED) {
                        $idtopics .= "<li><a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]
                            ) . "'>$subj</a></li>";
                    } elseif ($row->type == ThreadType::MYSTERY) {
                        $mysteryForumTopic = $row->topic_nr;
                    }
                    $topics .= "<p>{$row->topic_nr}. <a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]
                        ) . "'>$subj</a></p>";
                }
                $forumtopics .= '
                    <h2>' . _('Related Forum Topics') . '</h2>
                    <p>' . _(
                        'The following forum topics may have additional information or discussions about this recording:'
                    ) . "</p>
                    $topics";
            }

            // sometimes the recording itself has had the 'id questioned' flag
            // cleared even when there are still 'id questioned' topics open about
            // it.  Someday the db schema should be fixed to prevent this sort of
            // thing, but for now, just only display a notice if they agree
            if (($rec->status() == ThreadType::ID_QUESTIONED) && $idtopics) {
                $idnote .= "
                    <section class='important'>
                    <h2><a name='idnote'>*</a> " . _('ID under discussion') . '</h2>
                    <p>
                    ' . _(
                        'The identification of this recording is currently under discussion. See the following forum topics for more information:'
                    ) . "
                    </p>
                    <ul>$idtopics</ul>
                    </section>";

                $idAsterisk = "<a href='#idnote'>*</a>";
            } elseif (($rec->status() === ThreadType::MYSTERY) && $mysteryForumTopic) {
                $idnote .= "
                    <section class='important'>
                    <h2><a name='idnote'>*</a> " . _('ID under discussion') . '</h2>
                    <p>
                    ' . sprintf(
                        _(
                            "This recording was uploaded as a mystery recording. If you can help identify the species in this recording, please contribute to the <a href='%s'>discussion on the forum</a>."
                        ),
                        getUrl('discuss_forum', ['topic_nr' => $mysteryForumTopic])
                    ) . '
                    </p>
                    </section>';

                $idAsterisk = "<a href='#idnote'>*</a>";
            } elseif (($rec->status() === ThreadType::ID_UNCONFIRMED)) {
                $confirmForm = '';
                if ($user && $user->canConfirm() && ($rec->recordistID() != $user->userId())) {
                    $confirmForm = "<form action='" . getUrl('recording-confirm', ['xcid' => $rec->xcid()]) . "' method='post'>
                        <input type='submit' value='" . _('Confirm') . "'/>
                        </form>";
                }
                $idnote .= "
                    <section class='important'>
                    <h2><a name='idnote'>*</a> " . _('ID Confirmation Required') . '</h2>
                    <p>' . _('This ID of this recording has not yet been independently confirmed') . "</p>
                    $confirmForm
                    </section>";

                $idAsterisk = "<a href='#idnote'>*</a>";
            }

            $title = "XC$xcid {$rec->commonName()}";
            if (!$rec->isPseudoSpecies()) {
                $title .= " ({$rec->scientificName()})";
            }

            // XC-318: change to "<a href='{$rec->subspeciesURL()}'>
            $recordingTitle = "<a href='{$rec->speciesURL()}'>";
            if ($rec->commonName()) {
                $recordingTitle .= "{$rec->commonName()}</a> &middot; <span class='sci-name'>{$rec->fullScientificName()}</span>";
            } else {
                $recordingTitle .= "<span class='sci-name'>{$rec->fullScientificName()}</span></a>";
            }
            // FIXME: ENVREC
            if ($rec->isPseudoSpecies()) {
                $recordingTitle = $rec->commonName();
                if ($rec->isMystery()) {
                    $recordingTitle .= " ({$rec->groupNameSingular()})";
                }
            }
            $recordingTitle .= $idAsterisk;
            $remarks = $rec->formatRemarks();
            if (!$remarks) {
                /// translators: as in: "no data was specified"
                $remarks = "<p class='unspecified'>" . _('None given') . '</p>';
            }

            $restricted = '';
            if ($this->isRestricted($rec)) {
                $message = _('This recording is currently restricted');
                if ($this->hasRestrictedBackgroundSpecies($rec)) {
                    $message = _('This recording contains a restricted background species');
                }

                $restricted = "
                    <p class='important'>" . $message . ". <a href='" . getUrl('FAQ') . "#restricted'>" . _(
                        'Explain this.'
                    ) . '</a>
                    </p>';
            }

            $output .= "
                <div itemscope itemtype='//schema.org/AudioObject'>
                <header>
                <h1 itemprop='name'>XC{$rec->xcid()} &middot; $recordingTitle</h1>";

            if (!$this->isRestricted($rec)) {
                $duration = XC_formatDuration($rec->length(), true);
                $output .= "
                    <meta itemprop='encodingFormat' content='{$rec->fileType()}' />
                    <meta itemprop='contentURL' content='{$rec->fileUrl()}' />
                    <meta itemprop='duration' content='$duration' />
            ";
            }

            $mainPlayer = "<div id='main-player'>" . $rec->player([
                    'highResSono' => true,
                    'showAxis' => true,
                ]) . "</div>";
            $slowPlayer = $mp3Warning = '';

            if ($rec->isLossless() && $rec->mp3StillScheduled()) {
                $mp3Warning .= '<p class="important">' . '<strong>' . _('Warning') . '! </strong>' . _(
                        'An mp3 derivative for this recording has not been created yet. Your browser will attempt to play the original wav file. This may not be possible for the highest resolution files. The mp3 should be available within five minutes.'
                    ) . '</p>';
            }

            if ($rec->isHighRes()) {
                $mp3Warning .= '<p class="important mp3-warning">' . sprintf(
                        _(
                            "This is a high-frequency recording. A high quality mp3 derivative is used for playback, which drops frequencies over ~20 kHz. Please %s for a full resolution file."
                        ),
                        '<a href="' . $rec->downloadURL() . '">' . _("download the original file") . '</a>'
                    ) . '</p>';
                if ($rec->slowFactor() && file_exists($rec->mp3Path($rec->slowFactor()))) {
                    $slowPlayer = $rec->player([
                        'slowPlayer' => true,
                        'showSpecies' => false,
                        'showRecordist' => false,
                        'showLocation' => false
                    ]);
                    $slowPlayer = "<div id='slow-player'>$slowPlayer</div>";
                }
            }

            $output .= "
                <meta itemprop='thumbnailUrl' content='{$rec->sonoURL()}' />
                </header>
                
                <section id='player' class='column'>
                $mp3Warning
                <div id='detail-player'>
                $mainPlayer 
                $slowPlayer
                </div>
                $idnote
                $restricted
                
                <h2>" . _('Remarks from the Recordist') . "</h2>
                $remarks";

            if (!$this->isRestricted($rec)) {
                $output .= $this->locationMap($rec);
            }

            $output .= $this->rating($rec) . " $forumtopics" . $this->citation($rec) . $this->license($rec) . "
                </section>

                <section id='recording-data' class='column'>" . $this->basicData($rec);
            $output .= $this->soundDetails($rec) . $this->fileProperties($rec) . $this->actions($rec);


            $output .= '
                </section>
                </div>';

            if (!$this->isRestricted($rec)) {
                $ogdata = "
                    <meta property='og:title' content='" . sanitize($title) . "' />
                    <meta property='og:url' content='" . sanitize($rec->URL(true)) . "' />
                    <meta property='og:type' content='music.song' />
                    <meta property='og:description' content='" . sanitize($rec->remarks()) . "' />
                    <meta property='og:image' content='" . sanitize($rec->sonoURL(SonoSize::MEDIUM)) . "' />
                    <meta property='og:image' content='" . sanitize($rec->sonoURL(SonoSize::LARGE)) . "' />
                    <meta property='og:audio' content='" . sanitize($rec->downloadURL(true)) . "' />
                    <meta property='music:duration' content='$duration' />
                    <meta property='og:audio:type' content='" . $rec->mimeType() . "' />";
                $this->template->addHeaderData($ogdata);
            }
        } else {
            $title = _('No recording found');
            // check if this recording did exist but was deleted
            $rec = DeletedRecording::load($xcid);
            if ($rec) {
                $output .= "<p class='important'>" . sprintf(
                        _('Recording %s has been removed from the collection.'),
                        "<strong>XC$xcid</strong>"
                    ) . '</p>';
                if (User::current() && User::current()->isAdmin()) {
                    $deleter = User::load($rec->deleteUser());
                    $output .= "<div style='margin: 25px 0;'><p><img class='icon' src='/static/img/admin-16.png'/>The recording was deleted by <a href='{$deleter->getProfileURL()}'>{$deleter->userName()}</a> on {$rec->deleteDate()}. ";
                    if ($rec->deleteReason()) {
                        $output .= "The following reason was given for deletion: <strong>{$rec->deleteReason()}</strong>";
                    } else {
                        $output .= 'No reason was given for the deletion.';
                    }
                    $output .= "</p>
                        <form action='" . getUrl('restore', ['snd_nr' => $rec->xcid()]) . "'>
                        <input type='submit' value='Restore recording'>
                        </form>
                        </div>";
                }
                $output .= '<p>' . _('Some basic information about this recording is show below:') . "</p>
                    <table class='key-value'>
                    <tr>
                    <td>" . _('Common name') . "</td>
                    <td>{$rec->commonName()}</td>
                    </tr>";

                if (!$rec->isPseudoSpecies()) {
                    $output .= '<tr>
                        <td>' . _('Scientific name') . "</td>
                        <td><span class='sci-name'>{$rec->scientificName()}</span></td>
                        </tr>";
                }

                $output .= '
                    <tr>
                    <td>' . _('Recordist') . "</td>
                    <td>{$rec->recordist()}</td>
                    </tr>
                    <tr>
                    <td>" . _('Sound Type') . "</td>
                    <td>{$rec->soundType()}</td>
                    </tr>
                    <tr>
                    <td>" . _('Date') . "</td>
                    <td>{$rec->date()}</td>
                    </tr>
                    <tr>
                    <td>" . _('Location') . "</td>
                    <td>{$rec->location()}, {$rec->country()}</td>
                    </td>
                    </tr>
                    </table>
                    </div>";
            } else {
                $output .= printErrorMessages(_('No recording found'), [
                    sprintf(_('Catalogue number %s does not exist in the database'), "<strong>XC$xcid</strong>"),
                ]);
            }
        }
        return $this->template->render($output, ['title' => $title, 'bodyId' => 'recording-details']);
    }

    protected function isRestricted($recording)
    {
        $user = User::current();
        if ($user && (($user->userId() == $recording->recordistID()) || $user->canDownloadRestrictedRecordings(
                ) || $user->isAdmin())) {
            return false;
        }
        if ($recording->species()->restricted() || $this->hasRestrictedBackgroundSpecies($recording)) {
            return true;
        }
        return false;
    }

    protected function hasRestrictedBackgroundSpecies($recording)
    {
        return false;

        // XC-94 reverted
        foreach ($recording->backgroundSpecies() as $sp) {
            if ($sp->restricted) {
                return true;
            }
        }
        // Try extra for names
        if (!empty($recording->backgroundExtra())) {
            foreach ($recording->backgroundExtra() as $str) {
                $parts = explode(' ', $str);
                $sp = new Species($parts[0], $parts[1], null, null);
                if ($sp->restricted) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function locationMap($recording)
    {
        $loc = '<h2>' . _('Location') . '</h2>
            ';
        if ($recording->hasCoordinates()) {
            $loc .= "
                <div id='map-canvas'></div>
                ";
            $map = new MarkerMap(
                'map-canvas', [
                [
                    'latitude' => $recording->latitude(),
                    'longitude' => $recording->longitude(),
                    'title' => $recording->location(),
                    'id' => $recording->xcid(),
                ],
            ], $recording->species()->rangeMapURL()
            );
            $loc .= $map->getJS();
        } else {
            $loc .= '<p>' . _('No coordinates given') . '</p>';
        }

        return $loc;
    }

    protected function rating($recording)
    {
        $rating = '
            <h2>' . _('Rating') . "</h2>
            <p class='rating'>" . _('Rate the quality of this recording (A is best, E worst)') . ':</p>';
        $rating .= XC_ratingWidget($recording->xcid(), $recording->rating());
        return $rating;
    }

    protected function citation($recording)
    {
        $citation = '<h2>' . _('Citation') . '</h2>';
        /// translators: as in "recordist, XC3456. Accessible at http://url"
        $citation .= '<p>' . sprintf(
                _('%s, %s. Accessible at %s.'),
                $recording->recordist(),
                "XC{$recording->xcid()}",
                "www.xeno-canto.org/{$recording->xcid()}"
            ) . '</p>';
        return $citation;
    }

    protected function license($recording)
    {
        $license = License::lookupById($recording->license());
        return '<h2>' . _('License') . "</h2>
            <p><a href='{$license->url}'><img src='/static/img/cc.png' class='icon'> {$license->name}</a> </p>
            ";
    }

    protected function basicData($recording)
    {
        if (!$recording->hasCoordinates()) {
            $lat = $this->notSpecified();
            $lon = $lat;
        } else {
            $lat = $recording->latitude();
            $lon = $recording->longitude();
        }

        $uploaded = $recording->uploadDateString();
        $modified = $recording->lastModifiedDateString();

        $data = '
            <h2>' . _('Basic data') . "</h2>
            <table class='key-value'>
            <tbody>
            <tr><td>" . _('Recordist') . "</td><td>
            <div itemprop='author' itemscope itemtype='//schema.org/Person'>
            <a itemprop='url' href='" . getUrl('recordist', ['id' => $recording->recordistID()]) . "'><span itemprop='name'>{$recording->recordist()}</span></a>
            </div>
            </td></tr>
            <tr><td>" . _('Date') . "</td><td>{$recording->date()}</td></tr>
            <tr><td>" . _('Time') . "</td><td>{$recording->time()}</td></tr>";

        if (!$recording->species()->restricted()) {
            $data .= '
                <tr><td>' . _('Latitude') . "</td><td>$lat</td></tr>
                <tr><td>" . _('Longitude') . "</td><td>$lon</td></tr>
                <tr><td>" . _('Location') . "</td>
                <td> <a href='{$recording->locationURL()}'>{$recording->location()}</a></td>
                </tr>";
        }

        $data .= '
            <tr><td>' . _('Country') . "</td><td>{$recording->country()}</td></tr>
            <tr><td>" . _('Elevation') . "</td><td>{$recording->elevation()} m</td></tr>
            <tr><td>" . _('Uploaded') . "</td><td>$uploaded</td></tr>";
        if (!empty($modified) && $uploaded != $modified) {
            $data .= "<tr><td>" . _('Last modified') . "</td><td>$modified</td></tr>";
        }
        $data .= "</tbody>
            </table>";

        return $data;
    }


    protected function soundDetails($recording)
    {
        $bgspecies = $recording->backgroundSpecies();
        $extra = $recording->backgroundExtra();

        $bgtext = '';
        if ($bgspecies || $extra) {
            $bgtext .= '<ul>';
            for ($i = 0; $i < count($bgspecies); $i++) {
                $sp = $bgspecies[$i];
                $bgtext .= "<li>{$sp->htmlDisplayName()}</li>";
            }
            for ($i = 0; $i < count($extra); $i++) {
                $bgtext .= '<li>' . $extra[$i] . '</li>';
            }
            $bgtext .= '</ul>';
        } else {
            $bgtext .= _('none');
        }

        $output = '
            <h2>' . _('Sound details') . "</h2>
            <table class='key-value'>
            <tbody>";

        if ($recording->speciesNumber() == Species::soundscapeSpeciesNumber()) {
            $output .= "
                <tr>
                    <td>" . _('Background') . "</td>
                    <td valign='top'>$bgtext</td>
                </tr>";
        } else {
            $output .= "<tr><td>" . _('Type') . "</td><td></td></tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;" . _('predefined') . "</td>
                    <td>" . ($recording->soundTypePredefined() ?: $this->notSpecified()) . "</td>
                </tr>

                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;" . _('other') . "</td>
                    <td>" . ($recording->soundTypeExtra() ?: $this->notSpecified()) . "</td>
                </tr>
                <tr>
                    <td>" . _('Sex') . "</td>
                    <td>" . ($recording->sex() ?: $this->notSpecified()) . "</td>
                </tr>
                <tr>
                    <td>" . _('Life stage') . "</td>
                    <td>" . ($recording->lifeStage() ?: $this->notSpecified()) . "</td>
                </tr>
                <tr>
                    <td>" . _('Method') . "</td>
                    <td>" . ($recording->recordingMethod() ?: $this->notSpecified()) . "</td>
                </tr>
                <tr>
                    <td>" . _('Background') . "</td>
                    <td valign='top'>$bgtext</td>
                </tr>
                <tr>
                    <td>" . _('Animal seen?') . "</td>
                    <td>{$recording->animalSeen()}</td>
                </tr>
                <tr>
                    <td>" . _('Playback used?') . "</td>
                    <td>{$recording->playbackUsed()}</td>
                </tr>";
            if ($recording->temperature() != '') {
                $output .= "
                <tr>
                    <td>" . _('Temperature') . "</td>
                    <td>{$recording->temperature()} (&deg;C)</td>
                </tr>";
            }
            if ($recording->specimen() != '') {
                $output .= "
                <tr>
                    <td>" . _('Specimen') . "</td>
                    <td>{$recording->specimen()}</td>
                </tr>";
            }
            if ($recording->collectionDate()) {
                $output .= "
                <tr>
                    <td>" . _('Collection date') . "</td>
                    <td>{$recording->collectionDate()}</td>
                </tr>";
            }
        }
        $output .= "
                </tbody>
            </table>";
        return $output;
    }

    protected function fileProperties($rec)
    {
        $fftsquery = "
            SELECT `format`, length, bitrate, smp, channels 
            FROM audio_info where snd_nr= {$rec->xcid()}";
        $fftsres = query_db($fftsquery);
        $fftsrow = $fftsres->fetch_object();
        if (!$fftsrow) {
            return '';
        }
        $channelsText = '';
        if ($fftsrow->channels == 1) {
            $channelsText = '(' . _('mono') . ')';
        } elseif ($fftsrow->channels == 2) {
            $channelsText = '(' . _('stereo') . ')';
        }

        $props = '
            <h2>' . _('Technical details') . "</h2>
            <table class='key-value'>
            <tbody>
            <tr><td>" . _('File type') . "</td><td>$fftsrow->format</td></tr>
            <tr><td>" . _('Length') . '</td><td>' . round($fftsrow->length, 1) . ' (s)</td></tr>
            <tr><td>' . _('Sampling rate') . "</td><td>$fftsrow->smp (Hz)</td></tr>";
        if ($fftsrow->format == 'mp3') {
            $props .= '
            <tr><td>' . _('Bitrate of mp3') . "</td><td>$fftsrow->bitrate (bps)</td></tr>";
        }
        $props .= '
            <tr><td>' . _('Channels') . "</td><td>$fftsrow->channels $channelsText</td></tr>
            <tr><td>" . _('Device') . "</td><td>" . ($rec->device() ?: $this->notSpecified()) . "</td></tr>
            <tr><td>" . _('Microphone') . "</td><td>" . ($rec->microphone() ?: $this->notSpecified()) . "</td></tr>
            <tr><td>" . _('Automatic recording') . "</td><td>{$rec->automatic()}</td></tr>
            </tbody>
            </table>
            ";
        return $props;
    }

    protected function actions($recording)
    {
        $sono = '';
        $res = query_db("SELECT sono_full from sonograms WHERE snd_nr={$recording->xcid()}");
        $row = $res->fetch_object();
        $fn = htmlspecialchars("XC{$recording->xcid()}.png", ENT_QUOTES);
        if ($row && $row->sono_full) {
            $sono = "<li><a download='$fn' href='{$row->sono_full}'><img class='icon' src='/static/img/graph.png' /> " . _(
                    'Download full-length sonogram'
                ) . '</a></li>';
        }

        $download = '';
        if (!$this->isRestricted($recording)) {
            $download = "<a title='" . sprintf(
                    _('Download file %s'),
                    htmlspecialchars("'{$recording->suggestedFileName()}'", ENT_QUOTES)
                ) . "'";
            $download .= " href='{$recording->downloadURL()}'><img class='icon' src='/static/img/download-toolbar.png' /> " . _(
                    'Download audio file'
                ) . '</a>';
        }
        $html = "
            <h2>" . _('Actions') . "</h2>
            <ul class='simple'>
            $download
            $sono
            <li><a href='" . getUrl('recording-embed', ['XC' => $recording->xcid()]
            ) . "'><img class='icon' src='/static/img/share.png' /> " . _('Embed') . "</a></li>
            <li><a href='" . getUrl('new_thread', ['xcid' => $recording->xcid()]
            ) . "'><img class='icon' src='/static/img/discuss-toolbar.png' /> " . _('Discuss') . '</a></li>
            ';

        $user = User::current();

        if ($user && $user->isLoggedIn()) {
            if ($user->canModifyRecording($recording->xcid())) {
                $html .= "<li><a href='" . getUrl('revise', ['snd_nr' => $recording->xcid()]
                    ) . "'><img class='icon' src='/static/img/edit-toolbar.png' /> " . _('Edit') . '</a></li>';
                $html .= "<li><a href='" . getUrl('delete', ['snd_nr' => $recording->xcid()]
                    ) . "'><img class='icon' src='/static/img/delete-toolbar.png' /> " . _('Delete') . '</a></li>';
            }
            $html .= "
                <li><a href='" . getUrl('recording-add-to-set', ['xcid' => $recording->xcid()]
                ) . "'><img class='icon' src='/static/img/bookmark-add-toolbar.png' /> " . _(
                    'Add to Set'
                ) . "</a></li>
                <li><a href='" . getUrl('recording-history', ['xcid' => $recording->xcid()]
                ) . "'><img class='icon' src='/static/img/calendar.png' /> View revision history</a></li>";
        }

        if ($user && $user->isAdmin()) {
            $html .= "</ul><h2>" . _('Admin') . "</h2><ul class='simple'>";
            $html .= "
                <li><a href='" . getUrl('admin-replace-recording', ['xcid' => $recording->xcid()]) . "'><img class='icon' src='/static/img/admin-16.png' /> Replace recording file</a></li>
                
                <li><a href='" . getUrl('recording-ratings', ['xcid' => $recording->xcid()]) . "'><img class='icon' src='/static/img/admin-16.png' /> Recording ratings</a></li>
                
                <li>
                <form method='post' action='" . getUrl('admin-update-recording-status') . "' id='rec-status-form'>
                <img class='icon' src='/static/img/admin-16.png'/><input type='hidden' name='xcid' value='{$recording->xcid()}'/>
                " . HtmlUtil::threadTypeSelect(
                    'admin',
                    'status',
                    $recording->status()
                ) . "<input type='submit' value='Update'/></form></li>";
        }
        $html .= '
            </ul>';

        return $html;
    }
}
