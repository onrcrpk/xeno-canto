<?php

namespace xc\Controllers;

use xc\Query;
use xc\Recording;
use xc\SortDirection;
use xc\Species;
use xc\XCRedis;

use function PHP81_BC\strftime;
use function xc\escape;
use function xc\getUrl;
use function xc\XC_formatDuration;

class WorldArea extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public function handleRequest($branch)
    {
        if (!$branch) {
            return $this->showDashboard();
        } else {
            return $this->showArea($branch);
        }
    }

    protected function showDashboard()
    {
        $title = _('World Area Dashboards');
        $output = "<h1>$title</h1>
            <ul class='simple'>";

        foreach (\xc\WorldArea::all() as $area) {
            $output .= "<li><a href='" . getUrl(
                    'world-area',
                    ['area' => $area->branch]
                ) . "'>{$area->desc}</a></li>";
        }

        $output .= '</ul>';
        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'world-area']
        );
    }

    protected function showArea($branch)
    {
        $area = \xc\WorldArea::lookup($branch);
        if (!$area) {
            return $this->notFound();
        }

        $this->redis->setKey($this->redisKey('daily-stats-world-area', [$branch, $this->groupId()]));
        $title = $area->desc . ' :: ' . _('Collection Details');
        $output = $this->redis->get();

        if (!$output) {
            $output = "
            
            <ul class='breadcrumbs'>
            <li><a href='" . getUrl('collection-details') . "'>" . _('Collection Details') . "</a></li>
            <li><strong>{$area->desc}</strong></li>
            </ul>
            <div class='group-select'>" . $this->printGroupSelect() . "</div>
            <h1>{$area->desc}: " . _('Collection Details') . "</h1>
            
            <section class='column' id='sidebar'>
                <h2>" . _('Collection Statistics') . '</h2>' .
                $this->areaStats($area) . "
                <h2>" . _('New Species') . '</h2>
                <p>' . sprintf(_('Species recently added to the %s collection'), $area->desc) . ':</p>' .
                $this->areaNewSpecies($area) . '
                <h2>' . _('Recordists') . '</h2>' .
                $this->areaRecordists($area) . '
                <h2>' . _('Common Species') . '</h2>
                <p>' . sprintf(_('Species most-frequently recorded in the %s collection'), $area->desc) . ':</p>' .
                $this->areaCommonSpecies($area) . "
            </section>

            <section id='main' class='column'>" .
                $this->growthChart($area->branch) . '
                <h2>' . _('Recently Uploaded Recordings') . '</h2>' .
                $this->areaRecentRecordings($area) . '
                <h2>' . _('Recent Forum Posts') . '</h2>' .
                $this->areaRecentForumPosts($area) . '
                <h2>' . _('Country Statistics') . '</h2>' .
                $this->areaCountries($area) . ' 
            </section>';
            $this->redis->set($output);
        }

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'world-area']);
    }

    private function areaStats($area)
    {
        if ($this->groupId()) {
            $sql = "
                SELECT * FROM wa_collection_stats 
                WHERE branch = '{$area->branch}' AND group_id = " . $this->groupId();
        } else {
            $sql = "
                SELECT SUM(branch) AS branch, SUM(nrecordings) AS nrecordings,
                    SUM(nspecies) AS nspecies, SUM(nrecordists) AS nrecordists,
                    SUM(length) AS length, SUM(ncountries) AS ncountries, SUM(nlocations) AS nlocations
                FROM wa_collection_stats 
                WHERE branch='{$area->branch}'";
        }
        $res = query_db($sql);
        $row = $res->fetch_object();

        if (!$row) {
            return '';
        }

        $duration = XC_formatDuration($row->length);
        return "<table class='key-value'>
            <tbody>
            <tr>
            <td>" . _('Recordings') . "</td>
            <td>$row->nrecordings</td>
            </tr>
            <tr>
            <td>" . _('Species') . "</td>
            <td>$row->nspecies</td>
            </tr>
            <tr>
            <td>" . _('Recordists') . "</td>
            <td>$row->nrecordists</td>
            </tr>
            <tr>
            <td>" . _('Locations') . "</td>
            <td>$row->nlocations</td>
            </tr>
            <tr>
            <td>" . _('Countries') . "</td>
            <td>$row->ncountries</td>
            </tr>
            <tr>
            <td>" . _('Recording Time') . "</td>
            <td>$duration</td>
            </tr>
            </tbody>
            </table>
        ";
    }

    private function areaNewSpecies($area)
    {
        $localName = localNameLanguage();
        $statsSql = "
            SELECT tax.genus, tax.species, tax.eng_name, tax.species_nr, cnames.$localName as localName
            FROM wa_new_species ns
            INNER JOIN taxonomy tax USING(species_nr)
            LEFT JOIN taxonomy_multilingual as cnames USING(species_nr)
            WHERE ns.branch='{$area->branch}' ";
        if ($this->groupId()) {
            $statsSql .= " AND ns.group_id = " . $this->groupId();
        }
        $statsSql .= " ORDER BY snd_nr DESC";
        $res = query_db($statsSql);

        $html = "
            <ul class='simple'>
            ";

        if ($res->num_rows > 0) {
            while ($row = $res->fetch_object()) {
                $commonName = $row->localName;
                if (!$commonName) {
                    $commonName = $row->eng_name;
                }
                $sp = new Species($row->genus, $row->species, $commonName, $row->species_nr);
                $html .= "
                <li><a href='{$sp->profileURL()}'>{$sp->commonName()}</a></li>
                ";
            }
        } else {
            $html .= "<li>" . _('None') . "</li>";
        }
        $html .= '</ul>';

        return $html;
    }

    private function areaRecordists($area)
    {
        $statsSql = "SELECT
            wa_recordists.userid,
            users.username,
            nrecordings, nspecies
            FROM wa_recordists
            INNER JOIN users ON users.dir = wa_recordists.userid
            WHERE wa_recordists.branch='{$area->branch}'";
        if ($this->groupId()) {
            $statsSql .= " AND wa_recordists.group_id = " . $this->groupId();
        }
        $statsSql .= " ORDER BY nrecordings DESC";
        $res = query_db($statsSql);

        if ($res->num_rows > 0) {
            $html = "
            <table class='results'>
            <thead>
            <tr>
            <th>" . _('Recordist') . '</th>
            <th>' . _('rec.') . '</th>
            <th>' . _('sp.') . '</th>
            </tr>
            </thead>
            <tbody> ';
            while ($row = $res->fetch_object()) {
                $html .= "
                <tr>
                <td><a href='" . getUrl('recordist', ['id' => $row->userid]) . "'>{$row->username}</a></td>
                <td>{$row->nrecordings}</td>
                <td>{$row->nspecies}</td>
                </tr>
                ";
            }
            $html .= '
            </tbody>
            </table>';
        } else {
            $html = _('None');
        }


        return $html;
    }

    private function areaCommonSpecies($area)
    {
        $localName = localNameLanguage();
        $statsSql = "
            SELECT tax.genus, tax.species, tax.eng_name, species_nr, nrecordings, cnames.$localName as localName
            FROM wa_common_species
            INNER JOIN taxonomy tax USING(species_nr)
            LEFT JOIN taxonomy_multilingual as cnames USING(species_nr)
            WHERE wa_common_species.branch='{$area->branch}'";
        if ($this->groupId()) {
            $statsSql .= " AND wa_common_species.group_id = " . $this->groupId();
        }
        $statsSql .= " ORDER BY nrecordings DESC";
        $res = query_db($statsSql);

        $html = "
            <ul class='simple'>
            ";

        if ($res->num_rows > 0) {
            while ($row = $res->fetch_object()) {
                $commonName = $row->localName;
                if (!$commonName) {
                    $commonName = $row->eng_name;
                }
                $sp = new Species($row->genus, $row->species, $commonName, $row->species_nr);
                $html .= "
                <li>
                    <span class='recording-count'>$row->nrecordings</span>
                    <a href='{$sp->profileURL()}'>" . ($sp->commonName() ?? $sp->htmlDisplayName()) . "</a>
                </li>";
            }
        } else {
            $html .= "<li>" . _('None') . "</li>";
        }
        $html .= '</ul>';

        return $html;
    }

    private function growthChart($area)
    {
        $column = escape($area) . '_recordings';
        if ($this->groupId()) {
            $query = "
                SELECT UNIX_TIMESTAMP(date) as ts, $column as nrecordings 
                FROM stats_date 
                WHERE group_id = " . $this->groupId();
        } else {
            $query = "
                SELECT UNIX_TIMESTAMP(`date`) as ts, SUM($column) as nrecordings 
                FROM stats_date
                GROUP BY `date`";
        }

        $res = query_db($query);
        $data = [[], []];
        $prevMonth = 0;
        $first = true;
        while ($row = $res->fetch_object()) {
            // skip the first item because it consists of all of th recordings with no
            // date, which we don't want to plot...
            if ($first) {
                $first = false;
                continue;
            }

            $ts = $row->ts * 1000;
            $data[0][] = "[$ts, {$row->nrecordings}]";
            if ($prevMonth) {
                $nuploads = $row->nrecordings - $prevMonth;
                $data[1][] = "[$ts, $nuploads]";
            }
            $prevMonth = $row->nrecordings;
        }

        $jsdata = [
            '{ data: [ ' . implode(',', $data[0]) . "], label: '" . _(
                'All recordings'
            ) . "', color: '#862709', hoverable: false}",
            '{ data: [ ' . implode(',', $data[1]) . "], label: '" . _(
                'New uploads'
            ) . "', color: '#ba5b3c', lines: {fill: false}, points: {show: true}, stack: false, yaxis: 2}",
        ];

        $xopts = "{ mode: 'time', min: (new Date(2005,6,1)).getTime(), tickSize: [12, 'month']}";
        return "
            <script type='text/javascript' src='/static/js/flot/jquery.flot.js'></script>
            <script type='text/javascript' src='/static/js/flot/jquery.flot.time.js'></script>
            <script type='text/javascript' src='/static/js/xc-plot.js'></script>
            <script type='text/javascript'>
            jQuery(document).ready(function() {
                jQuery.plot(jQuery('#plot-recordings'), [ {$jsdata[0]}, {$jsdata[1]}], { xaxis: $xopts, yaxes: [{min: 0}, { position: 'right'}], series: { stack: true, lines: { fill: true, show: true }, points: { show: false}}, legend: {position: 'nw'}, grid: {hoverable: true}})
                jQuery('#plot-recordings').bind('plothover', xc.onplothover);
                });
            </script>
            <div style='height: 200px;' id='plot-recordings'></div>";
    }

    private function areaRecentRecordings($area)
    {
        $qStr = "area:{$area->branch}" . ($this->groupId() ? ' grp:' . $this->groupName($this->groupId()) : '');
        $q = new Query($qStr);
        $q->setOrder('xc');
        $q->setPageSize(15);
        $res = $q->execute(1);

        if ($res->num_rows > 0) {
            $html = "
            <table class='results'>
            <thead>
            <tr>
            <th></th>
            <th> " . _('Cat.nr.') . '</th>
            <th>' . _('Species') . '</th>
            <th>' . _('Recordist') . '</th>
            <th>' . _('Country') . '</th>
            </tr>
            </thead>
            <tbody>';
            while ($row = $res->fetch_object()) {
                $rec = new Recording($row);
                $sp = $rec->species();
                $html .= "
                <tr>
                <td>{$rec->miniPlayer()}</td>
                <td><a href='{$rec->URL()}'>XC{$rec->xcid()}</a></td>
                <td><a href='{$sp->profileURL()}'>" . ($sp->commonName() ?: $sp->fullScientificName()) . "</a></td>
                <td> <a href='" . getUrl('recordist', ['id' => $rec->recordistID()]) . "'>{$rec->recordist()}</a></td>
                <td>{$rec->country()}</td>
                </tr> ";
            }

            $html .= "
            </tbody>
            </table>
            <a href='" . getUrl(
                    'browse',
                    [
                        'query' => $qStr,
                        'order' => 'xc',
                        'dir' => SortDirection::NORMAL,
                    ]
                ) . "'>" . _('More') . '...</a>
            ';
        } else {
            $html = _('None');
        }

        return $html;
    }

    private function areaRecentForumPosts($area)
    {
        $sql = "SELECT F.topic_nr, F.subject, F.name, F.userid, F.date as date FROM forum_world F LEFT JOIN birdsounds USING(snd_nr)
            WHERE F.branch = '$area->branch' OR birdsounds.$area->birdsoundsColumn = 'Y'
            ORDER BY topic_nr DESC LIMIT 15";
        $res = query_db($sql);
        if (!$res || $res->num_rows == 0) {
            return '';
        }

        $html = "<table class='results'>
            <thead>
            <tr>
            <th>" . _('nr') . '</th>
            <th>' . _('Subject') . ' </th>
            <th> ' . _('Posted by') . '</th>
            <th>' . _('Date') . '</th>
            </tr>
            </thead>
            <tbody>
            ';
        while ($row = $res->fetch_object()) {
            $html .= "
                <tr>
                <td>$row->topic_nr</td>
                <td><a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]) . "'>{$row->subject}</a></td>
                <td>$row->name</td>
                <td> " . strftime('%b %e, %Y', strtotime($row->date)) . '</td>
                </tr>';
        }
        $html .= "
            </tbody>
            </table>
            <p> <a href='" . getUrl('forum', ['b' => $area->branch]) . "'>" . _('More') . '...</a> </p>';

        return $html;
    }

    private function areaCountries($area)
    {
        if ($this->groupId()) {
            $sql = "
                SELECT * FROM wa_countries 
                WHERE branch='$area->branch' AND group_id = " . $this->groupId() . "
                ORDER by nrecordings DESC";
        } else {
            $sql = "
                SELECT branch, country, SUM(nrecordings) AS nrecordings, SUM(nspecies) AS nspecies, 
                    SUM(nrecordists) AS nrecordists, SUM(length) AS length
                FROM wa_countries 
                WHERE branch='$area->branch'
                GROUP BY country
                ORDER by nrecordings DESC";
        }
        $res = query_db($sql);

        if ($res->num_rows > 0) {
            $html = "
            <table class='results'>
            <thead>
            <tr>
            <th>" . _('Country') . '</th>
            <th>' . _('Recordings') . '</th>
            <th>' . _('Species') . '</th>
            <th>' . _('Recordists') . '</th>
            <th>' . _('Recording Time') . '</th>
            </tr>
            </thead>
            <tbody>';

            while ($row = $res->fetch_object()) {
                $query = "area:$area->branch cnt:\"=$row->country\"";
                if ($this->groupId()) {
                    $query .= " grp:" . $this->groupName();
                }

                $duration = XC_formatDuration($row->length);
                $html .= "
                <tr>
                <td><a href='" . getUrl('browse', ['query' => $query]) . "'>{$row->country}</a></td>
                <td>$row->nrecordings</td>
                <td>$row->nspecies</td>
                <td>$row->nrecordists</td>
                <td>$duration</td>
                </tr>";
            }
            $html .= '
            </tbody>
            </table>';
        } else {
            $html = _('None');
        }

        return $html;
    }
}
