<?php

namespace xc\Controllers;

use xc\XCRedis;

use function xc\getUrl;
use function xc\sanitize;

class CollectionGraphs extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
        $this->redis->setKeyByRequest($this->redisKey('daily-stats-collection-graphs', $this->groupId()), $request);
    }

    public function handleRequest()
    {
        $title = _('Collection Graphs');
        $output = $this->redis->get();

        if (!$output) {
            if ($this->groupId()) {
                $query = "
                    SELECT UNIX_TIMESTAMP(`date`) as ts, nrecordings, nrecordists, nspecies, america_recordings, 
                        europe_recordings, asia_recordings, africa_recordings, australia_recordings, active_recordists, 
                        active_species 
                    FROM stats_date WHERE group_id = " . $this->groupId();
            } else {
                $query = "
                    SELECT UNIX_TIMESTAMP(`date`) as ts, SUM(nrecordings) AS nrecordings, SUM(nrecordists) AS nrecordists, 
                        SUM(nspecies) AS nspecies, SUM(america_recordings) AS america_recordings, 
                        SUM(europe_recordings) AS europe_recordings, SUM(asia_recordings) AS asia_recordings, 
                        SUM(africa_recordings) AS africa_recordings, SUM(australia_recordings) AS australia_recordings, 
                        SUM(active_recordists) AS active_recordists, SUM(active_species) AS active_species
                    FROM stats_date
                    GROUP BY `date` ";
            }
            $res = query_db($query);

            $totalRecordings = [];
            $totalContributors = [];
            $totalSpecies = [];
            $americaRecordings = [];
            $europeRecordings = [];
            $asiaRecordings = [];
            $africaRecordings = [];
            $australiaRecordings = [];
            $uploadData = [];
            $activeContributors = [];
            $activeSpecies = [];
            $RecordingsRecordists = [];
            $RecordingsSpecies = [];
            $RecordingsActiverecordists = [];
            $ActivespeciesActiverecordists = [];
            $prevMonth = 0;
            $first = true;
            while ($row = $res->fetch_object()) {
                // skip the first item because it consists of all of the recordings with no
                // date, which we don't want to plot...
                if ($first) {
                    $first = false;
                    continue;
                }

                $ts = $row->ts * 1000;
                $numberRecordings = $row->nrecordings;
                $numberRecordists = $row->nrecordists;
                $numberSpecies = $row->nspecies;
                $numberactiveRecordists = $row->active_recordists;
                $numberactiveSpecies = $row->active_species;

                $totalRecordings[] = "[$ts, {$numberRecordings}]";
                $totalContributors[] = "[$ts, {$numberRecordists}]";
                $totalSpecies[] = "[$ts, {$numberSpecies}]";
                $americaRecordings[] = "[$ts, {$row->america_recordings}]";
                $europeRecordings[] = "[$ts, {$row->europe_recordings}]";
                $asiaRecordings[] = "[$ts, {$row->asia_recordings}]";
                $africaRecordings[] = "[$ts, {$row->africa_recordings}]";
                $australiaRecordings[] = "[$ts, {$row->australia_recordings}]";
                $activeContributors[] = "[$ts, {$numberactiveRecordists}]";
                $activeSpecies[] = "[$ts, {$numberactiveSpecies}]";
                $RecordingsRecordists[] = "[$numberRecordings,{$numberRecordists}]";
                $RecordingsSpecies[] = "[$numberRecordings,{$numberSpecies}]";

                $nuploads = 0;
                if ($prevMonth) {
                    $nuploads = $numberRecordings - $prevMonth;
                    $uploadData[] = "[$ts, $nuploads]";
                }
                $prevMonth = $numberRecordings;
                $RecordingsActiverecordists[] = "[$numberactiveRecordists,{$nuploads}]";
                $ActivespeciesActiverecordists[] = "[$numberactiveRecordists,{$numberactiveSpecies}]";
            }

            $res = query_db(
                "
                SELECT UNIX_TIMESTAMP(DATE_FORMAT(datetime, '%Y-%m-01')) as ts, COUNT(mes_nr) as ncomments 
                FROM discussion_forum_world 
                GROUP BY YEAR(datetime), MONTH(datetime)"
            );
            $forumdata = [[], []];
            $total_comments = 0;
            while ($row = $res->fetch_object()) {
                $ts = $row->ts * 1000;
                $total_comments += $row->ncomments;
                $forumdata[0][] = "[$ts, $total_comments]";
            }

            $res = query_db(
                "
                SELECT UNIX_TIMESTAMP(DATE_FORMAT(date, '%Y-%m-01')) as ts, COUNT(topic_nr) as ntopics 
                FROM forum_world 
                GROUP BY YEAR(date), MONTH(date)"
            );
            $total_topics = 0;
            while ($row = $res->fetch_object()) {
                $ts = $row->ts * 1000;
                $total_topics += $row->ntopics;
                $forumdata[1][] = "[$ts, $total_topics]";
            }

            if ($this->groupId()) {
                $recYearQuery = "
                    SELECT YEAR(date) as year, COUNT(snd_nr) as nrec, COUNT(DISTINCT species_nr) as nsp 
                    FROM birdsounds 
                    WHERE group_id = " . $this->groupId() . "
                    GROUP BY YEAR(date)";
            } else {
                $recYearQuery = "
                    SELECT YEAR(date) as year, COUNT(snd_nr) as nrec, COUNT(DISTINCT species_nr) as nsp 
                    FROM birdsounds 
                    GROUP BY YEAR(date)";
            }
            $res = query_db($recYearQuery);
            $recyeardata = [[], []];
            while ($row = $res->fetch_object()) {
                if ($row->year < 1900 || $row->year > intval(date('Y'))) {
                    continue;
                }
                $recyeardata[0][] = "[{$row->year}, {$row->nsp}]";
                $recyeardata[1][] = "[{$row->year}, {$row->nrec}]";
            }

            $jsRecordingsData = [
                'total' => '{ 
                    data: [ ' . implode(',', $totalRecordings) . "], 
                    label: '" . sanitize(_('All recordings')) . "'
                }",
                'america' => '{ 
                    data: [ ' . implode(',', $americaRecordings) . "], 
                    label: '" . sanitize(_('Americas')) . "', 
                    color: '#CE5C00', 
                    hoverable: false
                }",
                'europe' => '{ 
                    data: [ ' . implode(',', $europeRecordings) . "], 
                    label: '" . sanitize(_('Europe')) . "', 
                    color: '#C4A000', 
                    hoverable: false
                }",
                'asia' => '{ 
                    data: [ ' . implode(',', $asiaRecordings) . "], 
                    label: '" . sanitize(_('Asia')) . "', 
                    color: '#4E9A06', 
                    hoverable: false
                }",
                'africa' => '{ 
                    data: [ ' . implode(',', $africaRecordings) . "], 
                    label: '" . sanitize(_('Africa')) . "', 
                    color: '#204A87', 
                    hoverable: false
                }",
                'australia' => '{ 
                    data: [ ' . implode(',', $australiaRecordings) . "], 
                    label: '" . sanitize(_('Australasia')) . "', 
                    color: '#5C3566', 
                    hoverable: false
                }",
                'uploads' => '{ 
                    data: [ ' . implode(',', $uploadData) . "], 
                    label: '" . sanitize(_('New uploads')) . "', 
                    color: '#862709', 
                    lines: {fill: false}, 
                    points: {show: true}, 
                    stack: false, 
                    yaxis: 2
                }",
            ];

            $jsSpeciesData = [
                'total' => '{ 
                    data: [ ' . implode(',', $totalSpecies) . "], 
                    label: '" . sanitize(_('Total species')) . "', 
                    color: '#ba5b3c'
                }",
                'active' => '{ 
                    data: [ ' . implode(',', $activeSpecies) . "], 
                    label: '" . sanitize(_('Species uploaded during current time period')) . "', 
                    color: '#862709'
                }",
            ];

            $jsContributorsData = [
                'total' => '{ 
                    data: [ ' . implode(',', $totalContributors) . "], 
                    label: '" . sanitize(_('Total contributors')) . "', 
                    color: '#ba5b3c'
                }",
                'active' => '{ 
                    data: [ ' . implode(',', $activeContributors) . "], 
                    label: '" . sanitize(_('Active contributors during current time period')) . "', 
                    color: '#862709'
                }",
            ];

            $jsForumData = [
                'comments' => '{ 
                    data: [ ' . implode(',', $forumdata[0]) . "], 
                    label: '" . sanitize(_('Forum comments')) . "', 
                    color: '#ba5b3c'
                }",
                'topics' => '{ 
                    data: [ ' . implode(',', $forumdata[1]) . "], 
                    label: '" . sanitize(_('Forum topics')) . "', 
                    color: '#862709'
                }",
            ];

            $jsRecordingsBaseData = [
                'recordistsvsrecordings' => '{ 
                    data: [ ' . implode(',', $RecordingsRecordists) . "], 
                    label: '" . sanitize(_('nr. recordists')) . "', 
                    color: '#ba5b3c'
                }",
                'speciesvsrecordings' => '{ 
                    data: [ ' . implode(',', $RecordingsSpecies) . "], 
                    label: '" . sanitize(_('nr. species')) . "', 
                    color: '#862709'
                }",
            ];

            $jsactiveRecordistsBaseData = [
                'activespeciesvsactiverecordists' => '{ 
                    data: [ ' . implode(',', $ActivespeciesActiverecordists) . "], 
                    label: '" . sanitize(_('nr. species per month')) . "', 
                    color: '#ba5b3c'
                }",
                'uploadsvsactiverecordists' => '{ 
                    data: [ ' . implode(',', $RecordingsActiverecordists) . "], 
                    label: '" . sanitize(_('nr. uploads per month')) . "', 
                    color: '#862709'
                }",
            ];

            $jsRecYearData = [
                'species' => '{ 
                    data: [ ' . implode(',', $recyeardata[0]) . "], 
                    label: '" . sanitize(_('Number of species')) . "', 
                    color: '#ba5b3c'
                }",
                'recordings' => '{ 
                    data: [ ' . implode(',', $recyeardata[1]) . "], 
                    label: '" . sanitize(_('Number of recordings')) . "', 
                    color: '#862709'
                }",
            ];

            $xopts = "{ mode: 'time', min: (new Date(2005,6,1)).getTime(), tickSize: [6, 'month']}";
            $script = "<script type='text/javascript' src='/static/js/flot/jquery.flot.js'></script>
                    <script type='text/javascript' src='/static/js/flot/jquery.flot.time.js'></script>
                    <script type='text/javascript' src='/static/js/flot/jquery.flot.stack.js'></script>
                    <script type='text/javascript' src='/static/js/xc-plot.js'></script>
                    <script type='text/javascript'>
                    jQuery(document).ready(function() {
                        var previousPoint = null;
                    
                        jQuery.plot(jQuery('#plot-recordings'), [
                                {$jsRecordingsData['america']},
                                {$jsRecordingsData['europe']},
                                {$jsRecordingsData['asia']},
                                {$jsRecordingsData['africa']},
                                {$jsRecordingsData['australia']},
                                {$jsRecordingsData['uploads']}
                            ],
                            {
                                xaxis: $xopts,
                                yaxes: [{min: 0, tickSize: 50000, labelWidth: 80}, { position: 'right'}],
                                series: {stack: true, 
                                lines: {fill: true, show: true}, 
                                points: {show: false}},
                                legend: {position: 'nw'},
                                grid: {hoverable: true}
                            }
                        )
                        jQuery('#plot-recordings').bind('plothover', xc.onplothover);
                    
                        jQuery.plot(jQuery('#plot-species'), [
                                {$jsSpeciesData['total']},
                                {$jsSpeciesData['active']}
                            ],
                            {
                                xaxis: $xopts,
                                yaxis: {min: 0, labelWidth: 80},
                                lines: {show: true},
                                points: {show: true},
                                legend: {position: 'nw'},
                                grid: {hoverable: true}
                            }
                        )
                        jQuery('#plot-species').bind('plothover', xc.onplothover);
                    
                        jQuery.plot(jQuery('#plot-recordists'), [
                                {$jsContributorsData['total']},
                                {$jsContributorsData['active']}
                            ],
                            {
                                xaxis: $xopts,
                                yaxis: {min: 0, labelWidth: 80},
                                lines: {show: true},
                                points: {show: true},
                                legend: {position: 'nw'},
                                grid: { hoverable: true}
                            }
                        )
                        jQuery('#plot-recordists').bind('plothover', xc.onplothover);
                    
                        jQuery.plot(jQuery('#plot-forum'), [
                                {$jsForumData['comments']}, {$jsForumData['topics']}
                            ],
                            {
                                xaxis: $xopts,
                                yaxis: {min: 0, labelWidth: 80},
                                lines: {show: true},
                                points: {show: true},
                                legend: {position: 'nw'},
                                grid: { hoverable: true}
                            }
                        )
                        jQuery('#plot-forum').bind('plothover', xc.onplothover);
                    
                        jQuery.plot(jQuery('#plot-Recordingsbase'), [
                                {$jsRecordingsBaseData['recordistsvsrecordings']},
                                {$jsRecordingsBaseData['speciesvsrecordings']}
                            ],
                            {
                                xaxis: {min:0},
                                yaxis: {min: 0, labelWidth: 80},
                                lines: {show: true },
                                points: {show: true},
                                legend: {position: 'nw'},
                                grid: {hoverable: true}
                            }
                        )
                        jQuery('#plot-Recordingsbase').bind('plothover', xc.onplothover);
                    
                        jQuery.plot(jQuery('#plot-activeRecordistsbase'), [
                                {$jsactiveRecordistsBaseData['activespeciesvsactiverecordists']},
                                {$jsactiveRecordistsBaseData['uploadsvsactiverecordists']}
                            ],
                            {
                                xaxis: {min:0},
                                yaxis: {min: 0, labelWidth: 80},
                                lines: {show: false },
                                points: {show: true},
                                legend: {position: 'nw'},
                                grid: {hoverable: true}
                            }
                        )
                        jQuery('#plot-activeRecordistsbase').bind('plothover', xc.onplothover);
                    
                        jQuery.plot(jQuery('#plot-recyear'), [
                                {$jsRecYearData['recordings']}, {$jsRecYearData['species']}
                            ],
                            {
                                xaxis: {},
                                yaxis: {min: 0, labelWidth: 80},
                                lines: { show: true },
                                points: { show: true},
                                legend: {position: 'nw'},
                                grid: { hoverable: true }
                            }
                        )
                        jQuery('#plot-recyear').bind('plothover', xc.onplothover);
                     });
                     </script>";

            $output = "
                        <ul class='breadcrumbs'>
                        <li><a href='" . getUrl('collection-details') . "'>" . _(
                    'Collection Details'
                ) . "</a></li>
                        <li class='current'>$title</li>
                        </ul>
                        <div class='group-select'>" . $this->printGroupSelect() . "</div>
                        <p>" . _(
                    'Several graphs showing how the collection has grown over time.'
                ) . '
                        ' . _(
                    "Unfortunately, precise upload dates are not known for recordings that were uploaded during the first several years of xeno-canto's existence."
                ) . '</p>
                    
                        <h2>' . _('Recordings') . '</h2>
                        <p>' . _(
                    'The total number of recordings and monthly upload rate over time. Note that the upload rate is plotted at a different scale according to the axis on the right.'
                ) . "</p>
                        <div style='width: 100%; height: 400px;' id='plot-recordings'></div>
                        <h2>" . _('Species') . '</h2>
                        <p>' . _(
                    'The number of species represented by at least one recording in the collection.'
                ) . "</p>
                        <div style='width: 100%; height: 400px;' id='plot-species'></div>
                        <h2>" . _('Contributors') . '</h2>
                        <p>' . _(
                    'The number of users that have contributed at least one recording to the collection.'
                ) . "</p>
                        <div style='width: 100%; height: 400px;' id='plot-recordists'></div>
                        <h2>" . _('Forum Activity') . '</h2>
                        <p>' . _('Activity on our forums over time.') . "</p>
                        <div style='width: 100%; height: 400px;' id='plot-forum'></div>
                        <h2>" . _('Recordists & Species vs. Recordings') . '</h2>
                        <p>' . _(
                    'Number of recordists and species as a function of number of recordings instead of time.'
                ) . "</p>
                        <div style='width: 100%; height: 400px;' id='plot-Recordingsbase'></div>
                        <h2>" . _('Recordings & Species vs. Active recordists') . '</h2>
                        <p>' . _(
                    'Number of uploads and species per month as a function of number of active recordists in that month.'
                ) . "</p>
                        <div style='width: 100%; height: 400px;' id='plot-activeRecordistsbase'></div>
                        <h2>" . _('Recording Year') . '</h2>
                        <p>' . _(
                    'Number of recordings and species as a function of the year they were recorded.'
                ) . "</p>
                    <div style='width: 100%; height: 400px;' id='plot-recyear'></div>
                    $script
                ";

            $this->redis->set($output);
        }

        return $this->template->render($output, ['title' => $title]);
    }
}
