<?php

namespace xc\Controllers;

use xc\ForumThread;
use xc\User;
use xc\XCMail;

use function PHP81_BC\strftime;
use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\XC_adminMailAddresses;
use function xc\XC_formatUserText;

function userCanView($row)
{
    $user = User::current();
    return ($row->public || ($user && ($user->isAdmin() || $row->dir === $user->userId())));
}

function userCanEdit($row)
{
    $user = User::current();
    return ($user && ($user->isAdmin() || ($row->dir === $user->userId())));
}

function userInfoBox($row)
{
    // this doesn't have full information, but enough to use parts of the User
    // API.  yeah... ugly.
    $u = new User($row);
    $avatarText = '';
    if ($u->hasAvatar()) {
        $avatarText = "<a href='{$u->getAvatar()}' class='fancybox'><img class='avatar' src='{$u->getThumbnail()}'/></a>";
    } else {
        $avatarText = "<img class='avatar' src='{$u->getDefaultThumbnail()}'/>";
    }

    return '
        <h1>' . _('About the author') . "</h1>
        <div class='userbox'>
          <div class='avatar-container'>
            $avatarText
          </div>
          <h1>
          <a href='{$u->getProfileURL()}' title='public profile'>{$u->userName()}</a>
          </h1>
          <ul>
          <li><img class='icon' src='/static/img/discuss-light.png' title='Number of forum posts'/>$row->ncomments</li>
          <li><img class='icon' src='/static/img/audio.png' title='Number of recordings' />$row->nrecordings</li>
          </ul>
        </div>
        <p>" . XC_formatUserText($u->blurb()) . '</p>
        ';
}

class ArticleView extends Controller
{

    public function handlePost($blognr)
    {
        $comment = $this->request->request->get('comment-text');
        $incomplete = false;
        $error = null;

        $user = User::current();
        if (!$user) {
            notifyError(_('You must be logged in to post a comment.'));
            $incomplete = true;
            // $error = new Notification(Notification::ERROR, _("You must be logged in to post a comment."));
        } elseif (!$user->isVerified()) {
            notifyError(
                _(
                    'You must verify your email address before posting a comment.'
                ) . ' ' .
                sprintf(
                    _(
                        "To re-send a verification code, please visit <a href='%s'>your account page</a>."
                    ),
                    getUrl('mypage')
                )
            );
            $incomplete = true;
            // $error = new Notification(Notification::ERROR, _("You must verify your email address before posting a comment.") . " " .
            //    sprintf(_("To re-send a verification code, please visit <a href='%s'>your account page</a>."), \xc\getUrl('mypage')));
        } elseif (empty($comment)) {
            notifyError(_('You cannot post an empty comment.'));
            $incomplete = true;
            // $error = new Notification(Notification::ERROR, _("You cannot post an empty comment."));
        }

        if (!$error && !$incomplete) {
            $name = escape($user->userName());
            $Date = date('d-m-Y');
            $Time = time();
            $hours = (int)floor($Time / 3600) % 24 + 1;
            $minutes = (int)floor($Time / 60) % 60;
            if ($minutes < 10) {
                $minutes = "0$minutes";
            }
            $ecomment = escape($comment);
            $userdir = User::current()->userId();
            $sql = "insert into feature_discussions values ('$name','$ecomment','$Date','$hours:$minutes','$userdir','$blognr',NULL, NOW())";
            query_db($sql);

            $subject = "[xeno-canto] New article comment on $blognr";
            $link = getUrl('feature-view', ['blognr' => $blognr]);
            $body = "From: $name\n\nLink: $link\n\nText: $comment";

            (new XCMail(XC_adminMailAddresses(), $subject, $body))->send();

            $res = query_db(
                "select title, email, username from blogs INNER JOIN users USING(dir) where blognr=$blognr"
            );
            $row = $res->fetch_object();
            $featurename = $row->title;

            $authoremail = $row->email;
            $commenter = $user->userName();

            $subject = '[xeno-canto] New comment on article';
            $message = wordwrap(
                sprintf(
                    "%s has posted a new comment on your article '%s'.  To view all comments, visit the following URL:",
                    $commenter,
                    $featurename
                ),
                60
            );
            $wrappedComment = wordwrap($comment, 60);
            (new XCMail(
                $authoremail,
                "[xeno-canto] $subject",
                "$message

                $link

                $wrappedComment"
            ))->send();

            return $this->seeOther($link);
        }

        return $this->renderContent($blognr, $error);
    }

    protected function renderContent($blognr, $error = null)
    {
        $row = null;
        $blognr = escape($blognr);
        if ($blognr) {
            $res = query_db(
                "
                SELECT title, blog_as_html, dir, public, published AS published, 
                    date_last_update AS updated, U.username, U.blurb, 
                    nrecordings, ncomments
                FROM blogs 
                INNER JOIN users U USING(dir) 
                LEFT JOIN rec_summary_stats S ON S.userid = U.dir AND S.group_id = 0
                WHERE blognr = $blognr
                GROUP BY dir"
            );
            $row = $res->fetch_object();
        }

        if (!$row || !($row && userCanView($row))) {
            return $this->notFound();
        }

        $date = strftime('%B %e, %Y', strtotime($row->published));
        $updateDate = null;
        if ($row->updated > $row->published) {
            $updateDate .= '<em>' .
                sprintf(
                    _('This article was last updated on %s at %s'),
                    strftime('%B %e, %Y', strtotime($row->updated)),
                    date('H:i', strtotime($row->updated))
                ) . '</em>';
        }

        $articleTitle = $row->title;
        $body = "
            <ul class='breadcrumbs'>
            <li><a href='" . getUrl('features') . "'>" . _('Articles') . "</a></li>
            <li class='current'>$articleTitle</li>
            </ul>
            ";

        $editLink = '';
        if (userCanEdit($row)) {
            $editLink = "<p><a href='" . getUrl('feature-edit', ['blognr' => $blognr]) . "'>" .
                _('Edit this article') . '</a></p>';
        }
        $body .= "<section class='column forum-thread'>
            <article itemscope itemtype='//schema.org/Article'>
            <h1 itemprop='name'>{$row->title}</h1>
            <h2>" . sprintf(
                _('By %s'),
                "<span itemprop='author' itemscope itemtype='//schema.org/Person'><span itemprop='name'>{$row->username}</span></span>"
            ) . "</h2>
            <p>$date</p>
            <div itemprop='articleBody'>$row->blog_as_html</div>
            $updateDate
            $editLink
            </article>";

        //add discussion if there is anything on record
        $body .= '
            <p>' .
            _('How was this made?') . " <a href='" . getUrl('feature-preview', ['blognr' => $blognr]) . "'>" .
            _('View the text') . "</a></p>
            <a name='comments'></a>
            ";
        if ($error) {
            $body .= $error->formatHTML();
        }

        $thread = new ForumThread($blognr, 'feature_discussions', 'blognr');
        $body .= $thread->generateHTML();
        $body .= '
            <h2>' . _('Add your comments') . "</h2>
            <form class='new-message' action='#last' method='post'>";
        if (User::current()) {
            $body .= "
                <input type='hidden' name='blognr' value='$blognr'>
                <p>" .
                sprintf(
                    _(
                        'You can format your text using the %s text formatting syntax.'
                    ),
                    "<a target='_blank' href='" . getUrl(
                        'markdown'
                    ) . "'>Markdown</a>"
                )
                . "</p>
                <p>
                <p>
                <textarea class='message-text' name='comment-text' placeholder='" .
                htmlspecialchars(_('Comment Text'), ENT_QUOTES) . "'>{$this->request->request->get('comment-text')}</textarea>
                </p>
                <input type='submit' value='" . htmlspecialchars(
                    _('Comment'),
                    ENT_QUOTES
                ) . "'>
                ";
        } else {
            $body .= '
                <p>' .
                sprintf(
                    _("<a href='%s'>Log in</a> to reply to this topic."),
                    getUrl('login')
                ) . '
                </p>';
        }
        $body .= "
            </form>
            </section>
            <section class='column author-info'>
            " . userInfoBox($row) . '
            </section>';

        return $this->template->render(
            $body,
            ['title' => _('Article'), 'bodyId' => 'features']
        );
    }

    public function handleRequest($blognr)
    {
        return $this->renderContent($blognr);
    }

}
