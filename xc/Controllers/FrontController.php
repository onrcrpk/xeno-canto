<?php

namespace xc\Controllers;

use Exception;
use InvalidArgumentException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\ErrorHandler\ErrorHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;
use xc\App;
use xc\DB;
use xc\Language;
use xc\Query;
use xc\Settings;
use xc\User;

class FrontController extends ControllerBase implements App
{
    private $router;

    private $settings;

    private $db;

    private $dataDir;

    private $session;

    private $exceptionHandler;

    private $requestContext;


    public function __construct($request, $dataDir, $env = 'prod')
    {
        parent::__construct($request);

        // look inside *this* directory
        $locator = new FileLocator([$dataDir]);
        $this->requestContext = new RequestContext();
        $this->requestContext->fromRequest($request);
        $settingsFile = $locator->locate('settings.php', 'config', true);
        $this->settings = Settings::load($settingsFile, $env);
        $this->dataDir = $dataDir;
        $this->db = new DB($this->settings());
        $storage = null;
        if ($this->settings()->useMockSession()) {
            $storage = new MockArraySessionStorage();
        }
        $this->session = new Session($storage);
        $this->session->start();

        if ($this->settings()->debug()) {
            Debug::enable();
            $this->exceptionHandler = new ErrorHandler();
        }

        $this->router = new Router(
            new YamlFileLoader($locator), 'routes.yml', ['cache_dir' => $dataDir . '/cache'], $this->requestContext
        );
    }

    public function settings()
    {
        return $this->settings;
    }

    public function handleRequest()
    {
        if (!User::current() && isset($_COOKIE['login'])) {
            User::loginWithCookie($_COOKIE['login']);
        }

        $language = $this->determineBestLanguage();
        $this->setLanguage($language);

        // FIXME: ugly, put here for now
        Query::setDefaultNumPerPage(defaultNumPerPage());
        try {
            $response = $this->dispatchRequest($this->request);
            $response->headers->set('X-Content-Type-Options', 'nosniff');
            // Set X-Frame-Options SAMEORIGIN for all but embed pages
            if (!$this->pageIsEmbedded()) {
                $response->headers->set('X-Frame-Options', 'SAMEORIGIN');
            }
            // Set Strict-Transport-Security header except for localhost (which may use http)
            if (!$this->serverIsLocalHost() && $response->getStatusCode() === Response::HTTP_OK) {
                $response->headers->set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
            }
        } catch (ResourceNotFoundException $e) {
            $this->debugException($e);
            $response = $this->notFound();
        } catch (MissingMandatoryParametersException $e) {
            $this->debugException($e);
            $response = $this->badRequest();
        } catch (Exception $e) {
            $this->debugException($e);
            $response = $this->internalServerError();
        }
        $response->send();
    }

    public function determineBestLanguage()
    {
        // highest-priority is current session override
        if ($this->language()) {
            return $this->language();
        }
        // try cookie next
        if ($this->request->cookies->get('language')) {
            $language = Language::lookup($this->request->cookies->get('language'));
            if ($language) {
                return $language;
            }
        }
        // next priority is user preference
        $user = User::current();
        if ($user) {
            return $user->getLanguage();
        }
        // look through sorted list and use first one that matches our languages
        foreach ($this->request->getLanguages() as $code) {
            // use the first matched language
            $language = Language::lookup($code);
            if ($language) {
                return $language;
            }
        }
        return null;
    }

    public function language()
    {
        return $this->session()->get('language');
    }

    public function setCookie($name, $value, $expires = null)
    {
        $options = [
            'expires' => (int)$expires ?: time() + 3600 * 24 * 60,
            'path' => '/',
            'samesite' => 'Lax',
            'httponly' => true,
            'secure' => true
        ];
        setcookie($name, $value, $options);
    }

    public function session()
    {
        return $this->session;
    }

    public function setLanguage($language, $overrideSession = false)
    {
        if ($overrideSession) {
            $this->session()->remove('language');
        }

        if ($language) {
            setlocale(LC_MESSAGES, $language->locale);
            setlocale(LC_TIME, $language->locale);
            Query::setLocalNameLanguage($language->localNameLanguage);
        } else {
            setlocale(LC_MESSAGES, '');
            setlocale(LC_TIME, '');
            Query::setLocalNameLanguage(null);
        }
        bindtextdomain('xenocanto', $this->dataDir . '/locales/');
        bind_textdomain_codeset('xenocanto', 'UTF-8');
        textdomain('xenocanto');
    }

    protected function dispatchRequest($request)
    {
        $params = $this->router->matchRequest($request);
        return $this->dispatchInternal($params);
    }

    protected function dispatchInternal($params)
    {
        $this->request->attributes = $params;
        $controller = $this->createController(
            $params['_controller'],
            $this->request
        );
        $response = $controller[0]->authCheck();
        if (!$response) {
            $args = [];
            foreach ($params as $key => $val) {
                if (strpos($key, '_') !== 0) {
                    $args[] = $val;
                }
            }
            $response = call_user_func_array($controller, $args);
        }

        if (is_string($response)) {
            $r = new Response();
            $r->setContent($response);
            $r->setStatusCode(Response::HTTP_OK);
            $response = $r;
        }

        return $response;
    }

    private function createController($controller, $request)
    {
        if (!str_contains($controller, '::')) {
            throw new InvalidArgumentException(
                sprintf('Unable to find controller "%s".', $controller)
            );
        }

        [$class, $method] = explode('::', $controller, 2);

        if (!class_exists($class)) {
            throw new InvalidArgumentException(
                sprintf('Class "%s" does not exist.', $class)
            );
        }
        return [new $class($request), $method];
    }

    public function pageIsEmbedded()
    {
        return str_contains($this->request->getPathInfo(), 'embed');
    }

    public function serverIsLocalHost()
    {
        return isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'localhost';
    }

    public function debugException($e)
    {
        if ($this->settings()->debug()) {
            $this->exceptionHandler->handleException($e);
        }
    }

    public function getUrl($route, $args = [], $absolute = false)
    {
        if ($absolute) {
            $absolute = Router::ABSOLUTE_URL;
        }

        // @TODO hack, fix this!
        if (!$this->serverIsLocalHost()) {
            // Force https for server; check why this defaults to http
            $this->requestContext->setScheme('https');
            $this->router->setContext($this->requestContext);
        }

        return $this->router->generate($route, $args, $absolute);
    }

    public function db()
    {
        return $this->db;
    }

    public function soundsStagingDir($user)
    {
        return $this->soundsBaseDir() . "/staging/$user";
    }

    private function soundsBaseDir()
    {
        return $this->request->server->get('DOCUMENT_ROOT', $this->dataDir) . '/sounds';
    }

    public function soundsUploadDir($user)
    {
        return $this->soundsBaseDir() . "/uploaded/$user";
    }

    public function soundsTrashDir()
    {
        return $this->soundsBaseDir() . '/trash';
    }

    protected function dispatchUrl($url)
    {
        $params = $this->router->match($url);
        return $this->dispatchInternal($params);
    }

}
