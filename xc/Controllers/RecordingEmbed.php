<?php

namespace xc\Controllers;

use Symfony\Component\HttpFoundation\Response;
use xc\DeletedRecording;
use xc\EmbedTemplate;
use xc\Recording;

use function xc\escape;
use function xc\getUrl;

class RecordingEmbed extends Controller
{

    public function showHelp($xcid)
    {
        $title = _('Embed a Recording');
        $xcid = escape($xcid);
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $simpleEmbed = "<iframe src='" . getUrl(
                'embed',
                ['XC' => $xcid, 'simple' => 1],
                true
            ) . "' scrolling='no' frameborder='0' width='340' height='115'></iframe>";
        $sonoEmbed = "<iframe src='" . getUrl(
                'embed',
                ['XC' => $xcid],
                true
            ) . "' scrolling='no' frameborder='0' width='340' height='220'></iframe>";
        $darkEmbed = "<iframe src='" . getUrl(
                'embed',
                ['XC' => $xcid, 'darkbg' => 1],
                true
            ) . "' scrolling='no' frameborder='0' width='340' height='220'></iframe>";

        $output = "
            <h1>$title</h1>
            <p>" . _(
                'To embed a player for this recording on an external website, copy and paste the following HTML into your webpage.  There are two player options.  An example of each is shown below.'
            ) . '
            </p>
            <p>' . _(
                'These are the same players that are used throughout the site and should work in all modern browsers. It will use native HTML5 audio if the MP3 format is supported, otherwise it will fall back to using flash.'
            ) . '
            </p>
            <h2>' . _('Simple Player') . '</h2>
            <p>' . _('A simple, compact player.') . ' ' . _(
                'To include this player in your website, copy and paste the following code:'
            ) . '</p>
            <textarea>' . htmlentities($simpleEmbed) . '</textarea>
            <p>' . _('Preview') . ":</p>
            $simpleEmbed
            <h2>" . _('Sonogram Player') . '</h2>
            <p>' . _(
                'A slightly larger player that also includes a sonogram that displays the first ten seconds of the recording.'
            ) . ' ' . _(
                'To include this player in your website, copy and paste the following code:'
            ) . '</p>
            <textarea>' . htmlentities($sonoEmbed) . '</textarea>
            <p>' . _('Preview') . ":</p>
            $sonoEmbed
            <h2>" . _('Dark Background') . '</h2>
            <p>' . _(
                'If you need to embed it into a page with a dark background, you can add darkbg=1 to either of the URLS mentioned above.'
            ) . ' ' . _(
                'For example, to include the sonogram player in your website, copy and paste the following code:'
            ) . '</p>
            <textarea>' . htmlentities($darkEmbed) . '</textarea>
            <p>' . _('Preview') . ":</p>
            <div style='border-radius: 1em; color:#EEEEEE; padding: 2em; background-color:#111111;'>
            $darkEmbed
            </div>";

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'recording-embed']
        );
    }

    public function embed($xcid)
    {
        $t = new EmbedTemplate();
        $darkbg = $this->request->query->get('darkbg');
        $rec = Recording::load(intval($xcid));

        if (!$rec) {
            $rec = DeletedRecording::load($xcid);
            if ($rec) {
                $output = "<p class='error'>The recording XC$xcid {$rec->commonName()} ({$rec->fullScientificName()}) has been removed by its author.  We apologize for the inconvenience.</p>";
            } else {
                $output = "<p class='error'>Recording XC$xcid does not exist.</p>";
            }
            return new Response(
                $t->render(
                    "<h1>xeno-canto</h1> $output",
                    ['title' => "XC$xcid not found"]
                ), Response::HTTP_NOT_FOUND
            );
        }

        $options = [
            'showLocation' => true,
            'showLogo' => true,
            'linkSono' => false,
        ];
        if ($this->request->query->get('simple') == 1) {
            $options['showSono'] = false;
        }

        $bodyClass = '';
        if ($darkbg) {
            $bodyClass = 'darkbg';
        }

        return $t->render(
            $rec->player($options),
            [
                'bodyClass' => $bodyClass,
                'title' =>
                    "XC$xcid {$rec->commonName()} ({$rec->fullScientificName()})",
            ]
        );
    }

}
