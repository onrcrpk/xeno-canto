<?php

namespace xc\Controllers;

use xc\Recording;
use xc\RecordingFormData;
use xc\Species;
use xc\ThreadType;
use xc\UploadStep;
use xc\UploadStepDetails;
use xc\UploadStepLocation;
use xc\UploadStepVerify;
use xc\User;

use function xc\getUrl;
use function xc\XC_logger;

class Upload extends UploadBase
{

    public function showSuccess($xcid)
    {
        $rec = Recording::load($xcid);

        if (!$rec) {
            return $this->errorPage();
        }

        $title = _('Upload Successful');
        $confirmationNote = '';
        if ($rec->status() == ThreadType::ID_UNCONFIRMED) {
            $confirmationNote = "<p class='important'><strong>" .
                _('Note') . '</strong>: ' . _(
                    "A high number of your recordings from {$rec->country()} have been questioned by other users.  Therefore, this recording will need to be independently confirmed before it will be included in the collection. If you resolve your questioned recordings from {$rec->country()}, you will again be able to upload without restrictions."
                ) . '</p>';
        }
        $output = "<h1>$title</h1>";
        $output .= '<p>' .
            sprintf(
                _(
                    'Your recording has been successfully uploaded as %s.  Thank you for sharing your recording.'
                ),
                "<a href='{$rec->URL()}'>XC$xcid</a>"
            ) . "
            </p>
            $confirmationNote
            <p>" .
            _('Upload another recording?') . "
            <ul>
            <li><a href='" . getUrl('upload') . "'>" . _('Yes, upload a new recording') . "</a></li>
            <li><a href='" . getUrl('upload', ['tmpl' => $xcid]) . "'>" . sprintf(
                _('Yes, and use the data from %s as a template for the new recording'),
                "XC$xcid"
            ) . '</a></li>
            </ul>
            </p>
            ';
        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'upload-results']
        );
    }

    public function errorPage()
    {
        $title = _('Upload Error');
        $output = "<h1>$title</h1>";
        $output .= '
            <p>
                There was an error during the upload process.  We apologize for the inconvenience. Please try again.
            </p>';
        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'upload-results']
        );
    }

    public function handleRequest($i, $nextStep)
    {
        if (!$nextStep) {
            // FIXME this is strange
            if ($this->request->query->get('error') == 1) {
                return $this->errorPage();
            }
        }

        if (!$i) {
            return $this->startNewUpload(
                $this->request->query->get('tmpl'),
                $this->request->query->get('mystery')
            );
        }

        $formData = $this->getFormData($i);
        $title = _('Upload Your Recording');

        if ($nextStep && !$formData) {
            // we've apparently arrived here by navigating backward via history or
            // something so that we're visiting a step of the upload procedure but
            // without any valid session form data
            $message = '<h1>' . _('Expired Upload') . '</h1>
                <p>' . _(
                    'This upload has expired. Either it has already been added to the database or an unrecoverable error has occurred.'
                ) . '</p>';
            $this->badRequest($message);
        }

        $step = null;
        switch ($nextStep) {
            case UploadStep::LOCATION:
                $step = new UploadStepLocation($formData);
                break;

            case UploadStep::DETAILS:
                $step = new UploadStepDetails($formData);
                break;

            case UploadStep::VERIFY:
                $step = new UploadStepVerify($formData);
                break;
        }

        $output = $this->contentIntro($nextStep, $title);
        $output .= $step->content($this->request);
        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => $step->pageId()]
        );
    }

    protected function startNewUpload($tmpl, $mystery = false)
    {
        $i = nextSessionCounter();
        $formDataVar = $this->formDataVar($i);

        $rec = null;
        if ($tmpl) {
            $rec = Recording::load($tmpl);
        }

        if ($rec) {
            $formData = RecordingFormData::fromRecording($rec);
        } else {
            $formData = new RecordingFormData();
        }

        if ($mystery) {
            $formData->speciesNr = Species::mysterySpeciesNumber();
        }

        // FIXME: use symfony sessions, but first we need to re-work things so
        // that we don't need to maintain a refernce to the session superglobal
        $_SESSION[$formDataVar] = $formData;

        // finished setting up the session data, now enter the location
        return $this->proceed(UploadStep::LOCATION, $i);
    }

    protected function formDataVar($i)
    {
        return "upload-form-data-$i";
    }

    protected function proceed($step, $id)
    {
        $url = getUrl('upload', ['i' => $id, 'step' => $step]);
        return $this->seeOther($url);
    }

    protected function getFormData($i)
    {
        if (!$i) {
            die('Internal Error: trying to get form data without an upload session.');
        }
        // this needs to be a reference, and we need to pass it by reference to the
        // various validate functions so that the session variable gets updated in-place
        // with the submitted for data.
        $formData = &$_SESSION[$this->formDataVar($i)];

        if (!$formData) {
            $username = User::current()->userName();
            XC_logger()->logWarn(
                "No form data for upload '$i' for user '$username'"
            );
        }

        return $formData;
    }

    protected function contentIntro($step, $title = 'FIXME')
    {
        $html = "<h1>$title</h1>";

        $stepClasses = [
            UploadStep::LOCATION => '',
            UploadStep::DETAILS => '',
            UploadStep::VERIFY => '',
        ];

        $stepClasses[$step] = "class='current'";

        $html .= "
            <ul class='breadcrumbs'>
            <li {$stepClasses[UploadStep::LOCATION]}>" . _('Recording file + basic metadata') . "</li>
            <li {$stepClasses[UploadStep::DETAILS]}>" . _('Sound details') . "</li>
            <li {$stepClasses[UploadStep::VERIFY]}>" . _('Verify and submit') . '</li>
            </ul>';

        return $html;
    }

    public function handlePost($i, $lastStep)
    {
        if (!$i) {
            return $this->badRequest('Trying to upload without a valid upload session');
        }

        $formData = $this->getFormData($i);

        switch ($lastStep) {
            case UploadStep::LOCATION:
                $step = new UploadStepLocation($formData);
                if (!$step->validate($this->request)) {
                    return $this->proceed(UploadStep::LOCATION, $i);
                } else {
                    return $this->proceed(UploadStep::DETAILS, $i);
                }
                break;

            case UploadStep::DETAILS:
                $step = new UploadStepDetails($formData);
                if (!$step->validate($this->request)) {
                    return $this->proceed(UploadStep::DETAILS, $i);
                } else {
                    return $this->proceed(UploadStep::VERIFY, $i);
                }
                break;

            case UploadStep::VERIFY:
                $step = new UploadStepVerify($formData);
                if (!$step->validate($this->request)) {
                    return $this->proceed(UploadStep::VERIFY, $i);
                } else {
                    if ($this->request->request->get('edit')) {
                        return $this->proceed(UploadStep::LOCATION, $i);
                    } else {
                        $id = self::finishUpload($formData, User::current()->userId());
                        unlink($formData->filename);

                        // clear session data for next upload
                        $_SESSION[$this->formDataVar($i)] = null;
                        if ($id) {
                            return $this->seeOther(getUrl('upload-success', ['xcid' => $id]));
                        } else {
                            return $this->seeOther(getUrl('upload-error'));
                        }
                    }
                }
                break;
        }
    }

}
