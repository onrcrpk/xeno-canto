<?php

namespace xc\Controllers;

use xc\Query;
use xc\QueryParser;
use xc\Species;
use xc\User;
use xc\XCRedis;

use function xc\escape;
use function xc\getUrl;
use function xc\printErrorMessages;
use function xc\sanitize;
use function xc\XC_logger;
use function xc\XC_makeViewLinks;
use function xc\XC_pageNumber;
use function xc\XC_pageNumberNavigationWidget;
use function xc\XC_resultsSummary;
use function xc\XC_resultsTableForView;

class Browse extends Controller
{

    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public function handlePost()
    {
        $species_nr = $this->request->request->get('species_nr');
        if ($species_nr) {
            $sp = Species::load($species_nr);
            if ($sp) {
                return $this->seeOther($sp->profileURL());
            }
        }

        $query = $this->request->request->get('query');
        $params = [];
        if ($query) {
            $params['query'] = $query;
        }

        return $this->seeOther(getUrl('browse', $params));
    }

    public function handleRequest()
    {
        $pagenumber = XC_pageNumber($this->request);
        $query = $this->request->query->get('query');
        $num_per_page = defaultNumPerPage();
        $view = $this->request->query->get('view', defaultResultsView());

        $this->redis->setKeyByRequest($this->redisKey("recording-browse-$num_per_page-$view"), $this->request);

        if (!empty($query)) {
            $title = sprintf(_("Results for '%s'"), sanitize($query)) . ' :: ' . sprintf(_('page %d'), $pagenumber);
        } else {
            $title = _('Browse Recordings') . ' :: ' . sprintf(_('page %d'), $pagenumber);
        }

        $output = $this->redis->get();

        if (!$output) {
            $output = '<h1>' . _('Recordings') . '</h1>';
            $q = new Query($query);

            $invalid = $q->invalidTaggedQueries();

            if (!empty($invalid)) {
                if (in_array(reset($invalid), QueryParser::$tagNames)) {
                    $output .= printErrorMessages(_('Trouble parsing search tags'), [
                        sprintf(
                            _(
                                "Cannot succesfully parse the search term %s. When searching specifically for an operator (%s, %s, %s), please add this as the last tag to your search string, %s."
                            ),
                            '<strong><tt>' . sanitize($query) . '</tt></strong>',
                            "'='",
                            "'&lt;'",
                            "'&gt;'",
                            "e.g. <strong><tt>len:\"&lt;5\" rmk:=</tt></strong>"
                        )
                    ]);
                } else {
                    $output .= printErrorMessages(_('Invalid search term'), [
                        sprintf(
                            _(
                                "Your search term %s is invalid. When using operators (%s, %s, %s), the operator plus search term should be escaped between double quotes, e.g. %s or %s."
                            ),
                            '<strong><tt>' . sanitize($query) . '</tt></strong>',
                            "'='",
                            "'&lt;'",
                            "'&gt;'",
                            '<strong><tt>cnt:"=Niger"</tt></strong>',
                            '<strong><tt>q:">B"</tt></strong>'
                        )
                    ]);
                }
            } else {
                //$q->useLocalNameSearch();
                $q->setOrder(escape($this->request->query->get('order')), escape($this->request->query->get('dir')));
                $number_of_hits = $q->numRecordings();
                $num_species = $q->numSpecies();

                if (!$this->showLocations($query)) {
                    $output .= "
                <p class='important'>" . _(
                            'Locations are always hidden when searching for background species, as these species may be restricted'
                        ) . ". <a href='" . getUrl('FAQ') . "#restricted'>" . _('Explain this.') . '</a></p>';
                }

                if ($number_of_hits) {
                    $no_pages = ceil($number_of_hits / $num_per_page);
                    $start_no_shown = ($pagenumber - 1) * $num_per_page + 1;

                    // now actually find the results we want to show
                    $result = $q->execute($pagenumber);
                    if (!$result) {
                        XC_logger()->logError("ERROR: cannot execute query: " . mysqli()->error);
                    }

                    $end_no_shown = $start_no_shown + ($result->num_rows - 1);
                    $output .= XC_resultsSummary($number_of_hits, $num_species, $start_no_shown, $end_no_shown, $query);
                    $output .= "
                <ul class=\"results-actions\">";

                    if (!empty($query) && User::current()) {
                        $output .= "
                            <li>
                            <a href='" . getUrl('export-csv', ['query' => $query]) . "'>" . _(
                                'Export all results as a CSV (spreadsheet) file'
                            ) . '</a> (' . sprintf(_('maximum %d results'), MAX_EXPORT_RECORDS) . ')
                            </li>';
                        if ($this->showLocations($query)) {
                            $output .= "
                            <li>
                            <a href='" . getUrl('locations', ['query' => $query]) . "'>" . _(
                                    'Make a map of all locations of the current selection'
                                ) . '</a>
                            </li>';
                        }
                        $output .= "
                            <li>
                            <a href='" . getUrl('set-add-query', ['query' => $query]) . "'>" . _(
                                'Add all results to a recording set'
                            ) . '</a>
                            </li>';
                    }

                    $links = XC_makeViewLinks($this->request, $view);
                    $output .= "
                <li>
                <div class=\"results-format\">" . _('Results format') . ": $links </div>
                </li>
                </ul>";

                    $output .= XC_pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
                    $output .= XC_resultsTableForView(
                        $this->request,
                        $result,
                        $view,
                        null,
                        $this->showLocations($query)
                    );
                    $output .= XC_pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
                } else {
                    if ($query) {
                        $message = sprintf(
                            _("No recordings were found that match the search term '%s'."),
                            '<strong>' . sanitize($query) . '</strong>'
                        );
                    } else {
                        $message = _('One or more search terms are required!');
                    }

                    $output .= printErrorMessages(_('No recordings found'), [
                        $message . '  ' . sprintf(
                            _("To improve your search please read the <a href='%s'>search tips</a> page."),
                            getUrl('tips')
                        ),
                    ]);
                }
            }
            $this->redis->set($output, 3600);
        }

        return $this->template->render($output, ['title' => $title]);
    }

    private function showLocations($query)
    {
        return true;

        // XC-94 reverted
//        $pq = new QueryParser($query);
//        $r = $pq->parse();
//        foreach ($r->taggedQueries as $tag) {
//            if (strpos($tag->tagName, 'also') === 0) {
//                return false;
//            }
//        }
//        return true;
    }

}
