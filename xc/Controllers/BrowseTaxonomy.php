<?php

namespace xc\Controllers;

use function xc\escape;
use function xc\getUrl;
use function xc\printErrorMessages;
use function xc\XC_extinctSymbol;
use function xc\XC_logger;


class BrowseTaxonomy extends Controller
{

    public function handleRequest()
    {
        $title = _('Browse by Taxonomy');
        $output = "<h1>$title</h1>";

        $group = strip_tags($this->request->query->get('grp'));
        $order = strip_tags($this->request->query->get('ord') ?? $this->request->query->get('o'));
        $family = strip_tags($this->request->query->get('fam') ?? $this->request->query->get('f'));
        $genus = strip_tags($this->request->query->get('gen') ?? $this->request->query->get('g'));

        if (!empty($genus)) {
            $g = escape($genus);
            $localName = localNameLanguage();
            $sql = "
                SELECT T.order, T.family, T.family_english, T.genus, T.species, T.eng_name, species_nr, T.recordings, 
                       cnames.$localName AS localName, T.extinct, T.authority 
                FROM taxonomy T 
                LEFT JOIN taxonomy_multilingual cnames USING(species_nr) 
                WHERE T.genus='$g' 
                ORDER BY CASE WHEN T.group_id = 1 THEN IOC_order_nr ELSE T.species END ASC";
            $result = query_db($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= printErrorMessages(_('Error!'), [sprintf(_("No results for genus '%s'"), $genus)]);
            } else {
                $output .= "
                    <ul class='tree'>
                    <li><a href='" . getUrl('browse-taxonomy') . "'>" . _('Groups') . "...</a>
                    <ul>
                    <li>" . $this->makeOrderLink($row) . '
                    <ul>
                    <li>' . $this->makeFamilyLink($row) . '
                    <ul>
                    <li>' . $this->makeGenusLink($row) .
                    " (<a href='" . getUrl('genus', ['genus' => $row->genus]) . "'>" . _('genus overview') . '</a>)' .
                    $this->printSpeciesList($result) . '
                    </li>
                    </ul>
                    </li>
                    </ul>
                    </li>
                    </ul>
                    </li>
                    </ul>';
            }
        } elseif (!empty($family)) {
            $f = escape($family);
            $sql = "
                SELECT taxonomy.order, family, family_english, genus, sum(recordings) AS recordings, 
                    (COUNT(species_nr)-SUM(extinct)) AS nextant
                FROM taxonomy 
                WHERE family='$f' 
                GROUP BY genus 
                ORDER BY CASE WHEN group_id = 1 THEN IOC_order_nr ELSE genus END ASC";
            $result = query_db($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= printErrorMessages(_('Error!'), [sprintf(_("No results for family '%s'"), $family)]);
            } else {
                $output .= "
                    <ul class='tree'>
                    <li><a href='" . getUrl('browse-taxonomy') . "'>" . _('Groups') . "...</a>
                    <ul>
                    <li>" . $this->makeOrderLink($row) . '
                    <ul>
                    <li>' . $this->makeFamilyLink($row) . $this->printGenusList($result) . '</li>
                    </ul>
                    </li>
                    </ul>
                    </li>
                    </ul>';
            }
        } elseif (!empty($order)) {
            $o = escape($order);
            $sql = "
                SELECT taxonomy.order, family, family_english, sum(recordings) as recordings, 
                    (COUNT(species_nr)-SUM(extinct)) as nextant 
                FROM taxonomy 
                WHERE taxonomy.order='$o' 
                GROUP BY family 
                ORDER BY CASE WHEN group_id = 1 THEN IOC_order_nr ELSE family END ASC";
            $result = query_db($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= printErrorMessages(_('Error!'), [sprintf(_("No results for order '%s'"), $order)]);
            } else {
                $output .= "
                    <ul class='tree'>
                    <li><a href='" . getUrl('browse-taxonomy') . "'>" . _('Groups') . "...</a>
                    <ul>
                    <li>" . $this->makeOrderLink($row) . $this->printFamilyList($result) . '</li>
                    </ul>
                    </li>
                    </ul>';
            }
        } elseif (!empty($group)) {
            $g = escape($group);
            $sql = "
                SELECT t1.order, sum(recordings) as recordings, t1.group_id,
                    (COUNT(species_nr)-SUM(extinct)) as nextant 
                FROM taxonomy AS t1
                LEFT JOIN groups AS t2 ON t1.group_id = t2.id
                WHERE t2.name = '$g'
                GROUP BY t1.order 
                ORDER BY " . ($g == 'birds' ? 't1.IOC_order_nr' : 't1.order');
            $result = query_db($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= printErrorMessages(_('Error!'), [sprintf(_('No results for group %s'), $group)]);
            } else {
                $output .= $this->printOrderList($result);
            }
        } else {
            $sql = "
                SELECT t2.id AS group_id, t2.name AS group_name, sum(recordings) AS recordings, 
                    (COUNT(species_nr)-SUM(extinct)) AS nextant 
                FROM taxonomy t1
                LEFT JOIN groups AS t2 ON t1.group_id = t2.id
                GROUP BY t1.group_id 
                ORDER BY t2.id ASC";
            $result = query_db($sql);
            $row = $result->fetch_object();
            if (!$row) {
                $output .= printErrorMessages(_('Error!'), [_('Internal Database Error')]);
            } else {
                $output .= $this->printGroupList($result);
            }
        }

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'taxonomy']
        );
    }

    private function makeOrderLink($row)
    {
        return "<a href='" . getUrl('browse-taxonomy', ['ord' => $row->order]) . "'>$row->order</a>";
    }

    private function makeFamilyLink($row)
    {
        return "<a href='" . getUrl('browse-taxonomy', ['fam' => $row->family]) . "'>{$row->family}</a>" .
            ($row->family_english ? " ($row->family_english)" : '');
    }

    private function makeGenusLink($row)
    {
        return "<a href='" . getUrl('browse-taxonomy', ['gen' => $row->genus]) . "'>$row->genus</a>";
    }

    private function printSpeciesList($res)
    {
        $res->data_seek(0);

        $text = '<ul>';
        while ($row = $res->fetch_object()) {
            $class = '';
            if ($row->extinct) {
                $class = "class='extinct'";
            }
            $text .= "<li $class>" . $this->makeSpeciesLink($row) .
                $this->printSubspeciesList($row->species_nr) . '</li>';
        }
        $text .= '</ul>';

        return $text;
    }

    private function makeSpeciesLink($row)
    {
        $cname = $row->localName;
        if (!$cname) {
            $cname = $row->eng_name;
        }
        $extinct = '';
        if ($row->extinct) {
            $extinct = ' ' . XC_extinctSymbol();
        }
        $href = getUrl('species', ['genus' => $row->genus, 'species' => $row->species]);
        if ($cname) {
            $link = "<a href='$href'><span class='common-name'>$cname</span></a> &middot; <span class='sci-name'>$row->genus {$row->species}</span>";
        } else {
            $link = "<a href='$href'><span class='sci-name'>$row->genus $row->species</span></a> ";
        }
        return $link . "$extinct &middot; <span class='authority'>$row->authority</span> <span class='recording-count'>{$row->recordings}</span>";
    }

    private function printSubspeciesList($species_nr)
    {
        $res = query_db(
            "SELECT S.ssp, S.author, extinct, COUNT(B.ssp) as nrecs FROM taxonomy_ssp S LEFT JOIN birdsounds B USING(species_nr, ssp) WHERE species_nr='$species_nr' GROUP BY S.ssp"
        );
        if (!$res) {
            XC_logger()->logWarn(mysqli()->error);
        }
        if ($res->num_rows == 0) {
            return '';
        }

        $html = '<ul>';
        while ($row = $res->fetch_object()) {
            $class = 'ssp';
            $extinct = '';
            if ($row->extinct == 1) {
                $extinct = ' ' . XC_extinctSymbol();
                $class .= ' extinct';
            }
            $html .= "<li class='$class'><strong>$row->ssp</strong>$extinct &middot; <span class='authority'>$row->author</span> <span class='recording-count'>$row->nrecs</span></li>";
        }
        $html .= '</ul>';

        return $html;
    }

    private function printGenusList($res)
    {
        $res->data_seek(0);

        $text = "<ul>";
        while ($row = $res->fetch_object()) {
            $class = '';
            $extinct = '';
            if ($row->nextant == 0) {
                $extinct = ' ' . XC_extinctSymbol();
                $class = "class='extinct'";
            }
            $text .= "<li $class>" . $this->makeGenusLink($row) . $extinct;
            if (isset($row->recordings)) {
                $text .= " <span class='recording-count'>$row->recordings</span>";
            }
            $text .= '</li>';
        }
        $text .= '</ul>';

        return $text;
    }

    private function printFamilyList($res)
    {
        $res->data_seek(0);

        $text = "<ul>";
        while ($row = $res->fetch_object()) {
            $class = '';
            if ($row->nextant == 0) {
                $class = "class='extinct'";
            }
            $text .= "<li $class>" . $this->makeFamilyLink($row);
            if (isset($row->recordings)) {
                $text .= " <span class='recording-count'>$row->recordings</span>";
            }
            $text .= '</li>';
        }
        $text .= '</ul>';

        return $text;
    }

    private function printOrderList($res)
    {
        $res->data_seek(0);

        $text = "<ul class='tree'>
             <li><a href='" . getUrl('browse-taxonomy') . "'>" . _('Groups') . "...</a></li>
             <ul>";
        while ($row = $res->fetch_object()) {
            // Ignore the 'mystery' order, which is blank
            if (!empty($row->order)) {
                $class = '';
                if ($row->nextant == 0) {
                    $class = "class='extinct'";
                }
                $text .= "<li $class>" . $this->makeOrderLink($row);
                if (isset($row->recordings)) {
                    $text .= " <span class='recording-count'>{$row->recordings}</span>";
                }
                $text .= '</li>';
            }
        }
        $text .= '</ul></ul>';

        return $text;
    }

    private function printGroupList($res)
    {
        $res->data_seek(0);

        $text = '<ul>';
        while ($row = $res->fetch_object()) {
            // Ignore the 'mystery' order, which is blank
            if (!empty($row->group_name)) {
                $class = '';
                if ($row->nextant == 0) {
                    $class = "class='extinct'";
                }
                $text .= "<li $class>" . $this->makeGroupLink($row);
                if (isset($row->recordings)) {
                    $text .= " <span class='recording-count'>$row->recordings</span>";
                }
                $text .= '</li>';
            }
        }
        $text .= '</ul>';

        return $text;
    }

    private function makeGroupLink($row)
    {
        $group = strtoupper($row->group_name);
        return "<a href='" . getUrl('browse-taxonomy', ['grp' => $row->group_name]) . "'>$group</a>";
    }

}
