<?php

namespace xc\Controllers;

use xc\Species;

use function xc\getUrl;
use function xc\XC_speciesListForLocation;

class LocationMap extends Controller
{

    public function handleRequest()
    {
        $loc = strip_tags($this->request->query->get('loc'));

        if (!$loc) {
            return $this->notFound();
        }

        $lat = floatval($this->request->query->get('lat'));
        $long = floatval($this->request->query->get('long'));
        $hasCoordinates = $lat && $long;
        $jsLoc = json_encode($loc);
        $apiUrl = 'https://maps.googleapis.com/maps/api/js?language=en&amp;key=' . GOOGLE_MAPS_GEOCODING_KEY;

        $script = <<<EOT
            <script type="text/javascript" src="$apiUrl"></script>
            <script type="text/javascript">
            jQuery(document).ready(function() {
            var pos = new google.maps.LatLng($lat, $long)
            var mapOptions = {
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                zoom: 8,
                center: pos
            };
            var gmap = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var markerOptions = {
                position: pos,
                map: gmap,
                title: $jsLoc,
                animation: google.maps.Animation.DROP
            }
            var marker = new google.maps.Marker(markerOptions);
            });
            </script>
            EOT;

        $output = "<h1>$loc</h1>
            <section id='map-column' class='column'>";

        if ($hasCoordinates) {
            $output .= "<div id='map-canvas'></div>
            <p>The marker shows the location of <b>$loc</b>, at ($lat, $long)</p>
            <p>Zoom in and out with the buttons. Click on the map and drag it around.</p>";
        } else {
            $output .= '<p>No coordinates were given for this location</p>';
        }

        $output .= "</section>
            <section class='column'>";

        $fgSpecies = [];
        $bgSpecies = [];
        XC_speciesListForLocation($loc, $fgSpecies, $bgSpecies);

        if (!empty($fgSpecies)) {
            $output .= "<p>Species <a href='" . getUrl('browse', ['query' => "loc:\"$loc\""]) . "'>recorded</a> at <em>$loc</em>:</p>
                <ul>";

            foreach ($fgSpecies as $sp) {
                // FIXME: ENVREC
                if ($sp['nr'] == Species::soundscapeSpeciesNumber()) {
                    continue;
                }

                $output .= "<li><a href='" . getUrl('browse', ['query' => "sp:{$sp['nr']} loc:\"$loc\""]
                    ) . "'>{$sp['commonName']}</a>";
                if ($sp['uncertain']) {
                    $output .= ' (?)';
                }
                $output .= '</li>';
            }
            $output .= '</ul>';
        }

        if (!empty($bgSpecies)) {
            $output .= '<p>And also the following species in the background:</p>
                <ul>
                ';
            foreach ($bgSpecies as $sp) {
                $output .= "<li>{$sp['commonName']}</li>";
            }
            $output .= '</ul>';
        }

        if ($hasCoordinates) {
            $query_near = 'box:' . ($lat - 1 / 2) . ',' . ($long - 1 / 2) . ',' . ($lat + 1 / 2) . ',' . ($long + 1 / 2);
            $output .= "<p>
            <a href='" . getUrl('browse', ['query' => $query_near]) . "'>Search</a> for recordings made near to this location
            </p>
            </section>";
            $output .= $script;
        }

        return $this->template->render($output, ['title' => $loc, 'bodyId' => 'maps']);
    }
}
