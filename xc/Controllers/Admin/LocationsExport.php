<?php

namespace xc\Controllers\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class LocationsExport extends Controller
{

    public function __construct($request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        $fileName = 'gpslist_' . date('Y-m-d') . '_' . date('H-i-s') . '.txt';

        $fh = fopen('php://output', 'w');
        ob_start();

        // Run in batches as apparently not enough memory for full query
        $res   = query_db('select count(1) as total from birdsounds');
        $row   = $res->fetch_object();
        $total = $row->total;
        $batch = 50000;

        for ($i = 0; $i < $total; $i += $batch) {
            $res = query_db(
                "
                select genus, species, ssp, recordist, dir, snd_nr, latitude, longitude, location, country, background 
                from birdsounds 
                limit $i, $batch"
            );
            while ($row = $res->fetch_object()) {
                //fwrite($fh, "$row->snd_nr;$row->genus;$row->species;$row->ssp;$row->recordist;$row->dir;$row->latitude;$row->longitude\n");
                fputcsv(
                    $fh,
                    [
                        $row->snd_nr,
                        $row->genus,
                        $row->species,
                        $row->ssp,
                        $row->recordist,
                        $row->dir,
                        $row->latitude,
                        $row->longitude,
                        $row->location,
                        $row->country,
                        $row->background,
                    ],
                    ';'
                );
            }
        }

        $stream = ob_get_clean();
        fclose($fh);

        // Return a response with a specific content
        $response    = new Response($stream);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
