<?php

/**
 * @todo
 *
 * - copy data to real tables (needs group_id in tables)
 * - fix IOC_order_nr (primary key now decimal?!), determine start number for groups
 */

namespace xc\Controllers\Admin;

use Symfony\Component\Finder\Finder;

use function xc\escape;
use function xc\getUrl;


class DwcaUpdate extends Controller
{

    protected $minioPath;
    protected $error;
    protected $errorStyle = "style='color: red; font-weight: bold;'";
    protected $speciesNrs = [];
    private $files = [];
    private $dwca = [
        'Taxon.tsv' => '_dwca_taxon',
        'VernacularName.tsv' => '_dwca_vernacular'
    ];
    private $fields = [];
    private $languageColumns = [
        'eng' => 'english',
        'deu' => 'german',
        'fra' => 'french',
        'spa' => 'spanish',
        'jpn' => 'japanese',
        'dut' => 'dutch',
        'por' => 'portuguese',
        'zho' => 'chinese'
    ];

    public function __construct($request)
    {
        parent::__construct($request);
        $this->minioPath = $this->checkFilePath(MINIO_DWCA);
    }

    public function index()
    {
        $this->error = $this->bootstrap();
        $output = "
            <h1>Classification Update</h1>
            <div style='width: 700px;'>";

        if (!$this->error) {
            $select = $this->dwcaSelect();
            $output .= "
                <p>Use this script to update the classification through a DarwinCore Archive (DwCA) downloaded
                from the <a href='https://catalogueoflife.org'>Catalogue of Life Checklist</a>.</p>
                <p>Preparing an update:</p>
                <ol>
                    <li>Download a DarwinCore archive of the latest 
                    <a href='https://data.catalogueoflife.org/dataset?limit=50&offset=0&releasedFrom=3'>Catalogue of Life Checklist</a>.
                    You will need a GBIF account to download archives, so create one if necessary.</li>
                    <li>On the download page, select '<strong>dwca</strong>' as format and <strong>uncheck 
                    the option 'Include synonyms'</strong>. Synonyms will be stripped from the archive anyway, so these will only
                    inflate the download and slow down the conversion. Make sure the <strong>root level taxon</strong> corresponds
                    to the group you want to import (e.g. Orthoptera, Chiroptera).</li>
                    <li>Unzip the archive and copy the following files to the bucket <b>dwca</b> in <b>Minio</b>:
                        <ul>
                            <li><strong>Taxon.tsv</strong></li>
                            <li><strong>VernacularName.tsv</strong></li>
                        </ul>
                    </li>
                </ol>
                <form method='get' action='" . getUrl('admin-dwca-prepare') . "'>
                <p style='margin-top: 30px;'>Select a group from the meu and click the link below to start a test run. 
                It may take several minutes before the script is ready and any output appears on screen.</p>
                <p style='margin-top: 20px;'> 
                $select <input type='submit' value='Prepare import' />
                </p>
                </form>
                </div>";
        } else {
            $output = "<p>$this->error</p></div>";
        }

        return $this->template->render($output);
    }

    protected function bootstrap()
    {
        // Check if path to Minio server is correct
        if (file_exists($this->minioPath)) {
            $finder = new Finder();
            $this->files = $finder->files()->in($this->minioPath);
        } else {
            return "<b>Minio</b> is not set at " . MINIO_DWCA;
        }
        // Any files present?
        if (empty($this->files)) {
            return "Cannot locate any files in <b>Minio</b>, please verify that the path '" . $this->minioPath . "' is configured correctly!";
        }
    }

    protected function dwcaSelect()
    {
        $select = "<select name='group_id'>\n";
        $res = query_db("SELECT id, name FROM groups WHERE dwca = 1");
        while ($row = $res->fetch_object()) {
            $select .= "<option value='$row->id'>$row->name</option>\n";
        }
        $select .= "</select>\n";
        return $select;
    }

    public function replace($id)
    {
        $output = '<h1>Classification Update</h1><p></p>';
        $this->setGroupId($id);

        if (!$this->getGroupId()) {
            return $this->template->render($output . "group_id is missing" . '</p>');
        }

        // First verify that IOC numbers (primary key) are unique!
        $res = query_db("SELECT species_nr FROM taxonomy WHERE IOC_order_nr IN (SELECT IOC_order_nr FROM _taxonomy)");
        if ($res->num_rows > 0) {
            $error = "
                <p $this->errorStyle>" . $res->num_rows . " conflicting IOC numbers, cannot proceed!</p>
                <p>Please contact Naturalis to fix this issue, as the script needs an update.</p>";
            return $this->template->render($output . $error);
        }

        $queries = [
            "DELETE FROM taxonomy WHERE group_id = " . $this->groupId(),
            "DELETE FROM taxonomy_multilingual WHERE group_id = " . $this->groupId(),
            "DELETE FROM taxonomy_ssp WHERE group_id = " . $this->groupId(),
            "INSERT INTO taxonomy (SELECT * FROM _taxonomy)",
            "INSERT INTO taxonomy_multilingual (SELECT * FROM _taxonomy_multilingual)",
            "INSERT INTO taxonomy_ssp (SELECT * FROM _taxonomy_ssp)",
        ];
        foreach ($queries as $q) {
            query_db($q);
        }
        $output .= "<p>Successfully updated the classification!</p>";

        return $this->template->render($output);
    }

    protected function setGroupId($id)
    {
        $this->groupId = $id;
    }

    protected function getGroupId()
    {
        if (!$this->groupId) {
            $this->groupId = $this->request->query->get('group_id');
        }
        return $this->groupId;
    }

    public function compare($id)
    {
        $output = '<h1>Classification Update</h1><p>';
        $this->setGroupId($id);

        if (!$this->getGroupId()) {
            return $this->template->render($output . "group_id is missing" . '</p>');
        }

        $added = $this->printComparison(
            "(SELECT genus, species, NULL AS ssp, family 
            FROM _taxonomy 
            WHERE species_nr NOT IN (SELECT species_nr FROM taxonomy WHERE group_id = " . $this->groupId() . "))

            UNION
            
            (SELECT genus, species, ssp, family 
            FROM _taxonomy_ssp
            WHERE species_nr NOT IN (SELECT species_nr FROM taxonomy_ssp WHERE group_id = " . $this->groupId() . "))
            
            ORDER BY genus, species, ssp"
        );
        $dropped = $this->printComparison(
            "
            (SELECT genus, species, NULL AS ssp, family 
            FROM taxonomy 
            WHERE species_nr NOT IN (SELECT species_nr FROM _taxonomy) AND group_id = " . $this->groupId() . ")
            
            UNION
            
            (SELECT genus, species, ssp, family 
            FROM taxonomy_ssp 
            WHERE species_nr NOT IN (SELECT species_nr FROM _taxonomy_ssp) AND group_id = " . $this->groupId() . ")
             
            ORDER BY genus, species, ssp"
        );

        if ($dropped) {
            $output .= "
                <p $this->errorStyle>The classification cannot be automatically replaced, as there are
                taxa that were synonimised, renamed or lumped.</p>
                <p>Taxa that would be dropped from the current classification:</p>" . $dropped;
        }
        if ($added) {
            $output .= "<p>Taxa not present in the current classification:</p>" . $added;
        }
        if ($added && !$dropped) {
            $output .= '
                <p style="margin-top: 30px;">
                <form action="' . getUrl('admin-dwca-replace', ['group_id' => $this->groupId()]) . '">
                <input type="submit" value="Replace classification">
                </form>';
        }

        return $this->template->render($output);
    }

    protected function printComparison($query)
    {
        $res = query_db($query);
        if ($res->num_rows > 0) {
            $output = '<ul>';
            while ($row = $res->fetch_object()) {
                $output .= "<li>{$row->genus} {$row->species} {$row->ssp} ({$row->family})</li>";
            }
            return $output . '</ul>';
        }
        return null;
    }

    public function prepare()
    {
        $output = '<h1>Classification Update</h1><p>';

        if (!$this->getGroupId()) {
            return $this->template->render($output . "group_id is missing" . '</p>');
        }

        // Convert DwCA
        $total = $this->importDwca();
        if (!$this->error) {
            $output .= "Imported {$total['Taxon.tsv']} taxa and {$total['VernacularName.tsv']} vernacular names to temporary tables<br>";
            if ($total['synonyms'] > 0) {
                $output .= "<span $this->errorStyle>The DwCA contained {$total['synonyms']} synonyms, which were deleted</span><br>";
            }
            if ($total['duplicates'] > 0) {
                $output .= "<span $this->errorStyle>Deleted {$total['duplicates']} duplicate accepted taxa</span><br>";
            }
        } else {
            return $this->template->render($output . $this->error . '</p>');
        }

        // Fill taxonomy table
        $total = $this->createSpecies();
        if (!$this->error) {
            $output .= "<br>Imported $total species<br>";
        } else {
            return $this->template->render($output . $this->error . '</p>');
        }

        // Fill taxonomy_ssp table
        $total = $this->createSubspecies();
        if (!$this->error) {
            $output .= "Imported $total subspecies <br>";
        } else {
            return $this->template->render($output . $this->error . '</p>');
        }

        // Fill taxononmy_multilingual table
        $total = $this->createMultilingual();
        if (!$this->error) {
            $output .= "Imported {$total['names']} non-English vernacular names for {$total['species']} species<br>";
        } else {
            return $this->template->render($output . $this->error . '</p>');
        }

        $output .= '<p style="margin-top: 30px;">
            <form action="' . getUrl('admin-dwca-compare', ['group_id' => $this->groupId()]) . '">
            <input type="submit" value="Compare with current classification">
            </form>';

        $output .= '</p>';

        return $this->template->render($output);
    }

    protected function importDwca()
    {
        ini_set('auto_detect_line_endings', true);
        $total = [];

        foreach ($this->dwca as $tsv => $table) {
            $file = $this->minioPath . $tsv;
            if (!file_exists($file)) {
                return $this->error = "File $file is missing <br>";
            } else {
                $i = 0;
                // Clear previous import
                query_db("TRUNCATE TABLE $table");
                // Create insert statement from csv file
                $fh = fopen($file, 'r');
                while (($line = fgetcsv($fh, 0, "\t")) !== false) {
                    // Set fields
                    if ($i == 0) {
                        $this->fields = [];
                        foreach ($line as $i => $field) {
                            $this->fields[$i] = substr($field, strrpos($field, ':') + 1);
                        }
                    } else {
                        $sql = "
                            INSERT INTO $table (`" . implode('`, `', $this->fields) . "`)
                            VALUES " . $this->escapeValues($line);
                        // Insert by line to properly detect errors (impossible in batches)
                        if (!query_db($sql)) {
                            $this->error = "Error importing data from $tsv: <pre>$sql</pre><br>";
                            return false;
                        }
                    }
                    $i++;
                }
                $total[$tsv] = $i;
            }
        }

        // Dump synonyms in case the archive contained them
        query_db("DELETE FROM _dwca_taxon WHERE taxonomicStatus IN('synonym', 'ambiguous synonym')");
        $total['synonyms'] = mysqli()->affected_rows;

        // Dump duplicate accepted taxa
        $res = query_db(
            "
            SELECT genericName, specificEpithet
            FROM _dwca_taxon 
            WHERE taxonRank = 'species' and taxonomicStatus = 'accepted'
            GROUP BY genericName, specificEpithet
            HAVING COUNT(genericName) > 1 and COUNT(specificEpithet) > 1"
        );
        $total['duplicates'] = mysqli()->affected_rows;
        if ($res) {
            while ($row = $res->fetch_object()) {
                query_db(
                    "
                    DELETE FROM _dwca_taxon 
                    WHERE genericName = '$row->genericName' and specificEpithet = '$row->specificEpithet'"
                );
            }
        }

        return $total;
    }

    protected function escapeValues($row)
    {
        // Quote and escape strings, replace empty strings with null
        $values = array_map(function ($value) {
            return $value === "" ? 'null' : (is_numeric($value) ? $value : "'" . escape($value) . "'");
        }, $row);
        return ' (' . implode(', ', $values) . ')';
    }

    protected function createSpecies()
    {
        $this->createImportTable('taxonomy');
        // Abuse avibase_id column to store DWCA taxonID, because why not
        query_db('ALTER TABLE `_taxonomy` ADD INDEX (`avibase_id`)');

        $query = "
            SELECT taxonID, parentNameUsageID, genericName, specificEpithet, scientificName, scientificNameAuthorship 
            FROM _dwca_taxon 
            WHERE taxonRank = 'species' and taxonomicStatus = 'accepted'
            ORDER BY(taxonID + 0)";
        $res = query_db($query);
        $total = $res->num_rows;
        $iocNr = ($this->groupId() + 1) * 20000;

        while ($row = $res->fetch_object()) {
            $tree = $this->getClassification($row->parentNameUsageID);

            $values = [
                isset($tree['order']) ? strtoupper(strtok($tree['order']['name'], ' ')) : '[undefined]',
                isset($tree['family']) ? strtok($tree['family']['name'], ' ') : '[undefined]',
                isset($tree['family']) ? $this->getEnglishName($tree['family']['id']) : '',
                $row->genericName,
                $row->specificEpithet,
                $this->getEnglishName($row->taxonID) ?? '',
                $iocNr,
                $this->getSpeciesNumber($row->taxonID, $row->genericName, $row->specificEpithet),
                $row->taxonID, // abuse avibase_id to store DWCA id
                $row->scientificNameAuthorship,
                0,
                0,
                $this->groupId()
            ];

            $insert = "
                INSERT INTO _taxonomy
                (
                    `order`,
                    family,
                    family_english,
                    genus,
                    species,
                    eng_name,
                    IOC_order_nr,
                    species_nr,
                    avibase_id,
                    authority,
                    recordings,
                    back_recordings,
                    group_id
                )
                VALUES " . $this->escapeValues($values);
            if (!query_db($insert)) {
                $this->error = "Error inserting species: <pre>$insert</pre><br> ";
                return false;
            }
            $iocNr += 0.01;
        }
        return $total;
    }

    protected function createImportTable($table)
    {
        query_db("DROP TABLE if EXISTS _$table");
        query_db("CREATE TABLE _$table LIKE $table");
    }

    protected function getClassification($parentId, &$tree = [])
    {
        $parent = $this->getTaxon($parentId);

        if (isset($parent->taxonRank)) {
            $tree[$parent->taxonRank] = ['id' => $parent->taxonID, 'name' => $parent->scientificName];
            $this->getClassification($parent->parentNameUsageID, $tree);
        }

        return $tree;
    }

    protected function getTaxon($id)
    {
        $query = '
            SELECT taxonID, parentNameUsageID, taxonRank, scientificName 
            FROM _dwca_taxon 
            WHERE taxonID = "' . escape($id) . '"';
        $res = query_db($query);
        return $res ? $res->fetch_object() : false;
    }

    protected function getEnglishName($id)
    {
        $query = "
            SELECT vernacularName 
            FROM _dwca_vernacular 
            WHERE taxonID = '" . escape($id) . "' and language = 'eng'
            LIMIT 1";
        $res = query_db($query);
        $row = $res->fetch_object();
        return $row ? ucfirst($row->vernacularName) : null;
    }

    protected function getSpeciesNumber($id, $genus = false, $species = false)
    {
        if (isset($this->speciesNrs[$id])) {
            return $this->speciesNrs[$id];
        }

        $query = $genus && $species ? "SELECT species_nr FROM taxonomy WHERE genus = '$genus' and species = '$species'" : "SELECT species_nr FROM _taxonomy WHERE avibase_id = '$id'";
        $res = query_db($query);
        $row = $res->fetch_object();

        $speciesNr = $row ? $row->species_nr : $this->generateSpeciesNumber();
        $this->speciesNrs[$id] = $speciesNr;
        return $speciesNr;
    }

    protected function generateSpeciesNumber()
    {
        do {
            // makes a random alpha numeric string of a given length
            $letters = range('a', 'z');
            $out = '';
            for ($c = 0; $c < 6; $c++) {
                $out .= $letters[mt_rand(0, count($letters) - 1)];
            }
        } while ($this->speciesNumberExists($out));

        return $out;
    }

    protected function speciesNumberExists($species_nr)
    {
        if (in_array($species_nr, $this->speciesNrs)) {
            return true;
        }

        $res = query_db("SELECT species_nr FROM taxonomy WHERE species_nr = '$species_nr'");
        return $res && $res->num_rows == 1;
    }

    protected function createSubspecies()
    {
        $this->createImportTable('taxonomy_ssp');
        query_db(
            "
            ALTER TABLE `_taxonomy_ssp` CHANGE `family_english` `family_english` VARCHAR(255) CHARACTER SET utf8  COLLATE utf8_general_ci  null  default null"
        );

        $query = "
            SELECT t1.taxonID, t1.parentNameUsageID, t2.`order`, t2.family, t2.family_english, t2.genus, t2.species, t2.species_nr,
                t1.infraspecificEpithet, t1.scientificName, t1.scientificNameAuthorship
            FROM _dwca_taxon as t1
            LEFT JOIN _taxonomy as t2 ON t1.parentNameUsageID = t2.avibase_id
            WHERE t1.taxonRank = 'subspecies' and t2.`order` IS NOT NULL";
        $res = query_db($query);
        $total = $res->num_rows;

        while ($row = $res->fetch_object()) {
            //$tree = $this->getClassification($row->parentNameUsageID);

            $values = [
                $row->order,
                $row->family,
                $row->family_english ?? '',
                $row->genus,
                $row->species,
                $this->getEnglishName($row->taxonID),
                $row->species_nr,
                $row->infraspecificEpithet,
                $row->scientificNameAuthorship,
                $this->groupId()
            ];

            $insert = "
                INSERT INTO _taxonomy_ssp
                (
                    `order`,
                    family,
                    family_english,
                    genus,
                    species,
                    eng_name,
                    species_nr,
                    ssp,
                    author,
                    group_id
                )
                VALUES " . $this->escapeValues($values);
            if (!query_db($insert)) {
                $this->error = "Error inserting subspecies: <pre>$insert</pre><br>";
                return false;
            }
        }

        return $total;
    }

    protected function createMultilingual()
    {
        $this->createImportTable('taxonomy_multilingual');

        query_db(
            "
            INSERT INTO _taxonomy_multilingual (species_nr, genus, species, english, group_id)
            (
                SELECT species_nr, genus, species, eng_name, group_id FROM _taxonomy WHERE avibase_id IN
                (
                    SELECT DISTINCT t1.taxonID
                    FROM _dwca_vernacular as t1
                    LEFT JOIN _dwca_taxon as t2 ON t1.taxonID = t1.taxonID
                    WHERE t1.language != 'eng' and t2.taxonRank = 'species'
                )
            )"
        );
        $total['species'] = mysqli()->affected_rows;

        if ($total['species'] > 0) {
            $query = "
                SELECT DISTINCT t1.taxonID, t1.vernacularName, t1.language
                FROM _dwca_vernacular as t1
                LEFT JOIN _dwca_taxon as t2 ON t1.taxonID = t2.taxonID
                WHERE t1.language != 'eng' and t2.taxonRank = 'species'";
            $res = query_db($query);
            $total['names'] = $res->num_rows;

            while ($row = $res->fetch_object()) {
                $update = '
                    UPDATE _taxonomy_multilingual 
                    SET ' . $this->languageColumns[$row->language] . ' = "' . escape($row->vernacularName) . '" 
                    WHERE species_nr = "' . escape($this->getSpeciesNumber($row->taxonID)) . '"';
                if (!query_db($update)) {
                    return $this->error = "Error inserting common names: <pre>$update</pre><br> ";
                }
            }
            // Bummer...
        } else {
            $total['names'] = 0;
        }

        return $total;
    }
}
