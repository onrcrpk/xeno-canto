<?php

namespace xc\Controllers\Admin;

use xc\AudioUtil;

use function xc\getUrl;

class WriteId3 extends Controller
{

    // this is posted via AJAX, so no need to return anything
    public function generate()
    {
        $xcid = intval($this->request->request->get('XC'));
        $status = AudioUtil::writeId3($xcid);

        if (!$status) {
            return $this->internalServerError();
        }

        return json_encode(['error' => !$status]);
    }

    public function handleRequest()
    {
        $generateUrl = getUrl('admin-id3-post');
        $script      = <<<EOT
<script type='text/javascript'>
jQuery(document).ready(function() 
{
    var n = 0;
    jQuery("#form-id3").submit(function()
        {
            var xcid = jQuery("#form-id3 input[name=XC]").val();
            var id = "xc-" + n++;
            var status = jQuery("<p id='" + id + "'>Writing ID3 tags for XC" + xcid + "... <span class='result'></span></p>");
            jQuery("#status-log").append(status);
            jQuery.post("{$generateUrl}", {XC: xcid},
                function(data)
                {
                    var resultElt = jQuery("#" + id + " > .result");
                    resultElt.text("done");
                }).fail(function() {
                    var resultElt = jQuery("#" + id + " > .result");
                    resultElt.html("<span class='warning'>failed</span>");
                });
;
            return false;
        });
});
</script>

EOT;

        $output = "
            <h1>Re-write ID3 tags</h1>
            <form id='form-id3' method='post'>
            <input type='text' name='XC' placeholder='xcid' />
            <input type='submit' name='write' value='Write' />
            </form>
            <div id='status-log'></div>
            $script
            ";

        return $this->template->render(
            $output,
            ['title' => 'Re-write ID3 tags']
        );
    }
}
