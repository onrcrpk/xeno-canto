<?php

namespace xc\Controllers\Admin;

use xc\Query;
use xc\User;

use function xc\getUrl;
use function xc\notifySuccess;

class DisableUser extends Controller
{

    public function confirm($id)
    {
        $user = User::load($id);
        if (!$user) {
            return $this->notFound();
        }

        $body = "<h1>Disable User Account?</h1>
            <p>This will move the user account for <b>{$user->userName()}</b> into the <tt>spammers</tt> database table, which will disable the account but retain the user information for future use.</p>";
        if (!$this->canDisableUser($user)) {
            $body .= "<div class='error'>
                Cannot disable <b>{$user->userName()}</b> since they have uploaded
                recordings to xeno-canto.</div>";
        } else {
            $body .= "
                <form method='post'/>
                <input type='submit' value='Delete'/>
                </form>";
        }


        return $this->template->render(
            $body,
            ['title' => 'Disable User Account']
        );
    }

    public function disable($id)
    {
        $user = User::load($id);
        if (!$user) {
            return $this->notFound();
        }

        if (!$this->canDisableUser($user)) {
            return $this->badRequest(
                'Unable to disable user who has uploaded recordings'
            );
        }

        query_db(
            "INSERT INTO spammers SELECT username, email, dir, blurb, joindate, verified FROM users WHERE dir='$id'"
        );
        query_db("DELETE FROM users WHERE dir='$id'");
        query_db("DELETE FROM user_cookies WHERE userid='$id'");
        notifySuccess('Disabled user account');

        return $this->seeOther(getUrl('index'));
    }

    private function canDisableUser($user)
    {
        // check if this user has any recordings
        $q = new Query("dir:{$user->userId()}");
        return ($q->numRecordings() == 0);
    }
}
