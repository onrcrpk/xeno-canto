<?php

namespace xc\Controllers\Admin;

use finfo;
use xc\Query;
use xc\Recording;

class FileInfo extends Controller
{

    public function handleRequest()
    {
        $info = new finfo(FILEINFO_MIME_TYPE);
        $output = "
            <h1>Files with non-standard media info</h1>
            <p>This page lists files with information in the fft table that have information that looks strange.</p>
            <h2>No Media Information</h2>
            <p>The following files are missing media information (e.g. bitrate, length,
                sample rate).  This might be an indication that there's something wrong with the
                file (possibly permissions, filename issues, a wrong file format, or even a
                missing file).</p>

                <table class='results'>
                <thead>
                <tr>
                <th>XC#</th>
                <th>Common</th>
                <th>Latin</th>
                <th>Filename</th>
                <th>ID3 Format</th>
                <th>MIME Type</th>
                </tr>
                </thead>";
        $splitRes = query_db(
            "
                SELECT " . Query::birdsoundsSelectFields() . ", audio_info.format 
                FROM birdsounds 
                INNER JOIN audio_info USING(snd_nr) 
                WHERE length=0 OR bitrate=0 OR smp=0 OR format != 'mp3' 
                ORDER BY snd_nr ASC"
        );
        while ($row = $splitRes->fetch_object()) {
            $rec = new Recording($row);
            $missing = '';
            $mime = "<span class='unspecified'>none</span>";
            if (!file_exists($rec->filePath())) {
                $missing = "<span class='warning' style='display: inline-block'>FILE MISSING</span>";
            } else {
                $mime = $info->file($rec->filePath());
            }
            $output .= "
                <tr>
                <td><a href='{$rec->URL()}'>XC{$rec->xcid()}</a></td>
                <td>{$rec->commonName()}</td>
                <td><span class='sci-name'>{$rec->fullScientificName()}</span></td>
                <td><a href='{$rec->filePath()}'><tt>{$rec->filename()}</tt></a> $missing</td>
                <td><strong>{$row->format}</strong></td>
                <td><b>$mime</b></td>
        </tr>";
        }
        $output .= '</table>';

        return $this->template->render($output, ['title' => 'File Info']);
    }
}
