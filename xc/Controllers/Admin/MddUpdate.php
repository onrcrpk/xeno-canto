<?php

/**
 * @todo
 *
 * - copy data to real tables (needs group_id in tables)
 * - fix IOC_order_nr (primary key now decimal?!), determine start number for groups
 */

namespace xc\Controllers\Admin;

use Symfony\Component\Finder\Finder;

use function xc\escape;
use function xc\getUrl;


class MddUpdate extends Controller
{

    private $files = [];

    private $minioPath;

    private $error;

    private $errorStyle = "style='color: red; font-weight: bold;'";

    private $speciesNrs = [];

    private $mddCsv = 'mdd.csv';

    private $mddTable = '_mdd';

    private $keepKeys = [];

    private $keepFields = [
        'id',
        'phylosort',
        'sciName',
        'mainCommonName',
        'otherCommonNames',
        'order',
        'family',
        'genus',
        'subgenus',
        'specificEpithet',
        'authoritySpeciesAuthor',
        'authoritySpeciesYear',
        'authorityParentheses',
        'originalNameCombination',
        'iucnStatus',
        'extinct',
        'domestic'
    ];

    private $orderToExport = [
        3 => ['CHIROPTERA'],
    ];

    private $families = [];

    public function __construct($request)
    {
        parent::__construct($request);
        $this->minioPath = $this->checkFilePath(MINIO_DWCA);
    }

    public function index()
    {
        $this->error = $this->bootstrap();
        $output = "
            <h1>Classification Update</h1>
            <div style='width: 700px;'>";

        if (!$this->error) {
            $select = $this->mddSelect();
            $output .= "
                <p>Use this script to update the classification through a csv file downloaded
                from the <a href='https://www.mammaldiversity.org'>ASM Mammal Diversity Database (MDD)</a>.</p>
                <p>Preparing an update:</p>
                <ol>
                    <li><a href='https://www.mammaldiversity.org/assets/data/MDD.zip'>Download</a> the latest version of the MDD.</li>
                    <li>Unzip the archive, rename the csv to <strong>mdd.csv</strong> and copy the file to the bucket <b>dwca</b> in <b>Minio</b></li>
                </ol>
                <form method='get' action='" . getUrl('admin-mdd-prepare') . "'>
                <p style='margin-top: 30px;'>Select a group from the meu and click the link below to start a test run. 
                It may take several minutes before the script is ready and any output appears on screen.</p>
                <p style='margin-top: 20px;'> 
                $select <input type='submit' value='Prepare import' />
                </p>
                </form>
                </div>";
        } else {
            $output = "<p>$this->error</p></div>";
        }

        return $this->template->render($output);
    }

    private function bootstrap()
    {
        // Check if path to Minio server is correct
        if (file_exists($this->minioPath)) {
            $finder = new Finder();
            $this->files = $finder->files()->in($this->minioPath);
        } else {
            return "<b>Minio</b> is not set at " . MINIO_DWCA;
        }
        // Any files present?
        if (empty($this->files)) {
            return "Cannot locate any files in <b>Minio</b>, please verify that the path '" .
                $this->minioPath . "' is configured correctly!";
        }
    }

    private function mddSelect()
    {
        $select = "<select name='group_id'>\n";
        $res = query_db("SELECT id, name FROM groups WHERE dwca = 2");
        while ($row = $res->fetch_object()) {
            $select .= "<option value='$row->id'>$row->name</option>\n";
        }
        $select .= "</select>\n";
        return $select;
    }

    public function replace($id)
    {
        $output = '<h1>Classification Update</h1><p></p>';
        $this->setGroupId($id);

        if (!$this->getGroupId()) {
            return $this->template->render($output . "group_id is missing" . '</p>');
        }

        // First verify that IOC numbers (primary key) are unique!
        $res = query_db("SELECT species_nr FROM taxonomy WHERE IOC_order_nr IN (SELECT IOC_order_nr FROM _taxonomy)");
        if ($res->num_rows > 0) {
            $error = "
                <p $this->errorStyle>" . $res->num_rows . " conflicting IOC numbers, cannot proceed!</p>
                <p>Please contact Naturalis to fix this issue, as the script needs an update.</p>";
            return $this->template->render($output . $error);
        }

        $queries = [
            "DELETE FROM taxonomy WHERE group_id = " . $this->groupId(),
            "DELETE FROM taxonomy_ssp WHERE group_id = " . $this->groupId(),
            "INSERT INTO taxonomy (SELECT * FROM _taxonomy)",
            "INSERT INTO taxonomy_ssp (SELECT * FROM _taxonomy_ssp)",
        ];
        foreach ($queries as $q) {
            query_db($q);
        }
        $output .= "<p>Successfully updated the classification!</p>";

        return $this->template->render($output);
    }

    private function setGroupId($id)
    {
        $this->groupId = $id;
    }

    private function getGroupId()
    {
        if (!$this->groupId) {
            $this->groupId = $this->request->query->get('group_id');
        }
        return $this->groupId;
    }

    public function compare($id)
    {
        $output = '<h1>Classification Update</h1><p>';
        $this->setGroupId($id);

        if (!$this->getGroupId()) {
            return $this->template->render($output . "group_id is missing" . '</p>');
        }

        $added = $this->printComparison(
            "SELECT genus, species, family 
            FROM _taxonomy 
            WHERE species_nr NOT IN (SELECT species_nr FROM taxonomy WHERE group_id = " . $this->groupId() . ")
            ORDER BY genus, species"
        );
        $dropped = $this->printComparison(
            "
            SELECT genus, species, family 
            FROM taxonomy 
            WHERE species_nr NOT IN (SELECT species_nr FROM _taxonomy) AND group_id = " . $this->groupId() . "
            ORDER BY genus, species"
        );

        if ($dropped) {
            $output .= "
                <p $this->errorStyle>The classification cannot be automatically replaced, as there are
                taxa that were synonimised, renamed or lumped.</p>
                <p>Taxa that would be dropped from the current classification:</p>" . $dropped;
        }
        if ($added) {
            $output .= "<p>Taxa not present in the current classification:</p>" . $added;
        }
        if ($added && !$dropped) {
            $output .= '
                <p style="margin-top: 30px;">
                <form action="' . getUrl('admin-mdd-replace', ['group_id' => $this->groupId()]) . '">
                <input type="submit" value="Replace classification">
                </form>';
        } else {
            $output .= "<p>Current classification is already up-to-date.</p>";
        }

        return $this->template->render($output);
    }

    private function printComparison($query)
    {
        $res = query_db($query);
        if ($res->num_rows > 0) {
            $output = '<ul>';
            while ($row = $res->fetch_object()) {
                $output .= "<li>$row->genus $row->species ($row->family)</li>";
            }
            return $output . '</ul>';
        }
        return null;
    }

    public function prepare()
    {
        $output = '<h1>Classification Update</h1><p>';

        if (!$this->getGroupId()) {
            return $this->template->render($output . "group_id is missing" . '</p>');
        }

        // Convert DwCA
        $total = $this->importMdd();
        if (!$this->error) {
            $output .= "Imported $total taxa to temporary table<br>";
        } else {
            return $this->template->render($output . $this->error . '</p>');
        }

        // Fill taxonomy table
        $total = $this->createSpecies();
        if (!$this->error) {
            $output .= "Imported $total species<br>";
        } else {
            return $this->template->render($output . $this->error . '</p>');
        }

        $output .=
            '<p style="margin-top: 30px;">
            <form action="' . getUrl('admin-mdd-compare', ['group_id' => $this->groupId()]) . '">
            <input type="submit" value="Compare with current classification">
            </form>';

        $output .= '</p>';

        return $this->template->render($output);
    }

    private function importMdd()
    {
        ini_set('auto_detect_line_endings', true);

        $file = $this->minioPath . $this->mddCsv;
        if (!is_file($file)) {
            return $this->error = "File $file is missing <br>";
        } else {
            $i = 0;
            // Clear previous import
            query_db("TRUNCATE TABLE $this->mddTable");
            // Create insert statement from csv file
            $fh = fopen($file, 'r');
            while (($line = fgetcsv($fh, 0, ",")) !== false) {
                // Read header
                if ($i == 0) {
                    $this->keepKeys = array_intersect($line, $this->keepFields);
                } else {
                    $sql = "INSERT INTO $this->mddTable {$this->setFields()} VALUES " . $this->setValues($line);
                    // Insert by line to properly detect errors (impossible in batches)
                    if (!query_db($sql)) {
                        $this->error = "Error importing data from $this->mddCsv: <pre>$sql</pre><br>";
                        return false;
                    }
                }
                $i++;
            }
        }
        return $i;
    }

    private function setFields()
    {
        return '(`' . implode('`, `', $this->keepKeys) . '`)';
    }

    private function setValues($row)
    {
        return $this->escapeValues(array_intersect_key($row, $this->keepKeys));
    }

    private function escapeValues($row)
    {
        $values = array_map(function ($value) {
            return $value === "" ? 'null' : (is_numeric($value) ? $value : "'" . escape($value) . "'");
        }, $row);
        return ' (' . implode(', ', $values) . ')';
    }

    private function createSpecies()
    {
        $this->createImportTable('taxonomy');
        // Abuse avibase_id column to store DWCA taxonID, because why not
        query_db('ALTER TABLE `_taxonomy` ADD INDEX (`avibase_id`)');
        $orders = "('" . implode("', '", $this->orderToExport[$this->groupId]) . "')";

        $query = "SELECT * FROM $this->mddTable WHERE `order` IN $orders ORDER BY id";
        $res = query_db($query);
        $total = $res->num_rows;
        $iocNr = ($this->groupId() + 1) * 20000;

        while ($row = $res->fetch_object()) {
            $authorship = str_replace(', &', ' &', $row->authoritySpeciesAuthor) . ", $row->authoritySpeciesYear";
            if ($row->authorityParentheses == 1) {
                $authorship = "($authorship)";
            }
            $family = ucfirst(strtolower($row->family));

            $values = [
                $row->order,
                $family,
                $this->getFamilyEnglish($family),
                $row->genus,
                $row->specificEpithet,
                $row->mainCommonName,
                $iocNr,
                $this->getSpeciesNumber($row->id, $row->genus, $row->specificEpithet),
                $row->id, // abuse avibase_id to store DWCA id
                $authorship,
                0,
                0,
                $this->groupId()
            ];

            $insert = "
                INSERT INTO _taxonomy
                (
                    `order`,
                    family,
                    family_english,
                    genus,
                    species,
                    eng_name,
                    IOC_order_nr,
                    species_nr,
                    avibase_id,
                    authority,
                    recordings,
                    back_recordings,
                    group_id
                )
                VALUES " .
                $this->escapeValues($values);
            if (!query_db($insert)) {
                $this->error = "Error inserting species: <pre>$insert</pre><br> ";
                return false;
            }
            $iocNr += 0.01;
        }
        return $total;
    }

    private function createImportTable($table)
    {
        query_db("DROP TABLE if EXISTS _$table");
        query_db("CREATE TABLE _$table LIKE $table");
    }

    private function getFamilyEnglish($family)
    {
        if (isset($this->families[$family])) {
            return $this->families[$family];
        }

        $query = "
            SELECT t1.vernacularName FROM _dwca_vernacular AS t1
            LEFT JOIN _dwca_taxon AS t2 ON t1.taxonId = t2.taxonId
            WHERE t1.language = 'eng' AND t2.taxonRank = 'family' AND t2.scientificName LIKE '$family%'
            LIMIT 1";
        $res = query_db($query);
        if ($res && $res->num_rows == 1) {
            $row = $res->fetch_object();
            $this->families[$family] = ucfirst($row->vernacularName);
            return $this->families[$family];
        }
        return '';
    }

    private function getSpeciesNumber($id, $genus = false, $species = false)
    {
        if (isset($this->speciesNrs[$id])) {
            return $this->speciesNrs[$id];
        }

        $query = $genus && $species ?
            "SELECT species_nr FROM taxonomy WHERE genus = '$genus' and species = '$species'" :
            "SELECT species_nr FROM _taxonomy WHERE avibase_id = '$id'";
        $res = query_db($query);
        $row = $res->fetch_object();

        $speciesNr = $row ? $row->species_nr : $this->generateSpeciesNumber();
        $this->speciesNrs[$id] = $speciesNr;
        return $speciesNr;
    }

    private function generateSpeciesNumber()
    {
        do {
            // makes a random alpha numeric string of a given length
            $letters = range('a', 'z');
            $out = '';
            for ($c = 0; $c < 6; $c++) {
                $out .= $letters[mt_rand(0, count($letters) - 1)];
            }
        } while ($this->speciesNumberExists($out));

        return $out;
    }

    protected function speciesNumberExists($species_nr)
    {
        if (in_array($species_nr, $this->speciesNrs)) {
            return true;
        }

        $res = query_db("SELECT species_nr FROM taxonomy WHERE species_nr = '$species_nr'");
        return $res && $res->num_rows == 1;
    }

    private function getTaxon($id)
    {
        $query = '
            SELECT taxonID, parentNameUsageID, taxonRank, scientificName 
            FROM _dwca_taxon 
            WHERE taxonID = "' . escape($id) . '"';
        $res = query_db($query);
        return $res ? $res->fetch_object() : false;
    }
}
