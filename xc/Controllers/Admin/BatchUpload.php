<?php

namespace xc\Controllers\Admin;

use BatchUploadTask;
use Symfony\Component\Finder\Finder;

use function xc\getUrl;

class BatchUpload extends Controller
{

    private $batches = [];

    private $minioPath;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->minioPath = $this->checkFilePath(MINIO_BATCH);

        // Check if path to Minio server is correct
        if (file_exists($this->minioPath)) {
            $finder = new Finder();
            $this->batches = $finder->directories()->in($this->minioPath)->depth('== 0');
        }
    }

    public function index()
    {
        $output = "
            <h1>Batch Upload</h1><div style='width: 700px;'>";

        if (!empty($this->batches)) {
            $output .= "
                <p>Tool to upload a batch of sounds for a single user. Upload a batch to the 
                <b>Minio environment</b>, 
                using the uploader's user id as the name of the bucket. Add the sounds, plus a 
                <a href='https://docs.google.com/spreadsheets/d/1XQ3Kh3lCXoXUJfTkP3RuV0N_1UfmHxVavTr0a3TTxJA'>metadata.csv</a> 
                file describing the files, to this bucket.</p>
                <p>Batch upload buckets are not automatically removed once the import is complete. Use the Minio 
                environment to delete the processed batches.</p>
                <p>The following batches are available. Click a name to perform a dry run. If a dry run finishes without errors,
                you can start the batch upload from the result page of the dry run.</p>" .
                $this->printBatches() . '</div>';
        } else {
            $output .= "<p>Cannot locate files on the <b>Minio environment</b>, please verify that the path '" .
                $this->minioPath . "' is configured correctly!</p></div>";
        }

        return $this->template->render($output);
    }

    private function printBatches()
    {
        if (count($this->batches) == 0) {
            return "<p style='color: red; font-weight: bold; margin-top: 20px;'>No user directories with batches uploaded yet.</p>";
        }
        $output = '<ul>';
        foreach ($this->batches as $batch) {
            $user = basename($batch->getRealPath());
            $dry = getUrl('admin-batch-dry', ['user' => $user]);
            $output .= "<li><a href='$dry'>$user</a></li>";
        }
        return $output . '</ul>';
    }

    public function dryRun($user)
    {
        include_once 'tasks/batch-upload.php';

        $batch = new BatchUploadTask();
        $result = $batch->run(
            [
                0 => dirname(__FILE__),
                1 => $user,
                2 => $this->minioPath . $user . '/metadata.csv',
                3 => 'dry'
            ]
        );

        $output = "
            <h1>Batch Upload</h1>
            <p style='margin-bottom: 25px;'>Dry run for user <strong>$user</strong>. If the test fails, you
            will need to adapt the metadata.csv file before you can continue.</p>
            <p>" . nl2br($result) . "</p>
            <p style='margin-top: 25px;";
        // Contains errors: no link to real upload
        if (strpos($result, 'ERROR -->') !== false) {
            $output .= " color: red; font-weight: bold;'>Cannot proceed to upload, please first fix the errors!";
            // Error free, so go ahead
        } else {
            $output .= "'>Test successful! <a href='" . getUrl('admin-batch-upload', ['user' => $user]) .
                "'>Proceed to upload</a> (starts immediately!).";
        }

        return $this->template->render($output . '</p>');
    }

    public function upload($user)
    {
        include_once 'tasks/batch-upload.php';

        $batch = new BatchUploadTask();
        $result = $batch->run(
            [
                0 => dirname(__FILE__),
                1 => $user,
                2 => $this->minioPath . $user . '/metadata.csv',
                3 => 'up'
            ]
        );

        $output = '
            <h1>Batch Upload</h1>
            <p>' . nl2br($result) . "</p>";

        return $this->template->render($output);
    }
}
