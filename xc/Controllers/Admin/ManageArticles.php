<?php

namespace xc\Controllers\Admin;

use xc\ArticleUtil;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;

function featuresSelectHtml()
{
    $html = "<select name='feature-selection' id='feature-selection'>
        <option value='all'>All Articles</option>
        ";
    $res  = query_db('SELECT * FROM blogs ORDER BY blognr DESC');
    while ($row = $res->fetch_object()) {
        $html .= "<option value='{$row->blognr}'>{$row->blognr}. '{$row->title}' by {$row->author}</option>
            ";
    }
    $html .= '</select>';

    return $html;
}

class ManageArticles extends Controller
{

    public function handlePost()
    {
        if ($this->request->request->get('rebuild-features') == 'Rebuild') {
            $sql = null;

            if ($this->request->request->get('feature-selection') === 'all') {
                $sql = 'SELECT blognr, title, blog, author FROM blogs';
            } else {
                $id  = intval(
                    $this->request->request->get('feature-selection')
                );
                $sql = "SELECT blognr, title, blog, author FROM blogs WHERE blognr=$id";
            }

            $res = query_db($sql);
            if (!$res) {
                notifyError(
                    'Unable to retrieve data about the specified articles'
                );
            } elseif ($res->num_rows == 0) {
                notifyError('No articles matched your query');
            } else {
                while ($row = $res->fetch_object()) {
                    $generatedHtml = ArticleUtil::generate_blog(
                        $row->title,
                        $row->blog,
                        $row->blognr,
                        $row->author
                    );
                    $escaped       = escape($generatedHtml);
                    $res2          = query_db(
                        "UPDATE blogs SET blog_as_html='$escaped' WHERE blognr={$row->blognr}"
                    );
                    if (!$res2) {
                        notifyError(
                            "Error while rebuilding article #{$row->blognr}: {$row->title}"
                        );
                    }
                }
                notifySuccess('Rebuilt articles');
            }
        }
        return $this->seeOther(getUrl('admin-articles'));
    }

    public function handleRequest()
    {
        $output = "
            <h1>Manage Articles</h1>
            <h2>Rebuild Articles</h2>
            <form method='post'>
            " . featuresSelectHtml() . "
            <input type='submit' name='rebuild-features' value='Rebuild' />
            </form>
            ";

        return $this->template->render($output, ['title' => 'Manage Articles']);
    }
}
