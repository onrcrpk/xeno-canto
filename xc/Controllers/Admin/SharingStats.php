<?php

namespace xc\Controllers\Admin;

use xc\User;

class SharingStats extends Controller
{

    private $totalRecordings;

    private $totalRecorders;

    private $shared;

    private $nonShared;

    private $thirdPartyMapping = [
        -1 => 'Not yet selected',
        0  => 'Does not allow sharing',
        1  => 'Allows sharing',
    ];

    public function __construct($request)
    {
        parent::__construct($request);
    }

    public function index()
    {
        //print_r($this->getSharingDenyingUsers()); die();

        $output = '
            <h1>Data Sharing Stats</h1>
            <p><b>Selected licenses for a total of ' . $this->getTotalRecordings(
        ) . ' recordings</b>:
            <ul>';
        foreach ($this->getLicenseStats() as $license => $nr) {
            $output .= "<li>$license: $nr (" . round(
                ($nr / $this->getTotalRecordings() * 100),
                1
            ) . '%)</li>';
        }
        $output .= '
            </ul>
            </p>
            <p><b>User sharing statistics</b>:
            <ul>';
        foreach ($this->getRecordistStats() as $license => $nr) {
            $output .= "<li>$license: $nr users (" .
                       round(
                           ($nr / $this->getTotalRecorders() * 100),
                           1
                       ) . '%)</li>';
        }
        $output .= '
            </ul>
            </p>
            <p><b>Recording sharing statistics</b>:  
            <ul>
                <li>Shared: ' . $this->getNrSharedRecordings(
        ) . ' recordings (' .
                   round(
                       ($this->getNrSharedRecordings(
                       ) / $this->getTotalRecordings() * 100),
                       1
                   ) . '%)</li>
                <li>Sharing not allowed: ' . $this->getNrNonSharedRecordings(
                   ) . ' recordings (' .
                   round(
                       ($this->getNrNonSharedRecordings(
                       ) / $this->getTotalRecordings() * 100),
                       1
                   ) . '%)</li>
                <li>Not yet selected: ' . $this->getNrUndeterminedRecordings(
                   ) . ' recordings (' .
                   round(
                       ($this->getNrUndeterminedRecordings(
                       ) / $this->getTotalRecordings() * 100),
                       1
                   ) . '%)</li>
            </ul>
            </p><p><b>Users denying sharing</b>:';

        $deniers = $this->getSharingDenyingUsers();
        if (!empty($deniers)) {
            $output .= '
                <ul>';
            foreach ($deniers as $user) {
                $output .= "<li><a href='mailto:{$user->emailAddress()}'>{$user->userName()}</a>: 
                    {$user->nrDeniedRecordings} recordings</li>";
            }
            $output .= '   </ul>';
        } else {
            $output .= ' none!';
        }
        $output .= '</p>';

        return $this->template->render($output);
    }

    private function getTotalRecordings()
    {
        if (!$this->totalRecordings) {
            $res                   = query_db(
                'select count(1) as nr from birdsounds'
            );
            $row                   = $res->fetch_object();
            $this->totalRecordings = $row->nr;
        }
        return $this->totalRecordings;
    }

    private function getLicenseStats()
    {
        $res      = query_db(
            'select license, count(1) as nr from birdsounds group by license order by nr desc'
        );
        $licenses = [];
        while ($row = $res->fetch_object()) {
            $licenses[$row->license] = $row->nr;
        }
        return $licenses;
    }

    private function getRecordistStats()
    {
        $res      = query_db(
            'select third_party_license, count(1) as nr from users 
            group by third_party_license order by third_party_license'
        );
        $licenses = [];
        while ($row = $res->fetch_object()) {
            $licenses[$this->thirdPartyMapping[$row->third_party_license]] = $row->nr;
        }
        ksort($licenses);
        return $licenses;
    }

    private function getTotalRecorders()
    {
        if (!$this->totalRecorders) {
            $res                  = query_db(
                'select count(1) as nr from users'
            );
            $row                  = $res->fetch_object();
            $this->totalRecorders = $row->nr;
        }
        return $this->totalRecorders;
    }

    private function getNrSharedRecordings()
    {
        if (!$this->shared) {
            $res          = query_db('select nr_gbif from latest_statistics');
            $row          = $res->fetch_object();
            $this->shared = $row->nr_gbif;
        }
        return $this->shared;
    }

    private function getNrNonSharedRecordings()
    {
        if (!$this->nonShared) {
            $sql             = '
                select count(1) as no_gbif
                from birdsounds as t1
                left join users as t2 on t1.dir = t2.dir
                left join taxonomy as t3 on t1.species_nr = t3.species_nr
                where t2.third_party_license = 0 and t3.restricted = 0';
            $res             = query_db($sql);
            $row             = $res->fetch_object();
            $this->nonShared = $row->no_gbif;
        }
        return $this->nonShared;
    }

    private function getNrUndeterminedRecordings()
    {
        return $this->getTotalRecordings() - $this->getNrSharedRecordings() -
               $this->getNrNonSharedRecordings();
    }

    private function getSharingDenyingUsers()
    {
        $users = [];
        $res   = query_db(
            'select dir as id from users where third_party_license = 0'
        );
        while ($row = $res->fetch_object()) {
            $nrDenied = $this->getNrRecordingsForUser($row->id);
            if ($nrDenied > 0) {
                $d                                = User::load($row->id);
                $d->nrDeniedRecordings            = $nrDenied;
                $users[$d->userName() . $row->id] = $d;
            }
        }
        if (!empty($users)) {
            ksort($users);
            return $users;
        }
        return [];
    }

    private function getNrRecordingsForUser($userId = false)
    {
        $sql = "
            select count(1) as nr from birdsounds as t1
            left join taxonomy as t2 on t1.species_nr = t2.species_nr
            where t1.dir = '$userId' and t2.restricted = 0";
        $res = query_db($sql);
        $row = $res->fetch_object();
        return $row->nr;
    }
}
