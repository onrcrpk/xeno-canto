<?php

namespace xc\Controllers\Admin;

use xc\Recording;
use xc\User;

use function PHP81_BC\strftime;
use function xc\db;
use function xc\notifyError;

class RecordingRatings extends Controller
{

    public function show($xcid)
    {
        $rec = Recording::load($xcid, false);

        if (!$rec) {
            return $this->notFound();
        }

        $html = "<h1>Ratings for <a href='{$rec->URL()}'>XC{$rec->xcid()}</a></h1>
            <table class='results'>
            <thead>
            <tr>
            <td>timestamp</td>
            <td>user</td>
            <td>rating</td>
            </tr>
            </thead>
            <tbody>
            ";

        $sql = "
            SELECT R.id, U.*, R.rating, R.date from birdsounds_user_ratings R 
            INNER JOIN users U ON R.userid = U.dir 
            WHERE snd_nr = $xcid
            ORDER BY `date` ASC";
        $res = db()->query($sql);
        if (!$res) {
            notifyError(mysqli()->error);
            return $this->internalServerError();
        }

        while ($row = $res->fetch_object()) {
            $user = new User($row);
            $timestamp = strftime('%Y-%m-%d %T', strtotime($row->date));
            $ratings = [1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E'];
            $html .= "<tr>
                <td>$timestamp</td>
                <td><a href='{$user->getProfileURL()}'>{$user->userName()}</a></td>
                <td>{$ratings[$row->rating]}</td>
                </tr>";
        }
        $html .= '
            </tbody>
            </table>';

        return $this->template->render(
            $html,
            ['title' => "Ratings for XC{$rec->xcid()}"]
        );
    }
}
