<?php

namespace xc\Controllers\Admin;

use function xc\getUrl;

class Translations extends Controller
{

    // this is posted via AJAX, so no need to display anything...
    public function action($action)
    {
        $localesPath = __DIR__ . '/../../../locales';
        if ($action == 'upload') {
            return shell_exec("cd \"$localesPath\"; make upload 2>&1");
        } elseif ($action == 'download') {
            return shell_exec("cd \"$localesPath\"; make compile 2>&1");
        }
        return 'Unknown server request';
    }

    public function index()
    {
        $uploadUrl = getUrl(
            'admin-translations-action',
            ['action' => 'upload']
        );
        $downloadUrl = getUrl(
            'admin-translations-action',
            ['action' => 'download']
        );
        $script = <<<EOT
<script type='text/javascript'>
jQuery(document).ready(function() 
{
    jQuery('#upload-action').click(function() {
        postAction("$uploadUrl", "upload");
    });
    
    jQuery('#download-action').click(function() {
        postAction("$downloadUrl", "download");
    });
    
    function postAction (url, action) {
        jQuery.post(url, {action: action}, function(data) {
           jQuery("#status-log").html("<p style='margin-top: 15px;'><strong>Server response</strong>" + 
                "<pre>" + data + "</pre>");
        });
    }
});
</script>
EOT;

        $output = "
            <div style='width: 600px;'>
            <h1>Translations</h1>
            
            <p><strong>Upload</strong><br>
            We use poeditor.com to edit translations. When new translatable strings are
            added to the source code, we need to extract them into a translation messages
            file and upload them to poeditor.com.</p>
            <p><a id='upload-action' href='#'>Upload translations</a></p>

            <p style='margin-top: 15px;'><strong>Download</strong><br>
            When new terms are translated, you can download them from poeditor and compile
            them into .mo files.
            <p><a id='download-action' >Download translations</a> (response may take a while, please be patient!)</p>

            <p style='margin-top: 15px;'><strong>Additional translations</strong><br>
            When a new translation is added, you must create the directory structure (e.g.
            nl_NL/LC_MESSAGES) and then add the locale name to the LOCALES variable in the
            Makefile. After this, you must add the relevant information to the \$localMap variable in
            the file xc/Language.php</p>

            <div id='status-log'></div>
            $script
            </div>";

        return $this->template->render($output, ['title' => 'Translations']);
    }
}
