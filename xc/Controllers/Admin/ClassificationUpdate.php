<?php

namespace xc\Controllers\Admin;

use Symfony\Component\Finder\Finder;
use UpdateTaxonomy;

use function xc\getUrl;

class ClassificationUpdate extends Controller
{

    private $files = [];

    private $minioPath;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->minioPath = $this->checkFilePath(MINIO_CLASSIFICATION);

        // Check if path to Minio server is correct
        if (file_exists($this->minioPath)) {
            $finder = new Finder();
            $this->files = $finder->files()->in($this->minioPath);
        }
    }

    public function index()
    {
        $output = "
            <h1>Classification Update</h1>
            <div style='width: 700px;'>";

        if (!empty($this->files)) {
            $output .= "
                <p>Tool to update the classification to the latest standards. Upload three files to the 
                classification bucket in <b>Minio</b>:
                <ul>
                    <li><strong>changes.php</strong> (containing renames, lumps and splits)</li>
                    <li><strong>multilingual.csv</strong> (list of vernacular names)</li>
                    <li><strong>names.xml</strong> (latest classification)</li>
                </ul>
                You can optionally post-process individual recordings by additionally adding:
                <ul>
                    <li><strong>splits_fg.csv</strong> (foreground species)</li>
                    <li><strong>splits_bg.csv</strong> (background species)</li>
                </ul>
                
                The names of these files <strong>should match exactly</strong>!</p>
                <p>Click the link below to start a test run. It may take several minutes before
                the script is ready and any output appears on screen. When the dry run
                finishes without errors, please pass the three files to the Naturalis team.</p>
                <p style='margin-top: 20px;'><a href='" . getUrl(
                    'admin-classification-dry'
                ) .
                "'>Start a dry run</a></p>
                </div>";
        } else {
            $output .= "<p>Cannot locate files in <b>Minio</b>, 
                please verify that the path '" . $this->minioPath . "' is configured correctly!</p></div>";
        }

        return $this->template->render($output);
    }

    public function dryRun()
    {
        include_once 'tools/taxonomy/update-taxonomy.php';

        $update = new UpdateTaxonomy();
        $output = $update->run(
            [
                0 => dirname(__FILE__),
                1 => $this->minioPath . 'names.xml',
                2 => $this->minioPath . 'multilingual.csv',
                3 => $this->minioPath . 'changes.php',
            ]
        );

        $output = '<h1>Classification Update (dry run)</h1><p>' . nl2br($output);
        // Offer option to really update if dry run was successful
        if (!$update->hasErrors()) {
            $output .= "</p><p style='margin-top: 20px;'><a href='" . getUrl(
                    'admin-classification-run'
                ) .
                "'>Update classification</a>";
        }
        $output .= '</p>';

        return $this->template->render($output);
    }

    public function run()
    {
        include_once 'tools/taxonomy/update-taxonomy.php';

        $update = new UpdateTaxonomy();
        $output = $update->run(
            [
                0 => dirname(__FILE__),
                1 => $this->minioPath . 'names.xml',
                2 => $this->minioPath . 'multilingual.csv',
                3 => $this->minioPath . 'changes.php',
                4 => '-r',
            ]
        );

        $output = '<h1>Classification Update</h1><p>' . nl2br($output) . '</p>';

        return $this->template->render($output);
    }
}
