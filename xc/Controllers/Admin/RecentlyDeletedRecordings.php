<?php

namespace xc\Controllers\Admin;

use xc\Recording;

use function PHP81_BC\strftime;
use function xc\getUrl;
use function xc\XC_pageNumberNavigationWidget;

class RecentlyDeletedRecordings extends Controller
{

    public function handleRequest()
    {
        $perpage = 50;
        $page = $this->request->query->get('pg', 1);
        $offset = ($page - 1) * $perpage;
        $limitClause = "LIMIT $offset, $perpage";

        $res = query_db('SELECT COUNT(*) AS N FROM birdsounds_trash');
        $row = $res->fetch_object();
        if ($row) {
            $nrows = $row->N;
        }

        $pagenav = XC_pageNumberNavigationWidget(
            $this->request,
            ceil($nrows / $perpage),
            $page
        );

        $start = $offset + 1;
        $end = $offset + $perpage;
        $output = "
            <h1>Recently Deleted Recordings</h1>
            <p>A list showing the most-recently-deleted files. Showing records <b>$start - $end</b>.</p>
            $pagenav
            ";
        $output .= "<table class='results'>
            <thead>
            <tr>
            <th>ID</th>
            <th>Date deleted</th>
            <th>By</th>
            <th>File info</th>
            <th>Reason for deletion</th>
            </tr>
            </thead>
            ";
        $splitRes = query_db(
            "select D.*, D.delete_date as utime, U.username from birdsounds_trash D INNER JOIN users U ON U.dir=D.delete_user ORDER BY delete_date DESC $limitClause"
        );
        while ($row = $splitRes->fetch_object()) {
            $rec = new Recording($row);
            $dt = strftime('%c', strtotime($row->utime));
            $output .= "<tr>
                <td><a href='" . getUrl('recording', ['xcid' => $row->snd_nr]) . "'>XC{$row->snd_nr}</a></td>
                <td>$dt</td>
                <td>{$row->username}</td>
                <td>{$rec->debugString()}</td>
                <td style='font-weight:bold'>{$row->delete_reason}</td>
                </tr>";
        }
        $output .= "</table>
            $pagenav";

        return $this->template->render(
            $output,
            ['title' => 'Recently Deleted Recordings']
        );
    }
}
