<?php

namespace xc\Controllers\Admin;

use xc\Species;

use function xc\getUrl;

class SubspeciesIssues extends Controller
{

    public function show()
    {
        $output = '<h1>Subspecies Issues</h1>';

        $issuesres = query_db(
            'SELECT A.snd_nr, A.genus as curGenus, A.species as curSpecies, A.eng_name as curEng, A.species_nr as curNr,
            B.genus as newGenus, B.species as newSpecies, B.eng_name as newEng, B.species_nr as newNr, B.ssp
            FROM birdsounds A INNER JOIN taxonomy_ssp B ON
            A.genus=B.genus AND
            A.species != B.species AND
            A.ssp = B.ssp
            ORDER BY A.order_nr'
        );

        $output .= '
            <p>Recordings where <i>subspecies</i> matches a ssp in a different taxa in the same genus</p>
            <ul>';
        while ($row = $issuesres->fetch_object()) {
            $cursp = new Species($row->curGenus, $row->curSpecies, $row->curEng, $row->curNr);
            $newsp = new Species($row->newGenus, $row->newSpecies, $row->newEng, $row->newNr);
            $xcurl = getUrl('recording', ['xcid' => $row->snd_nr]);
            $output .= "
                <li>
                <a href='$xcurl'>XC{$row->snd_nr}</a>:
                <a href='{$cursp->profileURL()}'>{$cursp->commonName()}</a> (<span class='sci-name'>{$cursp->scientificName()} {$row->ssp}</span>)
                to <a href='{$newsp->profileURL()}'>{$newsp->scientificName()}</a>?
                </li>";
        }
        $output .= '</ul>';

        $issuesres = query_db(
            "SELECT DISTINCT A.snd_nr, A.genus as curGenus, A.species as curSpecies, A.eng_name as curEng, A.species_nr as curNr, A.ssp
            FROM birdsounds AS A
            WHERE NOT 
            EXISTS (

                SELECT * 
                FROM taxonomy_ssp AS B
                WHERE A.genus = B.genus
                AND A.species = B.species
                AND A.ssp = B.ssp
            )
            AND A.dir != 'WOEAFQRMUD'
            AND A.dir !=  'KZYUWIRZVH'
            AND A.ssp != ''
            AND A.ssp != A.species
            AND A.ssp NOT 
            REGEXP  'presumed'
            AND A.ssp NOT 
            REGEXP  'unnamed'
            AND A.ssp NOT 
            REGEXP  'presumably'
            AND A.ssp NOT 
            REGEXP  'prob '
            AND A.ssp NOT 
            REGEXP  'possible '
            AND A.ssp NOT 
            REGEXP  'undescribed'
            AND A.ssp NOT 
            REGEXP  'probably'
            AND A.ssp NOT 
            REGEXP  ' x '
            AND A.ssp NOT 
            REGEXP  ' or '
            AND A.ssp REGEXP  '^[^\?\)\(\'\.\/\\]*$'
            ORDER BY A.order_nr"
        );

        $output .= '
            <p>Recordings where <i>subspecies</i> does not exist in the taxonomy_ssp table</p>
            <ul>';
        while ($row = $issuesres->fetch_object()) {
            $cursp = new Species($row->curGenus, $row->curSpecies, $row->curEng, $row->curNr);
            $xcurl = getUrl('recording', ['xcid' => $row->snd_nr]);
            $output .= "
                <li>
                <a href='$xcurl'>XC{$row->snd_nr}</a>:
                <a href='{$cursp->profileURL()}'>{$cursp->commonName()}</a> (<span class='sci-name'>{$cursp->scientificName()} {$row->ssp}</span>)
                is not a valid taxon</a>?
                </li>";
        }
        $output .= '</ul>';

        return $this->template->render(
            $output,
            ['title' => 'Subspecies Issues']
        );
    }
}
