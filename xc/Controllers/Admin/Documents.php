<?php

namespace xc\Controllers\Admin;

use DateTime;
use Symfony\Component\Finder\Finder;

class Documents extends CopyFiles
{

    public function __construct($request)
    {
        parent::__construct($request);
        $this->setMinioPath(MINIO_DOCS);
        $this->setFilesPath('docs');
        $this->setActionUrl('admin-documents-copy');

        // Check if path to Minio server is correct
        if (file_exists($this->minioPath)) {
            $finder = new Finder();
            $this->files = $finder->files()->in($this->minioPath);
        }
    }

    public function list()
    {
        $finder = new Finder();
        $finder->files()->in($this->filesPath);
        $finder->sortByName();

        $server = "//{$_SERVER['HTTP_HOST']}";

        $output = '
            <table class="results" style="width: 750px; border-bottom: 1px solid hsl(0, 0%, 80%);">
                <tr>
                    <th>File name</th>
                    <th>Uploaded</th>
                    <th>Size</th>
                </tr> ';

        foreach ($finder as $file) {
            $filename = $file->getFilename();
            $modified = DateTime::createFromFormat('U', $file->getMTime());
            $dateTime = $modified->format("Y-m-d H:i:s");
            $size = number_format(round(($file->getSize() / 1000)), 0, '.', '.');

            $output .= "
                <tr>
                    <td><a href='$server/docs/$filename' target='_blank'>$filename</a></td> 
                    <td>$dateTime</td>
                    <td>$size KB</td>
                </tr>";
        }

        $output .= '</table>';

        return $this->template->render($output, ['title' => _('Downloadable documents'), 'bodyId' => 'docs']);
    }
}
