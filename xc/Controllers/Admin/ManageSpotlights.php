<?php

namespace xc\Controllers\Admin;

use xc\Spotlight;

use function xc\escape;
use function xc\getUrl;
use function xc\notifySuccess;
use function xc\sanitize;

class ManageSpotlights extends Controller
{

    public function index()
    {
        $output = "
            <h1>Manage Spotlights</h1>
            <h2>Create a new spotlight</h2>
            <p><a href='" . getUrl('admin-spotlight-new') . "'>New Spotlight</a></p>

            <h2>Edit an existing spotlight</h2>
            <form method='post'>
            <div>
            <select name='id' id='spotlight-selection'>
            ";
        $res = query_db(
            'SELECT * FROM spotlight_v2 ORDER BY spotlight_id  DESC'
        );
        while ($row = $res->fetch_object()) {
            $output .= "<option value='{$row->spotlight_id}'>{$row->spotlight_id}. {$row->title}";
            if (!$row->enabled) {
                $output .= ' [DISABLED]';
            }
            $output .= '</option>';
        }
        $output .= "
            </select>
            <input type='submit' value='Edit'/>
            </div>
            </form>
            ";

        return $this->template->render($output);
    }

    public function indexRedirect()
    {
        return $this->seeOther(
            getUrl(
                'admin-spotlight-edit',
                ['id' => $this->request->request->get('id')]
            )
        );
    }

    public function newSpotlight()
    {
        $content = "<h1>Create a new spotlight</h1>
            <p><a href='" . getUrl(
                'admin-spotlights'
            ) . "'>&laquo; Back to Spotlight Manager</a></p>"
            . $this->spotlightEditForm();
        return $this->template->render($content);
    }

    private function spotlightEditForm($id = null)
    {
        $title = '';
        $text = '';
        $caption = '';
        $filename = '';
        $enabled = false;
        $xcid = '';
        $preview = '';
        if ($id) {
            $res = query_db(
                "SELECT * from spotlight_v2 where spotlight_id=$id"
            );
            $row = $res->fetch_object();
            $title = sanitize($row->title);
            $text = sanitize($row->text);
            $caption = sanitize($row->img_caption);
            $filename = sanitize($row->img_path);
            $enabled = $row->enabled;
            $xcid = sanitize($row->featured_recording);
            $spotlight = new Spotlight($row);
            $preview = "<div style='width:720px; margin:2em auto'>" . $spotlight->toHtml() . '</div>';
        }
        $existingFilename = '';
        if ($filename) {
            $existingFilename = "<td><img src='/$filename' style='width:100px'/></td>";
        }
        $enabledText = '';
        if ($enabled) {
            $enabledText = 'checked';
        }

        return "
            $preview
            <form id='spotlight-form' method='post' enctype='multipart/form-data'>
            <input type='hidden' name='id' value='$id'/>
            <div>
            <p>Title:</p>
            <input type='text' name='title' placeholder='Spotlight title' value='$title' /></div>
            <div>
            <p>Text body:</p>
            <textarea name='text' placeholder='Spotlight text...'>$text</textarea></div>
            <h2>Image</h2>
            <p class='important'>Image must be exactly 400x300 pixels</p>
            <table>
            <tr>
            $existingFilename
            <td><input type='file' name='image'/></td>
            </tr>
            </table>
            <div>
            <p>Caption:</p>
            <input type='text' name='image-caption' placeholder='Short image caption...' value='$caption'/></div>
            <h2>Other</h2>
            <div><input type='checkbox' id='checkbox-enabled' name='enabled' value='1' $enabledText /><label for='checkbox-enabled'>Enabled</label></div>
            <div>
            <p>Featured Recording number:</p>
            <input type='text' name='featured-recording' placeholder='XC# of featured recording' value='$xcid'/></div>
            <div><input type='submit' name='Save' value='Save' /></div>
            </form>";
    }

    public function saveSpotlight($id = null)
    {
        $title = escape($this->request->request->get('title'));
        $text = escape($this->request->request->get('text'));
        $caption = escape($this->request->request->get('image-caption'));
        $file = $_FILES['image'];
        $enabled = intval($this->request->request->get('enabled'));
        $xcid = intval($this->request->request->get('featured-recording'));
        if (!$xcid) {
            die('missing featured recording');
        }
        $id = intval($this->request->request->get('id'));
        $img_path = '';

        if ($file && $file['error'] == UPLOAD_ERR_OK) {
            $name = $file['name'];
            if (!$name) {
                $ext = 'jpg';
                $mime = $file['type'];
                if ($mime) {
                    if (stristr($mime, 'png')) {
                        $ext = 'png';
                    } elseif (stristr($mime, 'gif')) {
                        $ext = 'gif';
                    }
                }
                $name = 'spotlight-' . uniqid() . ".$ext";
            }
            $img_path = "graphics/spotlight/$name";
            move_uploaded_file($file['tmp_name'], $img_path);
        }
        $img_path = escape($img_path);

        $sql = '';
        $message = '';
        if (!$id) {
            // adding a new spotlight
            $sql = "INSERT INTO spotlight_v2 VALUES (null, '$text', '$img_path', '$caption', NOW(), $xcid, '$title', $enabled)";
            $message = 'Added new spotlight';
        } else {
            // editing an existing spotlight
            $imgUpdate = '';
            // only update the img if a new image was uploaded
            if ($img_path) {
                $imgUpdate = "img_path='$img_path',";
            }
            $sql = "UPDATE spotlight_v2 SET
                text='$text',
                $imgUpdate
                img_caption='$caption',
                featured_recording=$xcid,
                title='$title',
                enabled=$enabled WHERE spotlight_id=$id";
            $message = "Updated spotlight $id";
        }
        if (query_db($sql)) {
            notifySuccess($message);
        }

        if (!$id) {
            // look up newly-inserted spotlight id
            $res = query_db('SELECT MAX(spotlight_id) as id from spotlight_v2');
            $row = $res->fetch_object();
            $id = $row->id;
        }

        if ($id) {
            return $this->seeOther(
                getUrl('admin-spotlight-edit', ['id' => $id])
            );
        }

        return $this->seeOther(getUrl('admin-spotlights'));
    }

    public function editSpotlight($id)
    {
        $content = "
            <p><a href='" . getUrl('admin-spotlights') . "'>&laquo; Back to Spotlight Manager</a></p>
            <h1>Edit Spotlight</h1>
            <form id='delete-form' action='" . getUrl(
                'admin-spotlight-delete',
                ['id' => $id]
            ) . "' method='get'>
            <input type='submit' id='delete-button' class='delete' value='Delete this Spotlight'/>
            </form>";
        $content .= $this->spotlightEditForm($id);
        return $this->template->render($content);
    }

    public function confirmDelete($id)
    {
        $content = "<h1>Delete spotlight $id?</h1>
            <p>Are you sure you want to delete this spotlight?</p>
            <form id='delete-form' method='post'>
            <input type='submit' id='delete-button' class='delete' value='Delete this Spotlight'/>
            </form>";

        return $this->template->render($content);
    }

    public function delete($id)
    {
        query_db("DELETE FROM spotlight_v2 WHERE spotlight_id=$id");
        notifySuccess("Deleted spotlight $id");
        return $this->seeOther(getUrl('admin-spotlights'));
    }
}
