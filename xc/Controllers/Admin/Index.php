<?php

namespace xc\Controllers\Admin;

use function xc\getUrl;

class Index extends Controller
{

    public function handleRequest()
    {
        $output = "
            <h1>Validator Tools</h1>
            <ul>
            <li><a href='" . getUrl('admin-resolved') . "'>Resolved mystery and questioned recordings</a></li>
            </ul>
            <h1>Content Tools</h1>
            <ul>
            <li><a href='" . getUrl('admin-articles') . "'>Manage Articles</a></li>
            <li><a href='" . getUrl('admin-spotlights') . "'>Manage Spotlights</a></li>
            <li><a href='" . getUrl('admin-tips') . "'>Manage 'New Stuff' Tips</a></li>
            <li><a href='" . getUrl('admin-news') . "'>Manage News</a></li>
            <li><a href='" . getUrl('admin-sets') . "'>Manage Sets</a></li>
            <li><a href='" . getUrl('admin-replace-recording') . "'>Replace a recording file</a></li>
            <li><a href='" . getUrl('admin-sono') . "'>Generate Sonograms</a></li>
            <li><a href='" . getUrl('admin-id3') . "'>Redo ID3 Tags</a></li>
            <li><a href='" . getUrl('admin-restricted-species') . "'>Manage Restricted Species</a></li>
            <li><a href='" . getUrl('admin-modify-species-set') . "'>Modify Recordings (change species name using sets)</a></li>
            <li><a href='" . getUrl('admin-batch-index') . "'>Batch Upload</a></li>
            <li><a href='" . getUrl('admin-locations-export') . "'>Download Text File With Locations</a></li>
            </ul>
            <h1>Classification Tools</h1>
            <ul>
            <li><a href='" . getUrl('admin-classification-index') . "'>XML Classification Update</a></li>            
            <li><a href='" . getUrl('admin-dwca-index') . "'>DwCA Classification Update</a></li>            
            <li><a href='" . getUrl('admin-mdd-index') . "'>MDD Classification Update</a></li>            
            </ul>
            <h1>Sync From Minio</h1>
            <ul>
            <li><a href='" . getUrl('admin-range-maps-index') . "'>Copy Range Maps</a></li>
            <li><a href='" . getUrl('admin-documents-index') . "'>Copy Files To Docs</a> (-> <a href='" . getUrl(
                'admin-documents-list'
            ) . "'>List Of Uploaded Files</a>)</li>
            </ul>
            <h1>User Tools</h1>
            <ul>
            <li><a href='" . getUrl('admin-restricted-users') . "'>A list of restricted users</a></li>
            <li><a href='" . getUrl('admin-potential-spammers') . "'>A list of potential spammers</a></li>
            <li><a href='" . getUrl('admin-masquerade') . "'>Temporarily log in as another user</a></li>
            <li><a href='" . getUrl('admin-mail') . "'>Send an email to groups of users</a></li>
            <li><a href='" . getUrl('admin-user-permissions') . "'>Manage user permissions</a></li>
            </ul>
            <h1>Translations</h1>
            <ul>
            <li><a href='" . getUrl('admin-translations') . "'>Upload/download translations</a></li>
            </ul>
            <h1>QA Tools</h1>
            <ul>
            <li><a href='" . getUrl('admin-splits') . "'>A list of recordings from potential splits</a> (warning: long query)</li>
            <li><a href='" . getUrl('admin-ssp-issues') . "'>A list of recordings with potential subspecies issues</a> (warning: long query)</li>
            <li><a href='" . getUrl('admin-fileinfo') . "'>A list of files with non-standard media info</a></li>
            <li><a href='" . getUrl('admin-recently-deleted') . "'>A list of recently-deleted files</a></li>
            <li><a href='" . getUrl('admin-recently-revised') . "'>A list of recently-revised recordings</a></li>
            <li><a href='" . getUrl('admin-sharing-stats') . "'>Metadata sharing options for users</a></li>
            </ul>
            <h1>Server Tools</h1>
            <ul>
            <li><a href='" . getUrl('admin-server-info') . "'>Server Info</a></li>
            </ul>
            <h1>Experimental Pages</h1>
            <ul>
            <li><a href='" . getUrl('gpslist') . "'>gpslist</a></li>
            </ul>
            ";

        return $this->template->render($output);
    }
}
