<?php

namespace xc\Controllers\Admin;

use xc\Query;

use function xc\XC_makeViewLinks;
use function xc\XC_pageNumber;
use function xc\XC_pageNumberNavigationWidget;
use function xc\XC_resultsTableForView;

class Resolved extends Controller
{

    public function handleRequest()
    {
        $title = _('Resolved Recordings');
        $num_per_page = defaultNumPerPage();
        $output = '';

        $resultsTable = '';

        $dir = $this->request->query->get('dir');
        $order = $this->request->query->get('order', 'xc');
        $view = $this->request->query->get('view', defaultResultsView());

        $pagenumber = XC_pageNumber($this->request);

        $sql = '
            SELECT %s
            FROM birdsounds 
            LEFT JOIN audio_info USING (snd_nr) 
            LEFT JOIN taxonomy_multilingual cnames ON birdsounds.species_nr=cnames.species_nr 
            LEFT JOIN forum_world USING (snd_nr) 
            LEFT JOIN forum_topic_thread_types ON forum_world.topic_nr = forum_topic_thread_types.topic_nr
            WHERE forum_topic_thread_types.type_id = 30 ';
        $select = Query::birdsoundsSelectFields() . ', audio_info.length, audio_info.smp AS sample_rate, 
            cnames.english as localName';

        $fgQuery = new Query('');
        $fgQuery->setRawSqlClause($sql, $select);
        $fgQuery->setOrder($order, $dir);

        //now do the results table
        $number_of_hits = $fgQuery->numRecordings();
        $no_pages = ceil($number_of_hits / $num_per_page);
        $res = $fgQuery->execute($pagenumber);

        $links = XC_makeViewLinks($this->request, $view);
        $resultsTable .= "
            <p>
            <div class=\"results-format\">" .
            _('Results format') . ":
            $links
            </div>
            </p>
            ";
        $resultsTable .= XC_pageNumberNavigationWidget(
            $this->request,
            $no_pages,
            $pagenumber
        );
        $resultsTable .= XC_resultsTableForView($this->request, $res, $view);
        $resultsTable .= XC_pageNumberNavigationWidget(
            $this->request,
            $no_pages,
            $pagenumber
        );

        $resultsSummary = '
            <p>' . sprintf(
                _('%s resolved mystery and questioned recordings'),
                "<strong>{$fgQuery->numRecordings()}</strong>"
            ) . '
            </p>';

        $output .= "
            <h1>$title</h1>
            $resultsSummary
            $resultsTable";

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'species-profile']
        );
    }
}
