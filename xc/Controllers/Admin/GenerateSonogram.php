<?php

namespace xc\Controllers\Admin;

use xc\AudioUtil;

use function xc\getUrl;

class GenerateSonogram extends Controller
{

    // this is posted via AJAX, so no need to display anything...
    public function generate()
    {
        $xcid = intval($this->request->request->get('xcid', 0));
        if ($xcid) {
            // Improved debugging
            if (!AudioUtil::updateAudioProperties($xcid)) {
                return 'Could not update audio properties';
            }
            if (!AudioUtil::generateSonos($xcid)) {
                return 'Could not create sonograms';
            }
            if (!AudioUtil::scheduleFullLengthSono($xcid)) {
                return 'Could not schedule full length sonogram and mp3 derivatives';
            }
            return json_encode(['error' => false]);
        }
    }

    public function showForm()
    {
        $apiUrl = getUrl('api-recording-search');
        $script = <<<EOT
<script type='text/javascript'>
jQuery(document).ready(function() 
{
    var n = 0;

    function regenRecording(url, xcid)
    {
        var d = jQuery.Deferred();
        var id = "xc-" + n++;
        var status = jQuery("<p id='" + id + "'>Regenerating sonogram for XC" + xcid + "... <span class='result'></span></p>");
        console.log("starting " + xcid);
        jQuery("#status-log").append(status);
        jQuery.post(url, {xcid: xcid})
            .done(function(data)
            {
                var resultElt = jQuery("#" + id + " > .result");
                resultElt.text("done");
            }).fail(function(xhr, status, error)
            {
                console.log(xhr.statusText);
                console.log(xhr.responseText);
                var resultElt = jQuery("#" + id + " > .result");
                resultElt.html("<span class='warning'>failed</span>");
            }).always(function()
            {
                d.resolve();
            });
        return d;
    }

    jQuery("#form-sono-gen-id").submit(function()
        {
            var xcid = jQuery(this).find("input[name=xcid]").val();
            if (xcid)
            {
                regenRecording(jQuery(this).attr('action'), xcid);
            }
            return false;
        });

    jQuery("#form-sono-gen-query").submit(function()
        {
            var query = jQuery(this).find("input[name=query]").val();
            var url = jQuery(this).attr('action');
            if (query)
            {
                jQuery.get("$apiUrl", {query: query},
                    function(data)
                    {
                        if (!data.error && data.recordings.length)
                        {
                            // issue these requests sequentially
                            var deferred = jQuery.Deferred();
                            var next = deferred;
                            var x;
                            for (x = 0; x < data.recordings.length; ++x)
                            {
                                (function (url, xcid) {
console.log("chaining request for " + xcid);
                                    next = next.pipe(function() {
                                        return regenRecording(url, xcid).done(function() { console.log("done"); });
                                    });
                                }(url, data.recordings[x].id));
                            }
                            // kick off the pipeline
                            deferred.resolve();
                        }
                    }, 'json');
            }
            return false;
        });
});
</script>
EOT;

        $output = "
            <h1>Generate Sonograms</h1>

            <form id='form-sono-gen-id' action='" . getUrl('admin-sono-post') . "' method='post'>
            <p>Re-do a single recording</p>
            <p><input type='text' name='xcid' placeholder='xcid' />
            <input type='submit' name='generate' value='Generate' /></p>
            </form>
            <form id='form-sono-gen-query' action='" . getUrl(
                'admin-sono-post'
            ) . "' method='post'>
            <p><label>Re-do all recordings matching a query (<b>Careful!</b>)</label></p>
            <p><input type='text' name='query' placeholder='query' />
            <input type='submit' name='generate' value='Generate' /></p>
            </form>
            <div id='status-log'></div>
            $script
            ";

        return $this->template->render(
            $output,
            ['title' => 'Generate Sonograms']
        );
    }
}
