<?php

/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace xc\Controllers\Admin;

use xc\ForumPost;
use xc\ThreadType;

use function xc\getUrl;
use function xc\XC_acceptableErrors;

class RestrictedUserData
{

    public $userid;

    public $username;

    public $country;

    public $totalUploads;

    public $allowedErrors;

    public $questioned;

    public $unconfirmed;

    public function __construct(
        $userid,
        $username,
        $country,
        $n,
        $allowed,
        $questioned,
        $unconfirmed
    ) {
        $this->userid = $userid;
        $this->username = $username;
        $this->country = $country;
        $this->totalUploads = $n;
        $this->allowedErrors = $allowed;
        $this->questioned = $questioned;
        $this->unconfirmed = $unconfirmed;
    }

}

class RestrictedUsers extends Controller
{

    public function show()
    {
        $output = '
            <h1>Restricted Users</h1>
            <p>A list showing which users are currently (or nearly) restricted from uploading from specific countries</p>
            ';
        // check to see if this user can upload uncomfirmed recordings for this
        // country. If the user has more than one sigma deviation from the
        // expected accuracy for this country (currently 99%), then all of their
        // recordings will be set to ID_UNCOMFIRMED and require confirmation by
        // another user/admin
        // Mystery recordings and unconfirmed recordings don't count toward the
        // total for this calculation.
        $sql = 'SELECT dir, U.username, country, SUM(IF(discussed!=' . ThreadType::MYSTERY . ',1,0)) as N, SUM(IF(discussed=' . ThreadType::ID_QUESTIONED . ', 1, 0)) AS NQ,
            SUM(IF(discussed=' . ThreadType::ID_UNCONFIRMED . ',1,0)) as NU
            FROM birdsounds INNER JOIN users U USING(dir) GROUP BY dir, country HAVING (NQ+NU)>0 ORDER BY username ASC, country ASC';
        $res = query_db($sql);
        $restricted = [];
        $almostRestricted = [];
        while ($row = $res->fetch_object()) {
            $totalErrors = $row->NQ + $row->NU;
            $n = $row->N;
            $acceptableErrors = XC_acceptableErrors($n);
            if ((($totalErrors / $acceptableErrors) > 0.5)) {
                $data = new RestrictedUserData(
                    $row->dir,
                    $row->username,
                    $row->country,
                    $row->N,
                    $acceptableErrors,
                    $row->NQ,
                    $row->NU
                );

                if ($totalErrors > $acceptableErrors) {
                    $restricted[] = $data;
                } else {
                    $almostRestricted[] = $data;
                }
            }
        }

        if ($restricted) {
            $output .= '<h2>Users under active restrictions in particular countries</h2>';
            $output .= $this->showUserTable($restricted);
        }

        if ($almostRestricted) {
            $output .= '<h2>Users nearing the allowed error limit</h2>
                <p>Calculated as within 50% of the error limit</p>
                ';
            $output .= $this->showUserTable($almostRestricted);
        }

        return $this->template->render($output, ['title' => 'Restricted Users']
        );
    }

    private function showUserTable($userData)
    {
        global $icons;
        $output = "<table class='results'>
            <thead>
            <tr>
            <th>User</th>
            <th>Country</th>
            <th>Total Recordings</th>
            <th>Errors Allowed</th>
            <th>Errors</th>
            </tr>
            </thead>
            ";

        foreach ($userData as $row) {
            $totalErrors = $row->unconfirmed + $row->questioned;
            $output .= "<tr>
                <td><a href='" . getUrl('recordist', ['id' => $row->userid]) . "'>{$row->username}</a></td>
                <td>{$row->country}</td>
                <td>{$row->totalUploads}</td>
                <td>{$row->allowedErrors}</td>
                <td>{$totalErrors}
                (<a href='" . getUrl(
                    'recordist',
                    [
                        'id' => $row->userid,
                        'query' => "cnt:\"{$row->country}\" dis:" . ThreadType::ID_QUESTIONED,
                    ]
                ) . "'>{$row->questioned} " . ForumPost::$threadTypeIcons[ThreadType::ID_QUESTIONED] . "</a> /
                <a href='" . getUrl(
                    'recordist',
                    [
                        'id' => $row->userid,
                        'query' => "cnt:\"{$row->country}\" dis:" . ThreadType::ID_UNCONFIRMED,
                    ]
                ) . "'>{$row->unconfirmed} " . ForumPost::$threadTypeIcons[ThreadType::ID_UNCONFIRMED] . '</a>)
                </tr>';
        }
        $output .= '</table>';
        return $output;
    }

}
