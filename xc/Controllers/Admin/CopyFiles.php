<?php

namespace xc\Controllers\Admin;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use function xc\getUrl;

class CopyFiles extends Controller
{

    protected $files = [];

    protected $minioPath;

    protected $filesPath;

    protected $actionUrl;

    public function __construct($request)
    {
        parent::__construct($request);
    }

    protected function setMinioPath($path)
    {
        $this->minioPath = $this->checkFilePath($path);
    }

    protected function setFilesPath($path)
    {
        $this->filesPath = realpath(dirname(__FILE__, 4)) . "/$path/";
    }

    protected function setActionUrl($route)
    {
        $this->actionUrl = getUrl($route);
    }

    public function index()
    {
        $output = "
            <h1>Copy Files</h1><div style='width: 700px;'>";

        if (!is_writable($this->filesPath)) {
            $output .= "<p>Error: $this->filesPath is not writable!</p></div>";
        } elseif (!empty($this->files)) {
            $output .= '
                <p>Copy files from the <b>Minio environment</b> to the site. Files with
                identical names will be overwritten without warning!</p>' .
                $this->printFiles() .
                "<p><a href='$this->actionUrl'>Copy files now</a></p>
                </div>";
        } else {
            $output .= "<p>Cannot locate files on the <b>Minio environment</b>, 
                please verify that the path '$this->minioPath' is configured correctly!</p></div>";
        }

        return $this->template->render($output);
    }

    private function printFiles()
    {
        $output = '<ul>';
        foreach ($this->files as $file) {
            $name = $file->getFilename();
            $output .= "<li>$name";
            if (file_exists($this->filesPath . $name)) {
                $output .= " <span style='color: red; font-weight: bold;'>→ file will be overwritten</span>";
            }
            $output .= "</li>\n";
        }
        return $output . '</ul>';
    }

    public function copy()
    {
        $output = '<h1>Files Copied</h1><p>' . $this->copyFiles() . '</p>';
        return $this->template->render($output . '</p>');
    }

    private function copyFiles()
    {
        $fileSystem = new Filesystem();

        $done = $skipped = [];
        foreach ($this->files as $i => $file) {
            $name = basename($file->getRealPath());
            try {
                $fileSystem->copy(
                    $file->getRealPath(),
                    $this->filesPath . $name,
                    true
                );
                $done[] = $name;
            } catch (IOException $e) {
                $skipped[] = "$name: $e";
            }
        }

        $output = '<p>Number of files copied: ' . count($done) . '. You will need to
            remove the files from the Minio server manually.</p>';

        if (!empty($skipped)) {
            $output .= '<p>The following files could not be copied:</p><ul>';
            foreach ($skipped as $name) {
                $output .= "<li>$name</li>";
            }
            $output .= '</ul>';
        }
        return $output;
    }
}
