<?php

namespace xc\Controllers\Admin;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;

class ManageNews extends Controller
{

    public function saveItem($id = 0)
    {
        $referer = $this->request->request->get('referer', getUrl('index'));
        $text    = $this->request->request->get('text');
        if (!$text) {
            notifyError('You cannot post an empty news item');
        } else {
            $successMessage = '';
            $text           = escape($text);
            if (!$id) {
                $sql            = "INSERT INTO news_world VALUES (CURDATE(),'$text','world', NULL)";
                $successMessage = 'Added news item';
            } else {
                $sql            = "UPDATE news_world SET text='$text' WHERE news_nr=$id";
                $successMessage = "Updated news item $id";
            }
            $res = query_db($sql);
            if (!$res) {
                notifyError(
                    "Unable to save news item to the database. ($sql) " . mysqli(
                    )->error
                );
            } else {
                notifySuccess($successMessage);
                return $this->seeOther($referer);
            }
        }
        return $this->showContent($id, $text);
    }

    public function newItem()
    {
        return $this->showContent();
    }

    public function editItem($id)
    {
        $text = '';
        $res  = query_db("SELECT text from news_world WHERE news_nr=$id");
        $row  = null;
        if ($res) {
            $row = $res->fetch_object();
        }

        if (!$row) {
            return $this->notFound();
        }

        $text = $row->text;

        return $this->showContent($id, $text);
    }

    private function showContent($id = null, $text = '')
    {
        $desc = 'Add a News Item';
        if ($id) {
            $desc = "Editing News Item $id";
        }

        $text    = htmlspecialchars($text);
        $referer = $this->request->headers->get('Referer');

        $output = "
            <h1>Manage News</h1>
            <p>$desc</p>
            <form method='post' action='" . getUrl(
                'admin-news-post',
                ['id' => $id]
            ) . "'>
            <textarea name='text' style='width:600px;height:200px' placeholder='news text...'>$text</textarea>
            <p>
            <input type='submit' name='submit' value='Save' />
            <input type='hidden' name='referer' value='$referer'/>
            </p>
            </form>
            ";

        return $this->template->render($output, ['title' => 'Manage News']);
    }

}
