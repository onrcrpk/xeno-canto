<?php

namespace xc\Controllers\Admin;

use xc\Recording;
use xc\Species;

class PotentialSplits extends Controller
{

    public function show()
    {
        $output = "
            <h1>Potential Splits</h1>
            <p>This page just does some very simple heuristics to create a list of recordings that might be classified incorrectly</p>

            <p>This just looks for recordings that have a subspecies name that is the 
            same as a species name within the same genus.  For example: <span 
            class='sci-name'>Coracina tenuirostris salomonis</span> has a ssp of 
                salamonis, and there's also a species <span class='sci-name'>Coracina 
                salomonis</span>.  This is often an indication that the species has been 
                split in the current taxonomy and the recording can be moved to the new 
                taxon.  Of course, it's possible that there are false positives, so each 
                recording should be investigated before simply re-assigning.</p>
                <p>Obviously, this only catches the recordings that have subspecies
                specified (and spelled correctly). Also, in the case where several ssp are
                split off to a new species, it will only catch the the one that matches the new
                specific epithet.  But at least it's a good indication that a split
                happened, so you can investigate the species more closely to determine
                whether there are other recordings that also need to be re-assigned.</p>
                <ul>";

        $splitRes = query_db(
            "select S.snd_nr, T.species_nr from birdsounds as S left join taxonomy T on (T.genus=S.genus AND T.species=S.ssp) WHERE S.species != S.ssp AND S.ssp !='' AND T.species is not null ORDER BY T.IOC_order_nr"
        );
        while ($row = $splitRes->fetch_object()) {
            $rec    = Recording::load($row->snd_nr);
            $sp     = Species::load($row->species_nr);
            $output .= "
                <li>
                <a href='{$rec->URL()}'>XC{$rec->xcid()}</a>:
                <a href='{$rec->speciesURL()}'>{$rec->commonName()}</a> (<span class='sci-name'>{$rec->fullScientificName()}</span>)
                to <a href='{$sp->profileURL()}'>{$sp->scientificName()}</a>?
                </li>";
        }
        $output .= '</ul>';

        return $this->template->render($output, ['title' => 'Potential Splits']);
    }
}
