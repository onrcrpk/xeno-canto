<?php

namespace xc\Controllers\Admin;

use xc\Recording;

use function xc\notifyError;
use function xc\notifySuccess;
use function xc\XC_updateRecordingStatus;

class UpdateRecordingStatus extends Controller
{

    public function handlePost()
    {
        $xcid = $this->request->request->get('xcid');
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        if (XC_updateRecordingStatus($xcid, $this->request->request->get('status'))) {
            notifySuccess('Updated recording status');
        } else {
            notifyError('Unable to update recording status');
        }

        return $this->seeOther($this->request->headers->get('Referer'));
    }
}
