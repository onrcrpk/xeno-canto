<?php

namespace xc\Controllers\Admin;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\sanitize;

class ManageTips extends Controller
{

    // if it hasn't been confirmed, it uses the GET method, after confirmation, it
    // uses POST
    public function deleteTip($id)
    {
        if (!$id) {
            return $this->notFound();
        }

        query_db("DELETE FROM newstuff WHERE nr=$id");
        notifySuccess("Deleted tip $id from database");

        return $this->seeOther(getUrl('admin-tips'));
    }

    public function saveTip($id = null)
    {
        // strip \r characters because they cause problems with translations
        $title = escape(
            str_replace(
                "\r",
                '',
                $this->request->request->get('title')
            )
        );
        $text  = escape(
            str_replace(
                "\r",
                '',
                $this->request->request->get('text')
            )
        );
        $id    = intval($id);

        $sql     = '';
        $message = '';
        if (!$id) {
            // adding a new tip
            $sql     = "INSERT INTO newstuff VALUES ('$title', '$text', '', '', NOW(), NULL)";
            $message = 'Successfully added new tip';
        } else {
            // editing an existing tip
            $sql     = "UPDATE newstuff SET
                text='$text',
                title='$title'
                WHERE nr=$id";
            $message = 'Successfully updated tip';
        }
        if (query_db($sql)) {
            notifySuccess($message);
        } else {
            notifyError(mysqli()->error);
        }

        if (!$id) {
            // look up newly-inserted id
            $res = query_db('SELECT MAX(nr) as id from newstuff');
            $row = $res->fetch_object();
            $id  = $row->id;
        }

        return $this->seeOther(getUrl('admin-tip-edit', ['id' => $id]));
    }

    public function index()
    {
        $title = 'Manage Tips';
        $html  = "<h1>$title</h1>
            <h2>Add a new Tip</h2>
            <p><a href='" . getUrl('admin-tip-new') . "'>New Tip</a></p>
            ";
        $html  .= "<h2>Edit an existing Tip</h2>
            <form method='post'>
            <div>
            <select name='id' id='tip-selection'>
            ";
        $res   = query_db('SELECT * FROM newstuff ORDER BY nr DESC');
        while ($row = $res->fetch_object()) {
            $html .= "<option value='{$row->nr}'>{$row->nr}. {$row->title}</option>
                ";
        }
        $html .= "
            </select>
            <input type='submit' value='Edit'/>
            </div>
            </form>
            ";

        return $this->template->render($html, ['title' => $title]);
    }

    public function indexRedirect()
    {
        return $this->seeOther(
            getUrl(
                'admin-tip-edit',
                ['id' => $this->request->request->get('id')]
            )
        );
    }

    public function newTip()
    {
        $content = "<h1>Create a new tip</h1>
            <p><a href='{$this->request->getBaseUrl()}'>&laquo; Back to Tip Manager</a></p>" .
                   $this->tipEditForm();

        return $this->template->render(
            $content,
            ['title' => 'Create a new Tip']
        );
    }

    public function editTip($id)
    {
        $content = "<p><a href='" . getUrl('admin-tips') . "'>&laquo; Back to Tip Manager</a></p>
                <h1>Edit Tip $id</h1>
                <form id='delete-form' method='get' action='" . getUrl(
                'admin-tip-delete',
                ['id' => $id]
            ) . "'>
                <input type='submit' id='delete-button' class='delete' value='Delete this Tip'/>
                </form>" .
                   $this->tipEditForm($id);

        return $this->template->render($content, ['title' => "Edit Tip $id"]);
    }

    public function confirmDelete($id)
    {
        $content = "
            <h1>Delete Tip $id</h1>
            <p class='imporant'>Are you sure you want to delete this tip?</p>
            <form id='delete-form' method='post'>
            <input type='submit' id='delete-button' class='delete' value='Delete this Tip'/>
            </form>";

        return $this->template->render($content, ['title' => "Delete Tip $id"]);
    }

    protected function GET()
    {
        $content = '<h1>Manage Tips</h1>';

        if ($this->request->query->get('delete')) {
            $content = "<p class='imporant'>Are you sure you want to delete this tip?</p>";
            $content .= $this->tipDeleteForm(
                $this->request->query->get('id'),
                'post'
            );
        } elseif ($this->request->query->get('action') == 'edit') {
            $content = "<p><a href='{$this->request->getBaseUrl()}'>&laquo; Back to Tip Manager</a></p>";
            $content .= $this->tipDeleteForm(
                $this->request->query->get('id')
            );
            $content .= $this->tipEditForm($this->request->query->get('id'));
        } elseif ($this->request->query->get('action') == 'new') {
            $content = "<p><a href='{$this->request->getBaseUrl()}'>&laquo; Back to Tip Manager</a></p>";
            $content .= $this->tipEditForm();
        } else {
            $content = $this->mainMenuHtml($this->request);
        }

        return $content;
    }

    private function tipDeleteForm($id, $method = 'get')
    {
        return "<form id='delete-form' method='$method'>
            <input type='hidden' name='delete' value='1' />
            <input type='hidden' name='id' value='$id' />
            <input type='submit' id='delete-button' class='delete' value='Delete this Tip'/>
            </form>";
    }

    private function tipEditForm($id = null)
    {
        $title = '';
        $text  = '';
        if ($id) {
            $res   = query_db("SELECT * from newstuff where nr=$id");
            $row   = $res->fetch_object();
            $title = sanitize($row->title);
            $text  = sanitize($row->text);
        }

        return "
            <form id='spotlight-form' method='post' enctype='multipart/form-data'>
            <div>
            <p>Title:</p>
            <input type='text' name='title' placeholder='Tip Title' value='$title' /></div>
            <div>
            <p>Text body (markdown syntax is accepted):</p>
            <textarea name='text' placeholder='Tip text...'>$text</textarea></div>
            <div><input type='submit' name='Save' value='Save' /></div>
            </form>";
    }

}
