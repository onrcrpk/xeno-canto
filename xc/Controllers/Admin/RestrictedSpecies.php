<?php

namespace xc\Controllers\Admin;

use xc\Species;

use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;

class RestrictedSpecies extends Controller
{

    public function show()
    {
        $output = "<h1>Restricted Species</h1>
        <p>The following species have additional restrictions on downloading / streaming</p>
        <form method='post' action='" . getUrl(
                'admin-restricted-species-remove'
            ) . "'>";

        $res = app()->db()->query(
            'SELECT species_nr, genus, species, eng_name FROM taxonomy T WHERE restricted=1'
        );
        $i = 0;
        while ($row = $res->fetch_object()) {
            $sp = new Species($row->genus, $row->species, $row->eng_name, $row->species_nr);
            $output .= "
            <p>
            <input id='r-s$i' type='checkbox' name='species[]' value='{$sp->speciesNumber()}'> 
            <label for='r-s$i'>{$sp->htmlDisplayName()}</label>
            </p>";
            $i++;
        }

        $output .= "<input type='submit' value='Remove Restrictions'>
        </form>

        <h2>Add New Restricted Species</h2>
        <form method='post' class='species-completion' action='" . getUrl('admin-restricted-species-add') . "'>
        <input type='text' name='species' class='species-input'/>
        <input type='submit' value='Add'>
        </form>";

        return $this->template->render($output, ['title' => 'Restricted Species']);
    }

    public function add()
    {
        $speciesText = $this->request->request->get('species');
        $spnr = Species::getSpeciesNumberForString($speciesText);
        if (!$spnr) {
            return $this->badRequest('That species was not found');
        }

        $res = app()->db()->query(
            "UPDATE taxonomy set restricted=1 WHERE species_nr='{$spnr}'"
        );
        if ($res) {
            notifySuccess("Added '$speciesText' to restricted species list");
        } else {
            notifyError(
                "Failed to add '$speciesText' to restricted species list: " . mysqli()->error
            );
        }

        return $this->seeOther(getUrl('admin-restricted-species'));
    }

    public function remove()
    {
        $nrs = $this->request->request->get('species');
        $output = '';
        if (!$nrs) {
            return $this->badRequest('No species specified');
        }

        for ($i = 0; $i < count($nrs); $i++) {
            $nrs[$i] = "'$nrs[$i]'";
        }
        $speciesList = implode(', ', $nrs);
        echo $speciesList;
        $res = app()->db()->query(
            "UPDATE taxonomy set restricted=0 WHERE species_nr IN ($speciesList)"
        );
        $n = count($nrs);
        if ($res) {
            notifySuccess("Removed $n species from restricted species list");
        } else {
            notifyError(
                "Failed to remove $n species from restricted species list: " . mysqli()->error
            );
        }

        return $this->seeOther(getUrl('admin-restricted-species'));
    }
}
