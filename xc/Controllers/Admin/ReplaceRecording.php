<?php

namespace xc\Controllers\Admin;

use getID3;
use xc\AudioUtil;
use xc\Recording;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\notifyWarning;

class ReplaceRecording extends Controller
{

    public function handleRequest()
    {
        $limitClause = '';
        $xcid = intval($this->request->query->get('xcid'));
        $rec = null;
        if ($xcid) {
            $rec = Recording::load($xcid);
        }

        if (!$rec) {
            if ($xcid) {
                notifyError("Recording $xcid not found");
            }
            return $this->chooseRecordingBody();
        }

        return $this->showUploadForm($rec);
    }

    protected function chooseRecordingBody()
    {
        $content = "
            <h1>Replace a recording file</h1>
            <p>Which recording would you like to replace?</p>
            <form method='get'>
            <p><label for='xcid'>XC ID</label></p>
            <input type='text' name='xcid' id='xcid'/>
            <input type='submit' value='Submit'/>
            </form>";
        return $this->template->render($content);
    }

    protected function showUploadForm($recording)
    {
        $content = "
            <h1>Replace a recording file</h1>
            <p>Replacing recording file for the following recording:</p>
            {$recording->player()}
            <p>Current filename for this recording: <tt>{$recording->filePath()}</tt></p>
            <h2>Upload a replacement recording file</h2>
            <form method='post' enctype='multipart/form-data'>
            <p><label for='input-file'>Upload a sound file (NOTE: subject to same uploading limits as main site):</label></p>
            <input type='file' id='input-file' name='replacement-file'/>
            <input type='hidden' name='xcid' value='{$recording->xcid()}'/>
            <p><input type='submit' value='Replace'/></p>
            </form>
        ";
        return $this->template->render($content);
    }

    public function handlePost()
    {
        $xcid = intval($this->request->request->get('xcid'));
        if (!$xcid) {
            notifyError('Fatal error: missing catalogue number');
            return $this->seeOther(getUrl('admin-index'));
        }
        $rec = Recording::load($xcid);
        if (!$rec) {
            notifyError(
                "Fatal error: trying to replace a recording file for XC$xcid which doesn't exist."
            );
            return $this->seeOther(getUrl('admin-index'));
        }

        $f = $_FILES['replacement-file'];
        if (!$this->fileIsValid($f)) {
            return $this->showUploadForm($rec);
        }

        $oldFile = $rec->filePath();
        $oldFileRenamed = $oldFile . '.' . date('YmdHis');
        if (!@rename($oldFile, $oldFileRenamed)) {
            notifyWarning(
                "Unable to rename old file <tt>$oldFile</tt> to <tt>$oldFileRenamed</tt>: " .
                var_export(error_get_last(), true)
            );
        }
        $newFile = dirname($oldFile) . '/XC' . $xcid . '-' . $f['name'];
        if (!move_uploaded_file($f['tmp_name'], $newFile)) {
            notifyError("Unable to move uploaded file <tt>{$f['tmp_name']}</tt> to <tt>$newFile</tt>");
            return $this->showUploadForm($rec);
        }
        $escapedFilename = escape(basename($newFile));
        if (!query_db("UPDATE birdsounds SET path='$escapedFilename' WHERE snd_nr=$xcid")) {
            notifyError(
                'Failed to update database with new name ' . basename($newFile) . ': ' . mysqli()->error
            );
            return $this->showUploadForm($rec);
        }
        AudioUtil::writeId3($xcid);
        AudioUtil::updateAudioProperties($xcid);
        AudioUtil::generateSonos($xcid);
        AudioUtil::scheduleFullLengthSono($xcid);
        notifySuccess("Replaced recording for XC$xcid");

        return $this->seeOther(getUrl('admin-replace-recording', ['xcid' => $xcid]));
    }

    protected function fileIsValid($f)
    {
        if ($f && $f['error'] == UPLOAD_ERR_OK) {
            if (!$this->checkValidFileFormat($f['tmp_name'])) {
                notifyError("Invalid file format");
                return false;
            }
        } else {
            notifyError(sprintf(_("Couldn't upload file: internal error %s"), $f['error']));
            return false;
        }

        return true;
    }

    public function checkValidFileFormat($file)
    {
        $getID3 = new getID3();
        $info = $getID3->analyze($file);

        if (!isset($info['fileformat']) || !in_array($info['fileformat'], ['mp3', 'wav'])) {
            return false;
        }
        return true;
    }
}
