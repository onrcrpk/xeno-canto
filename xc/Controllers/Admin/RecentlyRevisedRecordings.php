<?php

namespace xc\Controllers\Admin;

use xc\Recording;

use function xc\getUrl;
use function xc\XC_pageNumberNavigationWidget;

class RecentlyRevisedRecordings extends Controller
{

    public function handleRequest()
    {
        $perpage = 50;
        $page = $this->request->query->get('pg', 1);
        $offset = ($page - 1) * $perpage;
        $limitClause = "LIMIT $offset, $perpage";

        $res = query_db('SELECT COUNT(*) AS N FROM (SELECT snd_nr FROM birdsounds_history GROUP BY snd_nr) H');
        $row = $res->fetch_object();
        if ($row) {
            $nrows = $row->N;
        }

        $pagenav = XC_pageNumberNavigationWidget(
            $this->request,
            ceil($nrows / $perpage),
            $page
        );

        $output = '
            <h1>Recently Revised Recordings</h1>
            <p>A list showing the most-recently-revised files. Showing revisions <b>' . ($offset + 1) . ' - ' . ($offset + $perpage) . "</b></p>
            $pagenav
            ";
        $output .= "<table class='results'>
            <thead>
            <tr>
            <th>Catalog Number</th>
            <th>Species</th>
            <th>Recordist</th>
            <th>Revision date</th>
            <th>History</th>
            </tr>
            </thead>
            ";
        $res = query_db(
            "
            SELECT timestamp, B.*, U.username 
            FROM (
                select MAX(timestamp) as timestamp, snd_nr 
                from birdsounds_history 
                GROUP BY snd_nr 
                ORDER BY MAX(historyid) DESC $limitClause
            ) H 
            INNER JOIN birdsounds B USING(snd_nr) 
            INNER JOIN users U USING(dir) 
            ORDER BY timestamp DESC"
        );
        while ($row = $res->fetch_object()) {
            $rec = new Recording($row);
            $output .= "
            <tr>
                <td><a href='" . $rec->URL() . "'>XC{$row->snd_nr}</a></td>
                <td>{$rec->htmlDisplayName()}</td>
                <td>{$rec->recordist()}</td>
                <td>{$row->timestamp}</td>
                <td><a href='" . getUrl('recording-history', ['xcid' => $row->snd_nr]) . "'>view revisions</a></td>
           </tr>";
        }
        $output .= "</table>
        
        $pagenav";

        return $this->template->render(
            $output,
            ['title' => 'Recently Revised Recordings']
        );
    }

}
