<?php

namespace xc\Controllers\Admin;

use function xc\getUrl;

const NR_MAILS_LIMIT = 5;

class PotentialSpammers extends Controller
{

    public function show()
    {
        $startDate = date('Y-m-d', strtotime('-3 month'));

        $output = '
            <h1>Potential Spammers</h1>
            <p>A list showing users who have sent more than ' . NR_MAILS_LIMIT . ' mails per day during the last three months.</p>
            ';

        $sql = "
            SELECT COUNT(t1.id) as nr, t2.dir, t2.username, t1.date_sent, GROUP_CONCAT(DISTINCT(t3.username) SEPARATOR ', ') as recipients
            FROM mails_sent AS t1
            LEFT JOIN users AS t2 ON t1.sender_dir = t2.dir
            LEFT JOIN users AS t3 ON t1.recipient_dir = t3.dir
            WHERE t1.date_sent >= '$startDate'
            GROUP BY t1.sender_dir, t1.date_sent
            HAVING nr >= " . NR_MAILS_LIMIT . '
            ORDER BY t1.date_sent DESC, t2.username ASC';
        $res = query_db($sql);

        $table = "<table class='results'>
            <thead>
            <tr>
            <th>User</th>
            <th>Date</th>
            <th>Number of Mails</th>
            <th>Recipients</th>
            </tr>
            </thead>
            ";

        $rows = '';
        while ($row = $res->fetch_object()) {
            if (isset($row->dir)) {
                $rows .= "
                <tr>
                    <td><a href='" . getUrl('recordist', ['id' => $row->dir]) . "'>{$row->username}</a></td>
                    <td>{$row->date_sent}</td>
                    <td>{$row->nr}</td>
                    <td>{$row->recipients}</td>
                </tr>";
            }
        }

        if ($rows != '') {
            $output .= $table . $rows . '</table>';
        } else {
            $output .= '<p>No potential spammers detected!</p>';
        }

        return $this->template->render(
            $output,
            ['title' => 'Potential Spammers']
        );
    }
}
