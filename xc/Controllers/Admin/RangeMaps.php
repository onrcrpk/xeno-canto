<?php

namespace xc\Controllers\Admin;

use Symfony\Component\Finder\Finder;

class RangeMaps extends CopyFiles
{

    public function __construct($request)
    {
        parent::__construct($request);
        $this->setMinioPath(MINIO_RANGES);
        $this->setFilesPath('ranges');
        $this->setActionUrl('admin-range-maps-copy');

        // Check if path to Minio server is correct
        if (file_exists($this->minioPath)) {
            $finder = new Finder();
            $this->files = $finder->files()->in($this->minioPath)->name(
                '*.kmz'
            );
        }
    }
}
