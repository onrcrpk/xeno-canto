<?php

namespace xc\Controllers\Admin;

use xc\User;
use xc\XCMail;

use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;

const USER_TYPE_ALL            = 0;
const USER_TYPE_NO_LICENSE     = 1;
const USER_TYPE_NO_RECORDINGS  = 2;
const USER_TYPE_RECORDISTS     = 3;
const USER_TYPE_TOP_RECORDISTS = 4;
const USER_TYPE_INACTIVE       = 5;

const TOP_RECORDISTS_MIN_RECORDINGS = 100;


class MailUsers extends Controller
{

    private $userGroups = [
        USER_TYPE_ALL            => 'All users',
        USER_TYPE_NO_LICENSE     => 'Users that have not yet accepted the third party license',
        USER_TYPE_NO_RECORDINGS  => 'Users that have not yet uploaded a recording',
        USER_TYPE_RECORDISTS     => 'Users that have uploaded at least one recording',
        USER_TYPE_TOP_RECORDISTS => 'Users that have uploaded at least ' . TOP_RECORDISTS_MIN_RECORDINGS . ' recordings',
        USER_TYPE_INACTIVE       => 'Users that have been inactive for ' . User::MAX_COOKIE_AGE . ' or more days',
    ];

    public function send()
    {
        $text      = $this->request->request->get('text');
        $subject   = $this->request->request->get('subject');
        $userGroup = $this->request->request->get('user_group');

        $wrappedText = wordwrap($text);

        if ($this->request->request->get('send')) {
            if (empty($subject) || empty($text)) {
                notifyError(
                    'Required field missing: Both subject and body must be filled in'
                );
            } else {
                $addresses = $this->getUserEmails($userGroup);
                if ($addresses) {
                    $mail = new XCMail(null, $subject, $wrappedText);
                    foreach ($addresses as $email) {
                        $mail->to($email)->send();
                    }
                    notifySuccess('Mail Sent');
                    $this->seeOther(getUrl('admin-index'));
                } else {
                    notifyError(
                        'Unknown failure while querying user email addresses.'
                    );
                }
            }
        }
        return $this->showForm();
    }

    protected function getUserEmails($type)
    {
        switch ($type) {
            case USER_TYPE_ALL:
                $sql = 'select email from users';
                break;
            case USER_TYPE_NO_LICENSE:
                $sql = 'select email from users where third_party_license = -1';
                break;
            case USER_TYPE_NO_RECORDINGS:
                $sql = '
                    select t1.email from users as t1
                    left join birdsounds as t2 on t1.dir = t2.dir
                    where t2.snd_nr is null';
                break;
            case USER_TYPE_RECORDISTS:
                $sql = '
                    select distinct(t1.email) from users as t1
                    left join birdsounds as t2 on t1.dir = t2.dir
                    where t2.snd_nr is not null';
                break;
            case USER_TYPE_TOP_RECORDISTS:
                $sql = '
                    select t1.email from users as t1
                    left join birdsounds as t2 on t1.dir = t2.dir
                    where t2.snd_nr is not null 
                    group by t1.dir
                    having count(t2.snd_nr) >= ' . TOP_RECORDISTS_MIN_RECORDINGS;
                break;
            case USER_TYPE_INACTIVE:
                $sql = '
                    select t1.email from users as t1
                    left join user_cookies as t2 on t1.dir = t2.userid
                    where t2.userid is null';
                break;
            default:
                return false;
        }
        $res = query_db($sql);

        return $res ? array_column(
            $res->fetch_all(MYSQLI_ASSOC),
            'email'
        ) : false;
    }

    protected function showForm()
    {
        $body = '
            <h1>Mail Members</h1>
            <p>Send a message to registered users</p>';

        $subject   = $this->request->request->get('subject');
        $text      = $this->request->request->get('text');
        $userGroup = $this->request->request->get('user_group');

        // preview
        if ($subject || $text) {
            $body = "
                <div class='mail-preview'>
                <h2>Message Preview</h2>
                <p><strong>To</strong>: " . $this->userGroups[$userGroup] . '</p>
                <p><strong>Subject</strong>: ' . htmlspecialchars($subject) . "</p>
                <pre>$text</pre>
                </div>";
        }

        $body .= "
            <form method='post'>
            <p>" . $this->userGroupSelect($userGroup) . "</p>
            <p>
            <input style='width:100%;' name='subject' value='" . htmlspecialchars(
                $subject,
                ENT_QUOTES
            ) . "'
            type='text' placeholder='Subject'/>
            </p>
            <textarea style='width:100%;height:20em;' placeholder='Email text' name='text'>" . htmlspecialchars(
                $text,
                ENT_QUOTES
            ) . "</textarea>
            <p>
            <input type='submit' name='preview' value='Preview'/>
            <input type='submit' name='send' value='Send'/>
            </p>
            </form>
            ";

        return $this->template->render($body, ['title' => 'Mail Users']);
    }

    private function userGroupSelect($selectedOption = false)
    {
        $select = '
            <label for="user_group">Choose a user group:</label>
            <select name="user_group" id="user_group">';
        foreach ($this->userGroups as $type => $description) {
            $selected = $type == $selectedOption ? 'selected' : '';
            $select   .= "<option value='$type' $selected>$description (" . $this->userCount(
                $type
            ) . ")</option>\n";
        }
        return $select;
    }

    private function userCount($type)
    {
        switch ($type) {
            case USER_TYPE_ALL:
                $sql = 'select count(dir) as nr from users';
                break;
            case USER_TYPE_NO_LICENSE:
                $sql = 'select count(dir) as nr from users where third_party_license = -1';
                break;
            case USER_TYPE_NO_RECORDINGS:
                $sql = '
                    select count(t1.dir) as nr from users as t1
                    left join birdsounds as t2 on t1.dir = t2.dir
                    where t2.snd_nr is null';
                break;
            case USER_TYPE_RECORDISTS:
                $sql = 'select count(distinct(dir)) as nr from birdsounds';
                break;
            case USER_TYPE_TOP_RECORDISTS:
                $sql = '
                    select count(nr_recordings) as nr from (
                        select count(t2.snd_nr) as nr_recordings from users as t1
                        left join birdsounds as t2 on t1.dir = t2.dir
                        where t2.snd_nr is not null 
                        group by t1.dir
                        having nr_recordings >= ' . TOP_RECORDISTS_MIN_RECORDINGS . '
                        order by nr_recordings desc
                    ) as nr';
                break;
            case USER_TYPE_INACTIVE:
                $sql = '
                    select count(t1.dir) as nr from users as t1
                    left join user_cookies as t2 on t1.dir = t2.userid
                    where t2.userid is null';
                break;
            default:
                return false;
        }
        $res = query_db($sql);

        return $res ? $res->fetch_object()->nr : false;
    }

    public function compose()
    {
        return $this->showForm();
    }
}
