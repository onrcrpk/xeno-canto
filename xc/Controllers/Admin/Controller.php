<?php

namespace xc\Controllers\Admin;

use xc\AdminTemplate;
use xc\User;

class Controller extends \xc\Controllers\Controller
{

    public function __construct($request)
    {
        parent::__construct($request);
        $this->template = new AdminTemplate($request);
    }

    public function authCheck()
    {
        if (User::current() && User::current()->isAdmin()) {
            return null;
        }

        return $this->notFound();
    }

    // Adds trailing slash if necessary
    public function checkFilePath($path)
    {
        return rtrim($path, '/') . '/';
    }
}
