<?php

namespace xc\Controllers\Admin;

use xc\XCRedis;

use function xc\notifySuccess;
use function xc\XC_formatUserText;

class DeleteComment extends Controller
{

    public function handleRequest()
    {
        $t       = $this->request->query->get('t');
        $id      = intval($this->request->query->get('id'));
        $referer = $this->request->headers->get('Referer');

        if ($t == 'discuss_news_world') {
            $sql = "SELECT text FROM discuss_news_world WHERE nr=$id";
        } elseif ($t == 'discussion_forum_world') {
            $sql = "SELECT text FROM discussion_forum_world WHERE mes_nr=$id";
        } elseif ($t == 'feature_discussions') {
            $sql = "SELECT text FROM feature_discussions WHERE mes_nr=$id";
        }

        $res = query_db($sql);
        $row = $res->fetch_object();

        $output = '<h1>Delete Comment?</h1>
            <p>Would you like to delete the following comment?</p>
            <blockquote>' .
                  XC_formatUserText($row->text) . "
            </blockquote>
            <form method='post' />
            <input type='hidden' name='t' value='$t'/>
            <input type='hidden' name='id' value='$id'/>
            <input type='hidden' name='referer' value='$referer'/>
            <input type='submit' value='Delete'/>
            </form>";

        return $this->template->render($output, ['title' => 'Delete Comment']);
    }

    public function handlePost()
    {
        $t       = $this->request->request->get('t');
        $id      = intval($this->request->request->get('id'));
        $referer = $this->request->request->get('referer');

        // Decrease the reply count (XC-46) but keep the old value
        // for last_activity
        if ($t == 'discussion_forum_world') {
            $q   = "
                SELECT t1.topic_nr, t2.last_activity 
                FROM discussion_forum_world as t1
                LEFT JOIN forum_world as t2 on t1.topic_nr = t2.topic_nr
                WHERE mes_nr=$id";
            $res = query_db($q);
            $row = $res->fetch_object();
            if ($row->topic_nr) {
                query_db(
                    '
                    UPDATE forum_world 
                    SET replies=(replies-1), last_activity="' . $row->last_activity . '"
                    WHERE topic_nr=' . $row->topic_nr
                );
            }
        }

        $sql = '';
        if ($t == 'discuss_news_world') {
            $sql = "DELETE FROM discuss_news_world WHERE nr=$id";
        } elseif ($t == 'discussion_forum_world') {
            $sql = "DELETE FROM discussion_forum_world WHERE mes_nr=$id";
        } elseif ($t == 'feature_discussions') {
            $sql = "DELETE FROM feature_discussions WHERE mes_nr=$id";
        }

        (new XCRedis())->clearForumCache();

        notifySuccess('Comment deleted');
        query_db($sql);
        return $this->seeOther($referer);
    }

}
