<?php

namespace xc\Controllers\Admin;

use xc\Set;
use xc\Species;
use xc\User;

use function xc\escape;
use function xc\getUrl;
use function xc\notifySuccess;
use function xc\populateBackgroundData;
use function xc\sanitize;

class ModifySpecies extends Controller
{

    private $set;

    private $fromSpecies;

    private $toSpecies;

    private $nrForeUpdated = 0;

    private $nrBackUpdated = 0;

    public function __construct($request)
    {
        parent::__construct($request);
    }

    public function selectSet()
    {
        $output = "<h2>Modify Species</h2>
            <p style='width: 600px;'>This page lets you modify the species name for a batch of recordings. The recordings should be grouped
            in a (disposible) set. Please make absolutely sure that the set contains just those recordings that need modification
            and that you select the correct set, as there is no option to revert the change!</p>
            <p style='margin-bottom: 15px;'><b>Step 1 (of 4)</b>: select a set:</p>
            <ul class='simple'>";

        $userid = User::current()->userId();
        $sets = Set::loadUserSets($userid, true);

        if ($sets) {
            foreach ($sets as $set) {
                $output .= "<li><a href='{$set->url()}'>{$set->name()}</a> &mdash; " . sprintf(
                        ngettext(
                            '%d recording',
                            '%d recordings',
                            intval($set->numRecordings())
                        ),
                        $set->numRecordings()
                    ) . "
                    <a href='" . getUrl(
                        'admin-modify-species-from', ['set' => $set->id()]
                    ) . "'><img src='/static/img/next.png' title='" . sanitize(
                        _(
                            'Select Set'
                        )
                    ) . "' class='icon'/></a>" . '</li>';
            }
        } else {
            $output .= "<li class='unspecified'>" . _('None') . '</li>';
        }

        $output .= '</ul>';

        return $this->template->render($output);
    }


    public function selectFromSpecies($setId)
    {
        $this->setSet($setId);

        if (empty($this->set)) {
            return $this->badRequest();
        }

        $output = "<h2>Modify Species</h2>
            <p style='margin-bottom: 15px;'><b>Step 2 (of 4)</b>: select the foreground or background
            species in the set <b>{$this->set->name()}</b> to modify:</p>
            <ul>";

        $species = $this->set->speciesInSet($setId);

        foreach ($species as $sp) {
            $url = getUrl(
                'admin-modify-species-to', [
                    'set' => $this->set->id(),
                    'fromSpecies' => $sp->speciesNumber(),
                ]
            );
            $output .= "<li><a href='$url'>{$sp->scientificName()}</a></li>";
        }

        return $this->template->render($output . '</ul>');
    }

    private function setSet($setId)
    {
        $this->set = Set::load($setId);
        return $this->set;
    }

    public function selectToSpecies($setId, $fromSpecies)
    {
        $this->setSet($setId);
        $this->setFromSpecies($fromSpecies);

        if (empty($this->set) || empty($this->fromSpecies)) {
            return $this->badRequest();
        }

        $output = "<h2>Modify Species</h2>
            <p style='margin-bottom: 15px;'><b>Step 3 (of 4)</b>: select a species to change
            <b>{$this->fromSpecies->scientificName()}</b> to:</p>
            <ul>";

        $sql = "
            select species_nr, genus, species
            from taxonomy
            where species_nr != '{$this->fromSpecies->speciesNumber()}' and species_nr != ''
            order by genus, species";
        $res = query_db($sql);
        while ($row = $res->fetch_object()) {
            $url = getUrl(
                'admin-modify-species-confirm', [
                    'set' => $this->set->id(),
                    'fromSpecies' => $this->fromSpecies->speciesNumber(),
                    'toSpecies' => $row->species_nr,
                ]
            );
            $output .= "<li><a href='$url'>$row->genus $row->species</a></li>";
        }

        return $this->template->render($output . '</ul>');
    }

    private function setFromSpecies($speciesNr)
    {
        $this->fromSpecies = Species::load($speciesNr, false);
        return $this->fromSpecies;
    }

    public function confirm($setId, $fromSpecies, $toSpecies)
    {
        $this->setSet($setId);
        $this->setFromSpecies($fromSpecies);
        $this->setToSpecies($toSpecies);

        if (empty($this->set) || empty($this->fromSpecies) || empty($this->toSpecies)) {
            return $this->badRequest();
        }

        $url = getUrl(
            'admin-modify-species-save', [
                'set' => $this->set->id(),
                'fromSpecies' => $this->fromSpecies->speciesNumber(),
                'toSpecies' => $this->toSpecies->speciesNumber(),
            ]
        );
        $output = "<h2>Modify Species</h2>
            <p style='width: 600px; margin-bottom: 15px;'><b>Step 4 (of 4)</b>: please confirm
            that you want to change the foreground and background species in the set
            <b>{$this->set->name()}</b> from <b>{$this->fromSpecies->scientificName()}</b> to
            <b>{$this->toSpecies->scientificName()}</b>.</p>
            <p><a href='$url'>Do it!</a></p>";

        return $this->template->render($output);
    }

    private function setToSpecies($speciesNr)
    {
        $this->toSpecies = Species::load($speciesNr, false);
        return $this->toSpecies;
    }

    public function save($setId, $fromSpecies, $toSpecies)
    {
        $this->setSet($setId);
        $this->setFromSpecies($fromSpecies);
        $this->setToSpecies($toSpecies);

        if (empty($this->set) || empty($this->fromSpecies) || empty($this->toSpecies)) {
            return $this->badRequest();
        }

        $this->updateHistory();
        $this->updateRecordings();

        $successMessage = "Successfully modified $this->nrForeUpdated foreground and
            $this->nrBackUpdated background recordings in the set <b>{$this->set->name()}</b>.
            The species name was changed from  <b>{$this->fromSpecies->scientificName()}</b> to
            <b>{$this->toSpecies->scientificName()}</b>.";

        notifySuccess($successMessage);
        return $this->seeOther(getUrl('admin-modify-species-set'));
    }

    private function updateHistory()
    {
        $update = "
            insert into birdsounds_history (
                select null, now(), t1.snd_nr, t1.genus, t1.species, t1.ssp, t1.eng_name,
                    t1.songtype, t1.recordist, t1.longitude, t1.latitude, t1.remarks, t1.license, null
                from
                    birdsounds as t1
                left join
                    datasets_recordings as t2 on t1.snd_nr = t2.snd_nr
                where
                    t2.datasetid = {$this->set->id()} and
                    (t1.species_nr = '{$this->fromSpecies->speciesNumber()}' or
                    t1.species_nr like '%{$this->fromSpecies->speciesNumber()}%')
             )";
        app()->db()->query($update);
        return mysqli()->affected_rows;
    }

    private function updateRecordings()
    {
        $genus = escape($this->toSpecies->genus());
        $species = escape($this->toSpecies->species());
        $ssp = escape($this->toSpecies->subspecies());
        $commonName = escape($this->toSpecies->commonName());
        $family = escape($this->toSpecies->family());

        $updateFore = "
            update
                birdsounds as t1
            left join
                datasets_recordings as t2 on t1.snd_nr = t2.snd_nr
            set
                t1.genus = '$genus',
                t1.species = '$species',
                t1.ssp = '$ssp',
                t1.eng_name = '$commonName',
                t1.species_nr = '{$this->toSpecies->speciesNumber()}',
                t1.family = '$family'
            where
                t1.species_nr = '{$this->fromSpecies->speciesNumber()}' and
                t2.datasetid = {$this->set->id()}";
        app()->db()->query($updateFore);
        $this->nrForeUpdated = mysqli()->affected_rows;

        $sql = "
            select
                t1.snd_nr,
                t1.background
            from
                birdsounds as t1
            left join
                datasets_recordings as t2 on t1.snd_nr = t2.snd_nr
             where
                t1.back_nrs like '%{$this->fromSpecies->speciesNumber()}%' and
                t2.datasetid = {$this->set->id()}";
        $res = app()->db()->query($sql);
        while ($row = $res->fetch_object()) {
            $backSp[$row->snd_nr] = $row->background;
        }

        if (!empty($backSp)) {
            foreach ($backSp as $sndNr => $background) {
                // Code more or less identical to tools/update-bgspecies.php
                $match = false;
                $bG = explode(',', $background);
                foreach ($bG as $k => $v) {
                    $bG[$k] = trim($v);
                    if (strtolower(trim($v)) == strtolower(
                            $this->fromSpecies->scientificName()
                        )) {
                        $bG[$k] = $this->toSpecies->scientificName();
                        $match = true;
                    }
                }
                // Fallback for when common names have been used instead
                if (!$match) {
                    foreach ($bG as $k => $v) {
                        $bG[$k] = trim($v);
                        if (strtolower(trim($v)) == strtolower(
                                $this->fromSpecies->commonName()
                            )) {
                            $bG[$k] = $this->toSpecies->scientificName();
                            $match = true;
                        }
                    }
                }
                $newBg = implode(',', $bG);

                if ($match) {
                    populateBackgroundData(
                        $newBg,
                        $nrs,
                        $eng,
                        $latin,
                        $family,
                        $extra
                    );
                    $nrs = escape(implode(',', $nrs));
                    $eng = escape(implode(',', $eng));
                    $latin = escape(implode(',', $latin));
                    $family = escape(implode(',', $family));
                    $extra = escape(implode(',', $extra));

                    $sql = "
                        UPDATE birdsounds
                        SET
                            background='$newBg',
                            back_nrs='$nrs',
                            back_english='$eng',
                            back_latin='$latin',
                            back_families='$family',
                            back_extra='$extra'
                        WHERE
                            snd_nr=$sndNr";
                    app()->db()->query($sql);

                    $this->nrBackUpdated++;
                }
            }
        }
    }
}
