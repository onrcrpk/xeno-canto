<?php

namespace xc\Controllers\Admin;

use xc\User;

use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\userListHtml;

class Masquerade extends Controller
{

    public function handlePost()
    {
        $id = $this->request->request->get('u');
        $user = User::load($id);
        if ($user) {
            $user->setMasquerade(true);
            app()->session()->set('user', $user);
            notifySuccess("Masquerading as {$user->userName()}");
            return $this->seeOther(getUrl('index'));
        }

        notifyError("Couldn't find that user");
        return $this->seeOther(getUrl('admin-masquerade'));
    }

    public function switch($id, $referer = false)
    {
        // Coming from admin-masquerade-direct; return to page from which it was invoked
        if (!$referer) {
            $referer = $this->request->headers->get('Referer');
            // Coming from admin-masquerade-redirect; referer is provided as route in second argument
        } else {
            $referer = getUrl($referer);
        }

        $user = User::load($id);
        if ($user) {
            $user->setMasquerade(true);
            app()->session()->set('user', $user);
            notifySuccess("Masquerading as {$user->userName()}");
        } else {
            notifyError("Couldn't find that user");
        }
        return $this->seeOther($referer);
    }

    public function handleRequest()
    {
        $output = '
            <h1>Masquerade as another User</h2>
            
            <p>This page allows you to easily log in as another user to debug problems
            that they report with their accounts</p>
            ' . userListHtml(getUrl('admin-masquerade') . '/', 'index');

        /*
            <form method='post'>
            " . \xc\userSelectHtml() . "
            <input type='submit' name='login' value='Login' />
            </form>
            ";

        */

        return $this->template->render(
            $output,
            ['title' => 'Masquerade as another user']
        );
    }
}
