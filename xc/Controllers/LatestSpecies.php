<?php

namespace xc\Controllers;

use xc\Species;
use xc\XCRedis;

use function PHP81_BC\strftime;
use function xc\getUrl;

class LatestSpecies extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis($this->redisKey('hourly-stats-latest-species', $this->groupId()));
    }

    public function handleRequest()
    {
        $title = _('New species');
        $output = $this->redis->get();

        if (!$output) {
            $localName = localNameLanguage();
            $sql = "
                SELECT B.genus, B.species, B.eng_name, M.$localName as localName,
                    B.species_nr, B.datetime, B.dir, B.recordist
                FROM latest_species L
                INNER JOIN birdsounds B USING(snd_nr)
                LEFT JOIN taxonomy_multilingual M ON (M.species_nr=L.species_nr)
                WHERE " . Species::excludePseudoSpeciesSQL('B');
            if ($this->groupId()) {
                $sql .= " AND B.group_id = " . $this->groupId();
            }
            $sql .= " ORDER BY snd_nr desc LIMIT 100";

            $res = query_db($sql);

            $output = "
            <ul class='breadcrumbs'>
                <li><a href='" . getUrl('collection-details') . "'>" . _('Collection Details') . "</a></li>
                <li class='current'>$title</li>
            </ul>
            <div class='group-select'>" . $this->printGroupSelect() . "</div>
            <h1>$title</h1>
            <p>" .
                _(
                    'A list of the most recently added species in the xeno-canto collection, in reverse order of appearance.'
                ) . "
            </p>
 
            <table class='results'>
            <thead>
            <tr>
                <th>" . _('Species Name') . '</td>
                <th>' . _('Added by') . '</th>
                <th>' . _('Date first added') . '</th>
            </tr>
            </thead>
            ';

            while ($row = $res->fetch_object()) {
                // use translated names if possible
                $commonName = $row->eng_name;
                if ($row->localName) {
                    $commonName = $row->localName;
                }

                $sp = new Species($row->genus, $row->species, $commonName, $row->species_nr);
                $date_first_added = strftime('%d %B, %Y', strtotime($row->datetime));
                
                $output .= "
                <tr>
                    <td><a href='{$sp->profileURL()}'>{$sp->htmlDisplayName()}</td>
                    <td><a href='" . getUrl('recordist', ['id' => $row->dir]) . "'>{$row->recordist}</a></td>
                    <td>$date_first_added</td>
                </tr>
                ";
            }

            $output .= '
            </table>';

            $this->redis->set($output, 900);
        }
        return $this->template->render($output, ['title' => $title]);
    }
}
