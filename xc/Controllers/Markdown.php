<?php

namespace xc\Controllers;

use function xc\XC_formatUserText;

function demonstrate($str)
{
    return "
        <div class='markdown-example'>
        <p>" . _('What you type:') . "</p>
        <pre class='markdown-input'>" .
           htmlspecialchars($str, ENT_QUOTES) .
           '</pre>
        <p>' . _('What you see:') . "</p>
        <div class='markdown-output'>" .
           XC_formatUserText($str) .
           '</div>
        </div>
        ';
}


class Markdown extends Controller
{

    public function handleRequest()
    {
        $title  = _('Markdown Syntax Guide');
        $output = "<style type='text/css'>
            div.markdown-example
            {
                padding: 1em;
                margin-left: 2em;
                background-color: #EEEEEE;
                border-radius: 0.5em;
            }

            div.markdown-output,
                pre.markdown-input
            {
                margin-left: 2em;
                padding: 1em;
                background-color: #DFDFDF;
                border-radius: 0.5em;
            }

            </style>";

        $output .= "<h1>$title</h1>
            <p>" . _(
                "In various places throughout the site, you can format text using the <a 
            href='http://en.wikipedia.org/wiki/Markdown'>Markdown</a> text-formatting 
            syntax.  This tutorial explains some of the basics of using markdown to 
            format your messages."
            ) . '
            </p>';

        $output .= '<h2>' . _('Paragraphs') . '</h2>
            <p>' . _(
                'To create paragraphs, simply separate text by one or more blank lines.'
            ) . '</p>';
        $output .= demonstrate(
            'This is a paragraph. It has two sentences.

This is another paragraph. It also has 
two sentences.'
        );

        $output .= '<h2>' . _('Formatted Text') . '</h2>
            <p>' . _(
                'Bold or italic text can be generated as follows:'
            ) . '</p>';
        $output .= demonstrate(
            '*This* or _this_ is italicized text.  **This** or __this__ is boldface text.'
        );

        $output .= "<h2><a name='links'></a>" . _('Links') . '</h2>
            <p>' . _('There are several ways to create hyperlinks.') . '</p>';
        $output .= demonstrate(
            '
A basic link: <https://www.xeno-canto.org/26648>

A link with a title: [Willow Ptarmigan](https://www.xeno-canto.org/26648)

Not a link, just plain text: https://www.xeno-canto.org/26648'
        );

        $output .= '<h2>' . _('Lists') . '</h2>
            <p>' . _(
                'You can create both ordered and unordered lists as follows:'
            ) . '</p>';
        $output .= demonstrate(
            "
* an unordered list item
* another unordered item
* a sub-item (indented with 4 spaces)

1. an ordered list item
2. another ordered item
2. even if you don't write the numbers in the correct order,
it still works.
3. and another

This will not generate a list because it's not separated from the
paragraph text by at least one blank line:
* this is just a continuation of the paragraph
* and so is this"
        );

        $output .= '<h2>' . _('Headers') . '</h2>
            <p>' . _(
                "To create a header, simply surround your text with '#' characters.  The number of '#' characters determines the level of the header.  A top-level header would be specified as:"
            ) . '</p>
            <p># ' . _('Header Text') . ' #</p>
            ';
        $output .= demonstrate(
            "
# Phasianidae #
## Lagopus ##
### Willow Ptarmigan ###
#### The closing '#' characters are optional
This is some plain text"
        );

        $output .= '<h2>' . _('Quoting') . '</h2>
            <p>' . _(
                "To quote a block of text, prepend a '>' character  as the very first character on the line for each paragraph you are quoting.  Multiple levels of quoting can be achieved by adding additional quote characters."
            ) . ' ' . _(
                       'Here is an example of email reply style quoting:'
                   ) . '</p>
            ';
        $output .= demonstrate(
            "
> > Can you identify this bird?

> That's the song of *Megarynchus pitangua*

Thank you very much!"
        );
        $output .= '<p>' . _(
                'A demonstration of how to format a multiple-paragraph quotation'
            ) . '</p>';
        $output .= demonstrate(
            "
Text before the quote.

> This is a rather lengthy quoted section of text that spans multiple
paragraphs. Each consecutive quoted paragraph will be treated as if it were
part of the same quoted section, they won't be treated as several different
quotes.

> You can prefix every line of a paragraph with a quote character if you wish,
> but it's not strictly necessary. Only the character at the beginning of the
> paragraph is required.

Text after the quote.
"
        );
        $output .= '<h2>' . _('Images') . '</h2>
            <p>' . _(
                "To embed an image, you can use a format similar to the <a href='#links'>links</a> described above. An embedded image begins with a <tt>!</tt> character, followed by description of the image between square brackets (which is only displayed to users that have images disabled), followed immediately by the image URL enclosed within parentheses."
            ) . '</p>
            <p><strong>' . _(
                       'Please use images very sparingly.'
                   ) . '</strong></p>';
        $output .= demonstrate(
            'This is an image: ![A xeno-canto icon](https://www.xeno-canto.org/static/img/favicon.png)'
        );

        return $this->template->render($output, ['title' => $title]);
    }

}
