<?php

namespace xc\Controllers;

use function xc\getUrl;

class About extends Controller
{

    public function handleRequest()
    {
        $bobProfile = getUrl('recordist', ['id' => 'OH38YHKJBS']);
        $wpProfile = getUrl('recordist', ['id' => 'AEFZWKWLMO']);
        $sanderProfile = getUrl('recordist', ['id' => 'HMLZGZJENG']);
        $jonathonProfile = getUrl('recordist', ['id' => 'OJMFAOUBDU']);
        $ruudProfile = getUrl('recordist', ['id' => 'LNPVBAPBQQ']);
        $rolfProfile = getUrl('recordist', ['id' => 'PTYTXQRLSZ']);
        $memberUrl = getUrl('meetmembers');
        $termsUrl = getUrl('termsofuse');
        $newsUrl = getUrl('news-archives');
        $anbiUrl = getUrl('anbi-pbo');

        $output = '<h1>About</h1> 
           <p>' . sprintf(
                "www.xeno-canto.org (\"Xeno-canto\", \"XC\") is a website for sharing recordings of wildlife sounds from all across the world. " . "It was started in 2005 by %s and %s (see below for some <a href='%s'>history</a>).",
                "<a href='$bobProfile'>Bob Planqu&eacute</a>",
                "<a href='$wpProfile'>Willem-Pier Vellinga</a>",
                '#history'
            ) . ' ' . sprintf(
                "Xeno-canto is maintained by a small team of admins (%s, %s, %s, %s and %s) with crucial assistance from <a href='//naturalis.nl'>Naturalis Biodiversity Center</a>, especially %s, and <a href='%s'>all of the xeno-canto community</a>.",
                "<a href='$bobProfile'>Bob</a>",
                "<a href='$wpProfile'>WP</a>",
                "<a href='$sanderProfile'>Sander Pieterse</a>",
                "<a href='$jonathonProfile'>Jonathon Jongsma</a>",
                "<a href='$rolfProfile'>Rolf de By</a>",
                "<a href='$ruudProfile'>Ruud Altenburg</a>",
                $memberUrl
            ) . ' ' . sprintf(
                'Xeno-canto is run by the Xeno-canto foundation (or officially %s), a charity (Dutch "ANBI") from the Netherlands.',
                '<i>Stichting Xeno-canto voor natuurgeluiden</i>'
            ) . '
            </p>
            <h2>Aims</h2>
            <p>Xeno-canto uses the ever evolving possibilities of the internet to:' . "
            <ul>
              <li>popularise wildlife sound recording worldwide,
              <li>improve accessibility of wildlife sounds,
              <li>& increase knowledge of wildlife sounds.
            </ul>
            So, the recordings are here for
            <ul>
              <li>enjoyment,</li>
              <li>education,</li>
              <li>conservation,</li>
              <li> & science.</li>
            </ul>
            It works. Xeno-canto recordings are used in so many places and for so many purposes that it is impossible to give an overview. 
            Personally we are very proud of XC's contribution to BirdCLEF since 2014 and its impact on bird-sound recognition, 
            for which see <a href='https://scholar.google.com/scholar?hl=nl&as_sdt=0%2C5&q=%22xeno+canto%22+clef&btnG='>this Google Scholar page</a>.
            </p>
            <h2>" . _('Principles') . "</h2>
                <p>
              Xeno-canto has a number of basic principles.
            <ul>
            <li><i><b>Everyone</b></i> can make a valuable contribution, by adding recordings of course, but also by sharing opinions, 
            questioning identifications, solving mystery recordings, spotting bugs, programming, offering advice or experience, 
            and by spreading the word.</li>
            <li>Recordings are <i><b>shared</b></i> under various Creative Commons licenses. These differ between recordings 
            (they are always shown) but in general the allow distribution provided <b>recordists are credited</b> and 
            provided <b>no commercial proceeds are sought</b>. Read more about them in the <a href='$termsUrl'>terms of use</a>.
            <li><i><b>Any</b></i> recording is welcome. If you want to add your recording, of whatever quality, it is welcome, and will remain in the collection <b></i> provided the identification is 100% correct</i></b>. 
        (If the identification is not correct you will likely at some point receive an email that a fellow user has questioned the ID.)
        </li>
            <li>Xeno-canto values argued opinions, not authority. Identification of recordings remains tentative, 
            and recordings are continuously discussed and revised. When the identification of some recording is questioned, 
            the recording is taken out of the main collection until the matter is resolved. 
            These discussions are always visible on the recording's page and in the forum.</li>
            <li>Wherever possible we use open source resources.</li>
            </ul>
            </p>
            <p>
            <h2>Contributing</h2>
        
        Now, what is the point of uploading recordings when already 700,000+ (August 2022) are there?<br> 
        Simple: a <b><i>lot</i></b> is still missing. XC aims to set up a collection of <b><i>all</i></b> wildlife sounds, 
        representing
        <ul>
            <li> all taxa (rather than all species),
            <li> their complete repertoire,
            <li> all of the geographic variability,
            <li> at all stages of development.
        </ul>
        
        And organise that collection in such a way that XC becomes
        <ul>
            <li>the ultimate wildlife sound guide.
        </ul>
        <p>
        <b>What does this mean for XC?</b>
        <br>
        
        <ul>
        <li><b>XC is a long term project</b>
        <br>
        To collect all those sounds we have to be in it for the long term. XC continuously invests in
        <ul>
            <li>up-to-date durable hardware & software,</li>
            <li>good relations with sound recording enthousiasts world-wide.</li>
        </ul>
        </li>
        
        <li><b>XC needs to work on identification & classification</b>
        <br>
        Interestingly, at this moment we really do not know very well what is in the collection! 
        Suppose you upload a sound, we should really be figuring out if it is already represented in the collection, 
        by establishing to what extent it differs from what is present.
        We need to find ways to
        <ul>
            <li>recognise</li>
            <li>and classify</li>
        </ul>
        the content of the recordings. XC wants to try both <b>computational</b> as well as <b>crowdsourcing</b> solutions.
        </li>
        
        <li><b>XC needs open access</b>
        <br>
        We strongly feel that the best way of achieving the aims of XC is to open up the collection, and give the community 
        the opportunity to figure out ways how to contribute to it and to make use of it. 
        It has worked for us very well in the past and brought us to where we are now. We'll keep at it.
        </li>
        </ul>
        </p>
        
        <h2>Xeno-canto foundation</h2>
        <p>The Xeno-canto foundation (or the \"Stichting Xeno-canto voor natuurgeluiden\") is the legal body set up in the Netherlands 
        to run the website. As explained above, Xeno-canto has a rather ambitious program. If you upload recordings we assume 
        that you subscribe to those aims, as well as with actions that XC will take to further those aims. 
        Two relevant examples of such activities are improving accessibility of XC data by sharing them with others 
        (scientists, institutions) and applying for funds. We stress that although money is involved in such applications for funds, 
        it is not about selling your recordings, or the metadata, nor is it about making a profit, 
        it is about keeping the site up-and-running, up-to-date and in-flux and to better fulfil its aims.</p> 
        <p>The Xeno-canto foundation has an ANBI (\"Algemeen Nut Beogende Instelling\") or PBO 
        (\"Public Benefit Organisation\") status in the Netherlands that guarantees the financial aspect. Please see 
        <a target='_blank' href='https://www.belastingdienst.nl/wps/wcm/connect/bldcontenten/belastingdienst/business/other_subjects/public_benefit_organisations/'>an explanation of the status</a> and 
        the <a href='$anbiUrl'>required public documents supporting the status</a>.</p>
        
        
        <p>
        <h2>Terms of use</h2>
        In the <a href='$termsUrl'><b>terms of use</b></a> we indicate some rules that you subscribe to if you use www.xeno-canto.org and the recordings. 
        We also set out the kind of activities we may initiate to support the aims of XC.
        <i>We assume you have read the terms of use when you upload recordings or when you use the collection in any other way.</i>  
        </ul>
        </p>
        
        <p>
        <h2><a id=\"history\"></a>History</h2>
        <p>
        Xeno-canto was launched on the 30th of may 2005 by Bob and WP. 
        Below is the original announcement on NEOORN. 
        <p>
        <i>
        Hello NEOORN!
        <br>
        We have set up a new website for the identification of bird songs from
        tropical America:
        <br>
        www.xeno-canto.org
        <br>
        Its main aim is to collect recordings from Central and South America,
        such that they can be used to help identification of recordings you
        may have at home. The identification is done using easy
        characteristics such as length of the song, number of notes, speed and
        so on, but this may also be combined with habitat, elevation, country,
        etc. (This is possible because The Chicago University Press has given
        permission to use the databases by Parker et al (1996), that came with
        <i>Neotropical birds: Ecology and conservation</i> (Stotz et al 1996).)
        <br>
        The idea is to quickly focus on a short list of species which have
        songs similar to the one on your recording, and potentially present in
        the place where you were recording.
        <br>
        Everyone is encouraged to submit recordings to increase the scope of
        the collections. The ownership of these recordings remains with the
        recordists, and it will always be able to remove them if they choose
        to do so.
        <br>
        Note that at this moment the collection is still small, since we have
        only combined our own recording libraries. There are some 160 species.
        <br>
        However, work is underway to also be able to search through ALL
        species from the neotropics, by classifying sounds on published
        CD(-ROM)s. The resulting search results may then be used to listen to
        relevant bird songs in your own local library of CDs.
        <br>
        We hope that over time it will turn out to be a useful and enjoyable
        resource, and we also hope that it will generate a bit of honest web
        community effort. You're all invited to have a look around and
        participate!
        <br>
        Cheers,
        <br>
        Bob Planque and Willem-Pier Vellinga.
        </i>
        </P>
        <p>
        Within weeks of its opening, the major collections of Sjoerd Mayer,
        Sebastian Herzog and A. Bennett Hennessey, previously published on Mayer's
        seminal DVD-ROM of the Birds of Bolivia, gave the website great coverage of neotropical recordings.
        Nick Athanas quickly added hundreds upon hundreds of recordings from all over South America. These
        initial contributions created the main critical mass that attracted others.
        Check out the flow of events on <a href='$newsUrl'>the archive of the news page on XC.</a>
        </p>
        <p>
        Over the next months and years, coverage was expanded to North America, Africa and Asia, and finally Europe 
        and Australasia. In 2022, Xeno-canto widened its scope beyond birds, welcoming Orthoptera.
        </p>
        <p>
        In 2022, recordings are still being added at a phenomenal rate. The number of new recordists keeps growing as well, 
        their number is now above 8,800. Awesome.
        </p>";
        return $this->template->render($output, ['title' => _('About'), 'bodyId' => 'about']);
    }
}
