<?php

namespace xc\Controllers;

use xc\ForumPost;
use xc\HtmlUtil;
use xc\ThreadType;
use xc\User;

use function xc\getUrl;
use function xc\notifyError;
use function xc\sanitize;
use function xc\XC_logger;

class ForumTopicNew extends LoggedInController
{

    public function handlePost()
    {
        if (!$this->userCanPost()) {
            XC_logger()->logWarn(
                'User ' . User::current()->userName() .
                " attempted to create a forum topic without meeting requirements: [{$this->request->request->get('subject')}]"
            );
            notifyError($this->postingRequirementsMessage());
            return $this->seeOther(getUrl('new_thread'));
        }

        $xcid = sanitize($this->request->request->get('xcid'));
        $nr = 0;

        // Subject and message will be sanitized later!
        $subject = $this->request->request->get('subject');
        $messageText = $this->request->request->get('text');
        $type = sanitize($this->request->request->get('type'));
        $idq = sanitize($this->request->request->get('idq'));
        $error = false;

        if ($xcid || $xcid == '0') {
            $nr = str_ireplace('xc', '', $xcid);
            $res = query_db("SELECT snd_nr FROM birdsounds WHERE snd_nr='$nr'");
            if ($res && $res->num_rows != 1) {
                $error = true;
                notifyError(
                    sprintf(
                        _('Recording %s was not found.  It may have been deleted.'),
                        "<strong>XC{$nr}</strong>"
                    )
                );
            }
        }

        if (empty($subject)) {
            $error = true;
            notifyError(_('Message subject cannot be empty'));
        }

        if (empty($messageText)) {
            $error = true;
            notifyError(_('Message body cannot be empty'));
        }

        if (!$error && !$this->request->request->get('preview')) {
            if (intval($nr) > 0) {
                $subject .= " (XC$nr)";
                if ($idq == 1) {
                    $type = ThreadType::ID_QUESTIONED;
                } else {
                    $type = ThreadType::RECORDING_DISCUSS;
                }
            }

            $topic_nr = ForumPost::create($type, $subject, $messageText, $nr);

            if ($topic_nr) {
                return $this->seeOther(getUrl('discuss_forum', ['topic_nr' => $topic_nr]));
            } else {
                // couldn't insert into db
                $error = true;
                notifyError(_('Unable to save forum post'));
                XC_logger()->logError(
                    'DB error, unable to save forum post for user ' .
                    User::current()->userName() . ": [$subject]"
                );
            }
        }

        return $this->renderContent($xcid, $idq, $subject, $messageText, $type, $error);
    }

    protected function userCanPost()
    {
        return (User::current()->isVerified() && (User::current()->age() > $this->requiredAge()));
    }

    protected function requiredAge()
    {
        return 1;
    }

    protected function postingRequirementsMessage()
    {
        return sprintf(
                ngettext(
                    'In order to reduce spam, new users must verify their email address and wait at least %s hour before starting a new topic on the forum.',
                    'In order to reduce spam, new users must verify their email address and wait at least %s hours before starting a new topic on the forum.',
                    $this->requiredAge()
                ),
                $this->requiredAge()
            ) . ' ' .
            _('We apologize for the inconvenience.') . ' ' .
            sprintf(
                _("To re-send a verification code, please visit <a href='%s'>your account page</a>."),
                getUrl('mypage')
            );
    }

    protected function renderContent(
        $xcid,
        $idq = false,
        $subject = null,
        $messageText = null,
        $type = null,
        $error = false
    ) {
        $existingTopics = '';
        if ($xcid) {
            $nr = str_ireplace('xc', '', $xcid);
            if (intval($nr) != 0) {
                $res = query_db("SELECT topic_nr,subject,type FROM forum_world WHERE snd_nr='$nr'");
                if ($res && $res->num_rows > 0) {
                    $existingTopics = "
                        <div class='important'>
                        <p>" . sprintf(
                            _(
                                'There is already at least one forum topic about recording %s.  If you are trying to reply to a topic listed below, please click the link to reply instead of creating a new topic. If your message is about something else, please start a new topic using the form below.'
                            ),
                            "XC$nr"
                        ) .
                        '</p>
                        <ul>
                        ';
                    while ($row = $res->fetch_object()) {
                        $subj = sanitize($row->subject);
                        $existingTopics .= "<li><a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]) .
                            "'>$subj</a></li>";
                    }
                    $existingTopics .= '
                        </ul>
                        </div>
                        ';
                }
            }
        }

        $output = '
            <h1>' . _('New Forum Topic') . "</h1>
            $existingTopics
            ";

        $idqchecked = $idq ? 'checked' : '';
        $typeSelect = HtmlUtil::threadTypeSelect('general', 'type', empty($idq) ? $type : null);

        if ($messageText && !$error) {
            $previewText = ForumPost::formatBodyText($messageText);
            $output .= "
                <div class='forum-thread'>
                <p class='important'>" . _('This is only a preview, your comment has not been saved yet.') . '</p>
                <h2>' . htmlspecialchars($subject) . "</h2>
                <article>$previewText</article>
                </div>";
        }

        $output .= "
            <form class='new-message' method='post'>
            <input type='hidden' name='submit' value='1'/>
            <div>
            <p>
            <input autofocus class='message-subject' type='text' name='subject' placeholder='" .
            _('Subject') . "' value='" . sanitize($subject) . "'>
            </p>
            <p style='margin-top: 15px;'>" . _(
                'Select an (optional) general category for your topic. <strong>Note</strong>: If your post concerns the identification of a specific recording, please use the option of the bottom of this form instead.'
            ) . "
            </p>
            $typeSelect
            <p style='margin-top: 15px;'>" .
            sprintf(
                _('You can format your text using the %s text formatting syntax.'),
                "<a target='_blank' href='" . getUrl('markdown') . "'>Markdown</a>"
            )
            . "</p>
            <p>
            <textarea class='message-text' name='text' placeholder='" . _(
                'Message Text'
            ) . "'>$messageText</textarea>
            </p>
            </div>
            <div>
            <p style='margin-top: 15px;'>
            <strong>" . _('Note') . ': </strong>' .
            _(
                'If this topic concerns a specific recording, please enter the catalog number below so that a notification can be sent to the recordist'
            ) . ":
            </p>
            <p>
            <input type='text' name='xcid' value='{$xcid}' placeholder='" . _('Catalog Number') . "'>
            </p>
            <p><input type='checkbox' $idqchecked name='idq' value='1'> " .
            _('This topic concerns the identification of the recording') . ".</p>
            </div>

            <input name='preview' type='submit' value='" . _('Preview') . "'>
            <input name='submit' type='submit' value='" . _('Submit') . "'>
            </form>
            </div>
            ";

        return $this->template->render($output, ['title' => _('New Forum Topic'), 'bodyId' => 'new-topic',]);
    }

    public function handleRequest()
    {
        if (!$this->userCanPost()) {
            return "<p class='important'>{$this->postingRequirementsMessage()}</p>";
        }

        $xcid = $this->request->query->get('xcid', $this->request->query->get('frontID'));
        return $this->renderContent($xcid);
    }

}
