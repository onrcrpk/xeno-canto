<?php

namespace xc\Controllers;

use xc\User;
use xc\XCMail;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\XC_adminMailAddresses;

class NewsItem extends Controller
{

    public function handlePost($news_nr)
    {
        if (!$news_nr) {
            return $this->notFound();
        }

        $incomplete = false;
        $text = $this->request->request->get('text');

        if (!User::current()) {
            notifyError(_('You must be logged in to post a comment'));
            $incomplete = true;
        } elseif (!User::current()->isVerified()) {
            notifyError(
                _(
                    'You must verify your email address before posting a comment.'
                ) . ' ' .
                sprintf(
                    _(
                        "To re-send a verification code, please visit <a href='%s'>your account page</a>."
                    ),
                    getUrl('mypage')
                )
            );
            $incomplete = true;
        } elseif (empty($text)) {
            notifyError(_('You cannot post an empty comment.'));
            $incomplete = true;
        }

        if (!$incomplete) {
            $user = User::current();
            assert($user);

            $name = $user->userName();
            $dir = $user->userId();
            $Date = date('Y-m-d');
            $Time = time();
            $hours = (int)floor($Time / 3600) % 24 + 1;
            $minutes = (int)floor($Time / 60) % 60;
            if ($minutes < 10) {
                $minutes = "0$minutes";
            }
            $text = escape($text);
            $sql = "insert into discuss_news_world values (\"$text\",\"$name\",\"$Date\",\"$hours:$minutes\",\"$news_nr\",NULL, NOW(), '$dir')";
            if (!query_db($sql)) {
                notifyError('Database error: Unable to save reply');
            } else {
                $text = "$name responded to a news item:\n\n" . $text . "\n\n" .
                    getUrl(
                        'news_discussion',
                        ['news_nr' => $news_nr],
                        true
                    );

                (new XCMail(
                    XC_adminMailAddresses(),
                    '[xeno-canto] News item front page',
                    $text
                ))->send();
                notifySuccess(_('Comment posted'));
                return $this->seeOther(
                    getUrl('news_discussion', ['news_nr' => $news_nr])
                );
            }
        }

        return $this->renderContent($news_nr, $text);
    }

    protected function renderContent($news_nr, $replyText = null)
    {
        $title = sprintf(_('News discussion %d'), $news_nr);
        $item = \xc\NewsItem::load($news_nr);

        if (!$item) {
            return $this->notFound();
        }

        $output = "
            <div id='news-discussion'>
            <h1>News Item $news_nr</h1>" .
            $item->toHtml();

        if (User::current()) {
            $output .= '
                <h2>' . _('Reply') . "</h2>
                <p class='important'>" .
                sprintf(
                    _(
                        'You can format your text using the %s text formatting syntax.'
                    ),
                    "<a target='_blank' href='" . getUrl('markdown') . "'>Markdown</a>"
                )
                . "</p>
                <p>
                <form action='' method='post'>
                <input type='hidden' name='news_nr' value='$news_nr'/>
                <input type='hidden' name='submit' value='1'/>
                <textarea autofocus name='text' placeholder='" . htmlspecialchars(_('Your reply'), ENT_QUOTES) . "...'>$replyText</textarea>
                <input type='submit' value='" . htmlspecialchars(_('Submit'), ENT_QUOTES) . "'/>
                </form>
                ";
        }
        $output .= '</div>';

        return $this->template->render($output, ['title' => $title]);
    }

    public function handleRequest($news_nr)
    {
        return $this->renderContent($news_nr);
    }

}
