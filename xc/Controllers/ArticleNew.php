<?php

namespace xc\Controllers;

use xc\ArticleUtil;
use xc\User;
use xc\XCRedis;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\XC_logger;

class ArticleNew extends LoggedInController
{
    public function handlePost()
    {
        if (!$this->userCanPost()) {
            XC_logger()->logWarn(
                'User ' . User::current()->userName(
                ) . " attempted to create a new article without meeting requirements: [{$this->request->request->get('title')}]"
            );
            notifyError($this->postingRequirementsMessage());
            return $this->seeOther(getUrl('feature-new'));
        }

        $title  = escape($this->request->request->get('title'));
        $text   = escape($this->request->request->get('blog'));
        $author = escape($this->request->request->get('author'));
        $title  = escape($this->request->request->get('title'));
        $dir    = User::current()->userId();
        $public = intval($this->request->request->get('public'));
        query_db(
            "insert into blogs values ('$title', '$text', '', '$author', 'world', NOW(), '$dir', $public, NULL, NOW()) "
        );
        $res    = query_db('select LAST_INSERT_ID() as maxnr FROM blogs');
        $row    = $res->fetch_object();
        $blognr = $row->maxnr;
        if (!$blognr) {
            notifyError(_('Unable to create article'));
            return $this->seeOther(getUrl('features'));
        }

        $generatedText = ArticleUtil::generate_blog(
            $this->request->request->get('title'),
            $this->request->request->get('blog'),
            $blognr,
            $this->request->request->get('author')
        );
        $escaped       = escape($generatedText);
        query_db(
            "UPDATE blogs SET blog_as_html='$escaped' WHERE blognr=$blognr"
        );

        (new XCRedis())->clearArticleCache();

        return $this->seeOther(
            getUrl('feature-edit', ['blognr' => $blognr])
        );
    }

    protected function userCanPost()
    {
        return (User::current()->isVerified()
                && (User::current()->age() > $this->requiredAge()));
    }

    protected function requiredAge()
    {
        return 24;
    }

    protected function postingRequirementsMessage()
    {
        return sprintf(
            ngettext(
                       'In order to reduce spam, new users must verify their email address and wait at least %s hour before writing an article.',
                       'In order to reduce spam, new users must verify their email address and wait at least %s hours before writing an article.',
                       $this->requiredAge()
                   ),
            $this->requiredAge()
        ) . ' ' .
               _('We apologize for the inconvenience.') . ' ' .
               sprintf(
                   _(
                       "To re-send a verification code, please visit <a href='%s'>your account page</a>."
                   ),
                   getUrl('mypage')
               );
    }

    public function handleRequest()
    {
        if (!$this->userCanPost()) {
            XC_logger()->logWarn(
                'User ' . User::current()->userName(
                ) . ' wants to create a new article without meeting requirements'
            );
            return "<p class='important'>{$this->postingRequirementsMessage()}</p>";
        }

        $output = '
            <h1>' . _('Create a new Article') . "</h1>
            <section class='edit-feature'>
            <section class='tip'>
            <p>
            " .
                  sprintf(
                      _(
                          "Read the <a href='%s'>tutorial</a> for an overview of all the possibilities."
                      ),
                      getUrl(
                          'feature-view',
                          ['blognr' => 5]
                      )
                  )
                  . '</p>
                </section>';
        $output .= ArticleUtil::featureEditForm();

        $output .= '</section>';

        return $this->template->render(
            $output,
            [
                'title' => _(
                    'New Article'
                ),
            ]
        );
    }
}
