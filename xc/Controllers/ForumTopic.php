<?php

namespace xc\Controllers;

use xc\ForumPost;
use xc\ForumThread;
use xc\HtmlUtil;
use xc\MarkerMap;
use xc\Recording;
use xc\ThreadType;
use xc\User;
use xc\XCMail;
use xc\XCRedis;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\sanitize;

class ForumTopic extends Controller
{
    private $thread;

    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public function handlePost($topic_nr)
    {
        $replyText = $this->request->request->get('text');
        $isPreview = $this->request->request->get('preview', false);
        if (!$topic_nr) {
            return $this->notFound();
        }

        if ($this->request->request->get('update')) {
            return $this->POSTUpdate(
                $topic_nr,
                $this->request->request->get('type'),
                $this->request->request->get('branch')
            );
        } elseif ($this->request->request->get('admin-delete')) {
            return $this->POSTDelete($topic_nr);
        }

        if ($this->request->request->get('resolved')) {
            return $this->POSTResolve($topic_nr);
        } else {
            return $this->POSTReply($topic_nr, $replyText, $isPreview);
        }
    }

    protected function POSTUpdate($topic_nr, $type, $branch)
    {
        $user = User::current();
        if (!$user) {
            return $this->unauthorized();
        }

        $branch = escape($branch);
        query_db("UPDATE forum_world SET type=$type, branch='$branch' WHERE topic_nr=$topic_nr");
        query_db("delete from forum_topic_thread_types where topic_nr = $topic_nr");
        $res3 = query_db("SELECT snd_nr FROM forum_world WHERE topic_nr=$topic_nr");
        $row = $res3->fetch_object();
        if ($row->snd_nr) {
            query_db("UPDATE birdsounds SET discussed=$type WHERE snd_nr=$row->snd_nr");
        }

        $this->redis->clearForumCache();
        return $this->seeForumTopic($topic_nr);
    }

    private function seeForumTopic($topic_nr, $anchor = null)
    {
        $url = getUrl('discuss_forum', ['topic_nr' => $topic_nr]);
        if ($anchor) {
            $url .= "#$anchor";
        }
        return $this->seeOther($url);
    }

    private function POSTDelete($topic_nr)
    {
        $user = User::current();
        if (!($user && $user->isAdmin())) {
            return $this->unauthorized();
        }

        if ($this->request->request->get('confirm-delete') == 'yes') {
            query_db("DELETE FROM forum_world WHERE topic_nr=$topic_nr");
            query_db("DELETE FROM discussion_forum_world WHERE topic_nr=$topic_nr");
            query_db("DELETE FROM forum_topic_thread_types WHERE topic_nr = $topic_nr");
            $this->redis->clearForumCache();
            return $this->seeOther(getUrl('forum'));
        }

        $res = query_db("SELECT subject FROM forum_world WHERE topic_nr=$topic_nr");
        $row = $res->fetch_object();
        $subj = $row->subject;
        $output = "<h1>Delete Topic</h1>
            <p>Do you really want to delete forum thread $topic_nr: <em>$subj</em>?</p>
            <form method='post'>
            <input type='hidden' name='topic_nr' value='$topic_nr'>
            <input type='hidden' name='confirm-delete' value='yes'>
            <input type='submit' name='admin-delete' value='Delete topic'/>
            </form>";

        return $this->template->render(
            $output,
            ['title' => "Delete Forum thread $topic_nr?"]
        );
    }

    protected function POSTResolve($topic_nr)
    {
        $user = User::current();
        if (!$user) {
            return $this->unauthorized();
        }
        query_db(
            "INSERT IGNORE INTO forum_topic_thread_types VALUES ($topic_nr, " . ThreadType::REPORT_RESOLVED . ')'
        );
        notifySuccess(
            _(
                "Thank you for notifying the admins that this recording's identification discussion has been resolved!"
            )
        );
        $this->redis->clearForumCache();
        return $this->seeForumTopic($topic_nr);
    }

    private function POSTReply($topic_nr, $replyText, $isPreview)
    {
        $replyFailed = false;
        if (User::current()) {
            if (!User::current()->isVerified()) {
                $replyFailed = true;
                notifyError(
                    _('You must verify your email address before posting a comment.') . ' ' .
                    sprintf(
                        _(
                            "To re-send a verification code, please visit <a href='%s'>your account page</a>."
                        ),
                        getUrl('mypage')
                    )
                );
            }
            if (!$this->request->request->get('topic_nr')) {
                $replyFailed = true;
                notifyError('Internal error: No topic specified');
            }
            if (empty($replyText)) {
                $replyFailed = true;
                notifyError(_('You cannot post an empty comment.'));
            }

            if (!$replyFailed) {
                if (!$isPreview) {
                    if ($this->processReply($topic_nr, $replyText)) {
                        $this->redis->clearForumCache();
                        return $this->seeForumTopic($topic_nr, 'last');
                    } else {
                        notifyError('Database error: Unable to save reply');
                    }
                }
            }
        } else {
            notifyError(_('You must be logged in to post a comment'));
        }

        return $this->renderContent($topic_nr, $replyText);
    }

    private function processReply($topic_nr, $replyText)
    {
        assert(User::current());

        $dir = User::current()->userId();

        // attempt to prevent duplicate submissions
        $res = query_db(
            "SELECT dir, text from discussion_forum_world WHERE topic_nr=$topic_nr ORDER BY mes_nr DESC LIMIT 1"
        );
        $row = $res->fetch_object();
        if (
            $row
            && ($row->text === $replyText)
            && ($row->dir === $dir)
        ) {
            return true;
        }

        $name = escape(User::current()->userName());
        $Date = date('d-m-Y');
        $tm = date('H:i');
        $etext = escape($replyText);
        $sql = "insert into discussion_forum_world values ('$name','$etext','$Date','$tm','','$dir',$topic_nr,NULL, NOW())";
        if (query_db($sql)) {
            // Update number of replies (XC-46)
            query_db("update `forum_world` set `replies` = (`replies` + 1) where `topic_nr` = $topic_nr");

            $this->notifyTopicParticipants($topic_nr, $replyText);
            notifySuccess(_('Comment posted'));
            return true;
        }
        return false;
    }

    private function notifyTopicParticipants($nr, $text)
    {
        assert(User::current());

        $nr = intval($nr);
        $userid = User::current()->userId();
        $res = query_db(
            "SELECT
            users.email, users.dir, users.username, subject, type
            FROM
            forum_world AS forum
            LEFT JOIN
            users ON forum.userid = users.dir
            WHERE
            forum.topic_nr=$nr"
        );
        $row = $res->fetch_object();
        if (!$row) {
            return;
        }

        $addresses = [];
        // always notify the topic originator (unless this is the topic originator
        // posting a comment)
        if ($row->dir != $userid) {
            $addresses[$row->email] = $row->username;
        }

        $type = intval($row->type);
        $subject = $row->subject;
        $commenter = User::current()->userName();

        // get a list of all topic participants and their email addresses
        $sql = "
            (SELECT
            users.email, users.username
            FROM
            discussion_forum_world AS cmt
            LEFT JOIN
            users USING(dir)
            WHERE
            cmt.topic_nr=$nr AND dir!='$userid')

            UNION
            (SELECT
            users.email, users.username
            FROM
            forum_world AS forum
            INNER JOIN
            birdsounds USING(snd_nr)
            INNER JOIN
            users USING(dir)
            WHERE
            forum.topic_nr=$nr AND dir!='$userid')
            ";

        $res = query_db($sql);
        while ($row = $res->fetch_object()) {
            $addresses[$row->email] = $row->username;
        }

        if (!empty($addresses)) {
            $wrappedText = wordwrap($text, 60);
            foreach ($addresses as $email => $username) {
                $messageBody = <<<EOT
Dear {$username},

xeno-canto user $commenter has posted the following comment on
a forum topic that you have participated in:

Topic: $subject

$wrappedText
EOT;
                $message = ForumPost::formatNotificationEmail(
                    $nr,
                    $messageBody
                );
                (new XCMail(
                    $email,
                    "[xeno-canto] New comment on forum topic $nr",
                    $message
                ))->send();
            }
        }
    }

    protected function renderContent($topic_nr, $replyText = null)
    {
        $topic_nr = intval($topic_nr);
        $sql = "select subject, snd_nr from forum_world where topic_nr='$topic_nr'";
        $res = query_db($sql);
        $row = $res->fetch_object();
        if (!$row) {
            return $this->notFound();
        }

        $map = null;
        $topic = $row->subject;

        $rec = '';
        if (!empty($row->snd_nr)) {
            $r = Recording::load($row->snd_nr);

            if ($r) {
                $userHasAccess = !$r->restrictedForUser(User::current());
                $rec = $r->player(
                    [
                        'showLocation' => $userHasAccess,
                        'showDate' => true,
                        'linkFields' => $userHasAccess,
                    ]
                );
                if ($r->hasCoordinates() && $userHasAccess) {
                    $rec .= "<div id='map-canvas'></div>";
                    $map = new MarkerMap(
                        'map-canvas',
                        [
                            [
                                'latitude' => $r->latitude(),
                                'longitude' => $r->longitude(),
                                'title' => $r->location(),
                                'id' => $r->xcid(),
                            ],
                        ],
                        $r->species()->rangeMapURL()
                    );
                }
            } else {
                $rec = "<p class='important'>" .
                    sprintf(
                        _('Recording %s was not found.  It may have been deleted.'),
                        "<strong>XC{$row->snd_nr}</strong>"
                    ) . '</p>';
            }
        }

        // Create the thread before the actions form is generated, as we need info on the number of replies
        $this->thread = new ForumThread($topic_nr);

        $output = "<ul class='breadcrumbs'>
            <li><a href='" . getUrl('forum') . "'>" . _('Forum') . "</a></li>
            <li class='current'>$topic</li>
            </ul>" .
            //add a drop down menu to admin the thread type
            $this->renderActionsForm($topic_nr) . "
            <div>
            <section class='column forum-thread'>";

        $output .= $this->thread->generateHTML();

        if (User::current()) {
            $output .= "
                <!-- reply to this forum -->
                <a name='reply'></a>
                <h2>" . _('Reply to this topic') . '</h2>';

            if (!empty($replyText)) {
                $output .= "<p class='important'>" .
                    _(
                        'This is only a preview, your comment has not been saved yet.'
                    ) . "</p>
                    <article class='comment-preview'> " .
                    ForumPost::formatBodyText($replyText) . '
                    </article>
                    ';
            }

            $output .= "
                <form class='new-message' action='#reply' method='post'>
                <input type='hidden' name='topic_nr' value='$topic_nr'>
                <input type='hidden' name='submit' value='1'>
                <p>" .
                sprintf(
                    _('You can format your text using the %s text formatting syntax.'),
                    "<a target='_blank' href='" . getUrl('markdown') . "'>Markdown</a>"
                )
                . ' ' .
                _(
                    'Typing the catalogue number for a recording (for example XC20464) will cause a player for that recording to be appended to your comment.'
                ) . "</p>
                <p>
                <p>
                <textarea class='message-text' name='text' placeholder='" .
                sanitize(_('Message Text')) . "'>{$this->request->request->get('text')}</textarea>
                </p>
                <input type='submit' name='preview' value='" . sanitize(_('Preview')) . "'>
                <input type='submit' name='submit' value='" . sanitize(_('Submit')) . "'>
                </form>
                ";
        } else {
            $output .= '
                <p>' .
                sprintf(
                    _("<a href='%s'>Log in</a> to reply to this topic."),
                    getUrl('login')
                ) . '
                </p>';
        }

        $output .= "
            </section>
            <section id='forum-thread-recording' class='column'>
            $rec
            </section>
            </div>";

        if ($map) {
            $output .= $map->getJS();
        }

        return $this->template->render(
            $output,
            ['title' => "$topic :: topic $topic_nr"]
        );
    }

    private function renderActionsForm($topic_nr)
    {
        $user = User::current();
        $menu = '';

        // Better safe than sorry
        if (!$this->thread) {
            $this->thread = new ForumThread($topic_nr);
        }

        if ($user) {
            $q = "
                select t1.`type`, t1.`branch`, t1.`userid`, t2.`type_id` as resolved
                from forum_world as t1
                left join forum_topic_thread_types as t2 on t1.topic_nr = t2.topic_nr
                where t1.topic_nr = '$topic_nr'";
            $res = query_db($q);
            $row = $res->fetch_object();

            /*
             * XC-29: user can modify his own thread type has been disabled;
             * replace line 140 with the one below to enable editing.
             *
             if ($user->isAdmin() || $user->userId() == $row->userid) {
             */

            $menu = "
                <form method='post'>
                <input type='hidden' name='topic_nr' value='$topic_nr'>\n";

            if ($user->isAdmin()) {
                $select = '';
                if ($user->isAdmin()) {
                    $select = "
                        <img class='icon' src='/static/img/admin-16.png'/> " .
                        HtmlUtil::threadTypeSelect('admin', 'type', $row->type) .
                        HtmlUtil::regionSelect('branch', $row->branch);
                } elseif ($user->userId() == $row->userid) {
                    $select = '
                     <h2>Topic category</h2>' .
                        HtmlUtil::threadTypeSelect('general', 'type', $row->type);
                }

                $menu .=
                    $select . "
                    <input type='submit' name='update' value='Update'>\n";
                if ($user->isAdmin()) {
                    $menu .= "<input type='submit' name='admin-delete' value='Delete thread'/>\n";
                }
            }

            // Report id/mystery as solved
            if (
                $row->type == ThreadType::ID_QUESTIONED ||
                ($row->type == ThreadType::MYSTERY && $this->thread->nrReplies() >= 1)
            ) {
                $disabled = $row->resolved == ThreadType::REPORT_RESOLVED ? 'disabled' : '';
                $menu .= "<p><input type='submit' name='resolved' $disabled value='Report as resolved'/></p>\n";
            }

            $menu .= '</form>';
        }

        return $menu;
    }

    public function handleRequest($topic_nr)
    {
        if (!$topic_nr) {
            return $this->notFound();
        }

        return $this->renderContent($topic_nr);
    }
}
