<?php

namespace xc\Controllers;

use xc\Query;
use xc\Recording;
use xc\Set;
use xc\User;

use function PHP81_BC\strftime;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\sanitize;

class SetAddRecording extends LoggedInController
{
    private $set;

    public function chooseSetForRecording($xcid)
    {
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $sets = Set::loadUserSets(User::current()->userId(), true);

        $title = _('Add Recording to Set');
        $output = "<h1>$title</h1>
            <p>" . _('Choose a set for the following recording:') . "  XC{$rec->xcid()} {$rec->htmlDisplayName(false)}</p>
            <form action='" . getUrl('recording-add-to-set-post', ['xcid' => $xcid]) . "' method='post'>";

        foreach ($sets as $set) {
            $disabled = '';
            if ($set->containsRecording($rec)) {
                $disabled = 'disabled';
            }
            $output .= "
            <div>
                <input $disabled type='radio' name='setid' value='{$set->id()}' id='set-input-{$set->id()}' /><label for='set-input-{$set->id()}'>{$set->name()}</label>
            </div>";
        }
        $output .= "
            <div>
            <input type='radio' name='setid' value='new' id='new-set-input' /><label for='new-set-input'>" .
            _('Create a new set') . "</label>
            </div>
            <div>
            <input type='submit' value='" . sanitize(_('Add to Set')) . "'/>
            </div>
            <input type='hidden' name='redirect' value='{$this->request->headers->get('Referer')}'/>
            </form>";

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'set-add']);
    }

    public function chooseSetForQuery($query)
    {
        if (empty($query)) {
            return $this->badRequest('Query cannot be empty');
        }

        $q = new Query($query);
        //$q->useLocalNameSearch();
        $sets = Set::loadUserSets(User::current()->userId(), true);

        $title = _('Add Recordings to Set');
        $output = "<h1>$title</h1>";

        if ($q->numRecordings() == 0) {
            $output .= "<div class='error'>" .
                sprintf(_("Query '%s' does not match any recordings"), $query) . '</div>';
        } else {
            $output .= '<p>' . sprintf(
                    _("The query '%s' matches %d recordings. Add all recordings to the following set:"),
                    "<strong>$query</strong>",
                    $q->numRecordings()
                ) . "
                </p>
                <form action='" . getUrl('set-add-query-post', ['query' => $query]) . "' method='post'>";

            foreach ($sets as $set) {
                $output .= "
                    <div>
                    <input type='radio' name='setid' value='{$set->id()}' id='set-input-{$set->id()}' /><label for='set-input-{$set->id()}'>{$set->name()}</label>
                    </div>";
            }
            $output .= "
                <div>
                <input type='radio' name='setid' value='new' id='new-set-input' /><label for='new-set-input'>" . _(
                    'Create a new set'
                ) . "</label>
                </div>
                <div>
                <input type='submit' value='" . sanitize(_('Add to Set')) . "'/>
                </div>
                <input type='hidden' name='redirect' value='{$this->request->headers->get('Referer')}'/>
                </form>";
        }

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'set-add']);
    }

    public function addRecordingToSet($xcid)
    {
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $setid = $this->request->request->get('setid');
        if (!$setid) {
            notifyError(_('Please choose a set'));
            return $this->seeOther(getUrl('recording-add-to-set', ['xcid' => $xcid]));
        }

        $this->setSet($setid);
        $this->set->addRecording($rec);

        notifySuccess(
            sprintf(
                _('Added recording %s to set %s'),
                "XC{$rec->xcid()}",
                "<a href='" . getUrl('set', ['id' => $this->set->id()]) . "'>{$this->set->name()}</a>"
            )
        );
        return $this->seeOther($this->getRedirect());
    }

    private function setSet($setid)
    {
        if ($setid === 'new') {
            $name = _('New Set') . ' - ' . strftime('%c');
            $this->set = Set::create($name, '', User::current()->userId(), false);
        } else {
            $this->set = Set::load(intval($setid));
        }

        if (!$this->set) {
            return $this->internalServerError();
        }

        if ($this->set->userId() != User::current()->userId() && !User::current()->isAdmin()) {
            return $this->internalServerError('no permission');
        }
    }

    private function getRedirect()
    {
        return $this->request->request->get('redirect', $this->set->url()) ?: getUrl('index');
    }

    public function addQueryToSet($query)
    {
        if (empty($query)) {
            return $this->badRequest('Query cannot be empty');
        }

        $q = new Query($query);
        //$q->useLocalNameSearch();
        $setid = $this->request->request->get('setid');
        if (!$setid) {
            notifyError(_('Please choose a set'));
            return $this->seeOther(getUrl('set-add-query', ['query' => $query]));
        }

        $this->setSet($setid);
        $results = $q->execute();

        $recs = [];
        while ($row = $results->fetch_object()) {
            $recs[] = new Recording($row);
        }

        $n = $this->set->addRecordings($recs);
        notifySuccess(
            sprintf(
                _('Added %d recordings to set %s'),
                $n,
                "<a href='" . getUrl('set', ['id' => $this->set->id()]) . "'>{$this->set->name()}</a>"
            )
        );

        return $this->seeOther($this->getRedirect());
    }

}
