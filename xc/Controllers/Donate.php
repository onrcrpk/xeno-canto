<?php

namespace xc\Controllers;

use xc\User;

use function xc\getUrl;
use function xc\strip;

class Donate extends Controller
{

    public function handleRequest()
    {
        $userName = User::current() ? User::current()->userName() : '';
        $form = '';
        $bodyText = [
            _(
                "The mission of Xeno-canto (\"XC\") is to make recordings of wildlife sound available to the public and to popularise and encourage the study of those sounds."
            ),
            _(
                'Since its inception in 2005, XC has grown into a community of thousands of birders, biologists, conservationists and other sound enthusiasts, all volunteers, that together have amassed and curated anenormous open collection. The sounds are used all over the web, in scientific papers, by birders studying their bird sounds or planning their next trips etc. Together we are shaping this incredible journey.'
            ),
            _(
                'XC is built on open software, it does not receive regular subsidies, and it does not show ads. XC so far has managed to survive because of a number of generous and loyal partners, that have become ever more important over the years, and we will stick with them. However, it is important that XC maintains its independence as well and that is where you can help out: donate!'
            ),
            _(
                'Donations are important to maintain our independence, to maintain the website, and to develop it in new directions.'
            ),
            sprintf(
                _(
                    'Every donation, no matter how small, counts! You can choose to donate once or monthly. After returning from PayPal, you can optionally add your name to our %s.'
                ),
                sprintf(
                    "<a href='%s'>%s</a>",
                    getUrl('donors'),
                    _(
                        'list of donors'
                    )
                )
            ),
            _('<h3>Thanks for supporting Xeno-canto!</h3>'),
        ];

        if ($this->payPalCancel()) {
            $title = _('Donation cancelled');
            $body = implode('</p><p>', $bodyText);
        } elseif ($this->payPalSuccess()) {
            $title = _('Donation successful');
            $body = _('Thank you for your donation!');

            if ($this->donationFormVisible()) {
                $body .= '</p><p>' . sprintf(
                        _(
                            'Your donation to XC is anonymous by default. If you want your name to appear in the list of donors, you can add your name to finish your donation. If you want to remain anonymous, you can %s.'
                        ),
                        sprintf(
                            "<a href='%s'>%s</a>",
                            getUrl('index'),
                            _(
                                'return to the home page'
                            )
                        )
                    );
                $form = '
                    <form action="' . getUrl('donor-add') . '" method="post">
                    <input type="hidden" name="public" value=1>
                    <table class="settings-form">
                    <tr>
                        <td><label for="name">' . _('Name') . '</label>
                        <td><input type="text" name="name" value="' . $userName . '" /></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td><input type="submit" value="' . _(
                        'Add my name to donors'
                    ) . '"></td>
                    </tr>
                    </table>
                    </form>        
                ';
            }
            $this->logDonation();
        } else {
            $title = _('Support the Xeno-canto project with a donation');
            $body = implode('</p><p>', $bodyText);
            $form = '
                <form action="https://www.paypal.com/donate" method="post" target="_top">
                <input type="hidden" name="hosted_button_id" value="Z6LTK644MVKGE" />
                <input type="image" src="https://www.paypalobjects.com/en_US/NL/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
                <img alt="" border="0" src="https://www.paypal.com/en_NL/i/scr/pixel.gif" width="1" height="1" />
                </form>        
            ';
        }

        $output = "
            <h1>$title</h1>
            <p>$body<p>
            <p style='margin-top: 25px;'>$form</p>
        ";

        return $this->template->render($output, ['title' => $title]);
    }

    private function payPalCancel()
    {
        return $this->request->query->get('cancel');
    }

    private function payPalSuccess()
    {
        return $this->request->query->get('success') && ($this->request->query->get('add') == PAYPAL_SUCCESS_ID);
    }

    private function donationFormVisible()
    {
        if (!$this->donationIsLogged()) {
            return true;
        }
        $res = query_db(
            "
            SELECT `id` 
            FROM donations 
            WHERE 
                `session_id` = '" . $this->getSessionId() . "' AND 
                `modified` > DATE_SUB(NOW(), INTERVAL 1 HOUR) AND 
                `public` = 0
        "
        );
        return $res->num_rows == 1;
    }

    private function donationIsLogged()
    {
        $res = query_db(
            "
            SELECT `id` 
            FROM donations 
            WHERE `session_id` = '" . $this->getSessionId() . "' AND `modified` > DATE_SUB(NOW(), INTERVAL 1 HOUR)
        "
        );
        return $res->num_rows == 1;
    }

    private function getSessionId()
    {
        return app()->session()->getId();
    }

    private function logDonation()
    {
        if (!$this->donationIsLogged() && app()->session()->getId()) {
            $dir = $name = '';
            if (User::current()) {
                $dir = User::current()->userId();
                $name = User::current()->userName();
            }
            query_db(
                "
                INSERT INTO donations (`dir`, `name`, `session_id`) 
                VALUES ('$dir', '$name', '" . $this->getSessionId() . "')
            "
            );
        }
    }

    public function addDonor()
    {
        $name = strip($this->request->request->get('name'), true);
        $public = $this->request->request->get('public');

        query_db(
            "
            UPDATE donations 
            SET `name` = '$name', `public` = $public, `modified` = NOW() 
            WHERE `session_id` = '" . $this->getSessionId() . "'"
        );

        return $this->seeOther(getUrl('index'));
    }

    public function donors()
    {
        $title = _('Xeno-canto donors');
        $output = "
            <h1>$title</h1>
            <p>" . _(
                'The following persons or organisations have donated to Xeno-canto'
            ) . ':</p>
            <ul>';

        $res = query_db(
            '
            SELECT `name`, date(`modified`) AS `date`
            FROM donations 
            WHERE `public` = 1 
            ORDER BY modified DESC
            LIMIT 0, 100'
        );
        while ($row = $res->fetch_object()) {
            $name = $row->name ?: _('Anonymous');
            $output .= "<li>$name</li>";
        }
        $output .= '</ul>';

        return $this->template->render($output, ['title' => $title]);
    }

}
