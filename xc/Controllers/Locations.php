<?php

namespace xc\Controllers;

use xc\LocationMap;
use xc\Query;
use xc\User;

use function xc\getUrl;

class Locations extends Controller
{

    public function handleRequest()
    {
        $user = User::current();

        if (!($user && $user->isAdmin())) {
            return $this->notFound('<p>This page is temporarily disabled for performance reasons</p>');
        }

        $queryString = $this->request->query->get('query');
        $res = null;
        if ($queryString) {
            $q = new Query($queryString);
            $q->excludeRestrictedSpecies();
            $nr_species = $q->numSpecies();
            $res = $q->getLocations();
        }

        $map = new LocationMap('map-canvas', $res);
        $nr_locations = $res->num_rows;

        $script = $map->getJS(false);
        $apiUrl = getUrl('api-location-species-list');
        $browseUrl = getUrl('browse');
        $script .= <<<EOT
<script type="text/javascript">
jQuery(document).ready(function() {

    function locationMarkerClicked()
    {
        var loc = this.title;
        jQuery('#species-list').html('<p>Loading species list for ' + loc + '...</p>');
        jQuery.get("{$apiUrl}", {location: loc},
            function(data) {
                re = / /g;
                var fg = data[0];
                var html = '';
                if (fg.length) {
                    var qstr = 'loc:"' + loc + '"';
                    html += "<p>Species <a href='$browseUrl?query=" + encodeURIComponent(qstr) + "'>recorded</a> at " + loc + "</p><ul>";
                    for (var i = 0; i < fg.length; i++)
                    {
                        var qstr = "sp:" + fg[i].nr + ' loc:"' + loc + '"';
                        html += "<li><a href='$browseUrl?query=" + encodeURIComponent(qstr) + "'>" + fg[i].commonName + "</a></li>";
                    }
                    html += "</ul>";
                }
                bg = data[1];
                if (bg.length) {
                    html += "<p>Background species</p><ul>";
                    for (var i = 0; i < bg.length; i++)
                    {
                        html += "<li>" + bg[i].commonName + "</li>";
                    }
                    html += "</ul>";
                }
                console.log("setting html " + html);
                jQuery('#species-list').html(html);
            }, 'json');
    }

    map = xc.initMap();
    map.registerMarkerEventHandler("click", locationMarkerClicked);
});
</script>
EOT;

        $title = _('Find Locations');
        $output = "<h1>$title</h1>
            <form method='get'>
            <input id='location-search-input' type='text' name='query' value='" . htmlspecialchars(
                $queryString,
                ENT_QUOTES
            ) . "'>
            <input type='submit' value='" . htmlspecialchars(
                _('Search'),
                ENT_QUOTES
            ) . "'>
            </form>
            ";

        $stats = '';
        $mapInstructions = '';
        if ($res) {
            $stats = '<p>' . sprintf(
                    _('Found %s species at %s locations.'),
                    "<b>$nr_species</b>",
                    "<b>$nr_locations</b>"
                ) . '
                </p>';
            $mapInstructions = _(
                'Click a location marker on the map to see a list of all species recorded at that location'
            );
        }

        $output .= "
            $stats
            <section id=\"map-column\" class=\"column\">
            <div id=\"map-canvas\"></div>
            <p>" . _(
                'The map displays the geographical locations that have recordings that match your search query.'
            ) . "</p>
            </section>
            <section id=\"species-list\" class=\"column\">
            <p><em>$mapInstructions</em></p>
            </section>
            $script";

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'locations']
        );
    }

}
