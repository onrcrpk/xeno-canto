<?php

namespace xc\Controllers;

use Symfony\Component\HttpFoundation\Response;

abstract class ApiMethod extends ControllerBase
{

    public const CLIENT_ERROR = 'client_error';

    public const SERVER_ERROR = 'server_error';

    public function authCheck()
    {
        return null;
    }

    protected function methodNotSupported()
    {
        return $this->respond(
            [
                'error' => ApiMethod::CLIENT_ERROR,
                'message' => 'Method not supported',
            ],
            Response::HTTP_METHOD_NOT_ALLOWED
        );
    }

    public function respond($object, $code = Response::HTTP_OK)
    {
        $response = new Response();
        $response->setStatusCode($code);

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set(
            'Access-Control-Allow-Headers',
            'DNT, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Range, Authorization'
        );
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $response->headers->set('Access-Control-Expose-Headers', 'Content-Length, Content-Range');
        $response->headers->set('Allow', 'GET, POST, OPTIONS');
        if ($this->request->isMethod("OPTIONS")) {
            $response->headers->set('Access-Control-Max-Age', 1728000);
            $response->headers->set('Content-Type', 'text/plain; charset=utf-8');
            $response->headers->set('Content-Length', 0);
        } else {
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode($object));
        }

        $response->prepare($this->request);
        return $response;
    }

    protected function notImplemented()
    {
        return $this->respond(
            [
                'error' => ApiMethod::SERVER_ERROR,
                'message' => 'Not Implemented',
            ],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    protected function forbidden()
    {
        return $this->respond(['success' => false], Response::HTTP_FORBIDDEN);
    }

    protected function missingParameter($paramName)
    {
        return $this->respond(
            [
                'error' => ApiMethod::CLIENT_ERROR,
                'message' => "'$paramName' parameter is required; see https://xeno-canto.org/explore/api for documentation",
            ],
            Response::HTTP_BAD_REQUEST
        );
    }
}
