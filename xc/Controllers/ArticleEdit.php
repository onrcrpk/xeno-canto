<?php

namespace xc\Controllers;

use xc\ArticleUtil;
use xc\XCRedis;

use function xc\escape;
use function xc\getUrl;
use function xc\notifySuccess;

class ArticleEdit extends LoggedInController
{
    public function handleDeletePost($blognr)
    {
        if (empty($blognr)) {
            return $this->notFound();
        }

        // deletion has already been confirmed
        $blognr = escape($blognr);
        query_db("DELETE from blogs where blognr=$blognr");
        query_db("DELETE from feature_discussions where blognr=$blognr");
        notifySuccess(_('Article was successfully deleted'));

        (new XCRedis())->clearArticleCache();

        return $this->seeOther(getUrl('features'));
    }

    public function handlePost($blognr)
    {
        // the 'edit' form was posted
        $blognr = escape($blognr);
        $title  = escape($this->request->request->get('title'));
        $text   = escape($this->request->request->get('blog'));
        $author = escape($this->request->request->get('author'));
        $title  = escape($this->request->request->get('title'));
        $public = intval($this->request->request->get('public'));
        $sql    = "UPDATE blogs SET title='$title', blog='$text', author='$author', date_last_update=NOW(), published=IF(!public and $public, NOW(), published), public=$public where blognr=$blognr";
        query_db($sql);
        $generatedText = ArticleUtil::generate_blog(
            $this->request->request->get('title'),
            $this->request->request->get('blog'),
            $blognr,
            $this->request->request->get('author')
        );
        $escaped       = escape($generatedText);
        if (
            query_db(
                "UPDATE blogs SET blog_as_html='$escaped' WHERE blognr=$blognr"
            )
        ) {
            notifySuccess(_('Article was successfully saved'));
        }

        (new XCRedis())->clearArticleCache();

        return $this->seeOther(
            getUrl('feature-view', ['blognr' => $blognr])
        );
    }

    public function handleDeleteRequest($blognr)
    {
        $blognr    = escape($blognr);
        $pageTitle = _('Delete Article?');
        $res       = query_db("select * FROM blogs WHERE blognr=$blognr");
        $output    = '';
        if (!$res || $res->num_rows == 0) {
            return $this->notFound();
        }

        $output .= "
            <h1>$pageTitle</h1>

            <section class='edit-feature'>
            <p class='important'>"
                   . sprintf(
                       _(
                           'Are you sure you want to delete article %d?  This action cannot be undone.'
                       ),
                       $blognr
                   ) . '</p>
            ' .
                   ArticleUtil::featureDeleteForm($blognr, 'post')
                   . "
            <p><a href='" . getUrl(
                       'feature-edit',
                       ['blognr' => $blognr]
                   ) . "'>&laquo; " . _(
                       'No, return to editing'
                   ) . '</a></p>
            </section>';

        return $this->template->render(
            $output,
            ['title' => $pageTitle, 'bodyId' => 'feature-edit']
        );
    }

    public function handleRequest($blognr)
    {
        $blognr    = escape($blognr);
        $pageTitle = _('Edit Article');
        $res       = query_db("select * FROM blogs WHERE blognr=$blognr");
        if (!$res || $res->num_rows == 0) {
            return $this->notFound();
        }

        $output = "
            <h1>$pageTitle</h1>
            <section class='edit-feature'>";

        // submitting a GET form for deletion, will force a confirmation
        $output .= ArticleUtil::featureDeleteForm($blognr);
        $output .= ArticleUtil::featureEditForm($blognr);
        $output .= '</section>';

        return $this->template->render(
            $output,
            ['title' => $pageTitle, 'bodyId' => 'feature-edit']
        );
    }
}
