<?php

namespace xc\Controllers;

use function xc\getUrl;
use function xc\obfuscateEmail;

class FAQ extends Controller
{

    public function handleRequest()
    {
        $pageTitle = _('Frequently Asked Questions');
        $output = "
            <h1>$pageTitle</h1>

            <p>
            " . sprintf(
                _(
                    "Below are some common questions you may have about this site. If your question is not listed here, please post your question to the <a href='%s'>forum</a>."
                ),
                getUrl('forum')
            ) . "
            </p>

            <ul>
            <li><a href='#about'>" . _("What's this site about?") . "</a></li>
            <li><a href='#submit'>" . _(
                'What are the Terms of Use of www.xeno-canto.org?'
            ) . "</a></li>
            <li><a href='#register'>" . _('Why should I register?') . "</a></li>
            <li><a href='#revise'>" . _('Can I revise the sounds afterwards?') . "</a></li>
            <li><a href='#delete'>" . _('Can I delete a sound?') . "</a></li>
            <li><a href='#lotsofsounds'>" . _('I want to share a lot of sounds. Uploading takes ages!') . "</a></li>
            <li><a href='#longsounds'>" . _('I want to share an extremely long recording!') . "</a></li>
            <li><a href='#publication'>" . _(
                'I want to use the sounds for a popular or scientific publication. May I do that?'
            ) . "</a></li>
            <li><a href='#app'>" . _(
                'I want to use the sounds for an app. May I do that?'
            ) . "</a></li>
            <li><a href='#wrongID'>" . _('I have found a song with a wrong ID. How can we change this?') . "</a></li>
            <li><a href='#download'>" . _(
                'How do I download the recordings and save them to my computer? When I click the download icon, they just play in my browser.'
            ) . "</a></li>
            <li><a href='#rating'>" . _('Are there any guidelines for rating recordings?') . "</a></li>
            <li><a href='#soundscape'>" . _("What is a 'soundscape' recording?") . "</a></li>
            <li><a href='#restricted'>" . _('Why are recordings of certain species restricted?') . "</a></li>
            <li><a href='#bats'>" . _('Bat recordings') . "</a></li>
            </ul>

            <article>
            <h1>
            <a name='about'>" . _("What's this site about?") . '</a>
            </h1>
            <p>' . sprintf(
                _(
                    "Good question. The way we (that’s we as in “the XC-team”) see xeno-canto has evolved over the years. 
            Check out <a href='%s'>this page</a> for the latest insights."
                ),
                getUrl('about')
            ) . "
            </article>

            <article>
            <h1>
            <a name='submit'>" . _(
                'What are the Terms of Use of www.xeno-canto.org?'
            ) . '</a>
            </h1>
            <p>' . sprintf(
                _(
                    "The Terms of Use can be read in detail <a href='%s'>here</a>. 
            It is crucial reading material for both recordists and general users alike!"
                ),
                getUrl('termsofuse')
            ) . "
            </p>
            </article>

            <article>
            <h1>
            <a name='register'>" . _('Why should I register?') . '</a>
            </h1>
            <p>' . _(
                'Only if you register you can actively participate on xeno-canto by uploading files, 
            by asking for opinions or by offering advice and expertise on the forum. 
            Everything you see on XC has been put together with volunteer efforts by the registered users. 
            So, join them, sign up and start helping out.'
            ) . "
            </p>
            </article>

            <article>
            <h1>
            <a name='revise'>" . _('Can I revise the sounds afterwards?') . '</a>
            </h1>
            <p>' . _(
                'The metadata of the recordings that you have entered into the database may be 
            revised (corrected) by yourself. You should note the change in the remarks field or in a 
            discussion of the recording. You cannot change the sound itself (see below). 
            Due to continuing changes in taxonomy (splits, lumps) the name of the taxon to which a 
            recording is attributed may be changed by XC or by yourself over time. '
            ) . "
            </p>
            </article>

            <article>
            <h1>
            <a name='delete'>" . _('Can I delete a sound?') . '</a>
            </h1>
            <p>' . _(
                ' Well yes, sort of. Or rather, no, not really. 
            We want XC to be a collection that people can refer to in discussions, 
            on their websites, in scientific papers etcetera. This requires that the 
            collection is stable: recordings with a certain XC-number should remain 
            on-site and unchanged. So the answer is no. Of course if there is a really 
            good reason to delete a recording, for instance, it occurs twice for some 
            reason, or there are problems with the rights than it can be removed. If 
            we think the reason is not convincing, we may decide to keep it on site 
            anyway. (Do check the all-important explanation of the licenses to convince 
            yourself that they allow that.) '
            ) . "
            </p>
            </article>


            <article>
            <h1>
            <a name='lotsofsounds'>" . _(
                'I want to share a lot of sounds. Uploading takes ages!'
            ) . "</a>
            </h1>
            <p>If you have a lot of recordings that you want to share, it can take a long time to use the forms.Instead, you can make a spreadsheet to fill out all the data.
            For batch uploads, you can <a href='https://docs.google.com/spreadsheets/d/1aMLIF97Im-vX-Py-2bRAANpniTByBGlDC8HYPuVulVs'>use a spreadsheet template</a>. Then send a zip-file with all the recordings to the admins at the contact 
            address, and the spreadheet that provides all the data to us " . obfuscateEmail(
                CONTACT_EMAIL . '?subject=Batch%20Upload',
                'via email'
            ) . ". 
            If you are thinking of using this possibility, drop us a line.</p>
            </article>

            <article>
            <h1>
            <a name='longsounds'>" . _(
                'I want to share an extremely long recording!'
            ) . '</a>
            </h1>
            <p>
            ' . _(' Drop us a line! ') . "
            </p>
            </article>

            <article>
            <h1>
            <a name='publication'>" . _(
                'I want to use the sounds for a popular or scientific publication. May I do that?'
            ) . '</a>
            </h1>
            <p>' . _(
                "Short answer: Yes. Cool. Go ahead.
            Long answer: Use in popular and scientific publications falls squarely within the aims of XC. 
            So we want that to be possible. 
            However, at some point in time we chose to start using CC licenses to make it clear that 
            some types of use were not allowed. We chose a rather restrictive license at the time BY-NC-ND. 
            The ND clause (No Derivative) used to lead to some discussion. 
            It is a good idea to read the creative commons licenses that apply to the recordings that 
            you'd like to use. The page for each recording contains a link to the exact license that 
            it is released under. You can find the full legal text of the license by following that link. 
            You will find that many uses are actually possible without contacting the recordists. 
            See the terms of use page for a fuller discussion of the different licenses used in 
            the collection.
            "
            ) . "
            </p>
            </article>

            <article>
            <h1>
            <a name='app'>" . _(
                'I want to use the sounds for an app. May I do that?'
            ) . '</a>
            </h1>
            <p>' . _(
                "Short answer: it is complicated, read on.
            Long answer: It is a good idea to read the Creative Commons licenses that apply to 
            the recordings that you'd like to use. The page for each recording contains a link 
            to the exact license that it is released under. You can find the full legal text of 
            the license by following that link. You will find that many uses are actually possible 
            without contacting the recordists. See the terms of use page for a fuller discussion 
            of the different licenses used in the collection.
            Here and here are a few forum discussions on the subject that may be useful if you want 
            to use the recordings in specific cases such as scientific publications.

            You will notice that many recordings have NC or Non-Commercial licenses. 
            That does *not* automatically mean that use by not-for-profit or unprofitable 
            institutions is allowed.To be on the safe side, whenever you want to use a recording 
            with an NC license in project that has a financial component, contact the recordist 
            and ask for permission. And yes, that can be a lot of work.

            "
            ) . "
            </p>
            </article>

            <article>
            <h1>
            <a name='wrongID'>" . _(
                'I have found a song with a wrong ID. What do I do?'
            ) . '</a>
            </h1>
            <p>' . sprintf(
                _(
                    "Use the 'discuss' link when browsing or viewing the recording in question. 
            This will post a thread with the catalogue number on the <a href='%s'>forum</a> 
            and send an e-mail notification to the recordist. 
            The thread will remain connected to the recording so future users can easily find it. 
            All recordings that have had their identity questioned will be omitted from standard search 
            results until the question has been resolved."
                ),
                getUrl('forum')
            ) . "
            </p>
            </article>

            <article>
            <h1>
            <a name='download'>" . _(
                'How do I download the recordings and save them to my computer? When I click the download icon, they just play in my browser.'
            ) . '</a>
            </h1>
            <p>' . _(
                "Instead of clicking the download icon (<img class='icon' src='/static/img/download.png'/>) with the left mouse button, click the icon with the right mouse button and select the option called <b><tt>Save Link as…</tt></b> from the popup menu. Depending on which browser you use, the name of the menu item may be slightly different. Note that there may also be an option called <b><tt>Save Image as…</tt></b>.  Do not click this as it will simply download the icon to your computer."
            ) . '
            </p>
            <p>' . _(
                'To download a recording on an Apple Macintosh computer, you may need to hold down the <b><tt>&#8997; option</tt></b> key on the keyboard while clicking the icon.'
            ) . "
            </p>
            <p>If you need additional guidance, we have also produced a short <a href='http://www.youtube.com/watch?v=HfiPYKa30IQ'>video tutorial</a> demonstrating how to download audio files.</p>
            </article>

            <article>
            <h1>
            <a name='rating'>" . _(
                'Are there any guidelines for rating recordings?'
            ) . '</a>
            </h1>
            <p>' . _(
                'Use the following general guidelines when rating recordings on xeno-canto. Ratings are obviously subjective, and will inevitably vary slightly between different individuals, but these guidelines should improve consistency.'
            ) . '
            <ul>
            <li><strong>A</strong>: ' . _('Loud and Clear') . '</li>
            <li><strong>B</strong>: ' . _(
                'Clear, but animal a bit distant, or some interference with other sound sources'
            ) . '</li>
            <li><strong>C</strong>: ' . _(
                'Moderately clear, or quite some interference'
            ) . '</li>
            <li><strong>D</strong>: ' . _(
                'Faint recording, or much interference'
            ) . '</li>
            <li><strong>E</strong>: ' . _('Barely audible') . "</li>
            </ul>
            </p>
            </article>

            <article>
            <h1>
            <a name='soundscape'>" . _("What is a 'soundscape' recording?") . "</a>
            </h1>
            <p>
            In 2013, we added the ability to upload 'soundscape' recordings
            on xeno-canto.  The intention is to allow people to
            share longer recordings where more than a single species is
            vocalizing. The prototypical example is a recording of a dawn
            chorus, where many species are vocalizing together. In such cases,
            attempting to extract a single-species portion of the recording
            will often diminish its value.
            <p>
            </p>
            Soundscape recordings should generally be at least a minute
            long, and have two or more species identified in the
            background species list.  Occasional uploads of non-avian
            species are usually acceptable, but we discourage large
            numbers of these recordings.  Eventually, we may add proper
            taxonomic support to allow people to share vocalizations of
            different classes of animals, but for now the focus is on
            birds.
            </p>
            The following types of recordings should <em>not</em> be
            uploaded as soundscapes:
            <p>
            <ul>
            <li>Unknown vocalizations: If the vocalization is unknown,
            upload it as a mystery rather than a soundscape.</li>
            <li>Ambient sounds (ocean waves, etc.)</li>
            </ul>
            <p>
            </p>

            </article>

            <article>
            <h1>
            <a name='restricted'>" . _(
                'Why are recordings of certain species restricted?'
            ) . "</a>
            </h1>
            <p>
            Some species are under extreme pressure due to trapping or
            harassment. The open availability of high-quality recordings of these
            species can make the problems even worse. For this reason,
            streaming and downloading of these recordings is disabled.
            Recordists are still free to share them on xeno-canto, but they
            will have to approve access to these recordings.
            </p>
            <p>
            We do not take this action lightly, and we wish it were not
            necessary, but we have been convinced that the negative impacts
            of offering easy access to these recordings outweigh the
            benefits. If you would like access to these recordings, you may
            contact the recordist directly.
            </p>
            </article>
           
            <article>
            <h1>
            <a name='bats'>" . _('Bat recordings') . "</a>
            </h1>
            <p>
            Bats emit ultrasonic sounds, that can be captured and stored in several ways. 
            \"Bat detectors\" use heterodyne mixing (look it up!) to make sounds audible to humans. 
            For compressed storage of the sounds \"zero-crossing detection\" has been popular 
            since it only records the instances where the sound pressure is zero. 
            Since XC has chosen to only allow real-time recordings, these types of recordings are 
            <i>not appropriate</i> to share with others on XC, since they do not contain a record of the 
            original sounds.
            </p>
            <p>
            Some recorders allow recording with \"time-expansion\". This lowers the pitch, 
            thus making them audible to us humans. Luckily, \"time-expanded recordings\" can be restored 
            to real-time to allow comparison with other recordings on XC.  So if you have recorded 
            such time-expanded recordings, and wish to share them on XC: you're welcome to, but 
            you need to convert them to real-time first. This can be done easily, without a change 
            in the actual content!
            </p>
            <p>
            In the open software audio package <a href='https://www.audacityteam.org/download/' target='_blank'>Audacity</a>, first convert your recording back to WAV if necessary.
            Then 
            <ol>
            <li>change the <strong>Project rate</strong> at the bottom of your screen from your 
            current sampling rate to the sampling rate of your recording device (e.g. 96kHz, 192kHz or 384kHz),</li>
            <li>change the <strong>Sampling rate</strong> of your recording to the same frequency. 
            The menu can be found in the light blue left pane, using the drop done menu that shows the file name,</li>
            <li>now <strong>save the file</strong> again as WAV.</li>
            </ol>
            Done!
            </p>
            <p>
            If you have many recordings to convert, it is easier to use a program like <a href='https://sourceforge.net/projects/sox'>SoX</a>. 
            On the command line of a Terminal, run
            <p><code>&gt; sox file.mp3 file.wav</code></p>
            <p>to convert the recording from mp3 to wav.</p>
            <p><code>&gt; sox -r 384k file.wav file_original_speed.wav</code></p>
            <p>
            to convert the recording from its current sampling rate to 384kHz 
            (if that's the sampling rate of your recording device of course). The file 
            <i>file_original_speed.wav</i> can be shared on XC.
            </p>";


        return $this->template->render($output, ['title' => $pageTitle, 'bodyId' => 'faq']);
    }

}
