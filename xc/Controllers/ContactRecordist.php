<?php

namespace xc\Controllers;

use xc\User;
use xc\XCMail;

use function xc\escape;
use function xc\getUrl;
use function xc\notifySuccess;
use function xc\printErrorMessages;
use function xc\XC_logger;

const MAX_MAILS_PER_DAY = 10;

class ContactRecordist extends LoggedInController
{

    private $title;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->title = _('Contact a Recordist');
    }

    public function handlePost($id)
    {
        if ($this->isDisabled()) {
            return $this->unauthorized(
                'Contact form temporarily disabled to prevent spam'
            );
        }

        $warning   = null;
        $recordist = null;

        $error = $this->validateSender();

        if (!$error) {
            $error     = null;
            $recordist = $this->validateReceiver($id, $error);

            $subject  = $this->request->request->get('subject');
            $body     = $this->request->request->get('body');
            $sendCopy = $this->request->request->get('send_copy') !== null;

            $user   = User::current();
            $output = '';

            if (empty($subject)) {
                $warning = _('Message subject cannot be empty');
            } elseif (empty($body)) {
                $warning = _('Message body cannot be empty');
            }

            if ($error) {
                $fromuser = $user->userName() . ' (' . $user->userId() . ')';
                XC_logger()->logWarn(
                    "Error while $fromuser was trying to contact a user: $error"
                );
            }

            if (!$error && !$warning) {
                $fromaddr = $user->emailAddress();
                $fromname = $user->userName();
                $profile  = $user->getProfileURL(true);
                $toaddr   = $recordist->emailAddress();
                $toname   = $recordist->userName();
                $intro    = wordwrap(
                    "The following message was sent by user '$fromname' ($profile) via the xeno-canto website.  To respond to this user, simply reply to this email. The message follows below:",
                    68
                );
                $body     = wordwrap($body, 68);
                $prefsUrl = getUrl('mypage', ['p' => 'prefs'], true);
                $message  = <<<EOM
$intro

------------------------------------------------------------

$body

------------------------------------------------------------

If you wish to prevent other users from contacting you via
the xeno-canto website in the future, please change your
preferences at the following URL:

    $prefsUrl

With best wishes,
the xeno-canto.org team
EOM;

                (new XCMail(
                    $recordist->emailAddress(), $subject, $message, $fromaddr
                ))->send();

                if ($sendCopy) {
                    $senderMessage = "Copy of message to $toname\n\n$message";
                    (new XCMail(
                        $fromaddr, $subject, $senderMessage, $fromaddr
                    ))->send();
                }

                $adminMessage = "ADMIN Notification: message to $toname\n\n$message";
                (new XCMail(
                    WEBMASTER_EMAIL,
                    $subject,
                    $adminMessage,
                    $fromaddr
                ))->send();

                $this->increaseMailCountToday($recordist->userId());

                notifySuccess(sprintf(_('Message sent to %s'), $toname));
                return $this->seeOther(getUrl('index'));
            } elseif ($error) {
                $output .= printErrorMessages(_('Error'), [$error]);
            } else {
                $output .= printErrorMessages(
                    _('Unable to send message'),
                    [$warning]
                );

                $output .= $this->showForm(
                    $recordist,
                    $subject,
                    $body,
                    $sendCopy
                );
            }
        }

        return $this->template->render(
            $output,
            ['title' => $this->title, 'bodyId' => 'contact']
        );
    }

    private function isDisabled()
    {
        return false;
    }

    protected function validateSender()
    {
        if (!User::current()->isVerified()) {
            return _(
                       'To reduce spam, you are not allowed to contact other users until your email address has been verified.'
                   ) . ' ' .
                   sprintf(
                       _(
                           "To re-send a verification code, please visit <a href='%s'>your account page</a>."
                       ),
                       getUrl('mypage')
                   );
        }
        if ($this->mailCountToday() >= MAX_MAILS_PER_DAY) {
            return sprintf(
                _(
                    'Maximum number of %d messages per day reached, please try again tomorrow.'
                ),
                MAX_MAILS_PER_DAY
            );
        }
        return null;
    }

    protected function mailCountToday()
    {
        $userDir = User::current()->userId();
        $today   = date('Y-m-d');

        $res = query_db(
            "SELECT count(id) AS nr FROM mails_sent WHERE sender_dir = '$userDir' AND date_sent ='$today'"
        );
        return $res->fetch_object()->nr;
    }

    protected function validateReceiver($id, &$errorMsg)
    {
        if (empty($id)) {
            $errorMsg = _('No user was specified');
        }

        if (!$errorMsg) {
            $recordist = User::load($id);

            if (!$recordist || !$recordist->emailAddress(
                ) || !$recordist->canContact()) {
                $errorMsg = _(
                    'This user cannot be contacted through this website'
                );
            }

            return $recordist;
        }

        return null;
    }

    protected function increaseMailCountToday($recipientDir)
    {
        $userDir      = User::current()->userId();
        $recipientDir = escape($recipientDir);
        $today        = date('Y-m-d');

        $query = "
            INSERT INTO mails_sent (sender_dir, recipient_dir, date_sent) 
            VALUES ('$userDir', '$recipientDir', '$today')";

        mysqli()->query($query);
    }

    protected function showForm($recordist, $subject, $body, $sendCopy)
    {
        if (!$recordist) {
            return printErrorMessages(
                _('Internal Error'),
                [
                    _(
                        'Invalid user'
                    ),
                ]
            );
        }

        $sendCopyChecked = $sendCopy ? 'checked' : '';
        $mailCount       = $this->mailCountToday();
        $mailWarning     = $mailCount == 0 ? '' : "
            </p>
            <p class='important'>" . sprintf(
                _(
                    'You can still send <b>%d</b> messages today.'
                ),
                MAX_MAILS_PER_DAY - $mailCount
            );

        return '<p>' .
               sprintf(
                   _('Send an email to %s by filling out the form below. '),
                   "<i>{$recordist->userName()}</i>"
               )
               . _(
                   'Sending a message via this form will reveal your email address to the recipient.'
               ) .
               '</p>
          <p>' .
               sprintf(
                   _(
                       'To reduce spam, we have restricted the number of messages that can be sent through this form to a maximum of %d per day.'
                   ),
                   MAX_MAILS_PER_DAY
               ) . ' ' . $mailWarning .
               "</p>
          <p class='important'>" . sprintf('<b>%s</b>: ', _('Note')) .
               sprintf(
                   _(
                       "This form should not be used to discuss or question the identification of a particular recording.  If you believe that a particular recording is mis-identified, please discuss it the <a href='%s'>forum</a> instead."
                   ),
                   getUrl('forum')
               )
               . "</p>
          <form id='new-message' method='post'>
          <input type='hidden' name='id' value='" . htmlspecialchars(
                   $recordist->userId()
               ) . "'/>
          <p><input class='message-subject' type='text' name='subject' value='" . htmlspecialchars(
                   $subject,
                   ENT_QUOTES
               ) . "' placeholder='" . htmlspecialchars(
                   _(
                       'Subject'
                   ),
                   ENT_QUOTES
               ) . "'></p>
          <textarea class='message-text' name='body' placeholder='" . htmlspecialchars(
                   _(
                       'Message Text'
                   ),
                   ENT_QUOTES
               ) . "'>$body</textarea>
          <p><input type='checkbox' id='send_copy' name='send_copy' $sendCopyChecked/><label for='send_copy'>" . htmlspecialchars(
                   _(
                       'Send copy to myself'
                   ),
                   ENT_QUOTES
               ) . "</label></p>
          <p><input type='submit' name='send' value='" . htmlspecialchars(
                   _(
                       'Send'
                   ),
                   ENT_QUOTES
               ) . "'/></p>
          </form>
          ";
    }

    public function handleRequest($id)
    {
        if ($this->isDisabled()) {
            return $this->unauthorized(
                'Contact form temporarily disabled to prevent spam'
            );
        }

        $subject   = $this->request->query->get('subject');
        $body      = $this->request->query->get('body');
        $recordist = null;
        $error     = $this->validateSender();
        $sendCopy  = $this->request->query->get('send_copy') !== null;

        if (!$error) {
            $error     = null;
            $recordist = $this->validateReceiver($id, $error);
        }

        $output = "<h1>{$this->title}</h1>";

        if ($error) {
            $output .= printErrorMessages(_('Unable to send message'), [$error]
            );
        } else {
            $output .= $this->showForm($recordist, $subject, $body, $sendCopy);
        }

        return $this->template->render(
            $output,
            ['title' => $this->title, 'bodyId' => 'contact']
        );
    }

}
