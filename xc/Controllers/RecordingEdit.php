<?php

namespace xc\Controllers;

use xc\AudioUtil;
use xc\DeletedRecording;
use xc\Recording;
use xc\SonoSize;
use xc\ThreadType;
use xc\User;
use xc\XCMail;
use xc\XCRedis;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\notifyWarning;
use function xc\XC_logger;

class RecordingEdit extends LoggedInController
{

    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public function delete($xcid)
    {
        $xcid = escape($xcid);
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $user = User::current();
        if (!($user->isAdmin() || ($rec->recordistID() == $user->userId()))) {
            return $this->unauthorized();
        }

        $reason = escape($this->request->request->get('delete-reason'));

        $deleter = User::current()->userId();
        XC_logger()->logWarn(
            "Deleting recording XC$xcid from database:\n{$rec->debugString()}"
        );
        $path = $rec->filePath();
        $sql = "
            INSERT INTO birdsounds_trash (
                genus,
                species,
                ssp,
                eng_name,
                family,
                length_snd,
                volume_snd,
                speed_snd,
                pitch_snd,
                number_notes_snd,
                variable_snd,
                songtype,
                song_classification,
                quality,
                recordist,
                olddate,
                `date`,
                `time`,
                country,
                location,
                longitude,
                latitude,
                elevation,
                remarks,
                background,
                back_nrs,
                back_english,
                back_latin,
                back_families,
                back_extra,
                path,
                species_nr,
                order_nr,
                dir,
                neotropics,
                africa,
                asia,
                europe,
                australia,
                `datetime`,
                discussed,
                license,
                snd_nr,
                group_id,
                temperature,
                collection_specimen,
                collection_date,
                observed,
                playback,
                automatic, 
                device,
                microphone,
                form_data,
                delete_date,
                delete_user,
                delete_reason
            )
            (SELECT
                genus,
                species,
                ssp,
                eng_name,
                family,
                length_snd,
                volume_snd,
                speed_snd,
                pitch_snd,
                number_notes_snd,
                variable_snd,
                songtype,
                song_classification,
                quality,
                recordist,
                olddate,
                `date`,
                `time`,
                country,
                location,
                longitude,
                latitude,
                elevation,
                remarks,
                background,
                back_nrs,
                back_english,
                back_latin,
                back_families,
                back_extra,
                path,
                species_nr,
                order_nr,
                dir,
                neotropics,
                africa,
                asia,
                europe,
                australia,
                `datetime`,
                discussed,
                license,
                snd_nr,
                group_id,
                temperature,
                collection_specimen,
                collection_date,
                observed,
                playback,
                automatic, 
                device,
                microphone,
                form_data,
                NOW(),
                '$deleter',
                '$reason'
            FROM birdsounds 
            WHERE snd_nr = $xcid
        )";

        if (query_db($sql)) {
            $sql = "DELETE from birdsounds where snd_nr=$xcid";
            query_db($sql);
            $sql = "DELETE from audio_info where snd_nr=$xcid";
            query_db($sql);
            if (!empty($path) && is_file($path)) {
                $fname = basename($path);
                $destdir = app()->soundsTrashDir();
                if (!is_dir($destdir)) {
                    mkdir($destdir, 0755, true);
                }

                $dest = "$destdir/{$rec->recordistID()}_$fname";
                $res = rename($path, $dest);
                if (!$res) {
                    XC_logger()->logError("Unable to rename file '$path' to '$dest'");
                }
            }
            $res = @unlink($rec->sonoPath(SonoSize::SMALL));
            if (!$res) {
                XC_logger()->logWarn("Unable to delete small sono for XC$xcid");
            }
            $res = @unlink($rec->sonoPath(SonoSize::MEDIUM));
            if (!$res) {
                XC_logger()->logWarn(
                    "Unable to delete medium sono for XC$xcid"
                );
            }
            $res = @unlink($rec->sonoPath(SonoSize::LARGE));
            if (!$res) {
                XC_logger()->logWarn("Unable to delete large sono for XC$xcid");
            }
            $res = @unlink($rec->sonoPath(SonoSize::FULL));
            if (!$res) {
                XC_logger()->logWarn("Unable to delete full sono for XC$xcid");
            }
            notifySuccess(sprintf(_('Deleted recording XC%d'), $xcid));
            $this->redis->clearRecordingCache();
        } else {
            notifyError(
                'Unable to delete recording.  Please report this error to ' . CONTACT_EMAIL . '.'
            );
        }

        $referer = $this->request->request->get(
            'redirect',
            User::current()->getProfileURL()
        );
        return $this->seeOther($referer);
    }

    public function confirmDelete($xcid)
    {
        $xcid = escape($xcid);
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $user = User::current();
        if (
            $rec && !($user->isAdmin() || ($rec->recordistID() == $user->userId()))
        ) {
            (new XCMail(
                WEBMASTER_EMAIL,
                'Unauthorized delete',
                "Unauthorized user attempting to delete recording XC$xcid from database:\n{$rec->debugString()}"
            ))->send();
            return $this->unauthorized();
        }
        $title = _('Delete a recording');
        $output = "<h1>$title</h1>
            <p class='important'>" .
            _(
                '<b>NOTE</b>: although it is possible to delete your recordings from the collection, we <i>strongly</i> discourage it. Please only delete a recording if it is of very poor quality, has errors, or has copyright issues. It is important that the collection is stable so that people can reference recordings without them disappearing. In some cases we may restore a deleted recording to the collection under the terms of its original license if we feel that it is valuable (the Creative Commons licenses under which the recordings are shared permit this).'
            ) . '
            </p>
            <p>' . sprintf(
                _(
                    'Are you sure you want to delete recording %s from the collection?'
                ),
                "<a href='{$rec->URL()}' target='_blank'>XC$xcid</a>"
            ) . "
                </p>
                <table class='key-value'>
                <tr>
                <td>" . _('Common name') . "</td>
                <td>{$rec->commonName()}</td>
                </tr>
                <tr>
                <td>" . _('Scientific name') . "</td>
                <td><span class='sci-name'>{$rec->scientificName()}</span></td>
                </tr>
                <tr>
                <td>" . _('Sound Type') . "</td>
                <td>{$rec->soundType()}</td>
                </tr>
                <tr>
                <td>" . _('Date') . "</td>
                <td>{$rec->date()}</td>
                </tr>
                <tr>
                <td>" . _('Location') . "</td>
                <td>{$rec->location()}, {$rec->country()}</td>
                </td>
                </tr>
                </table>
                <form method='post'>
                <p>" . _(
                'We would appreciate it if you would specify a reason for deleting this recording'
            ) . "</p>
                <p>
                <textarea name='delete-reason'></textarea>
                </p>
                <input type='hidden' name='redirect' value='{$this->request->headers->get('Referer')}'>
                <input type='submit' name='delete' value='" . htmlspecialchars(_('Delete'), ENT_QUOTES) . "'></td>
                </form>
                ";

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'delete-recording']
        );
    }

    public function restore($xcid)
    {
        if (!(User::current()->isAdmin())) {
            return $this->unauthorized();
        }

        $xcid = escape($xcid);
        $rec = DeletedRecording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        // First check if sound is still present in the trash; if so move file back
        $dest = $rec->filePath();
        $fname = basename($dest);
        $sourceDir = app()->soundsTrashDir();
        $source = "$sourceDir/{$rec->recordistID()}_$fname";

        // Looks like older files may not have the recordist id prefix; test without if file cannot be found
        if (!file_exists($source)) {
            $source = "$sourceDir/$fname";
        }

        if (file_exists($source)) {
            // Move file back to use dir
            $res = @rename($source, $rec->filePath());
            if ($res) {
                // Restore audio info for the file (has been deleted from audio_info table)
                AudioUtil::updateAudioProperties($xcid);
                // Create sonos
                AudioUtil::generateSonos($xcid);
                AudioUtil::generateFullLengthSonogram($xcid);
                // Move data back to birdsounds and delete entry from trash table
                $sql = "
                INSERT INTO birdsounds (
                    genus,
                    species,
                    ssp,
                    eng_name,
                    family,
                    length_snd,
                    volume_snd,
                    speed_snd,
                    pitch_snd,
                    number_notes_snd,
                    variable_snd,
                    songtype,
                    song_classification,
                    quality,
                    recordist,
                    olddate,
                    `date`,
                    `time`,
                    country,
                    location,
                    longitude,
                    latitude,
                    elevation,
                    remarks,
                    background,
                    back_nrs,
                    back_english,
                    back_latin,
                    back_families,
                    back_extra,
                    path,
                    species_nr,
                    order_nr,
                    dir,
                    neotropics,
                    africa,
                    asia,
                    europe,
                    australia,
                    `datetime`,
                    discussed,
                    license,
                    snd_nr,
                    group_id,
                    temperature,
                    collection_specimen,
                    collection_date,
                    observed,
                    playback,
                    automatic, 
                    device,
                    microphone,
                    form_data,
                    modified
                )
                (SELECT
                    genus,
                    species,
                    ssp,
                    eng_name,
                    family,
                    length_snd,
                    volume_snd,
                    speed_snd,
                    pitch_snd,
                    number_notes_snd,
                    variable_snd,
                    songtype,
                    song_classification,
                    quality,
                    recordist,
                    olddate,
                    `date`,
                    `time`,
                    country,
                    location,
                    longitude,
                    latitude,
                    elevation,
                    remarks,
                    background,
                    back_nrs,
                    back_english,
                    back_latin,
                    back_families,
                    back_extra,
                    path,
                    species_nr,
                    order_nr,
                    dir,
                    neotropics,
                    africa,
                    asia,
                    europe,
                    australia,
                    `datetime`,
                    discussed,
                    license,
                    snd_nr,
                    group_id,
                    temperature,
                    collection_specimen,
                    collection_date,
                    observed,
                    playback,
                    automatic, 
                    device,
                    microphone,
                    form_data,
                    NOW()
                FROM birdsounds_trash 
                WHERE snd_nr = $xcid
                )";
                query_db($sql);
                $sql = "DELETE from birdsounds_trash where snd_nr=$xcid";
                query_db($sql);
                $this->redis->clearRecordingCache();
            } else {
                notifyError('Unable to move sound file to user directory, check permissions');
                XC_logger()->logWarn("Unable to rename file '$source' to '$dest'");
            }
        } else {
            notifyError('Unable to locate sound file to restore');
            XC_logger()->logError("Unable to locate $source to restore");
        }

        return $this->seeOther(getUrl('recording', ['xcid' => $xcid]));
    }

    public function confirm($xcid)
    {
        $xcid = escape($xcid);
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        $user = User::current();
        if ($user->canConfirm()) {
            $res = query_db('UPDATE birdsounds SET discussed=' . ThreadType::NONE . " WHERE snd_nr=$xcid");
            if (!$res) {
                notifyError(_('Internal Database Error'));
            } else {
                notifySuccess(sprintf(_('Thank you for confirming recording %s'), "XC$xcid"));
            }

            query_db(
                "INSERT INTO sounds_confirm VALUES(NULL, $xcid, '" . $user->userId(
                ) . "', NOW(), '" . $rec->speciesNumber() . "')"
            );
        } else {
            notifyWarning('Unable to confirm');
        }

        return $this->seeOther(getUrl('recording', ['xcid' => $xcid]));
    }

}
