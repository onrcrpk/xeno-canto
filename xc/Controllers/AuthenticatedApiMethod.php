<?php

namespace xc\Controllers;

use xc\User;

abstract class AuthenticatedApiMethod extends ApiMethod
{

    public function call()
    {
        if (!User::current()) {
            return $this->forbidden();
        }

        // Not yet implemented
        //return parent::call();
    }
}
