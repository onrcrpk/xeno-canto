<?php

namespace xc\Controllers;

use xc\User;
use xc\XCMail;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;

class VerifyEmail extends Controller
{

    public function resend()
    {
        $me = User::current();
        if (!$me) {
            return $this->unauthorized();
        }

        $dir = $me->userId();
        $res = query_db(
            "SELECT username, email, verificationCode FROM users WHERE dir='$dir'"
        );
        $row = $res->fetch_object();
        if ($row) {
            $verificationUrl = getUrl(
                'verify-email',
                ['userid' => $dir, 'code' => $row->verificationCode],
                true
            );
            $message         = "
Dear {$row->username},

Please visit the following URL in your browser to verify
your email address:

$verificationUrl

Best regards,

The xeno-canto.org team";
            (new XCMail(
                $row->email,
                'Verification code for xeno-canto.org',
                $message
            ))->send();

            notifySuccess(
                "The verification code has been sent to {$row->email}"
            );
        } else {
            notifyError(_("Couldn't send verification code"));
        }

        return $this->seeOther(getUrl('index'));
    }

    public function handleRequest($userid, $code)
    {
        $title = _('Verify Email Address');
        $body  = "<h1>$title</h1>";

        $userid = escape($userid);
        $c      = escape($code);
        $res    = query_db(
            "SELECT dir, email, verified, verificationCode FROM users WHERE dir='$userid'"
        );
        if (!$res || $res->num_rows == 0) {
            return $this->badRequest("User doesn't exist");
        } else {
            $row = $res->fetch_object();
            if ($row->verified) {
                $body .= '<p>' . _(
                    'This email address has already been verified.'
                ) . '</p>';
            } elseif ($row->verificationCode !== $code) {
                $body .= "<div class='error'>" . _(
                    'Verification code does not match'
                ) . '</div>';
            } else {
                query_db("UPDATE users SET verified=1 WHERE dir='$userid'");
                $body .= '<p>' . _(
                    'Your email address is now verified'
                ) . '</p>';
                if (User::current()) {
                    User::current()->reloadFromDatabase();
                }
            }
        }

        return $this->template->render($body, ['title' => $title]);
    }
}
