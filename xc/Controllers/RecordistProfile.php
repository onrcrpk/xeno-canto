<?php

namespace xc\Controllers;

use xc\MultiSpeciesMap;
use xc\Query;
use xc\Set;
use xc\User;

use function xc\escape;
use function xc\getUrl;
use function xc\printErrorMessages;
use function xc\XC_formatDuration;
use function xc\XC_formatUserText;
use function xc\XC_makeViewLinks;
use function xc\XC_pageNumber;
use function xc\XC_pageNumberNavigationWidget;
use function xc\XC_resultsTableForView;

class RecordistProfile extends Controller
{

    public function redirect()
    {
        $id = $this->request->query->get(
            'id',
            $this->request->query->get('XCrecordist')
        );
        if (!$id) {
            return $this->badRequest();
        }
        $params = $this->request->query->all();
        unset($params['XCrecordist']);
        $params['id'] = $id;
        return $this->movedPermanently(getUrl('recordist', $params));
    }

    public function handleRequest($id)
    {
        $num_per_page = defaultNumPerPage();
        $escapedXCrec = escape($id);
        $view = $this->request->query->get('view', defaultResultsView());
        $pagenumber = XC_pageNumber($this->request);

        $me = User::current();
        $user = User::load($id);
        if (!$user) {
            return $this->notFound();
        }

        $recordist = $user->userName();
        $bio = '';
        if ($user->blurb()) {
            $blurb = XC_formatUserText($user->blurb());
            $bio = '<section>
                <h2>' . _('About') . "</h2>
                <p>$blurb</p>
                </section>";
        }

        $bio_img = '<section>';
        if (@file_exists("{$_SERVER['DOCUMENT_ROOT']}/graphics/memberpics/$id.png")) {
            $bio_img .= "<a href='/graphics/memberpics/$id.png' class='fancybox'>
            <img class='avatar' src='/graphics/memberpics/$id.png' ></a>";
        } else {
            $bio_img .= "<img src='/static/img/avatar-default-200.png'>";
        }
        if ($user->isAdmin()) {
            $bio_img .= "<p class='admin-badge'><img class='icon' src='/static/img/admin-16.png' title='xeno-canto administrator'/>" . _(
                    'XC administrator'
                ) . '</p>';
        }
        if ($me && ($id == $me->userId())) {
            $bio_img .= "<p><a href='" . getUrl('mypage') . "'>" . _('Edit your profile') . '</a></p>';
        } elseif ($me && $me->isAdmin() || $me && $me->isLoggedIn() && $user->canContact()) {
            $bio_img .= "<p><a href='" . getUrl('contact-recordist', ['id' => $id]) . "'>" . _(
                    'Contact this user'
                ) . '</a></p>';
        }
        if ($me && $me->isAdmin()) {
            $bio_img .= "
                <p>
                    <img class='icon' src='/static/img/admin-16.png' /> <a href='" . getUrl('admin-disable-user',
                    ['id' => $id]) . "'>Disable this account</a><br>
                    <img class='icon' src='/static/img/admin-16.png' /> <a href='" . getUrl('admin-masquerade-direct',
                    ['id' => $id]) . "'>Masquerade as this user</a><br>
                    <img class='icon' src='/static/img/admin-16.png' /> <a href='" . getUrl('admin-user-permissions',
                    ['u' => $id]) . "'>Edit user permissions</a>
                </p>";
        }
        $bio_img .= '</section>';

        //==========================================================================
        // look for features

        $sqlblog = "select * from blogs where dir='$escapedXCrec' AND public=1 order by date_last_update desc";
        $resblog = query_db($sqlblog);
        $returned_rows = $resblog->num_rows;

        $blogs = '';
        if ($returned_rows != 0) {
            $blogs = '<section>
                <h2>' . _('Articles') . '</h2>
                <ul>';
            while ($row = $resblog->fetch_object()) {
                $blogs .= "<li><a href='" . getUrl('feature-view', ['blognr' => $row->blognr]
                    ) . "'>$row->title</a></li>";
            }

            $blogs .= '</ul></section>';
        }

        // public sets
        $sets = '';
        $userSets = Set::loadUserSets($user->userId());
        if ($userSets) {
            $sets .= '<section>
                <h2>' . _('Recording Sets') . '</h2>
                <ul>';

            foreach ($userSets as $set) {
                $sets .= "<li><a href='{$set->url()}'>{$set->name()}</a></li>";
            }

            $sets .= '</ul>
                </section>';
        }


        //--------------------------------------------------------------------------
        // query of stats and birdsounds and treat results
        $statsres = query_db("SELECT * FROM rec_summary_stats WHERE userid='$escapedXCrec' AND group_id = 0");
        $statsrow = $statsres->fetch_object();

        if ($statsrow && $statsrow->nrecordings != 0) {
            $duration = XC_formatDuration($statsrow->length);
            $regiontext = '<section>
                <h2>' . _('Statistics') . "</h2>
                <table class='profile-stats'>
                <tbody>
                <tr>
                <td>" . _('Recordings') . "</td><td>{$statsrow->nrecordings}</td></tr>
                <tr><td>" . _('Species') . "</td><td>{$statsrow->nspecies}</td></tr>
                <tr><td>" . _('Unique Species') . "</td><td>{$statsrow->nunique}</td></tr>
                <tr><td>" . _('Locations') . "</td><td>{$statsrow->nlocations}</td></tr>
                <tr><td>" . _('Countries') . "</td><td>{$statsrow->ncountries}</td></tr>
                <tr><td>" . _('Recording Time') . "</td><td>$duration</td></tr>
                </tbody>
                </table>
                <p><a href='" . getUrl('stats-individual', ['rec' => $id]) . "'>" . _('More statistics') . '</a></p>
                </section>';
        } else {
            $regiontext = '';
        }

        $filter = $this->request->query->get('query');
        $query = "dir:$id $filter";

        $qstart = microtime(true);

        $q = new Query($query, Query::OPT_INCLUDE_MYSTERIES);
        //$q->useLocalNameSearch();
        $number_of_hits = $q->numRecordings();

        $no_pages = ceil($number_of_hits / $num_per_page);
        $start_no_shown = (($pagenumber - 1) * $num_per_page) + 1;

        $q->setOrder(
            $this->request->query->get('order'),
            $this->request->query->get('dir')
        );
        $res = $q->execute($pagenumber);

        $qend = microtime(true);
        $query_time = round($qend - $qstart, 2);

        $end_no_shown = $start_no_shown + $res->num_rows - 1;

        $map = new MultiSpeciesMap('map-canvas', $q->execute());

        $output = "
            <h1>$recordist</h1>
            <div class='profile'>
            $bio_img
            $regiontext
            $bio
            $blogs
            $sets
            </div>
            <div class='profile-recordings'>
            <div id='map-canvas'></div>
            <form id='filter-form' method='get'>
            <table>
            <tr>
            <td>
            <input type='text' placeholder='" . htmlspecialchars(
                _('Filter results'),
                ENT_QUOTES
            ) . "...' name='query' value='$filter' />
            </td>
            <td>
            <input type='submit' value='" . htmlspecialchars(
                _('Filter results'),
                ENT_QUOTES
            ) . "' />
            </td>
            </tr>
            </table>
            </form>
            ";

        if (!$number_of_hits) {
            if (empty($filter)) {
                $output .= printErrorMessages(_('No Recordings Found'), [
                    sprintf(
                        _('%s has not uploaded any recordings yet'),
                        "<strong>$recordist</strong>"
                    ),
                ]);
            } else {
                $output .= printErrorMessages(_('No Recordings Found'), [
                    sprintf(
                        _("%s has no recordings that match the search term '%s'"),
                        "<strong>$recordist</strong>",
                        "<em>$filter</em>"
                    ),
                ]);
            }
        } else {
            $links = XC_makeViewLinks($this->request, $view);
            $output .= "
                <p>
                <div class='results-format'>" . _('Results format') . ":
                $links
                </div>
                </p>
                ";
            $output .= XC_pageNumberNavigationWidget(
                $this->request,
                $no_pages,
                $pagenumber
            );
            $output .= XC_resultsTableForView($this->request, $res, $view);
            $output .= XC_pageNumberNavigationWidget(
                $this->request,
                $no_pages,
                $pagenumber
            );
        }

        $output .= '</div>';
        $output .= $map->getJS();

        return $this->template->render($output, ['title' => $recordist, 'bodyId' => 'recordist-profile']);
    }
}
