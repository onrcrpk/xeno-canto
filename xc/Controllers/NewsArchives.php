<?php

namespace xc\Controllers;

use xc\NewsItem;

use function xc\getUrl;

class NewsArchives extends Controller
{

    public function handleRequest($year)
    {
        $title = _('Latest News');
        $items = null;

        if (empty($year)) {
            $items = NewsItem::loadRecent(20);
        } else {
            $title = sprintf(_('News Archive for %d'), $year);
            $items = NewsItem::loadForYear($year);
        }


        $output = "
            <h1>$title</h1>
            <section id='news' class='column'>";
        foreach ($items as $item) {
            $output .= $item->toHtml();
        }
        $output .= "</section>
            <section id='archives' class='column'>
            <h2>News archives by year</h2>
            <ul class='simple'>";
        $res    = query_db(
            'SELECT YEAR(date) as year, COUNT(*) as N from news_world GROUP BY YEAR(date) ORDER BY year DESC'
        );
        while ($row = $res->fetch_object()) {
            $url = getUrl('news-archives', ['y' => $row->year]);
            $output .= "<li><a href='$url'>{$row->year}</a></li>";
        }
        $output .= '</ul>
            </section>';

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'news-archives']
        );
    }
}
