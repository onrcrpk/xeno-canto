<?php

namespace xc\Controllers;

use xc\Query;
use xc\ViewType;

use function xc\printErrorMessages;
use function xc\XC_resultsTableForView;

class BrowseRandom extends Controller
{

    public function handleRequest()
    {
        $title = _('Random Recordings');
        $num_per_page = defaultNumPerPage();

        // generate a set of random IDS.  we do this here, because the alternative
        // ("order by rand()" in sql) is insanely slow for large databases
        $randomIds = [];

        // generate 3x the number of IDs we need because some of the ids may end up
        // being deleted from the database
        $res = query_db('SELECT MAX(snd_nr) AS maxid FROM birdsounds');
        $row = $res->fetch_object();
        $maxId = $row->maxid;
        for ($i = 0; $i < ($num_per_page * 3); $i++) {
            $id = rand(1, $maxId);
            $randomIds[] = $id;
        }
        $randomIds = array_unique($randomIds);

        // compute the query, find the result
        $localName = localNameLanguage();
        $idsString = implode(', ', $randomIds);
        $sql = "
            SELECT * FROM (
                SELECT " . Query::birdsoundsSelectFields() . ", audio_info.length, M.$localName as localName
                FROM birdsounds
                INNER JOIN audio_info USING(snd_nr)
                INNER JOIN taxonomy_multilingual M USING (species_nr)
                WHERE snd_nr IN ($idsString)
            ) T ORDER BY RAND() LIMIT $num_per_page";
        $res = query_db($sql);
        $numHits = $res->num_rows;

        $output = "
            <h1>$title</h1>
            <p>" . _('Reload the page for a new set of random recordings.') . '</p>';

        if ($numHits) {
            $output .= XC_resultsTableForView($this->request, $res, ViewType::SONOGRAMS);
        } else {
            $output .= printErrorMessages(_('Internal Error'), [_('Unable to query recordings'),]);
        }

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'random']
        );
    }

}
