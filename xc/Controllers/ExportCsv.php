<?php

namespace xc\Controllers;

use Symfony\Component\HttpFoundation\Response;
use xc\License;
use xc\Query;
use xc\Recording;

function __outputCSV(&$vals, $key, $filehandle)
{
    fputcsv($filehandle, $vals);
}

function outputCSV($stream, $data)
{
    array_walk($data, '\xc\Controllers\__outputCSV', $stream);
}

class ExportCsv extends Controller
{

    public function handleRequest()
    {
        $query = strip_tags($this->request->query->get('query'));
        if (!$query) {
            return $this->badRequest();
        }

        $q = new Query($query);
        //$q->useLocalNameSearch();
        $res = $q->execute(
            Query::NO_PAGING,
            MAX_EXPORT_RECORDS
        );

        $header = [
            'Common name',
            'Scientific name',
            'Subspecies',
            'Recordist',
            'Date',
            'Time',
            'Location',
            'Country',
            'Latitude',
            'Longitude',
            'Elevation',
            'Songtype',
            'Remarks',
            'Back_latin',
            'Catalogue number',
            'License',
        ];

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set(
            'Content-Disposition',
            'attachment; filename=xeno-canto.csv'
        );
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');

        $outstream = fopen('php://memory', 'w+');
        outputCSV($outstream, [$header]);

        while ($row = $res->fetch_object()) {
            $rec = new Recording($row);
            $bgspecies = $rec->backgroundSpecies();
            $bg = [];
            foreach ($bgspecies as $sp) {
                $bg[] = $sp->scientificName();
            }
            $license = License::lookupById($rec->license());
            $csvrow = [
                $rec->commonName(),
                $rec->scientificName(),
                $rec->subspecies(),
                $rec->recordist(),
                $rec->date(),
                $rec->time(),
                (!$rec->species()->restricted() ? $rec->location() : '-'),
                $rec->country(),
                (!$rec->species()->restricted() ? $rec->latitude() : '-'),
                (!$rec->species()->restricted() ? $rec->longitude() : '-'),
                $rec->elevation(),
                $rec->soundType(),
                str_replace("\r\n", "\n", $rec->remarks()),
                implode(',', $bg),
                $rec->xcid(),
                $license->url,
            ];

            outputCSV($outstream, [$csvrow]);
        }

        fseek($outstream, 0);
        $csv = fread($outstream, 10000000);
        fclose($outstream);

        $response->setContent($csv);
        return $response;
    }
}
