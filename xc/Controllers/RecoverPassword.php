<?php

namespace xc\Controllers;

use xc\XCMail;
use xc\XCRedis;

use function xc\escape;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\obfuscateEmail;

function randomPassword()
{
    $length = 8;
    $str = '';
    for ($i = 0; $i < $length; $i++) {
        switch (rand(1, 3)) {
            case 1:
                $str .= chr(rand(48, 57));
                break; //0-9
            case 2:
                $str .= chr(rand(65, 90));
                break; //A-Z
            case 3:
                $str .= chr(rand(97, 122));
                break; //a-z
        }
    }
    return $str;
}

class RecoverPassword extends Controller
{
    private const MAX_ATTEMPTS = 5;

    private const ATTEMPT_TIMEOUT = 3600;

    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis('password-recovery-' . $this->request->getClientIp());
    }

    public function handlePost()
    {
        $nrAttempts = $this->redis->get();
        if (!$nrAttempts) {
            $this->redis->set(1, self::ATTEMPT_TIMEOUT);
        } else {
            $this->redis->incr();
        }

        $email = $this->request->request->get('email');

        if ($nrAttempts <= self::MAX_ATTEMPTS) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                notifyError(_('Invalid email address'));
            } else {
                $escapedEmail = escape($email);
                $resultMessage = '';

                $sql = "select username, salt from users where email = '$escapedEmail'";
                $res = query_db($sql);
                if ($res && $res->num_rows > 0) {
                    $row = $res->fetch_object();
                    $new_pwd = randomPassword();

                    $new_hashed_pwd = hash('sha256', $row->salt . $new_pwd, 0);

                    $sql = "update users set password = '$new_hashed_pwd' where email = '$escapedEmail'";
                    $res = query_db($sql);
                    if ($res) {
                        $message = wordwrap(
                            "
The password for $email has been reset to:

$new_pwd

Please visit https://xeno-canto.org/auth/login to log in using this new password.  After logging in, please change your password to something you're likely to remember by visiting the following page:

https://xeno-canto.org/account/password

The xeno-canto team",
                            60
                        );

                        (new XCMail($email, '[xeno-canto] Password Reset', $message))->send();

                        notifySuccess(
                            sprintf(
                                _(
                                    'We have sent a new password to %s.'
                                ),
                                "<b>$email</b>"
                            )
                        );
                        return $this->seeOther(getUrl('login'));
                    } else {
                        notifyError('Internal Database error, please try again');
                    }
                } else {
                    notifyError(
                        sprintf(
                            _(
                                "This email address does not exist in our members database. Try again, or <a href='%s'>register</a> first."
                            ),
                            getUrl('register')
                        )
                    );
                }
            }
        }
        return $this->content($email);
    }

    protected function content($email = '')
    {
        $nrAttempts = $this->redis->get();

        if ($nrAttempts >= self::MAX_ATTEMPTS) {
            $title = _('Password recovery temporarily blocked');
            $output = "
                <h1>$title</h1>
                <p>" .
                _(
                    'You have exceeded the maximum number of password recoveries.'
                ) . ' ' .
                sprintf(
                    _(
                        'Please try again in approximately %s minutes or %s.'
                    ),
                    (self::ATTEMPT_TIMEOUT / 60),
                    obfuscateEmail(
                        CONTACT_EMAIL . '?subject=Password%20recovery%20xeno-canto',
                        _(
                            'contact us via email'
                        )
                    )
                ) .
                '</p>';
        } else {
            $title = _('Recover Password');
            $emailValue = htmlspecialchars($email, ENT_QUOTES);
            $output = "
            <div id='login-form'>
            <h1>$title</h1>

            <p>" .
                _(
                    'Have you forgotten your password? Then fill in your email address below, and we will send you a new one.'
                ) . "
            </p>";

            if ($nrAttempts > 1) {
                $output .= '<p style="color: red; font-weight: bold;">' . sprintf(
                        _('Number of remaining requests: %s'),
                        (self::MAX_ATTEMPTS - $nrAttempts)
                    ) . '</p>';
            }

            $output .= "
            <form method='post'>
            <input type='hidden' name='submitted' value='1'>
            <p>
            <input type='text' name='email' value='$emailValue' placeholder='" . htmlspecialchars(
                    _(
                        'Email address'
                    ),
                    ENT_QUOTES
                ) . "'>
            </p>
            <p>
            <input type='submit' value='" . htmlspecialchars(
                    _('Submit'),
                    ENT_QUOTES
                ) . "'>
            </p>
            </form>
            </div>";
        }

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'recover-password']
        );
    }

    public function handleRequest()
    {
        return $this->content();
    }
}
