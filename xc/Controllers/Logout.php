<?php

namespace xc\Controllers;

use xc\User;

use function xc\getUrl;
use function xc\notifySuccess;

class Logout extends Controller
{

    public function handleRequest()
    {
        $user = User::current();
        if ($user) {
            $user->logout();
            notifySuccess(_('You have been logged out.'));
        }
        app()->session()->clear();

        return $this->seeOther(getUrl('index'));
    }
}
