<?php

namespace xc\Controllers\api;

use xc\Controllers\CompletionServiceApiMethod;
use xc\Language;
use xc\Species;
use xc\User;

use function xc\escape;

class CompletionServiceSpecies extends CompletionServiceApiMethod
{

    protected function query($term, &$suggestions, &$data)
    {
        $term = escape($term);

        // FIXME: ENVREC
        // ignore the species that are not really species...
        $cond = Species::excludePseudoSpeciesSQL('taxonomy');

        $parts = explode(' ', $term);
        $sqlAddition = '';
        if (count($parts) == 2) {
            $sqlAddition .= "
                UNION 
                
                SELECT genus, species, eng_name, species_nr, recordings, group_id 
                FROM taxonomy 
                WHERE genus LIKE '$parts[0]%' AND species LIKE '$parts[1]%' ";
        }

        // Interface and browser language are used to search for common names
        $searchLanguages = [$this->browserSearchLanguageCode(), app()->determineBestLanguage()->code];
        // ... add additional search languages from user preferences
        if (User::current()) {
            $user = User::current();
            foreach ($user->searchLanguages() as $language) {
                $searchLanguages[] = $language->code;
            }
        }

        foreach (array_unique($searchLanguages) as $code) {
            if ($code != 'en') {
                $column = Language::lookup($code)->localNameLanguage;
                $sqlAddition .= " 
                    UNION 
                    
                    SELECT taxonomy.genus, taxonomy.species, taxonomy_multilingual.$column AS eng_name,  
                        taxonomy.species_nr, taxonomy.recordings, taxonomy.group_id
                    FROM taxonomy_multilingual
                    LEFT JOIN taxonomy ON taxonomy_multilingual.species_nr = taxonomy.species_nr
                    WHERE $cond AND taxonomy_multilingual.$column LIKE \"$term%\" ";
            }
        }

        $sql = "
            SELECT genus, species, eng_name, species_nr, recordings, group_id 
            FROM taxonomy 
            WHERE $cond AND (genus like \"$term%\" or species like \"$term%\")
            
            UNION
            
            SELECT genus, species, eng_name, species_nr, recordings, group_id 
            FROM taxonomy 
            WHERE $cond AND eng_name like \"$term%\"
            
            UNION
            
            SELECT genus, species, eng_name, species_nr, recordings, group_id 
            FROM taxonomy 
            WHERE $cond AND eng_name like \"% $term%\" 
            
            $sqlAddition
            
            ORDER BY 
                CASE 
                    WHEN eng_name IS NULL OR eng_name = '' 
                    THEN concat(genus, ' ', species) 
                    ELSE eng_name 
                END";

        $res = query_db($sql);

        while ($row = $res->fetch_object()) {
            $suggestions[] = "$row->genus $row->species";
            $data[] = [
                'species_nr' => $row->species_nr,
                'recordings' => $row->recordings,
                'common_name' => $row->eng_name,
                'group_id' => $row->group_id
            ];
        }
    }

    protected function browserSearchLanguageCode()
    {
        foreach ($this->request->getLanguages() as $code) {
            $language = Language::lookup($code);
            if ($language) {
                return $language->code;
            }
        }
        return 'en';
    }
}
