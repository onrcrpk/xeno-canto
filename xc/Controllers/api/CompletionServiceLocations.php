<?php

namespace xc\Controllers\api;

use xc\Controllers\CompletionServiceApiMethod;

use function xc\escape;

class CompletionServiceLocations extends CompletionServiceApiMethod
{

    protected function query($term, &$suggestions, &$data)
    {
        $requireCoords = $this->request->query->get('coords');
        $coordClause   = '';

        $sqlquery = escape($term);
        if ($requireCoords) {
            $coordClause = 'AND latitude IS NOT NULL AND longitude IS NOT NULL';
        }
        $sql = "SELECT location, longitude, latitude, country FROM birdsounds WHERE location LIKE '%$sqlquery%' $coordClause GROUP BY location";
        $res = query_db($sql);

        while ($row = $res->fetch_object()) {
            $suggestions[] = "{$row->location}";
            $data[]        = [
                'country' => $row->country,
                'lat'     => $row->latitude,
                'lng'     => $row->longitude,
            ];
        }
    }
}
