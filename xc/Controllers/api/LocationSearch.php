<?php

namespace xc\Controllers\api;

use xc\Controllers\ApiMethod;
use xc\Query;

class LocationSearch extends ApiMethod
{

    public function get()
    {
        $queryString = $this->request->query->get('query');
        if (!$queryString) {
            return $this->missingParameter('query');
        }

        $q   = new Query($queryString);
        $res = $q->getLocations();

        $response['results'] = [];
        while ($row = $res->fetch_array()) {
            if ($row['latitude'] && $row['longitude']) {
                $response['results'][] = [
                    'name' => $row['location'],
                    'lat'  => $row['latitude'],
                    'lng'  => $row['longitude'],
                ];
            }
        }

        return $this->respond($response);
    }
}
