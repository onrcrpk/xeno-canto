<?php

namespace xc\Controllers\api;

use xc\Controllers\AuthenticatedApiMethod;
use xc\User;

use function xc\XC_rateRecording;

class RateRecording extends AuthenticatedApiMethod
{

    public function post()
    {
        $snd_nr = intval($this->request->request->get('snd_nr'));
        if (!$snd_nr) {
            return $this->missingParameter('snd_nr');
        }

        if (!User::current()->canRateRecording($snd_nr)) {
            return $this->forbidden();
        }

        $quality = intval($this->request->request->get('quality'));

        $res = XC_rateRecording($snd_nr, $quality);

        $payload = [
            'success' => ($res != false),
            'snd_nr'  => $snd_nr,
            'quality' => $quality,
        ];

        return $this->respond($payload);
    }
}
