<?php

namespace xc\Controllers\api;

use xc\Controllers\CompletionServiceApiMethod;

use function xc\escape;

class CompletionServiceSubspecies extends CompletionServiceApiMethod
{

    protected function additionalRequiredParameters()
    {
        return ['sp'];
    }

    protected function query($term, &$suggestions, &$data)
    {
        $query = escape($term);
        $sp = escape($this->request->query->get('sp'));

        $res = query_db(
            "SELECT ssp,author FROM taxonomy_ssp A
            INNER JOIN taxonomy B ON (A.genus=B.genus AND A.species=B.species)
            WHERE B.species_nr='$sp' AND A.ssp LIKE '%$query%'"
        );

        while ($row = $res->fetch_object()) {
            $suggestions[] = "$row->ssp";
            $data[] = "$row->author";
        }
    }
}
