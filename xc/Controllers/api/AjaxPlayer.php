<?php

namespace xc\Controllers\api;

use Symfony\Component\HttpFoundation\Response;
use xc\Controllers\ApiMethod;
use xc\Marker;
use xc\Recording;

class AjaxPlayer extends ApiMethod
{

    public function get()
    {
        $output = [];
        $nr = $this->request->query->get('nr');
        $contentType = $this->request->query->get('ct', Marker::Foreground);

        $content = '';
        if (!$nr) {
            return $this->respond(
                [
                    'error' => ApiMethod::CLIENT_ERROR,
                    'message' => "'nr' parameter is required",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        $rec = Recording::load($nr);
        if (!$rec) {
            return $this->respond(
                [
                    'error' => ApiMethod::CLIENT_ERROR,
                    'message' => "Recording '$nr' not found",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        if ($contentType == Marker::Background) {
            $spName = $rec->htmlDisplayName(false);

            $content = "<p>In background of <a href='{$rec->URL()}'>XC{$rec->xcid()}</a></p>
                <p>{$rec->miniPlayer()} $spName</p>";
        } else {
            $content = $rec->player(['showLocation' => true]);
        }

        return $this->respond(['content' => "<div>$content</div>"]);
    }
}
