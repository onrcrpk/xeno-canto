<?php

namespace xc\Controllers\api;

use Symfony\Component\HttpFoundation\Response;
use xc\Controllers\ApiMethod;
use xc\Recording;

use function xc\escape;

class PlayPing extends ApiMethod
{

    public function post()
    {
        $xcid = intval($this->request->request->get('XC'));

        if (!$xcid) {
            return $this->missingParameter('XC');
        }

        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->respond(
                [
                    'error'   => ApiMethod::CLIENT_ERROR,
                    'message' => 'Recording ID is invalid',
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        $ip  = escape($this->request->getClientIp());
        $res = query_db(
            "INSERT INTO birdsounds_play_stats (snd_nr, date, ip) VALUES ($xcid, NOW(), '$ip')"
        );

        if (!$res) {
            return $this->respond(
                [
                    'error'   => ApiMethod::SERVER_ERROR,
                    'message' => 'Unable to update database',
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->respond(['success' => true]);
    }
}
