<?php

namespace xc\Controllers\api;

use xc\Controllers\ApiMethod;

use function xc\XC_speciesListForLocation;

class LocationSpeciesList extends ApiMethod
{

    public function get()
    {
        $loc = $this->request->query->get('location');
        if (!$loc) {
            return $this->missingParameter('location');
        }


        $fgSpecies = [];
        $bgSpecies = [];

        XC_speciesListForLocation($loc, $fgSpecies, $bgSpecies);

        return $this->respond(
            [array_values($fgSpecies), array_values($bgSpecies)]
        );
    }
}
