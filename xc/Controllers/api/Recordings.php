<?php

namespace xc\Controllers\api;

use Symfony\Component\HttpFoundation\Response;
use xc\Controllers\ApiMethod;
use xc\License;
use xc\Query;
use xc\Recording;
use xc\SonoSize;
use xc\SonoType;

use function xc\getUrl;

class Recordings extends ApiMethod
{

    public function search()
    {
        $queryString = strip_tags($this->request->query->get('query'));
        if (!$queryString) {
            return $this->missingParameter('query');
        }

        $page = intval($this->request->query->get('page', 1));

        $q = new Query($queryString);
        $q->setOrder('taxonomy');
        $q->setPageSize(500);
        //$q->useLocalNameSearch();

        if ($page < 1 || $page > $q->numResultsPages()) {
            return $this->respond(
                [
                    'error' => ApiMethod::CLIENT_ERROR,
                    'message' => "The 'page' parameter must be between 1 and 'numPages'",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        $results = [
            'numRecordings' => $q->numRecordings(),
            'numSpecies' => $q->numSpecies(),
            'page' => $page,
            'numPages' => $q->numResultsPages(),
            'recordings' => null,
        ];

        $res = $q->execute($page);

        if (!$res) {
            $this->respond(
                [
                    'error' => ApiMethod::SERVER_ERROR,
                    'message' => 'Unable to query database',
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        $recordings = [];
        while ($row = $res->fetch_object()) {
            $rec = new Recording($row);
            $recordings[] = $this->recordingObject($rec);
        }

        $results['recordings'] = $recordings;

        return $this->respond($results);
    }

    private function recordingObject($rec)
    {
        $hide = $rec->restrictedForUser();

        $license = License::lookupById($rec->license());
        $url = str_replace(['http:', 'https:'], '', getUrl('recording', ['xcid' => $rec->xcid()], true));

        return [
            'id' => $rec->xcid(),
            'gen' => $rec->genus(),
            'sp' => $rec->speciesName(),
            'ssp' => $rec->subspecies(),
            'group' => $rec->groupName(),
            'en' => $rec->commonName(),
            'rec' => $rec->recordist(),
            'cnt' => $rec->country(),
            'loc' => $hide ? '' : $rec->location(),
            'lat' => $hide ? '' : $rec->latitude(),
            'lng' => $hide ? '' : $rec->longitude(),
            'alt' => $rec->elevation(),
            'type' => $rec->soundType(),
            'sex' => $rec->sex(),
            'stage' => $rec->lifeStage(),
            'method' => $rec->recordingMethod(),
            'url' => $url,
            'file' => $hide ? '' : $rec->downloadURL(true),
            'file-name' => $hide ? '' : $rec->filename(),
            'sono' => [
                SonoSize::SMALL => $rec->sonoUrl(SonoSize::SMALL),
                SonoSize::MEDIUM => $rec->sonoUrl(SonoSize::MEDIUM),
                SonoSize::LARGE => $rec->sonoUrl(SonoSize::LARGE),
                SonoSize::FULL => $rec->sonoUrl(SonoSize::FULL),
            ],
            'osci' => [
                SonoSize::SMALL => $rec->sonoUrl(SonoSize::SMALL, SonoType::OSCILLOGRAM),
                SonoSize::MEDIUM => $rec->sonoUrl(SonoSize::MEDIUM, SonoType::OSCILLOGRAM),
                SonoSize::LARGE => $rec->sonoUrl(SonoSize::LARGE, SonoType::OSCILLOGRAM),
            ],
            'lic' => $license->url,
            'q' => $rec->ratingString(),
            'length' => $rec->lengthString(),
            'time' => $rec->time(),
            'date' => $rec->date(),
            'uploaded' => $rec->uploadDateString(),
            'also' => $rec->backgroundScientific(),
            'rmk' => $rec->remarks() ?? '',
            'bird-seen' => $rec->animalSeen(),
            'animal-seen' => $rec->animalSeen(),
            'playback-used' => $rec->playbackUsed(),
            'temp' => $rec->temperature() ?? '',
            'regnr' => $rec->specimen() ?? '',
            'auto' => $rec->automatic(),
            'dvc' => $rec->device() ?? '',
            'mic' => $rec->microphone() ?? '',
            'smp' => $rec->sampleRate(),
        ];
    }

    public function showRecording($xcid)
    {
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        return $this->respond($this->recordingObject($rec));
    }
}
