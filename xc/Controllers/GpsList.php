<?php

namespace xc\Controllers;

use xc\Species;

function roundTowardZero($fval)
{
    if ($fval >= 0) {
        return floor($fval);
    } else {
        return ceil($fval);
    }
}

class GpsList extends Controller
{

    public function handleRequest()
    {
        $title = 'Species list by Location';
        $initialLocationJS = 'null';
        $initialZoom = 1;
        $lat = $this->request->query->get('lat');
        $lon = $this->request->query->get('lon');
        $roundedLat = roundTowardZero($lat);
        $roundedLon = roundTowardZero($lon);
        $apiUrl = 'https://maps.googleapis.com/maps/api/js?language=en&amp;key=' . GOOGLE_MAPS_GEOCODING_KEY;

        if ($lat && $lon) {
            $initialLocationJS = "new google.maps.LatLng({$lat}, {$lon})";
            $initialZoom = 6;
        }

        $mapScript = <<<EOT
<script type="text/javascript" src="$apiUrl"></script>
<script type="text/javascript" src="/static/js/xc-map.js"></script>
<script type='text/javascript'>
var xmap = null;
var polygon = null;
var marker = null;
var initialLocation = $initialLocationJS;

function createPolygon(coords)
{
    var lat = Math.floor(coords.lat());
    var lng = Math.floor(coords.lng());

    var squareCoords = [
                new google.maps.LatLng(lat, lng),
                new google.maps.LatLng(lat + 1, lng),
                new google.maps.LatLng(lat + 1, lng + 1),
                new google.maps.LatLng(lat, lng + 1)
            ];
    var polygonOptions = {
        map: xmap,
        fillColor: '#CC0000',
        fillOpacity: 0.4,
        strokeColor: '#CC0000',
        strokeOpacity: 0.8,
        paths: squareCoords
    };
    return new google.maps.Polygon(polygonOptions);
}

function setLocation(coords)
{
    if (polygon)
        polygon.setMap(null);
    if (marker)
        marker.setMap(null);

    marker = new google.maps.Marker({map: xmap,
            icon: new google.maps.MarkerImage('img/markers/a-14.png', new google.maps.Size(14,14), null, new google.maps.Point(7,7)),
            position: coords });

    polygon = createPolygon(coords);

    jQuery("#location-form input[name=lat]").val(coords.lat());
    jQuery("#location-form input[name=lon]").val(coords.lng());
    jQuery("#location-form input[type=submit]").removeAttr("disabled");
    xmap.panTo(coords);
}

function setInitialLocation () {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var curLoc = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            xmap.setCenter(curLoc);
            setLocation(curLoc);
        });
    }
    xmap.setCenter(new google.maps.LatLng(0, 0));
}

jQuery(document).ready( function ()
{
    var mapOptions = {
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        zoom: $initialZoom,
        scrollwheel: false
    }
    xmap = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    google.maps.event.addListener(xmap, 'click', function(event) { setLocation(event.latLng); });

    if (initialLocation)
    {
        xmap.setCenter(initialLocation);
        setLocation(initialLocation);
    }
    else
    {
        setInitialLocation();
    }
});
</script>
EOT;

        $output = "
            <h1>Species list by Location</h1>
            <div class='column'>
            <div id='map-canvas'></div>
            <p>
            <form id='location-form' method='get'>
            <input name='lat' value='{$lat}' type='hidden' />
            <input name='lon' value='{$lon}' type='hidden' />
            <input type='submit' value='Submit' disabled>
            </form>
            </p>
            </div>
            <div class='column'>
            <p> Click a location on the map and then click 'Submit' to see the list of
            species most likely to be present at that location.
            </p>
            <p class='important'>Note that the range information we have does not currently
            match the taxonomy used on xeno-canto, so some irregularities and
            inconsistencies may occur.
            </p>
            </div>
            ";

        if ($lat && $lon) {
            $sqlgps = "SELECT tax.family, tax.genus, tax.species, tax.eng_name, tax.species_nr
                FROM raster_distributions
                INNER JOIN taxonomy tax
                USING (species_nr)
                WHERE raster_distributions.latitude = $roundedLat
                AND raster_distributions.long_left <= $roundedLon
                AND raster_distributions.long_right >= $roundedLon
                AND raster_distributions.species_nr != \"\"
                ORDER BY tax.IOC_order_nr ASC";
            $resgps = query_db($sqlgps);
            $nrgps = $resgps->num_rows;

            if ($nrgps) {
                $tablegps = "
                    <p>$nrgps species present at $roundedLat latitude, $roundedLon longitude</p>
                    <table class='results'>
                    <thead>
                    <th>Common name</th>
                    <th>Scientific name</td>
                    </thead>
                    <tbody>";

                $fam_old = '';
                while ($row = $resgps->fetch_object()) {
                    $fam_nu = $row->family;
                    if (strcmp($fam_nu, $fam_old) != '0') {
                        $class = 'new-species';
                    } else {
                        $class = '';
                    }

                    $sp = new Species($row->genus, $row->species, $row->eng_name, $row->species_nr);
                    $tablegps .= "
                        <tr class='$class'>
                        <td><a href='{$sp->profileURL()}'>{$sp->commonName()}</td>
                        <td>{$sp->scientificName()}</td>
                        </tr>
                        ";
                    $fam_old = $fam_nu;
                }
                $tablegps .= '
                    </tbody>
                    </table>';
            }
            $output .= $tablegps;
        }
        $output .= $mapScript;

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'species-list']
        );
    }
}
