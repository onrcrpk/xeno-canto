<?php

/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace xc\Controllers;

use xc\XCRedis;

use function xc\getUrl;
use function xc\XC_formatDuration;

class StatsOrder
{
    public const RECORDINGS = 0;

    public const SPECIES = 1;

    public const UNIQUE = 2;

    public const DURATION = 3;
}

class StatsRecordists extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
        $this->redis->setKeyByRequest($this->redisKey('daily-stats-recordists', $this->groupId()), $request);
    }

    public function handleRequest()
    {
        $title = _('Recordist Statistics');
        $output = $this->redis->get();

        if (!$output) {
            $order = intval($this->request->query->get('o'));

            switch ($order) {
                case StatsOrder::SPECIES:
                    $orderClause = 'ORDER BY nspecies DESC, nrecordings DESC, nunique DESC, length DESC';
                    break;
                case StatsOrder::UNIQUE:
                    $orderClause = 'ORDER BY nunique DESC, nrecordings DESC, nspecies DESC, length DESC';
                    break;
                case StatsOrder::DURATION:
                    $orderClause = 'ORDER BY length DESC, nrecordings DESC, nspecies DESC, nunique DESC';
                    break;
                default:
                    $orderClause = 'ORDER BY nrecordings DESC, nspecies DESC, nunique DESC, length DESC';
            }

            if ($this->groupId()) {
                $sql = "
                    SELECT users.dir, users.username, S.nrecordings, S.nspecies, S.nunique, S.length 
                    FROM rec_summary_stats S
                    INNER JOIN users on S.userid=users.dir
                    WHERE nrecordings AND group_id = " . $this->groupId();
            } else {
                $sql = "
                    SELECT users.dir, users.username, S.nrecordings, S.nspecies, S.nunique, S.length 
                    FROM rec_summary_stats S
                    INNER JOIN users on S.userid = users.dir AND S.group_id = 0
                    WHERE nrecordings";
            }
            $sql .= ' ' . $orderClause;
            $res = query_db($sql);

            $tablebody = '';
            $recs = [];
            $sp = [];
            $uq = [];
            $i = 1;
            $maxRecordings = 0;
            $tableMinRecordings = $this->groupId() <= 1 ? 50 : 0;

            while ($row = $res->fetch_object()) {
                if ($i == 1) {
                    $maxRecordings = $row->nrecordings;
                }

                $name = $row->username ?: '<i>missing name</i>';
                if ($row->nrecordings >= $tableMinRecordings) {
                    $tablebody .= "
                    <tr>
                        <td><a href='" . getUrl('stats-individual', ['rec' => $row->dir]) . "'>$name</a></td>
                        <td>$row->nrecordings</td>
                        <td>$row->nspecies</td>
                        <td>$row->nunique</td>
                        <td>" . XC_formatDuration($row->length) . '</td>
                    </tr>';
                }

                $recs[] = "[$i, $row->nrecordings]";
                $sp[] = "[$i, $row->nspecies]";
                $uq[] = "[$i, $row->nunique]";
                $i++;
            }

            $factor = pow(10, strlen($i) - 1);
            $xTickSize = ((ceil($i / $factor) + 1) * $factor) / 10;

            $yTicks = [
                2,
                4,
                6,
                8,
                10,
                20,
                40,
                60,
                80,
                100,
                200,
                400,
                600,
                800,
                1000,
                1500,
                2500,
                4000,
                6000,
                10000,
                15000,
                25000,
                50000
            ];
            $yTicksString = implode(
                ',',
                array_filter($yTicks, function ($value) use ($maxRecordings) {
                    return $value <= $maxRecordings;
                })
            );

            $jsdata = '
                [
                    { 
                        data: [ ' . implode(',', $recs) . "], 
                        label: '" . _('Recordings') . "', 
                        color: '#872709'
                    },
                    { 
                        data: [ " . implode(',', $sp) . "], 
                        label: '" . _('Species') . "', 
                        color: '#ba5b3c'
                    },
                    { 
                        data: [ " . implode(',', $uq) . "], 
                        label: '" . _('Unique') . "', 
                        color: '#e08769'
                    },
                ]";

            $script = "<script type='text/javascript' src='/static/js/flot/jquery.flot.js'></script>
            <script type='text/javascript' src='/static/js/flot/jquery.flot.time.js'></script>
            <script type='text/javascript'>
            jQuery(document).ready(function() {
                jQuery.plot(jQuery('#plot-recordings'), $jsdata, { xaxis: {show: true, tickSize: $xTickSize}, yaxis: {min:1, transform: function(v) { return Math.log(v); }, tickFormatter: function(v) { console.log(v); return v; }, ticks: [$yTicksString]}, lines: { show: false }, points: { show: true}, legend: {position: 'ne'}})
                });
            </script> ";

            $header = $this->groupId() <= 1 ?
                "The table shows the names of recordists having contributed $tableMinRecordings or more recordings, sorted by number of recordings. The graph plots the number of recordings per contributor for all recordists." :
                "The table shows the names of recordists, sorted by number of recordings.";

            $output = "<ul class='breadcrumbs'>
                <li><a href='" . getUrl('collection-details') . "'>" . _('Collection Details') . "</a></li>
                <li class='current'>$title</li>
                </ul>
                <div class='group-select'>" . $this->printGroupSelect() . "</div>
                <p>" . _($header) . '
                </p>
                <p>
                <b>' . _('Recordings') . ':</b> ' . _('the number of recordings by that recordist') . '
                </p>
                <p>
                <b>' . _('Species') . ':</b> ' . _('the number of species by that recordist') . '
                </p>
                <p>
                <b>' . _('Unique') . ':</b> ' . _(
                    "the number of species contributed only by that recordist ('xeno-canto recordist endemics')"
                ) . '
                </p>
                <p>
                <b>' . _('Duration') . ':</b> ' . _('the total duration of recordings contributed by that recordist') . "
                </p>
                <div class='column'>
                <table class='results'>
                <thead>
                <tr>
                <th>" . _('Recordist Name') . "</th>
                <th><a href='" . getUrl('stats-recordists', ['o' => StatsOrder::RECORDINGS]) . "'>" .
                _('Recordings') . "</a></th>
                <th><a href='" . getUrl('stats-recordists', ['o' => StatsOrder::SPECIES]) . "'>" .
                _('Species') . "</a></th>
                <th><a href='" . getUrl('stats-recordists', ['o' => StatsOrder::UNIQUE]) . "'>" .
                _('Unique') . "</a></th>
                <th><a href='" . getUrl('stats-recordists', ['o' => StatsOrder::DURATION]) . "'>" .
                _('Duration') . "</a></th>
                </tr>
                </thead>
                <tbody>
                $tablebody
                </tbody>
                </table>
                </div>
                <div class='column'>
                <div id='plot-recordings' style='width:500px;height:600px'></div>
                </div>
                $script";

            $this->redis->set($output);
        }

        return $this->template->render($output, ['title' => $title]);
    }
}
