<?php

namespace xc\Controllers;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DownloadFile extends Controller
{

    private $downloadDir = 'files';

    private $filePath;

    public function handleRequest($file)
    {
        $this->setFilePath($file);

        if (file_exists($this->filePath)) {
            $response = new BinaryFileResponse($this->filePath);
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file);
            return $response;
        }

        return $this->notFound();
    }

    private function setFilePath($file)
    {
        $this->filePath = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->downloadDir . '/' . $file;
    }
}
