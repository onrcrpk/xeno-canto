<?php

namespace xc\Controllers;

use xc\NewsItem;
use xc\Query;
use xc\Recording;
use xc\SortDirection;
use xc\Species;
use xc\Spotlight;
use xc\User;
use xc\XCRedis;

use function xc\getUrl;
use function xc\XC_formatDuration;
use function xc\XC_formatNumber;
use function xc\XC_formatText;

class Index extends Controller
{

    private $redis;


    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
    }

    public function handleRequest()
    {
        $user = User::current();
        // generate front page
        $output = "<div id='primary-content' class='column'>";
        //$output .= Spotlight::random($this->groupId())->toHtml();
        $output .= Spotlight::random()->toHtml();
        if (!$user) {
            $output .= "
                <section id='whatisxc'>
                <h1>" . _('What is xeno-canto?') . '</h1>
                <p>' . _(
                    'xeno-canto is a website dedicated to sharing wildlife sounds from all over the
                world. Whether you are a research scientist, a birder, or simply curious
                about a sound that you heard out your kitchen window, we invite you to
                listen, download, and explore the wildlife sound recordings in the collection.'
                ) . '
                </p>
                <p>' . _(
                    'But xeno-canto is more than just a collection of recordings.  It is also a
                collaborative project. We invite you to share your own recordings, help
                identify mystery recordings, or share your expertise in the forums. Welcome!'
                ) . '
                </p>
                </section>';
        }
        $output .= "<section id='news' class='column'>
            <h1>" . _('Latest News') . '</h1>
            ';
        if ($user && $user->isAdmin()) {
            $output .= "<a href='" . getUrl(
                    'admin-news'
                ) . "'><img class='icon' width='16' height='16' src='/static/img/admin-16.png'/> Add news item</a>";
        }

        $items = NewsItem::loadRecent(4);
        foreach ($items as $item) {
            $output .= $item->toHtml();
        }
        $output .= "
            <p><a href='" . getUrl('news-archives') . "'>" . _(
                'Older news...'
            ) . '</a></p>
            </section>';


        $output .= "
            <div id='latest' class='column'>";
        $output .= $this->randomRecording();
        $output .= $this->tryThisTip();
        $output .= $this->latestArticles();
        $output .= $this->latestNewRecordists();
        $output .= '
            </div>
            </div>'; // the last one closes primary_content div after startBody()

        $output .= "

            <div id='secondary-content' class='column'>
            ";

        $output .= $this->printGroupSelect();
        $output .= $this->collectionStats();
        $output .= $this->latestNewSpecies();
        $output .= $this->latestAdditions();
        $output .= $this->latestMysteries();

        // now close 'secondary-content' div, and then 'content-area' div
        $output .= '
            </div>
            </div>
            ';

        return $this->template->render($output, ['bodyId', 'index']);
    }

    protected function randomRecording()
    {
        $random_row = $this->randomRowFromCollection();
        $rec = new Recording($random_row);

        $random_recording = '
            <section>
            <h1>' . _('Random Recording') . "</h1>
                {$rec->player(['showLocation'=>true])}
                <div>" . _('ID incorrect?') . " <a href='" . getUrl('new_thread', ['xcid' => $random_row->snd_nr]
            ) . "'>" . _('Tell us!') . '</a>
       </div>';
        $random_recording .= '</section>';

        return $random_recording;
    }

    protected function randomRowFromCollection()
    {
        $max_sql = 'SELECT max(snd_nr) AS max_id FROM birdsounds';
//        if ($this->groupId()) {
//            $max_sql .= ' WHERE group_id = ' . $this->groupId();
//        }
        $max = query_db($max_sql);
        $max_row = $max->fetch_object();
        for ($i = 0; $i < 10; $i++) {
            $random_number = mt_rand(1, $max_row->max_id);
            $localName = localNameLanguage();

            $random_sql = "
                SELECT " . Query::birdsoundsSelectFields() . ", cnames.$localName as localName, 
                    audio_info.length, audio_info.format, audio_info.smp as sample_rate
                FROM birdsounds LEFT JOIN taxonomy_multilingual cnames USING(species_nr)
                LEFT JOIN audio_info USING(snd_nr) 
                WHERE snd_nr >= $random_number AND " . Query::ignoreMysteryAndPseudoSpeciesSql() . ' AND quality = 1';
//            if ($this->groupId()) {
//                $random_sql .= ' AND birdsounds.group_id = ' . $this->groupId();
//            }
            $random_sql .= ' ORDER BY snd_nr ASC
                LIMIT 1';

            $random = query_db($random_sql);
            $random_row = $random->fetch_object();
        }

        return $random_row ?? null;
    }

    protected function tryThisTip()
    {
        $sql = 'select * from newstuff order by rand() limit 1';
        $res = query_db($sql);
        $row = $res->fetch_object();
        $content = XC_formatText(_($row->text));
        return "
            <section class='tip'>
            <h1>" . _('Try this!') . '</h1>
            <h2>' . _($row->title) . "</h1>
            $content
            </section>
            ";
    }

    protected function latestArticles()
    {
        $this->redis->setKey($this->redisKey('article-home-latest'));
        $latest_features = $this->redis->get();

        if (!$latest_features) {
            $res = query_db(
                'select title, author, blognr from blogs where public = 1 and blognr != 5 order by published desc limit 5'
            );

            $latest_features = "   <section class='article'>
            <h1>" . _('Latest Articles') . '</h1>
            <ul>
            ';
            while ($row = $res->fetch_object()) {
                /// as in: 'title' by 'author'
                $tmpl = _('%s by %s');
                $desc = sprintf(
                    $tmpl,
                    "<a href='" . getUrl(
                        'feature-view', ['blognr' => $row->blognr]
                    ) . "'>{$row->title}</a>",
                    $row->author
                );
                $latest_features .= "
                <li>$desc</li>
                ";
            }
            $latest_features .= "
            <li><a href='" . getUrl('features') . "'>" . _('More') . '...</a></li>
            </ul>
            </section>
            ';
            $this->redis->set($latest_features);
        }
        return $latest_features;
    }

    protected function latestNewRecordists()
    {
        $this->redis->setKey($this->redisKey('daily-stats-home-recordists', $this->groupId()));
        $html = $this->redis->get();

        if (!$html) {
            $res_new_sp = query_db(
                'SELECT U.username, userid
                FROM latest_recordists L
                INNER JOIN users U ON U.dir = L.userid
                WHERE L.group_id = 0
                ORDER BY L.snd_nr DESC 
                LIMIT 10'
            );
            $html = '
                <section>
                <h1>' . _('Latest New Recordists') . '</h1>
                <ul>
                ';
            while ($row = $res_new_sp->fetch_object()) {
                $url = getUrl('recordist', ['id' => $row->userid]);
                $name = $row->username;
                $html .= "
                    <li>
                    <a href='$url'>$name</a>
                    </li>
                    ";
            }
            $html .= '
                <li>' . _('Welcome!') . '</li>
                </ul>
                </section>
            ';

            $this->redis->set($html, 3600);
        }

        return $html;
    }

    protected function collectionStats()
    {
        $this->redis->setKey($this->redisKey('hourly-stats-home-collection', $this->groupId()));
        $output = $this->redis->get();

        if (!$output) {
            $bgQuery = ' 
                SELECT COUNT(1) as nr_bg FROM birdsounds_background AS t1
                LEFT JOIN birdsounds AS t2 on t1.snd_nr = t2.snd_nr ';
            if ($this->groupId()) {
                $bgQuery .= 'WHERE t2.group_id = ' . $this->groupId();
            } else {
                $bgQuery .= 'WHERE t2.group_id > 0';
            }
            $res = query_db($bgQuery);
            $row = $res->fetch_object();
            $nrBg = $row->nr_bg;

            if ($this->groupId()) {
                $query = 'SELECT * FROM latest_statistics WHERE group_id = ' . $this->groupId();
            } else {
                $query = ' 
                    SELECT SUM(nr_recordings) AS nr_recordings,
                    SUM(total_duration) AS total_duration,
                    (SELECT COUNT(DISTINCT(dir)) AS nrecordists FROM birdsounds) AS nr_recordists, 
                    SUM(nr_species) AS nr_species, 
                    SUM(nr_ssp) AS nr_ssp, 
                    SUM(nr_gbif) AS nr_gbif
                    FROM latest_statistics';
            }
            $res = query_db($query);
            $row = $res->fetch_object();
            $nrRecordings = $row->nr_recordings ?? 0;

            $nrIds = XC_formatNumber($nrBg + $nrRecordings);

            $output = '
                <section>
                <h1>' . _('Collection Statistics') . "</h1>
                <table class='key-value'>
                <tbody>
                <tr>
                    <td>$nrIds</td>
                    <td> " . _('IDs') . '</td>
                </tr>
                <tr>
                    <td>' . XC_formatNumber($nrRecordings) . '</td>
                    <td> ' . _('Recordings') . '</td>
                </tr>
                <tr>
                    <td>' . XC_formatNumber($row->nr_species ?? 0) . "</td>
                    <td>" . _('Species') . '</td>
                </tr>
                <tr>
                    <td>' . XC_formatNumber($row->nr_ssp ?? 0) . '</td>
                    <td> ' . _('Subspecies') . '</td>
                </tr>
                <tr>
                    <td>' . XC_formatNumber($row->nr_recordists ?? 0) . '</td>
                    <td> ' . _('Recordists') . '</td>
                </tr>
                <tr>
                    <td>' . XC_formatDuration($row->total_duration ?? 0) . '</td>
                    <td> ' . _('Recording Time') . '</td>
                </tr>
                <tr>
                    <td>' . XC_formatNumber($row->nr_gbif ?? 0) . "</td>
                    <td><a href='" . $this->printGbifLink() . "' target='_blank'>" . _('Records to GBIF') . "</a></td>
                </tr>
                </tbody>
                </table>
                <p><a href='" . getUrl('collection-details') . "'>" . _('More') . '...</a></p>
                </section>
            ';
            $this->redis->set($output, 900);
        }

        return $output;
    }

    protected function latestNewSpecies()
    {
        $this->redis->setKey($this->redisKey('hourly-stats-home-latest-species', $this->groupId()));
        $latest_new_species = $this->redis->get();

        if (!$latest_new_species) {
            $localName = localNameLanguage();

            $query = "SELECT T.genus, T.species, T.eng_name, T.species_nr, M.$localName as localName
                FROM latest_species L
                INNER JOIN taxonomy T USING(species_nr)
                LEFT JOIN taxonomy_multilingual M USING(species_nr)";
            if ($this->groupId()) {
                $query .= ' WHERE T.group_id = ' . $this->groupId();
            }
            $query .= ' ORDER BY L.snd_nr DESC LIMIT 5';
            $res_new_sp = query_db($query);

            $latest_new_species = ' ';
            if ($res_new_sp->num_rows > 0) {
                $latest_new_species = '
                <section>
                <h1>' . _('Latest New Species') . '</h1>
                <ul>
                ';
                while ($row = $res_new_sp->fetch_object()) {
                    // FIXME: figure out why there's a blank row here sometimes...
                    if (!$row->species_nr) {
                        continue;
                    }

                    $commonName = $row->localName ?: $row->eng_name;

                    // load the species again from db so we have a chance to get the
                    // localized name, etc. Perhaps we can improve this later...
                    $sp = new Species($row->genus, $row->species, $commonName, $row->species_nr);
                    $latest_new_species .= "
                    <li>
                    <a href='{$sp->profileURL()}'>" . ($sp->commonName() ?: $sp->htmlDisplayName()) . "</a>
                    </li>
                    ";
                }
                $latest_new_species .= "
                <li><a href='" . getUrl('new_species') . "'>" . _('More') . '...</a></li>
                </ul>
                </section>';
            }

            $this->redis->set($latest_new_species, 900);
        }

        return $latest_new_species;
    }

    protected function latestAdditions()
    {
        $this->redis->setKey($this->redisKey('recording-home-latest', $this->groupId()));
        $latest_additions = $this->redis->get();

        if (!$latest_additions) {
            $query = 'since:31';

            if ($this->groupId()) {
                $query .= ' grp:' . $this->groups[$this->groupId()];
            }

            $q = new Query($query);
            $q->setOrder('xc');
            $q->setPageSize(10);
            $result = $q->execute(1);

            $latest_additions = ' ';
            if ($result->num_rows > 0) {
                $latest_additions = "
                <section class='recording' id='latest-additions'>
                <h1>" . _('Latest Additions') . '</h1>
                <ul>
                ';

                while ($row = $result->fetch_object()) {
                    $rec = new Recording($row);
                    // check if the recording is the first recording of the species
                    $res_new = query_db(
                        "select MIN(snd_nr) as first_nr from birdsounds where species_nr = '{$rec->speciesNumber()}'"
                    );
                    $speciesLinkText = $rec->commonName() ?: $rec->htmlDisplayName();
                    $first_row = $res_new->fetch_object();
                    if ($rec->xcid() == $first_row->first_nr) {
                        $speciesLinkText = "<strong class='new-species'>$speciesLinkText</strong>";
                    }
                    if (!$rec->isPseudoSpecies()) {
                        $speciesLinkText = "<a href='{$rec->species()->profileURL()}'>$speciesLinkText</a>";
                    }

                    $tmpl = _('%s by %s from %s');
                    $latest_additions .= "
                    <li>{$rec->miniPlayer()}
                    <a href='{$rec->URL()}'>XC{$rec->xcid()}</a>: " . sprintf(
                            $tmpl,
                            $speciesLinkText,
                            "<a href='" . getUrl('recordist', ['id' => $rec->recordistID()]
                            ) . "'>{$rec->recordist()}</a>",
                            $rec->country()
                        ) . '
                    </li>
                    ';
                }

                $latest_additions .= "
                <li><a href='" . getUrl('browse', ['query' => $query, 'dir' => SortDirection::NORMAL, 'order' => 'xc']
                    ) . "'>" . _('More') . '...</a>
                </li>
                </ul>
                <p>' . _('Great work!') . '</p>
                </section>';
            }

            $this->redis->set($latest_additions, 900);
        }

        return $latest_additions;
    }

    private function latestMysteries()
    {
        $this->redis->setKey($this->redisKey('mystery-home-latest', $this->groupId()));
        $latest_mysteries = $this->redis->get();

        if (!$latest_mysteries) {
            $sql = "
                SELECT dir, birdsounds.snd_nr, species_nr, genus, species, eng_name, family, ssp, country, recordist, `path`,
                    audio_info.smp AS sample_rate
                FROM birdsounds 
                LEFT JOIN audio_info USING(snd_nr) 
                WHERE species_nr = '" . Species::mysterySpeciesNumber() . "'";
            if ($this->groupId()) {
                $sql .= " AND group_id = " . $this->groupId();
            }
            $sql .= " ORDER BY snd_nr DESC LIMIT 0, 10";
            $res = query_db($sql);

            $latest_mysteries = ' ';
            if ($res->num_rows > 0) {
                $latest_mysteries = "
                    <section class='recording'>
                    <h1>" . _('Latest Mysteries') . '</h1>
                    <ul>';

                /// as in: 'recording' by 'recordist' from 'country'
                $tmpl = _('%s by %s from %s');
                while ($row = $res->fetch_object()) {
                    $rlink = getUrl('recording', ['xcid' => $row->snd_nr]);
                    $desc = sprintf(
                        $tmpl,
                        "<a href='$rlink'>XC{$row->snd_nr}</a>",
                        "<a href='" . getUrl('recordist', ['id' => $row->dir]) . "'>{$row->recordist}</a>",
                        $row->country
                    );
                    $rec = new Recording($row);
                    $latest_mysteries .= "
                    <li>
                        {$rec->miniPlayer()} $desc
                    </li>
                    ";
                }

                $url = $this->groupId() ? getUrl(
                    'mysteries', ['query' => 'grp:' . $this->groups[$this->groupId()] . ' since:365']
                ) : getUrl('mysteries');

                $latest_mysteries .= "
                    <li><a href='$url'>" . _('More') . '...</a></li>
                    </ul>
                    </section>';
            }

            $this->redis->set($latest_mysteries, 900);
        }

        return $latest_mysteries;
    }

    private function printGbifLink()
    {
        $res = query_db('SELECT gbif FROM groups WHERE id = ' . $this->groupId());
        if ($res->num_rows == 1) {
            return $res->fetch_object()->gbif;
        }
        return 'https://www.gbif.org/dataset/search?publishing_org=1f00d75c-f6fc-4224-a595-975e82d7689c';
    }

}
