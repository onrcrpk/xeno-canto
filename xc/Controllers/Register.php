<?php

namespace xc\Controllers;

//require_once('vendor/recaptcha/recaptchalib.php');


use xc\User;
use xc\ViewType;
use xc\XCMail;

use function curl_exec;
use function curl_init;
use function curl_setopt;
use function json_decode;
use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\sanitize;

function verifyRecaptcha($secret, $response, $remoteip)
{
    $post = [
        'secret' => $secret,
        'response' => $response,
        'remoteip' => $remoteip,
    ];

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, RECAPTCHA_VERIFY_URL);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    return json_decode(curl_exec($curl));
}

// to make a bit of salt to add to the hashing of passwords. only made once for each user
// upon registration
function random_salt()
{
    $length = 5;
    $str = '';
    for ($i = 0; $i < $length; $i++) {
        $str .= strtolower(chr(rand(65, 90)));
    }
    return $str;
}

function random_dir()
{
    $length = 10;

    $str = '';
    // build string
    for ($i = 0; $i < $length; $i++) {
        $str .= chr(rand(65, 90));  // A-Z
    }

    return $str;
}

class Register extends Controller
{

    public function handleRequest()
    {
        if (User::current()) {
            return $this->seeOther(getUrl('index'));
        }

        return $this->renderForm();
    }

    protected function renderForm(
        $name = null,
        $email = null,
        $recaptchaError = null
    ) {
        global $recaptcha_publickey;

        //$recaptcha = recaptcha_get_html($recaptcha_publickey, $recaptchaError);

        $recaptcha = "";
        if (RECAPTCHA_SITE_KEY) {
            $recaptcha =
                '<div class="g-recaptcha" data-sitekey="' . RECAPTCHA_SITE_KEY . '"></div>
                 <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>';
        }


        $output = "<div id='login-form'>
            <h1>" . _('Register') . '</h1>

            <p>' . _('To register as a member, please fill out the form') . '.</p>
            <p>' . _(
                "If you don't see an <a href='https://support.google.com/recaptcha' target='_blank'>\"I'm not a robot\" checkbox</a> in the form, you may need to adjust privacy plugins in your browser."
            ) . "</p>

            <form class='register' method='post'>
            <p>
            <input autofocus type='text' name='name' value='" . sanitize(
                $name
            ) . "' placeholder='" . _(
                'Name'
            ) . "'>
            </p>
            <p>
            <input type='email' name='email' value='" . sanitize(
                $email
            ) . "' placeholder='" . _(
                'Email Address'
            ) . "'>
            </p>
            <p>
            <input type='password' name='password' placeholder='" . _(
                'Password'
            ) . "'>
            </p>
            <p>
            <input type='password' name='password2' placeholder='" . _(
                'Type password again'
            ) . "'>
            </p>
            <p>$recaptcha</p>
            <input type='submit' value='" . _('Register') . "'>
            </form>
            <p>
            <a href='" . getUrl('FAQ') . "#register'>" . _(
                'Why become a member?'
            ) . '</a>
            </p>
            </div>
            ';

        return $this->template->render(
            $output,
            ['title' => _('Register'), 'bodyId' => 'register']
        );
    }

    public function handlePost()
    {
        $incomplete = false;
        $recaptchaError = '';

        $userid = random_dir();

        $name = trim(
            str_replace(
                ['<', '>'],
                ['&lt;', '&gt'],
                $this->request->request->get('name')
            )
        );
        $email = $this->request->request->get('email');
        $password = $this->request->request->get('password');
        $password2 = $this->request->request->get('password2');

        if (empty($name)) {
            notifyError(_('Name is required'));
            $incomplete = true;
        }
        if (empty($email)) {
            notifyError(_('Email is required'));
            $incomplete = true;
        }
        if (empty($password)) {
            notifyError(_('Password is required'));
            $incomplete = true;
        }
        if (empty($password2)) {
            notifyError(_('Password Verification is required'));
            $incomplete = true;
        }

        // Alle verplichte velden zijn ingevuld
        if (!$incomplete) {
            $email_OK = filter_var($email, FILTER_VALIDATE_EMAIL);
            if (!$email_OK) {
                $incomplete = true;
                notifyError(
                    _(
                        'That is not a valid email address, Please try again.'
                    )
                );
            }
            // check if password has been typed twice correctly.
            if ($password != $password2) {
                $incomplete = true;
                notifyError(
                    _(
                        'The two passwords do not match. Please try again.'
                    )
                );
            }
            if (strlen($password) < 5) {
                notifyError(
                    _(
                        'Your password must be at least 5 characters long.'
                    )
                );
                $incomplete = true;
            }

            if (RECAPTCHA_SECRET_KEY && RECAPTCHA_VERIFY_URL) {
                $resp = verifyRecaptcha(
                    RECAPTCHA_SECRET_KEY,
                    $this->request->request->get('g-recaptcha-response'),
                    $this->request->getClientIp()
                );
                /*

                            $resp = recaptcha_check_answer($recaptcha_privatekey,
                                $this->request->getClientIp(),
                                $this->request->request->get('recaptcha_challenge_field'),
                                $this->request->request->get('recaptcha_response_field'));
                 */

                if (!$resp->success) {
                    notifyError(
                        _(
                            'Your verification code was incorrect. Please try again.'
                        )
                    );
                    $incomplete = true;
                    $recaptchaError = isset($resp->{'error-codes'}) && is_array(
                        $resp->{'error-codes'}
                    ) ?
                        implode('; ', $resp->{'error-codes'}) : '';
                }
            }

            if (!$incomplete) {
                // check if the email address or username don't already exist in db.
                $sql = "SELECT username FROM users WHERE email='$email'";
                $result = query_db($sql);

                if ($result && $result->num_rows > 0) {
                    notifyError(_('This email address is already in use.'));
                    $incomplete = true;
                } else {
                    // register the new member
                    $salt = random_salt();
                    $hashed_password = User::hashPassword($password, $salt);

                    $view = ViewType::DETAILED;
                    $vc = User::otp();

                    // set the current session language as the user's default
                    // language, otherwise default to english
                    $lang = 'en';
                    if (app()->language()) {
                        $lang = app()->language()->code;
                    }

                    $sql = "INSERT INTO users values ('$name', '$email', '$hashed_password', '$salt',
                        '$userid', '$lang', 30, $view, DEFAULT, 0, '', '', DEFAULT, NOW(), 0, '$vc', DEFAULT, -1)";
                    $result = query_db($sql);

                    if ($result && mysqli()->affected_rows == 1) {
                        User::loginWithPassword($email, $password);
                        $verificationUrl = getUrl(
                            'verify-email',
                            ['userid' => $userid, 'code' => $vc],
                            true
                        );
                        $uploadUrl = getUrl('upload', [], true);
                        $forumUrl = getUrl('forum', [], true);
                        $faqUrl = getUrl('FAQ', [], true);

                        $message = <<<EOT
Dear $name,

Thank you for registering with xeno-canto.org!

Your registration details are:

login: $email

Please keep this for future reference.

Now you've registered, you can participate in the mystery
section and enlarge the database of wildlife song recordings by
uploading your own.  

The copyright of the uploads remains yours, but you share them 
under certain conditions that are described in 
Creative Commons licenses. Be sure to read them!
 
To start uploading a recording, go to

    $uploadUrl

In order to participate in the forum section, you'll need to
verify your email address by visiting the following URL:

    $verificationUrl

If you have any questions, please search the FAQ

    $faqUrl

or use the forum

    $forumUrl

Best regards,

The xeno-canto.org team.
EOT;

                        (new XCMail($email, 'Registration details for xeno-canto.org', $message))->send();

                        notifySuccess(_('You have successfully registered your account. Welcome!'));
                        return $this->seeOther(getUrl('mypage'));
                    } else {
                        notifyError(_('A database error occurred, please contact support.'));
                        $incomplete = true;
                    }
                }
            }
        }

        if ($incomplete) {
            app()->session()->remove('user');
        }

        return $this->renderForm($name, $email, $recaptchaError);
    }

}
