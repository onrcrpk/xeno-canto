<?php

namespace xc\Controllers;

use xc\WorldArea;

use function xc\getUrl;

class CollectionDetails extends Controller
{

    public function handleRequest()
    {
        $title = _('Collection Details');

        $output = "<h1>$title</h1>
            <p>
            " . _(
                'Several interesting details about the xeno-canto collection are presented
            here. More will undoubtedly be added as time goes by.'
            ) . '
            </p>

            <h2>' . _('Statistics') . "</h2>
            <dl>
            <dt><a href='" . getUrl('stats-growth') . "'>" . _(
                'Collection Graphs'
            ) . '</a></dt>
            <dd>' . _('Graphs showing the growth of the collection over time') . "</dd>
            <dt><a href='" . getUrl('stats-recordists') . "'>" . _(
                'Recordist Statistics'
            ) . '</a></dt>
            <dd>' . _('A list of all recordists and their contributions') . '</dd>
            </dl>

            <h2>' . _('Species') . "</h2>
            <dl>
            <dt><a href='" . getUrl('all_species') . "'>" . _('All Species') . '</a> (birds only)</dt>
            <dd>' . _(
                'A list of all of the bird species represented in the collection'
            ) . "</dd>
            <dt><a href='" . getUrl('missing') . "'>" . _('Wanted Species') . '</a> (birds only)</dt>
            <dd>' . _(
                'A list of bird species that are still missing from the collection, by country'
            ) . "</dd>
            <dt><a href='" . getUrl('new_species') . "'>" . _(
                'Latest New Species'
            ) . '</a></dt>
            <dd>' . _(
                'A list of the latest species that were added to the collection'
            ) . '</dd>
            </dl>

            <h2>' . _('World Areas') . '</h2>
            <p>' . _(
                'A dashboard summarizing the status of the collection for each major world area'
            ) . "</p>
            <ul class='simple'>
            ";

        foreach (WorldArea::all() as $area) {
            $output .= "<li><a href='" . getUrl(
                    'world-area',
                    ['area' => $area->branch]
                ) . "'>{$area->desc}</a></li>";
        }

        $output .= '</ul>
            </dd>
            
            <h2>' . _('Other') . "</h2>
            <dl>
            <dt><a href='" . getUrl('spotlights') . "'>" . _(
                'Spotlight Recordings'
            ) . '</a></dt>
            <dd>' . _(
                'Interesting, rare, or noteworthy recordings from the collection'
            ) . '</dd>
            </dl>
            ';

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'collection-details']
        );
    }

}
