<?php

namespace xc\Controllers;

use xc\User;
use xc\XCRedis;

use function xc\getUrl;
use function xc\notifyError;
use function xc\notifySuccess;
use function xc\obfuscateEmail;

class Login extends Controller
{

    private const MAX_ATTEMPTS = 8;

    private const ATTEMPT_TIMEOUT = 3600;

    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis('login-' . $this->request->getClientIp());
    }

    public function handleRequest()
    {
        if (User::current()) {
            return $this->seeOther(getUrl('index'));
        }

        return $this->renderForm(
            null,
            $this->request->headers->get('Referer', getUrl('mypage'))
        );
    }

    protected function renderForm($email = null, $referer = null)
    {
        $nrAttempts = $this->redis->get();

        if ($nrAttempts >= self::MAX_ATTEMPTS) {
            $title = _('Login temporarily blocked');
            $html = "
                <h1>$title</h1>
                <p>" .
                _('You have exceeded the maximum number of login attempts.') . ' ' .
                sprintf(
                    _('Please try again in approximately %s minutes or %s.'),
                    (self::ATTEMPT_TIMEOUT / 60),
                    obfuscateEmail(
                        CONTACT_EMAIL . '?subject=Login%20xeno-canto',
                        _('contact us via email')
                    )
                ) .
                '</p>';
        } else {
            $html = "
            <div id='login-form'>
            <h1>" . _('Log in') . "</h1>";

            if ($nrAttempts > 2) {
                $html .= '<p style="color: red; font-weight: bold;">' . sprintf(
                        _('Number of remaining login attempts: %s'),
                        (self::MAX_ATTEMPTS - $nrAttempts)
                    ) . '</p>';
            }

            $html .= "
            <form method='post'>
            <input type='hidden' name='referer' value='" . htmlspecialchars($referer, ENT_QUOTES) . "' >
            <p>
            <input autofocus type='email' name='email' placeholder='" . htmlspecialchars(
                    _('Email Address'),
                    ENT_QUOTES
                ) . "'
            value = '" . htmlspecialchars($email, ENT_QUOTES) . "' />
            </p>
            <p>
            <input type='password' name='password' placeholder='" . htmlspecialchars(_('Password'), ENT_QUOTES) . "' />
            </p>
            <p>
            <input type='submit' value='" . htmlspecialchars(_('Log in'), ENT_QUOTES) . "' >
            </p>
            </form>

            <p> " . _("Don't have an account yet?") . " <a href='" . getUrl('register') . "'>" .
                _('Register here') . "</a></p>
            <p><a href='" . getUrl('forgot_pwd') . "'>" . _('Forgot your password ? ') . ' </a ></p>

            </div>
            </div>
              ';
        }

        return $this->template->render($html, ['title' => _('Log in'), 'bodyId' => 'login']);
    }

    public function handlePost()
    {
        if (User::current()) {
            return $this->seeOther(getUrl('index'));
        }

        $nrAttempts = $this->redis->get();
        if (!$nrAttempts) {
            $this->redis->set(1, self::ATTEMPT_TIMEOUT);
        } else {
            $this->redis->incr();
        }

        $email = $this->request->request->get('email');
        $password = $this->request->request->get('password');
        $referer = $this->request->request->get('referer');
        $error = false;

        if ($nrAttempts <= self::MAX_ATTEMPTS) {
            if (empty($email)) {
                $error = true;
                notifyError(_('Email is required'));
            }

            if (empty($password)) {
                $error = true;
                notifyError(_('Password is required'));
            }

            if (!$error) {
                $user = User::loginWithPassword($email, $password);

                if ($user) {
                    notifySuccess(sprintf(_('Successfully logged in as %s'), $user->userName()));
                    $this->redis->set(0, self::ATTEMPT_TIMEOUT);
                    if ($user->thirdPartyLicense() == -1) {
                        return $this->seeOther(getUrl('mypage', ['p' => 'license', 'referer' => 'login']));
                    }
                    if (!empty($referer)) {
                        return $this->seeOther($referer);
                    }
                    return $this->seeOther(getUrl('index'));
                }

                notifyError(_('Email or password is incorrect'));
            }
        }
        app()->session()->remove('user');
        return $this->renderForm($email, $referer);
    }
}
