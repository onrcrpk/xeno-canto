<?php

namespace xc\Controllers;

class AnbiPbo extends Controller
{

    public function handleRequest()
    {
        $output = "
            <h1>Required information supporting ANBI-PBO status</h1>
            <ul>
            <li>Name: Stichting Xeno-canto voor natuurgeluiden. (\"Xeno-canto Foundation\")</li>
            <li>RSIN (Legal Entities & Partnerships Identification Number): 819962946</li>
            <li>KvK number: 27327175</li>
            <li>Visiting address: Laan van Nieuw Oost Indië 177, 2593BN 's-Gravenhage</li>
            <li>Mail address: Laan van Nieuw Oost Indië 177, 2593BN 's-Gravenhage</li>
            <li>Purpose: Increase accessibility of sounds of nature; performing research into sounds of nature, 
                especially geographical and temporal variation</li>
            <li><a href='/static/files/XC_Beleidsplan_2018.pdf'>Policy plan</a></li>
            <li>Board:</li>
            <ul>
                <li>Chairman: Willem-Pier Vellinga</li>
                <li>Treasurer: Bob Planqué</li>
                <li>Secretary: Sander Pieterse</li>
            </ul>
            <li>The board is non-renumerated</li>
            <li><a href='/static/files/XC_Jaarverslag_2017.pdf'>Report of activities in 2017</a></li>
            <li><a href='/static/files/jaarrekening_XC_2017 WP_SP_BP.pdf'>Financial statement for 2017</a></li>
            <li><a href='/static/files/jaarrekening_XC_2018.pdf'>Financial statement for 2018</a></li>
            <li><a href='/static/files/jaarrekening_XC_2019.pdf'>Financial statement for 2019</a></li>
            </ul>";
        return $this->template->render($output, ['title' => _('ANBI-PBO')]);
    }
}
