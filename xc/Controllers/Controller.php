<?php

namespace xc\Controllers;

use Exception;

class Controller extends ControllerBase
{
    protected $groupId;

    protected $groups;

    public function authCheck()
    {
        return null;
    }

    public function redisKey($key, $extra = [])
    {
        // Must have initialized FrontController to set language, otherwise fail
        if (!function_exists('app')) {
            throw new Exception('Cannot determine language for Redis key, please set manually');
        }
        $lan = app()->determineBestLanguage();
        if (!is_array($extra)) {
            $extra = [$extra];
        }

        return $key . '-' . ($lan ? $lan->code : 'en') . (!empty($extra) ? '-' . implode('-', $extra) : '');
    }

    protected function printGroupSelect()
    {
        $output = "
            <form method='get'>
            <select name='gid' class='unified-dropdown' onchange='this.form.submit();'>
            <option value='0'>" . _("All groups") . "</option>\n";

        foreach ($this->groups() as $id => $name) {
            $selected = (int)$id == $this->groupId() ? 'selected' : '';
            $output .= "<option value='$id' $selected>" . ucfirst(_($name)) . "</option>";
        }
        return $output . '
            </select>
            </form>';
    }

    protected function notSpecified()
    {
        return "<span class='unspecified'>" . _('not specified') . '</span>';
    }

    protected function groupName($id = null)
    {
        if (!$id) {
            $id = $this->groupId();
        }
        return $this->groups()[$id] ?? null;
    }

    protected function groupId()
    {
        if (!$this->groupId) {
            if (!is_null($this->request->query->get('gid'))) {
                $this->groupId = (int)$this->request->query->get('gid');
                if (!array_key_exists($this->groupId, $this->groups())) {
                    $this->groupId = 0;
                }
//                $options = [
//                    'expires' => time() + 3600 * 24 * 60,
//                    'path' => '/',
//                    'samesite' => 'Lax',
//                    'httponly' => true,
//                    'secure' => true
//                ];
//                setcookie('gid', $this->groupId, $options);
                app()->setCookie('gid', $this->groupId);
            } elseif ($this->request->cookies->get('gid')) {
                $this->groupId = (int)$this->request->cookies->get('gid');
            }
        }

        return $this->groupId ?? 0;
    }

    protected function groups()
    {
        if (!$this->groups) {
            $res = query_db('SELECT id, name FROM groups');
            while ($row = $res->fetch_object()) {
                $this->groups[$row->id] = $row->name;
            }
        }
        return $this->groups;
    }
}
