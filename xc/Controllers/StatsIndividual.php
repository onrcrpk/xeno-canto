<?php

namespace xc\Controllers;

use xc\Query;
use xc\Recording;
use xc\Species;
use xc\User;

use function xc\escape;
use function xc\getUrl;
use function xc\XC_formatDuration;
use function xc\XC_userProfileURL;

class StatsIndividual extends Controller
{

    public function handleRequest($rec)
    {
        if (!$rec) {
            return $this->badRequest();
        }

        $user = User::load($rec);
        if (!$user) {
            return $this->notFound();
        }

        $content = "
            <ul class='breadcrumbs'>
            <li><a href='" . getUrl('collection-details') . "'>" . _('Collection Details') . "</a></li>
            <li><a href='" . getUrl('stats-recordists') . "'>" . _('Recordist Statistics') . "</a></li>
            <li class='current'>{$user->userName()}</li>
            </ul>
            <div class='group-select'>" . $this->printGroupSelect() . "</div>";

        $dir = escape($rec);

        $recdata = [];

        if ($this->groupId()) {
            $sql = "
                SELECT COUNT(snd_nr) as nrecordings, UNIX_TIMESTAMP(DATE(datetime)) AS ts 
                FROM birdsounds 
                WHERE dir = '$dir' AND group_id = {$this->groupId()}
                GROUP BY DATE(datetime)";
        } else {
            $sql = "
                SELECT COUNT(snd_nr) as nrecordings, UNIX_TIMESTAMP(DATE(datetime)) AS ts 
                FROM birdsounds 
                WHERE dir = '$dir' 
                GROUP BY DATE(datetime)";
        }
        $graphres = query_db($sql);
        $total_recs = 0;
        while ($row = $graphres->fetch_object()) {
            $ts = $row->ts * 1000;
            $total_recs += $row->nrecordings;
            $recdata[] = "[ $ts, $total_recs ]";
        }
        // add one extra data point on the end (in the future) so that the graph
        // continues to the end even if the user hasn't contributed recently
        $tomorrow = (time() + 60 * 60 * 24) * 1000;
        $recdata[] = "[ $tomorrow, $total_recs ]";

        $postdata = [];
        $graphres = query_db(
            "SELECT COUNT(mes_nr) as nposts, UNIX_TIMESTAMP(DATE(datetime)) AS ts FROM discussion_forum_world WHERE dir='$dir' GROUP BY DATE(datetime)"
        );
        $total_posts = 0;
        while ($row = $graphres->fetch_object()) {
            $ts = $row->ts * 1000;
            $total_posts += $row->nposts;
            $postdata[] = "[ $ts, $total_posts ]";
        }
        $postdata[] = "[ $tomorrow, $total_posts ]";

        $jsgraphdata = '[
            { data: [ ' . implode(',', $recdata) . "], label: '" . _('Recordings') . "', color: '#862709', lines: { fill: true }},
            { data: [ " . implode(',', $postdata) . "], label: '" . _('Forum comments') . "', color: '#ba5b3c', yaxis: 1},
        ]";

        $script = "
            <script type='text/javascript' src='/static/js/flot/jquery.flot.js'></script>
            <script type='text/javascript' src='/static/js/flot/jquery.flot.time.js'></script>
            <script type='text/javascript' src='/static/js/xc-plot.js'></script>
            <script type='text/javascript'>
            jQuery(document).ready(function() {
                jQuery.plot(jQuery('#plot-recordings'), $jsgraphdata, { xaxis: { mode: 'time', min: (new Date(2005,6,1)).getTime(), max: (new Date()).getTime(), tickSize: [6, 'month']}, yaxes: [{min: 0}, { label: 'posts', position: 'right'}], lines: { steps: true, show: true }, points: { show: true}, legend: {position: 'nw'}, grid: {hoverable: true}})
                jQuery('#plot-recordings').bind('plothover', xc.onplothover);
            });
            </script>";

        if ($this->groupId()) {
            $sql = "
                SELECT users.username, nrecordings, nspecies, nlocations, ncountries, length
                FROM rec_summary_stats 
                INNER JOIN users ON users.dir = rec_summary_stats.userid
                WHERE dir = '$dir' AND group_id = {$this->groupId()}";
        } else {
            $sql = "
                SELECT users.username, nrecordings, nspecies, nlocations, ncountries, length
                FROM rec_summary_stats 
                INNER JOIN users ON users.dir = rec_summary_stats.userid AND rec_summary_stats.group_id = 0
                WHERE dir = '$dir'";
        }

        $res = query_db($sql);
        $row = $res->fetch_object();
        $username = $user->userName();
        // really, there are users in the DB with empty usernames...
        if (empty($username)) {
            $username = '<i>' . _('Unknown Username') . '</i>';
        }
        $duration = XC_formatDuration($row->length ?? 0);

        $profileurl = XC_userProfileURL($dir);
        $title = $username . ' :: ' . _('Individual Statistics');

        $content .= "
        <h1>$username (<a href='$profileurl'>" . _('Public Profile') . '</a>)</h1>
        <h2>' . _('Contributions over time') . "</h2>
        <div id='plot-recordings' style='height: 250px'></div>
        <h2>" . _('Summary') . "</h2>
        <table class='results'>
        <thead>
        <tr>
            <th> " . _('Category') . ' </th>
            <th>' . _('Number') . '</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>' . _('Total Recordings') . "</td>
            <td>" . ($row->nrecordings ?? 0) . "</td>
        </tr>
        <tr>
            <td>" . _('Total Species') . "</td>
            <td>" . ($row->nspecies ?? 0) . "</td>
        </tr>
        <tr>
            <td>" . _('Total Locations') . "</td>
            <td>" . ($row->nlocations ?? 0) . "</td>
        </tr>
        <tr>
            <td>" . _('Total Countries') . " </td>
            <td>" . ($row->ncountries ?? 0) . "</td>
        </tr>
        <tr>
            <td>" . _('Recording Time') . "</td>
            <td>$duration</td>
        </tr>
        </tbody>
        </table>
        ";

        $localName = localNameLanguage();
        if ($this->groupId()) {
            $sql = "
                SELECT tax.genus, tax.species, tax.eng_name, cnames.$localName as localName, species_nr, nrecordings
                FROM rec_unique_species
                INNER JOIN taxonomy tax USING(species_nr)
                LEFT JOIN taxonomy_multilingual cnames USING(species_nr)
                WHERE userid = '$dir' AND rec_unique_species.group_id = {$this->groupId()}
                ORDER BY " . ($this->groupId() == 1 ? 'tax.IOC_order_nr' : 'tax.genus, tax.species');
        } else {
            $sql = "
                SELECT tax.genus, tax.species, tax.eng_name, cnames.$localName as localName, species_nr, 
                    SUM(nrecordings) AS nrecordings
                FROM rec_unique_species
                INNER JOIN taxonomy tax USING(species_nr)
                LEFT JOIN taxonomy_multilingual cnames USING(species_nr)
                WHERE userid = '$dir'
                GROUP BY species_nr
                ORDER BY tax.genus, tax.species";
        }
        $res = query_db($sql);

        if ($res && $res->num_rows > 0) {
            $nrunique = $res->num_rows;
            $content .= '
            <h2>' . _('Unique Species') . '</h2>
            <p>' . sprintf(
                    _('%s has recorded %s species that no other xeno-canto member has recorded'),
                    $username,
                    $nrunique
                ) . "</p>
            <table class='results'>
            <thead>
            <tr>
                <th>" . _('Species Name') . '</th>
                <th>' . _('Num. Recordings') . '</th>
            </tr>
            </thead>
            <tbody>';
            while ($row = $res->fetch_object()) {
                $cname = $row->localName;
                if (!$cname) {
                    $cname = $row->eng_name;
                }
                $sp = new Species($row->genus, $row->species, $cname, $row->species_nr);
                $nr = $row->nrecordings;
                $content .= "
                <tr>
                    <td><a href='{$sp->profileURL()}'>{$sp->htmlDisplayName()}</a></td>
                    <td>$nr</td>
                </tr>";
            }
            $content .= '
            </tbody>
            </table>
            ';
        }

        if ($this->groupId()) {
            $sql = "
                SELECT country, nrecordings, nspecies, length
                FROM rec_countries
                WHERE userid = '$dir' AND group_id = {$this->groupId()}
                ORDER BY nrecordings DESC";
        } else {
            $sql = "
                SELECT 
                    country, 
                    SUM(nrecordings) AS nrecordings, 
                    SUM(nspecies) AS nspecies, 
                    SUM(length) AS length
                FROM rec_countries
                WHERE userid = '$dir' 
                GROUP BY country
                ORDER BY nrecordings DESC";
        }

        $res = query_db($sql);
        if ($res && $res->num_rows > 0) {
            $content .= '
            <h2>' . _('Countries with the most recordings') . "</h2>
            <table class='results'>
            <thead>
            <tr>
                <th> " . _('Country') . '</th>
                <th>' . _('Num. Recordings') . '</th>
                <th>' . _('Num. Species') . '</th>
                <th>' . _('Recording Time') . '</th>
            </tr>
            </thead>
            <tbody>';
            
            while ($row = $res->fetch_object()) {
                $cnt = $row->country;
                $nr = $row->nrecordings;
                $nsp = $row->nspecies;
                $duration = XC_formatDuration($row->length);
                $query = "dir:$dir cnt:\"=$cnt\"";
                if ($this->groupId()) {
                    $query .= " grp:{$this->groupName()}";
                }

                $content .= "
                    <tr>
                        <td><a href='" . getUrl('browse', ['query' => $query]) . "'>$cnt</a></td>
                        <td>$nr</td>
                        <td>$nsp</td>
                        <td>$duration</td>
                    </tr>";
            }
            $content .= '
            </tbody>
            </table>
            ';
        }

        // Most-frequently recorded species
        $lname = localNameLanguage();
        if ($this->groupId()) {
            $sql = "
                SELECT tax.eng_name, tax.genus, tax.species, tax.species_nr, M.$lname as localName, nrecordings
                FROM rec_common_species 
                INNER JOIN taxonomy tax USING(species_nr)
                LEFT JOIN taxonomy_multilingual M USING(species_nr)
                WHERE userid = '$dir' AND rec_common_species.group_id = {$this->groupId()}
                ORDER BY nrecordings DESC, tax.IOC_order_nr ASC 
                LIMIT 20";
        } else {
            $sql = "
                SELECT tax.eng_name, tax.genus, tax.species, tax.species_nr, M.$lname as localName, nrecordings
                FROM rec_common_species 
                INNER JOIN taxonomy tax USING(species_nr)
                LEFT JOIN taxonomy_multilingual M USING(species_nr)
                WHERE userid = '$dir' 
                ORDER BY nrecordings DESC, tax.IOC_order_nr ASC 
                LIMIT 20";
        }

        $res = query_db($sql);
        if ($res && $res->num_rows > 0) {
            $content .= '
            <h2>' . _('Most Frequently Recorded Species') . "</h2>
            <table class='results'>
            <thead>
            <tr>
                <th>" . _('Species Name') . '</th>
                <th>' . _('Num. Recordings') . '</th>
            </tr>
            </thead>
            <tbody>';
            while ($row = $res->fetch_object()) {
                $cname = $row->localName;
                if (!$cname) {
                    $cname = $row->eng_name;
                }
                $sp = new Species($row->genus, $row->species, $cname, $row->species_nr);
                $nr = $row->nrecordings;
                $content .= "
                    <tr>
                        <td><a href='{$sp->profileURL()}'>{$sp->htmlDisplayName()}</a></td>
                        <td>$nr</td>
                    </tr>";
            }
            $content .= "
            </tbody>
            </table>";
        }

        // Latest new species
        $lname = localNameLanguage();
        if ($this->groupId()) {
            $sql = "
                SELECT tax.eng_name, tax.genus, tax.species, tax.species_nr, M.$lname AS localName, tax.family, 
                    '' AS ssp, MIN(snd_nr) AS snd_nr
                FROM birdsounds 
                INNER JOIN taxonomy tax USING(species_nr)
                LEFT JOIN taxonomy_multilingual M USING(species_nr)
                WHERE dir = '$dir' AND " . Query::ignoreMysteryAndPseudoSpeciesSql() . " 
                    AND birdsounds.group_id = {$this->groupId()}
                GROUP BY species_nr 
                ORDER BY MIN(snd_nr) DESC, tax.IOC_order_nr ASC 
                LIMIT 20";
        } else {
            $sql = "
                SELECT tax.eng_name, tax.genus, tax.species, tax.species_nr, M.$lname AS localName, tax.family, 
                    '' AS ssp, MIN(snd_nr) AS snd_nr
                FROM birdsounds 
                INNER JOIN taxonomy tax USING(species_nr)
                LEFT JOIN taxonomy_multilingual M USING(species_nr)
                WHERE dir = '$dir' AND " . Query::ignoreMysteryAndPseudoSpeciesSql() . " 
                GROUP BY species_nr 
                ORDER BY MIN(snd_nr) DESC, tax.IOC_order_nr ASC 
                LIMIT 20";
        }

        $res = query_db($sql);
        if (!$res) {
            echo(mysqli()->error);
        }
        if ($res && $res->num_rows > 0) {
            $content .= '
            <h2>' . _('Latest New Species') . "</h2>
            <table class='results'>
            <thead>
            <tr>
                <th>" . _('Catalog Number') . '</th>
                <th>' . _('Species name') . '</th>
            </tr>
            </thead>
            <tbody>';
            while ($row = $res->fetch_object()) {
                $cname = $row->localName;
                if (!$cname) {
                    $cname = $row->eng_name;
                }
                $rec = new Recording($row);
                $sp = $rec->species();
                $content .= "
                    <tr>
                        <td><a href='{$rec->URL()}'>XC{$rec->xcid()}</a></td>
                        <td><a href='{$sp->profileURL()}'>{$sp->htmlDisplayName()}</a></td>
                    </tr>";
            }
            $content .= "
            </tbody>
            </table>";
        }

        return $this->template->render(
            $content . $script,
            ['title' => $title, 'bodyId' => 'stats-individual']
        );
    }

}
