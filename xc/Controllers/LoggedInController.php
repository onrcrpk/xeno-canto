<?php

namespace xc\Controllers;

use xc\User;

use function xc\getUrl;

class LoggedInController extends Controller
{

    public function authCheck()
    {
        if (User::current()) {
            return null;
        }

        $title  = _('Login Required');
        $prompt = "
            <h1>$title</h1>
            <p>" .
                  _('Oops! You have to be logged in to use this feature.') . '
            </p>
            <p>' .
                  sprintf(
                      _(
                          "If you are registered you can <a href='%s'>log in here</a>. Or else
                <a href='%s'>please register here</a>; it only takes a minute."
                      ),
                      getUrl('login'),
                      getUrl('register')
                  ) . '
                </p>';

        return $this->template->render($prompt, ['title' => $title]);
    }
}
