<?php

namespace xc\Controllers;

use xc\MultiSpeciesMap;
use xc\Query;
use xc\XCRedis;

use function xc\getUrl;
use function xc\sanitize;
use function xc\XC_makeViewLinks;
use function xc\XC_pageNumber;
use function xc\XC_pageNumberNavigationWidget;
use function xc\XC_resultsTableForView;

class Mysteries extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
        $this->redis->setKeyByRequest($this->redisKey('mystery-index', $this->groupId()), $request);
    }

    public function handleRequest()
    {
        $title = _('Mystery Recordings');
        $output = $this->redis->get();

        if (!$output) {
            $num_per_page = defaultNumPerPage();
            $output = '';
            $resultsTable = '';

            $dir = $this->request->query->get('dir');
            $order = $this->request->query->get('order', 'xc');
            $view = $this->request->query->get('view', defaultResultsView());

            $pagenumber = XC_pageNumber($this->request);

            $filter = $this->request->query->get('query');

            if (!$filter) {
                $filter = "since:365";
                if ($this->groupId() >= 1) {
                    $filter = "grp:" . $this->groupName() . " $filter";
                }
            }

            $fgQuery = new Query($filter);
            $fgQuery->setMysteryOnly();
            $fgQuery->setOrder($order, $dir);

            $profileMap = new MultiSpeciesMap('map-canvas', $fgQuery->execute());

            //now do the results table
            $number_of_hits = $fgQuery->numRecordings();
            $no_pages = ceil($number_of_hits / $num_per_page);

            $res = $fgQuery->execute($pagenumber);

            $links = XC_makeViewLinks($this->request, $view);
            $resultsTable .= "
                <p> <div class=\"results-format\">" . _('Results format') . ": $links </div></p>";
            $resultsTable .= XC_pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);
            $resultsTable .= XC_resultsTableForView($this->request, $res, $view);
            $resultsTable .= XC_pageNumberNavigationWidget($this->request, $no_pages, $pagenumber);

            $mapSidebar = '<h3>' . _('Help solve mysteries!') . '</h3>
            <p>' . _(
                    'Our recordists cannot always identify every sound they hear and could use your help to identify the species in these recordings.'
                ) . '</p>
            <p>' . _(
                    'This page displays recordings that were uploaded as mysteries in addition to recordings whose identity has been questioned.'
                ) . '
            </p>
            <h3>' . _('Get help with your mystery recording!') . '</h3>
            <p>' . _(
                    "Have you recorded a wildlife sound that you can't identify? Upload a mystery sound and see if somebody else can identify it."
                ) . "
            </p>
            <br><br>
            <span class='prominent-button'><a href='" . getUrl('upload', ['mystery' => 1]) . "'>" .
                _('Upload a mystery recording') . '</a></span>';

            $resultsSummary = '
                <p>' . sprintf(
                    _('%s mystery recordings'),
                    "<strong>{$fgQuery->numRecordings()}</strong>"
                ) . '</p>';

            $output .= "
                <h1>$title</h1>
               
                <table id=\"map-table\">
                <tr>
                <td id=\"map-cell\">
                <p>" . _(
                    "Initial results are limited to mystery recordings for the selected group for a period of a year, as the complete set tends to overwhelm the map."
                ) . " " . sprintf(
                    _(
                        "You can tweak the results by updating the filter (below the map) using %s. Examples are %s or %s.",
                    ),
                    '<a href="' . getUrl('tips') . '">' . _('search tags') . '</a>',
                    '<a href="' . getUrl('mysteries', ['query' => 'grp:birds area:asia year:">2018"']) . '">' .
                    _('all mystery birds from Asia after 2018') . '</a>',
                    '<a href="' . getUrl('mysteries', ['query' => 'cnt:"united states" q:">C"']) . '">' .
                    _('very good to excellent sound quality mysteries from the US') . '</a>'
                ) .
                "</p>
                <div id=\"map-canvas\"></div>
                </td>
                <td>
                <div class=\"map-sidebar\">
                <div>" . $this->printGroupSelect() . "</div>
                <div style='margin-top: 20px;'>
                $mapSidebar
                </div>
                </div>
                </td>
                </tr>
                <tr>
                <td>
                <form id='filter-form' method='get' style='margin-top: 8px;'>
                <table>
                <tr>
                <td>
                <input type='text' placeholder='" . sanitize(_('Filter results')) .
                "...' name='query' value='" . sanitize($filter) . "' />
                </td>
                <td>
                <input type='submit' value='" . sanitize(_('Filter results')) . "' />
                </td>
                </tr>
                </table>
                </form>
                </td>
                </tr>
                </table>
    
            $resultsSummary
            $resultsTable
            {$profileMap->getJS()}";

            $this->redis->set($output, 3600);
        }

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'species-profile']
        );
    }
}
