<?php

namespace xc\Controllers;

use function xc\getUrl;

class Redirect extends Controller
{

    public function moved($route)
    {
        $url = getUrl($route, $this->request->query->all());
        return $this->movedPermanently($url);
    }
}
