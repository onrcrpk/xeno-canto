<?php

/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace xc\Controllers;

use xc\Query;
use xc\Set;
use xc\User;

use function xc\getUrl;
use function xc\sanitize;
use function xc\strip;
use function xc\XC_formatUserText;
use function xc\XC_makeViewLinks;
use function xc\XC_pageNumber;
use function xc\XC_pageNumberNavigationWidget;
use function xc\XC_resultsTableForView;

class SetActions
{

    private $set;

    public function __construct($set)
    {
        $this->set = $set;
    }

    public function actions($rec)
    {
        $user = User::current();
        if (
            $user && ($user->isAdmin() || ($user->userId() == $this->set->userId()))
        ) {
            return "<a href='" . getUrl(
                    'set-remove-recording',
                    ['id' => $this->set->id(), 'xcid' => $rec->xcid()]
                ) . "' title='" . _(
                    'Remove from set'
                ) . "'><img class='icon' src='/static/img/bookmark-remove.png' /></a>";
        }
        return '';
    }
}

class SetView extends Controller
{

    public function showSet($id)
    {
        $set = Set::load(strip($id));
        if (!$set) {
            return $this->notFound();
        }

        $user = User::current();
        $allowed = false;
        $canEdit = false;
        if ($set->visibility() != Set::VISIBILITY_PRIVATE) {
            $allowed = true;
        }

        if ($user) {
            if ($user->isAdmin() || ($user->userId() == $set->userId())) {
                $allowed = true;
                $canEdit = true;
            }
        }

        if (!$allowed) {
            return $this->unauthorized();
        }

        $pagenumber = XC_pageNumber($this->request);
        $order = $this->request->query->get('order');
        $direction = $this->request->query->get('dir');

        $num_per_page = defaultNumPerPage();

        $setUser = User::load($set->userId());
        $actions = '';
        if ($canEdit) {
            $actions = "<a href='" . getUrl(
                    'set-edit',
                    ['id' => $id]
                ) . "'><img src='/static/img/edit.png' title='" . sanitize(
                    _(
                        'Edit Set Details'
                    )
                ) . "' class='icon'/></a>" .
                "<a href='" . getUrl(
                    'set-delete',
                    ['id' => $id]
                ) . "'><img src='/static/img/delete.png' title=" . sanitize(
                    _(
                        'Delete Set'
                    )
                ) . "'' class='icon'/></a>";
        }

        $body = "<header>
            <h1>{$set->name()} $actions</h1>
            <div>" . sprintf(
                _('Curated by %s'),
                "<a href='{$setUser->getProfileURL()}'>{$setUser->userName()}</a>"
            ) . '</div>';

        if ($set->description()) {
            $body .= "
            <div id='set-description'>" .
                XC_formatUserText($set->description()) . '
            </div>';
        }

        $filter = $this->request->query->get('filter');
        $qstring = "$filter set:{$set->id()}";
        $q = new Query($qstring, Query::OPT_INCLUDE_MYSTERIES);
        //$q->useLocalNameSearch();
        $q->setOrder($order, $direction);
        $total = $q->numRecordings();
        $nSpecies = $q->numSpecies();
        $no_pages = ceil($total / $num_per_page);
        $res = $q->execute($pagenumber);

        $body .= "
            <form method='get' class='hflex'>
            <input class='flex-main' type='text' name='filter' value='" . sanitize(
                $filter
            ) . "' placeholder='" . sanitize(
                _(
                    'Filter recordings'
                )
            ) . "...' />
            <input type='submit' value='" . sanitize(_('Filter')) . "'/>
            </form>
            <p>" . sprintf(
                ngettext(
                    '%d recording',
                    '%d recordings',
                    $total
                ),
                $total
            ) . ', ' .
            sprintf(
                ngettext(
                    '%d species',
                    '%d species',
                    $nSpecies
                ),
                $nSpecies
            ) . '</p>
                </header>';

        $body .= XC_pageNumberNavigationWidget(
            $this->request,
            $no_pages,
            $pagenumber
        );
        $view = $this->request->query->get('view', defaultResultsView());
        $body .= "<div class='results-format'>" .
            _(
                'Results format'
            ) . ': ' . XC_makeViewLinks($this->request, $view) . '</div>';

        $setActions = new SetActions($set);
        $body .= XC_resultsTableForView(
            $this->request,
            $res,
            $view,
            [$setActions, 'actions']
        );
        $body .= XC_pageNumberNavigationWidget(
            $this->request,
            $no_pages,
            $pagenumber
        );
        $body .= "<script type='text/javascript'>
jQuery(document).ready(function() {
    jQuery('div#set-description').expander({
        slicePoint: 200,
        expandText: '" . _('more') . " &raquo;',
        userCollapseText: '&laquo; " . _('less') . "'
    });
});
</script>";

        return $this->template->render(
            $body,
            ['title' => $set->name(), 'bodyId' => 'set']
        );
    }

    public function redirect()
    {
        $id = $this->request->query->get('set');
        if ($id) {
            return $this->movedPermanently(getUrl('set', ['id' => $id]));
        }
        return $this->notFound();
    }
}
