<?php

namespace xc\Controllers;

use xc\HtmlUtil;
use xc\Species;
use xc\WorldArea;
use xc\XCRedis;

use function xc\getUrl;

class CollectionSpecies extends Controller
{
    private $redis;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->redis = new XCRedis();
        $this->redis->setKeyByRequest($this->redisKey('daily-stats-species'), $request);
    }

    public function handleRequest()
    {
        $pageTitle = _('Species');
        $output = $this->redis->get();

        if (!$output) {
            $branch = $this->request->query->get('area');
            $area = WorldArea::lookup($branch);

            $selectForm = '
            <form >' . HtmlUtil::regionSelect('area', $branch) . "</select>
            <input type='submit' value='" . htmlspecialchars(_('Submit'), ENT_QUOTES) . "'/>
            </form>";

            $output = "
            <ul class='breadcrumbs'>
            <li><a href='" . getUrl('collection-details') . "'>" . _('Collection Details') . "</a></li>
            <li>$pageTitle</li>
            <li class='current'>$selectForm</li>
            </ul>
            ";

            $localName = localNameLanguage();
            if ($area) {
                $basesql = "
                    SELECT T.genus, T.species, T.eng_name, T.family, T.species_nr, T.IOC_order_nr, T.recordings, 
                        T.back_recordings, C.$localName AS localName, extinct, T.group_id
                    FROM taxonomy T 
                    LEFT JOIN taxonomy_multilingual C USING(species_nr) 
                    INNER JOIN checklists_branch CB USING (species_nr) 
                    WHERE CB.branch='$area->branch' AND " . Species::excludePseudoSpeciesSQL('T');
            } else {
                $basesql = "
                    SELECT T.genus, T.species, T.eng_name, T.family, T.species_nr, T.IOC_order_nr, T.recordings, 
                        T.back_recordings, C.$localName as localName, extinct, T.group_id
                    FROM taxonomy T 
                    LEFT JOIN taxonomy_multilingual C USING(species_nr) 
                    WHERE " . Species::excludePseudoSpeciesSQL('T') . " AND T.group_id = 1";
            }

            $countSql = "
                SELECT COUNT(*) AS nr FROM (
                $basesql AND recordings != 0
                UNION
                $basesql AND back_recordings != 0) A";

            $res = query_db($countSql);
            $row = $res->fetch_object();
            $grand_total = $row->nr;

            $sql = "$basesql ORDER BY IOC_order_nr";
            $res = query_db($sql);

            $grand_total2 = $res->num_rows;
            $pct = round(($grand_total / $grand_total2 * 100));

            $table = '';
            $fam_old = null;
            while ($row = $res->fetch_object()) {
                $cname = $row->localName;
                if (!$cname) {
                    $cname = $row->eng_name;
                }
                $sp = new Species($row->genus, $row->species, $cname, $row->species_nr);

                $fam_nu = $row->family;
                if (strcmp($fam_nu, $fam_old) != '0') {
                    $class = 'new-species';
                } else {
                    $class = '';
                }

                if (!($row->recordings || $row->back_recordings)) {
                    $class .= ' missing';
                }

                $extinct = '';
                if ($row->extinct) {
                    $class .= ' extinct';
                    $extinct = _('Extinct');
                }

                $table .= "
                    <tr class='$class'>
                    <td>
                    <span clas='common-name'>
                    <a href=\"{$sp->profileURL()}\">{$sp->commonName()}</a>
                    </span>
                    </td>
                    <td>{$sp->scientificName()}</td>
                    <td>$extinct</td>
                    <td align='right' width='20'>$row->recordings</td>
                    <td align='right' width='30'>$row->back_recordings</td>
                    </tr>

                    ";
                $fam_old = $fam_nu;
            }

            $summary = sprintf(
                _("A list of all %s. That's %s of the total number of %s species."),
                "<b>$grand_total</b>",
                "<b>$pct%</b>",
                $grand_total2
            );
            if ($area) {
                $summary = sprintf(
                    _(
                        "%s species from the %s checklist are represented on xeno-canto. That's %s of the total number of %s species."
                    ),
                    "<b>$grand_total</b>",
                    "<b>{$area->desc}</b>",
                    "<b>$pct%</b>",
                    $grand_total2
                );
            }
            $output .= "<p>$summary</p>
            <p>" .
                sprintf(
                    _(
                        "'%s' refers to the number of recordings in which a species can be heard in the background."
                    ),
                    _(
                        'No. Back'
                    )
                ) . "
            </p>

            <table class='results'>
            <thead>
            <th>" . _('Common name') . '</th>
            <th>' . _('Scientific name') . '</th>
            <th>' . _('Status') . '</th>
            <th>' . _('No.') . '</th>
            <th>' . _('No. Back') . '</th>
            </thead>
            ';

            $output .= $table;

            $output .= '
            </table>
            ';

            $this->redis->set($output);
        }

        return $this->template->render(
            $output,
            ['title' => $pageTitle, 'bodyId' => 'checklists']
        );
    }
}
