<?php

namespace xc\Controllers;

use xc\WorldArea;

use function xc\getUrl;

class SearchHelp extends Controller
{
    private $tagProperties = [];

    public function handleRequest()
    {
        /// minimum latitude
        $LAT_MIN = _('LAT_MIN');
        /// minimum longitude
        $LON_MIN = _('LON_MIN');
        /// maximum latitude
        $LAT_MAX = _('LAT_MAX');
        /// maximum longitude
        $LON_MAX = _('LON_MAX');

        $areaTagOptions = [];
        foreach (WorldArea::all() as $area) {
            $areaTagOptions[] = "<tt>{$area->branch}</tt>";
        }
        $areaTagOptions = implode(', ', $areaTagOptions);

        $title = _('Search Tips');
        $output = "
            <h1>$title</h1>
            <p>" . _(
                "In order to find the recordings you're interested in, it can be helpful to know a little more about how search queries work."
            ) . '
            </p>
            <h2>' . _('Basic Queries') . '</h2>
            <p>
            ' . sprintf(
                _(
                    'A normal query such as %s will search through the following fields:'
                ),
                '<tt>Tinamus major</tt>'
            ) . '
            </p>
            <ul>
            <li>' . _('Common name') . '</li>
            <li>' . _('Scientific name') . '</li>
            <li>' . _('Scientific name of family') . '</li>
            </ul>
            </p>
            <p>
            ' . sprintf(
                _(
                    'In addition to scientific or common names, you can use the term %s to search for recordings featuring multiple species, or %s for recordings with unidentified species.'
                ),
                '<tt>soundscape</tt>',
                '<tt>mystery</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'Searches are not case-sensitive, so the search term %s will return the same results as the search term %s.'
                ),
                '<tt>TINAMUS</tt>',
                '<tt>tinamus</tt>'
            ) . '
            </p>
            <p>
            ' . _(
                "Wildcards such as '*' are not necessary.  Where possible, we will search for your query term as a substring of the full field.  However, in order to keep database performance at an acceptable level, we can't match on every substring.  So, even though you may have typed a string that is contained with a particular species' name (e.g. \"spot\" within \"Spot-breasted Wren\"), it is possible that the search won't return recordings of that species as a match.  We're always trying to improve our performance and capabilities, so this aspect may be improved in the future."
            ) . '
            </p>
            
            <h2>' . _('Search Languages') . '</h2>
            <p>' . _(
                'Common names are always searched in English, plus the language you have selected for the interface (at the bottom right). Common names in languages other than English are available mostly for birds.'
            ) . '
            </p>
            <p class="important">' . sprintf(
                _(
                    'If you have registered and are logged in, you can set multiple languages to search for common names. Visit <a href="%s">your preference page</a> to select these!'
                ),
                getUrl('mypage', ['p' => 'prefs'])
            ) . '
            </p>
            
            <h2>' . _('Advanced Queries') . '</h2>
            <p>
            ' . _(
                'If you want to search for more specific details of a recording, you can use tags in your queries. This allows you to search within a particular aspect of a recording, for example, the recordist name, location, sound type, and more.'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    "The <a href='%s'>advanced search</a> page allows you to search these additional fields. These fields (and several extras) can also be searched by using the keywords as described below."
                ),
                getUrl('search')
            ) . '
            </p>
            <p>
            ' . _(
                'Tags are of the form <tt>tag:searchterm</tt>. In other words, the tag is a short string followed by a colon, followed by the term you want to search for. The tag specifies which field of the recording you want to search.'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'More than one tag can be used at a time and they can also be combined with plain queries. When this is done, the results must match all queries in order to be returned for the search.  So for example, the query %s will only return results for birds in the genus %s that were recorded at a location whose name contains %s and that were recorded within a country whose name contains %s.'
                ),
                '<tt>orthonyx cnt:papua loc:tari</tt>',
                '<i>Orthonyx</i>',
                "\"tari\"",
                "\"papua\""
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'To perform a tag search for a term that has multiple words, <b>you must enclose the words in double quotes</b>. For example, %s will search for all recordings recorded in a country matching %s. Without the quotes, only the first word will be used for the tag query, so it will search for recordings recorded in a country matching %s and whose common name, latin name, or family name matches %s (according to the rules for basic queries above).'
                ),
                "<tt>cnt:\"United States\"</tt>",
                "\"United States\"",
                "\"United\"",
                "\"States\""
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    "Occasionally, a wildcard search does not deliver the best results. You can prepend an '=' to many search terms to force an exact match. <b>Such searches must also be enclosed in double quotes</b>. E.g. use %s to search for recordings from Niger, but not from Nigeria. Besides '=', also '>' (greater than) and '<' (less than) can be used for selected fields. The field descriptions below explicitly mention the accepted operators."
                ),
                '<tt>cnt:"=niger"</tt>'
            ) . '
            </p>
            <p>
            ' . _(
                'The fields that can be searched using tags are specified below.'
            ) . '
            </p>
            <h3>' . _('Group') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'Use the %s tag to narrow down your search to a specific group. This tag is particularly useful in combination with one of the other tags. Valid group values are %s, %s and %s. You can also use their respective ids (%s to %s), so %s will restrict your search to grasshoppers. Soundscapes are a special case, as these recordings may include multiple groups. Use %s or %s to search these.'
                ),
                '<tt>grp</tt>',
                '<tt>birds</tt>',
                '<tt>grasshoppers</tt>',
                '<tt>bats</tt>',
                '<tt>1</tt>',
                '<tt>3</tt>',
                '<tt>grp:2</tt>',
                '<tt>grp:soundscape</tt>',
                '<tt>grp:0</tt>'
            ) . '
            </p>
            <h3>' . _('Genus/Subspecies') . '</h3>
            <p>
            ' . sprintf(
                _(
                    "Genus is part of a species' scientific name, so it is searched by default when performing a basic search (as mentioned above). But you can use the %s tag to limit your search query only to the genus field. So %s will find all recordings of sparrows in the genus Zonotrichia."
                ),
                '<tt>gen</tt>',
                '<tt>gen:zonotrichia</tt>'
            ) . ' ' . '
            ' . sprintf(
                _(
                    "Similarly, %s can be used to search for subspecies. These fields use a 'starts with' rather than 'contains' query and accept a 'matches' operator."
                ),
                '<tt>ssp</tt>'
            ) . ' ' . '
            </p>
            <h3>' . _('Recordist') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'To search for all recordings from a particular recordist, use the %s tag.  For example, %s will return all recordings from recordists whose names contain the string %s.'
                ),
                '<tt>rec</tt>',
                '<tt>rec:John</tt>',
                "\"John\""
            ) . ' ' . '
            ' . _("This field accepts a 'matches' operator.") . '
            </p>
            <h3>' . _('Country') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'To return all recordings that were recorded in the a particular country, use the %s tag.  The following query will return all recordings from the country of %s: %s.'
                ),
                '<tt>cnt</tt>',
                "\"Brazil\"",
                '<tt>cnt:brazil</tt>'
            ) . ' ' . _(
                "This field uses a 'starts with' query and accepts a 'matches' operator."
            ) . '
               </p>
            <h3>' . _('Location') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'To return all recordings from a specific location, use the %s tag. For example %s.'
                ),
                '<tt>loc</tt>',
                '<tt>loc:tambopata</tt>'
            ) . ' ' . _("This field accepts a 'matches' operator.") . '
            </p>
            <h3>' . _('Recordist remarks') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'Many recordists leave remarks about the recording and this field can be searched using the %s tag, e.g. %s. The remarks field contains free text, so it is unlikely to produce complete results. Note that information about whether the recorded animal was seen or if playback was used, formerly stored in remarks, now can be searched using dedicated fields! This field accepts a \'matches\' operator.'
                ),
                '<tt>rmk</tt>',
                '<tt>rmk:flock</tt>'
            ) . '
            </p>
            <h3>' . _('Animal seen') . '/' . _('Playback used') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'Two tags (%s and %s respectively) that previously were stored as part of Recordist remarks, but now can be used independently. Both only accept %s and %s as input. For example, use %s to search for recordings where the animal was seen, but not lured by playback.'
                ),
                '<tt>seen</tt>',
                '<tt>playback</tt>',
                '<tt>yes</tt>',
                '<tt>no</tt>',
                '<tt>seen:yes playback:no</tt>',
            ) . '
            </p>
            <h3>' . _('Geographic coordinates') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'There are two sets of tags that can be used to search via geographic coordinates. The first set of tags is %s and %s. These tags can be used to search within one degree in either direction of the given coordinate, for instance: %s.'
                ),
                '<tt>lat</tt>',
                '<tt>lon</tt>',
                '<tt>lat:-12.234 lon:-69.98</tt>'
            ) . ' ' . sprintf(
                _(
                    "This field also accepts '&lt;' and '&gt;' operators; e.g. use %s to search for all recordings made above the Arctic Circle."
                ),
                '<tt>lat:"&gt;66.5"</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    "The second tag allows you to search for recordings that occur within a given rectangle, and is called %s.  It is more versatile than %s and %s, but is more awkward to type in manually, so we have made a <a href='%s'>map-based search tool</a> to make things simpler. The general format of the %s tag is as follows: %s. Note that there must not be any spaces between the coordinates."
                ),
                '<tt>box</tt>',
                '<tt>lat</tt>',
                '<tt>lon</tt>',
                getUrl('browse-region'),
                '<tt>box</tt>',
                "<tt>box:$LAT_MIN,$LON_MIN,$LAT_MAX,$LON_MAX</tt>"
            ) . '
            </p>
            <h3>' . _('Background species') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'To search for recordings that have a given species in the background, use the %s tag. Use this field to search for both species (common names in English and scientific names) and families (scientific names). For example, %s will return all recordings that have a member of the Antthrush family identified as a background voice.'
                ),
                '<tt>also</tt>',
                '<tt>also:formicariidae</tt>'
            ) . '
            </p>
            <h3>' . _('Sound type') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'To search for recordings of a particular sound type, use the %s tag. For instance, %s will return all recordings identified as songs. Note that options may pertain to a specific group only, e.g. \'searching song\' is a search term used for grasshoppers, but not for birds. Valid values for this tag are: %s. This tag always uses a \'matches\' operator.'
                ),
                '<tt>type</tt>',
                '<tt>type:song</tt>',
                $this->tagSearchValues('type')
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'Up until 2022, the \'type\' tag used to search a free text field. We have retained the option to search for non-standardized sound types by using the %s tag. This tag also accepts a \'matches\' operator, e.g. %s.'
                ),
                '<tt>othertype</tt>',
                '<tt>othertype:"=wing flapping"</tt>'
            ) . '
            </p>
            <h3>' . _('Sex') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'Formerly included under \'sound types\', the %s tag can now be used independently. Valid values for this tag are: %s. This tag always uses a \'matches\' operator.'
                ),
                '<tt>sex</tt>',
                $this->tagSearchValues('sex')
            ) . '
            </p>
            <h3>' . _('Life stage') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'Values of the %s tag were previously included under \'sound types\' as well. Valid values are: %s. This tag always uses a \'matches\' operator.'
                ),
                '<tt>stage</tt>',
                $this->tagSearchValues('stage')
            ) . '
            </p>
            <h3>' . _('Recording method') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'The %s tag accepts the following, group-dependent values: %s. Do not forget to enclose the term between double quotes! This tag always uses a \'matches\' operator.'
                ),
                '<tt>method</tt>',
                $this->tagSearchValues('method'),
            ) . '
            </p>
            <h3>' . _('XC number') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'All recordings on xeno-canto are assigned a unique catalog number (generally displayed in the form XC76967).  To search for a known recording number, use the %s tag: for example %s.  You can also search for a range of numbers as %s.'
                ),
                '<tt>nr</tt>',
                '<tt>nr:76967</tt>',
                '<tt>nr:88888-88890</tt>'
            ) . '
            </p>
            <h3>' . _('Recording license') . '</h3>
            <p>
            ' . sprintf(
                _(
                    "Recordings on xeno-canto are licensed under a small number of different Creative Commons licenses.  You can search for recordings that match specific license conditions using the %s tag.  License conditions are Attribution (BY), NonCommercial (NC), ShareAlike (SA),  NoDerivatives (ND) and Public Domain/copyright free (CC0).  Conditions should be separated by a '-' character. For instance, to find recordings that are licensed under an Attribution-NonCommercial-ShareAlike license, use %s; for \"no rights reserved\" recordings, use %s.  See the <a href='//creativecommons.org/licenses/'>Creative Commons</a> website for more details about the individual licenses."
                ),
                '<tt>lic</tt>',
                '<tt>lic:BY-NC-SA</tt>',
                '<tt>lic:PD</tt>'
            ) . '
            </p>
            <h3>' . _('Recording quality') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'Recordings are rated by quality. Quality ratings range from A (highest quality) to E (lowest quality). To search for recordings that match a certain quality rating, use the %s tag. This field also accepts \'&lt;\' and \'&gt;\' operators. For example:'
                ),
                '<tt>q</tt>'
            ) . '
            </p>
            <ul>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings with a quality rating of A.'
                ),
                '<tt>q:A</tt>'
            ) . '
            </li>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings with a quality rating of D or E.'
                ),
                '<tt>q:"&ltC"</tt>'
            ) . '
            </li>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings with a quality rating of B or A.'
                ),
                '<tt>q:"&gt;C"</tt>'
            ) . '
            </li>
            </ul>
            <p>
            ' . sprintf(
                _(
                    'Note that not all recordings are rated. Unrated recordings will not be returned for a search on quality rating.'
                ),
                '<tt>q:0</tt>'
            ) . '
            </p>
            <h3>' . _('Recording length') . '</h3>
            <p>
            ' . sprintf(
                _(
                    'To search for recordings that match a certain length (in seconds), use the %s tag. This field also accepts \'&lt;\' , \'&gt;\' and \'=\' operators. For example:'
                ),
                '<tt>len</tt>'
            ) . '
            </p>
            <ul>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings with a duration of 10 seconds (with a margin of 1%%, so actually between 9.9 and 10.1 seconds)'
                ),
                '<tt>len:10</tt>'
            ) . '
            </li>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings lasting between 10 and 15 seconds.'
                ),
                '<tt>len:10-15</tt>'
            ) . '
            </li>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings half a minute or shorter in length.'
                ),
                '<tt>len:"<30"</tt>'
            ) . '
            </li>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings longer than two minutes in length.'
                ),
                '<tt>len:">120"</tt>'
            ) . '
            </li>
            <li>
            ' . sprintf(
                _(
                    '%s will return recordings lasting exactly 19.8 seconds, dropping the default 1%% margin.'
                ),
                '<tt>len:"=19.8"</tt>'
            ) . '
            </li>
            </ul>
            <h2>' . _('Additional search tags') . '</h2>
            <p>
            ' . _(
                'In addition to the search tags mentioned above, there are some less commonly-used tags available.'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'The %s tag allows you to search by world area. Valid values for this tag are %s.'
                ),
                '<tt>area</tt>',
                $this->areaSearchValues()
            ) . '
            </p>
             <p>
            ' . sprintf(
                _(
                    'The %s tag allows you to search for recordings that have been uploaded since a certain date. Using a simple integer value such as %s will find all recordings uploaded in the past 3 days. If you use a date with a format of YYYY-MM-DD, it will find all recordings uploaded since that date (e.g. %s). Note that this search considers the <em>upload</em> date, not the date that the recording was made.'
                ),
                '<tt>since</tt>',
                '<tt>since:3</tt>',
                '<tt>since:2012-11-09</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'The %s and %s tags allow you to search for recordings that were recorded on a certain date. The following query will find all recordings that were recorded in May of 2010: %s. Similarly, %s will find recordings that were recorded during the month of June in any year. Both tags also accept \'>\' (after) and \'<\' (before).'
                ),
                '<tt>year</tt>',
                '<tt>month</tt>',
                '<tt>year:2010 month:5</tt>',
                '<tt>month:6</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'The %s and %s tags for collection date operate similarly, but these apply only to specific groups that are allowed to be recorded in a studio setting (currently grasshoppers only).'
                ),
                '<tt>colyear</tt>',
                '<tt>colmonth</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'The %s tag for temperature currently also applies only to grasshoppers. This field also accepts \'&lt;\' and \'&gt;\' operators. Use %s to search for sounds recorded between 25-26 &deg;C or %s for temperatures over 20 &deg;C.'
                ),
                '<tt>temp</tt>',
                '<tt>temp:25</tt>',
                '<tt>temp:"&gt;20"</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'The %s tag can be used to search for animals that were sound recorded before ending up in a (museum) collection. This tag also accepts a \'matches\' operator.'
                ),
                '<tt>regnr</tt>',
                '<tt>tmp:25</tt>',
                '<tt>tmp:"&gt;20"</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'The %s tag searches for automatic (non-supervised) recordings. This tag accepts %s and %s.'
                ),
                '<tt>auto</tt>',
                '<tt>yes</tt>',
                '<tt>no</tt>'
            ) . '
            </p>
            <p>
            ' . sprintf(
                _(
                    'Use the %s (device) and %s (microphone) tags to search for specific recording equipment.'
                ),
                '<tt>dvc</tt>',
                '<tt>mic</tt>'
            ) . '
            </p>

            <p>
            ' . sprintf(
                _(
                    'The %s tag can be used to search for recordings with a specific sampling rate (in Hz). For example, %s will return hi-res recordings. Other frequencies include 22050, 44100 and multiples of 48000.'
                ),
                '<tt>smp</tt>',
                '<tt>smp:"&gt;48000"</tt>'
            ) . '
            </p>';

        return $this->template->render($output, ['title' => $title, 'bodyId' => 'tips']);
    }

    private function tagSearchValues($tag)
    {
        // Create a map of categories, plus property names and ids
        if (empty($this->tagProperties)) {
            $res = query_db(
                "select t1.id, t2.tag, t1.property 
                from sound_properties as t1
                left join sound_property_categories as t2 on t1.category_id = t2.id
                where t1.property not in ('uncertain', 'unknown')
                order by t1.property"
            );
            while ($row = $res->fetch_object()) {
                $this->tagProperties[$row->tag][] = "<tt>$row->property</tt>";
            }
        }
        return implode(', ', $this->tagProperties[$tag] ?? []);
    }

    private function areaSearchValues()
    {
        $values = [];
        foreach (WorldArea::all() as $area) {
            $values[] = "<tt>$area->branch</tt>";
        }
        return implode(', ', $values);
    }
}
