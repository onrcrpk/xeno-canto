<?php

namespace xc\Controllers;

use function xc\escape;
use function xc\getUrl;
use function xc\XC_formatUserText;

class Members extends Controller
{

    public function handleRequest()
    {
        $title  = _('Meet the Members');
        $output = "<h1>$title</h1>
            <p>" . _('Short bios of our community members.') . '</p>
            <p> ' .
                  _(
                      'Are you a member, but not shown in the list below? Then add a description and image of yourself on your account page.'
                  ) . '
            </p>';


        $q = $this->request->query->get('q', 'A');

        $lettersSql = "SELECT distinct UPPER(LEFT(username,1)) as letter FROM users
            where username != '' and (blurb != '') order by letter asc;";

        $res         = query_db($lettersSql);
        $letterLinks = '';
        if ($res && $res->num_rows > 0) {
            $letterLinks .= "<nav class='results-pages'>
                <ul>";
            while ($row = $res->fetch_object()) {
                if (strtoupper($row->letter) == strtoupper($q)) {
                    $letterLinks .= "<li class='selected'><span>$row->letter</span></li>";
                } else {
                    $letterLinks .= "<li><a href='" . getUrl(
                        'meetmembers',
                        ['q' => $row->letter]
                    ) . "'>$row->letter</a></li>";
                }
            }

            if (strtoupper($q) == 'ALL') {
                $letterLinks .= "<li class='selected'><span>All</span></li>";
            } else {
                $letterLinks .= "<li><a href='" . getUrl(
                    'meetmembers',
                    ['q' => 'all']
                ) . "'>All</a></li>";
            }

            $letterLinks .= '</ul>
                </nav>';
        }

        $output .= $letterLinks;

        $whereConstraint = '';
        if ($q != 'all') {
            $escaped_letter  = escape($q);
            $whereConstraint = "AND LEFT(username, 1)='$escaped_letter'";
        }

        $sql = "select distinct username, dir, blurb from users
            where (users.blurb != '' $whereConstraint)
            order by username
            ";

        $resusr = query_db($sql);

        $output .= "<table class='results'>
            <thead>
            <tr>
            <th>" . _('Name') . '</th>
            <th>' . _('About') . '</th>
            </tr>
            </thead>
            <tbody>';


        while ($row = $resusr->fetch_object()) {
            if (
                file_exists(
                    "{$_SERVER['DOCUMENT_ROOT']}/graphics/memberpics/{$row->dir}_tb.png"
                )
            ) {
                $img = "<a href='graphics/memberpics/$row->dir.png' class='fancybox'>
                    <img class='avatar' src='graphics/memberpics/$row->dir" . "_tb.png'></a>";
            } else {
                $img = '';
            }
            $output .= "
                <tr>
                <td><a href='" . getUrl('recordist', ['id' => $row->dir]) . "'>$row->username</a>
                <div>$img</div></td>";
            if ($row->blurb) {
                $output .= '<td>' .
                           XC_formatUserText($row->blurb) . '
                    </td></tr>
                    ';
            } else {
                $output .= '<td></td></tr>
                    ';
            }
        }

        $output .= '</tbody>
            </table>';

        $output .= $letterLinks;

        return $this->template->render(
            $output,
            ['title' => $title, 'bodyId' => 'member-list']
        );
    }
}
