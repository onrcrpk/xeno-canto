<?php

namespace xc\Controllers;

use xc\User;

use function xc\getUrl;

class ArticlesIndex extends Controller
{

    public function handleRequest()
    {
        $pageTitle = _('Articles');
        // redirect old links to new urls
        $action = $this->request->query->get('action');
        $blognr = $this->request->query->get('blognr');
        if ($action && $blognr) {
            $url = '';
            switch ($action) {
                case 'view':
                    $url = getUrl('feature-view', ['blognr' => $blognr]);
                    break;
                case 'edit':
                    $url = getUrl('feature-edit', ['blognr' => $blognr]);
                    break;
                case 'preview':
                    $url = getUrl('feature-preview', ['blognr' => $blognr]);
                    break;
            }

            if ($url) {
                return $this->movedPermanently($url);
            }
        }

        $sql = "select * from blogs where blognr != '5' order by published desc";
        $res = query_db($sql);

        $opennewblog = _('Would you like to write an article on animal song?') . ' ';
        if (User::current()) {
            $opennewblog .= sprintf(
                _("Then <a href='%s'>create one</a>!"),
                getUrl('feature-new')
            );
        } else {
            $opennewblog .= sprintf(
                _(
                    "Then first <a href='%s'>register</a> as a member,
                or, if you already are, <a href='%s'>log in</a>."
                ),
                getUrl('register'),
                getUrl('login')
            );
        }

        $blogs = "
            <h1>$pageTitle</h1>
            <div class='column'>
            <p>" . _(
                'Read articles on animal song, written by xeno-canto members.
            These articles may be on any topic, such as identification issues, taxonomic
            questions, discussing patterns of biogeographic variation, and so on.'
            ) . "</p>

            <p>$opennewblog</p>

            <h2>" . _('Current Articles') . "</h2>
            <ul class='simple'>
            ";
        while ($row = $res->fetch_object()) {
            $user = User::current();
            if ($user && ($row->dir == $user->userId() || $user->isAdmin())) {
                $editLink = "(<a href='" . getUrl('feature-edit', ['blognr' => $row->blognr]) . "'>" . _(
                        'edit'
                    ) . '</a>)';
            } else {
                $editLink = '';
            }
            $title = $row->title;
            if (empty($title)) {
                $title = _('[Untitled]');
            }

            $author = $row->author;

            $draftClass = '';
            if (!$row->public) {
                $draftClass = 'draft';
            }

            if ($row->public || ($user && ($user->isAdmin() || ($row->dir == User::current()->userId())))) {
                $blogs .= "
                    <li class='$draftClass'>" . sprintf(
                        _('%s by %s'),
                        "<a href='" . getUrl('feature-view', ['blognr' => $row->blognr]) . "'><b>$title</b></a>",
                        "$author"
                    ) . " $editLink</li>";
            }
        }
        $blogs .= '
            </ul>
            </div>';

        $blogs .= "
            <div class='column'>
            <section class='tip'>
            <p>" . sprintf(
                _("Read the <a href='%s'>tutorial</a> for an overview of all the possibilities."),
                getUrl('feature-view', ['blognr' => 5])
            ) . '</p>
            </section>
            </div>';

        return $this->template->render($blogs, ['title' => $pageTitle, 'bodyId' => 'features']);
    }

}
