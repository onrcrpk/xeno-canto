<?php

namespace xc\Controllers;

use function xc\getUrl;

class AboutApi extends Controller
{

    public function handleRequest()
    {
        $apiUrl = getUrl('api-recording-search');
        $tipsUrl = getUrl('tips');

        $output = "
            <h1>Application Programming Interface (API v2)</h1> 
            
            <p>In the original version of xeno-canto, a very simple web service was provided. 
            In 2013, a more full-featured, RESTful JSON-based API was introduced. The xeno-canto API v2 is stable:
            we won't rename or remove any resources or response properties to ensure backwards compatibility, 
            but we might add new resources to the API.</p>    
            
            <p class='important' style='margin: 15px 0;'>When other groups besides birds were welcomed to XC in 2022, some of the data previously stored under
            sound <span class='code strong'>type</span> were restructured into dedicated fields: <span class='code strong'>sex</span>, (life)
            <span class='code strong'>stage</span> and (recording) <span class='code strong'>method</span>. We consider this a non-breaking
            change that does not warrant a new version of the API, but you may need to modify your application.</p>       
            
            <p>This API can be used without restrictions. However, intensive use would occasionally degrade general web site performance. 
            We have now implemented a rate limit of <b>1 request per second</b>. You need to take this into consideration
            and possibly adapt your application to accommodate this change.</p> 
            
           <h1 style='margin-top: 30px;'>Request</h1>
            <p>The endpoint for the webservice is at <a href='$apiUrl'>https://xeno-canto.org/api/2/recordings</a>.</p>
            
            <h2>Query Parameters</h2>
            <p>There are two
            possible query parameters for this API.</p>
            <p>The <span class='code strong'>query</span> parameter is <em>required</em> and must be non-empty.  This parameter must
            be a search string in the format described on the <a href='$tipsUrl'>search
            tips</a> page.  Some examples are shown below:</p>
            
            <ul>
            <li><a href='$apiUrl?query=cnt:brazil'>https://xeno-canto.org/api/2/recordings?query=cnt:brazil</a></li>
            <li><a href='$apiUrl?query=troglodytes+troglodytes'>https://xeno-canto.org/api/2/recordings?query=troglodytes+troglodytes</a></li>
            <li><a href='$apiUrl?query=bearded+bellbird+q:A'>https://xeno-canto.org/api/2/recordings?query=bearded+bellbird+q:A</a></li>
            </ul>
            
            <p>The <span class='code strong'>page</span>
            parameter is <em>optional</em> and is only needed if the results from a
            given search don't fit in a single page. If specified, <span class='code'>page</span> must be
            an integer between 1 and <span class='code'>results.numPages</span> (see results format below
            for details). For example, to retrieve the fifth page of Brazilian
            recordings, use the following URL:</p>
            
            <p><a href='$apiUrl?query=cnt:brazil&page=5'>https://xeno-canto.org/api/2/recordings?query=cnt:brazil&page=5</a></p>
            
            <h1 style='margin-top: 30px;'>Response</h1>
            <p>The response is
            returned with a payload in <a href='http://en.wikipedia.org/wiki/JSON'>JSON</a>
            format.  The details are shown below.</p>
           
            <h2>Success</h2>
            <p>For a successful
            query, a HTTP 200 response code will be returned, with a payload in
            the following format:</p>
            <pre>
            <code> 
            {
                \"numRecordings\": \"1\",
                \"numSpecies\": \"1\",
                \"page\": 1,
                \"numPages\": 1,
                \"recordings\": [
                    ...,
                    Array of Recording objects (see below),
                    ...
                ]
            }
            </code>
            </pre>            
            <p>This JSON object
            will contain details about the recordings found with the given query.
             For performance reasons, the number of recordings returned for a
            given query will be limited.  If the limit is lower than the total
            number of results, they will be split into 'pages'.  To fetch the
            next page of results, you must append the <span class='code'>page</span> query parameter as
            specified above.</p>
            <p>A brief description of the fields:</p>
            <ul>
                <li><span class='code strong'>numRecording</span>: the total number of recordings found for this query</li>
                <li><span class='code strong'>numSpecies</span>: the total number of species found for this query</li>
                <li><span class='code strong'>page</span>: the page number of the results page that is being displayed</li>
                <li><span class='code strong'>numPages</span>: the total number of pages available for this query</li>
                <li><span class='code strong'>recordings</span>: an array of recording objects, described in detail below</li>
            </ul>
           
            <h2>Recording Object</h2>
            <p>Each object in the
            <span class='code'>recordings</span> array mentioned above has the following format:</p>
            <pre>
            <code>" . '
            {
                "id": "694038",
                "gen": "Troglodytes",
                "sp": "troglodytes",
                "ssp": "",
                "group": "birds",
                "en": "Eurasian Wren",
                "rec": "Jacobo Ramil MIllarengo",
                "cnt": "Spain",
                "loc": "Sisalde, Ames, A Coru\u00f1a, Galicia",
                "lat": "42.8373",
                "lng": "-8.652",
                "alt": "30",
                "type": "song",
                "sex": "male",
                "stage": "adult",
                "method": "field recording",
                "url": "\/\/xeno-canto.org\/694038",
                "file": "\/\/xeno-canto.org\/694038\/download",
                "file-name": "XC694038-211223_02Carrizo variaci\u00f3ns dunha frase bastante stereotipada siteD 9.30 Sisalde.mp3",
                "sono": {
                    "small": "\/\/xeno-canto.org\/sounds\/uploaded\/LHCOINSOBZ\/ffts\/XC694038-small.png",
                    "med": "\/\/xeno-canto.org\/sounds\/uploaded\/LHCOINSOBZ\/ffts\/XC694038-med.png",
                    "large": "\/\/xeno-canto.org\/sounds\/uploaded\/LHCOINSOBZ\/ffts\/XC694038-large.png",
                    "full": "\/\/xeno-canto.org\/sounds\/uploaded\/LHCOINSOBZ\/ffts\/XC694038-full.png"
                },
                "osci": {
                    "small": "\/\/xeno-canto.org\/sounds\/uploaded\/LHCOINSOBZ\/wave\/XC694038-small.png",
                    "med": "\/\/xeno-canto.org\/sounds\/uploaded\/LHCOINSOBZ\/wave\/XC694038-med.png",
                    "large": "\/\/xeno-canto.org\/sounds\/uploaded\/LHCOINSOBZ\/wave\/XC694038-large.png"
                },
                "lic": "\/\/creativecommons.org\/licenses\/by-nc-sa\/4.0\/",
                "q": "A",
                "length": "4:08",
                "time": "09:30",
                "date": "2021-12-23",
                "uploaded": "2021-12-27",
                "also": [
                    "Turdus viscivorus",
                    "Turdus iliacus",
                    "Parus major"
                ],
                "rmk": "A male repeat a stereotyped phrase with few syllabic variation. HPF 270 Hz. It\u00b4s raining.",
                "bird-seen": "yes",
                "animal-seen": "yes",
                "playback-used": "no",
                "temp": "",
                "regnr": "",
                "auto": "unknown",
                "dvc": "",
                "mic": "",
                "smp": "44100"
            }
            ' . "</code>
            </pre>
            <p>The following is a detailed description of the fields of this object:</p>
            <ul>
                <li><span class='code strong'>id</span>: the catalogue number of the recording on xeno-canto</li>
                <li><span class='code strong'>gen</span>: the generic name of the species</li>
                <li><span class='code strong'>sp</span>: the specific name (epithet) of the species</li>
                <li><span class='code strong'>ssp</span>: the subspecies name (subspecific epithet)</li>
                <li><span class='code strong'>group</span>: the group to which the species belongs (birds, grasshoppers, bats)</li>
                <li><span class='code strong'>en</span>: the English name of the species</li>
                <li><span class='code strong'>rec</span>: the name of the recordist</li>
                <li><span class='code strong'>cnt</span>: the country where the recording was made</li>
                <li><span class='code strong'>loc</span>: the name of the locality</li>
                <li><span class='code strong'>lat</span>: the latitude of the recording in decimal coordinates</li>
                <li><span class='code strong'>lng</span>: the longitude of the recording in decimal coordinates</li>
                <li><span class='code strong'>type</span>: the sound type of the recording (combining both predefined 
                terms such as 'call' or 'song' and additional free text options)</li>
                <li><span class='code strong'>sex</span>: the sex of the animal</li>
                <li><span class='code strong'>stage</span>: the life stage of the animal (adult, juvenile, etc.)</li>
                <li><span class='code strong'>method</span>: the recording method (field recording, in the hand, etc.)</li>
                <li><span class='code strong'>url</span>: the URL specifying the details of this recording</li>
                <li><span class='code strong'>file</span>: the URL to the audio file</li>
                <li><span class='code strong'>file-name</span>: the original file name of the audio file</li>
                <li><span class='code strong'>sono</span>: an object with the urls to the four versions of sonograms</li>
                <li><span class='code strong'>osci</span>: an object with the urls to the three versions of oscillograms</li>
                <li><span class='code strong'>lic</span>: the URL describing the license of this recording</li>
                <li><span class='code strong'>q</span>: the current quality rating for the recording</li>
                <li><span class='code strong'>length</span>: the length of the recording in minutes</li>
                <li><span class='code strong'>time</span>: the time of day that the recording was made</li>
                <li><span class='code strong'>date</span>: the date that the recording was made</li>
                <li><span class='code strong'>uploaded</span>: the date that the recording was uploaded to xeno-canto</li>
                <li><span class='code strong'>also</span>: an array with the identified background species in the recording</li>
                <li><span class='code strong'>rmk</span>: additional remarks by the recordist</li>
                <li><span class='code strong'>bird-seen</span>: despite the field name (which was kept to ensure backwards compatibility), this field indicates whether the recorded <b>animal</b> was seen</li>
                <li><span class='code strong'>animal-seen</span>: was the recorded animal seen?</li>
                <li><span class='code strong'>playback-used</span>: was playback used to lure the animal?</li> 
                <li><span class='code strong'>temperature</span>: temperature during recording (applicable to specific groups only)</li>
                <li><span class='code strong'>regnr</span>: registration number of specimen (when collected)</li>
                <li><span class='code strong'>auto</span>: automatic (non-supervised) recording?</li>
                <li><span class='code strong'>dvc</span>: recording device used</li>
                <li><span class='code strong'>mic</span>: microphone used</li>
                <li><span class='code strong'>smp</span>: sample rate</li>
            </ul>
            <h2>Error</h2>
            <p>In case of an error,
            a HTTP 400 or 500 response code will be returned (depending on
            whether the error was a client or server error).  The payload
            returned will be of the following form:</p>
            <pre>
            <code>
            {
                \"error\": {
                    \"code\":\"missing_parameter\",
                    \"message\":\"No query specified\"
                }
            }            
            </code>
            </pre>
            <p>The <span class='code'>code</span> parameter
            will be an identifier for the error, and <span class='code'>message</span> will offer some
            additional explanatory details.</p>
            
            <h1 style='margin-top: 30px;'>Changes</h1>
            <ul>
                <li><span class='code strong'>q</span> parameter added December 19, 2014</li>
                <li><span class='code strong'>time</span> and <span class='code strong'>date</span> parameter added October 2, 2015</li>
                <li>updated endpoints to https April 16, 2019</li> 
                <li>
                <span class='code strong'>url</span>, 
                <span class='code strong'>length</span>, 
                <span class='code strong'>file-name</span>, 
                <span class='code strong'>sono</span>, 
                <span class='code strong'>uploaded</span>, 
                <span class='code strong'>also</span>, 
                <span class='code strong'>rmk</span>, 
                <span class='code strong'>bird-seen</span>, 
                and <span class='code strong'>playback-used</span> parameters added November 15, 2019</li>
                <li>rate limiting added November 14, 2021</li>
                <li>many new fields added in September 2022, basically all fields that can be used to search.
                Some of the data previously stored under sound types were restructured into dedicated fields: 
                <span class='code strong'>sex</span>, (life) <span class='code strong'>stage</span> and 
                (recording) <span class='code strong'>method</span>. The field <span class='code strong'>animal-seen</span> 
                replaces the field <span class='code strong'>bird-seen</span>. The latter, however, 
                is still present to fully ensure backward compatibility.</li>
            </ul>";

        return $this->template->render($output, ['title' => _('API'), 'bodyId' => 'about-api']);
    }
}
