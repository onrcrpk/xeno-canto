<?php

namespace xc\Controllers;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use xc\Recording;
use xc\User;

use function xc\escape;
use function xc\getUrl;

define('MAX_DOWNLOADS_PER_DAY', 1000);

class RecordingDownload extends Controller
{

    public function handleRequest($xcid)
    {
        $rec = Recording::load($xcid);
        if (!$rec) {
            return $this->notFound();
        }

        /* DISABLE ABUSE CHECK FOR NOW
         *
         $ip = $_SERVER['REMOTE_ADDR'];
        $res = query_db("SELECT count(*) as nplays from birdsounds_play_stats WHERE ip='$ip' AND date > DATE_SUB(NOW(), INTERVAL 1 DAY)");
        if ($res)
            $row = $res->fetch_object();
        if ($row)
            $count = $row->nplays;

        if ($count && $count > MAX_DOWNLOADS_PER_DAY)
            errorTooManyDownloads();
         */

        $content = "<h1>Download disabled for this species</h1>
            <p>We apologize for the inconvenience, but recordings of this
            species are currently restricted due to conservation concerns.
            If you would like access to this recording, you may contact the 
            recordist directly.
            <a href='" . getUrl('FAQ') . "#restricted'>" . _(
                'Explain this.'
            ) . '</a></p>';
        if ($rec->restrictedForUser(User::current())) {
            return $this->template->render($content);
        }

        $ip = escape($this->request->getClientIp());
        query_db(
            "INSERT INTO birdsounds_play_stats (snd_nr, date, ip) VALUES ($xcid, NOW(), '$ip')"
        );

        if (file_exists($rec->filePath())) {
            $response = new BinaryFileResponse($rec->filePath());
            $response->headers->set('Content-Type', $rec->mimeType());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $rec->suggestedFileName());
            return $response;
        }

        return $this->notFound();
    }

    private function errorTooManyDownloads()
    {
        header('HTTP/1.1 503 Service Temporarily Unavailable');
        header('Status: 503 Service Temporarily Unavailable');
        header('Retry-After: 86400');// 1 day in seconds
        $pageTitle = _('File Not Found');
        echo "<h1>$pageTitle</h1>";
        echo "<div class='error'>
            <p>" . _(
                'In order to prevent excessive load on our server, we limit the number of downloads you can make per day.'
            ) . '</p>
            <p>' . _(
                "If you feel that you have received this message in error, or if you would like to to arrange a method of downloading a large number of files that does not put an undue burden on the server, please <a href='mailto:contact_at_xeno-canto.org'>contact us by email</a>."
            ) . '</p>
            <p>' . _(
                'Otherwise, simply try again tomorrow and you will be able to download again.'
            ) . '</p>
            </div>
            ';
        exit();
    }
}
