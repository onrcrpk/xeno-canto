<?php

namespace xc\Controllers;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use xc\BaseTemplate;

class ControllerBase
{
    protected $request;
    
    protected $template;

    public function __construct($request)
    {
        $this->request = $request;
        $this->template = new BaseTemplate($this->request);
    }

    // helper function to redirect to a new url
    protected function seeOther($url)
    {
        return new RedirectResponse($url, Response::HTTP_SEE_OTHER);
    }

    // helper function to redirect to a new permanently location
    protected function movedPermanently($url)
    {
        return new RedirectResponse($url, Response::HTTP_MOVED_PERMANENTLY);
    }

    // helper function to return 404 error
    protected function notFound($content = null)
    {
        if (!$content) {
            $content = '<h1>' . _('Not Found') . '</h1>
                <p>' . sprintf(
                    _(
                        "The requested page %s cannot be found.  We're sorry for the inconvenience."
                    ),
                    "<b><tt>{$this->request->getRequestUri()}</tt></b>"
                ) . '</p>';
        }

        // create a new base template for displaying the error message so that
        // we don't display the admin page template while visiting an admin url
        // while not logged in as an admin
        $template = new BaseTemplate($this->request);
        return new Response($template->render($content), Response::HTTP_NOT_FOUND);
    }

    // helper function to return 400 error
    protected function badRequest($content = null)
    {
        if (!$content) {
            $content = "
                <h1>Bad Request</h1>
                <p>You made a bad request. Don't do that.</p>";
        }
        return new Response($this->template->render($content), Response::HTTP_BAD_REQUEST);
    }

    // helper function to return 401 error
    protected function unauthorized($content = null)
    {
        if (!$content) {
            $content = '
                <h1>Unauthorized</h1>
                <p>Sorry, you are not authorized to visit this page.</p>';
        }
        return new Response($this->template->render($content), Response::HTTP_UNAUTHORIZED);
    }

    // helper function to return 500 error
    protected function internalServerError($content = null)
    {
        if (!$content) {
            $content = '
                <h1>Internal Server Error</h1>
                <p>Sorry, something went wrong and we were unable to complete your request.</p>';
        }
        return new Response($this->template->render($content), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
