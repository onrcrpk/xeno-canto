<?php

namespace xc\Controllers;

use function xc\getUrl;

class ArticlePreview extends Controller
{

    public function handleRequest($blognr)
    {
        $sql = "select * from blogs where blognr=$blognr";
        $res = query_db($sql);
        $row = $res->fetch_object();

        if (!$row) {
            return $this->notFound();
        }

        $title = _('Article Preview');
        $output = "
            <h1>$title</h1>
            <p>
            <a href='" . getUrl(
                'feature-view',
                ['blognr' => $blognr]
            ) . "'>" . _(
                      'Return to this article'
                  ) . "</a>
            </p>

            <section class='column forum-thread'>
            <article class='preview'>
            <h2>$row->title</h2>
            ";
        $output .= htmlentities($row->blog);
        $output .= '
            </article>
            </section>';

        return $this->template->render($output, ['title' => $title]);
    }

}
