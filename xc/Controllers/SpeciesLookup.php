<?php

namespace xc\Controllers;

use xc\Species;

use function xc\getUrl;

class SpeciesLookup extends Controller
{

    public function handleRequest()
    {
        $q = strip_tags($this->request->query->get('query'));

        if (!empty($q)) {
            $spnr = Species::getSpeciesNumberForString($q);
            if ($spnr) {
                $sp = Species::load($spnr);
                if ($sp) {
                    return $this->seeOther($sp->profileURL());
                }
            }
            return $this->seeOther(getUrl('browse', ['query' => $q]));
        }

        return $this->seeOther(getUrl('index'));
    }
}
