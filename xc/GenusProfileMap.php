<?php

namespace xc;

class GenusProfileMap extends RecordingMap
{

    private $m_unspecMarkerSpec;

    private $m_bgMarkerSpec;


    // override the legend function

    private $m_spMarkerSpec;

    private $i;

    public function __construct($mapCanvasId, $genus, $includeBg)
    {
        // ignore $includeBg for now...
        $this->i = 0;
        $query   = new Query("gen:$genus");
        $query->setOrder('taxonomy');
        parent::__construct($mapCanvasId, $query->execute());
        $this->m_spMarkerSpec = [];
    }

    public function getLegendVfunc()
    {
        $html = "
            <div class=\"map-legend\">";

        if ($this->m_spMarkerSpec) {
            $html .= '
                <ul>';

            foreach ($this->m_spMarkerSpec as $spec) {
                $html .= "<li><span class=\"marker-image\"><img class=\"icon\" src=\"{$spec->url}\" /></span> <span class=\"marker-text\">{$spec->legendText}</span></li>";
            }
            $html .= '</ul>';
        }

        $html .= '
            </div>';

        return $html;
    }

    protected function createMarkerForRecording($rec)
    {
        $marker = new Marker();
        $spec   = null;
        $sp     = $rec->speciesName();
        $spec   = new MarkerSpec();

        $marker->restricted = $rec->restrictedForUser() ? 1 : 0;

        if (array_key_exists($sp, $this->m_spMarkerSpec)) {
            $spec = $this->m_spMarkerSpec[$sp];
        } elseif (!$marker->isRestricted()) {
            $spec->url = Marker::$icons[($this->i % count(Marker::$icons))];
            $this->i++;
            $spec->legendText = "<a href=\"{$rec->speciesURL()}\">{$rec->commonName()}</a> <span class='sci-name'>{$rec->scientificName()}</span>";

            $this->m_spMarkerSpec[$sp] = $spec;
        }

        $spec->height  = 14;
        $spec->width   = 14;
        $spec->anchorX = 7;
        $spec->anchorY = 7;

        $marker->spec    = $spec;
        $marker->zIndex  = 50;
        $marker->animate = true;

        $marker->latitude  = $rec->latitude();
        $marker->longitude = $rec->longitude();
        $marker->id        = $rec->xcid();
        $marker->title     = "XC{$rec->xcid()}: {$rec->commonName()}";

        return $marker;
    }
}
