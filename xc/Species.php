<?php

namespace xc;

class Species
{

    public static $restrictedSpecies;

    public static $pseudospecies;

    public $restricted;

    public $extinct;

    private $commonName;

    private $genus;

    private $species;

    private $family;

    private $speciesNumber;

    private $subspecies;

    private $order_nr;

    private $familyDesc;

    private $order;

    private $authority;

    private $groupId;

    public function __construct(
        $genus,
        $species,
        $commonName,
        $speciesNumber,
        $family = '',
        $subspecies = '',
        $order_nr = 0,
        $familyDesc = null,
        $order = null,
        $extinct = 0,
        $groupId = null
    ) {
        $this->genus = $genus;
        $this->species = $species;
        $this->commonName = $commonName;
        $this->speciesNumber = $speciesNumber;
        $this->family = $family;
        $this->subspecies = $subspecies;
        $this->order_nr = $order_nr;
        $this->familyDesc = $familyDesc;
        $this->order = $order;
        $this->extinct = $extinct;
        $this->authority = null;
        $this->restricted = $this->restricted();
        $this->groupId = $groupId;
    }

    public function restricted()
    {
        return in_array($this->speciesNumber, Species::restrictedSpecies());
    }

    public static function restrictedSpecies()
    {
        if (!Species::$restrictedSpecies) {
            Species::$restrictedSpecies = [];
            $res = query_db('SELECT species_nr FROM taxonomy WHERE restricted');
            while ($row = $res->fetch_object()) {
                Species::$restrictedSpecies[] = $row->species_nr;
            }
        }
        return Species::$restrictedSpecies;
    }

    public static function load($species_nr, $localized = true)
    {
        $localName = 'english';
        if ($localized) {
            $localName = localNameLanguage();
        }

        $nr = escape($species_nr);
        $sql = "
            SELECT tax.genus, tax.species, tax.eng_name, tax.family, IOC_order_nr as order_nr,
                tax.family_english, tax.order, cnames.$localName as localName, extinct, authority, 
                tax.group_id
            FROM taxonomy tax
            LEFT JOIN taxonomy_multilingual cnames USING (species_nr)
            WHERE species_nr = '$nr'";
        $res = query_db($sql);

        if (!$res) {
            XC_logger()->logError("Error loading species:" . mysqli()->error);
            return null;
        }

        $row = $res->fetch_object();

        // Try fetching data from birdsounds as a poor-man's backup option;
        // used for orphaned/undescribed species that may need to be renamed through recording sets
        if (!$row) {
            $sql = "
                SELECT genus, species, eng_name, family, order_nr, group_id
                FROM birdsounds
                WHERE species_nr = '$nr'
                ORDER BY snd_nr DESC
                LIMIT 1";
            $res = query_db($sql);
            $row = $res->fetch_object();

            if ($row) {
                $sql = "
                    SELECT family_english, `order`
                    FROM taxonomy
                    WHERE family = '$row->family'
                    LIMIT 1";
                $res = query_db($sql);
                $row2 = $res->fetch_object();

                $familyEnglish = $row2->family_english ?? '';
                $order = $row2->order ?? '';

                return new Species(
                    $row->genus,
                    $row->species,
                    $row->eng_name,
                    $nr,
                    $row->family,
                    '',
                    $row->order_nr,
                    $familyEnglish,
                    $order,
                    0,
                    $row->group_id
                );
            }

            return null;
        }

        $localName = $row->localName;
        if (!$localName) {
            $localName = $row->eng_name;
        }
        $sp = new Species(
            $row->genus,
            $row->species,
            $localName,
            $nr,
            $row->family,
            '',
            $row->order_nr,
            $row->family_english,
            $row->order,
            $row->extinct,
            $row->group_id
        );
        $sp->setAuthority($row->authority);

        return $sp;
    }

    // taxonomic order, not sort order

    public function setAuthority($authority)
    {
        $this->authority = $authority;
    }

    public static function excludePseudoSpeciesSQL($tablename = '')
    {
        return Species::excludeSpeciesSQL($tablename, Species::$pseudospecies);
    }

    private static function excludeSpeciesSQL($tablename, $speciesArray)
    {
        $fieldPrefix = '';
        if ($tablename) {
            $fieldPrefix = $tablename . '.';
        }
        $conds = [];
        foreach ($speciesArray as $sp) {
            $conds[] = "{$fieldPrefix}species_nr!='$sp'";
        }
        return implode(' AND ', $conds);
    }

    public static function excludeMysterySpeciesSQL($tablename = '')
    {
        return Species::excludeSpeciesSQL($tablename, [self::mysterySpeciesNumber()]);
    }

    public static function mysterySpeciesNumber()
    {
        return 'mysmys';
    }

    public static function getSpeciesNumberForString($speciesText)
    {
        if (empty($speciesText)) {
            return '';
        }

        $parts = explode(' ', $speciesText);
        $species_nr = '';
        if (count($parts) == 2) {
            $genus = escape($parts[0]);
            $species = escape($parts[1]);
            $res = query_db(
                "SELECT species_nr from taxonomy WHERE genus='$genus' AND species='$species'"
            );
            if ($res) {
                $row = $res->fetch_object();
                if ($row) {
                    $species_nr = $row->species_nr;
                }
            }
        }

        if (empty($species_nr)) {
            $text = escape($speciesText);
            $res = query_db(
                "SELECT species_nr from taxonomy WHERE eng_name='$text'"
            );
            if ($res) {
                $row = $res->fetch_object();
                if ($row) {
                    $species_nr = $row->species_nr;
                }
            }
        }

        return $species_nr;
    }

    public function subspeciesURL()
    {
        return $this->subspecies() ?
            $this->profileURL() . '?query=ssp:"' . urlencode($this->subspecies()) . '"' :
            $this->profileURL();
    }

    public function subspecies()
    {
        return $this->subspecies;
    }

    public function profileURL()
    {
        return XC_speciesURL($this->genus(), $this->species());
    }

    public function genus()
    {
        return $this->genus;
    }

    public function species()
    {
        return $this->species;
    }

    public function genusURL()
    {
        return XC_genusURL($this->genus());
    }

    public function rangeMapURL()
    {
        return "https://{$_SERVER['HTTP_HOST']}/ranges/{$this->genus()}_{$this->species()}.kmz?v=" . IMAGE_VERSION;
    }

    public function rangeMapPath()
    {
        return realpath(dirname(__FILE__, 2)) . "/ranges/{$this->genus()}_{$this->species()}.kmz";
    }

    public function speciesNumber()
    {
        return $this->speciesNumber;
    }

    public function family()
    {
        return $this->family;
    }

    public function order_nr()
    {
        return $this->order_nr;
    }

    public function familyDescription()
    {
        return $this->familyDesc;
    }

    public function order()
    {
        return $this->order;
    }

    public function isMystery()
    {
        return ($this->speciesNumber == self::mysterySpeciesNumber());
    }

    public function isSoundscape()
    {
        return $this->speciesNumber === self::soundscapeSpeciesNumber();
    }

    public static function soundscapeSpeciesNumber()
    {
        return 'envrec';
    }

    public function isExtinct()
    {
        return !!$this->extinct;
    }

    public function restrictedForUser($user = null)
    {
        if (empty($user)) {
            $user = User::current();
        }
        return !(
            !$this->restricted() ||
            $user && ($user->canDownloadRestrictedRecordings() || $user->isAdmin())
        );
    }

    public function displayName()
    {
        if ($this->isPseudoSpecies()) {
            return $this->commonName();
        }
        return $this->commonName() . ' (' . $this->fullScientificName() . ')';
    }

    public function isPseudoSpecies()
    {
        return in_array($this->speciesNumber, Species::pseudoSpecies(), true);
    }

    public static function pseudoSpecies()
    {
        return Species::$pseudospecies;
    }

    public function commonName()
    {
        return $this->commonName;
    }

    public function fullScientificName()
    {
        $n = $this->scientificName();
        if ($this->subspecies()) {
            $n .= ' ' . $this->subspecies();
        }
        return $n;
    }

    public function scientificName()
    {
        return "{$this->genus()} {$this->species()}";
    }

    public function groupId()
    {
        return $this->groupId;
    }

    public function htmlDisplayName($link = true)
    {
        if ($this->isPseudoSpecies()) {
            return $this->commonName() ?? $this->fullScientificName();
        }
        if ($this->commonName()) {
            $html = "<span class='common-name'>";
            if ($link) {
                $html .= "<a href='{$this->profileURL()}'>{$this->commonName()}</a>";
            } else {
                $html .= $this->commonName();
            }
            $html .= "</span> (<span class='sci-name'>{$this->fullScientificName()}</span>)";
        } else {
            $html = "<span class='common-name'>";
            if ($link) {
                $html .= "<a href='{$this->profileURL()}'>{$this->fullScientificName()}</a>";
            } else {
                $html .= $this->fullScientificName();
            }
            $html .= "</span>";
        }
        return $html;
    }

    public function authority()
    {
        return $this->authority;
    }

}

Species::$pseudospecies = [
    Species::mysterySpeciesNumber(),
    Species::soundscapeSpeciesNumber(),
];

function XC_genusURL($genus)
{
    return getUrl('genus', ['genus' => $genus]);
}

function XC_speciesURL($genus, $species)
{
    if ($genus && $species) {
        return getUrl('species', ['genus' => $genus, 'species' => $species]);
    }
}
