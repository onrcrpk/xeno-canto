<?php

namespace xc;

use Exception;

class Settings
{

    private $dbHost;

    private $dbUser;

    private $dbPassword;

    private $dbDatabase;

    private $enableDebug;

    private $mockSession;

    public function __construct($settings)
    {
        if (!array_key_exists('db', $settings)) {
            throw new Exception("no 'db' key in settings");
        } else {
            $this->setDbSettings($settings['db']);
        }

        if (array_key_exists('debug', $settings)) {
            $this->enableDebug = $settings['debug'];
        } else {
            $this->enableDebug = false;
        }

        if (array_key_exists('useMockSession', $settings)) {
            $this->mockSession = $settings['useMockSession'];
        } else {
            $this->mockSession = false;
        }
    }

    public static function load($filename, $env)
    {
        $settings = [];
        try {
            include $filename;
        } catch (Exception $e) {
            return null;
        }

        if (array_key_exists($env, $settings)) {
            return new Settings($settings[$env]);
        }
        throw new Exception("Config environment '$env' doesn't exist");
    }

    public function dbHost()
    {
        return $this->dbHost;
    }

    public function dbUser()
    {
        return $this->dbUser;
    }

    public function dbPassword()
    {
        return $this->dbPassword;
    }

    public function dbDatabase()
    {
        return $this->dbDatabase;
    }

    public function debug()
    {
        return $this->enableDebug;
    }

    public function useMockSession()
    {
        return $this->mockSession;
    }

    private function setDbSettings($db)
    {
        if (!array_key_exists('user', $db)) {
            throw new Exception('Missing db.user config setting');
        } else {
            $this->dbUser = $db['user'];
        }

        if (!array_key_exists('password', $db)) {
            throw new Exception('Missing db.password config setting');
        } else {
            $this->dbPassword = $db['password'];
        }

        if (!array_key_exists('database', $db)) {
            throw new Exception('Missing db.database config setting');
        } else {
            $this->dbDatabase = $db['database'];
        }

        if (array_key_exists('host', $db)) {
            $this->dbHost = $db['host'];
        } else {
            $this->dbHost = 'localhost';
        }
    }
}
