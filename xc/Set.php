<?php

namespace xc;

/* a set of recordings */

use Exception;

class Set
{

    public const VISIBILITY_PRIVATE = 0;

    public const VISIBILITY_UNLISTED = 1;

    public const VISIBILITY_PUBLIC = 2;

    public static $visibilityOptions;

    private $id;

    private $name;

    private $description;

    private $userId;

    private $timestamp;

    private $visibility;

    private $numRecordings;

    public function __construct(
        $id,
        $name,
        $description,
        $userId,
        $timestamp,
        $visibility,
        $numRecordings
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->userId = $userId;
        $this->timestamp = $timestamp;
        $this->visibility = $visibility;
        $this->numRecordings = $numRecordings;
    }

    public static function init()
    {
        self::$visibilityOptions = [
            self::VISIBILITY_PRIVATE => _('Private: only visible to you'),
            self::VISIBILITY_UNLISTED => _(
                'Unlisted: visible to anybody who knows the URL'
            ),
            self::VISIBILITY_PUBLIC => _(
                'Public: displayed on your public profile'
            ),
        ];
    }

    public static function loadAll($includePrivate = false)
    {
        $where = '';
        if (!$includePrivate) {
            $where = 'WHERE listed=' . self::VISIBILITY_PUBLIC;
        }
        $res = app()->db()->query(self::sql($where));
        $sets = [];
        while ($row = $res->fetch_object()) {
            $sets[] = self::fromDbRow($row);
        }
        return $sets;
    }

    private static function sql($whereClause, $orderClause = '')
    {
        return "
            SELECT S.*, COUNT(R.snd_nr) AS nrecs 
            FROM datasets S 
            LEFT JOIN datasets_recordings R USING(datasetid) $whereClause 
            GROUP BY datasetid $orderClause";
    }

    private static function fromDbRow($row)
    {
        return new Set(
            $row->datasetid,
            $row->name,
            $row->description,
            $row->userid,
            $row->date,
            $row->listed,
            $row->nrecs
        );
    }

    public static function loadUserSets($userid, $includePrivate = false)
    {
        $userid = escape($userid);
        $clause = "WHERE userid='$userid'";
        if (!$includePrivate) {
            $clause .= ' AND listed=' . self::VISIBILITY_PUBLIC;
        }
        $res = app()->db()->query(self::sql($clause));
        $sets = [];
        while ($row = $res->fetch_object()) {
            $sets[] = self::fromDbRow($row);
        }
        return $sets;
    }

    public static function find($queryString, $includePrivate = false)
    {
        $escaped = escape($queryString);
        $where = "WHERE MATCH(S.name, S.description) AGAINST('$escaped')";
        if (!$includePrivate) {
            $where .= ' AND S.listed=' . self::VISIBILITY_PUBLIC;
        }
        $order = "ORDER BY MATCH(S.name, S.description) AGAINST('$escaped') DESC";
        $res = app()->db()->query(self::sql($where, $order));

        $sets = [];
        while ($row = $res->fetch_object()) {
            $sets[] = self::fromDbRow($row);
        }
        return $sets;
    }

    public static function create($name, $description, $userid, $visibility)
    {
        $name = strip($name);
        $description = strip($description, true, true);
        $userid = escape($userid);
        $visibility = intval($visibility);
        $res = app()->db()->query(
            "INSERT INTO datasets VALUES(DEFAULT, '$name', '$description', '$userid', NOW(), $visibility)"
        );
        $r2 = app()->db()->query(
            self::sql('WHERE datasetid=LAST_INSERT_ID()')
        );
        $row = $r2->fetch_object();
        if (!$row) {
            return null;
        }
        return self::fromDbRow($row);
    }

    public function name()
    {
        return $this->name;
    }

    public function description()
    {
        return $this->description;
    }

    public function userId()
    {
        return $this->userId;
    }

    public function timestamp()
    {
        return $this->timestamp;
    }

    public function visibility()
    {
        return $this->visibility;
    }

    public function visibilityDescription()
    {
        return self::$visibilityOptions[$this->visibility];
    }

    public function numRecordings()
    {
        return $this->numRecordings;
    }

    public function recordings($setId = false, $page = Query::NO_PAGING, $pageSize = -1)
    {
        // Ruud: there's a context problem if this method is accessed from the outside:
        // add the option to pass the id in that case
        $setId = isset($this) ? $this->id() : $setId;

        $res = app()->db()->query("SELECT snd_nr from datasets_recordings where datasetid=$setId");
        $ids = [];
        while ($row = $res->fetch_object()) {
            $ids[] = intval($row->snd_nr);
        }

        $idlist = implode(', ', $ids);
        $q = new Query("nr:$idlist");
        if (!$pageSize != -1) {
            $q->setPageSize($pageSize);
        }
        $res = $q->execute($page);

        $recordings = [];
        if (!$res) {
            return $recordings;
        }
        while ($row = $res->fetch_object()) {
            $recordings[] = new Recording($row);
        }
        return $recordings;
    }

    public function id()
    {
        return $this->id;
    }

    public function url()
    {
        return getUrl('set', ['id' => $this->id()]);
    }

    public function update($name, $description, $visibility)
    {
        $name = escape($name);
        $description = escape($description);
        $visibility = intval($visibility);
        $res = app()->db()->query(
            "UPDATE datasets SET name='$name', description='$description', listed=$visibility WHERE datasetid={$this->id()}"
        );
        if (!$res) {
            throw new Exception(
                "unable to update set {$this->id()} " . mysqli()->error
            );
        }
        $this->reload();
        return true;
    }

    private function reload()
    {
        $res = app()->db()->query(self::sql("WHERE datasetid={$this->id()}"));
        if (!$res) {
            throw new Exception(
                "Unable to reload set {$this->id()} from database"
            );
        }
        $row = $res->fetch_object();
        $this->name = $row->name;
        $this->description = $row->description;
        $this->userId = $row->userid;
        $this->timestamp = $row->date;
        $this->visibility = $row->listed;
        $this->numRecordings = $row->nrecs;
    }

    public function delete()
    {
        app()->db()->query(
            "DELETE FROM datasets WHERE datasetid={$this->id()}"
        );
        app()->db()->query(
            "DELETE FROM datasets_recordings WHERE datasetid={$this->id()}"
        );
    }

    public function addRecording($rec)
    {
        if (!$rec) {
            return false;
        }
        return $this->addRecordings([$rec]);
    }

    public function addRecordings($recordings)
    {
        $values = [];
        foreach ($recordings as $rec) {
            $values[] = "(DEFAULT, {$this->id()}, {$rec->xcid()})";
        }

        $res = app()->db()->query(
            'INSERT IGNORE INTO datasets_recordings VALUES ' . implode(
                ',',
                $values
            )
        );

        $n = 0;
        if ($res) {
            $n = mysqli()->affected_rows;
        } else {
            echo(mysqli()->error);
        }
        $this->reload();

        return $n;
    }

    public function removeRecordings($ids)
    {
        foreach ($ids as $id) {
            $xcid = intval($id);
            if (!$xcid) {
                continue;
            }

            $sql = "DELETE FROM datasets_recordings WHERE datasetid={$this->id()} AND snd_nr=$xcid";
            $res = app()->db()->query($sql);
        }
        $this->reload();
    }

    public function containsRecording($rec)
    {
        $sql = "SELECT snd_nr FROM datasets_recordings WHERE datasetid={$this->id()} AND snd_nr={$rec->xcid()}";
        $res = app()->db()->query($sql);
        return $res->num_rows > 0;
    }

    public function speciesInSet($setId = false, $includeBackground = true)
    {
        if (!$setId) {
            throw new Exception('Missing parameter set_id');
        }
        $set = self::load($setId);

        $sql = '
            select t2.species_nr, t2.back_nrs from datasets_recordings as t1
            left join birdsounds as t2 on t1.snd_nr = t2.snd_nr
            where t1.datasetid = ' . $set->id();
        $res = app()->db()->query($sql);

        $species = [];
        while ($row = $res->fetch_object()) {
            // Use key to filter duplicates rather than MySQL query
            $species[$row->species_nr] = $row->species_nr;
            if ($includeBackground && !empty($row->back_nrs)) {
                $backNrs = explode(',', $row->back_nrs);
                foreach ($backNrs as $speciesNr) {
                    $species[$speciesNr] = $speciesNr;
                }
            }
        }

        foreach ($species as $speciesNr) {
            $species[$speciesNr] = Species::load($speciesNr, false);
        }

        usort(
            $species,
            function ($a, $b) {
                return strcmp(
                    $a->genus() . $a->species(),
                    $b->genus() . $b->species()
                );
            }
        );

        return $species;
    }

    public static function load($id)
    {
        $id = intval($id);
        $res = app()->db()->query(self::sql("WHERE datasetid=$id"));
        $row = $res->fetch_object();
        if ($row) {
            return self::fromDbRow($row);
        }
        return null;
    }
}

Set::init();
