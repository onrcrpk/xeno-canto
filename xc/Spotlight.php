<?php

namespace xc;

class Spotlight
{

    private $row;

    public function __construct($dbrow)
    {
        $this->row = $dbrow;
    }

    public static function random($groupId = false)
    {
        // Weight the selection of a spotlight toward the most recent spotlights.
        // There is an arbitrary constant added to the spotlight_id that is designed
        // to weight all of the spotlights in a range such that the earlier
        // spotlights could conceivably still be chosen, but making the newer ones
        // much more likely.  The weighting formula can certainly be tweaked in the
        // future.  This is not particularly efficient but since this is a small
        // table, it's not likely to matter much...
        if (!$groupId) {
            $sql = 'SELECT * FROM spotlight_v2 WHERE enabled = 1 ORDER BY (LOG(spotlight_id) * RAND()) DESC LIMIT 1';
        } else {
            $groupId = (int)$groupId;
            $sql = "
                SELECT t1.* FROM spotlight_v2 AS t1
                LEFT JOIN birdsounds AS t2 ON t1.featured_recording = t2.snd_nr
                WHERE t1.enabled = 1 AND t2.group_id = $groupId
                ORDER BY (LOG(t1.spotlight_id) * RAND()) DESC 
                LIMIT 1";
        }
        $res = query_db($sql);
        $row = $res->fetch_object();
        return new Spotlight($row);
    }

    public static function all()
    {
        $sql = 'SELECT * from spotlight_v2 WHERE enabled=1 ORDER BY spotlight_id DESC';
        $res = query_db($sql);
        $spotlights = [];
        while ($row = $res->fetch_object()) {
            $spotlights[] = new Spotlight($row);
        }

        return $spotlights;
    }

    public static function load($id)
    {
        $id = intval($id);
        $sql = "SELECT * from spotlight_v2 WHERE spotlight_id=$id";
        $res = query_db($sql);
        if (!$res) {
            echo(mysqli()->error);
        }

        $row = $res->fetch_object();
        return new Spotlight($row);
    }

    public function image($absolute = false)
    {
        $url = getUrl('index', [], $absolute);
        $url .= $this->row->img_path;
        return $url;
    }

    public function toHtml()
    {
        $playerOptions = ['showSono' => false];
        if (!$this->row) {
            return;
        }

        $body = XC_formatText($this->text());
        $rec = $this->recording();
        $player = '';
        if ($rec) {
            $player = $rec->player($playerOptions);
        }

        $spotlight = "<section class='spotlight'>
            <a name='spotlight-{$this->id()}'></a><h1><a href='" . getUrl(
                'spotlight',
                ['id' => $this->id()]
            ) . "'>{$this->title()}</a></h1>
            <figure>
            <img src='/{$this->row->img_path}'>";
        if ($this->row->img_caption) {
            $spotlight .= "
                <figcaption>
                <p>{$this->row->img_caption}</p>
                </figcaption>";
        }
        $spotlight .= "
            </figure>
            <div class='content'>
            $player
            <article>
            $body
            </article>
            </div>
            </section>
            ";

        return $spotlight;
    }

    public function text()
    {
        return $this->row->text;
    }

    public function recording()
    {
        return Recording::load($this->row->featured_recording);
    }

    public function id()
    {
        return $this->row->spotlight_id;
    }

    public function title()
    {
        return $this->row->title;
    }

}
