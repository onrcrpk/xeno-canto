<?php

namespace xc;

use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;

class XCMail
{

    private $to;

    private $subject;

    private $message;

    private $replyTo;

    private $transport;

    private $mailer;

    private $email;


    public function __construct($to, $subject, $message, $replyTo = null)
    {
        $this->to = !empty($to) && !is_array($to) ? [$to] : $to;
        $this->subject = $subject;
        $this->message = $message;
        $this->replyTo = $replyTo;

        $this->transport = Transport::fromDsn(MAILER_DSN);
        $this->mailer = new Mailer($this->transport);
    }

    # Use the to setter if you need to send several mails in a loop and
    # you do not want the recipients to see each other's addresses.
    # If you need to send to just one or a few addresses, you can initiate
    # the class with a single $to string or an array with multiple strings.
    public function to($to)
    {
        $this->to = !is_array($to) ? [$to] : $to;
        return $this;
    }

    public function send()
    {
        if (!$this->to) {
            XC_logger()->logError(
                'Could not send email: to address is missing'
            );
            return false;
        }

        $this->email = (new Email())
            ->from(CONTACT_EMAIL)
            ->subject($this->subject)
            ->text(html_entity_decode($this->message));

        foreach ($this->to as $i => $to) {
            // Reroute email in all but production environments
            if (strtolower(ENVIRONMENT) != 'prod') {
                $to = $this->rerouteTo($to);
            }
            $i == 0 ? $this->email->to($to) : $this->email->addTo($to);
        }

        if ($this->replyTo) {
            $this->email->replyTo($this->replyTo);
        }

        try {
            $this->mailer->send($this->email);
        } catch (TransportException $e) {
            XC_logger()->logError('Could not send email: ' . $e->getDebug());
        }
    }

    private function rerouteTo($to)
    {
        if (empty(REROUTE_EMAIL)) {
            $error = 'Cannot send email, please set REROUTE_EMAIL env variable!';
            XC_logger()->logError($error);
            $this->email->text(
                $error . "\n\nOriginal message:\n\n" . $this->message
            );
            return WEBMASTER_EMAIL;
        }
        [$user, $domain] = explode('@', REROUTE_EMAIL);
        return implode('@', [$user . $this->plusTo($to), $domain]);
    }

    private function plusTo($to)
    {
        return '+' . str_replace(['@', '+', '.'], ['_at_', '_plus_', '_'], $to);
    }

}
