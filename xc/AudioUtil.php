<?php

namespace xc;

use Exception;
use getID3;
use getid3_lib;
use getid3_writetags;

use const GETID3_INCLUDEPATH;

function psystem($cmd, $rv)
{
    XC_logger()->logInfo("Executing:\n[[$cmd]]\n");
    $out = system($cmd, $rv);
    if ($rv) {
        XC_logger()->logError("Failed to generate file.\n `$cmd`\nreturn => $rv \nOutput: [[$out]]\n");
    }
    return $out;
}

function unwrap($str)
{
    $out = str_replace("\r\n", ' / ', $str);
    $out = str_replace("\n\r", ' / ', $out);
    $out = str_replace("\n", ' / ', $out);
    return str_replace("\r", ' / ', $out);
}

class AudioUtil
{

    public static function writeId3($xcid)
    {
        $rec = Recording::load($xcid, false);
        if (!$rec) {
            XC_logger()->logError("Write id3: recording $xcid not found");
            return false;
        }

        // wavs do not support ID3 tags
        if ($rec->fileType() == 'mp3') {
            $title = $rec->commonName();
            if (!$rec->isPseudoSpecies()) {
                $title .= " ({$rec->fullScientificName()})";
            }
            $recordist = $rec->recordist();
            $loc = $rec->location();
            $cnt = $rec->country();
            $lat = $rec->latitude();
            $long = $rec->longitude();
            $date = $rec->date();
            $year = date('Y', strtotime($date));
            $time = $rec->time();
            $elev = $rec->elevation();
            $songtype = $rec->soundType();
            $rmk = unwrap($rec->remarks());
            $bgspecies = $rec->backgroundSpecies();
            $cat = $rec->xcid();
            $cclic = License::lookupById($rec->license());

            $dir = $_SERVER['DOCUMENT_ROOT'];
            if (!$dir) {
                $dir = '.';
            }
            $filename = "$dir/" . $rec->filePath();

            $others = 'also:  ';
            $bgnames = [];
            for ($i = 0; $i < count($bgspecies); $i++) {
                $bgnames[] = $bgspecies[$i]->commonName();
            }
            $others .= implode(', ', $bgnames);

            $coords = '';
            if ($lat && $long) {
                $coords = " ($lat, $long)";
            }
            $cmt = "XC$cat © $recordist // $loc$coords, $cnt // {$elev}m // {$time}h, $date // $songtype";
            if ($rmk) {
                $cmt .= " // $rmk";
            }
            if (count($bgspecies)) {
                $cmt .= " // $others";
            }

            $getID3 = new getID3();
            getid3_lib::IncludeDependency(GETID3_INCLUDEPATH . 'write.php', __FILE__);
            $tagwriter = new getid3_writetags();
            $tagwriter->filename = $filename;
            $tagwriter->tagformats = ['id3v2.4', 'ape'];
            $TagData['title'][0] = $title;
            $TagData['genre'][0] = ucfirst(strtolower($rec->family()));
            $TagData['artist'][0] = "$recordist";
            $TagData['album'][0] = 'xeno-canto';
            $TagData['content group description'][0] = $rec->speciesNumber();
            $TagData['comment'][0] = $cmt;
            $TagData['copyright'][0] = "$year {$rec->recordist()}";
            $TagData['copyright information'][0] = $cclic->url;
            $TagData['url_file'][0] = "//xeno-canto.org/{$rec->xcid()}";
            $tagwriter->tag_data = $TagData;

            if (!$tagwriter->WriteTags()) {
                $errmsg = implode('; ', $tagwriter->errors);
                XC_logger()->logError("Failed to set ID3 tags: $errmsg");
                return false;
            }
        }

        return true;
    }

    public static function generateSonos($xcid, $sono = true, $wave = true)
    {
        $rec = Recording::load($xcid, false);
        if (!$rec) {
            XC_logger()->logError("Generate sono's: recording $xcid not found");
            return false;
        }
        $file = $rec->filePath();
        $successSono = $successWave = true;

        if ($sono) {
            $successSono = self::generateSonoInternal(
                    $file,
                    SonoSize::SMALL,
                    $rec->sonoPath(SonoSize::SMALL, SonoType::SPECTROGRAM)
                ) && self::generateSonoInternal(
                    $file,
                    SonoSize::MEDIUM,
                    $rec->sonoPath(SonoSize::MEDIUM, SonoType::SPECTROGRAM)
                ) && self::generateSonoInternal(
                    $file,
                    SonoSize::LARGE,
                    $rec->sonoPath(SonoSize::LARGE, SonoType::SPECTROGRAM)
                );
        }
        if ($wave) {
            $successWave = self::generateWaveInternal(
                    $file,
                    SonoSize::SMALL,
                    $rec->sonoPath(SonoSize::SMALL, SonoType::OSCILLOGRAM)
                ) && self::generateWaveInternal(
                    $file,
                    SonoSize::MEDIUM,
                    $rec->sonoPath(SonoSize::MEDIUM, SonoType::OSCILLOGRAM)
                ) && self::generateWaveInternal(
                    $file,
                    SonoSize::LARGE,
                    $rec->sonoPath(SonoSize::LARGE, SonoType::OSCILLOGRAM)
                );

            // Delete intermediate .dat file used for waveforms
            self::deleteDatFile($file);
        }

        return $successSono && $successWave;
    }

    private static function generateSonoInternal($audioFile, $size, $outfile)
    {
        // get the existing locale
        // without this sometimes non-ascii characters get stripped from the
        // filename and the sonogen fails
        $locale = setlocale(LC_CTYPE, 0);
        setlocale(LC_CTYPE, 'en_US.UTF-8');

        XC_logger()->logInfo("Generating spectrogram for '$audioFile' ($size): '$outfile'");

        $outdir = dirname($outfile);
        if (!is_dir($outdir)) {
            XC_logger()->logInfo("'$outdir' doesn't exist, creating");
            $rv = mkdir($outdir, 0775, true);
            if (!$rv) {
                XC_logger()->logError("Unable to create directory '$outdir'");
            }
        }

        $grid = '';
        $duration = '';
        $freq = self::sonogenFrequency($audioFile);
        $input = escapeshellarg($audioFile);
        $output = '-o ' . escapeshellarg($outfile);

        switch ($size) {
            case SonoSize::MEDIUM:
                $geometryOptions = '-w 480 -h 160';
                $duration = '-d 10';
                break;
            case SonoSize::LARGE:
                $geometryOptions = '-w 960 -h 320';
                $duration = '-d 10';
                $grid = '-g';
                break;
            case SonoSize::FULL:
                // for some reason, a resolution of 80 sometimes causes crashes in the
                // sonogram generator.  Haven't been able to figure out why yet.
                $geometryOptions = '-h 160 -r 75';
                $grid = '-g';
                break;
            case SonoSize::SMALL:
            default:
                $geometryOptions = '-w 240 -h 80';
                $duration = '-d 10';
                break;
        }
        $rv = 0;
        $cmd = SONOGEN_PATH . " $freq $geometryOptions $duration $grid $output $input";

        psystem($cmd, $rv);

        // restore original locale
        setlocale(LC_CTYPE, $locale);
        return is_file($outfile);
    }

    private static function sonogenFrequency($audioFile)
    {
        return '-f ' . self::frequency(self::sampleRate($audioFile), self::fileType($audioFile));
    }

    public static function frequency($sampleRate, $fileType)
    {
        if ($fileType != 'wav') {
            return 15000;
        } elseif ($sampleRate <= 48000) {
            return 22000;
        } elseif ($sampleRate <= 96000) {
            return 48000;
        } elseif ($sampleRate <= 192000) {
            return 96000;
        }
        return 192000;
    }

    public static function sampleRate($audioFile)
    {
        $getID3 = new getID3();
        $info = $getID3->analyze($audioFile);
        if (!empty($info['audio']['sample_rate'])) {
            $rate = $info['audio']['sample_rate'];
            XC_logger()->logInfo("Sample rate for $audioFile is $rate (getID3)");
            return $rate;
        } else {
            XC_logger()->logWarn("Failed to determine sample rate for $audioFile using getID3");
        }
        try {
            $rate = exec(SOX_PATH . ' --i -r ' . escapeshellarg($audioFile));
            XC_logger()->logInfo("Sample rate for $audioFile is $rate (sox)");
            return $rate;
        } catch (Exception $e) {
            XC_logger()->logWarn("Failed to determine sample rate for $audioFile using sox: " . $e->getMessage());
        }
    }

    public static function fileType($audioFile)
    {
        $getID3 = new getID3();
        $info = $getID3->analyze($audioFile);
        if (!empty($info['fileformat'])) {
            $type = $info['fileformat'];
            XC_logger()->logInfo("File type for $audioFile is $type (getID3)");
            return $type;
        } else {
            XC_logger()->logWarn("Failed to determine file type for $audioFile using getID3");
        }
        try {
            $type = exec(SOX_PATH . ' --i -t ' . escapeshellarg($audioFile));
            XC_logger()->logInfo("File type for $audioFile is $type (sox)");
            return $type;
        } catch (Exception $e) {
            XC_logger()->logWarn("Failed to determine file type for $audioFile using sox: " . $e->getMessage());
        }
    }

    private static function generateWaveInternal($audioFile, $size, $outfile)
    {
        $locale = setlocale(LC_CTYPE, 0);
        setlocale(LC_CTYPE, 'en_US.UTF-8');

        XC_logger()->logInfo("Generating oscillogram for '$audioFile' ($size): '$outfile'");

        $outdir = dirname($outfile);
        if (!is_dir($outdir)) {
            XC_logger()->logInfo("'$outdir' doesn't exist, creating");
            $rv = mkdir($outdir, 0775, true);
            if (!$rv) {
                XC_logger()->logError("Unable to create directory '$outdir'");
            }
        }

        $output = '-o ' . escapeshellarg($outfile);
        $labels = $duration = '';
        $backgroundColor = '--background-color ffffff';
        $waveformColor = '--waveform-color 000000';

        // Create intermediate .dat file first, which speeds up generation of wav pngs
        $datFile = self::createDatFile($audioFile);
        $input = '-i ' . escapeshellarg($datFile);

        switch ($size) {
            case SonoSize::MEDIUM:
                $geometryOptions = '-w 480 -h 80';
                $duration = '-e 10';
                $labels = '--no-axis-labels';
                break;
            case SonoSize::LARGE:
                $geometryOptions = '-w 960 -h 320';
                $duration = '-e 10';
                break;
            case SonoSize::FULL:
                $geometryOptions = '-h 160 -r 75';
                break;
            case SonoSize::SMALL:
            default:
                $geometryOptions = '-w 240 -h 40';
                $duration = '-e 10';
                $labels = '--no-axis-labels';
                break;
        }
        $rv = 0;
        $cmd = AUDIOWAVEFORM_PATH . " -q $input $output $geometryOptions $duration $labels $backgroundColor $waveformColor";

        psystem($cmd, $rv);
        setlocale(LC_CTYPE, $locale);
        return is_file($outfile);
    }

    private static function createDatFile($audioFile)
    {
        $locale = setlocale(LC_CTYPE, 0);
        setlocale(LC_CTYPE, 'en_US.UTF-8');

        $input = escapeshellarg($audioFile);
        $datFile = $audioFile . '.dat';
        if (!file_exists($datFile)) {
            $output = escapeshellarg($datFile);
            psystem(AUDIOWAVEFORM_PATH . " -q --pixels-per-second 500 -i $input -o $output", 0);
        }

        setlocale(LC_CTYPE, $locale);
        return $datFile;
    }

    private static function deleteDatFile($audioFile)
    {
        $datFile = $audioFile . '.dat';
        @unlink($datFile);
        return !is_file($datFile);
    }

    public static function duration($audioFile)
    {
        return system(SOX_PATH . " --i -D " . escapeshellarg($audioFile));
    }

    public static function scheduleFullLengthSono($xcid)
    {
        $xcid = intval($xcid);
        return query_db(
            "INSERT INTO sonogram_queue VALUES($xcid) ON DUPLICATE KEY UPDATE snd_nr = $xcid"
        );
    }

    public static function createMp3($xcid)
    {
        $locale = setlocale(LC_CTYPE, 0);
        setlocale(LC_CTYPE, "en_US.UTF-8");

        $rec = Recording::load($xcid, false);
        if (!$rec) {
            XC_logger()->logError("Create mp3s: recording $xcid not found");
            return false;
        }
        $input = escapeshellarg($rec->filePath());
        $mp3 = $slow = true;
        $rate = $rec->isHighRes() ? '-r 48000' : '';

        if ($rec->isLossless()) {
            XC_logger()->logInfo("Generating mp3 derivative for $input");
            $output = escapeshellarg($rec->mp3Path());
            psystem(SOX_PATH . " $input -C 320 $rate $output", 0);
            $mp3 = is_file($rec->mp3Path());

            if ($rec->slowFactor() && $rec->isHighRes()) {
                XC_logger()->logInfo("Generating {$rec->slowFactor()}x slowed down mp3 derivative for $input");
                $output = escapeshellarg($rec->mp3Path($rec->slowFactor()));
                psystem(SOX_PATH . " $input -C 320 $rate $output speed " . (1 / $rec->slowFactor()), 0);
                $slow = is_file($rec->mp3Path($rec->slowFactor()));
            }
        }

        setlocale(LC_CTYPE, $locale);
        return $mp3 && $slow;
    }

    public static function generateFullLengthSonogram($xcid)
    {
        $rec = Recording::load($xcid, false);
        $rv = false;

        if ($rec) {
            $mp3 = $rec->filePath();
            $sonoloc = $rec->sonoPath();
            $dirname = pathinfo($sonoloc, PATHINFO_DIRNAME);
            if (!is_dir($dirname)) {
                mkdir($dirname, 0755, true);
            }

            $path = $rec->sonoPath(SonoSize::FULL);
            $cwd = getcwd();
            $rv = self::generateSonoInternal($mp3, SonoSize::FULL, $path);

            if ($rv) {
                query_db(
                    "INSERT INTO sonograms VALUES({$rec->xcid()}, '$path') ON DUPLICATE KEY UPDATE sono_full='$path'"
                );
            }
        }

        // remove this from the queue even if the sono wasn't generated
        // successfully.  Some sonograms are just not possible to generate (due to
        // missing files, too-long durations, etc.  If we keep it in the queue,
        // it'll just get retried over and over and keep failing
        query_db("DELETE FROM sonogram_queue WHERE snd_nr = " . intval($xcid));

        return $rv;
    }

    public static function updateAudioProperties($xcid)
    {
        $rec = Recording::load($xcid, false);
        if (!$rec) {
            XC_logger()->logError("Update audio properties: recording $xcid not found");
            return false;
        }

        $filepath = $rec->filePath();
        $getID3 = new getID3();
        $fileinfo = $getID3->analyze($filepath);

        $smp = !empty($fileinfo['audio']['sample_rate']) ? $fileinfo['audio']['sample_rate'] : 0;
        $chan = !empty($fileinfo['audio']['channels']) ? $fileinfo['audio']['channels'] : 0;
        $fmt = !empty($fileinfo['fileformat']) ? $fileinfo['fileformat'] : '';
        $len = !empty($fileinfo['playtime_seconds']) ? $fileinfo['playtime_seconds'] : 0;
        $bitrate = !empty($fileinfo['audio']['bitrate']) ? $fileinfo['audio']['bitrate'] : 0;
        $mode = !empty($fileinfo['audio']['bitrate_mode']) ? $fileinfo['audio']['bitrate_mode'] : 0;
        $loss = !empty($fileinfo['audio']['lossless']) ? $fileinfo['audio']['lossless'] : 0;

        $sql = "INSERT INTO audio_info (snd_nr, format, length, bitrate, bitrate_mode, bits_per_sample, smp, channels, lossless)
            VALUES($xcid, '$fmt', '$len', '$bitrate', '$mode', DEFAULT, '$smp', '$chan', '$loss')
            ON DUPLICATE KEY UPDATE
                format=VALUES(format),
                length=VALUES(length),
                bitrate=VALUES(bitrate),
                bitrate_mode=VALUES(bitrate_mode),
                bits_per_sample=VALUES(bits_per_sample),
                smp=VALUES(smp),
                channels=VALUES(channels),
                lossless=VALUES(lossless)";

        return query_db($sql);
    }
}
