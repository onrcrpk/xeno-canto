<?php

namespace xc;

use function PHP81_BC\strftime;

class NewsItem
{

    private $m_text;

    private $m_id;

    private $m_comments;

    private $m_date;

    public function __construct($db_row)
    {
        $this->m_text = $db_row->text;
        $this->m_id = $db_row->news_nr;
        $this->m_date = $db_row->date;
        $this->m_comments = null;
    }

    public static function load($id)
    {
        $nr = intval($id);
        $sql = "select * from news_world where news_nr = '$nr'";
        $res = query_db($sql);
        $row = $res->fetch_object();
        if ($row) {
            return new NewsItem($row);
        }

        return null;
    }

    public static function loadRecent($nr = 0)
    {
        $items = [];
        if (empty($nr)) {
            $nr = 4;
        }

        $sql = "select * from news_world where 1 order by news_nr desc limit $nr";
        $res = query_db($sql);
        while ($row = $res->fetch_object()) {
            $items[] = new NewsItem($row);
        }

        return $items;
    }

    //=====================
    // make the news for the front page
    //=====================

    public static function loadForYear($year)
    {
        $items = [];
        if (empty($year)) {
            return;
        }

        $sql = "SELECT * from news_world WHERE YEAR(date)=$year ORDER BY news_nr DESC";
        $res = query_db($sql);
        while ($row = $res->fetch_object()) {
            $items[] = new NewsItem($row);
        }

        return $items;
    }

    public function toHtml()
    {
        $date = $this->formatDate('%B %e, %Y');

        $articleContent = XC_formatUserText($this->text());
        $news = "
            <article>
            <footer>$date
            ";

        $user = User::current();
        if ($user && $user->isAdmin()) {
            $news .= "<span><a href='" . getUrl(
                    'admin-news-edit',
                    ['id' => $this->id()]
                ) . "'><img class='icon' width='16' height='16' src='/static/img/admin-16.png'/> Edit</a></span>";
        }

        $news .= "
            </footer>
            <p>
            $articleContent
            </p>
            <section class='comments'>";

        $comments = $this->comments();
        $nr_replies = count($comments);
        foreach ($comments as $comment) {
            /// 'username' on 'date'
            $dateStr = sprintf(
                _('%s on %s'),
                $comment->author(),
                $comment->formatDate('%B %e')
            );
            $content = XC_formatUserText($comment->text());
            $deleteLink = '';
            if ($user && $user->isAdmin()) {
                $deleteLink = "<img class='icon' src='/static/img/admin-16.png' title='xeno-canto administrator'/> <a href='" . getUrl(
                        'admin-delete-comment',
                        ['t' => 'discuss_news_world', 'id' => $comment->id()]
                    ) . "'>(delete this comment)</a>";
            }
            $news .= "
                <article>
                <div>$content $deleteLink
                </div>
                <footer>$dateStr</footer>
                </article>
                ";
        }
        if ($nr_replies) {
            $news .= "
                <p><b>$nr_replies</b> replies ::
                ";
        }// if nr_replies>0
        $news .= "
            <img width='14' height='14' src='/static/img/discuss-light.png'> <a href='" . getUrl(
                'news_discussion',
                ['news_nr' => $this->id()]
            ) . "'>" .
            _('Your reply') . '</a>
            </p>
            </section>
            </article>
            ';

        return $news;
    }

    public function formatDate($format)
    {
        $dateparts = explode('-', $this->m_date);
        return strftime(
            $format,
            mktime(0, 0, 0, $dateparts[1], $dateparts[2], $dateparts[0])
        );
    }

    public function text()
    {
        return $this->m_text;
    }

    public function id()
    {
        return $this->m_id;
    }

    public function comments()
    {
        if (is_null($this->m_comments)) {
            $sql = "select * from discuss_news_world where news_nr = '$this->m_id' order by nr asc";
            $res = query_db($sql);
            $this->m_comments = [];
            while ($row = $res->fetch_object()) {
                $this->m_comments[] = new NewsComment($row);
            }
        }

        return $this->m_comments;
    }
}
