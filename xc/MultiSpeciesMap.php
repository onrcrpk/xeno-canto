<?php

namespace xc;

class MultiSpeciesMap extends RecordingMap
{

    private $m_spec;

    public function __construct($mapCanvasId, $result)
    {
        // just a single marker spec for all results
        $this->m_spec          = new MarkerSpec();
        $this->m_spec->url     = Marker::$icons[0];
        $this->m_spec->height  = 14;
        $this->m_spec->width   = 14;
        $this->m_spec->anchorX = 7;
        $this->m_spec->anchorY = 7;

        parent::__construct($mapCanvasId, $result, true);
    }

    protected function createMarkerForRecording($rec)
    {
        $marker       = new Marker();
        $marker->spec = $this->m_spec;
        // avoid double-quotes and single-quotes which will cause
        // problems with the generated javascript
        $marker->title     = "XC{$rec->xcid()}: {$rec->commonName()}";
        $marker->zIndex    = 50;
        $marker->animate   = true;
        $marker->latitude  = $rec->latitude();
        $marker->longitude = $rec->longitude();
        $marker->id        = $rec->xcid();

        return $marker;
    }
}
