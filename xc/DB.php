<?php

namespace xc;

use mysqli;

class DB
{

    private $db_connection;

    private $profile;

    private $logger;

    private $timings;

    public function __construct($settings, $profile = false)
    {
        $this->db_connection = new mysqli(
            $settings->dbHost(),
            $settings->dbUser(),
            $settings->dbPassword(),
            $settings->dbDatabase()
        );
        $this->db_connection->set_charset('utf8');

        $this->profile = $profile;
        if ($profile) {
            $this->logger = new XCLogger('XC_profiler');
        }
    }

    public function query($sql)
    {
        $tstart = microtime(true);
        $result = $this->db_connection->query($sql) or
        XC_logger()->logError($sql . ":\n" . $this->db_connection->error);
        $tend = microtime(true);
        if ($this->profile) {
            $this->timings[] = [$sql, $tend - $tstart];
        }
        return $result;
    }

    public function logProfiles()
    {
        if (!$this->profile) {
            return;
        }

        $profileStr = "Database Profiles:\n";
        foreach ($this->timings as $timing) {
            $elapsed = round($timing[1], 6);
            $profileStr .= "    $timing[0]\n        => Time elapsed: {$elapsed}s\n";
        }
        $this->logger->debug($profileStr);
    }

    public function getConnection()
    {
        return $this->db_connection;
    }

    public function insertId()
    {
        return $this->db_connection->insert_id;
    }

}
