<?php

namespace xc;

use function PHP81_BC\strftime;

class NewsComment
{

    private $m_row;

    public function __construct($db_row)
    {
        $this->m_row = $db_row;
    }

    public function id()
    {
        return $this->m_row->nr;
    }

    public function text()
    {
        return $this->m_row->text;
    }

    public function author()
    {
        return $this->m_row->name;
    }

    public function formatDate($format)
    {
        $timeparts = explode(':', $this->m_row->time);
        $dateparts = explode('-', $this->m_row->date);
        return strftime(
            $format,
            mktime(
                $timeparts[0],
                $timeparts[1],
                0,
                $dateparts[1],
                $dateparts[2],
                $dateparts[0]
            )
        );
    }
}
