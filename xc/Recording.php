<?php

/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace xc;

class SonoSize
{
    public const SMALL = 'small';

    public const MEDIUM = 'med';

    public const LARGE = 'large';

    public const FULL = 'full';
}

class SonoType
{
    public const SPECTROGRAM = 'ffts';

    public const OSCILLOGRAM = 'wave';
}

class Recording
{
    public static $counter = 0;

    private static $qualityString = [
        'no score',
        'A',
        'B',
        'C',
        'D',
        'E',
    ];

    protected $m_row;

    private $slowFactor;

    private $group;

    private $soundProperties;

    private $soundPropertyCategories;

    private $backgroundSpeciesAll;

    private $backgroundSpecies;

    private $sp;

    public function __construct($db_row)
    {
        $this->m_row = $db_row;
    }

    public static function load($xcid, $localized = true)
    {
        // Input is escaped in loadMany()
        $recordings = self::loadMany([$xcid], $localized);
        if ($recordings) {
            return $recordings[0];
        }
        return null;
    }

    public static function loadMany($ids, $localized = true)
    {
        assert(is_array($ids));
        $localName = 'english';
        if ($localized) {
            $localName = localNameLanguage();
        }

        $safeIds = [];
        foreach ($ids as $id) {
            $safeIds[] = intval($id);
        }
        $idset = implode(', ', $safeIds);

        $recordings = [];
        $res = query_db(
            "
            SELECT " . Query::birdsoundsSelectFields() . ", cnames.$localName as localName, 
                   audio_info.length, audio_info.format, audio_info.smp as sample_rate
            FROM birdsounds 
            LEFT JOIN taxonomy_multilingual cnames USING(species_nr) 
            LEFT JOIN audio_info USING(snd_nr) 
            WHERE snd_nr IN ($idset)"
        );
        if ($res) {
            while ($row = $res->fetch_object()) {
                $recordings[] = new Recording($row);
            }
        }
        return $recordings;
    }

    public function miniPlayer()
    {
        if ($this->restrictedForUser(User::current())) {
            return '';
        }

        $xcid = $this->xcid();
        $c = self::$counter++;
        $controlsId = "xc_audio_{$xcid}_{$c}";
        $attrFilepath = htmlspecialchars($this->playerUrl(), ENT_QUOTES);
        $colour = $this->isHighRes() ? 'red' : 'black';
        $title = $this->isHighRes() ? _('Ultrasonic, high frequency sound recording') : '';

        return "
            <div class='xc-mini-audio'>
                <audio class='xc-mini-player' id='$controlsId' src='$attrFilepath' preload='metadata'></audio>
                <button class='xc-mini-player' id='btn_$controlsId' data-icon='$colour' title='$title'><img src='/static/img/play-$colour.svg'></button>
            </div>";
    }

    public function restrictedForUser($user = null)
    {
        if (empty($user)) {
            $user = User::current();
        }
        return !(!$this->species()->restricted() || $user && ($user->userId() == $this->recordistID(
                ) || $user->canDownloadRestrictedRecordings() || $user->isAdmin()));
    }

    public function species()
    {
        if (!$this->sp) {
            $this->sp = new Species(
                $this->m_row->genus,
                $this->m_row->species,
                $this->commonName(),
                $this->m_row->species_nr,
                $this->m_row->family,
                $this->m_row->ssp
            );
        }
        return $this->sp;
    }

    public function commonName()
    {
        if (!empty($this->m_row->localName)) {
            return $this->m_row->localName;
        }
        return $this->m_row->eng_name;
    }

    public function recordistID()
    {
        return $this->m_row->dir;
    }

    public function xcid()
    {
        return $this->m_row->snd_nr;
    }

    public function playerUrl($sf = false)
    {
        $path = $this->filePath(true);
        if ($this->isLossless()) {
            $path = file_exists($this->mp3Path($sf)) ? $this->mp3Path($sf) : $path;
        }
        return (REMOTE_BASE_URL ?: "//{$_SERVER['HTTP_HOST']}/") . $path;
    }

    public function filePath($encode = false)
    {
        $path = $this->m_row->path;
        // some files are in subdirectories (e.g. a bunch of A. Bennett Hennessey's recording
        if ($encode) {
            $paths = explode('/', $path);
            $last = count($paths) - 1;
            $paths[$last] = rawurlencode($paths[$last]);
            $path = implode('/', $paths);
        }
        return "sounds/uploaded/{$this->m_row->dir}/$path";
    }

    public function isLossless()
    {
        return $this->fileType() == 'wav';
    }

    public function fileType()
    {
        return $this->m_row->format ?? 'mp3';
    }

    public function mp3Path($sf = false)
    {
        $info = pathinfo($this->filePath());
        return ($info['dirname'] ? $info['dirname'] . '/' : '') . $info['filename'] . ($sf ? "-{$sf}x" : '') . '.mp3';
    }

    public function isHighRes()
    {
        return $this->sampleRate() > 48000;
    }

    public function sampleRate()
    {
        return $this->m_row->sample_rate;
    }

    public function mp3StillScheduled()
    {
        $res = query_db("SELECT snd_nr FROM sonogram_queue WHERE snd_nr = " . $this->xcid());
        return $res->num_rows != 0;
    }

    public function fileUrl()
    {
        return (REMOTE_BASE_URL ?: "//{$_SERVER['HTTP_HOST']}/") . $this->filePath(true);
    }

    public function player($options = null)
    {
        $c = self::$counter++;
        $slowPlayer = false;
        $showSono = true;
        $highResSono = false;
        $showLogo = false;
        $showSpecies = true;
        $showRecordist = true;
        $showDate = false;
        $showLocation = false;
        $linkFields = false;
        $linkSono = true;
        $showAxis = false;

        if ($options) {
            extract($options, EXTR_OVERWRITE);
        }

        $xcid = $this->xcid();
        $controlsId = "xc_audio_{$xcid}_{$c}";
        $xcidText = $titleText = $sonoText = $sonoAxis = '';

        if (!$slowPlayer) {
            $xcidText .= "<div class='jp-xc-id'><table><tr><td>";

            if ($showAxis) {
                $fq = AudioUtil::frequency($this->sampleRate(), $this->fileType()) / 1000;
                $sonoAxis = "<div class='jp-xc-axis'>$fq kHz</div>";
            }
            if ($showSono) {
                if ($highResSono) {
                    $spectroImg = $this->sonoURL(SonoSize::MEDIUM);
                    $waveImg = $this->sonoURL(SonoSize::MEDIUM, SonoType::OSCILLOGRAM);
                } else {
                    $spectroImg = $this->sonoURL();
                    $waveImg = $this->sonoURL(SonoSize::SMALL, SonoType::OSCILLOGRAM);
                }
                if ($linkSono) {
                    // regardless of whether we display the high res sonogram in the
                    // player, provide a link to view the full-size image in a 'lightbox'
                    $name = "{$this->commonName()} ({$this->fullScientificName()})";
                    // FIXME: ENVREC
                    if ($this->isPseudoSpecies()) {
                        $name = $this->commonName();
                    }
                    $sonoTitle = htmlspecialchars("XC{$this->xcid()}: $name by {$this->recordist()}", ENT_QUOTES);
                    $sonoText = "<div class='jp-xc-sono'>
                        <a class='fancybox' rel='sono' title='$sonoTitle' href='{$this->sonoURL(SonoSize::LARGE)}'>
                            <img class='spectro-image' src='$spectroImg'>
                        </a>";
                    if (file_exists($this->sonoPath(SonoSize::SMALL, SonoType::OSCILLOGRAM))) {
                        $sonoText .= "<a class='fancybox' rel='sono' title='$sonoTitle' href='{$this->sonoURL(SonoSize::LARGE, SonoType::OSCILLOGRAM)}'>
                            <img class='wave-image' src='$waveImg'>
                        </a>";
                    }
                    $sonoText .= "</div>";
                } else {
                    $sonoText = "<div class='jp-xc-sono'><img src='$spectroImg'></div>";
                }
            }

            if ($showLogo) {
                $xcidText .= "<span class='xc-logo'>xeno-canto</span> ";
            }

            $xcidText .= "<a href='{$this->URL()}'>XC$xcid</a>";
            if (!$this->restrictedForUser(User::current())) {
                $xcidText .= "
                    <a href='{$this->downloadURL()}'>
                    <img src='/static/img/download.png' width='14' height='14' 
                    title='" . sprintf(
                        _('Download file %s'),
                        htmlspecialchars("'{$this->suggestedFileName()}'", ENT_QUOTES)
                    ) . "'>
                    </a>";
            }
            $xcidText .= '</td>';

            $license = License::lookupById($this->license());
            $xcidText .= "<td class='licenses'><a href='{$license->url}' title='{$license->name}'><img width='14' height='14' src='/static/img/cc.png'/>";
            for ($i = 0; $i < count($license->attrs); $i++) {
                $img = 'cc-' . strtolower($license->attrs[$i]) . '.png';
                $xcidText .= " <img width='14' height='14' src='/static/img/$img' />";
            }
            $xcidText .= '</a></td></table></div>';
        }

        if ($showSpecies) {
            // FIXME: ENVREC
            if ($this->isPseudoSpecies()) {
                $altname = $this->commonName();
                $titlename = $this->commonName();
            } else {
                $altname = "{$this->commonName()} {$this->fullScientificName()}";
                $titlename = $this->htmlDisplayName($linkFields);
            }
            $alttext = htmlspecialchars("$altname - {$this->soundType()}", ENT_QUOTES);
            $titleText = "<div class='jp-title' title='$alttext'>$titlename";
            if ($this->soundType()) {
                $titleText .= " &middot; <span class='jp-xc-call-type'>{$this->soundType()}</span>";
            }
            $titleText .= "</div>";
        }

        $recordistText = '';
        if ($showRecordist) {
            if ($linkFields) {
                $rec = "<a href='" . getUrl('recordist', ['id' => $this->recordistID()]) . "'>{$this->recordist()}</a>";
            } else {
                $rec = $this->recordist();
            }
            $recordistText .= "<div class='jp-xc-recordist'>$rec</div>";
        }
        $recordingDate = '';
        if ($showDate) {
            $recordingDate = "<div class='jp-xc-date'>{$this->date()}</div>";
        }

        $location = '';
        if ($showLocation) {
            if ($linkFields && $this->hasCoordinates()) {
                $loc = "<a href='{$this->locationURL()}'>{$this->location()}, {$this->country()}</a>";
            } else {
                $loc = "{$this->location()}, {$this->country()}";
            }
            $alttext = htmlspecialchars(
                "{$this->location()}, {$this->country()}",
                ENT_QUOTES
            );
            $location = "<div class='jp-xc-location' title='$alttext'>$loc</div>";
        }

        if (!$slowPlayer) {
            $playerUrl = $this->playerUrl();
            $lengthString = $this->lengthString();
        } else {
            $playerUrl = $this->playerUrl($this->slowFactor());
            $lengthString = $this->lengthString($this->slowFactor());
        }

        $slowFactorText = '';
        if ($slowPlayer) {
            $factor = sprintf(_('Speed %sx decreased'), $this->slowFactor());
            $slowFactorText = "
                <p class='important'>" . _(
                    'A slowed-down version is provided for high-frequency recordings of this species.'
                ) . "</p>
                <div class='jp-xc-slow-down'>$factor</div>";
        }

        $attrFilepath = htmlspecialchars($playerUrl, ENT_QUOTES);
        $controls = <<<EOT
        <div class="jp-gui jp-interface">
            <ul class="jp-controls">
                <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
                <li><a href="javascript:;" class="jp-pause" style="display:none;" tabindex="1">pause</a></li>
                <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
            </ul>
            <div class="jp-progress">
                <div class="jp-seek-bar">
                    <div class="jp-play-bar">
                    </div>
                </div>
            </div>
            <div class="jp-time-holder">
                <div class="jp-current-time"></div>
                <div class="jp-xc-duration">$lengthString</div>
            </div>
        </div>
        EOT;

        $player = "<div class='xc-audio'>
            <div";

        if (!$this->restrictedForUser(User::current())) {
            $player .= " id='$controlsId' class='jp-type-single' data-xc-filepath='$attrFilepath' data-xc-id='$xcid'" . " data-xc-factor='{$this->slowFactor()}'";
        }

        $player .= ">
            $slowFactorText
            $xcidText
            $sonoAxis
            $sonoText";
        if (!$this->restrictedForUser(User::current())) {
            $player .= $controls;
        }

        $player .= "
            $titleText
            $recordistText
            $location
            $recordingDate
            </div>
        </div>";

        return $player;
    }

    public function sonoURL($size = SonoSize::SMALL, $type = SonoType::SPECTROGRAM)
    {
        return (REMOTE_BASE_URL ?: "//{$_SERVER['HTTP_HOST']}/") . $this->sonoPath($size, $type);
    }

    public function sonoPath($size = SonoSize::SMALL, $type = SonoType::SPECTROGRAM)
    {
        return "sounds/uploaded/{$this->m_row->dir}/$type/{$this->sonoFilename($size)}";
    }

    public function sonoFilename($size = SonoSize::SMALL)
    {
        return "XC{$this->xcid()}-$size.png";
    }

    public function fullScientificName()
    {
        return $this->species()->fullScientificName();
    }

    public function isPseudoSpecies()
    {
        return in_array($this->m_row->species_nr, Species::pseudoSpecies(), true);
    }

    public function recordist()
    {
        return $this->m_row->recordist;
    }

    public function URL($absolute = false)
    {
        return getUrl('recording', ['xcid' => $this->xcid()], $absolute);
    }

    public function downloadURL($absolute = false)
    {
        return getUrl('download', ['XC' => $this->xcid()], $absolute);
    }

    public function suggestedFileName()
    {
        $fn = "XC{$this->xcid()}";
        if ($this->commonName()) {
            $fn .= " - {$this->commonName()}";
        }
        if (!$this->isPseudoSpecies()) {
            $fn .= " - {$this->fullScientificName()}";
        }
        $fn .= '.' . strtolower(pathinfo($this->filename(), PATHINFO_EXTENSION));
        return $fn;
    }

    public function filename()
    {
        return $this->m_row->path;
    }

    public function license()
    {
        return $this->m_row->license;
    }

    public function htmlDisplayName($link = true)
    {
        $html = $this->species()->htmlDisplayName($link);

        if ($this->status() == ThreadType::ID_QUESTIONED || $this->status(
            ) == ThreadType::ID_UNCONFIRMED || $this->status() == ThreadType::MYSTERY) {
            $html = "<span title='" . htmlspecialchars(_('ID under discussion'), ENT_QUOTES) . "'>(?)</span> " . $html;
        }

        if ($this->species()->isMystery()) {
            $html .= " ({$this->groupNameSingular()})";
        }

        return $html;
    }

    public function status()
    {
        return intval($this->m_row->discussed);
    }

    public function groupNameSingular()
    {
        return rtrim($this->groupName(), 's');
    }

    public function groupName()
    {
        if (!$this->group) {
            $this->setGroupProperties();
        }
        return $this->group;
    }

    private function setGroupProperties()
    {
        $this->slowFactor = $this->group = null;

        if (!empty($this->groupId())) {
            $res = query_db('SELECT `name`, `slow_factor` FROM `groups` WHERE id = ' . $this->groupId());
            $row = $res->fetch_object();
            $this->slowFactor = $row->slow_factor;
            $this->group = $row->name;
        }
    }

    public function groupId()
    {
        return $this->m_row->group_id;
    }

    public function soundType()
    {
        return $this->getSoundPropertiesByCategory('sound type', $this->soundTypeExtra());
    }

    private function getSoundPropertiesByCategory($category, $extra = null)
    {
        $types = [];
        foreach ($this->soundProperties() as $prop) {
            if ($prop->category_id == $this->soundCategoryId($category)) {
                $types[] = _($prop->property);
            }
        }
        if ($extra) {
            $types[] = $extra;
        }
        return implode(', ', array_filter($types));
    }

    public function soundProperties()
    {
        if (!is_array($this->soundProperties)) {
            $res = query_db(
                'SELECT `property_id`, `category_id`, `property` 
                FROM `birdsounds_properties` 
                WHERE `snd_nr` = ' . ($this->xcid() ?? -1) . '
                ORDER BY `category_id`, `property`'
            );
            while ($row = $res->fetch_object()) {
                $properties[] = $row;
            }
            $this->soundProperties = $properties ?? [];
        }
        return $this->soundProperties;
    }

    public function soundCategoryId($category)
    {
        foreach ($this->soundPropertyCategories() as $cat) {
            if ($cat->category == $category) {
                return $cat->id;
            }
        }
        return null;
    }

    public function soundPropertyCategories()
    {
        if (!$this->soundPropertyCategories) {
            $res = query_db('SELECT `id`, `category` FROM sound_property_categories ORDER BY `sort_order`');
            while ($row = $res->fetch_object()) {
                $this->soundPropertyCategories[] = $row;
            }
        }
        return $this->soundPropertyCategories;
    }

    public function soundTypeExtra()
    {
        return implode(', ', array_map('trim', explode(',', $this->m_row->songtype)));
    }

    public function date()
    {
        return $this->m_row->date;
    }

    public function hasCoordinates()
    {
        return (!(is_null($this->m_row->longitude) || is_null($this->m_row->latitude)));
    }

    public function locationURL()
    {
        return getUrl('location-map', [
            'lat' => $this->latitude(),
            'long' => $this->longitude(),
            'loc' => $this->location(),
        ]);
    }

    public function latitude()
    {
        return $this->m_row->latitude;
    }

    public function longitude()
    {
        return $this->m_row->longitude;
    }

    public function location()
    {
        return $this->m_row->location;
    }

    public function country()
    {
        return $this->m_row->country;
    }

    public function lengthString($slowFactor = 1)
    {
        return XC_formatDuration($this->length() * $slowFactor);
    }

    public function length()
    {
        return $this->m_row->length;
    }

    public function slowFactor()
    {
        if (!$this->slowFactor) {
            $this->setGroupProperties();
        }
        return $this->slowFactor;
    }

    public function soundTypePredefined()
    {
        return $this->getSoundPropertiesByCategory('sound type');
    }

    public function isMystery()
    {
        return $this->m_row->species_nr === Species::mysterySpeciesNumber();
    }

    public function mimeType()
    {
        if ($this->fileType() == 'wav') {
            return 'audio/wav';
        }
        return 'audio/mpeg';
    }

    public function lifeStage()
    {
        return $this->getSoundPropertiesByCategory('life stage');
    }

    public function sex()
    {
        return $this->getSoundPropertiesByCategory('sex');
    }

    public function recordingMethod()
    {
        return $this->getSoundPropertiesByCategory('recording method');
    }

    public function formatRemarks()
    {
        $rmk = '';
        $processed = XC_processRemarks($this->remarks());
        if ($processed[0] || !empty($processed[1])) {
            $actions = join(' ', $processed[1]);
            $rmk .= $processed[0];
            if ($actions) {
                $rmk .= "<p>$actions</p>";
            }
        }

        if ($rmk) {
            $formatter = new SeekFormatter($this);
            $rmk = preg_replace_callback(
                '/(\d+)\:(\d+(\.\d+)?)/',
                [
                    $formatter,
                    'formatPlayLink',
                ],
                $rmk
            );
        }
        return $rmk;
    }

    public function remarks()
    {
        return $this->m_row->remarks;
    }

    public function soundPropertiesAsEntered()
    {
        return [$this->groupId() => array_column($this->soundProperties(), 'property_id')];
    }

    public function soundTypeHistory()
    {
        // The field form_data is not normally present in a generic Recording object, as it contains a
        // lot of data that is used only to track changes. You *MUST* first explicitly use setFormData
        // on the recording to retrieve this data for a generic Recording!
        if (!empty($this->formData())) {
            $form = json_decode($this->formData());
            $types = [];
            $propIds = $form->soundProperties;

            if (!empty($propIds)) {
                $query = sprintf(
                    "
                        SELECT property FROM sound_properties 
                        WHERE id IN (%s) AND category_id = %d
                        ORDER BY property",
                    implode(',', $propIds),
                    $this->soundCategoryId('sound type')
                );
                $res = query_db($query);
                while ($row = $res->fetch_object()) {
                    $types[] = _($row->property);
                }
            }
            if (!empty($this->m_row->songtype)) {
                $types[] = $this->m_row->songtype;
            }
            return implode(', ', array_filter($types));
        } else {
            return $this->m_row->songtype;
        }
    }

    public function formData()
    {
        return $this->m_row->form_data ?? null;
    }

    public function setFormData()
    {
        if (!isset($this->m_row->form_data)) {
            $row = query_db("SELECT form_data FROM  birdsounds WHERE snd_nr = " . $this->xcid())->fetch_object();
            $this->m_row->form_data = $row->form_data;
        }
    }

    public function staticMap($width, $height)
    {
        if (!$this->hasCoordinates()) {
            return '';
        }

        return "<a href='{$this->locationURL()}'>
            <img class='recording-map' src='//maps.google.com/staticmap?center={$this->latitude()},{$this->longitude()}&amp;zoom=7&amp;size={$width}x{$height}&amp;maptype=terrain&amp;markers={$this->latitude()},{$this->longitude()},red&amp;key=" . getEnv(
                'GOOGLE_MAPS_API_KEY'
            ) . "'/>
            </a>";
    }

    public function genus()
    {
        return $this->species()->genus();
    }

    public function family()
    {
        return $this->species()->family();
    }

    public function speciesName()
    {
        return $this->species()->species();
    }

    public function speciesNumber()
    {
        return $this->species()->speciesNumber();
    }

    public function scientificName()
    {
        return $this->species()->scientificName();
    }

    public function speciesURL()
    {
        return $this->species()->profileURL();
    }

    public function subspeciesURL()
    {
        return $this->species()->subspeciesURL();
    }

    public function genusURL()
    {
        return $this->species()->genusURL();
    }

    public function lastModifiedDateString()
    {
        $res = query_db(
            '
            SELECT `timestamp` AS last_modified 
            FROM `birdsounds_history`   
            WHERE `snd_nr` = ' . $this->xcid() . '
            ORDER BY `historyid` DESC LIMIT 1'
        );
        if ($res->num_rows == 1) {
            $row = $res->fetch_object();
            return date('Y-m-d', strtotime($row->last_modified));
        }
        return null;
    }

    public function uploadDateString()
    {
        return date('Y-m-d', $this->uploadDate());
    }

    public function uploadDate()
    {
        return strtotime($this->m_row->datetime);
    }

    public function time()
    {
        return $this->m_row->time;
    }

    public function temperature()
    {
        return $this->m_row->temperature;
    }

    public function elevation()
    {
        return $this->m_row->elevation;
    }

    public function device()
    {
        return $this->m_row->device;
    }

    public function microphone()
    {
        return $this->m_row->microphone;
    }

    public function subspecies()
    {
        return $this->m_row->ssp;
    }

    public function specimen()
    {
        return $this->m_row->collection_specimen;
    }

    public function collectionDate()
    {
        return $this->m_row->collection_date;
    }

    public function animalSeen()
    {
        return $this->m_row->observed == 1 ? _('yes') : ($this->m_row->observed == 0 ? _('no') : _('unknown'));
    }

    public function automatic()
    {
        return $this->m_row->automatic == 1 ? _('yes') : ($this->m_row->automatic == 0 ? _('no') : _('unknown'));
    }

    public function playbackUsed()
    {
        return $this->m_row->playback == 1 ? _('yes') : ($this->m_row->playback == 0 ? _('no') : _('unknown'));
    }

    public function hasDiscussion()
    {
        return $this->m_row->discussed != 0;
    }

    public function hasBackground()
    {
        return !empty($this->backgroundSpecies());
    }

    public function backgroundSpecies()
    {
        if (!is_array($this->backgroundSpecies)) {
            foreach (array_column($this->backgroundSpeciesAll(), 'species_nr') as $nr) {
                $species = Species::load($nr);
                if (!$species->restrictedForUser()) {
                    $background[] = $species;
                }
            }
            $this->backgroundSpecies = $background ?? [];
        }
        return $this->backgroundSpecies;
    }

    private function backgroundSpeciesAll()
    {
        if (!is_array($this->backgroundSpeciesAll)) {
            $res = query_db(
                'SELECT `species_nr`, `scientific`, `english`, `family` 
                FROM `birdsounds_background` 
                WHERE `snd_nr` = ' . ($this->xcid() ?? -1)
            );
            while ($row = $res->fetch_object()) {
                $background[] = $row;
            }
            $this->backgroundSpeciesAll = $background ?? [];
        }
        return $this->backgroundSpeciesAll;
    }

    public function backgroundAsEntered()
    {
        // This should return ALL data for update form, user automatically has rights
        $bgString = '';
        if ($this->backgroundSpeciesAll()) {
            $bgString = implode(',', $this->backgroundScientific());
        }
        if ($this->m_row->back_extra) {
            $bgString = ($bgString ? $bgString . ',' : '') . $this->m_row->back_extra;
        }
        return $bgString;
    }

    public function backgroundScientific()
    {
        return array_column($this->backgroundSpeciesAll(), 'scientific');
    }

    public function backgroundExtra()
    {
        if (trim($this->m_row->back_extra)) {
            return explode(',', $this->m_row->back_extra);
        }
        return [];
    }

    public function ratingString()
    {
        return self::$qualityString[$this->rating()];
    }

    public function rating()
    {
        return intval($this->m_row->quality);
    }

    public function soundCharacteristics()
    {
        return [
            'length' => (int)$this->m_row->length_snd,
            'speed' => (int)$this->m_row->speed_snd,
            'pitch' => (int)$this->m_row->pitch_snd,
            'volume' => (int)$this->m_row->volume_snd,
            'numnotes' => (int)$this->m_row->number_notes_snd,
            'variable' => (int)$this->m_row->variable_snd,
        ];
    }

    public function debugString()
    {
        return "XC{$this->xcid()} {$this->commonName()} ({$this->fullScientificName()}) by {$this->recordist()} ({$this->recordistID()}) from {$this->location()}, {$this->country()} on {$this->date()} {$this->filePath()}";
    }

    public function isSoundscape()
    {
        return $this->m_row->species_nr === Species::soundscapeSpeciesNumber();
    }

    public function updateStatus($status)
    {
        XC_updateRecordingStatus($this->xcid(), $status);
    }

}

// ugly, but whatever...
Recording::$counter = rand(0, 999);

function XC_recordingURL($xcid, $absolute = false)
{
    return getUrl('recording', ['xcid' => $xcid], $absolute);
}

function XC_updateRecordingStatus($xcid, $status)
{
    if (!$xcid) {
        return false;
    }

    $status = intval($status);
    $xcid = intval($xcid);
    $res = query_db("select topic_nr from forum_world where snd_nr = $xcid");
    $row = $res->fetch_object();

    if (!empty($row->topic_nr)) {
        query_db("update forum_world set type = $status where snd_nr = $xcid");
        query_db('delete from forum_topic_thread_types where topic_nr = ' . $row->topic_nr);
        $res = query_db("update birdsounds SET discussed = $status WHERE snd_nr = $xcid");
        (new XCRedis())->clearForumCache();
        return $res;
    }

    return false;
}
