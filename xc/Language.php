<?php

namespace xc;

class Language
{

    private static $localeMap;

    public $code;

    public $name;

    public $locale;

    public $localNameLanguage;

    public function __construct($code, $name, $locale, $localNameLanguage)
    {
        $this->code = $code;
        $this->name = $name;
        $this->locale = $locale;
        $this->localNameLanguage = $localNameLanguage;
    }

    public static function init()
    {
        self::$localeMap = [
            new Language('cs', 'Česky', '', 'czech'),
            new Language('da', 'Dansk', 'da_DK.UTF-8', 'danish'),
            new Language('de', 'Deutsch', 'de_DE.UTF-8', 'german'),
            new Language('et', 'Eesti', '', 'estonian'),
            new Language('en', 'English', '', 'english'),
            new Language('es', 'Español', 'es_ES.UTF-8', 'spanish'),
            new Language('fr', 'Français', 'fr_FR.UTF-8', 'french'),
            new Language('hu', 'Magyar', '', 'hungarian'),
            new Language('it', 'Italiano', '', 'italian'),
            new Language('lt', 'Lietuvių', '', 'lithuanian'),
            new Language('nl', 'Nederlands', 'nl_NL.UTF-8', 'dutch'),
            new Language('no', 'Norsk', 'nb_NO.UTF-8', 'norwegian'),
            new Language('pl', 'Polski', 'pl_PL.UTF-8', 'polish'),
            new Language('pt', 'Português', 'pt_PT.UTF-8', 'portuguese'),
            new Language('pt_BR', 'Português do Brasil', 'pt_BR.UTF-8', 'portuguese'),
            new Language('ru', 'Русский', 'ru_RU.UTF-8', 'russian'),
            new Language('si', 'Slovenčina', '', 'slovak'),
            new Language('fi', 'Suomi', '', 'finnish'),
            new Language('sv', 'Svenska', 'sv_SE.UTF-8', 'swedish'),
            new Language('ja', '日本語', '', 'japanese'),
            // Korean mapped to English; temporarily disabled
            // new Language("ko", "한국어", "ko_KR.UTF-8", "korean"),
            new Language('zh_CN', '简体中文', '', 'chinese'),
            new Language('zh_TW', '繁體中文', 'zh_TW.UTF-8', 'chinese_tw'),
            // New!
            new Language('af', 'Afrikaans', '', 'afrikaans'),
            new Language('ca', 'Català', '', 'catalan'),
            new Language('is', 'Íslenska', '', 'icelandic'),
            new Language('id', 'Bahasa Indonesia', '', 'indonesian'),
            new Language('lv', 'Latviešu', '', 'latvian'),
            new Language('se', 'Davvisámegiella', '', 'northern_sami'),
            new Language('sl', 'Slovenščina', '', 'slovenian'),
            new Language('th', 'ภาษาไทย', '', 'thai'),
            new Language('uk', 'Українська', '', 'ukranian'),
        ];
    }

    public static function all()
    {
        usort(
            self::$localeMap, function ($a, $b) {
            return strcmp($a->code, $b->code);
        }
        );
        return self::$localeMap;
    }

    public static function lookup($code)
    {
        // normalize codes to e.g. zh_TW instead of zh-TW
        $code = strtr($code, '-', '_');
        foreach (self::$localeMap as $item) {
            if ($item->code == $code) {
                return $item;
            }
        }

        // if it's a complex code (such as zh_TW or zh-TW, just use the initial
        // part)
        $clientparts = explode('_', $code);
        if (count($clientparts) > 1) {
            foreach (self::$localeMap as $item) {
                if ($item->code == $clientparts[0]) {
                    return $item;
                }
            }
        }

        // Last chance: match user-requested 'zh' with the first chinese variant
        // available on the server (e.g. zh_TW)
        foreach (self::$localeMap as $item) {
            $serverparts = explode('_', $item->code);
            if (count($serverparts) > 1) {
                if ($serverparts[0] == $clientparts[0]) {
                    return $item;
                }
            }
        }

        return null;
    }
}

Language::init();
