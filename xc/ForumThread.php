<?php

namespace xc;

class ForumThread
{

    private $m_nr;

    private $m_dbTable;

    private $m_dbKey;

    private $m_posts;

    public function __construct($nr, $dbtable = 'discussion_forum_world', $dbkey = 'topic_nr')
    {
        $this->m_nr = $nr;
        $this->m_dbTable = $dbtable;
        $this->m_dbKey = $dbkey;
    }

    public function nrReplies()
    {
        $nr = escape($this->m_nr);
        $sql = "SELECT COUNT(mes_nr) AS nr FROM $this->m_dbTable WHERE $this->m_dbKey = $nr";
        $res = query_db($sql);
        $row = $res->fetch_object();
        return max(intval($row->nr - 1), 0);
    }

    public function generateHTML()
    {
        $output = '';
        $posts = $this->getPosts();
        for ($i = 0; $i < count($posts); $i++) {
            $output .= $posts[$i]->generateHTML(($i + 1) == count($posts));
        }
        return $output;
    }

    public function getPosts()
    {
        if (empty($this->m_posts)) {
            $this->m_posts = [];
            $nr = escape($this->m_nr);
            $sql = "
                SELECT forum.*, permissions.*, users.email, 
                    stats.nrecordings AS nrecordings,
                    stats.ncomments AS ncomments
                FROM $this->m_dbTable AS forum
                LEFT JOIN users USING(dir)
                LEFT JOIN permissions ON dir = userid
                LEFT JOIN rec_summary_stats stats ON forum.dir = stats.userid AND stats.group_id = 0
                WHERE forum.$this->m_dbKey = $nr 
                GROUP BY mes_nr
                ORDER BY mes_nr ASC";
            $res = query_db($sql);
            while ($row = $res->fetch_object()) {
                $this->m_posts[] = new ForumPost($row, $this->m_dbTable);
            }
        }

        return $this->m_posts;
    }

}
