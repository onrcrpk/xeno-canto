<?php

namespace xc;

class AdminTemplate extends BaseTemplate
{

    public function __construct($request)
    {
        parent::__construct($request);
        $this->addCssInclude('/static/css/admin.css');
    }

    public function render($content, $args = [])
    {
        $defaultTitle = 'Administrator Dashboard';
        if (!array_key_exists('bodyId', $args)) {
            $args['bodyId'] = 'admin';
        }
        if (array_key_exists('title', $args)) {
            $title = $args['title'];

            $content = "
                <ul class='breadcrumbs'>
                <li><a href='" . getUrl('admin-index') . "'>$defaultTitle</a></li>
                <li class='current'>$title</li>
                </ul>
                $content";
        } else {
            $args['title'] = $defaultTitle;
        }

        return parent::render($content, $args);
    }
}
