<?php

namespace xc;

abstract class Template
{

    abstract public function render($content, $params = []);
}
