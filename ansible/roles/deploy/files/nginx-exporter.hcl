listen {
  port = 4040
}

namespace nginx {

  source = {
    syslog {
      listen_address = "udp://0.0.0.0:5531"
      format = "auto"
      tags = ["nginx"]
    }
  }

  parser = "json"

  print_log = true
}
