#!/bin/bash
export_dir="/data/minio/nba"

echo "$(date) - NBA export started"
/usr/local/bin/docker-compose \
    --env-file /opt/compose_projects/xeno-canto/.env \
    --file /opt/compose_projects/xeno-canto/docker-compose.yml \
    --file /opt/compose_projects/xeno-canto/docker-compose.override.yml \
    run --rm -v "${export_dir}":/export -u root php-fpm \
    /usr/local/bin/php /var/www/tasks/nba-export.php /export
