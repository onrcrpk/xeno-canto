#!/bin/sh
set -eu

docker pull hashicorp/vault:latest
# Get a vault token
VAULT_TOKEN=$(docker run --cap-add=IPC_LOCK hashicorp/vault:latest vault write -address="$VAULT_SERVER_URL" -field=token auth/jwt/login role="$VAULT_AUTH_ROLE" jwt="$CI_JOB_JWT")
export VAULT_TOKEN

# Retrieve hosts inventory
FULL_SECRET_PATH="kv2/$VAULT_SECRETS_PATH/hosts"
docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault:latest vault kv get -field data --format=yaml -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH" > "ansible/inventory/hosts.yml"

# Retrieve all group_vars
FULL_SECRET_PATH="kv2/$VAULT_SECRETS_PATH/group_vars"
CONFIGS=$(docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault:latest vault kv list -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH" | tail -n +3)
for config in $CONFIGS; do
    docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault:latest vault kv get -field data --format=yaml -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH/${config}" > "ansible/inventory/group_vars/${config}.yml"
done

# Retrieve all host_vars, remove comments '#' when hosts_vars are needed
FULL_SECRET_PATH="kv2/$VAULT_SECRETS_PATH/host_vars"
CONFIGS=$(docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault:latest vault kv list -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH" | tail -n +3)
for config in $CONFIGS; do
    docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault:latest vault kv get -field data --format=yaml -address="$VAULT_SERVER_URL" "$FULL_SECRET_PATH/${config}" > "ansible/inventory/host_vars/${config}.yml"
done
