<?php

define('ENVIRONMENT', getenv('ENVIRONMENT') ?: 'prod');
define('MAX_EXPORT_RECORDS', (getenv('MAX_EXPORT_RECORDS')) ?: 1000);
define('SONOGEN_PATH', getenv('SONOGEN_PATH') ?: '/usr/local/bin/sonogen');
define('AUDIOWAVEFORM_PATH', getenv('AUDIOWAVEFORM_PATH') ?: '/usr/local/bin/audiowaveform');
define('SOX_PATH', getenv('SOX_PATH') ?: '/usr/bin/sox');

define('WEBMASTER_EMAIL', getenv('WEBMASTER_EMAIL') ?: 'webmaster@xeno-canto.org');
define('CONTACT_EMAIL', getenv('CONTACT_EMAIL') ?: 'contact@xeno-canto.org');
define('ADMINS_EMAIL', getenv('ADMINS_EMAIL') ?: 'wp@xeno-canto.org,bob@xeno-canto.org');
define('REROUTE_EMAIL', getenv('REROUTE_EMAIL') ?: 'contact-dev@xeno-canto.org');
define('MAILER_DSN', getenv('MAILER_DSN') ?: '');

define('MINIO_BATCH', getenv('MINIO_BATCH') ?: '/data/minio/batch');
define('MINIO_RANGES', getenv('MINIO_RANGES') ?: '/data/minio/ranges');
define('MINIO_DOCS', getenv('MINIO_DOCS') ?: '/data/minio/docs');
define('MINIO_DWCA', getenv('MINIO_DWCA') ?: '/data/minio/dwca');
define(
    'MINIO_CLASSIFICATION',
    getenv('MINIO_CLASSIFICATION') ?: '/data/minio/classification'
);
define('MINIO_NBA', getenv('MINIO_NBA') ?: '/data/minio/nba');

define('NBA_EXPORT_ENDPOINT', getenv('NBA_EXPORT_ENDPOINT') ?: '');
define('NBA_EXPORT_REGION', getenv('NBA_EXPORT_REGION') ?: 'eu-central-1');
define('NBA_EXPORT_VERSION', getenv('NBA_EXPORT_VERSION') ?: 'latest');
define('NBA_EXPORT_KEY', getenv('NBA_EXPORT_KEY') ?: '');
define('NBA_EXPORT_SECRET', getenv('NBA_EXPORT_SECRET') ?: '');

define(
    'GOOGLE_ANALYTICS_TRACKING_ID',
    getenv('GOOGLE_ANALYTICS_TRACKING_ID') ?: false
);
define(
    'GOOGLE_TAG_MANAGER_TRACKING_ID',
    getenv('GOOGLE_TAG_MANAGER_TRACKING_ID') ?: false
);
define('GOOGLE_MAPS_API_KEY', getenv('GOOGLE_MAPS_API_KEY') ?: '');
define('GOOGLE_MAPS_GEOCODING_KEY', getenv('GOOGLE_MAPS_GEOCODING_KEY') ?: '');

define('RECAPTCHA_SITE_KEY', getenv('RECAPTCHA_SITE_KEY') ?: false);
define('RECAPTCHA_SECRET_KEY', getenv('RECAPTCHA_SECRET_KEY') ?: false);
define(
    'RECAPTCHA_VERIFY_URL',
    getenv(
        'RECAPTCHA_VERIFY_URL'
    ) ?: 'https://www.google.com/recaptcha/api/siteverify'
);

define('PAYPAL_SUCCESS_ID', 0);

define('REDIS_PASSWORD', getenv('REDIS_PASSWORD') ?: false);
define(
    'REMOTE_BASE_URL',
    getenv('REMOTE_BASE_URL') ?
        rtrim(getenv('REMOTE_BASE_URL'), '/') . '/' : false
);
define('IMAGE_VERSION', getenv('IMAGE_VERSION') ?: false);
