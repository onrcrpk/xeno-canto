<?php

// the main function to access the database.
use xc\ViewType;

use function xc\db;

function query_db($sql)
{
    return db()->query($sql);
}

function mysqli()
{
    return db()->getConnection();
}

function defaultResultsView()
{
    $user = xc\User::current();
    if ($user) {
        return $user->viewPreference();
    } else {
        return ViewType::DETAILED;
    }
}

function defaultNumPerPage()
{
    $user = xc\User::current();
    if ($user) {
        return $user->numResultsPerPage();
    } else {
        return 30;
    }
}

function localNameLanguage()
{
    $localNameLanguage = 'english';
    $lang = app()->determineBestLanguage();
    if ($lang) {
        $localNameLanguage = $lang->localNameLanguage;
    }
    return $localNameLanguage;
}

// returns a auto-incremented counter that is unique for this session
function nextSessionCounter()
{
    $i = app()->session()->get('counter', 0);
    $i++;
    app()->session()->set('counter', $i);

    return $i;
}
