<?php

namespace xc;

use getID3;
use Michelf\Markdown;

use function app;

require_once __DIR__ . '/basic_stuff.php';
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/constants.php';

class ViewType
{

    public const DETAILED = 0;

    public const CONCISE = 1;

    public const CODES = 2;

    public const SONOGRAMS = 3;

}

function XC_makeOrderURL($request, $field)
{
    $params = $request->query->all();
    $vars = [];
    if (isset($params['order']) and $params['order'] == $field) {
        if (isset($params['dir'])) {
            $vars['dir'] = !$params['dir'];
        } else {
            $vars['dir'] = SortDirection::REVERSE;
        }
    } else {
        $vars['dir'] = SortDirection::NORMAL;
    }
    $vars['order'] = $field;

    return XC_addUrlParamsToCurrentPage($request, $vars);
}


//======================
// the first row announcing the column names, only for detailed AND concise views
//======================
function SEARCH_topTableRow($request, $view)
{
    $toprow = '
    <thead>
      <tr>
      <th></th>
      ';

    // english (scientific) name
    $toprow .= "
        <th><a href=\"" . XC_makeOrderURL($request, 'en') . "\">" . _('Common name') . "</a> /
        <a href=\"" . XC_makeOrderURL($request, 'tax') . "\">" . _('Scientific') . "</a></th>
        <th><a href=\"" . XC_makeOrderURL($request, 'length') . "\" >" . _('Length') . "</a></th>
        <th><a href=\"" . XC_makeOrderURL($request, 'rec') . "\">" . _('Recordist') . '</a></th>';

    // date AND time
    if ($view == ViewType::DETAILED) {
        $toprow .= "
            <th><a href=\"" . XC_makeOrderURL($request, 'dt') . "\">" . _('Date') . '</a></th>';
        $toprow .= "
            <th><a href=\"" . XC_makeOrderURL($request, 'tm') . "\">" . _('Time') . '</a></th>';
    }

    // country, loc, elev
    $toprow .= "
       <th><a href=\"" . XC_makeOrderURL($request, 'cnt') . "\">" . _('Country') . '</a></th>';
    $toprow .= "
       <th><a href=\"" . XC_makeOrderURL($request, 'loc') . "\">" . _('Location') . '</a></th>';
    $toprow .= "
       <th><a href=\"" . XC_makeOrderURL($request, 'elev') . "\">" . _('Elev. (m)') . '</a></th>';

    // type, remarks, cat.nr
    if ($view == ViewType::DETAILED) {
        $toprow .= "
            <th>" . _('Type (predef. / other)') . '</th>
            <th>' . _('Remarks') . '</th>
            <th>' . _('Actions') . ' / <a href="' . XC_makeOrderURL($request, 'qual') . '">' . _(
                'Quality'
            ) . '</a></th>';
    }

    $toprow .= "
        <th><a href=\"" . XC_makeOrderURL($request, 'xc') . "\">" . _('Cat.nr.') . '</a></th>
        </tr>
        </thead>';

    return $toprow;
}


//===========================
// the basic function that makes one row of search results
// in Detailed or Concise views
//===========================
function SEARCH_resultsRowDetailedConcise(
    $r,
    $prev_recording,
    $cur,
    $view,
    $actionsCallback,
    $showLocations = true
) {
    if (empty($prev_recording) || ($r->commonName() == $prev_recording->commonName())) {
        $class = '';
    } else {
        $class = "class=\"new-species\"";
    }

    // FIXME: ENVREC
    $name = $r->htmlDisplayName();
    $lic = License::lookupById($r->license());

    $output = "
        <tr $class>
        <td>{$r->miniPlayer()}</td>
        <td>$name </td>
        <td> {$r->lengthString()}</td>
        <td> <a href='" . getUrl('recordist', ['id' => $r->recordistId()]) . "'>{$r->recordist()}</a></td>
        ";

    if ($view == ViewType::DETAILED) {
        $output .= "
        <td>{$r->date()}</td>
        <td> {$r->time()}</td>
      ";
    }
    $output .= "
        <td>{$r->country()}</td>";

    if (!$showLocations) {
        $output .= '<td>-</td>';
    } elseif ($r->hasCoordinates()) {
        $output .= "
        <td>
          <a href=\"{$r->locationURL()}\">{$r->location()}</a>
        </td>";
    } else {
        $output .= "
        <td>{$r->location()}</td>
        ";
    }

    $output .= "
        <td>{$r->elevation()}</td>
        ";

    if ($view == ViewType::DETAILED) {
        $output .= "<td>{$r->soundType()} ";
        $output .= '</td>
          ';

        // the following function is very big: it processes all the remarks, tags, drop down menus
        // to be displayed in the remarks column
        $rmk = SEARCH_remarksDetailedView($r);

        if (!empty($rmk)) {
            $output .= "
                <td>$rmk</td>
                ";
        }

        $output .= '<td>';

        if (!$r->species()->restricted()) {
            $output .= "<a href='{$r->downloadURL()}'><img class='icon' src='/static/img/download.png' width='14' height='14' ";
            $output .= " title='" . sprintf(
                    _('Download file %s'),
                    htmlspecialchars("'{$r->suggestedFilename()}'", ENT_QUOTES)
                ) . "'></a>";
        }

        $url = getUrl('new_thread', ['xcid' => $r->xcid()]);

        $output .= "<a class='tooltip' title='" . _(
                'Open a discussion, mail the recordist'
            ) . "' href='$url'><img class='icon' width='14' height='14' src='/static/img/discuss.png' alt='discuss icon'/></a>";

        if ($actionsCallback) {
            $output .= call_user_func($actionsCallback, $r);
        } else {
            $output .= defaultRecordingActions($r);
        }

        // score song links if user is logged
        $output .= XC_ratingWidget($r->xcid(), $r->rating());
        $output .= '</td>';
    }

    // the catalogue number
    $output .= "<td style='white-space: nowrap;'><a href=\"{$r->URL()}\">XC{$r->xcid()} <span title=\"{$lic->name}\"><a href=\"{$lic->url}\"><img class='icon' width='14' height='14' src='/static/img/cc.png'></a></span></td>
          ";

    $output .= '  
      </tr>
      ';

    return $output;
}


//=============================
// the song codes, only visible in codes view
//=============================
function SEARCH_ResultsCodeView($recording, $class)
{
    $characteristics = $recording->soundCharacteristics();
    $len = lengthToString($characteristics['length']);
    $vol = volumeToString($characteristics['volume']);
    $nn = numNotesToString($characteristics['numnotes']);
    $pitch = pitchToString($characteristics['pitch']);
    $var = variableToString($characteristics['variable']);
    $spd = speedToString($characteristics['speed']);

    return "
        <td class=\"$class\">$len</td>
        <td class=\"$class\">$vol</td>
        <td class=\"$class\">$nn</td>
        <td class=\"$class\">$pitch</td>
        <td class=\"$class\">$var</td>
        <td class=\"$class\">$spd</td>";
}


//========================
// the remarks
//========================

// returns an array: the first element is the remark stripped of all tagged
// links.  The second is an array of links found in the remark text
function XC_processRemarks($remarks)
{
    $ret = ['', []];
    if ($remarks) {
        // These functions will actually strip the special tags (e.g. link:foo)
        // from the remarks and retutn formatted html links

        // non-specific link
        $ret[1][] = SEARCH_generalLinkRemarks($remarks);
        // trip report
        $ret[1][] = SEARCH_tripReportLinkRemarks($remarks);
        // link to CD-ROM
        $ret[1][] = SEARCH_cdromLinkRemarks($remarks);
        // link to images of bird in recording
        $ret[1][] = SEARCH_imageLinkRemarks($remarks);

        // the tags have been stripped, so add it to the table
        $remarks = trim($remarks);
        if ($remarks) {
            $remarks = XC_formatUserText($remarks);
        }
        $ret[0] = $remarks;
    }
    return $ret;
}

function SEARCH_remarksDetailedView($r)
{
    // we add drop down boxes under various links in the
    // remarks. they are slowly collected below.
    $box_url = '';
    $rmk = '';

    $other = '';
    $remarks = $r->formatRemarks();

    if (!empty($remarks)) {
        $rmk .= "<div class='remarks readmore'>$remarks</div>";
    }

    // links to the forum
    $rmk .= SEARCH_linksToForum($r);

    // the [also] background box
    $rmk .= SEARCH_backgroundDropdownBox($r, $box_url);

    // the [sono] link
    $rmk .= SEARCH_sonoLinkRemarks($r);

    $rmk .= '</div>';

    return $rmk;
}


function SEARCH_extractLinks(&$remarks, $tag)
{
    $matches = [];
    $urls = [];
    if (preg_match_all("/$tag:(\S+)/", $remarks, $matches)) {
        for ($k = 0; $k < count($matches[1]); $k++) {
            $url = $matches[1][$k];
            if (!parse_url($url, PHP_URL_SCHEME)) {
                $url = "//$url";
            }
            $urls[] = $url;
        }
        $remarks = preg_replace("/$tag:\S+/", '', $remarks);
    }
    return $urls;
}

//======================
// link: tag in remarks
//======================
function SEARCH_generalLinkRemarks(&$remarks)
{
    $urls = SEARCH_extractLinks($remarks, 'link');
    $links = '';
    foreach ($urls as $url) {
        $links .= "<a href=\"$url\" target=\"_blank\" >[" . _(
                'more info'
            ) . ']</a> ';
    }
    return $links;
}


//======================
// trip: tag in remarks
//======================
function SEARCH_tripReportLinkRemarks(&$remarks)
{
    $urls = SEARCH_extractLinks($remarks, 'trip');
    $links = '';
    foreach ($urls as $url) {
        $links = "<a href=\"$url\" target=\"_blank\" >[" . _(
                'trip report'
            ) . ']</a>';
    }
    return $links;
}

//======================
// cd: tag in remarks
//======================
function SEARCH_cdromLinkRemarks(&$remarks)
{
    $urls = SEARCH_extractLinks($remarks, 'cd');
    $links = '';
    foreach ($urls as $url) {
        $links = "<a href=\"$url\" target=\"_blank\" >[CD-ROM]</a>";
    }
    return $links;
}

//=====================
// img: tag in remarks
//=====================
function SEARCH_imageLinkRemarks(&$remarks)
{
    $urls = SEARCH_extractLinks($remarks, 'img');
    $links = '';
    foreach ($urls as $url) {
        $links = "<a href=\"$url\" target=\"_blank\" >[img]</a>";
    }
    return $links;
}

//==================
// links to the forum
//==================
function SEARCH_linksToForum($r)
{
    $nr = $r->xcid();
    $status = $r->status();
    $rmk = '';
    // links to the forum
    if ($status !== ThreadType::NONE) {
        $res = query_db(
            "select type, topic_nr from forum_world where snd_nr = $nr"
        );
        $row = $res->fetch_object();

        // this shouldn't happen, but...
        if (!$row) {
            return;
        }

        $forumLink = "<a href='" . getUrl('discuss_forum', ['topic_nr' => $row->topic_nr]) . "'>" . _(
                'See the forum'
            ) . '</a>.';

        if ($status == ThreadType::ID_QUESTIONED) {
            $rmk .= '<strong>' . _(
                    'ID under discussion'
                ) . "</strong>. $forumLink";
        } elseif ($status == ThreadType::ID_RESOLVED) {
            $rmk .= '<strong>' . _(
                    'ID has been discussed and resolved'
                ) . "</strong>. $forumLink";
        } elseif ($status == ThreadType::MYSTERY) {
            $rmk .= '<strong>' . _(
                    'Mystery recording'
                ) . "</strong>. $forumLink";
        } elseif ($status == ThreadType::MYSTERY_RESOLVED) {
            $rmk .= '<strong>' . _(
                    'Originally a mystery recording'
                ) . "</strong>. $forumLink";
        } elseif ($status == ThreadType::ID_UNCONFIRMED) {
            $rmk .= '<strong>' . _(
                    'ID requires confirmation'
                ) . "</strong>. $forumLink";
        } else {
            $rmk .= '' . _(
                    'Recording (not its ID) has been discussed'
                ) . ". $forumLink";
        }
        return "<p>$rmk</p>";
    }
    return '';
}

//===================
// the [also] drop down box with background species
//===================
function SEARCH_backgroundDropdownBox($recording, $box_url)
{
    $html = '';
    // the [also] background box
    $list = htmlspecialchars(XC_getBackgroundList($recording), ENT_QUOTES);

    if ($list != '') {
        $html .= " <span class=\"tooltip\" data-qtip-header=\"" . _(
                'Other Species'
            ) . "\"
            data-qtip-content=\"$list\" >[" . _('also') . ']</span>
            ';
    }
    return $html;
}

function XC_getBackgroundList($recording)
{
    $html = '';
    $bgspecies = $recording->backgroundSpecies();
    $extra = $recording->backgroundExtra();

    if ($bgspecies || $extra) {
        $html .= '<ul>';
        for ($i = 0; $i < count($bgspecies); $i++) {
            $html .= "<li><a href='{$bgspecies[$i]->profileURL()}'>" . $bgspecies[$i]->commonName(
                ) . '</a> (' . $bgspecies[$i]->scientificName() . ')</li>';
        }
        for ($i = 0; $i < count($extra); $i++) {
            $html .= '<li>' . $extra[$i] . '</li>';
        }
        $html .= '</ul>';
    }

    return $html;
}

//=================
// the [sono] link in the remarks
//=================
function SEARCH_sonoLinkRemarks($rec)
{
    $url = $rec->sonoURL(SonoSize::LARGE);
    $name = $rec->commonName();
    // FIXME: ENVREC
    if (!$rec->isPseudoSpecies()) {
        $name .= " ({$rec->fullScientificName()})";
    }
    $sonoTitle = htmlspecialchars(
        "XC{$rec->xcid()}: $name by {$rec->recordist()}",
        ENT_QUOTES
    );
    return "<a class='fancybox' rel='sono' href='$url' title='$sonoTitle'>[" . _(
            'sono'
        ) . ']</a>';
}


function XC_makeViewLinks($request, $view)
{
    $detailedLink = XC_addUrlParamsToCurrentPage($request, ['view' => ViewType::DETAILED]);
    $conciseLink = XC_addUrlParamsToCurrentPage($request, ['view' => ViewType::CONCISE]);
    $sonogramsLink = XC_addUrlParamsToCurrentPage($request, ['view' => ViewType::SONOGRAMS]);
    $detailed = _('detailed');
    $concise = _('concise');
    $codes = _('codes');
    $sonograms = _('sonograms');

    if ($view == ViewType::DETAILED || is_null($view)) {
        return "
            <ul>
            <li>$detailed</li>
            <li><a href=\"$conciseLink\">$concise</a></li>
            <li><a href=\"$sonogramsLink\">$sonograms</a></li>
            </ul>
            ";
    }
    if ($view == ViewType::CONCISE) {
        return "
            <ul>
            <li><a href=\"$detailedLink\">$detailed</a></li>
            <li>$concise</li>
            <li><a href=\"$sonogramsLink\">$sonograms</a></li>
            </ul>
            ";
    }
    if ($view == ViewType::SONOGRAMS) {
        return "
            <ul>
            <li><a href=\"$detailedLink\">$detailed</a></li>
            <li><a href=\"$conciseLink\">$concise</a></li>
            <li>$sonograms</li>
            </ul>
            ";
    }
}

// Format text to show a summary of the search results
function XC_resultsSummary(
    $number_of_hits,
    $num_species,
    $start_no_shown,
    $end_no_shown,
    $query
) {
    $summary = '';
    if (empty($query)) {
        $summary .= sprintf(
            ngettext(
                '%d result from %d species',
                '%d results from %d species',
                intval($number_of_hits)
            ),
            $number_of_hits,
            $num_species
        );
    } else {
        $summary .= sprintf(
            ngettext(
                "%d result from %d species for query '%s'",
                "%d results from %d species for query '%s'",
                intval($number_of_hits)
            ),
            $number_of_hits,
            $num_species,
            "<span class='search-term'>" . sanitize($query) . '</span>'
        );
    }

    $summary .= ' (' . _('foreground species only') . ") </p>";

    return $summary;
}

function defaultRecordingActions($rec)
{
    $user = User::current();
    $output = '';
    if ($user) {
        if ($user->isAdmin() || ($rec->recordistID() == $user->userId())) {
            $output .= "<a href='" . getUrl('revise', ['snd_nr' => $rec->xcid()]) . "' title='" . _(
                    'Revise recording'
                ) . "'><img class='icon' width='14' height='14' src='/static/img/edit.png' /></a>
                <a href='" . getUrl('delete', ['snd_nr' => $rec->xcid()]) . "' title='" . _(
                    'Delete recording'
                ) . "'><img class='icon' width='14' height='14' src='/static/img/delete.png' /></a>";
        }
        $output .= "<a href='" . getUrl('recording-add-to-set', ['xcid' => $rec->xcid()]) . "' title='" . _(
                'Add to set'
            ) . "'><img class='icon' width='14' height='14' src='/static/img/bookmark-add.png' /></a>";
    }
    return $output;
}

function XC_resultsTableForView(
    $request,
    $results,
    $view,
    $actionsCallback = null,
    $showLocations = true
) {
    $tableClass = 'results';
    $view = intval($view);

    if ($view == ViewType::SONOGRAMS) {
        $tableClass = 'sonos';
    }

    $cur = $request->getBaseUrl();

    $output = '';

    // the detailed and concise views
    if ($view != ViewType::SONOGRAMS) {
        $output .= "<table class=\"$tableClass\">";
        $output .= SEARCH_topTableRow($request, $view);
    } else {
        $taxonomySelected = '';
        $englishSelected = '';
        $recordistSelected = '';
        $dateSelected = '';
        $timeSelected = '';
        $countrySelected = '';
        $locationSelected = '';
        $elevationSelected = '';
        $appearanceSelected = '';
        $lengthSelected = '';
        $qualitySelected = '';

        switch ($request->query->get('order')) {
            case 'english':
                $englishSelected = 'selected';
                break;
            case 'rec':
            case 'recordist':
                $recordistSelected = 'selected';
                break;
            case 'dt':
            case 'date':
                $dateSelected = 'selected';
                break;
            case 'tm':
            case 'time':
                $timeSelected = 'selected';
                break;
            case 'cnt':
            case 'country':
                $countrySelected = 'selected';
                break;
            case 'loc':
            case 'location':
                $locationSelected = 'selected';
                break;
            case 'elev':
            case 'elevation':
                $elevationSelected = 'selected';
                break;
            case 'xc':
            case 'appearance':
                $appearanceSelected = 'selected';
                break;
            case 'length':
                $lengthSelected = 'selected';
                break;
            case 'qual':
                $qualitySelected = 'selected';
                break;
            default:
                $taxonomySelected = 'selected';
                break;
        }

        $output .= "
            <form class='sort-order-form' method='get'>" . _('Sort by') . ': ';

        $output .= "<select name='order'>
            <option value='tax' $taxonomySelected>" . _('Taxonomy') . "</option>
            <option value='en' $englishSelected>" . _('Common name') . "</option>
            <option value='length' $lengthSelected>" . _('Length') . "</option>
            <option value='rec' $recordistSelected>" . _('Recordist') . "</option>
            <option value='dt' $dateSelected>" . _('Date') . "</option>
            <option value='tm' $timeSelected>" . _('Time') . "</option>
            <option value='cnt' $countrySelected>" . _('Country') . '</option>';
        if ($showLocations) {
            $output .= "
                <option value='loc' $locationSelected>" . _('Location Name') . '</option>';
        }
        $output .= "
            <option value='elev' $elevationSelected>" . _('Elevation') . "</option>
            <option value='qual' $qualitySelected>" . _('Quality') . "</option>
            <option value='xc' $appearanceSelected>" . _('Catalog Number') . '</option>
            </select>';

        $normalSelected = '';
        $reverseSelected = '';
        if (intval($request->query->get('dir')) === SortDirection::REVERSE) {
            $reverseSelected = 'selected';
        } else {
            $normalSelected = 'selected';
        }

        $output .= "
            <select name='dir'>
                <option value='" . SortDirection::NORMAL . "' $normalSelected>" . _('Normal') . "</option>
                <option value='" . SortDirection::REVERSE . "' $reverseSelected>" . _('Reverse') . '</option>
            </select>';
        foreach ($_GET as $key => $val) {
            if ($key != 'order' && $key != 'dir') {
                $output .= "<input type='hidden' name='$key' value='$val'>";
            }
        }

        $output .= "<input type='submit' value='" . _('Submit') . "'></form>";
        $output .= "<table class='$tableClass'>";
    }

    $prev_recording = null;
    $row = $results->fetch_object();

    $perRow = 3;
    for ($j = 1; $row; $j++) {
        $rec = new Recording($row);

        // If location should be shown depends on passed variable or recorded species
        // (the first is to disable locations for backgroud species).
        $showLocation = $showLocations;
        if ($showLocation) {
            $showLocation = !$rec->species()->restricted;
        }

        // get next row to see if we're at the end or not
        $nextRow = $results->fetch_object();

        if ($view != ViewType::SONOGRAMS) {
            $output .= SEARCH_resultsRowDetailedConcise(
                $rec,
                $prev_recording,
                $cur,
                $view,
                $actionsCallback,
                $showLocation
            );
        } else {
            // Sono flow is not yet supported, so just display sonograms
            if ($j % $perRow == 1) {
                $output .= '<tr>';
            }

            $tdclass = 'sonobox';
            if (!$prev_recording || ($rec->speciesNumber() != $prev_recording->speciesNumber())) {
                $tdclass .= ' new-species';
            }

            $output .= "<td class=\"$tdclass\">";
            $output .= $rec->player(['showDate' => true, 'showLocation' => $showLocation, 'linkFields' => true,]);

            $output .= SEARCH_linksToForum($rec);
            $remarks = $rec->formatRemarks();
            if ($remarks) {
                $output .= "<div class='remarks readmore'>$remarks</div>";
            }
            $output .= XC_ratingWidget($rec->xcid(), $rec->rating());
            $output .= '<p>' . SEARCH_backgroundDropdownBox($rec, '');
            if ($actionsCallback) {
                $output .= call_user_func($actionsCallback, $rec);
            } else {
                $output .= defaultRecordingActions($rec);
            }

            $output .= '
                </p>
                </td>';

            // if the results are finished, but we haven't finished our current
            // table row, add a couple of empty boxes;
            if (!$nextRow && (($j % $perRow) != 0)) {
                for (; ($j % $perRow) != 0; $j++) {
                    $output .= '<td></td>';
                }
            }

            if ($j % $perRow == 0) {
                $output .= '</tr>';
            }
        }

        $row = $nextRow;
        $prev_recording = $rec;
    }// for i =1 to many

    $output .= '</table>';

    return $output;
}


function XC_speciesListForLocation($location, &$fgSpecies, &$bgSpecies = null)
{
    $loc = mysqli()->real_escape_string($location);
    $res = query_db(
        "SELECT genus, species, eng_name, species_nr, discussed FROM birdsounds WHERE location = \"$loc\" GROUP BY order_nr ORDER BY order_nr "
    );
    $fgSpecies = [];

    $i = 0;
    while ($row = $res->fetch_object()) {
        $i++;
        $fgSpecies[$row->species_nr] = [
            'nr' => $row->species_nr,
            'scientificName' => "$row->genus $row->species",
            'commonName' => $row->eng_name,
            'uncertain' => ($row->discussed == 1 || $row->discussed == 2),
        ];
    }

    if (!is_null($bgSpecies)) {
        $bgSpecies = [];
        $res2 = query_db(
            "SELECT back_latin, back_english, back_nrs FROM birdsounds WHERE location = \"$loc\" AND back_nrs != ''"
        );

        while ($row = $res2->fetch_object()) {
            $bgnames = explode(',', $row->back_english);
            $bglatin = explode(',', $row->back_latin);
            $bgnrs = explode(',', $row->back_nrs);
            for ($i = 0; $i < count($bgnames); $i++) {
                if (!array_key_exists($bgnrs[$i], $fgSpecies) && !array_key_exists($bgnrs[$i], $bgSpecies)) {
                    $bgSpecies[$bgnrs[$i]] = [
                        'nr' => $bgnrs[$i],
                        'scientificName' => $bglatin[$i],
                        'commonName' => $bgnames[$i],
                    ];
                }
            }
        }
    }
}


function XC_addUrlParamsToCurrentPage($request, $params)
{
    if (!$request) {
        echo('<p><pre>');
        debug_print_backtrace();
        echo('</pre></p>');
    }
    $url = $request->getBaseUrl();
    $vars = $request->query->all();
    foreach ($params as $key => $val) {
        $vars[$key] = $val;
    }
    return XC_makeURL($url, $vars);
}


function XC_ratingToString($quality)
{
    $strings = ['', 'A', 'B', 'C', 'D', 'E'];

    if ($quality > 0 && $quality < count($strings)) {
        return $strings[$quality];
    }

    return '';
}

//=====================
// Create a rating widget for a specific recording
//=====================
function XC_ratingWidget($xcid, $quality)
{
    $ratings = [
        1 => ['A', false],
        2 => ['B', false],
        3 => ['C', false],
        4 => ['D', false],
        5 => ['E', false],
    ];

    if (array_key_exists($quality, $ratings)) {
        $ratings[$quality][1] = true;
    }

    $output = "<div class='rating'>
        <ul>
        ";

    for ($i = 1; $i <= count($ratings); $i++) {
        $isSelected = $ratings[$i][1];
        $qName = $ratings[$i][0];
        $class = '';
        if ($isSelected) {
            $class = 'selected';
        }

        // don't need a link to rate a song for its current rating
        if (User::current() && User::current()->canRateRecording($xcid)) {
            $link = "<a href='#' onclick='return RateRecording($xcid,$i)'>$qName</a>";
        } else {
            $link = "<span>$qName</span>";
        }

        $output .= "<li id='rating-$xcid-$i' class='$class'>$link</li>";
    }
    $output .= '
        </ul>
        </div>';
    return $output;
}


function checkedArrayVal($arr, $idx, $fallback)
{
    if (!is_numeric($idx) || ($idx < 0) || ($idx > count($arr)) || !isset($arr[$idx])) {
        return $fallback;
    }

    return $arr[$idx];
}

$lengthCharacteristics = [
    _('0-3(s)'),
    _('3-6(s)'),
    _('6-10(s)'),
    _('&gt;10(s)'),
];

function lengthToString($code, $fallback = '')
{
    global $lengthCharacteristics;
    return checkedArrayVal($lengthCharacteristics, $code, $fallback);
}

$speedCharacteristics = [
    _('level'),
    _('decelerating'),
    _('accelerating'),
    _('both'),
];

function speedToString($code, $fallback = '')
{
    global $speedCharacteristics;
    return checkedArrayVal($speedCharacteristics, $code, $fallback);
}

$pitchCharacteristics = [
    _('level'),
    _('decreasing'),
    _('increasing'),
    _('both'),
];

function pitchToString($code, $fallback = '')
{
    global $pitchCharacteristics;
    return checkedArrayVal($pitchCharacteristics, $code, $fallback);
}

$volumeCharacteristics = [
    _('level'),
    _('decreasing'),
    _('increasing'),
    _('both'),
];

function volumeToString($code, $fallback = '')
{
    global $volumeCharacteristics;
    return checkedArrayVal($volumeCharacteristics, $code, $fallback);
}

$numNotesCharacteristics = [
    '1-3',
    '4-6',
    '7-20',
    '&gt;20',
];

function numNotesToString($code, $fallback = '')
{
    global $numNotesCharacteristics;
    return checkedArrayVal($numNotesCharacteristics, $code, $fallback);
}

$variableCharacteristics = [
    _('no'),
    _('yes'),
];

function variableToString($code, $fallback = '')
{
    global $variableCharacteristics;
    return checkedArrayVal($variableCharacteristics, $code, $fallback);
}

function XC_makeURL($base, $params)
{
    return $base . '?' . http_build_query($params);
}

function XC_outputPageNumbers($request, $start, $end, $params, $curPage)
{
    if ($start > $end) {
        return;
    }

    $output = '';
    for ($i = $start; $i <= $end; $i++) {
        $params['pg'] = intval($i);
        if ($i == $curPage) {
            $output .= "
                <li class=\"selected\"><span>$i</span></li>";
        } else {
            // need to add one due to zero-based iterator
            $params['pg'] = intval($i);
            $output .= "
                <li><a href=\"" . XC_addUrlParamsToCurrentPage(
                    $request,
                    $params
                ) . "\">$i</a></li>";
        }
    }
    return $output;
}

function XC_pageNumberNavigationWidget($request, $numPages, $curPage)
{
    // for now, just assume that we need to page between the same url that we're
    // currently viewing, with the same query string, but with the 'pg'
    // parameter changing

    $numEndItems = 1;
    $numMainItems = 9;

    // don't print a page navigation widget if there's nothing to navigate
    if ($numPages < 2) {
        return;
    }

    if ($numPages < $curPage) {
        return;
    }

    $params = [];

    $output = "
        <nav class=\"results-pages\">
            <ul>";
    if ($curPage > 1) {
        $params['pg'] = $curPage - 1;
        $output .= "
            <li><a href=\"" . XC_addUrlParamsToCurrentPage($request, $params) . "\">
                <img width='14' height='14' src=\"/static/img/previous.png\"/> " . _(
                'Previous'
            ) . '</a>
            </li>';
    }

    if ($numPages < ($numMainItems + (2 * $numEndItems))) {
        $output .= XC_outputPageNumbers(
            $request,
            1,
            $numPages,
            $params,
            $curPage
        );
    } else {
        $firstMainItem = max(
            1,
            min(
                $curPage - floor(($numMainItems - 1) / 2),
                $numPages - $numMainItems
            )
        );
        $lastMainItem = min($numPages, $firstMainItem + ($numMainItems - 1));

        $missingNumbers = $firstMainItem - 1;
        if ($missingNumbers) {
            // +1 for a potential 'gap' marker
            if ($missingNumbers <= ($numEndItems + 1)) {
                $output .= XC_outputPageNumbers(
                    $request,
                    1,
                    $missingNumbers,
                    $params,
                    $curPage
                );
            } else {
                // need gap indicator
                $output .= XC_outputPageNumbers(
                    $request,
                    1,
                    $numEndItems,
                    $params,
                    $curPage
                );
                $output .= "<li class=\"gap\"><span>...</span></li>";
            }
        }

        $output .= XC_outputPageNumbers(
            $request,
            $firstMainItem,
            $lastMainItem,
            $params,
            $curPage
        );

        $missingNumbers = $numPages - $lastMainItem;
        if ($missingNumbers) {
            // +1 for a potential 'gap' marker, again
            if ($missingNumbers <= ($numEndItems + 1)) {
                $output .= XC_outputPageNumbers(
                    $request,
                    $lastMainItem + 1,
                    $numPages,
                    $params,
                    $curPage
                );
            } else {
                // need gap indicator
                $output .= "<li class=\"gap\"><span>...</span></li>";
                $output .= XC_outputPageNumbers(
                    $request,
                    $numPages - ($numEndItems - 1),
                    $numPages,
                    $params,
                    $curPage
                );
            }
        }
    }

    if ($curPage < $numPages) {
        $params['pg'] = intval($curPage) + 1;
        $output .= "
            <li><a href=\"" . XC_addUrlParamsToCurrentPage(
                $request,
                $params
            ) . "\">" . _(
                'Next'
            ) . "
            <img width='14' height='14' src=\"/static/img/next.png\"/></a>
            </li>";
    }
    $output .= '
        </ul>
        </nav>';

    return $output;
}

function printErrorMessages($headline, $messages = [])
{
    $output = "<div class=\"error\">
        <h2>$headline</h2>";

    if (!empty($messages)) {
        $output .= '<ul>';
        foreach ($messages as $message) {
            $output .= "<li>$message</li>";
        }
        $output .= '</ul>';
    }
    $output .= '</div>';

    return $output;
}

function sanitize($var)
{
    if (is_array($var)) {
        foreach ($var as $k => $v) {
            $var[$k] = sanitize($v);
        }
    } else {
        $var = htmlspecialchars($var ?? '', ENT_QUOTES, null, false);
    }
    return $var;
}

function escape($var)
{
    if (is_array($var)) {
        foreach ($var as $k => $v) {
            $var[$k] = escape($v);
        }
    } else {
        $var = mysqli()->real_escape_string($var ?? '');
    }
    return $var;
}

function strip($var, $escape = true, $allowTags = false)
{
    $var = !$allowTags ? strip_tags($var) : strip_tags(
        $var,
        '<b><i><em><strong><a><h1><h2><h3><h4><h5><h6><p><iframe><img><pre><code><ul><ol><li><blockquote>'
    );
    return $escape ? escape($var) : $var;
}

function formatRecordingLink($matches)
{
    if (count($matches) < 2) {
        return $matches[0];
    }

    $id = intval($matches[1]);
    return "<a href='" . getUrl('recording', ['xcid' => $id]) . "'>{$matches[0]}</a>";
}

function formatMLRecordingLink($matches)
{
    if (count($matches) < 2) {
        return $matches[0];
    }

    $id = intval($matches[1]);
    return "<a href='//macaulaylibrary.org/audio/$matches[1]'>$matches[0]</a>";
}

function XC_formatText($txt, $stripTags = false)
{
    //$markup = Markdown($txt);
    // @PHP7 new Markdown library used
    $markup = Markdown::defaultTransform($txt);

    if ($stripTags) {
        // sanitize the html so that it doesn't contain anything malicious, but
        // allow a few basic tags
        $markup = strip_tags(
            $markup,
            '<b><i><em><strong><a><h1><h2><h3><h4><h5><h6><p><iframe><img><pre><code><ul><ol><li><blockquote>'
        );
    }
    $linked = preg_replace_callback(
        '/\bXC\s?(\d+)/i',
        "\\xc\\formatRecordingLink",
        $markup
    );
    $linked = preg_replace_callback(
        '/\bML\s?(\d+)/i',
        "\\xc\\formatMLRecordingLink",
        $linked
    );
    return preg_replace_callback(
        '/\bLNS\s?(\d+)/i',
        "\\xc\\formatMLRecordingLink",
        $linked
    );
}

function XC_formatUserText($txt)
{
    return XC_formatText($txt, true);
}


function XC_rateRecording($snd_nr, $quality, $userid = null)
{
    if (!$userid) {
        $userid = User::current()->userId();
    }

    if (isset($quality) && isset($snd_nr)) {
        if ($quality >= 1 && $quality <= 5) {
            // this is experimental for now, a table with per-user ratings
            query_db(
                "INSERT INTO birdsounds_user_ratings (snd_nr, userid, rating, date)
                VALUES($snd_nr, '$userid', $quality, NOW())
                ON DUPLICATE KEY UPDATE rating=VALUES(rating), date=NOW()"
            );

            $sql = "update birdsounds set quality = '$quality' where snd_nr = '$snd_nr'";
            $res = query_db($sql);

            return (bool)$res;
        }
    }
    return false;
}

function XC_pageNumber($request)
{
    $pagenumber = abs((int)$request->query->get('pg'));
    if (empty($pagenumber)) {
        $pagenumber = abs((int)$request->query->get('pagenumber'));
    }
    if (empty($pagenumber)) {
        $pagenumber = 1;
    }
    return $pagenumber;
}

function mockMail($addr, $subject, $message, $headerstring)
{
    echo("\n\n");
    echo("--------------------------------------------------\n");
    echo("\n");
    echo("To: $addr\n");
    echo("Subject: $subject\n");
    echo("$headerstring\n");
    echo($message);
    echo("\n");
    echo("--------------------------------------------------\n");
    echo("\n\n");
}

$_logger = new XCLogger('XC');
function XC_logger()
{
    global $_logger;
    return $_logger;
}

function XC_formatDuration($duration, $iso8601 = false)
{
    $str = '';
    if ($iso8601) {
        $str .= 'PT';
    }

    $hours = 0;
    $minutes = 0;
    if ($duration > (60 * 60)) {
        $hours = floor($duration / (60 * 60));
    }
    if ($duration > 60) {
        $remainder = (int)$duration % (60 * 60);
        $minutes = floor($remainder / 60);
    }

    $seconds = (int)$duration % 60;

    if ($hours) {
        if ($iso8601) {
            $str .= "{$hours}H{$minutes}M";
        } else {
            $str .= "$hours:";
            // if there are hours, make sure minutes is 2 digits
            $str .= sprintf('%02d:', $minutes);
        }
    } elseif ($iso8601) {
        $str .= "{$minutes}M";
    } else {
        $str .= "$minutes:";
    }

    if ($iso8601) {
        $str .= "{$seconds}S";
    } else {
        $str .= sprintf('%02d', $seconds);
    }

    return $str;
}

function getCountryList()
{
    static $countryList = null;
    if (!$countryList) {
        $countryList = [];
        $res = query_db(
            'select country, country_code from world_country_list order by country'
        );
        if ($res) {
            while ($row = $res->fetch_object()) {
                $countryList[$row->country_code] = $row->country;
            }
        }
    }

    return $countryList;
}

function XC_ellipsize($string, $length)
{
    $string = strip_tags($string);
    if (strlen($string) > ($length + 1)) {
        return substr($string, 0, $length) . '…';
    }

    return $string;
}

// calculate the acceptable number of errors allowed for a set sized $n
function XC_acceptableErrors($n)
{
    $target = 0.99;

    $nominal = (1.0 - $target) * $n;
    $padding = sqrt($target * $n);

    return min(ceil($nominal + $padding), $n);
}

function XC_checkValidFileFormat($path, &$mime)
{
    /*
    // check mime type to make sure that it's not just another
    // file renamed with an .mp3 extension
    // USING custom gstreamer typefind utility
    $typefind_paths = array("/usr/local/bin/typefind", "./tools/typefind/typefind");
    $exe = "";

    foreach ($typefind_paths as $typefind)
    {
        if (file_exists($typefind))
        {
            $exe = $typefind;
            break;
        }
    }

    if (!$exe) {
        $mime = "unable to check";
        return false;
    }

    $cmd = $exe . " " . $path;
    $mime = exec($cmd);
    $commapos = strpos($mime, ',');
    if ($commapos !== false)
        $mime = substr($mime, 0, $commapos);
    */

    $getID3 = new getID3();
    $info = $getID3->analyze($path);

    if (!isset($info['fileformat'])) {
        return false;
    }

    if ($info['fileformat'] == 'mp3') {
        $mime = $info['mime_type'] ?? 'unknown';
    }

    if (!($mime == 'audio/mpeg' || $mime == 'application/x-id3')) {
        return false;
    }

    return true;
}

function XC_extinctSymbol()
{
    return "<span title='" . _('Extinct') . "'>&dagger;</span>";
}

function userSelectHtml($name = 'u')
{
    $html = "<select name='$name' id='u'>
        <option value='none'>Choose a user</option>
        ";
    $res = query_db(
        'SELECT U.username, U.dir, U.email, COUNT(snd_nr) as nrecs FROM users U LEFT JOIN birdsounds USING(dir) GROUP BY dir ORDER BY username ASC'
    );
    while ($row = $res->fetch_object()) {
        $html .= "<option value='{$row->dir}'>{$row->username}  - {$row->email} - {$row->nrecs} recordings ({$row->dir})</option>
            ";
    }
    $html .= '</select>';

    return $html;
}

function userListHtml($url, $referer = false)
{
    $html = '<ul>
        ';
    $res = query_db(
        'SELECT U.username, U.dir, U.email, COUNT(snd_nr) as nrecs FROM users U LEFT JOIN birdsounds USING(dir) GROUP BY dir ORDER BY username ASC'
    );
    while ($row = $res->fetch_object()) {
        $href = $url . $row->dir . ($referer ? '/' . $referer : '');
        $html .= "<li><a href='$href'>{$row->username} ({$row->email})</a>: {$row->nrecs} recordings ({$row->dir})</li>\n";
    }
    $html .= "\n</ul>";

    return $html;
}

function db()
{
    return app()->db();
}

function getUrl($route, $args = [], $absolute = false)
{
    return app()->getUrl($route, $args, $absolute);
}


function notifyError($text)
{
    app()->session()->getFlashBag()->add('error', $text);
}

function notifyWarning($text)
{
    app()->session()->getFlashBag()->add('warning', $text);
}

function notifySuccess($text)
{
    app()->session()->getFlashBag()->add('success', $text);
}

function notifyInfo($text)
{
    app()->session()->getFlashBag()->add('info', $text);
}

function populateBackgroundData($bgText, &$bgNrs, &$bgEnglish, &$bgLatin, &$bgFamily, &$bgExtra)
{
    $bgNrs = [];
    $bgExtra = [];
    $bgEnglish = [];
    $bgLatin = [];
    $bgFamily = [];
    $bgExtra = [];

    if (!empty($bgText)) {
        $names = explode(',', $bgText);
        for ($i = 0; $i < count($names); $i++) {
            $name = trim($names[$i]);
            if (!empty($name)) {
                $spnr = Species::getSpeciesNumberForString($name);
                if (empty($spnr)) {
                    $bgExtra[] = $name;
                } else {
                    $bgNrs[] = $spnr;
                }
            }
        }
    }

    if (!$bgNrs) {
        return;
    }

    for ($i = 0; $i < count($bgNrs); $i++) {
        $s = Species::load(trim($bgNrs[$i]), false);
        if (!$s) {
            continue;
        }
        $bgEnglish[] = $s->commonName();
        $bgLatin[] = $s->scientificName();
        $bgFamily[] = $s->family();
    }
}

function obfuscateEmail($address, $link)
{
    return '<script type="text/javascript">
            document.write("<n uers=\"' . str_rot13(
            'mailto:' . $address
        ) . '\" ery=\"absbyybj\">' . str_rot13($link) . '</n>".replace(/[a-zA-Z]/g, 
        function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}))
        </script>';
}

function XC_formatNumber($n)
{
    // Check why function isn't found!
    return $n;

    //$nf = new \NumberFormatter(setlocale(LC_ALL, 0), \NumberFormatter::DECIMAL);
    //return $nf->format($n);
}

function XC_adminMailAddresses()
{
    return array_merge(
        [WEBMASTER_EMAIL],
        array_map('trim', explode(',', ADMINS_EMAIL))
    );
}
