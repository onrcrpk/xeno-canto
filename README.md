# Xeno-canto

The Xeno-canto (XC) site is built as a set of Docker images. The base images, the building blocks, are maintained in
[a separate repository](https://gitlab.com/naturalis/bii/xenocanto/docker-xenocanto). These base images contain the
necessary third party software.

This repository contains everything to install the site both for development and production. The development environment
enables XDebug, whereas the production environment enables Traefik and demonstrates what the site is like on a
production server.

## Setting up a development environment

Run the following steps to create a XC development server:

1. Clone the repository: `git clone https://gitlab.com/naturalis/bii/xenocanto/xeno-canto.git`.
2. Enter the repository: `cd xeno-canto`.
3. Preliminarily create symlinks for the data directories, so these will point to the correct locations:

```
ln -s ./data/docs .;
ln -s ./data/graphics .;
ln -s ./data/ranges .;
ln -s ./data/sitemaps .;
ln -s ./data/sounds .;
```

4. Create an `.env` file by duplicating the sample file: `cp env.sample .env`.
5. In the .env file, uncomment the line `DATA_PATH=./data`. 

You can now start the application using `docker-compose up -d` (which incorporates the default `docker-compose.yml` and
override `docker-compose.override.yml`). This will create the required directories.

6. Install the required third-party PHP components using composer: `docker-compose exec php-fpm composer install`.

What is still missing, however, is the database. Bring down docker compose with `docker-compose down` and...

7. Copy the database dump `cp database/xc_trimmed.sql.gz data/database/init` and the database trigger
  `cp database/zz_trigger.sql data/database/init`.
8. Delete the database `storage` directory, so the database will be re-initiated: `rm -R data/database/storage`.
9. Restart the application using `docker-compose up`.

The XC site is now available at `http://localhost`.

### Code modifications

As the entire repository directory is exposed in the development environment, any changes in PHP or Javascript files are
directly reflected. All info, warnings and errors are logged to `stdout`, so use `docker-compose logs -f` to check for
information. To access the containers extending the base images:

- `docker-compose exec database mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE`
- `docker-compose exec php-fpm bash`
- `docker-compose exec nginx /bin/sh`

### Database

Similar to the code, the local database also is exposed to allow for direct modifications. Use `127.0.0.1`as host, plus
the credentials in `docker-compose.override.yml` to connect using a database management application.

### Media

When installing the site locally, you are likely to be greeted with many "Media URL could not be loaded" errors. You can
fix this by setting `REMOTE_BASE_URL=https://xeno-canto.org` in your `.env`. However, be aware that when using this
setting, **any new recording you upload will not be displayed correctly**!

### Google Maps

Valid keys should be added to your `.env` file to correctly display maps.

### Email

XC requires a dedicated SMTP server to send emails. While this can be enabled in the development environment, it's
probably easier to debug email using [Mailhog](https://github.com/mailhog/MailHog). Mailhog is installed by default and
can be enabled by setting the `.env` variable `MAILER_DSN=smtp://mailserver:1025`. Its web interface is accessible
at [http://localhost:8025](http://localhost:8025).

### Minio

[Minio](https://min.io) is used to allow admins to upload batch imports, range maps and classification updates and to
monitor exports. Several of these scripts expect data to be present in the buckets
`batch`, `ranges`, `classification` and `nba` respectively. Use the Minio console
at [http://localhost:9001](http://localhost:9001) to create these buckets. See `.env.sample`for the default credentials.
The port for the `mc` client is not enabled in your development environment, as you can directly copy files to
`./data/minio/[bucket]`.

## Setting up a production environment

Creating a production environment is more complicated.

First of all, you will need to access a remote database, as the production environment uses a dedicated database server.
So contrary to development, the production environment does not include a database.

Traefik enables https in this environment. Here it uses LetsEncrypt, and assumes you have access to a (Route53) DNS
service. Prepare to modify these settings or to face certificate warnings if you are using a different provider or don't
have access to a service that can generate certificates on the fly.

Finally, note that changes in your code will not be reflected immediately. The Docker images will need to be rebuilt to
incorporate any code changes.

1. Add the required additional variables to your `.env` file.
1. Assuming you have previously created a development environment, your `data` directory is located at the root level of
   your repository. If so, add `DATA_PATH=./data` to your `.env`.
1. While the development environment defaults to the latest versions of the base images, you are required to specify
   `IMAGE_VERSION` in `.env` (e.g. `develop` or `latest`). Use a different tag to test with other versions of the base
   images. Also see _Using modified base images_ below.
1. Provide values for the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` credentials in case you are able to create
   Route53 certificates.
1. Provide values for the `NGINX_FQDN` and `MINIO_FQDN` fully qualified domain names, e.g.
   ``NGINX_FQDN=Host(`xc.dryrun.link`)`` and
   ``MINIO_FQDN=Host(`minio.xc.dryrun.link`)``.
1. Add these to `/etc/hosts`: `127.0.0.1 xc.dryrun.link minio.xc.dryrun.link`.
1. A database is not part of the production environment, as this is hosted on a dedicated server. Uncomment and modify
   the `MYSQL_*` settings to connect to an external MariaDB server.
1. Start the site using `docker-compose -f docker-compose.yml -f docker-compose.deploy.yml up`.

The site should now be available at the url you provided for `NGINX_FQDN`.

## Using modified base images

Sometimes changes are required at a higher level, e.g. if you want to experiment with new PHP libraries. You can
modify `IMAGE_VERSION` in your `.env` to use different version of the image, or you can build a new base image yourself.
In this example, we modify the php-fpm base image:

1. Clone the base image repository: `git@gitlab.com:naturalis/bii/xenocanto/docker-xenocanto.git`.
1. Modify `php/Dockerfile` to suit your needs.
1. Build a local version of the base image, so from the root of the repository: 
   `cd php;`
   `docker build --no-cache -f Dockerfile -t php-fpm-test-build .`
   (don't forget the last dot, your local path!).
1. In `docker-compose.override.yml` or `docker-compose.deploy.yml`, modify the container `image` value to the name of
   the images you just created. So in this example, for the `php-fpm` service, temporarily replace
   `image: registry.gitlab.com/naturalis/bii/xenocanto/docker-xenocanto/php-dev:${IMAGE_VERSION:-latest}` with
   `image: php-fpm-test-build`. Make sure not to accidentally commit the modified docker compose file ;)
1. When switching versions of base images, it's recommended to `docker-compose pull` before `docker-compose up`.

## Importing an existing database

1. Create a dump of the current production database:
   `mysqldump -u root -p xenocanto > xc.sql`
1. Strip the trigger definer from the dump: `sed -i 's/DEFINER=[^*]*\*/\*/g' xc.sql` or alternatively,
   import `/database/trigger.sql` to replace the existing trigger
1. Delete `./data/storage` if present
1. Copy `xc.sql` to `./data/database/init`; the database will be imported on `docker-compose up`
1. Create a user develop and grant privileges:

- `docker-compose exec database mysql -u root -p xenocanto`
- `CREATE USER 'develop'@localhost IDENTIFIED BY 'develop';`
- `GRANT ALL PRIVILEGES ON xenocanto.* TO 'develop'@localhost`;

## Checking the logs

- `docker-compose logs` for the complete log
- `docker-compose logs php-fpm` for logs from a particular container
- `docker-compose logs nginx | grep rate_limited` for a particular string in the logs for that container

If you need to check the logs interactively, you can

- `docker ps`
- `copy/paste` the name of the container you want to check
- `docker container logs xeno-canto_nginx_1 --tail=100 -f`

## Deployment

Whenever there is push or merge into the `develop` branch and the pipeline is successful, these images are published and 
deployed to the dev server.

Feature releases can be triggered using the pipeline form in GitLab, by supplying the branch name. Be sure that these
images have been successfully built and are in the registry. Take note, that a feature deployment will be overwritten by
the develop build everytime a new feature of change is succefully merged into the develop branch.

To deploy to production, create a version tag and run the pipeline. 

## Testing deployment

Deployment scripts can be tested using a Vagrant virtual machine (VM). Make sure that Docker or other virtual machines
are not running while starting Vagrant. The VM uses the ports 2222, 8080, 8025 and 8081.

Follow the recipe below to create a working Vagrant machine. Take note that some steps are performed on your local
machine, whereas others are in the VM!

1. Install a base VM
   by [cloning the ansible-xenocanto repository](https://gitlab.com/naturalis/bii/xenocanto/ansible-xenocanto)
   and [following the steps in its Read Me](https://gitlab.com/naturalis/bii/xenocanto/ansible-xenocanto/-/tree/master).
1. On your local machine, `cd` to the root of this repository and verify that you have all ansible requirements
   installed:

- `cd ansible`
- `ansible-galaxy collection install -r requirements.yml -p collections --force`

1. Copy the database dump from this repo to the VM: `scp -P 2222 database/xc_trimmed.sql.gz vagrant@127.0.0.1:`
1. Exit to your local machine and deploy the
   server: `IMAGE_VERSION=[branch] ansible-playbook --inventory ansible/environments/staging/hosts.ini ansible/deploy_xc.yml -t deploy`
   . As the Docker images are downloaded in this step, this may take a while!
1. Log into the VM using `ssh vagrant@127.0.0.1 -p 2222` and switch to root user `sudo su`.
1. In the VM, move the dump to the appropriate location: `mv /home/vagrant/xc_trimmed.sql.gz /data/database/init/`
1. A MariaDB root password is required in the setup and this is not provided by default. To load the data from the
   dump (which again may take a while), issue the following in your VM:

```cd /opt/compose_projects/xeno-canto```
```echo "MARIADB_ALLOW_EMPTY_ROOT_PASSWORD=1" >> .env```
```docker-compose down && docker-compose up```

The site should now be accessible at `http://127.0.0.1:8080/`.

## Timed scripts

`cron` jobs have been replaced with `systemd` services.

- `systemctl status`: an overview of running services
- `systemctl list-timers`: schedule
- `systemctl --failed`: services requiring attention
- `journalctl --follow --since=now -u [name].service`: logs; `now` can be replaced with  `today|yesterday`or a specific
  time `YYYY-MM-DD HH:MM:SS`

You can format the output using `-o json | jq` of `-o json-pretty | jq`.

## Maintenance

The site can be set to "maintenance mode" by creating a temporary `.maintenance` file. Log into the site and

```
cd /opt/compose_projects/xeno-canto
docker-compose exec nginx sh
touch .maintenance
```

`rm` the file to resume normal operation.
