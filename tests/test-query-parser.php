<?php

use xc\TaggedQuery;
use xc\ParsedQuery;

set_include_path('..');
require_once '../vendor/autoload.php';

function buildParsedQuery($basic, $tagged)
{
    $pq                = new ParsedQuery();
    $pq->basicTerms    = $basic;
    $pq->taggedQueries = $tagged;

    return $pq;
}

$tests = [
    // basic query
    [
        "junco hyemalis rec:Jongsma cnt:\"United States\"",
        buildParsedQuery(
            ['junco', 'hyemalis'],
            [
                new TaggedQuery('rec', 'Jongsma'),
                new TaggedQuery('cnt', 'United States'),
            ]
        ),
    ],

    // search terms on either side of tagged queries
    [
        "junco rec:Jongsma cnt:\"United States\" hyemalis",
        buildParsedQuery(
            ['junco', 'hyemalis'],
            [
                new TaggedQuery('rec', 'Jongsma'),
                new TaggedQuery('cnt', 'United States'),
            ]
        ),
    ],

    // space after the tag colon
    [
        "junco hyemalis rec: Jongsma cnt:\"United States\"",
        buildParsedQuery(
            ['junco', 'hyemalis'],
            [
                new TaggedQuery('rec', 'Jongsma'),
                new TaggedQuery('cnt', 'United States'),
            ]
        ),
    ],

    // multiple spaces between words
    [
        "junco    hyemalis   rec:Jongsma cnt:\"United States\"",
        buildParsedQuery(
            ['junco', 'hyemalis'],
            [
                new TaggedQuery('rec', 'Jongsma'),
                new TaggedQuery('cnt', 'United States'),
            ]
        ),
    ],

    // a string with a single-quote in it
    [
        "junco hyemalis rec:\"Ryan O'Donnell\"",
        buildParsedQuery(
            ['junco', 'hyemalis'],
            [new TaggedQuery('rec', "Ryan O'Donnell")]
        ),
    ],

    // search terms immediately after quote without space
    [
        "junco rec:Jongsma cnt:\"United States\"hyemalis",
        buildParsedQuery(
            ['junco', 'hyemalis'],
            [
                new TaggedQuery('rec', 'Jongsma'),
                new TaggedQuery('cnt', 'United States'),
            ]
        ),
    ],

    // quoted string in basic query
    [
        "\"junco  hyemalis\" rec:Jongsma",
        buildParsedQuery(
            ['junco  hyemalis'],
            [new TaggedQuery('rec', 'Jongsma')]
        ),
    ],

    // quoted string attached to tagged search term without a space
    [
        "junco hyemalis rec:Jongsma cnt:peru\"United States\"",
        buildParsedQuery(
            ['junco', 'hyemalis', 'United States'],
            [
                new TaggedQuery('rec', 'Jongsma'),
                new TaggedQuery('cnt', 'peru'),
            ]
        ),
    ],

    // only tagged queries
    [
        'rec:Jongsma',
        buildParsedQuery(
            [],
            [new TaggedQuery('rec', 'Jongsma')]
        ),
    ],

    // excercise some additional tags
    [
        'also:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('also', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'box:1234,1234,1234,1234',
        buildParsedQuery(
            [],
            [new TaggedQuery('box', '1234,1234,1234,1234')]
        ),
    ],

    // only tagged queries
    [
        'dir:OWIERNJ',
        buildParsedQuery(
            [],
            [new TaggedQuery('dir', 'OWIERNJ')]
        ),
    ],

    // only tagged queries
    [
        'gen:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('gen', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'lat:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('lat', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'lon:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('lon', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'lic:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('lic', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'loc:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('loc', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'nr:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('nr', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'q:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('q', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'q>:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('q>', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'q<:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('q<', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'rmk:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('rmk', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'since:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('since', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'ssp:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('ssp', 'foo')]
        ),
    ],

    // only tagged queries
    [
        'type:foo',
        buildParsedQuery(
            [],
            [new TaggedQuery('type', 'foo')]
        ),
    ],

    // An invalid query tag
    [
        'foo:bar',
        buildParsedQuery(
            ['foo:bar'],
            []
        ),
    ],

    // A query tag without an argument.  The tag should just be dropped since
    // there's no term to search for
    [
        'bar rec:',
        buildParsedQuery(
            ['bar'],
            []
        ),
    ],

    // A query tag with an extra separator
    [
        'rec::bar',
        buildParsedQuery(
            [],
            [new TaggedQuery('rec', ':bar')]
        ),
    ],

    // A query tag argument with an colon
    [
        "rec:\"bar:baz\"",
        buildParsedQuery(
            [],
            [new TaggedQuery('rec', 'bar:baz')]
        ),
    ],

    // A query tag argument with an colon, unquoted
    [
        'rec:bar:baz',
        buildParsedQuery(
            [],
            [new TaggedQuery('rec', 'bar:baz')]
        ),
    ],

    // A query tag argument with a valid query tag in the argument
    [
        'rec:loc:baz',
        buildParsedQuery(
            [],
            [new TaggedQuery('rec', 'loc:baz')]
        ),
    ],

    // A query tag with a space before the colon
    [
        'rec :foo',
        buildParsedQuery(
            ['rec', ':foo'],
            []
        ),
    ],

    // A search for linked images in the remarks
    [
        'rmk:img:',
        buildParsedQuery(
            [],
            [new TaggedQuery('rmk', 'img:')]
        ),
    ],

    // A search for standard bird-seen values
    [
        'rmk:bird-seen:yes',
        buildParsedQuery(
            [],
            [new TaggedQuery('rmk', 'bird-seen:yes')]
        ),
    ],

    // An unterminated quoted string
    [
        "rmk:\"unfinished string",
        buildParsedQuery(
            [],
            [new TaggedQuery('rmk', 'unfinished string')]
        ),
    ],

    // Another unterminated quoted string
    [
        "rmk:\"unfinished rec:string",
        buildParsedQuery(
            [],
            [new TaggedQuery('rmk', 'unfinished rec:string')]
        ),
    ],

];

function fail($message, $var = null)
{
    if ($var) {
        echo('<pre>');
        var_dump($var);
        echo('</pre>');
    }
    die("<p style='color:red;font-weight:bold;'>Failed: $message</p>");
}

function assertTaggedQueriesEqual($expected, $actual)
{
    if ($expected->tagName != $actual->tagName) {
        fail(
            "Expected tag [{$expected->tagName}] but got [{$actual->tagName}]"
        );
    }
    if ($expected->tagName != $actual->tagName) {
        fail(
            "Expected tag [{$expected->tagName}] to be {$expected->searchTerm} but got [{$actual->searchTerm}]"
        );
    }
}

$n = 0;
foreach ($tests as $test) {
    $n++;
    echo("<h1>Running test $n</h1>");
    echo("<pre>{$test[0]}</pre>");
    $parser   = new QueryParser($test[0]);
    $expected = $test[1];
    $actual   = $parser->parse();
    if (count($expected->basicTerms) != count($actual->basicTerms)) {
        fail(
            'expected ' . count(
                $expected->basicTerms
            ) . ' basic terms but got ' . count($actual->basicTerms),
            $actual
        );
    }
    for ($i = 0; $i < count($expected->basicTerms); $i++) {
        echo('<pre>');
        echo("Checking basic term $i...");
        if ($expected->basicTerms[$i] != $actual->basicTerms[$i]) {
            fail(
                "Expected token $i to be [{$expected->basicTerms[$i]}] but got [{$actual->basicTerms[$i]}]"
            );
        } else {
            echo("[{$expected->basicTerms[$i]}]");
        }
        echo('</pre>');
    }
    if (count($expected->taggedQueries) != count($actual->taggedQueries)) {
        fail(
            'expected ' . count(
                $expected->taggedQueries
            ) . ' tagged queries but got ' . count($actual->taggedQueries),
            $actual
        );
    }
    for ($i = 0; $i < count($expected->taggedQueries); $i++) {
        echo('<pre>');
        echo("Checking tagged query $i...");
        assertTaggedQueriesEqual(
            $expected->taggedQueries[$i],
            $actual->taggedQueries[$i]
        );

        echo("[{$actual->taggedQueries[$i]->tagName}] : [{$actual->taggedQueries[$i]->searchTerm}]");
        echo('</pre>');
    }
    echo("<p style='color:darkgreen;font-weight:bold;'>Success</p>");
}
