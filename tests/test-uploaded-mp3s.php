<?php

// Test to verify existing mp3s according to the same method that
// is used to verify uploaded files.

require_once '../vendor/james-heinrich/getid3/getid3/getid3.php';
require_once '../tools/MP3File.php';

$dir = '/data/sounds/uploaded/';
$log = '/home/ruud.altenburg/corrupt.log';

$fp = fopen($log, 'w');

_log($fp, "Scanning for mp3 files in $dir...\n");
$files = getDirContents($dir);

_log($fp, 'Checking ' . count($files) . " mp3 files...\n");
foreach ($files as $mp3) {
    $getID3 = new getID3();
    $f      = $getID3->analyze($mp3);
    if (!isset($f['fileformat']) || $f['fileformat'] != 'mp3') {
        // Extra check in case getID3 fails to recognize valid mp3 file.
        // Modified class MP3File returns 0 for duration of false mp3 files.
        $mp3file = new MP3File($f);
        if ($mp3file->getDurationEstimate() == 0) {
            _log($fp, "$mp3\n");
        }
    }
}
fclose($fp);

function getDirContents($dir, &$mp3s = [])
{
    if (is_dir($dir)) {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path) && isMP3($path)) {
                $mp3s[] = $path;
            } elseif ($value != '.' && $value != '..') {
                getDirContents($path, $mp3s);
            }
        }
    }
    return $mp3s;
}

function isMP3($path)
{
    return strtolower(substr(trim($path), -4)) == '.mp3';
}

function _log($fp, $m)
{
    fwrite($fp, $m);
    echo $m;
}
