<?php

$strings = array(
_("Articles"),
_("The <a href=\"/articles\">Articles</a> on XC can be used to annotate sets of recordings.  They can be shared with other users, or you can use them as a notepad for yourself."),
_("Notifications"),
_("A daily, weekly or monthly digest can be sent with an overview of new recordings uploaded to xeno-canto matching your search criteria. 

For details log in and check <a href=\"/account/notifications\">this page</a>."),
_("Embed recordings on your own page."),
_("It is possible to embed xeno-canto recordings on your own web page. On every recording page for example <a href=\"http://www.xeno-canto.org/10042\">this one</a> there is a link below the sonogram that shows the code necessary to embed a recording. There is a version with and a version without sonogram."),
_("Looking for API access?"),
_("Xeno-canto runs a basic API. Check <a href=\"/article/153\">this article</a> for the details."),
_("Simple urls"),
_("Recordings of a certain genus can be found with a simple url like for instance:

<b>www.xeno-canto.org/genus/Grallaria</b>

the links in the list next to the maps will take you to a species page.

Recordings of a certain species can be found with a simple url like for instance:

<b>www.xeno-canto.org/species/Grallaria-rufula</b>

A single recording can be found with its catalogue number:

<b>www.xeno-canto.org/XC12345</b> or even <b>www.xeno-canto.org/12345</b>"),
_("Mystery page"),
_("XC has a  <a href=\"/mysteries\">mystery page</a>. Recordings that are uploaded as mystery and those that are flagged in the forum when their ID is not sure can be seen on that page. Use the map to locate mysteries near you and help solving them."),
_("Make your recordings count"),
_("XC recordist <b>Dan Lane</b> has written an article on <a href=\"/article/117\">how to make your recordings most valuable to others</a>. Check it out!"),
_("Contact other recordists"),
_("On recordist pages you'll find a link that allows you to contact them via email. Please note that using it will show your email address to that recordist."),
_("Translations"),
_("Xeno-canto has been partially translated into many different languages by the XC community. Check the footer of the page! If you want to add a translation: drop us a note."),
_("Translated bird names"),
_("Check the footer for translations of the bird list into various languages!"),
_("Longer Recordings"),
_("With the move to the servers of Naturalis Biodiversity Centre, the file size limits have been relaxed again. Recordings can be up to **128MB** in size. And of course you can always try to convince us of hosting even larger ones."),
_("Add time markers in remarks"),
_("It is possible to add time markers in the remarks as 0:20 or 0:15.5 that will automagically change into links. When clicked, the sound will start playing from the indicated moment in time.  For an example see XC122325."),
_("Score songs"),
_("If you feel up to it please help us out scoring recordings.
After you log in all recordings show a set of squares marked A,B,C,D,E. 
They are explained <a href=\"/help/FAQ#rating\">here</a>.
If a recording has been scored the current score is highlighted. 
Clicking on a value will reset the value to the one you chose. 
In sonogram view many recordings can be viewed & compared easily. "),
_("Recording Sets"),
_("You can now create sets of recordings for public or private use.  Visit [your account page](/account/sets) to get started, or look at an [example](/set/11)."),
_("Acknowledging XC"),
_("If the efforts of the XC community were useful to your project do acknowledge XC. Also, do honour the attribution clause in the CC license & and cite sounds that are treated as data like you cite articles, books and other information sources.  Fair and proper citation will provide the XC community with the opportunity to show the impact of  XC to everyone interested. Check out the <a href=\"/about/terms\">Terms of use</a> for more information."),
_("Using Audacity"),
_("Audacity is a freely available software package that lets you analyse and visualise XC recordings. XC recordist Volker Arnold wrote : [a manual](https://www.dropbox.com/s/cn56iuw4gszmlx5/usingaudacity.pdf?dl=0). Stein Ø. Nilsen wrote [another article](http://www.xeno-canto.org/article/216) on filters in Audacity. Very useful!"),
_("Using WarbleR"),
_("[WarbleR](https://rpubs.com/marcelo-araya-salas/110155) is a package written for R that allows in-depth analysis of sets of XC recordings. Co-published by XC recordist Marcelo Araya-Salas. Excellent. A formal publication is [here](http://onlinelibrary.wiley.com/doi/10.1111/2041-210X.12624/full)"),
_("Improving the maps"),
_("Spot a map with an apparent mistake? Let us know. Or rather let [Rolf de By](http://www.xeno-canto.org/contributor/PTYTXQRLSZ) know, at maps@xeno-canto.org. Read [this article](http://www.xeno-canto.org/article/194) to learn more about the maps on XC."),
_("Share metadata with GBIF"),
_("Check your CC license preference and default on [this new page](https://www.xeno-canto.org/account/license) and indicate whether XC can share metadata of your recordings with [GBIF](www.gbif.org)."),
);
