<?php

require_once 'vendor/autoload.php';
require_once 'xc.lib.php';

use xc\XCRedis;
use function xc\XC_logger;

class ClearRedisCache extends xc\Task
{
    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        XC_logger()->logInfo('[Clear cache] Clearing all keys...');

        $redis = new XCRedis();
        $redis->clearAllCache();

        XC_logger()->logInfo('[Clear cache] Ready!');
    }
}

function app()
{
    static $task = null;
    if (!$task) {
        $task = new ClearRedisCache();
    }
    return $task;
}

app()->run();
