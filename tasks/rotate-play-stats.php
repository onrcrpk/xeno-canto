<?php

require_once 'vendor/autoload.php';
require_once 'constants.php';

use xc\Task;
use xc\XCLogger;

class RotatePlayStatsTask extends Task
{

    private $XC_Logger;

    public function __construct()
    {
        parent::__construct('.');
        $this->XC_Logger = new XCLogger('XC');
    }

    public function run()
    {
        $cutoff = 'DATE_SUB(CURDATE(), INTERVAL 21 DAY)';
        $res    = $this->db()->query(
            "
            INSERT INTO total_play_stats
            (SELECT snd_nr, COUNT(*) as np from birdsounds_play_stats
            WHERE (date < $cutoff) GROUP BY snd_nr)
            ON DUPLICATE KEY UPDATE nplays=nplays+VALUES(nplays)"
        );

        if ($res !== false) {
            $this->db()->query(
                "DELETE FROM birdsounds_play_stats WHERE date < $cutoff"
            );
            $this->XC_Logger->logInfo(
                '[Rotate play stats] ' . $this->db()->getConnection(
                )->affected_rows . ' records deleted from table "birdsounds_play_stats"'
            );
            return 0;
        }

        $this->XC_Logger->logError(
            '[Rotate play stats] Could not update table "total_play_stats"'
        );
        return 1;
    }

}

$task = new RotatePlayStatsTask();
$task->run();
