<?php

require_once 'vendor/autoload.php';
require_once 'xc.lib.php';
require_once 'tasks/update-stats-common.php';

use xc\Query;
use xc\Species;
use xc\XCRedis;

use function xc\XC_logger;

class UpdateStatsHourlyTask extends UpdateStatsCommon
{

    public function __construct()
    {
        parent::__construct('.');
    }

    public function __destruct()
    {
        // Clear Redis cache when done
        XC_logger()->logInfo('[Update stats] Clearing Redis cache');
        $this->redis = new XCRedis();
        $this->redis->clearHourlyStatsCache();
    }

    public function run()
    {
        $this->updateSpeciesTotals();
        $this->updatesForFrontPage();
    }

    private function updateSpeciesTotals()
    {
        XC_logger()->logInfo(
            '[Update stats] Update species totals - species'
        );
        $this->query(
            '
            UPDATE taxonomy A
            LEFT JOIN
            (SELECT count( snd_nr ) AS nr, species_nr
            FROM birdsounds
            GROUP BY species_nr) B USING(species_nr)
            SET recordings = IF(B.nr IS NULL, 0, B.nr)'
        );

        XC_logger()->logInfo(
            '[Update stats] Update species totals - background species'
        );
        // background recording numbers
        $this->query(
            '
            UPDATE taxonomy A
            LEFT JOIN
            (SELECT count(snd_nr) AS nr, species_nr FROM birdsounds_background GROUP BY species_nr) B USING(species_nr)
            SET back_recordings = IF(B.nr IS NULL, 0, B.nr)'
        );
    }

    private function updatesForFrontPage()
    {
        $this->createNextTable('latest_statistics');
        $this->createNextTable('latest_species');
        $this->createNextTable('latest_recordists');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo("[Update stats] Updates for front page - $group");
            $sql = "select COUNT(snd_nr) as nrecs, COUNT(DISTINCT(dir)) AS nrecordists FROM birdsounds WHERE group_id = $id";
            $res = $this->query($sql);
            $row = $res->fetch_object();
            $nrrecordings = $row->nrecs;
            $nrrecordists = $row->nrecordists;

            $sql = "select sum(audio_info.length) as duration from audio_info left join birdsounds using (snd_nr)  WHERE birdsounds.group_id = $id";
            $res = $this->query($sql);
            $row = $res->fetch_object();
            $total_duration = intval($row->duration);

            $res = $this->query(
                'SELECT COUNT(species_nr) AS nspecies FROM taxonomy WHERE (recordings > 0 OR back_recordings > 0) AND ' .
                Species::excludePseudoSpeciesSQL('taxonomy') . " AND group_id = $id"
            );
            $row = $res->fetch_object();
            $nrspecies = $row->nspecies;

            $res = $this->query(
                "SELECT count(ssp) as nssp FROM (SELECT species_nr, ssp FROM taxonomy_ssp INNER JOIN birdsounds USING(species_nr, ssp) WHERE birdsounds.group_id = $id GROUP BY species_nr, ssp) A;"
            );
            $row = $res->fetch_object();
            $nrssp = $row->nssp;

            $sql = "
                select count(1) as nr_gbif
                from birdsounds as t1
                left join users as t2 on t1.dir = t2.dir
                left join taxonomy as t3 on t1.species_nr = t3.species_nr
                where t2.third_party_license = 1 and t3.restricted = 0 and t1.group_id = $id";
            $res = $this->query($sql);
            $row = $res->fetch_object();
            $nrgbif = intval($row->nr_gbif);

            $this->query(
                "INSERT INTO latest_statistics_next 
                VALUES($nrrecordings, $total_duration, $nrrecordists, $nrspecies, $nrssp, $nrgbif, $id)"
            );

            //update the latest species table
            $sql = "
                INSERT INTO latest_species_next 
                    SELECT species_nr, min(snd_nr) as nr, $id 
                    FROM birdsounds 
                    WHERE " . Query::ignoreMysteryAndPseudoSpeciesSql() . " AND group_id = $id
                    GROUP BY species_nr 
                    ORDER BY nr DESC";
            $this->query($sql);

            // update the latest recordists table (add group-specific stats, even though
            // they aren't used right now)
            $sql = "
                INSERT INTO latest_recordists_next 
                    SELECT dir, min(snd_nr) as nr, $id 
                    FROM birdsounds 
                    WHERE group_id = $id
                    GROUP BY dir 
                    ORDER BY nr DESC
                    LIMIT 25";
            $this->query($sql);
        }

        $sql = "
            INSERT INTO latest_recordists_next 
                SELECT dir, min(snd_nr) as nr, 0 
                FROM birdsounds 
                GROUP BY dir 
                ORDER BY nr DESC
                LIMIT 25";
        $this->query($sql);

        $this->replaceTable('latest_statistics', 'latest_statistics_next');
        $this->replaceTable('latest_species', 'latest_species_next');
        $this->replaceTable('latest_recordists', 'latest_recordists_next');
    }
}

$task = new UpdateStatsHourlyTask();
$task->run();
