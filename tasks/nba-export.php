<?php

require_once 'xc.lib.php';
require_once 'tools/MinioClient.php';

use Symfony\Component\Finder\Finder;
use xc\tools\MinioClient;

use function xc\XC_logger;

/**
 * Usage:
 * cd to root directory of XC
 * run script: php ./tasks/nba-export.php ['path-to-output-files'] ['1,100,1000,10000,100000']
 *
 * Add a string of xcids as a second argument to run a test drive of just those records.
 *
 * The idea:
 *
 * Query full dataset with dot-separated NBA paths as aliases.
 * Parse each row to a multi-dimensional array based on paths.
 * Output each row to json.
 *
 * Complications:
 *
 * Some fields in the json output are arrays storing multiple
 * values. Solution: create a list of these fields ($nested)
 * and convert on the fly. The current implementation *cannot* handle output
 * with multiple values in those arrays. The only field with two values is
 * associatedMultiMediaUris (in observation) and this is handled as an
 * exception (through createAccessUris)
 *
 * NBA validator does not accept empty values or empty arrays in the output.
 * Solution: as a final step, trim all empty values from the output.
 *
 * Some values need to be cast to integer or double; strings are not
 * accepted. Solution: each value is passed to a method (parseValue)
 * that casts values to types other than strings.
 *
 * Sound files have spaces in their names (yuck). This too trips the validator.
 * Solution: url encoding is cumbersome in MySQL. Path and file are collected
 * separately and joined in PHP, where the file name url-encoded. Separate
 * entities are dismissed before output is written.
 *
 * @author ruud
 */
class NbaExport extends xc\Task
{
    public const UPLOAD_READY = 'upload_ready';

    public const PROCESS_LOCK = 'process_lock';

    public const EXPORT_STARTED = 'nba_export_started';


    private array $minioConfig;

    private bool $testDrive = false;

    private string $testWhere = '';

    private int $nrTestRecords;

    private string $outputPath;

    private string $reportPath;

    private int $batchSize = 2000;

    private $logFh;

    private $minio;

    private $finder;

    private $lastId;

    private $currentId;

    private array $export = [
        'observations' => 'export-XC-specimen-%s-01-01.jsonl',
        'sounds' => 'export-XC-multimedia-%s-01-03.jsonl',
        'spectrograms' => 'export-XC-multimedia-%s-02-03.jsonl',
        'oscillograms' => 'export-XC-multimedia-%s-03-03.jsonl',
    ];

    private array $nested = [
        'gatheringPersons',
        'gatheringEvents',
        'vernacularNames',
        'siteCoordinates',
        'associatedTaxa',
        'associatedMultiMediaUris',
        'serviceAccessPoints',
        'identifications',
    ];

    // Dynamic properties
    private array $dynamic = [
        'recordingDevice',
        'microphone',
        'collectionDate',
        'airTemperatureInCelsius',
    ];

    // Categories to NBA field names
    private array $categoryMap = [
        'observations' => [
            'sex' => 'sex',
            'sound type' => 'gatheringEvent.behavior',
            'recording method' => 'preparationType',
            'life stage' => 'phaseOrStage',
        ],
        'sounds' => [
            'recording method' => 'physicalSetting',
            'sex' => 'sexes',
            'life stage' => 'phasesOrStages',
        ]
    ];

    // Always delete these fields
    private array $delete = ['group_id', 'sample_rate'];

    private array $cc = [
        'CC-by-nc-nd-2.5' => 'CC BY-NC-ND 2.5',
        'CC-by-nc-nd-3.0' => 'CC BY-NC-ND 3.0',
        'CC-by-nc-sa-3.0' => 'CC BY-NC-SA 3.0',
        'CC-by-nc-sa-3.0-us' => 'CC BY-NC-SA 3.0',
        'CC-by-sa-3.0' => 'CC BY-SA 3.0',
        'CC-by-nc-sa-4.0' => 'CC BY-NC-SA 4.0',
        'CC-by-nc-nd-4.0' => 'CC BY-NC-ND 4.0',
        'CC-by-sa-4.0' => 'CC BY-SA 4.0',
        'CC-by-nc-4.0' => 'CC BY-NC 4.0',
        'CC-by-4.0' => 'CC BY 4.0',
        'CC0' => 'CC0',
    ];

    private string $date;

    private array $nrExported = [];

    private int $nrDeleted = 0;

    private array $groups = [];

    //private $factors = [];

    private array $categories = [];

    private array $properties = [];

    private string $observations = "
        select
            t1.genus as 'identifications.scientificName.genusOrMonomial',
            t1.species as 'identifications.scientificName.specificEpithet',
            t1.ssp as 'identifications.scientificName.infraspecificEpithet',
            if (t1.eng_name != '' and t1.eng_name is not null, t1.eng_name, '') 
                as 'identifications.vernacularNames.name',
            if (t1.eng_name != '' and t1.eng_name is not null, 'English', '') 
                as 'identifications.vernacularNames.language',   
            t3.order as 'identifications.defaultClassification.order',
            t1.family as 'identifications.defaultClassification.family',
            'true' as 'identifications.preferred',
            t1.songtype as 'gatheringEvent.behavior',
            t1.recordist as 'gatheringEvent.gatheringPersons.fullName',
            t1.recordist as 'owner',
            t1.olddate as 'gatheringEvent.dateText',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvent.dateTimeBegin',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvent.dateTimeEnd',
            t1.country as 'gatheringEvent.country',
            t1.location as 'gatheringEvent.locality',
            t1.longitude as 'gatheringEvent.siteCoordinates.longitudeDecimal',
            t1.latitude as 'gatheringEvent.siteCoordinates.latitudeDecimal',
            concat(t1.elevation, ' m') as 'gatheringEvent.altitude',
            trim(
                trim(';' from
                    concat(
                        trim(replace(replace(t1.remarks, '\r', '. '), '\n', ' ')),
                        if (`time` REGEXP BINARY '[a-z]', concat('; ', `time`), ''),
                        case
                            when t1.observed = 0 then '; animal seen:no'
                            when t1.observed = 1 then '; animal seen:yes'
                            else ''
                        end,
                        case
                            when t1.playback = 0 then '; playback used:no'
                            when t1.playback = 1 then '; playback used:yes'
                            else ''
                        end
                    )
                )
            ) as 'notes',
            concat_ws(
                ', ', 
                @back:=(select group_concat(scientific separator '|') from birdsounds_background where snd_nr = t1.snd_nr), 
                if(length(t1.back_extra), t1.back_extra, null)
            ) as 'gatheringEvent.associatedTaxa.name',
            if ((@back != '' and @back is not null) or length(t1.back_extra), 'has background sounds', '') 
                as 'gatheringEvent.associatedTaxa.relationType',
            case
                when t1.discussed = 1 then 'identification questioned'
                when t1.discussed = 3 then 'identification unconfirmed'
                else ''
            end as 'identifications.remarks',
            concat('XC', t1.snd_nr) as unitID,
            concat(t1.snd_nr,'@XC') as id,
            t1.snd_nr as sourceSystemId,
            concat('https://data.biodiversitydata.nl/xeno-canto/observation/XC', t1.snd_nr) as unitGUID,
            concat('https://xeno-canto.org/sounds/uploaded/', t1.dir, '/')
                as 'associatedMultiMediaUris.accessUri-path',
            t1.path as 'associatedMultiMediaUris.accessUri-sound-file',
            concat('[type]/XC', t1.snd_nr, '-large.png') as 'associatedMultiMediaUris.accessUri-sonogram-file',
            concat('audio/', right(t1.path, 3)) as 'associatedMultiMediaUris.format',
            'ac:BestQuality' as 'associatedMultiMediaUris.variant',
            trim(concat(t1.genus, ' ', t1.species, ' ', t1.ssp))
                as 'identifications.scientificName.fullScientificName',
            if (t1.ssp != '' and t1.ssp is not null, 'subspecies', 'species') as 'identifications.taxonRank',
            if (t1.latitude != '' and t1.latitude is not null, 'WGS84', '')
                as 'gatheringEvent.siteCoordinates.spatialDatum',
            'XC' as 'sourceSystem.code',
            'Xeno-canto.org - Wildlife sounds' as 'sourceSystem.name',
            'Stichting Xeno-canto voor Natuurgeluiden' as sourceInstitutionID,
            'Xeno-canto' as sourceID,
            'true' as objectPublic,
            'Copyright' as licenseType,
            'CC BY-NC' as license,
            'Wildlife sounds' as collectionType,
            t1.device as recordingDevice,
            t1.microphone as microphone,
            t1.collection_date as collectionDate,
            t1.temperature as airTemperatureInCelsius,            
            if (t1.automatic = 1, 'MachineObservation', 'HumanObservation') as recordBasis,
            t1.collection_specimen as previousUnitsText,
            t1.group_id
       
        from
            birdsounds as t1
        
        left join
            users as t2 on t1.dir = t2.dir
        
        left join
            taxonomy as t3 on t1.species_nr = t3.species_nr
        
        where
            t2.third_party_license = 1 and
            t3.restricted = 0 and 
            t1.snd_nr <= %d %s
            
        order by 
            t1.snd_nr
        
        limit
            %d, %d";

    private string $sounds = "
        select
            t1.genus as 'identifications.scientificName.genusOrMonomial',
            t1.species as 'identifications.scientificName.specificEpithet',
            t1.ssp as 'identifications.scientificName.infraspecificEpithet',
            if (t1.eng_name != '' and t1.eng_name is not null, t1.eng_name, '') 
                as 'identifications.vernacularNames.name',
            if (t1.eng_name != '' and t1.eng_name is not null, 'English', '') 
                as 'identifications.vernacularNames.language',   
            case
                when t1.quality = 1 then 5
                when t1.quality = 2 then 4
                when t1.quality = 3 then 3
                when t1.quality = 4 then 2
                when t1.quality = 5 then 1
            end as rating,
            t1.recordist as 'gatheringEvents.gatheringPersons.fullName',
            t1.recordist as 'creator',
            t1.recordist as 'owner',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvents.dateTimeBegin',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvents.dateTimeEnd',
            t1.olddate as 'gatheringEvents.dateText',
            t1.country as 'gatheringEvents.country',
            t1.location as 'gatheringEvents.sublocality',
            t1.longitude as 'gatheringEvents.siteCoordinates.longitudeDecimal',
            t1.latitude as 'gatheringEvents.siteCoordinates.latitudeDecimal',
            if (t1.latitude != '' and t1.latitude is not null, 'WGS84', '')
                as 'gatheringEvents.siteCoordinates.spatialDatum',
            concat(t1.elevation, ' m') as 'gatheringEvents.altitude',
            t1.license,
            concat('XC', t1.snd_nr, '_snd') as unitID,
            concat(t1.snd_nr,'@XC_snd') as id,
            t1.snd_nr as sourceSystemId,
            concat(t1.snd_nr,'@XC') as associatedSpecimenReference,
            concat('https://data.biodiversitydata.nl/xeno-canto/observation/XC', t1.snd_nr) as recordURI,
            if (t3.length != '' and t3.length is not null, concat(round(t3.length), ' s'), '') as description,
            concat(
                'automatic recording: ', if(t1.automatic = 1, 'yes; ' , 'no; '),
                if(t3.bitrate != '' and t3.bitrate is not null,
                    concat('bitrate: ', t3.bitrate, ' bps; ') , ''),
                if(t3.bitrate_mode != '' and t3.bitrate_mode is not null,
                    concat('bitrate mode: ', t3.bitrate_mode, '; ') , ''),
                if(t3.bits_per_sample != '' and t3.bits_per_sample is not null,
                    concat('bits per sample: ', t3.bits_per_sample, '; ') , ''),
                if(t3.smp != '' and t3.smp is not null,
                    concat('audio sampling rate: ', t3.smp, ' Hz; ') , ''),
                if(t3.channels != '' and t3.channels is not null,
                    concat('number of channels: ', t3.channels, '; ') , ''),
                if(t3.lossless = 0, 'lossy', 'lossless')
            ) as resourceCreationTechnique,
            trim(concat(t1.genus, ' ', t1.species, ' ', t1.ssp))
                as 'identifications.scientificName.fullScientificName',
            'Sound' as 'type',
            'XC' as 'sourceSystem.code',
            'Stichting Xeno-canto voor Natuurgeluiden' as sourceInstitutionID,
            'Xeno-canto' as sourceID,
            'Xeno-canto.org - Wildlife sounds' as 'sourceSystem.name',
            'Copyright' as licenseType,
            'Wildlife sounds' as collectionType,
            concat('https://xeno-canto.org/sounds/uploaded/', t1.dir, '/')
                as 'serviceAccessPoints.accessUri-path',
            t1.path as 'serviceAccessPoints.accessUri-sound-file',
            trim(concat(t1.device, if (t1.device != '' and t1.device is not null, ' | ', ''), t1.microphone)) 
                as captureDevice,
            t1.group_id,
            t3.smp as sample_rate
        
        from
            birdsounds as t1
        
        left join
            users as t2 on t1.dir = t2.dir
        
        left join
            audio_info as t3 on t1.snd_nr = t3.snd_nr

        left join
            taxonomy as t4 on t1.species_nr = t4.species_nr
        
        where
            t2.third_party_license = 1 and
            t4.restricted = 0 and 
            t1.snd_nr <= %d %s
        
        order by 
            t1.snd_nr
        
        limit
            %d, %d";

    private string $spectrograms = "
        select
            t1.genus as 'identifications.scientificName.genusOrMonomial',
            t1.species as 'identifications.scientificName.specificEpithet',
            t1.ssp as 'identifications.scientificName.infraspecificEpithet',            
            if (t1.eng_name != '' and t1.eng_name is not null, t1.eng_name, '') 
                as 'identifications.vernacularNames.name',
            if (t1.eng_name != '' and t1.eng_name is not null, 'English', '') 
                as 'identifications.vernacularNames.language',   
            'Stichting Xeno-canto voor Natuurgeluiden' as 'gatheringEvents.gatheringPersons.fullName',
            'Stichting Xeno-canto voor Natuurgeluiden' as 'creator',
            'Stichting Xeno-canto voor Natuurgeluiden' as 'owner',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvents.dateTimeBegin',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvents.dateTimeEnd',
            t1.olddate as 'gatheringEvents.dateText',
            t1.country as 'gatheringEvents.country',
            t1.location as 'gatheringEvents.sublocality',
            t1.longitude as 'gatheringEvents.siteCoordinates.longitudeDecimal',
            t1.latitude as 'gatheringEvents.siteCoordinates.latitudeDecimal',
            if (t1.latitude != '' and t1.latitude is not null, 'WGS84', '')
                as 'gatheringEvents.siteCoordinates.spatialDatum',
            concat(t1.elevation, ' m') as 'gatheringEvents.altitude',
            t1.license,
            concat('XC', t1.snd_nr, '_img') as unitID,
            concat(t1.snd_nr, '_img@XC') as id,
            t1.snd_nr as sourceSystemId,
            concat(t1.snd_nr,'@XC') as associatedSpecimenReference,
            concat('https://data.biodiversitydata.nl/xeno-canto/observation/XC', t1.snd_nr) as recordURI,
            trim(concat(t1.genus, ' ', t1.species, ' ', t1.ssp))
                as 'identifications.scientificName.fullScientificName',
            'StillImage' as 'type',
            'XC' as 'sourceSystem.code',
            'Stichting Xeno-canto voor Natuurgeluiden' as sourceInstitutionID,
            'Xeno-canto' as sourceID,
            'Xeno-canto.org - Wildlife sounds' as 'sourceSystem.name',
            'Copyright' as licenseType,
            'Wildlife sounds' as collectionType,
            concat('https://xeno-canto.org/sounds/uploaded/', t1.dir, '/ffts/') as 'serviceAccessPoints.accessUri-path',
            concat('XC', t1.snd_nr, '-large.png') as 'serviceAccessPoints.accessUri-sono-file',
            'Spectrogram of the first ten seconds of the sound recording' as caption,
            t1.group_id
        
        from
            birdsounds as t1
        
        left join
            users as t2 on t1.dir = t2.dir
        
        left join
            taxonomy as t3 on t1.species_nr = t3.species_nr
        
        where
            t2.third_party_license = 1 and
            t3.restricted = 0 and 
            t1.snd_nr <= %d %s
        
        order by 
            t1.snd_nr
        
        limit
            %d, %d";

    private string $oscillograms = "
        select
            t1.genus as 'identifications.scientificName.genusOrMonomial',
            t1.species as 'identifications.scientificName.specificEpithet',
            t1.ssp as 'identifications.scientificName.infraspecificEpithet',
            if (t1.eng_name != '' and t1.eng_name is not null, t1.eng_name, '') 
                as 'identifications.vernacularNames.name',
            if (t1.eng_name != '' and t1.eng_name is not null, 'English', '') 
                as 'identifications.vernacularNames.language',   
            'Stichting Xeno-canto voor Natuurgeluiden' as 'gatheringEvents.gatheringPersons.fullName',
            'Stichting Xeno-canto voor Natuurgeluiden' as 'creator',
            'Stichting Xeno-canto voor Natuurgeluiden' as 'owner',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvents.dateTimeBegin',
            concat(t1.date, 'T', lpad(trim(t1.time), 5, 0), ':00') as 'gatheringEvents.dateTimeEnd',
            t1.olddate as 'gatheringEvents.dateText',
            t1.country as 'gatheringEvents.country',
            t1.location as 'gatheringEvents.sublocality',
            t1.longitude as 'gatheringEvents.siteCoordinates.longitudeDecimal',
            t1.latitude as 'gatheringEvents.siteCoordinates.latitudeDecimal',
            if (t1.latitude != '' and t1.latitude is not null, 'WGS84', '')
                as 'gatheringEvents.siteCoordinates.spatialDatum',
            concat(t1.elevation, ' m') as 'gatheringEvents.altitude',
            t1.license,
            concat('XC', t1.snd_nr, '_osc') as unitID,
            concat(t1.snd_nr, '_osc@XC') as id,
            t1.snd_nr as sourceSystemId,
            concat(t1.snd_nr,'@XC') as associatedSpecimenReference,
            concat('https://data.biodiversitydata.nl/xeno-canto/observation/XC', t1.snd_nr) as recordURI,
            trim(concat(t1.genus, ' ', t1.species, ' ', t1.ssp))
                as 'identifications.scientificName.fullScientificName',
            'StillImage' as 'type',
            'XC' as 'sourceSystem.code',
            'Stichting Xeno-canto voor Natuurgeluiden' as sourceInstitutionID,
            'Xeno-canto' as sourceID,
            'Xeno-canto.org - Wildlife sounds' as 'sourceSystem.name',
            'Copyright' as licenseType,
            'Wildlife sounds' as collectionType,
            concat('https://xeno-canto.org/sounds/uploaded/', t1.dir, '/wave/') as 'serviceAccessPoints.accessUri-path',
            concat('XC', t1.snd_nr, '-large.png') as 'serviceAccessPoints.accessUri-sono-file',
            'Oscillogram of the first ten seconds of the sound recording' as caption,
            t1.group_id
        
        from
            birdsounds as t1
        
        left join
            users as t2 on t1.dir = t2.dir
        
        left join
            taxonomy as t3 on t1.species_nr = t3.species_nr
        
        where
            t2.third_party_license = 1 and
            t3.restricted = 0 and 
            t1.snd_nr <= %d %s
        
        order by 
            t1.snd_nr
        
        limit
            %d, %d";

    public function __construct()
    {
        parent::__construct('.');

        $endpoint = NBA_EXPORT_ENDPOINT;
        $region = NBA_EXPORT_REGION;
        $version = NBA_EXPORT_VERSION;
        $key = NBA_EXPORT_KEY;
        $secret = NBA_EXPORT_SECRET;

        if (!$endpoint || !$region || !$version || !$key || !$secret) {
            $this->abort("Missing one or more NBA minio export credentials, please check your .env file.");
        }

        $this->minioConfig = [
            'endpoint' => $endpoint,
            'region' => $region,
            'version' => $version,
            'key' => $key,
            'secret' => $secret,
        ];
        $this->date = date('Ymd');
        $this->setReportPath();
    }

    private function abort($message)
    {
        XC_logger()->logError("[NBA export] " . $message);
        die();
    }

    private function setReportPath()
    {
        $this->reportPath = rtrim(MINIO_NBA, '/') . '/results/';
        if (!is_dir($this->reportPath)) {
            mkdir($this->reportPath);
        }
        return $this->reportPath;
    }

    public function __destruct()
    {
        if (is_resource($this->logFh)) {
            fclose($this->logFh);
        }
    }

    public function run($argv = null)
    {
        // First validate output path
        $this->setOutputPath($argv);
        // Just checking the output?
        $this->setTestDrive($argv);

        // First test if we even need to start;
        // if process_lock or upload_ready files are present, don't bother exporting
        if (!$this->previousUploadProcessed()) {
            $this->abort("Previous upload not processed yet; aborting...\n\n");
        }
        // Download reports from previous session; this also validates the Minio address
        $this->downloadReports();
        // Delete local files from previous session
        $this->deletePreviousExportFiles();

        // Determine total number of records
        if ($this->testDrive) {
            $nrTotal = $this->batchSize = $this->nrTestRecords;
        } else {
            $nrTotal = $this->getTotalRecords();
        }

        // Create properties cache
        $this->setGroupProperties();

        $message = 'Processing ' . number_format($nrTotal) . ' XC records';
        $this->writeLog($message);

        // Set export started flag on Minio server
        $this->putExportStartedFlag($message);

        foreach ($this->export as $query => $file) {
            $batchStart = microtime(true);
            $exported = 0;

            $file = $this->outputPath . sprintf($file, $this->date);
            $fh = fopen($file, 'wb');
            $this->writeLog("Writing data to $file");

            for ($i = 0; $i < $nrTotal; $i += $this->batchSize) {
                $res = $this->db()->query(
                    sprintf($this->{$query}, $this->getLastId(), $this->testWhere, $i, $this->batchSize)
                );
                if ($res) {
                    while ($row = $res->fetch_object()) {
                        $this->currentId = $row->unitID;
                        // Dynamic properties are not part of the main query (too expensive), add them here
                        $this->appendProperties($query, $row);
                        fwrite($fh, $this->rowToJson($row) . "\n");
                        $exported++;
                    }
                }
            }

            fclose($fh);

            $this->nrExported[$query] = $exported;
            $this->writeLog(
                'Export took ' . round((microtime(true) - $batchStart), 2) . "s\n"
            );
        }

        $this->writeLog(number_format($this->nrExported['observations']) . " observations exported");
        $this->writeLog(number_format($nrTotal - $this->nrExported['observations']) . " observations unaccounted for");
        $this->writeLog(number_format($this->nrExported['sounds']) . " recordings exported");
        $this->writeLog(number_format($nrTotal - $this->nrExported['sounds']) . " recordings unaccounted for");
        $this->writeLog(number_format($this->nrExported['spectrograms']) . " spectrograms exported");
        $this->writeLog(number_format($nrTotal - $this->nrExported['spectrograms']) . " spectrograms unaccounted for");
        $this->writeLog(number_format($this->nrExported['oscillograms']) . " oscillograms exported");
        $this->writeLog(number_format($nrTotal - $this->nrExported['oscillograms']) . " oscillograms unaccounted for");

        // Create reports
        // $this->writeDeleteFiles();
        $this->writeIndexFiles();
        // Upload files to Minio
        $this->uploadFiles();
        // We're done
        $this->putMinioReadyFlag();
    }

    private function setOutputPath($argv = [])
    {
        if (empty($argv[1])) {
            $this->abort("Please provide an output path as an argument.\n");
        }
        if (!is_writable($argv[1])) {
            $this->abort("Please make sure output path '" . $argv[1] . "' is writable.\n");
        }
        $this->outputPath = rtrim($argv[1], '/') . '/';
    }

    private function setTestDrive($argv = [])
    {
        if (isset($argv[2])) {
            $this->writeLog("Running test drive with xcids " . $argv[2]);
            $this->testDrive = true;

            $ids = array_map('trim', explode(',', str_replace(['"', '"'], '', $argv[2])));
            $this->nrTestRecords = count($ids);
            $this->testWhere = " AND t1.snd_nr IN (" . implode(',', $ids) . ")";
        }
    }

    private function writeLog($message = '')
    {
        if (!$this->outputPath) {
            $this->abort('Output path not set!');
        }
        if (!$this->logFh) {
            $path = $this->outputPath . 'logs/';
            if (!is_dir($path)) {
                mkdir($path);
            }
            $path .= 'nba-export-' . date('Ymd-His') . '.log';
            $this->logFh = fopen($path, 'wb');
        }
        fwrite($this->logFh, $message . "\n");
        XC_logger()->logInfo("[NBA export] " . $message);
    }

    private function previousUploadProcessed(): bool
    {
        if (!$this->minio) {
            $this->minio = new MinioClient((object)$this->minioConfig);
        }
        foreach (['specimen', 'multimedia'] as $bucket) {
            $this->minio->setBucket($bucket);
            $list = $this->minio->listObjects();

            if (isset($list['Contents'])) {
                foreach ($list['Contents'] as $file) {
                    $key = $file['Key'];
                    if (in_array($key, [self::UPLOAD_READY, self::PROCESS_LOCK])) {
                        $this->writeLog('Export cancelled!');
                        $this->writeLog(
                            'Signal file "' . $key . '" is present in "' . $bucket . '" bucket'
                        );
                        $this->writeLog('Timestamp: ' . $file['LastModified']);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private function downloadReports()
    {
        $this->writeLog('Downloading reports of previous upload from NBA server');
        $reports = $this->getReports();
        if (isset($reports->error)) {
            $this->abort($reports->error);
        }

        if (isset($reports['Contents'])) {
            foreach ($reports['Contents'] as $file) {
                $key = $file['Key'];
                $fh = fopen($this->reportPath . basename($key), 'wb');
                if (fwrite($fh, $this->minio->get($key))) {
                    $this->writeLog(basename($key) . ' successfully saved');
                } else {
                    $this->writeLog("Error! Could not download or save report $file");
                }
                fclose($fh);
            }
        } else {
            $this->writeLog("Error: no NBA reports available?!\n");
        }
        $this->writeLog();
    }

    private function getReports()
    {
        if (!$this->minio) {
            $this->minio = new MinioClient((object)$this->minioConfig);
        }
        $this->minio->setBucket('reports');
        return $this->minio->listObjects(10);
    }

    private function deletePreviousExportFiles()
    {
        foreach ($this->getOutputFiles() as $file) {
            unlink($file->getRealPath());
        }
    }

    private function getOutputFiles()
    {
        if (!$this->finder) {
            $this->finder = new Finder();
        }
        return $this->finder->files()->in($this->outputPath)->depth('== 0');
    }

    private function getTotalRecords()
    {
        $q = '
            select
                count(t1.snd_nr) as nr
            
            from
                birdsounds as t1
            
            left join
                users as t2 on t1.dir = t2.dir
    
               left join
                taxonomy as t3 on t1.species_nr = t3.species_nr
                
            where
                t2.third_party_license = 1 and 
                t3.restricted = 0 and 
                t1.snd_nr <= ' . $this->getLastId();

        $row = $this->db()->query($q)->fetch_object();
        return $row->nr;
    }

    private function getLastId()
    {
        if (!$this->lastId) {
            $row = $this->db()->query('select max(snd_nr) as nr from birdsounds')->fetch_object();
            $this->lastId = (int)$row->nr;
        }
        return $this->lastId;
    }

    private function setGroupProperties()
    {
        $res = query_db(
            "select t4.id as group_id, t4.name as group_name, t3.category, t2.category_id,
                    t2.id as property_id, t2.property, t3.multiple, t4.slow_factor 
                from group_sound_properties as t1
                left join sound_properties as t2 on t1.property_id = t2.id
                left join sound_property_categories as t3 on t2.category_id = t3.id
                left join groups as t4 on t1.group_id = t4.id
                order by t4.id, t3.sort_order, t1.sort_order"
        );
        while ($row = $res->fetch_object()) {
            $this->properties[$row->group_id][$row->category][$row->property_id] = $row->property;
            $this->categories[$row->category_id] = $row->category;
            $this->groups[$row->group_id] = $row->group_name;
            //$this->factors[$row->group_id] = $row->slow_factor;
        }
        $this->groups[0] = 'Soundscape';
    }

    private function putExportStartedFlag($message = false)
    {
        $path = $this->outputPath . self::EXPORT_STARTED;
        $fh = fopen($path, 'wb');

        fwrite($fh, 'NBA export started at ' . date('Y-m-d H:i:s') . "\n\n");
        fwrite($fh, $message);
        fclose($fh);

        $this->putMinioFlags(self::EXPORT_STARTED);
    }

    private function putMinioFlags($flag = false)
    {
        if (!$flag) {
            $this->writeLog('No Minio flag name to add provided');
            return false;
        }
        $file = $this->outputPath . $flag;

        // Use c flag in order not to truncate non-empty flag files
        if (!$fh = fopen($file, 'c')) {
            $this->writeLog('Cannot create Minio flag file');
            return false;
        }

        foreach (['specimen', 'multimedia'] as $bucket) {
            $this->minio->setBucket($bucket);
            $this->minio->put($file);
        }

        fclose($fh);
        unlink($file);
    }

    private function appendProperties($query, &$data): void
    {
        $res = query_db(
            'select category_id, property_id from birdsounds_properties where snd_nr = ' . $data->sourceSystemId
        );
        $properties = [];
        while ($row = $res->fetch_object()) {
            $category = $this->categories[$row->category_id] ?? null;
            if ($category && isset($this->categoryMap[$query][$category])) {
                $value = $this->properties[$data->group_id][$category][$row->property_id] ?? null;
                if ($value) {
                    // Exceptions here...
                    if (($category == 'sex' || $category == 'sexes') && $value == 'uncertain') {
                        $value = 'undetermined';
                    }
                    if ($query == 'sounds' && $category == 'recording method') {
                        $value = $value == "field recording" ? "Natural" : "Artificial";
                    }
                    $properties[$this->categoryMap[$query][$category]][] = $value;
                }
            }
        }
        foreach ($properties as $nbaField => $values) {
            // These should stay arrays
            if (in_array($nbaField, ['sexes', 'phasesOrStages'])) {
                $data->{$nbaField} = (array)$values;
            } elseif ($nbaField == 'sex' && count($values) > 1) {
                $data->{$nbaField} = 'mixed';
            } elseif (!empty($data->{$nbaField})) {
                $data->{$nbaField} = implode(', ', $values) . ', ' . $data->{$nbaField};
            } else {
                $data->{$nbaField} = implode(', ', $values);
            }
        }
    }

    private function rowToJson($row = null)
    {
        $output = [];
        foreach ($row as $k => $v) {
            $this->arrayByPath($output, $k, $v);
        }

        if (isset($output)) {
            $this->convertToDynamic($output);
            $this->removeEmptyFromArray($output);
            $this->createAccessUris($output);
            $this->fixStartEndDate($output);
            $this->setCollectionType($output);
            $this->deleteRedundantFields($output);
            return json_encode($output);
        }
        return null;
    }

    private function arrayByPath(&$array, $path, $value, $separator = '.')
    {
        $keys = explode($separator, $path);
        foreach ($keys as $key) {
            if (in_array($key, $this->nested)) {
                $array = &$array[$key][0];
            } else {
                $array = &$array[$key];
            }
        }
        $array = $this->parseValue($key, $value);
        return $array;
    }

    private function parseValue($key = null, $value = null)
    {
        $value = is_string($value) ? trim($value) : $value;
        // Specific fields
        if (strpos($key, 'Decimal') !== false && !is_null($value) && $value != '') {
            return floatval($value);
        }
        if ($key == 'rating' && !empty($value)) {
            return intval($value);
        }
        if (strpos($key, 'dateTime') !== false && !empty($value)) {
            return $this->fixDateTime($value);
        }
        if ($key == 'license' && !strpos($key, ' ')) {
            return $this->cc[$value] ?? $value;
        }
        // ... and values
        switch ($value) {
            case 'true':
                return true;
            case 'false':
                return false;
            default:
                return $value;
        }
    }

    private function fixDateTime($date)
    {
        // Regex below does not catch 0000-12-17 as invalid. First discard those dates
        if (strpos($date, '0000-') !== false) {
            //$this->writeLog("\tError: " . $this->currentId . ' contains an invalid date (' . $date . ')');
            return null;
        }
        if (preg_match(
                '/^-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\.[0-9]+)?|(24:00:00(\.0+)?))(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$/',
                $date
            ) > 0) {
            return $date;
        }
        // Does not validate to ISO8601; remove time string and test date.
        // Return null when date is invalid
        $newDate = substr($date, 0, -9);
        $check = DateTime::createFromFormat('Y-m-d', $newDate);
        if ($check && $check->format('Y-m-d') === $newDate) {
            //$this->writeLog("\tWarning: " . $this->currentId . ' contains an incorrect time (' . $date . ')');
            return $newDate;
        }
        //$this->writeLog("\tError: " . $this->currentId . ' contains an invalid date (' . $date . ')');
        return null;
    }

    private function convertToDynamic(&$output)
    {
        $dynamic = [];
        foreach ($this->dynamic as $field) {
            if (isset($output[$field])) {
                if (!empty($output[$field]) && strlen($output[$field])) {
                    $dynamic[$field] = $output[$field];
                }
                unset($output[$field]);
            }
        }
        if (!empty($dynamic)) {
            $output['dynamicProperties'] = json_encode((object)$dynamic);
        }
    }

    private function removeEmptyFromArray(&$array)
    {
        foreach ($array as $key => &$value) {
            if (is_array($value)) {
                $value = $this->removeEmptyFromArray($value);
                if (!$value) {
                    unset($array[$key]);
                }
                continue;
            }
            if (empty($value) && !is_numeric($value)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    private function createAccessUris(&$output)
    {
        // Sonograms
        if (isset($output['serviceAccessPoints'][0]['accessUri-sono-file'])) {
            $output['serviceAccessPoints'][0]['accessUri'] = $output['serviceAccessPoints'][0]['accessUri-path'] . rawurlencode(
                    $output['serviceAccessPoints'][0]['accessUri-sono-file']
                );
            $output['serviceAccessPoints'][0]['variant'] = 'ac:MediumQuality';
            $output['serviceAccessPoints'][0]['format'] = 'image/png';
        }

        // Sound
        if (isset($output['serviceAccessPoints'][0]['accessUri-sound-file'])) {
            $soundFile = $output['serviceAccessPoints'][0]['accessUri-sound-file'];
            $format = $this->getSoundExtension($soundFile);

            $output['serviceAccessPoints'][0]['accessUri'] = $output['serviceAccessPoints'][0]['accessUri-path'] . rawurlencode(
                    $soundFile
                );
            $output['serviceAccessPoints'][0]['variant'] = 'ac:BestQuality';
            $output['serviceAccessPoints'][0]['format'] = "audio/$format";

            // Just export original for the time being

//            // Regular mp3 for wav
//            $slowFactor = $this->factors[$output['group_id']] ?? null;
//            $sampleRate = $output['sample_rate'] ?? 0;

//            if ($format == 'wav') {
//                $output['serviceAccessPoints'][1]['accessUri'] =
//                    $output['serviceAccessPoints'][0]['accessUri-path'] .
//                    rawurlencode(str_replace(['.WAV', '.wav'], ".mp3", $soundFile));
//                $output['serviceAccessPoints'][1]['variant'] = 'ac:BestQuality';
//                $output['serviceAccessPoints'][1]['format'] = 'audio/mp3';
//            }
//
//            // Slowed down mp3
//            if ($slowFactor && $sampleRate >= 48000 && $slowFactor > 1) {
//                $output['serviceAccessPoints'][2]['accessUri'] =
//                    $output['serviceAccessPoints'][0]['accessUri-path'] .
//                    rawurlencode(str_replace(['.WAV', '.wav'], "-{$slowFactor}x.mp3", $soundFile));
//                $output['serviceAccessPoints'][2]['variant'] = 'ac:BestQuality';
//                $output['serviceAccessPoints'][2]['format'] = 'audio/mp3';
//            }
        }
        // Specimen
        if (isset($output['associatedMultiMediaUris'][0]['accessUri-sound-file'])) {
            // Sound
            $output['associatedMultiMediaUris'][0]['accessUri'] = $output['associatedMultiMediaUris'][0]['accessUri-path'] . rawurlencode(
                    $output['associatedMultiMediaUris'][0]['accessUri-sound-file']
                );
            $output['associatedMultiMediaUris'][0]['variant'] = 'ac:BestQuality';
            $output['associatedMultiMediaUris'][0]['format'] = 'audio/' . $this->getSoundExtension(
                    $output['associatedMultiMediaUris'][0]['accessUri-sound-file']
                );
            // Spectrogram
            $output['associatedMultiMediaUris'][1]['accessUri'] = $output['associatedMultiMediaUris'][0]['accessUri-path'] . str_replace(
                    '[type]',
                    'ffts',
                    $output['associatedMultiMediaUris'][0]['accessUri-sonogram-file']
                );
            $output['associatedMultiMediaUris'][1]['variant'] = 'ac:MediumQuality';
            $output['associatedMultiMediaUris'][1]['format'] = 'image/png';
            // Oscillogram
            $output['associatedMultiMediaUris'][2]['accessUri'] = $output['associatedMultiMediaUris'][0]['accessUri-path'] . str_replace(
                    '[type]',
                    'wave',
                    $output['associatedMultiMediaUris'][0]['accessUri-sonogram-file']
                );
            $output['associatedMultiMediaUris'][2]['variant'] = 'ac:MediumQuality';
            $output['associatedMultiMediaUris'][2]['format'] = 'image/png';
        }
        // Discard temp keys
        unset(
            $output['serviceAccessPoints'][0]['accessUri-path'], $output['serviceAccessPoints'][0]['accessUri-file'], $output['serviceAccessPoints'][0]['accessUri-sono-file'], $output['serviceAccessPoints'][0]['accessUri-sound-file'], $output['associatedMultiMediaUris'][0]['accessUri-path'], $output['associatedMultiMediaUris'][0]['accessUri-sound-file'], $output['associatedMultiMediaUris'][0]['accessUri-sonogram-file']
        );

        return $output;
    }

    private function getSoundExtension($file)
    {
        $ext = trim(pathinfo($file, PATHINFO_EXTENSION)) ?: 'mp3';
        return strtolower($ext);
    }

    private function fixStartEndDate(&$output)
    {
        //die($output['gatheringEvent']['dateTimeBegin'] . "\n");
        if (isset($output['gatheringEvent']['dateTimeBegin']) && strlen(
                $output['gatheringEvent']['dateTimeBegin']
            ) < 15) {
            $output['gatheringEvent']['dateTimeBegin'] .= 'T00:00:00';
            $output['gatheringEvent']['dateTimeEnd'] .= 'T23:59:00';
        } // No time appended
        elseif (isset($output['gatheringEvents'][0]['dateTimeBegin']) && strlen(
                $output['gatheringEvents'][0]['dateTimeBegin']
            ) < 15) {
            $output['gatheringEvents'][0]['dateTimeBegin'] .= 'T00:00:00';
            $output['gatheringEvents'][0]['dateTimeEnd'] .= 'T23:59:00';
        }
        return $output;
    }

    private function setCollectionType(&$output)
    {
        if ($output['identifications'][0]['scientificName']['fullScientificName'] == 'Sonus naturalis') {
            $output['collectionType'] .= ' - Soundscape';
        } else {
            $output['collectionType'] .= ' - ' . $this->groupName($output['group_id']);
        }
        return $output;
    }

    private function groupName($id)
    {
        return ucfirst(strtolower($this->groups[$id]));
    }

    private function deleteRedundantFields(&$output)
    {
        foreach ($this->delete as $field) {
            if (isset($output[$field])) {
                unset($output[$field]);
            }
        }
    }

    private function writeIndexFiles()
    {
        $fh1 = fopen(
            $this->outputPath . 'index-XC-specimen-' . $this->date . '.txt',
            'wb'
        );
        $fh2 = fopen(
            $this->outputPath . 'index-XC-multimedia-' . $this->date . '.txt',
            'wb'
        );

        fwrite(
            $fh1,
            sprintf($this->export['observations'], $this->date) . "\t" . $this->nrExported['observations'] . "\n"
        );
        fwrite($fh1, "delete-XC-specimen-$this->date.txt\t$this->nrDeleted\n");

        fwrite(
            $fh2,
            sprintf($this->export['sounds'], $this->date) . "\t" . $this->nrExported['sounds'] . "\n"
        );
        fwrite(
            $fh2,
            sprintf($this->export['spectrograms'], $this->date) . "\t" . $this->nrExported['spectrograms'] . "\n"
        );
        fwrite(
            $fh2,
            sprintf($this->export['oscillograms'], $this->date) . "\t" . $this->nrExported['oscillograms'] . "\n"
        );

        fwrite(
            $fh2,
            "delete-XC-multimedia-$this->date.txt\t" . (2 * $this->nrDeleted) . "\n"
        );

        fclose($fh1);
        fclose($fh2);
    }

    private function uploadFiles()
    {
        if (!$this->minio) {
            $this->minio = new MinioClient((object)$this->minioConfig);
        }

        $this->writeLog("\n\nFile uploads");

        foreach ($this->getOutputFiles() as $file) {
            $file = $file->getRealPath();
            $bucket = !strpos($file, 'multimedia') ? 'specimen' : 'multimedia';

            $this->minio->setBucket($bucket);
            $result = $this->minio->put($file);

            $this->writeLog(basename($file) . " successfully uploaded at $result->created");

            if (!empty($result->error)) {
                $this->writeLog('Upload failed, aborting NBA export');
                die();
            }
        }
    }

    private function putMinioReadyFlag()
    {
        $this->putMinioFlags(self::UPLOAD_READY);
        $this->deleteMinioFlags(self::EXPORT_STARTED);
    }

    private function deleteMinioFlags($flag = false)
    {
        if (!$flag) {
            $this->writeLog('No Minio flag name to delete provided');
            return false;
        }
        foreach (['specimen', 'multimedia'] as $bucket) {
            $this->minio->setBucket($bucket);
            $this->minio->delete($flag);
        }
    }

    private function writeDeleteFiles()
    {
        $res = $this->db()->query('select snd_nr as id from birdsounds_trash');
        $fh1 = fopen($this->outputPath . 'delete-XC-specimen-' . $this->date . '.txt', 'wb');
        $fh2 = fopen($this->outputPath . 'delete-XC-multimedia-' . $this->date . '.txt', 'wb');

        while ($row = $res->fetch_object()) {
            fwrite($fh1, $row->id . "@XC\n");
            fwrite($fh2, $row->id . "_snd@XC\n");
            fwrite($fh2, $row->id . "_img@XC\n");
            $this->nrDeleted++;
        }

        fclose($fh1);
        fclose($fh2);
    }

}

function app()
{
    static $task = null;
    if (!$task) {
        $task = new NbaExport();
    }
    return $task;
}


app()->run($argv);
