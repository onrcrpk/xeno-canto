<?php

require_once 'vendor/autoload.php';

use xc\Task;

class ExtractDatabaseStrings extends Task
{

    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        echo("<?php\n");
        echo("\$strings = array(\n");

        /*
        // country names
            $countries = getCountryList();
        foreach ($countries as $country)
        {
            echo("_(\"{$country['name']}\"),\n");
        }
         */

        // 'new stuff' tips
        $res = $this->db()->query('SELECT title, text FROM newstuff');
        while ($row = $res->fetch_object()) {
            $title = addcslashes($row->title, '"');
            $text  = addcslashes($row->text, '"');
            echo("_(\"$title\"),\n");
            echo("_(\"$text\"),\n");
        }

        echo(");\n");

        echo("?>\n");
    }

}

$task = new ExtractDatabaseStrings();
$task->run();
