<?php

require_once 'vendor/autoload.php';
require_once 'xc.lib.php';

use xc\Task;
use function xc\XC_logger;

class UpdateStatsCommon extends Task
{
    protected $log;

    protected $redis;

    protected $groups = [];

    public function __construct()
    {
        parent::__construct('.');
    }

    protected function createNextTable($table)
    {
        $this->query("DROP TABLE IF EXISTS `{$table}_next`");
        $this->query("CREATE TABLE `{$table}_next` LIKE `$table`");
    }

    protected function query($sql = '')
    {
        try {
            $res = $this->db()->query($sql) or
            XC_logger()->logError($sql . "\n" . $this->db()->error . "\n");
            return $res;
        } catch (Exception $e) {
            XC_logger()->logError($sql . "\n" . $e->getMessage() . "\n");
        }
    }

    protected function insertId()
    {
        return $this->db()->insertId();
    }

    protected function getGroups()
    {
        if (empty($this->groups)) {
            $res = $this->query("SELECT id, name FROM groups");
            while ($row = $res->fetch_object()) {
                $this->groups[$row->id] = $row->name;
            }
        }
        return $this->groups;
    }

    protected function replaceTable($old, $new)
    {
        $this->query("DROP TABLE IF EXISTS $old");
        $this->query("RENAME TABLE $new TO $old");
    }

    protected function tableIsEmpty($table)
    {
        $res = $this->query(
            "SELECT EXISTS (SELECT 1 FROM `$table`) AS `has_data`"
        );
        $row = $res->fetch_object();
        return $row->has_data == 0;
    }

}
