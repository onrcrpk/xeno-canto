<?php

require_once 'vendor/autoload.php';
require_once 'xc.lib.php';
require_once 'tools/MP3File.php';

use xc\Recording;

use function xc\escape;
use function xc\XC_logger;

class VerifySoundsTask extends xc\Task
{
    public function __construct()
    {
        parent::__construct('.');
    }

    public function run($clearResults = false)
    {
        $res = $this->db()->query('SELECT snd_nr FROM birdsounds');
        XC_logger()->logInfo(
            '[Verify sounds] Checking ' . $res->num_rows . " sound files"
        );

        if ($clearResults) {
            $this->db()->query('TRUNCATE TABLE birdsounds_missing');
        }

        $this->db()->query('SELECT snd_nr FROM birdsounds');
        while ($row = $res->fetch_object()) {
            // localize notification emails
            $rec = Recording::load($row->snd_nr);
            $filePath = $rec->filePath();
            $reason = false;

            if (!file_exists($filePath)) {
                $reason = "file not found: $filePath";
            } else {
                $getID3 = new getID3();
                $f = $getID3->analyze($filePath);
                if (!isset($f['fileformat']) || !in_array(
                        $f['fileformat'],
                        ['mp3', 'wav']
                    )) {
                    // Extra check in case getID3 fails to recognize valid mp3 file.
                    // Modified class MP3File returns 0 for duration of false mp3 files.
                    try {
                        $mp3file = new MP3File($filePath);
                        if ($mp3file->getDurationEstimate() == 0) {
                            $reason = "not a valid sound file: $filePath";
                        }
                    } catch (Exception $e) {
                        $reason = "not a valid sound file; crashed MP3File: $filePath";
                    }
                }
            }

            if ($reason) {
                XC_logger()->logError(
                    "[Verify sounds] $reason (xcid: " . $row->snd_nr . ')'
                );
                $this->db()->query(
                    "
                    INSERT INTO birdsounds_missing (`snd_nr`, `checked`, `reason`) 
                    VALUES (" . $row->snd_nr . ", NOW(), '" . escape(
                        $reason
                    ) . "')"
                );
            }
        }
    }
}

function app()
{
    static $task = null;
    if (!$task) {
        $task = new VerifySoundsTask(true);
    }
    return $task;
}

app()->run();
