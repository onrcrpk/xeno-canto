<?php

// Usage example: php tasks/batch-upload.php PLNKGHYEDF '/data/minio/batch/PLNKGHYEDF/metadata.csv' dry

require_once 'xc.lib.php';

use xc\Controllers\UploadBase;
use xc\Species;
use xc\XCLogger;

use function xc\escape;
use function xc\getUrl;

class BatchUploadTask extends xc\Task
{

    private $log;

    private $dryRun = true;

    private $properties = [];

    private $XC_Logger;

    private $fields = [
        'filename',
        'latitude',
        'longitude',
        'country',
        'location name',
        'elevation',
        'recording date',
        'time of day',
        'license',
        'recordist name',
        'recording device',
        'microphone',
        'type',
        'automatic recording',
        'genus',
        'species',
        'subspecies',
        'sex(es)',
        'life stage(s)',
        'animal seen?',
        'sound type(s)',
        'recording method',
        'collection date',
        'playback used?',
        'temperature',
        'background species',
        'collection specimen reference',
        'remarks',
        'quality'
    ];

    private $map = [];

    private $hasErrors;

    private $files = [];

    public function __construct()
    {
        parent::__construct('.');
        $this->XC_Logger = new XCLogger('XC');
    }

    public function run($argv)
    {
        ini_set('auto_detect_line_endings', true);

        // Correct number of arguments provided (4 expected)
        if (count($argv) < 4) {
            return $this->log($this->usage($argv[0]), 2);
        }
        // Check if this is a dry run
        if (!in_array($argv[3], ['dry', 'up'])) {
            return $this->log("Incorrect action parameter (should be 'dry' or 'up')", 2);
        } elseif ($argv[3] == 'up') {
            $this->dryRun = false;
        }
        // Check if directory name corresponds to a valid user id
        $userid = escape($argv[1]);
        $res = $this->db()->query("SELECT dir from users where dir='$userid'");
        if (!$res || $res->num_rows == 0) {
            return $this->log("User '$userid' does not exist", 2);
        }
        // Check upload directory
        $uploaddir = $this->soundsUploadDir($userid);
        if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
            return $this->log("Couldn't create upload dir '$uploaddir'", 2);
        }
        if (!is_writable($uploaddir)) {
            return $this->log("User upload dir '$uploaddir' is not writable", 2);
        }
        // Check if csv file is valid
        $metadata = $argv[2];
        if (!is_file($metadata)) {
            return $this->log("$metadata is missing or not a valid file", 2);
        }
        $datadir = dirname($metadata);
        // ... and can be opened
        try {
            $hdata = fopen($metadata, 'r');
        } catch (Exception $e) {
            return $this->log("$metadata cannot be opened", 2);
        }

        $this->setSoundProperties();

        $n = 0;
        $records = [];

        // Let's go
        while (($line = fgetcsv($hdata)) !== false) {
            // Check header
            if ($n == 0) {
                $headerError = $this->checkHeaderFields($line);
                if ($headerError) {
                    return $this->log($headerError, 2);
                }
                $this->map = array_flip($this->fields);
                $n++;
                continue;
            }

            $line = array_map('trim', $line);
            $skip = false;

            // Keep track and set when processing recording methods; saves a lookup later ;)
            $inStudio = false;

            // Species or soundscape?
            $type = strtolower($line[$this->map['type']]);
            if (!in_array($type, ['single species', 'soundscape'])) {
                $this->log("Checking line $n", 0);
                $this->lineError($n, "$type is not a valid sound type, must be 'single species' or 'soundscape'");
                $n++;
                continue;
            }

            // Species
            if ($type == 'single species') {
                $genus = escape($line[$this->map['genus']]);
                $species = escape(strtolower($line[$this->map['species']]));

                $this->log("Checking line $n: $genus $species", 0);

                // Check if species name is valid; if so, fetch its number and group id
                $res = $this->db()->query(
                    "SELECT species_nr, group_id from taxonomy WHERE genus='$genus' AND species='$species'"
                );
                if (!$res || $res->num_rows == 0) {
                    $this->lineError($n, "Couldn't find species '$genus $species' in the database");
                    $n++;
                    continue;
                }
                $row = $res->fetch_object();
                $speciesNr = $row->species_nr;
                $groupId = $row->group_id;
            } else {
                $this->log("Checking line $n: soundscape", 0);
                $speciesNr = Species::soundscapeSpeciesNumber();
                $groupId = 0;
            }

            $fd = new xc\RecordingFormData();
            $fd->speciesNr = $speciesNr;

            $quality = $line[$this->map['quality']];
            switch ($quality) {
                case '1':
                case 'a':
                case 'A':
                    $fd->quality = 1;
                    break;
                case '2':
                case 'b':
                case 'B':
                    $fd->quality = 2;
                    break;
                case '3':
                case 'c':
                case 'C':
                    $fd->quality = 3;
                    break;
                case '4':
                case 'd':
                case 'D':
                    $fd->quality = 4;
                    break;
                case '5':
                case 'e':
                case 'E':
                    $fd->quality = 5;
                    break;
                default:
                    $fd->quality = 0;
            }
            if ($fd->quality == 0) {
                $this->lineError($n, "Invalid quality value: '$quality'");
                $skip = true;
            }

            $fd->recordist = $line[$this->map['recordist name']];

            $dt = ($line[$this->map['recording date']]) ?? '?';
            if (!xc\UploadStep::validateDateString($dt)) {
                $this->lineError($n, "Invalid date string '{$dt}'");
                $skip = true;
            }
            $fd->recordingDate = $dt;

            $tm = $line[$this->map['time of day']];
            if (empty($tm)) {
                $tm = '?';
            }
            if (!xc\UploadStep::validateTimeString($tm)) {
                $this->lineError($n, "Invalid time string '$tm'");
                $skip = true;
            }
            $fd->time = $tm;

            $fd->lat = '';
            if ($line[$this->map['latitude']] != '') {
                $fd->lat = (string)round(floatval($line[$this->map['latitude']]), 4);
                if (!preg_match(
                    '/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/',
                    $fd->lat
                )) {
                    $this->lineError($n, "Invalid latitude '{$line[$this->map['latitude']]}'");
                    $skip = true;
                }
            }
            $fd->lng = '';
            if ($line[$this->map['longitude']] != '') {
                $fd->lng = (string)round(floatval($line[$this->map['longitude']]), 4);
                if (!preg_match(
                    '/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/',
                    $fd->lng
                )) {
                    $this->lineError($n, "Invalid longitude '{$line[$this->map['longitude']]}'");
                    $skip = true;
                }
            }
            $fd->location = $line[$this->map['location name']];

            $cnt = $line[$this->map['country']];
            if (!xc\UploadStep::validateCountryString($cnt)) {
                $this->lineError($n, "Invalid country '$cnt'");
                $skip = true;
            }

            $fd->playbackUsed = strtolower($line[$this->map['playback used?']]);
            if (!$this->validRadioProperty($fd->playbackUsed, true)) {
                $message = "%s option for 'playback used?'%s";
                if ($fd->playbackUsed == '') {
                    $message = sprintf($message, 'Missing', '');
                } else {
                    $message = sprintf($message, 'Invalid', ": '$fd->playbackUsed'");
                }
                $this->lineError($n, $message);
                $skip = true;
            }

            $fd->animalSeen = strtolower($line[$this->map['animal seen?']]);
            if (!$this->validRadioProperty($fd->animalSeen, true)) {
                $message = "%s option for 'animal seen?'%s";
                if ($fd->animalSeen == '') {
                    $message = sprintf($message, 'Missing', '');
                } else {
                    $message = sprintf($message, 'Invalid', ": '$fd->animalSeen'");
                }
                $this->lineError($n, $message);
                $skip = true;
            }

            $fd->automatic = strtolower($line[$this->map['automatic recording']]);
            if (!$this->validRadioProperty($fd->automatic, true)) {
                $message = "%s option for 'automatic'%s";
                if ($fd->automatic == '') {
                    $message = sprintf($message, 'Missing', '');
                } else {
                    $message = sprintf($message, 'Invalid', ": '$fd->automatic'");
                }
                $this->lineError($n, $message);
                $skip = true;
            }

            $fd->country = $cnt;
            $fd->device = $line[$this->map['recording device']];
            $fd->microphone = $line[$this->map['microphone']];
            $fd->elevation = $line[$this->map['elevation']] ?: '?';
            $fd->remarks = $line[$this->map['remarks']];

            // Soundscape must have background species
            $fd->bgSpecies = $line[$this->map['background species']];
            if ($speciesNr == Species::soundscapeSpeciesNumber() && !$fd->bgSpecies) {
                $this->lineError($n, "Soundscapes must have one or more background species");
                $skip = true;
            }

            $f = $line[$this->map['filename']];
            if (!is_file("$datadir/$f")) {
                $this->lineError($n, "File '$f' does not exist");
                $skip = true;
            } elseif (in_array($f, $this->files)) {
                $this->lineError($n, "File name '$f' is already used, names must be unique");
                $skip = true;
            }
            $fd->filename = "$datadir/$f";
            $this->files[] = $f;

            $lic = xc\License::lookupByName($line[$this->map['license']]);
            if (!isset($lic->id)) {
                $this->lineError($n, "Invalid license '{$line[$this->map['license']]}'");
                $skip = true;
            } else {
                $fd->license = $lic->id;
            }

            // Relevant only for regular species uploads (i.e. not soundscapes)
            if ($speciesNr != Species::soundscapeSpeciesNumber()) {
                $properties = [];
                $extra = [];

                $soundTypes = $this->getPropertyValues($line[$this->map['sound type(s)']]);
                $validSoundTypes = $this->validProperties($groupId, 'sound type');
                foreach ($soundTypes as $soundType) {
                    if (!in_array($soundType, $validSoundTypes)) {
                        $extra[] = $soundType;
                        $this->log(
                            "$soundType not a pre-determined sound type, will be saved as text string",
                            0
                        );
                    } else {
                        $properties[] = array_search($soundType, $validSoundTypes);
                    }
                    $fd->soundTypeExtra = implode(', ', $extra);
                }

                $sexes = $this->getPropertyValues($line[$this->map['sex(es)']]);
                $validSexes = $this->validProperties($groupId, 'sex');
                foreach ($sexes as $sex) {
                    if (!in_array($sex, $validSexes)) {
                        $this->lineError($n, "Invalid sex(es) value '$sex'");
                        $skip = true;
                    } else {
                        $properties[] = array_search($sex, $validSexes);
                    }
                }

                $lifeStages = $this->getPropertyValues($line[$this->map['life stage(s)']]);
                $validLifeStages = $this->validProperties($groupId, 'life stage');
                foreach ($lifeStages as $stage) {
                    if (!in_array($stage, $validLifeStages)) {
                        $this->lineError($n, "Invalid life stage(s) value '$stage'");
                        $skip = true;
                    } else {
                        $properties[] = array_search($stage, $validLifeStages);
                    }
                }

                $recordingMethod = strtolower($line[$this->map['recording method']]);
                $validRecordingMethods = $this->validProperties($groupId, 'recording method');
                if (!in_array($recordingMethod, $validRecordingMethods)) {
                    $this->lineError($n, "Invalid recording method value '$recordingMethod'");
                    $skip = true;
                } else {
                    $properties[] = array_search($recordingMethod, $validRecordingMethods);
                    if ($recordingMethod == 'studio recording') {
                        $inStudio = true;
                    }
                }

                $fd->groupId = $groupId;
                $fd->soundProperties[$groupId] = array_unique($properties);

                $fd->ssp = strtolower($line[$this->map['subspecies']]);
                $fd->specimen = $line[$this->map['collection specimen reference']];

                $temperature = $line[$this->map['temperature']];
                if ($temperature && $temperature !== '0') {
                    $temp = floatval($temperature);
                    if ($temp == 0 || $temp < -90 || $temp > 60) {
                        $this->lineError(
                            $n,
                            "Invalid temperature value '$temperature', should be in degrees Celsius."
                        );
                        $skip = true;
                    }
                }
                $fd->temperature = $temperature;

                $collectionDate = $line[$this->map['collection date']];
                if ($inStudio) {
                    $message = false;
                    if (!$collectionDate) {
                        $message = "Collection date required for studio recordings; enter 0000-00-00 for date unknown";
                    } elseif (!xc\UploadStep::validateDateString($collectionDate)) {
                        $message = "Invalid collection date string '$collectionDate'";
                    } elseif ($fd->recordingDate != '?' && $collectionDate != '?' && $collectionDate > $fd->recordingDate) {
                        $message = "Collection date must be earlier than recording date";
                    }
                    if ($message) {
                        $this->lineError($n, $message);
                        $skip = true;
                    }
                    $fd->collectionDate = $collectionDate;
                } elseif ($collectionDate != '') {
                    $this->lineError(
                        $n,
                        "Collection date '$collectionDate' is valid only in combination with studio recording method"
                    );
                    $skip = true;
                }
            }
            if (!$skip) {
                $records[] = $fd;
            }
            $n++;
        }

        // Dry run
        if ($this->dryRun) {
            $this->log .= "\n";
            $this->log(
                'Ready for import: ' . count($records) . '; skipped with errors: ' . (($n - 1) - count($records))
            );
        }

        // Upload
        if (!$this->dryRun && !$this->hasErrors) {
            $n = 0;
            $total = count($records);
            $uploaded = [];
            $this->log .= "\n";
            foreach ($records as $record) {
                $n++;
                $this->log("Uploading recording $n of $total");
                $xcid = UploadBase::finishUpload($record, $userid);
                if (!$xcid) {
                    $this->lineError($n, 'Upload failed :(');
                    return $this->log;
                }
                $uploaded[] = $xcid;
            }

            $first = reset($uploaded);
            $last = end($uploaded);

            $this->log .= "\n\nSuccesfully added " . count($uploaded) . " recordings! ";
            $this->log .= "<a href='" . getUrl('browse') . "?query=nr:$first-$last'>Show the uploaded recordings</a>.";
        }

        if (!empty($this->log)) {
            return $this->log;
        }
    }

    private function log($message, $level = 0)
    {
        if ($level == 0) {
            $this->XC_Logger->logInfo("[Batch upload] $message");
            $this->log .= "INFO --> $message<br>";
        } elseif ($level == 1) {
            $this->XC_Logger->logWarn("[Batch upload] $message");
            $this->log .= "<span style='color: red;'>WARNING --> $message</span><br>";
        } else {
            $this->XC_Logger->logError("[Batch upload] $message");
            $this->log .= "<span style='color: red;'>ERROR --> $message</span><br>";
            $this->hasErrors = true;
            return $this->log;
        }
    }

    private function usage($scriptname)
    {
        return "Usage: $scriptname USERID PATH/TO/METADATA.CSV dry/up\n\nThe mp3 files must be in the same directory as the CSV file that describes the recordings.\n";
    }

    private function setSoundProperties()
    {
        $res = query_db(
            "select t4.id as group_id, t4.name as group_name, t4.name as group_name, t3.category,
                    t2.id as property_id, t2.property, t3.multiple 
                from group_sound_properties as t1
                left join sound_properties as t2 on t1.property_id = t2.id
                left join sound_property_categories as t3 on t2.category_id = t3.id
                left join groups as t4 on t1.group_id = t4.id
                order by t4.id, t3.sort_order, t1.sort_order"
        );
        while ($row = $res->fetch_object()) {
            $this->properties[$row->group_id][$row->category][$row->property_id] = $row->property;
        }
    }

    private function checkHeaderFields($header)
    {
        if (!empty(array_diff_assoc($header, $this->fields))) {
            return "Column names in metadata.csv are not as expected</p>
                <p>Current header:" . $this->createList($header) . "</p>
                <p>Expected header:" . $this->createList($this->fields) . '</p>';
        }
        return false;
    }

    private function createList($array)
    {
        $output = '<ol>';
        foreach ($array as $v) {
            $output .= "<li>$v</li>";
        }
        return $output . '</ol>';
    }

    private function lineError($line, $message)
    {
        $this->log("line $line: $message", 2);
    }

    private function validRadioProperty($val, $required = false)
    {
        $valid = in_array($val, ['yes', 'no', 'unknown']);
        $empty = $val == '';
        if (($required && (!$valid || $empty)) || (!$required && (!$empty && !$valid))) {
            return false;
        }
        return true;
    }

    private function getPropertyValues($string)
    {
        return array_filter(array_map('strtolower', array_map('trim', explode(',', $string))),
            fn($value) => trim($value) !== "");
    }

    private function validProperties($group, $category)
    {
        return $this->properties[$group][$category];
    }
}

// For standalone use
if (!function_exists('app')) {
    function app()
    {
        static $task = null;
        if (!$task) {
            $task = new BatchUploadTask();
        }
        return $task;
    }

    app()->run($argv);
}
