<?php

require_once 'vendor/autoload.php';
require_once 'xc.lib.php';
require_once 'tasks/update-stats-common.php';

use xc\Query;
use xc\WorldArea;
use xc\XCRedis;

use function xc\XC_logger;

class UpdateDailyStatsTask extends UpdateStatsCommon
{
    private $recordists = [];

    public function __construct()
    {
        parent::__construct('.');
    }

    public function __destruct()
    {
        // Clear Redis cache when done
        XC_logger()->logInfo('[Update stats] Clearing Redis cache');
        $this->redis = new XCRedis();
        $this->redis->clearDailyStatsCache();
    }

    public function run()
    {
        $this->updateGraphStatistics();
        $this->updateWorldAreaSummary();
        $this->updateWorldAreaNewSpecies();
        $this->updateWorldAreaCountries();
        $this->updateWorldAreaRecordists();
        $this->updateWorldAreaCommonSpecies();
        $this->updateRecordistSummary();
        $this->updateRecordistCountries();
        $this->updateRecordistCommonSpecies();
        $this->updateRecordistUniqueSpecies();
    }

    private function updateGraphStatistics()
    {
        $this->createNextTable('stats_date');

        foreach ($this->getGroups() as $id => $group) {
            $res = $this->query(
                "
                SELECT YEAR(MIN(datetime)) AS `year`, MONTH(MIN(datetime)) AS `month`
                FROM birdsounds 
                WHERE datetime AND group_id = $id"
            );
            $row = $res->fetch_object();
            if ($row && !is_null($row->year)) {
                $ystart = $row->year;
                $mstart = $row->month;
                $yend = date('Y');
                $mend = date('m');
                for ($y = $ystart; $y <= $yend; $y++) {
                    if ($y == $ystart) {
                        $m = $mstart;
                    } else {
                        $m = 1;
                    }
                    XC_logger()->logInfo("[Update stats] Update graph statistics for $group ($y)");
                    for (; $m <= 12; $m++) {
                        $insert = "
                            INSERT INTO stats_date_next
                                SELECT
                                NULL,
                                '$y-$m-1',
                                COUNT(snd_nr),
                                COUNT(DISTINCT dir),
                                COUNT(DISTINCT CASE WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN species_nr END),
                                SUM(IF(neotropics = 'Y', 1, 0)),
                                SUM(IF(asia = 'Y', 1, 0)),
                                SUM(IF(europe = 'Y', 1, 0)),
                                SUM(IF(africa = 'Y', 1, 0)),
                                SUM(IF(australia = 'Y', 1, 0)),
                                0,
                                0,
                                $id
                                FROM birdsounds 
                                WHERE (YEAR(datetime) < $y OR (MONTH(datetime) <= $m AND YEAR(datetime) = $y)) AND group_id = $id";
                        $this->query($insert);

                        // FIXME: make this more efficient
                        $update = "
                            UPDATE stats_date_next 
                            SET active_recordists = (
                                SELECT COUNT(DISTINCT dir) 
                                FROM birdsounds 
                                WHERE YEAR(datetime) = $y AND MONTH(datetime) = $m AND group_id = $id
                            ),
                            active_species = (
                                SELECT COUNT(DISTINCT CASE WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN species_nr END)
                                FROM birdsounds 
                                WHERE YEAR(datetime) = $y AND MONTH(datetime) = $m AND group_id = $id
                            )
                            WHERE id = " . $this->insertId();
                        $this->query($update);

                        if (($y == $yend) && ($m == $mend)) {
                            break;
                        }
                    }
                }
            }
        }
        $this->replaceTable('stats_date', 'stats_date_next');
    }

    private function updateWorldAreaSummary()
    {
        $this->createNextTable('wa_collection_stats');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo('[Update stats] Update world area - summary for ' . $group);
            foreach (WorldArea::all() as $area) {
                $sql = "
                INSERT INTO wa_collection_stats_next
                    SELECT '{$area->branch}',
                        COUNT(*),
                        COUNT(DISTINCT case WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN birdsounds . species_nr END),
                        COUNT(DISTINCT dir),
                        if (SUM(audio_info . length) IS NULL, 0, SUM(audio_info . length)),
                        COUNT(DISTINCT country),
                        COUNT(DISTINCT location),
                        $id
                    FROM birdsounds
                    INNER JOIN audio_info USING(snd_nr)
                    WHERE {$area->birdsoundsColumn} = 'Y' and group_id = $id";
                $this->query($sql);
            }
        }
        $this->replaceTable('wa_collection_stats', 'wa_collection_stats_next');
    }

    private function updateWorldAreaNewSpecies()
    {
        $this->createNextTable('wa_new_species');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo('[Update stats] Update world area - new species for ' . $group);
            foreach (WorldArea::all() as $area) {
                $statsSql = "
                    INSERT INTO wa_new_species_next
                        SELECT null, '{$area->branch}', species_nr, min(birdsounds . snd_nr) as nr, $id
                        FROM birdsounds
                        WHERE " . Query::ignoreMysteryAndPseudoSpeciesSql() . " AND {$area->birdsoundsColumn} = 'Y' 
                            AND group_id = $id
                        group by species_nr
                        ORDER BY nr DESC 
                        LIMIT 20";
                $this->query($statsSql);
            }
        }
        $this->replaceTable('wa_new_species', 'wa_new_species_next');
    }

    private function updateWorldAreaCountries()
    {
        $this->createNextTable('wa_countries');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo('[Update stats] Update world area - countries for ' . $group);
            foreach (WorldArea::all() as $area) {
                $sql = "
                    INSERT INTO wa_countries_next
                        SELECT null,
                        '{$area->branch}',
                        country,
                        COUNT(*),
                        COUNT(DISTINCT case WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN birdsounds . species_nr END),
                        COUNT(DISTINCT dir),
                        if (SUM(audio_info . length) IS NULL, 0, SUM(audio_info . length)),
                        $id
                        FROM birdsounds
                        LEFT JOIN audio_info USING(snd_nr)
                        WHERE {$area->birdsoundsColumn} = 'Y' and group_id = $id
                        GROUP BY birdsounds . country";
                $this->query($sql);
            }
        }
        $this->replaceTable('wa_countries', 'wa_countries_next');
    }

    private function updateWorldAreaRecordists()
    {
        $this->createNextTable('wa_recordists');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo('[Update stats] Update world area - recordists for ' . $group);
            foreach (WorldArea::all() as $area) {
                $sql = "
                    INSERT INTO wa_recordists_next
                        SELECT null,
                        '{$area->branch}',
                        birdsounds . dir,
                        COUNT(*) as nrecs,
                        COUNT(DISTINCT CASE WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN birdsounds . species_nr END),
                        SUM(audio_info . length),
                        $id
                        FROM birdsounds
                        INNER JOIN audio_info USING(snd_nr)
                        WHERE {$area->birdsoundsColumn} = 'Y' and group_id = $id
                        GROUP BY birdsounds . dir
                        ORDER BY nrecs DESC LIMIT 30";
                $this->query($sql);
            }
        }
        $this->replaceTable('wa_recordists', 'wa_recordists_next');
    }

    private function updateWorldAreaCommonSpecies()
    {
        $this->createNextTable('wa_common_species');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo(
                '[Update stats] Update world area - common species'
            );
            foreach (WorldArea::all() as $area) {
                $sql = "
                    INSERT INTO wa_common_species_next
                        SELECT null,
                        '{$area->branch}',
                        species_nr,
                        COUNT(*) as nrecs,
                        $id
                        FROM birdsounds
                        WHERE " . Query::ignoreMysteryAndPseudoSpeciesSql() . " AND {$area->birdsoundsColumn} = 'Y' 
                            AND group_id = $id
                        GROUP BY birdsounds.species_nr
                        ORDER BY nrecs DESC LIMIT 30";
                $this->query($sql);
            }
        }
        $this->replaceTable('wa_common_species', 'wa_common_species_next');
    }

    private function updateRecordistSummary()
    {
        $this->createNextTable('rec_summary_stats');
        $this->createUniqueSpeciesTable();

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo('[Update stats] Update recordist - summary for ' . $group);

            $sql = "
                INSERT INTO rec_summary_stats_next
                    SELECT dir,
                    COUNT(snd_nr),
                    COUNT(DISTINCT CASE WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN birdsounds.species_nr END),
                    0,
                    IF(SUM(audio_info.length) IS NULL, 0, SUM(audio_info.length)),
                    COUNT(DISTINCT country),
                    COUNT(DISTINCT location),
                    0,
                    $id
                    FROM users 
                    LEFT JOIN birdsounds USING(dir)
                    LEFT JOIN audio_info USING(snd_nr)
                    WHERE birdsounds.group_id = $id
                    GROUP BY dir";
            $this->query($sql);

            $sql = "
                UPDATE rec_summary_stats_next
                INNER JOIN (SELECT COUNT(*) as N, userid FROM tmp_rec_unique_species WHERE group_id = $id GROUP BY userid) A
                USING(userid)
                SET nunique = A.N";
            $this->query($sql);
        }

        XC_logger()->logInfo('[Update stats] Update recordist - summary for all groups');
        // Add a group-independent extra row with group id = 0
        $sql = "
            INSERT INTO rec_summary_stats_next
                SELECT dir,
                COUNT(snd_nr),
                COUNT(DISTINCT CASE WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN birdsounds.species_nr END),
                (IFNULL((SELECT SUM(nunique) FROM rec_summary_stats_next WHERE userid = users.dir), 0)),
                IF(SUM(audio_info.length) IS NULL, 0, SUM(audio_info.length)),
                COUNT(DISTINCT country),
                COUNT(DISTINCT location),
                0,
                0
                FROM users 
                LEFT JOIN birdsounds USING(dir)
                LEFT JOIN audio_info USING(snd_nr)
                GROUP BY dir";
        $this->query($sql);

        XC_logger()->logInfo('[Update stats] Update recordist - summary for forum');
        // Forum contribution should be added to this row
        $sql = '
            UPDATE rec_summary_stats_next
            INNER JOIN (SELECT users.dir AS userid, A.narticle, B.nforum FROM users
            LEFT JOIN (
                select dir,COUNT(mes_nr) as narticle from feature_discussions GROUP BY dir
            ) A USING(dir)
            LEFT JOIN (
                SELECT dir,COUNT(mes_nr) as nforum from discussion_forum_world GROUP BY dir
            ) B USING(dir)) C USING(userid)
            SET ncomments = (IFNULL(C.narticle,0) + IFNULL(C.nforum,0)) WHERE group_id = 0';
        $this->query($sql);

        $this->replaceTable('rec_summary_stats', 'rec_summary_stats_next');
    }

    private function createUniqueSpeciesTable()
    {
        $this->query(
            'CREATE TEMPORARY TABLE IF NOT EXISTS `tmp_rec_unique_species` (
            `species_nr` VARCHAR(10) NOT NULL ,
            `userid` VARCHAR(20) NOT NULL ,
            `group_id` TINYINT(4) NOT NULL ,
            UNIQUE INDEX (`species_nr`, `group_id`))
            DEFAULT CHARACTER SET = utf8'
        );
        if ($this->tableIsEmpty('tmp_rec_unique_species')) {
            foreach ($this->getGroups() as $id => $group) {
                $this->query(
                    "
                    INSERT INTO tmp_rec_unique_species
                        SELECT species_nr, dir, $id 
                        FROM birdsounds  
                        WHERE " . Query::ignoreMysteryAndPseudoSpeciesSql() . " AND group_id = $id
                        GROUP BY species_nr 
                        HAVING COUNT(DISTINCT dir) = 1"
                );
            }
        }
    }

    private function updateRecordistCountries()
    {
        $this->createNextTable('rec_countries');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo('[Update stats] Update recordist - countries for ' . $group);
            foreach ($this->getRecordistsForGroup($id) as $recordist) {
                $sql = "
                INSERT INTO rec_countries_next
                    SELECT null,
                    '$recordist',
                    country,
                    COUNT(*),
                    COUNT(DISTINCT CASE WHEN " . Query::ignoreMysteryAndPseudoSpeciesSql() . " THEN birdsounds.species_nr END),
                    if (SUM(audio_info.length) IS NULL, 0, SUM(audio_info.length)),
                    $id
                    FROM birdsounds
                    LEFT JOIN audio_info USING(snd_nr)
                    WHERE dir = '$recordist' AND group_id = $id
                    GROUP BY country";
                $this->query($sql);
            }
        }

        $this->replaceTable('rec_countries', 'rec_countries_next');
    }

    private function getRecordistsForGroup($id)
    {
        if (!isset($this->recordists[$id])) {
            $res = $this->query('SELECT DISTINCT dir from birdsounds WHERE group_id = ' . $id);
            if ($res->num_rows > 0) {
                while ($row = $res->fetch_object()) {
                    $this->recordists[$id][] = $row->dir;
                }
            } else {
                $this->recordists[$id] = [];
            }
        }
        return $this->recordists[$id];
    }

    private function updateRecordistCommonSpecies()
    {
        $this->createNextTable('rec_common_species');

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo(
                '[Update stats] Update recordist - common species for group ' . $group
            );
            foreach ($this->getRecordistsForGroup($id) as $recordist) {
                $sql = "
                    INSERT INTO rec_common_species_next
                        SELECT null,
                        '$recordist',
                        species_nr,
                        COUNT(snd_nr) as nrecs,
                        $id
                        FROM birdsounds
                        WHERE dir = '$recordist' AND " . Query::ignoreMysteryAndPseudoSpeciesSql() . "
                            AND group_id = $id
                        GROUP BY species_nr
                        ORDER BY nrecs DESC 
                        LIMIT 30";
                $this->query($sql);
            }
        }
        $this->replaceTable('rec_common_species', 'rec_common_species_next');
    }

    private function updateRecordistUniqueSpecies()
    {
        $this->createNextTable('rec_unique_species');
        $this->createUniqueSpeciesTable();

        foreach ($this->getGroups() as $id => $group) {
            XC_logger()->logInfo(
                '[Update stats] Update recordist - unique species for group ' . $group
            );
            foreach ($this->getRecordistsForGroup($id) as $recordist) {
                $sql = "
                    INSERT INTO rec_unique_species_next
                        SELECT NULL,
                        '$recordist',
                        species_nr, 
                        COUNT(*),
                        $id
                        FROM tmp_rec_unique_species
                        INNER JOIN birdsounds USING(species_nr)
                        WHERE userid = '$recordist' AND " . Query::ignoreMysterySql() . " 
                            AND tmp_rec_unique_species.group_id = $id
                        GROUP BY species_nr";
                $this->query($sql);
            }
        }
        $this->replaceTable('rec_unique_species', 'rec_unique_species_next');
    }
}

// Ruud 03-10-17: added debugging and logging; UpdateStatsTask(true) to enable
$task = new UpdateDailyStatsTask();
$task->run();
