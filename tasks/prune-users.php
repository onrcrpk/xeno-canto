<?php

require_once 'vendor/autoload.php';
require_once 'xc.lib.php';

use function xc\XC_logger;

class PruneUsers extends xc\Task
{
    private $sql = "
        %s
        
        FROM 
            users_next AS t1
        
        LEFT JOIN 
            birdsounds AS t2 ON t1.dir = t2.dir
        LEFT JOIN 
            forum_world AS t3 ON t1.dir = t3.userid
        LEFT JOIN 
            datasets AS t4 ON t1.dir = t4.userid
        
        WHERE 
            t1.blurb = '' AND
            t1.region = '' AND
            t1.verified = 0 AND 
            t1.contactform = 1 AND 
            t1.language = 'en' AND
            t1.num_per_page = 30 AND
            t1.third_party_license = -1 AND
            t1.joindate < DATE_SUB(NOW(),INTERVAL 1 YEAR) AND
            t2.snd_nr IS NULL AND
            t3.topic_nr IS NULL AND
            t4.datasetid IS NULL     
    ";

    private $lock = "
        LOCK TABLES
            users WRITE,
            users_next WRITE,
            users_next AS t1 WRITE,
            users_pruned WRITE,
            users_pruned_next WRITE,
            birdsounds AS t2 WRITE,
            forum_world AS t3 WRITE,
            datasets AS t4 WRITE
    ";

    public function __construct()
    {
        parent::__construct('.');
    }

    public function __destruct()
    {
        $this->db()->query("UNLOCK TABLES");
    }

    public function run()
    {
        XC_logger()->logInfo(
            "[Prune users] Creating temporary tables"
        );

        $this->copyTable('users', 'users_next');
        $this->copyTable('users_pruned', 'users_pruned_next');

        $this->db()->query($this->lock);

        $total = $this->count('SELECT COUNT(*) as nr FROM users_next');

        XC_logger()->logInfo(
            "[Prune users] Checking $total members for inactive users to prune"
        );

        $pruned = $this->count('SELECT COUNT(*) as nr FROM users_pruned_next');
        $prunable = $this->count(sprintf($this->sql, 'SELECT COUNT(*) as nr'));

        if ($prunable > 0) {
            XC_logger()->logInfo(
                "[Prune users] Migrating $prunable prunable users to temporary table"
            );

            $query = "INSERT INTO users_pruned_next (" . sprintf($this->sql, 'SELECT t1.*') . ')';
            try {
                $this->db()->query($query);
            } catch (Exception $e) {
                $this->abort("cannot store pruned users: $e");
            }

            $newlyPruned = $this->count('SELECT COUNT(*) as nr FROM users_pruned_next') - $pruned;

            if ($newlyPruned == $prunable) {
                XC_logger()->logInfo("[Prune users] Deleting users from temporary table");
                $this->db()->query(sprintf($this->sql, 'DELETE t1.*'));

                $newTotal = $this->count('SELECT COUNT(*) as nr FROM users_next');

                if ($newTotal + $prunable == $total) {
                    XC_logger()->logInfo(
                        "[Prune users] Backing up main tables and renaming temporary tables"
                    );
                    $this->db()->query("UNLOCK TABLES");
                    $this->db()->query(
                        'RENAME TABLE users TO users_backup_' . date("Ymd_Gi")
                    );
                    $this->db()->query('RENAME TABLE users_pruned TO users_pruned_backup_' . date("Ymd_Gi"));
                    $this->db()->query('RENAME TABLE users_next TO users');
                    $this->db()->query('RENAME TABLE users_pruned_next TO users_pruned');
                    XC_logger()->logInfo('[Prune users] Pruning complete');
                } else {
                    $this->abort('not all prunable users stored');
                }
            } else {
                $this->abort('not all prunable users stored');
            }
        } else {
            XC_logger()->logInfo("[Prune users] No prunable users");
            $this->abort();
        }
    }

    private function copyTable($from, $to)
    {
        $this->db()->query("DROP TABLE IF EXISTS $to");
        $this->db()->query("CREATE TABLE $to LIKE $from");
        $this->db()->query("INSERT INTO $to SELECT * FROM $from");
    }

    private function count($query)
    {
        $res = $this->db()->query($query);
        return $res->fetch_object()->nr;
    }

    private function abort($message = false)
    {
        if ($message) {
            XC_logger()->logError("[Prune users] Aborting: $message");
        }
        $this->db()->query("UNLOCK TABLES");
        $this->db()->query("DROP TABLE IF EXISTS users_next");
        $this->db()->query("DROP TABLE IF EXISTS users_pruned_next");
        die();
    }
}

function app()
{
    static $task = null;
    if (!$task) {
        $task = new PruneUsers();
    }
    return $task;
}

app()->run();
