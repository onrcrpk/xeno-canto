<?php

require_once 'vendor/autoload.php';
require_once 'xc.lib.php';

use xc\Language;
use xc\NotificationPeriod;
use xc\Query;
use xc\Recording;
use xc\SortDirection;
use xc\XCMail;

use function xc\XC_logger;

function sinceValue($timeperiod)
{
    if ($timeperiod === NotificationPeriod::DAILY) {
        return 1;
    }
    if ($timeperiod === NotificationPeriod::WEEKLY) {
        return 7;
    }
    if ($timeperiod === NotificationPeriod::MONTHLY) {
        $now       = new DateTime();
        $beginning = new DateTime();
        $beginning->sub(DateInterval::createFromDateString('1 month'));
        $interval = $now->diff($beginning);
        return intval($interval->format('%a'));
    }
}

class MailNotificationTask extends xc\Task
{

    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        $timeperiods = [NotificationPeriod::DAILY];
        $dayweek     = date('D');
        if ($dayweek == 'Mon') {
            $timeperiods[] = NotificationPeriod::WEEKLY;
        }
        $daymonth = date('j');
        if ($daymonth == '1') {
            $timeperiods[] = NotificationPeriod::MONTHLY;
        }

        foreach ($timeperiods as $timeperiod) {
            $res = $this->db()->query(
                "
                SELECT notifications.*, users.email, users.username, users.language
                FROM notifications
                INNER JOIN users USING(dir)
                WHERE timeperiod = $timeperiod"
            );
            XC_logger()->logInfo(
                '[Mail notifications] Sending ' . $res->num_rows . ' mails for period ' . NotificationPeriod::$periods[$timeperiod]
            );
            while ($row = $res->fetch_object()) {
                // localize notification emails
                $lang = Language::lookup($row->language);
                $this->setLanguage($lang);
                $query = "{$row->match_string}";
                // many people used 'idae' in the past to match anything, but this
                // doesn't match anything anymore, so simulate the old behavior.
                if (strcasecmp(trim($query), 'idae') == 0) {
                    $query = '';
                }
                if ($row->branch !== 'world') {
                    $query .= " area:{$row->branch}";
                }

                $this->maybeSendNotification(
                    $query,
                    $timeperiod,
                    $row->username,
                    $row->email
                );
            }
        }
    }

    private function maybeSendNotification(
        $qstring,
        $timeperiod,
        $username,
        $email
    ) {
        $sinceDays   = sinceValue($timeperiod);
        $fullQstring = "$qstring since:$sinceDays";

        $q = new Query($fullQstring);
        $q->setOrder('xc', SortDirection::REVERSE);
        $q->setPageSize(50);

        $nRecordings = $q->numRecordings();
        $nSpecies    = $q->numSpecies();

        if (!$nRecordings) {
            return null;
        }

        $res = $q->execute(1);

        $message = sprintf(_('Dear %s,'), $username) . "\n\n" .
                   wordwrap(
                       sprintf(
                           _(
                               "There were %d new recordings from %d species added to xeno-canto in the last %d days that match your notification search string '%s'."
                           ),
                           $nRecordings,
                           $nSpecies,
                           $sinceDays,
                           $qstring
                       ),
                       68
                   ) . "\n\n";

        if ($nRecordings > 50) {
            $message .= wordwrap(
                            _(
                                'Below you will find the first 50 recordings. To view all of them, go to:'
                            ),
                            68
                        ) .
                        "\n\n{$this->getUrl('browse', ['query' => $fullQstring], true)}\n\n";
        }

        while ($row = $res->fetch_object()) {
            $rec     = new Recording($row);
            $message .= "  {$rec->URL(true)} - {$rec->species()->displayName()} - {$rec->recordist()}\n";
        }
        $message .= "\n";
        $message .= wordwrap(
                        _(
                            'To change your notifications, add new ones, or change the frequency with which you get these emails, go to:'
                        ),
                        68
                    ) . "\n\n";
        $message .= "  {$this->getUrl('mypage', ['p' => 'notifications'], true)}\n\n";
        $message .= _('Make sure you are logged in first.') . "\n\n";
        $message .= _('Happy recording,') . "\n";
        $message .= "xeno-canto.org\n";

        (new XCMail(
            $email,
            '[xeno-canto] ' . _('New recording notification'),
            $message
        ))->send();
    }

}

function app()
{
    static $task = null;
    if (!$task) {
        $task = new MailNotificationTask();
    }
    return $task;
}

app()->run();
