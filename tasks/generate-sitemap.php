<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'xc.lib.php';

use function xc\XC_logger;

class SitemapCreator extends xc\Task
{

    private $sitemapUrls = [];

    private $sitemapMediaUrls = [];

    private $files = [];

    private $totalRecordings;

    private $outputDir;

    private $baseFile;

    private $baseUrl = 'https://xeno-canto.org/';

    private $linksPerSitemap = 45000;

    private $xmlWriter;

    public function __construct()
    {
        parent::__construct('.');
        $this->setOutputDir();
        $this->initXmlWriter();
    }

    public function setOutputDir()
    {
        $this->outputDir = dirname(__DIR__) . '/sitemaps/';
    }

    private function initXmlWriter()
    {
        $this->xmlWriter = new XMLWriter();
        $this->xmlWriter->openMemory();
        $this->xmlWriter->setIndent(true);
        $this->xmlWriter->setIndentString('   ');
    }

    public function __destruct()
    {
        if ($this->xmlWriter) {
            unset($this->xmlWriter);
        }
    }

    public function run()
    {
        $this->bootstrap();
        $this->deleteOldFiles();
        $this->writeSpecies();
        $this->writeRecordings();
        $this->writeIndex();
    }

    private function bootstrap()
    {
        if (!file_exists($this->outputDir) && !mkdir(
                $this->outputDir
            ) && !is_dir($this->outputDir)) {
            XC_logger()->logError(
                '[Sitemaps] Cannot create directory ' . $this->outputDir
            );
        }
        // Test output directory
        if (!is_writable($this->outputDir)) {
            XC_logger()->logError(
                '[Sitemaps] ' . $this->outputDir . " is not writable!"
            );
        }
    }

    private function deleteOldFiles()
    {
        array_map('unlink', glob($this->outputDir . 'sitemap*'));
    }

    private function writeSpecies()
    {
        $this->getSpeciesUrls();
        $this->baseFile = 'species';
        $this->writeSitemapFiles(1);
    }

    private function getSpeciesUrls()
    {
        $res = $this->db()
                    ->query(
                        "SELECT species_nr, genus, species FROM taxonomy WHERE species != ''"
                    );
        while ($row = $res->fetch_object()) {
            $this->sitemapUrls[$row->species_nr] = "species/{$row->genus}-{$row->species}";
        }
    }

    private function writeSitemapFiles($i)
    {
        $fileName = "sitemap_{$this->baseFile}_$i.xml";
        XC_logger()->logInfo('[Sitemaps] Writing ' . $fileName);

        $this->xmlWriter->startDocument('1.0', 'UTF-8');
        $this->xmlWriter->startElement('urlset');
        $this->xmlWriter->writeAttribute(
            'xmlns',
            'http://www.sitemaps.org/schemas/sitemap/0.9'
        );
        $this->xmlWriter->writeAttribute(
            'xmlns:image',
            'http://www.google.com/schemas/sitemap-image/1.1'
        );

        foreach ($this->sitemapUrls as $id => $url) {
            $this->xmlWriter->startElement('url');
            $this->xmlWriter->writeElement('loc', $this->baseUrl . $url);

            if (isset($this->sitemapMediaUrls[$id])) {
                $media = $this->sitemapMediaUrls[$id];
                $this->xmlWriter->startElement('image:image');
                $this->xmlWriter->writeElement(
                    'image:loc',
                    $this->baseUrl . $media['loc']
                );
                if (!empty($media['caption'])) {
                    $this->xmlWriter->writeElement(
                        'image:caption',
                        $media['caption']
                    );
                }
                $this->xmlWriter->endElement();
            }

            $this->xmlWriter->endElement();
        }

        $this->xmlWriter->endElement();
        file_put_contents(
            $this->outputDir . $fileName,
            $this->xmlWriter->flush(true),
            FILE_APPEND
        );

        $this->files[] = $fileName;
        $this->resetUrls();
    }

    private function resetUrls()
    {
        $this->sitemapUrls = $this->sitemapMediaUrls = [];
    }

    private function writeRecordings()
    {
        $this->baseFile = 'recording';
        $set            = 1;
        for (
            $i = 0; $i <= $this->totalRecordings(); $i += $this->linksPerSitemap
        ) {
            $this->getRecordingUrls($i);
            $this->writeSitemapFiles($set);
            $set++;
        }
    }

    private function totalRecordings()
    {
        if (!$this->totalRecordings) {
            $query                 = '
                    SELECT COUNT(1) as total FROM birdsounds AS t1
                    LEFT JOIN taxonomy AS t2 on t1.species_nr = t2.species_nr
                    WHERE t2.restricted = 0';
            $this->totalRecordings = $this->db()->query($query)->fetch_object(
            )->total;
        }
        return $this->totalRecordings;
    }

    private function getRecordingUrls($offset = 0)
    {
        $query = "
                SELECT t1.snd_nr, t1.dir, t1.songtype, t1.genus, t1.species, t1.country FROM birdsounds AS t1
                LEFT JOIN taxonomy AS t2 on t1.species_nr = t2.species_nr
                WHERE t2.restricted = 0
                LIMIT $offset, {$this->linksPerSitemap}";

        $res = $this->db()->query($query);
        while ($row = $res->fetch_object()) {
            $this->sitemapUrls[$row->snd_nr]      = $row->snd_nr;
            $this->sitemapMediaUrls[$row->snd_nr] = [
                'loc'     => "sounds/uploaded/{$row->dir}/ffts/XC{$row->snd_nr}-full.png",
                'caption' => "Sonogram of {$row->genus} {$row->species} from {$row->country} (" .
                             htmlentities(strtolower($row->songtype)) . ')',
            ];
        }
    }

    private function writeIndex()
    {
        XC_logger()->logInfo('[Sitemaps] Writing index');
        $this->xmlWriter->startDocument('1.0', 'UTF-8');
        $this->xmlWriter->startElement('sitemapindex');
        $this->xmlWriter->startAttribute('xmlns');
        $this->xmlWriter->text('http://www.sitemaps.org/schemas/sitemap/0.9');
        $this->xmlWriter->endAttribute();
        foreach ($this->files as $file) {
            $this->xmlWriter->startElement('sitemap');
            $this->xmlWriter->writeElement('loc', $this->baseUrl . $file);
            $this->xmlWriter->writeElement('lastmod', date('Y-m-d'));
            $this->xmlWriter->endElement();
        }
        $this->xmlWriter->endElement();
        file_put_contents(
            $this->outputDir . 'sitemap_index.xml',
            $this->xmlWriter->flush(true),
            FILE_APPEND
        );
    }

}


$smc = new SitemapCreator();
$smc->run();
