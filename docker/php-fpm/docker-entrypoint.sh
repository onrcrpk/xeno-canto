#!/bin/sh
set -e

/bin/chown www-data:www-data /cache
exec "$@"
