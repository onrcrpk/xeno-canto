<?php

namespace xc\tools;

require 'vendor/autoload.php';

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Exception;
use stdClass;

class MinioClient
{

    private $client;

    private $config;

    private $bucket;

    private $result;

    public function __construct($config = false)
    {
        // RemoteStorage construct
        if (!$config) {
            throw new Exception('Error! No config provided for Minio client');
        }
        $this->config = $config;
        // Basic validation
        foreach (
            [
                'version',
                'region',
                'key',
                'secret',
                'endpoint',
            ] as $setting
        ) {
            if (!isset($this->config->{$setting}) || empty($this->config->{$setting})) {
                throw new Exception(
                    'Error! ' . $setting .
                    ' setting missing from Minio config'
                );
            }
        }
        $this->initClient();
        return $this;
    }

    public function listObjects($max = false)
    {
        $this->initResult();
        $max = !$max ? 1000 : (int)$max;
        try {
            $this->result = $this->client->listObjects(
                [
                    'Bucket'  => $this->getBucket(),
                    'MaxKeys' => $max,
                ]
            );
        } catch (S3Exception $e) {
            $this->result->error = 'Error! Could list objects: ' . $e->getMessage(
                );
        }
        return $this->result;
    }

    public function getBucket()
    {
        return $this->bucket;
    }

    public function setBucket($bucket)
    {
        $this->bucket = $bucket;
        return $this;
    }

    public function get($key = false)
    {
        if (!$key) {
            $this->result->error = 'Error! No file name provided to get from Minio server';
            return $this->result;
        }
        $this->initResult();
        try {
            $this->result = $this->client->getObject(
                [
                    'Bucket' => $this->getBucket(),
                    'Key'    => $key,
                ]
            );
        } catch (S3Exception $e) {
            $this->result->error = "Error! Could not get $key: " . $e->getMessage(
                );
        }
        return $this->result;
    }

    public function put($file = false)
    {
        if (!$file || !is_file($file)) {
            $this->result->error = "Error! Could not put $file to Minio; file does not exist";
            return $this->result;
        }
        $this->initResult();
        try {
            $minioResult            = $this->client->putObject(
                [
                    'Bucket'        => $this->bucket,
                    'Key'           => basename($file),
                    'SourceFile'    => $file,
                    'ContentSHA256' => hash_file('sha256', $file),
                ]
            );
            $info                   = $minioResult->get('@metadata');
            $this->result->minioUri = $info['effectiveUri'] ?? null;
            $this->result->created  = isset($info['headers']['date']) ?
                date('Y-m-d H:i:s', strtotime($info['headers']['date'])) : null;
        } catch (S3Exception $e) {
            $this->result->error = "Error! Could not put $file to Minio: " . $e->getMessage(
                );
        }
        return $this->result;
    }

    public function delete($file = false)
    {
        if (!$file) {
            $this->result->error = "Error! Could not delete $file from Minio; no file name provided";
            return $this->result;
        }
        $this->initResult();
        try {
            $this->client->deleteObject(
                [
                    'Bucket' => $this->bucket,
                    'Key'    => basename($file),
                ]
            );
        } catch (S3Exception $e) {
            $this->result->error = "Error! Could not delete $file from Minio: " . $e->getMessage(
                );
        }
        return $this->result;
    }

    private function initClient()
    {
        $this->client = new S3Client(
            [
                'endpoint'                => $this->config->endpoint,
                'version'                 => $this->config->version,
                'region'                  => $this->config->region,
                'use_path_style_endpoint' => true,
                'credentials'             => [
                    'key'    => $this->config->key,
                    'secret' => $this->config->secret,
                ],
            ]
        );
        return $this->client;
    }

    private function initResult()
    {
        $this->result = new stdClass();
        // Double-check if file actually exists
        // Set up Minio client if necessary
        if (!$this->client) {
            $this->initClient();
        }
        // Check if bucket is set
        if (empty($this->getBucket())) {
            throw new Exception('Error! Minio bucket not set');
        }
        return $this->result;
    }

}
