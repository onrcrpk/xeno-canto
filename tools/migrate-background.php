<?php

use function xc\db;

require_once 'xc.lib.php';


class MigrateBackground extends xc\Task
{

    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        $res = $this->mysqli()->query(
            "SELECT snd_nr, back_nrs, back_latin, back_english, back_families 
            FROM birdsounds WHERE back_nrs != ''"
        );
        while ($row = $res->fetch_object()) {
            $snd_nr = $row->snd_nr;
            $nrs = array_map('trim', explode(',', $row->back_nrs));
            $latin = array_map('trim', explode(',', $row->back_latin));
            $english = array_map('trim', explode(',', $row->back_english));
            $families = array_map('trim', explode(',', $row->back_families));

            for ($i = 0; $i < count($nrs); $i++) {
                $species_nr = $this->mysqli()->real_escape_string($nrs[$i]);
                $back_english = $this->mysqli()->real_escape_string($english[$i]);
                $back_latin = $this->mysqli()->real_escape_string($latin[$i]);
                $back_family = $this->mysqli()->real_escape_string($families[$i]);
                $q = "
                    INSERT IGNORE INTO `birdsounds_background` 
                    (`snd_nr`, `species_nr`, `scientific`, `english`, `family`) 
                    VALUES 
                    ($snd_nr, '$species_nr', '$back_latin', '$back_english', '$back_family')";
                $this->mysqli()->query($q);
            }
        }
    }

    private function mysqli()
    {
        return db()->getConnection();
    }

}


function app()
{
    static $task = null;
    if (!$task) {
        $task = new MigrateBackground();
    }
    return $task;
}

app()->run();
