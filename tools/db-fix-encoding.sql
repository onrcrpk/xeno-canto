-- optional
-- ALTER TABLE $src ADD PRIMARY KEY ($pkcolumn);
-- CREATE TABLE tmptable (SELECT * from $src WHERE LENGTH($field) != CHAR_LENGTH($field));
-- ALTER TABLE tmptable MODIFY $field $datatype CHARACTER SET latin1;
-- ALTER TABLE tmptable MODIFY $field blob;
-- ALTER TABLE tmptable MODIFY $field $datatype CHARACTER SET utf8;
-- DELETE FROM tmptable WHERE LENGTH($field) = CHAR_LENGTH($field);
-- REPLACE INTO $src (SELECT * FROM tmptable);
-- DROP TABLE tmptable;

-- ##### Birdsounds #####
-- ALTER TABLE birdsounds ADD PRIMARY KEY (snd_nr);

CREATE TABLE tmptable (SELECT * from birdsounds WHERE LENGTH(recordist) != CHAR_LENGTH(recordist));
ALTER TABLE tmptable MODIFY recordist VARCHAR(100) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY recordist blob;
ALTER TABLE tmptable MODIFY recordist VARCHAR(100) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(recordist) = CHAR_LENGTH(recordist);
REPLACE INTO birdsounds (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from birdsounds WHERE LENGTH(location) != CHAR_LENGTH(location));
ALTER TABLE tmptable MODIFY location VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY location blob;
ALTER TABLE tmptable MODIFY location VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(location) = CHAR_LENGTH(location);
REPLACE INTO birdsounds (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from birdsounds WHERE LENGTH(remarks) != CHAR_LENGTH(remarks));
ALTER TABLE tmptable MODIFY remarks TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY remarks blob;
ALTER TABLE tmptable MODIFY remarks TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(remarks) = CHAR_LENGTH(remarks);
REPLACE INTO birdsounds (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from birdsounds WHERE LENGTH(background) != CHAR_LENGTH(background));
ALTER TABLE tmptable MODIFY background VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY background blob;
ALTER TABLE tmptable MODIFY background VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(background) = CHAR_LENGTH(background);
REPLACE INTO birdsounds (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from birdsounds WHERE LENGTH(back_extra) != CHAR_LENGTH(back_extra));
ALTER TABLE tmptable MODIFY back_extra VARCHAR(100) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY back_extra blob;
ALTER TABLE tmptable MODIFY back_extra VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(back_extra) = CHAR_LENGTH(back_extra);
REPLACE INTO birdsounds (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from birdsounds WHERE LENGTH(path) != CHAR_LENGTH(path));
ALTER TABLE tmptable MODIFY path VARCHAR(200) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY path blob;
ALTER TABLE tmptable MODIFY path VARCHAR(200) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(path) = CHAR_LENGTH(path);
REPLACE INTO birdsounds (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### blogs #####
CREATE TABLE tmptable (SELECT * from blogs WHERE LENGTH(title) != CHAR_LENGTH(title));
ALTER TABLE tmptable MODIFY title VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY title blob;
ALTER TABLE tmptable MODIFY title VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(title) = CHAR_LENGTH(title);
REPLACE INTO blogs (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from blogs WHERE LENGTH(blog) != CHAR_LENGTH(blog));
ALTER TABLE tmptable MODIFY blog TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY blog blob;
ALTER TABLE tmptable MODIFY blog TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(blog) = CHAR_LENGTH(blog);
REPLACE INTO blogs (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from blogs WHERE LENGTH(blog_as_html) != CHAR_LENGTH(blog_as_html));
ALTER TABLE tmptable MODIFY blog_as_html LONGTEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY blog_as_html blob;
ALTER TABLE tmptable MODIFY blog_as_html LONGTEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(blog_as_html) = CHAR_LENGTH(blog_as_html);
REPLACE INTO blogs (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from blogs WHERE LENGTH(author) != CHAR_LENGTH(author));
ALTER TABLE tmptable MODIFY author VARCHAR(200) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY author blob;
ALTER TABLE tmptable MODIFY author VARCHAR(200) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(author) = CHAR_LENGTH(author);
REPLACE INTO blogs (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### discuss_news_world #####
CREATE TABLE tmptable (SELECT * from discuss_news_world WHERE LENGTH(text) != CHAR_LENGTH(text));
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY text blob;
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(text) = CHAR_LENGTH(text);
REPLACE INTO discuss_news_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from discuss_news_world WHERE LENGTH(name) != CHAR_LENGTH(name));
ALTER TABLE tmptable MODIFY name VARCHAR(150) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY name blob;
ALTER TABLE tmptable MODIFY name VARCHAR(150) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(name) = CHAR_LENGTH(name);
REPLACE INTO discuss_news_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### discussion_forum_world #####
ALTER TABLE discussion_forum_world ADD PRIMARY KEY (mes_nr);

CREATE TABLE tmptable (SELECT * from discussion_forum_world WHERE LENGTH(name) != CHAR_LENGTH(name));
ALTER TABLE tmptable MODIFY name VARCHAR(100) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY name blob;
ALTER TABLE tmptable MODIFY name VARCHAR(100) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(name) = CHAR_LENGTH(name);
REPLACE INTO discussion_forum_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from discussion_forum_world WHERE LENGTH(text) != CHAR_LENGTH(text));
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY text blob;
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(text) = CHAR_LENGTH(text);
REPLACE INTO discussion_forum_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### discussion_world #####
CREATE TABLE tmptable (SELECT * from discussion_world WHERE LENGTH(name) != CHAR_LENGTH(name));
ALTER TABLE tmptable MODIFY name VARCHAR(40) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY name blob;
ALTER TABLE tmptable MODIFY name VARCHAR(100) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(name) = CHAR_LENGTH(name);
REPLACE INTO discussion_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from discussion_world WHERE LENGTH(text) != CHAR_LENGTH(text));
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY text blob;
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(text) = CHAR_LENGTH(text);
REPLACE INTO discussion_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### feature_discussions #####
ALTER TABLE feature_discussions ADD PRIMARY KEY (mes_nr);

CREATE TABLE tmptable (SELECT * from feature_discussions WHERE LENGTH(name) != CHAR_LENGTH(name));
ALTER TABLE tmptable MODIFY name VARCHAR(100) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY name blob;
ALTER TABLE tmptable MODIFY name VARCHAR(100) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(name) = CHAR_LENGTH(name);
REPLACE INTO feature_discussions (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from feature_discussions WHERE LENGTH(text) != CHAR_LENGTH(text));
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY text blob;
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(text) = CHAR_LENGTH(text);
REPLACE INTO feature_discussions (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### forum_world #####
ALTER TABLE forum_world ADD PRIMARY KEY (topic_nr);

CREATE TABLE tmptable (SELECT * from forum_world WHERE LENGTH(name) != CHAR_LENGTH(name));
ALTER TABLE tmptable MODIFY name VARCHAR(100) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY name blob;
ALTER TABLE tmptable MODIFY name VARCHAR(100) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(name) = CHAR_LENGTH(name);
REPLACE INTO forum_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from forum_world WHERE LENGTH(subject) != CHAR_LENGTH(subject));
ALTER TABLE tmptable MODIFY subject VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY subject blob;
ALTER TABLE tmptable MODIFY subject VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(subject) = CHAR_LENGTH(subject);
REPLACE INTO forum_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### news_world #####
CREATE TABLE tmptable (SELECT * from news_world WHERE LENGTH(text) != CHAR_LENGTH(text));
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY text blob;
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(text) = CHAR_LENGTH(text);
REPLACE INTO news_world (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### spotlight_v2 #####
CREATE TABLE tmptable (SELECT * from spotlight_v2 WHERE LENGTH(text) != CHAR_LENGTH(text));
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY text blob;
ALTER TABLE tmptable MODIFY text TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(text) = CHAR_LENGTH(text);
REPLACE INTO spotlight_v2 (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### users #####
ALTER TABLE users ADD PRIMARY KEY (dir);

CREATE TABLE tmptable (SELECT * from users WHERE LENGTH(username) != CHAR_LENGTH(username));
ALTER TABLE tmptable MODIFY username VARCHAR(80) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY username blob;
ALTER TABLE tmptable MODIFY username VARCHAR(100) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(username) = CHAR_LENGTH(username);
REPLACE INTO users (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from users WHERE LENGTH(email) != CHAR_LENGTH(email));
ALTER TABLE tmptable MODIFY email VARCHAR(80) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY email blob;
ALTER TABLE tmptable MODIFY email VARCHAR(100) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(email) = CHAR_LENGTH(email);
REPLACE INTO users (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from users WHERE LENGTH(blurb) != CHAR_LENGTH(blurb));
ALTER TABLE tmptable MODIFY blurb TEXT CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY blurb blob;
ALTER TABLE tmptable MODIFY blurb TEXT CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(blurb) = CHAR_LENGTH(blurb);
REPLACE INTO users (SELECT * FROM tmptable);
DROP TABLE tmptable;

-- ##### IOC_2011_v2p10 #####
CREATE TABLE tmptable (SELECT * from IOC_2011_v2p10 WHERE LENGTH(eng_name) != CHAR_LENGTH(eng_name));
ALTER TABLE tmptable MODIFY eng_name VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY eng_name blob;
ALTER TABLE tmptable MODIFY eng_name VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(eng_name) = CHAR_LENGTH(eng_name);
REPLACE INTO IOC_2011_v2p10 (SELECT * FROM tmptable);
DROP TABLE tmptable;
ALTER TABLE IOC_2011_v2p10
ADD UNIQUE INDEX `species_nr_UNIQUE` (`species_nr` ASC) ;


-- ##### IOC_3_1_multilingual #####
CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(english) != CHAR_LENGTH(english));
ALTER TABLE tmptable MODIFY english VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY english blob;
ALTER TABLE tmptable MODIFY english VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(english) = CHAR_LENGTH(english);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(chinese) != CHAR_LENGTH(chinese));
ALTER TABLE tmptable MODIFY chinese VARCHAR(45) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY chinese blob;
ALTER TABLE tmptable MODIFY chinese VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(chinese) = CHAR_LENGTH(chinese);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(czech) != CHAR_LENGTH(czech));
ALTER TABLE tmptable MODIFY czech VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY czech blob;
ALTER TABLE tmptable MODIFY czech VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(czech) = CHAR_LENGTH(czech);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(danish) != CHAR_LENGTH(danish));
ALTER TABLE tmptable MODIFY danish VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY danish blob;
ALTER TABLE tmptable MODIFY danish VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(danish) = CHAR_LENGTH(danish);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(dutch) != CHAR_LENGTH(dutch));
ALTER TABLE tmptable MODIFY dutch VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY dutch blob;
ALTER TABLE tmptable MODIFY dutch VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(dutch) = CHAR_LENGTH(dutch);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(finnish) != CHAR_LENGTH(finnish));
ALTER TABLE tmptable MODIFY finnish VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY finnish blob;
ALTER TABLE tmptable MODIFY finnish VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(finnish) = CHAR_LENGTH(finnish);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(french) != CHAR_LENGTH(french));
ALTER TABLE tmptable MODIFY french VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY french blob;
ALTER TABLE tmptable MODIFY french VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(french) = CHAR_LENGTH(french);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(german) != CHAR_LENGTH(german));
ALTER TABLE tmptable MODIFY german VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY german blob;
ALTER TABLE tmptable MODIFY german VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(german) = CHAR_LENGTH(german);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(italian) != CHAR_LENGTH(italian));
ALTER TABLE tmptable MODIFY italian VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY italian blob;
ALTER TABLE tmptable MODIFY italian VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(italian) = CHAR_LENGTH(italian);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(japanese) != CHAR_LENGTH(japanese));
ALTER TABLE tmptable MODIFY japanese VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY japanese blob;
ALTER TABLE tmptable MODIFY japanese VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(japanese) = CHAR_LENGTH(japanese);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(norwegian) != CHAR_LENGTH(norwegian));
ALTER TABLE tmptable MODIFY norwegian VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY norwegian blob;
ALTER TABLE tmptable MODIFY norwegian VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(norwegian) = CHAR_LENGTH(norwegian);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(polish) != CHAR_LENGTH(polish));
ALTER TABLE tmptable MODIFY polish VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY polish blob;
ALTER TABLE tmptable MODIFY polish VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(polish) = CHAR_LENGTH(polish);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(russian) != CHAR_LENGTH(russian));
ALTER TABLE tmptable MODIFY russian VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY russian blob;
ALTER TABLE tmptable MODIFY russian VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(russian) = CHAR_LENGTH(russian);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(slovak) != CHAR_LENGTH(slovak));
ALTER TABLE tmptable MODIFY slovak VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY slovak blob;
ALTER TABLE tmptable MODIFY slovak VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(slovak) = CHAR_LENGTH(slovak);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(spanish) != CHAR_LENGTH(spanish));
ALTER TABLE tmptable MODIFY spanish VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY spanish blob;
ALTER TABLE tmptable MODIFY spanish VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(spanish) = CHAR_LENGTH(spanish);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;

CREATE TABLE tmptable (SELECT * from IOC_3_1_multilingual WHERE LENGTH(swedish) != CHAR_LENGTH(swedish));
ALTER TABLE tmptable MODIFY swedish VARCHAR(255) CHARACTER SET latin1;
ALTER TABLE tmptable MODIFY swedish blob;
ALTER TABLE tmptable MODIFY swedish VARCHAR(255) CHARACTER SET utf8;
DELETE FROM tmptable WHERE LENGTH(swedish) = CHAR_LENGTH(swedish);
REPLACE INTO IOC_3_1_multilingual (SELECT * FROM tmptable);
DROP TABLE tmptable;
