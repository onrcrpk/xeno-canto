<?php

$renames =
    [
        [['Ptiloxena', 'atroviolaceus'], ['Ptiloxena', 'atroviolacea']],
        [['Lemuresthes', 'nana'], ['Lepidopygia', 'nana']],
        [['Muscicapa', 'latirostris'], ['Muscicapa', 'dauurica']],
        [['Damophila', 'julie'], ['Juliamyia', 'julie']],
    ];
$splits  =
    [
        [
            ['Ammodramus', 'maritimus', 'juncicola'],
            [
                'Ammodramus',
                'maritimus',
                'peninsulae',
            ],
        ],
        [
            ['Anthus', 'cinnamomeus', 'spurius'],
            [
                'Anthus',
                'cinnamomeus',
                'spurium',
            ],
        ],
        [
            ['Ploceus', 'hypoxanthus', 'hymenaicus'],
            [
                'Ploceus',
                'hypoxanthus',
                'chryseus',
            ],
        ],
        [
            ['Cinnyris', 'talatala', 'talatala'],
            [
                'Cinnyris',
                'talatala',
                null,
            ],
        ],
        [
            ['Cinnyris', 'talatala', 'arestus'],
            [
                'Cinnyris',
                'talatala',
                null,
            ],
        ],
        [
            ['Cinnyris', 'mariquensis', 'ovamboensis'],
            [
                'Cinnyris',
                'mariquensis',
                'mariquensis',
            ],
        ],
        [
            ['Cinnyris', 'mariquensis', 'lucens'],
            [
                'Cinnyris',
                'mariquensis',
                'mariquensis',
            ],
        ],
        [
            ['Cyanomitra', 'olivacea', 'vincenti'],
            [
                'Cyanomitra',
                'olivacea',
                'ragazzii',
            ],
        ],
        [
            ['Cyanomitra', 'olivacea', 'lowei'],
            [
                'Cyanomitra',
                'olivacea',
                'ragazzii',
            ],
        ],
        [
            ['Paradigalla', 'carunculata', 'intermedia'],
            [
                'Paradigalla',
                'carunculata',
                null,
            ],
        ],
        [
            ['Paradigalla', 'carunculata', 'carunculata'],
            [
                'Paradigalla',
                'carunculata',
                null,
            ],
        ],
        [
            ['Clytorhynchus', 'vitiensis', 'compressirostris'],
            [
                'Clytorhynchus',
                'vitiensis',
                'brunneus',
            ],
        ],
        [
            ['Aphelocoma', 'californica', 'cactophila'],
            [
                'Aphelocoma',
                'californica',
                'hypoleuca',
            ],
        ],
        [
            ['Perisoreus', 'infaustus', 'ostjakorum'],
            [
                'Perisoreus',
                'infaustus',
                'rogosowi',
            ],
        ],
        [
            ['Perisoreus', 'infaustus', 'ruthenus'],
            [
                'Perisoreus',
                'infaustus',
                'infaustus',
            ],
        ],
        [
            ['Perisoreus', 'infaustus', 'tchakenkoi'],
            [
                'Perisoreus',
                'infaustus',
                'sibericus',
            ],
        ],
        [
            ['Perisoreus', 'infaustus', 'yakutensis'],
            [
                'Perisoreus',
                'infaustus',
                'sibericus',
            ],
        ],
        [
            ['Philentoma', 'velata', 'coesia'],
            [
                'Philentoma',
                'velata',
                'caesia',
            ],
        ],
        [
            ['Artamus', 'cinereus', 'dealbatus'],
            [
                'Artamus',
                'cinereus',
                'inkermani',
            ],
        ],
        [
            ['Oriolus', 'larvatus', 'additus'],
            [
                'Oriolus',
                'larvatus',
                'tibicen',
            ],
        ],
        [
            ['Pitohui', 'kirhocephalus', 'stramineipectus'],
            [
                'Pitohui',
                'kirhocephalus',
                'rubiensis',
            ],
        ],
        [
            ['Hylophilus', 'flavipes', 'acuticauda'],
            [
                'Hylophilus',
                'flavipes',
                'acuticaudus',
            ],
        ],
        [
            ['Malurus', 'splendens', 'musgravi'],
            [
                'Malurus',
                'splendens',
                'callainus',
            ],
        ],
        [
            ['Tarphonomus', 'certhioides', 'luscinius'],
            [
                'Tarphonomus',
                'certhioides',
                'luscinia',
            ],
        ],
        [
            ['Muscisaxicola', 'cinereus', 'argentina'],
            [
                'Muscisaxicola',
                'cinereus',
                'argentinus',
            ],
        ],
        [
            ['Knipolegus', 'nigerrimus', 'hoflingi'],
            [
                'Knipolegus',
                'nigerrimus',
                'hoflingae',
            ],
        ],
        [
            ['Lamprolia', 'victoriae', 'kleinschmidti'],
            [
                'Lamprolia',
                'victoriae',
                'klinesmithi',
            ],
        ],
        [
            ['Colinus', 'virginianus', 'manlandicus'],
            [
                'Colinus',
                'virginianus',
                'virginianus',
            ],
        ],
        [
            ['Colinus', 'virginianus', 'mexicanus'],
            [
                'Colinus',
                'virginianus',
                'virginianus',
            ],
        ],
        [
            ['Atlapetes', 'latinuchus', 'nigrifrons'],
            [
                'Atlapetes',
                'nigrifrons',
                null,
            ],
        ],
        [
            ['Atlapetes', 'latinuchus', 'latinuchus'],
            [
                'Atlapetes',
                'latinuchus',
                null,
            ],
        ],
        [['Passerella', 'iliaca', 'iliaca'], ['Passerella', 'iliaca', null]],
        [
            ['Passerella', 'iliaca', 'megarhyncha'],
            [
                'Passerella',
                'megarhyncha',
                null,
            ],
        ],
        [
            ['Passerella', 'iliaca', 'schistacea'],
            [
                'Passerella',
                'schistacea',
                null,
            ],
        ],
        [
            ['Passerella', 'iliaca', 'unalaschcensis'],
            [
                'Passerella',
                'unalaschcensis',
                null,
            ],
        ],
        [
            ['Sittiparus', 'varius', 'castaneoventris'],
            [
                'Sittiparus',
                'castaneoventris',
                null,
            ],
        ],
        [
            ['Sittiparus', 'varius', 'olivaceus'],
            [
                'Sittiparus',
                'olivaceus',
                null,
            ],
        ],
        [
            ['Sittiparus', 'varius', 'owstoni'],
            [
                'Sittiparus',
                'owstoni',
                null,
            ],
        ],
        [
            ['Pachycephala', 'pectoralis', 'occidentalis'],
            [
                'Pachycephala',
                'occidentalis',
                null,
            ],
        ],
        [
            ['Pachycephala', 'citreogaster', 'collaris'],
            [
                'Pachycephala',
                'collaris',
                'collaris',
            ],
        ],
        [
            ['Pachycephala', 'citreogaster', 'rosseliana'],
            [
                'Pachycephala',
                'collaris',
                'rosseliana',
            ],
        ],
        [
            ['Anthocephala', 'floriceps', 'floriceps'],
            [
                'Anthocephala',
                'floriceps',
                null,
            ],
        ],
        [
            ['Anthocephala', 'floriceps', 'berlepschi'],
            [
                'Anthocephala',
                'berlepschi',
                null,
            ],
        ],
        [
            ['Stephanoxis', 'lalandi', 'lalandi'],
            [
                'Stephanoxis',
                'lalandi',
                null,
            ],
        ],
        [
            ['Stephanoxis', 'lalandi', 'loddigesii'],
            [
                'Stephanoxis',
                'loddigesii',
                null,
            ],
        ],
        [
            ['Myiopsitta', 'monachus', 'luchsi'],
            [
                'Myiopsitta',
                'luchsi',
                null,
            ],
        ],
    ];
$lumps   =
    [
        [
            ['Pachycephala', 'melanura', 'whitneyi'],
            [
                'Pachycephala',
                'melanura',
                null,
            ],
        ],
    ];
