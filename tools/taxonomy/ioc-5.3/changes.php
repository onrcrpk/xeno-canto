<?php

$renames =
    [
        [['Geotrygon', 'veraguensis'], ['Leptotrygon', 'veraguensis']],
        [['Geotrygon', 'carrikeri'], ['Zentrygon', 'carrikeri']],
        [['Geotrygon', 'costaricensis'], ['Zentrygon', 'costaricensis']],
        [['Geotrygon', 'lawrencii'], ['Zentrygon', 'lawrencii']],
        [['Geotrygon', 'albifacies'], ['Zentrygon', 'albifacies']],
        [['Geotrygon', 'linearis'], ['Zentrygon', 'linearis']],
        [['Geotrygon', 'chiriquensis'], ['Zentrygon', 'chiriquensis']],
        [['Geotrygon', 'frenata'], ['Zentrygon', 'frenata']],
        [['Gallicolumba', 'hoedtii'], ['Alopecoenas', 'hoedtii']],
        [['Spizella', 'arborea'], ['Spizelloides', 'arborea']],
        [['Gallicolumba', 'kubaryi'], ['Alopecoenas', 'kubaryi']],
        [['Gallicolumba', 'xanthonura'], ['Alopecoenas', 'xanthonurus']],
        [['Gallicolumba', 'stairi'], ['Alopecoenas', 'stairi']],
        [
            ['Gallicolumba', 'sanctaecrucis'],
            [
                'Alopecoenas',
                'sanctaecrucis',
            ],
        ],
        [['Gallicolumba', 'ferruginea'], ['Alopecoenas', 'ferrugineus']],
        [['Gallicolumba', 'salamonis'], ['Alopecoenas', 'salamonis']],
        [['Gallicolumba', 'rubescens'], ['Alopecoenas', 'rubescens']],
        [['Gallicolumba', 'canifrons'], ['Alopecoenas', 'canifrons']],
        [['Psephotus', 'varius'], ['Psephotellus', 'varius']],
        [['Psephotus', 'dissimilis'], ['Psephotellus', 'dissimilis']],
        [
            ['Psephotus', 'chrysopterygius'],
            [
                'Psephotellus',
                'chrysopterygius',
            ],
        ],
        [['Psephotus', 'pulcherrimus'], ['Psephotellus', 'pulcherrimus']],
        [['Chalcopsitta', 'cardinalis'], ['Pseudeos', 'cardinalis']],
        [['Dryocopus', 'schulzi'], ['Dryocopus', 'schulzii']],
        [
            ['Sylviorthorhynchus', 'desmursii'],
            [
                'Sylviorthorhynchus',
                'desmurii',
            ],
        ],
        [['Hylophilus', 'sclateri'], ['Vireo', 'sclateri']],
        [['Buphagus', 'erythrorhynchus'], ['Buphagus', 'erythrorynchus']],
        [['Vestiaria', 'coccinea'], ['Drepanis', 'coccinea']],
        [
            ['Glossopsitta', 'porphyrocephala'],
            [
                'Parvipsitta',
                'porphyrocephala',
            ],
        ],
        [['Glossopsitta', 'pusilla'], ['Parvipsitta', 'pusilla']],
        // array(array("Telophorus", "quadricolor"),array("Telophorus", "viridis")),
        [['Geotrygon', 'goldmani'], ['Zentrygon', 'goldmani']],
        [['Manucerthia', 'mana'], ['Loxops', 'mana']],

    ];

$splits =
    [
        [
            ['Gallicolumba', 'beccarii', 'admiralitatis'],
            [
                'Alopecoenas',
                'beccarii',
                'admiralitatis',
            ],
        ],
        [
            ['Gallicolumba', 'erythroptera', 'albicollis'],
            [
                'Alopecoenas',
                'erythropterus',
                'albicollis',
            ],
        ],
        [
            ['Spizella', 'arborea', 'arborea'],
            [
                'Spizelloides',
                'arborea',
                'arborea',
            ],
        ],
        [
            ['Pachycephala', 'pectoralis', 'balim'],
            [
                'Pachycephala',
                'macrorhyncha',
                'balim',
            ],
        ],
        [
            ['Gallicolumba', 'beccarii', 'beccarii'],
            [
                'Alopecoenas',
                'beccarii',
                'beccarii',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'bellus'],
            [
                'Porphyrio',
                'melanotus',
                'bellus',
            ],
        ],
        [
            ['Geotrygon', 'frenata', 'bourcieri'],
            [
                'Zentrygon',
                'frenata',
                'bourcieri',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'caspius'],
            [
                'Porphyrio',
                'poliocephalus',
                'caspius',
            ],
        ],
        [
            ['Gallicolumba', 'jobiensis', 'chalconotus'],
            [
                'Alopecoenas',
                'jobiensis',
                'chalconotus',
            ],
        ],
        [
            ['Elaenia', 'pallatangae', 'davidwillardi'],
            [
                'Elaenia',
                'olivina',
                'davidwillardi',
            ],
        ],
        [
            ['Gallicolumba', 'beccarii', 'eichhorni'],
            [
                'Alopecoenas',
                'beccarii',
                'eichhorni',
            ],
        ],
        [
            ['Gallicolumba', 'erythroptera', 'erythroptera'],
            [
                'Alopecoenas',
                'erythropterus',
                'erythropterus',
            ],
        ],
        [
            ['Cinclosoma', 'castanotum', 'fordianum'],
            [
                'Cinclosoma',
                'clarum',
                'fordianum',
            ],
        ],
        [
            ['Geotrygon', 'frenata', 'frenata'],
            [
                'Zentrygon',
                'frenata',
                'frenata',
            ],
        ],
        [
            ['Geotrygon', 'goldmani', 'goldmani'],
            [
                'Zentrygon',
                'goldmani',
                'goldmani',
            ],
        ],
        [
            ['Parus', 'cinereus', 'intermedius'],
            [
                'Parus',
                'major',
                'intermedius',
            ],
        ],
        [
            ['Gallicolumba', 'beccarii', 'intermedia'],
            [
                'Alopecoenas',
                'beccarii',
                'intermedius',
            ],
        ],
        [
            ['Gallicolumba', 'jobiensis', 'jobiensis'],
            [
                'Alopecoenas',
                'jobiensis',
                'jobiensis',
            ],
        ],
        [
            ['Gallicolumba', 'beccarii', 'johannae'],
            [
                'Alopecoenas',
                'beccarii',
                'johannae',
            ],
        ],
        [
            ['Geotrygon', 'frenata', 'margaritae'],
            [
                'Zentrygon',
                'frenata',
                'margaritae',
            ],
        ],
        [
            ['Gallicolumba', 'beccarii', 'masculina'],
            [
                'Alopecoenas',
                'beccarii',
                'masculinus',
            ],
        ],
        [
            ['Eremopterix', 'nigriceps', 'forbeswatsoni'],
            [
                'Eremopterix',
                'nigriceps',
                'melanauchen',
            ],
        ],
        [
            ['Eremopterix', 'nigriceps', 'affinis'],
            [
                'Eremopterix',
                'nigriceps',
                'melanauchen',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'melanopterus'],
            [
                'Porphyrio',
                'melanotus',
                'melanopterus',
            ],
        ],
        [
            ['Aramides', 'cajaneus', 'mexicanus'],
            [
                'Aramides',
                'albiventris',
                'mexicanus',
            ],
        ],
        [
            ['Telophorus', 'quadricolor', 'nigricauda'],
            [
                'Telophorus',
                'viridis',
                'nigricauda',
            ],
        ],
        [
            ['Spizella', 'arborea', 'ochracea'],
            [
                'Spizelloides',
                'arborea',
                'ochracea',
            ],
        ],
        [
            ['Geotrygon', 'goldmani', 'oreas'],
            [
                'Zentrygon',
                'goldmani',
                'oreas',
            ],
        ],
        [
            ['Aramides', 'cajaneus', 'pacificus'],
            [
                'Aramides',
                'albiventris',
                'pacificus',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'pelewensis'],
            [
                'Porphyrio',
                'melanotus',
                'pelewensis',
            ],
        ],
        [
            ['Aramides', 'cajaneus', 'plumbeicollis'],
            [
                'Aramides',
                'albiventris',
                'plumbeicollis',
            ],
        ],
        [
            ['Paradoxornis', 'heudei', 'mongolicus'],
            [
                'Paradoxornis',
                'heudei',
                'polivanovi',
            ],
        ],
        [
            ['Erythropitta', 'erythrogaster', 'thompsoni'],
            [
                'Erythropitta',
                'erythrogaster',
                'propinqua',
            ],
        ],
        [
            ['Telophorus', 'quadricolor', 'quartus'],
            [
                'Telophorus',
                'viridis',
                'quartus',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'samoensis'],
            [
                'Porphyrio',
                'melanotus',
                'samoensis',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'seistanicus'],
            [
                'Porphyrio',
                'poliocephalus',
                'seistanicus',
            ],
        ],
        [
            ['Gallicolumba', 'beccarii', 'solomonensis'],
            [
                'Alopecoenas',
                'beccarii',
                'solomonensis',
            ],
        ],
        [
            ['Geotrygon', 'frenata', 'subgrisea'],
            [
                'Zentrygon',
                'frenata',
                'subgrisea',
            ],
        ],
        [
            ['Motacilla', 'tschutschensis', 'angarensis'],
            [
                'Motacilla',
                'tschutschensis',
                'tschutschensis',
            ],
        ],
        [
            ['Aramides', 'cajaneus', 'vanrossemi'],
            [
                'Aramides',
                'albiventris',
                'vanrossemi',
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'viridis'],
            [
                'Porphyrio',
                'indicus',
                'viridis',
            ],
        ],
        [
            ['Loxops', 'coccineus', 'coccineus'],
            [
                'Loxops',
                'coccineus',
                null,
            ],
        ],
        [
            ['Loxops', 'coccineus', 'ochraceus'],
            [
                'Loxops',
                'ochraceus',
                null,
            ],
        ],
        [
            ['Loxops', 'coccineus', 'wolstenholmei'],
            [
                'Loxops',
                'wolstenholmei',
                null,
            ],
        ],
        [
            ['Hemignathus', 'lucidus', 'lucidus'],
            [
                'Hemignathus',
                'lucidus',
                null,
            ],
        ],
        [
            ['Hemignathus', 'lucidus', 'affinis'],
            [
                'Hemignathus',
                'affinis',
                null,
            ],
        ],
        [
            ['Hemignathus', 'lucidus', 'hanapepe'],
            [
                'Hemignathus',
                'hanapepe',
                null,
            ],
        ],
        [
            ['Akialoa', 'ellisiana', 'lanaiensis'],
            [
                'Akialoa',
                'lanaiensis',
                null,
            ],
        ],
        [
            ['Akialoa', 'ellisiana', 'stejnegeri'],
            [
                'Akialoa',
                'stejnegeri',
                null,
            ],
        ],
        [
            ['Locustella', 'mandelli', 'idonea'],
            [
                'Locustella',
                'idonea',
                null,
            ],
        ],
        [
            ['Rhopophilus', 'pekinensis', 'pekinensis'],
            [
                'Rhopophilus',
                'pekinensis',
                null,
            ],
        ],
        [
            ['Rhopophilus', 'pekinensis', 'albosuperciliaris'],
            [
                'Rhopophilus',
                'albosuperciliaris',
                null,
            ],
        ],
        [
            ['Telophorus', 'quadricolor', 'quadricolor'],
            [
                'Telophorus',
                'viridis',
                'quadricolor',
            ],
        ],
        [
            ['Calliphlox', 'evelynae', 'lyrura'],
            [
                'Calliphlox',
                'lyrura',
                null,
            ],
        ],
        [['Eugenes', 'fulgens', 'fulgens'], ['Eugenes', 'fulgens', null]],
        [
            ['Eugenes', 'fulgens', 'spectabilis'],
            [
                'Eugenes',
                'spectabilis',
                null,
            ],
        ],
        [
            ['Forpus', 'xanthopterygius', 'crassirostris'],
            [
                'Forpus',
                'crassirostris',
                null,
            ],
        ],
        [
            ['Forpus', 'xanthopterygius', 'spengeli'],
            [
                'Forpus',
                'spengeli',
                null,
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'melanotus'],
            [
                'Porphyrio',
                'melanotus',
                null,
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'pulverulentus'],
            [
                'Porphyrio',
                'pulverulentus',
                null,
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'indicus'],
            [
                'Porphyrio',
                'indicus',
                null,
            ],
        ],
        [
            ['Porphyrio', 'porphyrio', 'poliocephalus'],
            [
                'Porphyrio',
                'poliocephalus',
                null,
            ],
        ],
        [
            ['Aramides', 'cajaneus', 'albiventris'],
            [
                'Aramides',
                'albiventris',
                'albiventris',
            ],
        ],
        [['Puffinus', 'newelli', 'myrtae'], ['Puffinus', 'myrtae', null]],
    ];

$lumps =
    [
        [
            ['Chroicocephalus', 'scopulinus', null],
            [
                'Chroicocephalus',
                'novaehollandiae',
                'scopulinus',
            ],
        ],

        [['Puffinus', 'newelli', 'newelli'], ['Puffinus', 'newelli', null]]
        ,
        [
            ['Lophoceros ', 'alboterminatus', 'alboterminatus'],
            [
                'Lophoceros ',
                'alboterminatus',
                null,
            ],
        ]
        ,
        [
            ['Lophoceros ', 'alboterminatus', 'geloensis'],
            [
                'Lophoceros ',
                'alboterminatus',
                null,
            ],
        ]
        ,
        [
            ['Lophoceros ', 'alboterminatus', 'suahelicus'],
            [
                'Lophoceros ',
                'alboterminatus',
                null,
            ],
        ]
        ,
        [
            ['Calliphlox', 'evelynae', 'evelynae'],
            [
                'Calliphlox',
                'evelynae',
                null,
            ],
        ]
        ,
        [['Eugenes', 'fulgens', 'fulgens'], ['Eugenes', 'fulgens', null]]
        ,
        [
            ['Porphyrio', 'porphyrio', 'porphyrio'],
            [
                'Porphyrio',
                'porphyrio',
                null,
            ],
        ]
        ,
        [['Gypaetus', 'barbatus', 'aureus'], ['Gypaetus', 'barbatus', null]]
        ,
        [['Puffinus', 'newelli', 'newelli'], ['Puffinus', 'newelli', null]],
    ];
