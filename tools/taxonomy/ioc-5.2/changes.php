<?php

$renames =
    [
        [['Spinus', 'barbata'], ['Spinus', 'barbatus']],
        [['Spinus', 'thibetana'], ['Spinus', 'thibetanus']],
        [['Spinus', 'notata'], ['Spinus', 'notatus']],
        [['Spinus', 'barbata'], ['Spinus', 'barbatus']],
        [['Spinus', 'xanthogastra'], ['Spinus', 'xanthogastrus']],
        [['Spinus', 'olivacea'], ['Spinus', 'olivaceus']],
        [['Spinus', 'magellanica'], ['Spinus', 'magellanicus']],
        [['Spinus', 'cucullata'], ['Spinus', 'cucullatus']],
        [['Spinus', 'atrata'], ['Spinus', 'atratus']],
        [['Chelidorhynx', 'hypoxantha'], ['Chelidorhynx', 'hypoxanthus']],
        [['Gerygone', 'cinerea'], ['Acanthiza', 'cinerea']],
        [['Megalaima', 'virens'], ['Psilopogon', 'virens']],
        [['Megalaima', 'lagrandieri'], ['Psilopogon', 'lagrandieri']],
        [['Megalaima', 'zeylanica'], ['Psilopogon', 'zeylanicus']],
        [['Megalaima', 'lineata'], ['Psilopogon', 'lineatus']],
        [['Megalaima', 'viridis'], ['Psilopogon', 'viridis']],
        [['Megalaima', 'faiostricta'], ['Psilopogon', 'faiostrictus']],
        [['Megalaima', 'corvina'], ['Psilopogon', 'corvinus']],
        [['Megalaima', 'chrysopogon'], ['Psilopogon', 'chrysopogon']],
        [['Megalaima', 'rafflesii'], ['Psilopogon', 'rafflesii']],
        [['Megalaima', 'mystacophanos'], ['Psilopogon', 'mystacophanos']],
        [['Megalaima', 'javensis'], ['Psilopogon', 'javensis']],
        [['Megalaima', 'flavifrons'], ['Psilopogon', 'flavifrons']],
        [['Megalaima', 'franklinii'], ['Psilopogon', 'franklinii']],
        [['Megalaima', 'oorti'], ['Psilopogon', 'oorti']],
        [['Megalaima', 'annamensis'], ['Psilopogon', 'annamensis']],
        [['Megalaima', 'faber'], ['Psilopogon', 'faber']],
        [['Megalaima', 'nuchalis'], ['Psilopogon', 'nuchalis']],
        [['Megalaima', 'asiatica'], ['Psilopogon', 'asiaticus']],
        [['Megalaima', 'monticola'], ['Psilopogon', 'monticola']],
        [['Megalaima', 'incognita'], ['Psilopogon', 'incognitus']],
        [['Megalaima', 'henricii'], ['Psilopogon', 'henricii']],
        [['Megalaima', 'armillaris'], ['Psilopogon', 'armillaris']],
        [['Megalaima', 'pulcherrima'], ['Psilopogon', 'pulcherrimus']],
        [['Megalaima', 'australis'], ['Psilopogon', 'australis']],
        [['Megalaima', 'eximia'], ['Psilopogon', 'eximius']],
        [['Megalaima', 'rubricapillus'], ['Psilopogon', 'rubricapillus']],
        [['Megalaima', 'malabarica'], ['Psilopogon', 'malabaricus']],
        [['Megalaima', 'haemacephala'], ['Psilopogon', 'haemacephalus']],
        [['Megalaima', 'duvaucelii'], ['Psilopogon', 'duvaucelii']],
    ];
$splits  =
    [
        [
            ['Spinus', 'magellanica', 'alleni'],
            [
                'Spinus',
                'magellanicus',
                'alleni',
            ],
        ],
        [
            ['Icteria', 'virens', 'tropicalis'],
            [
                'Icteria',
                'virens',
                'auricollis',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'boliviana'],
            [
                'Spinus',
                'magellanicus',
                'bolivianus',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'capitalis'],
            [
                'Spinus',
                'magellanicus',
                'capitalis',
            ],
        ],
        [
            ['Spinus', 'spinescens', 'capitanea'],
            [
                'Spinus',
                'spinescens',
                'capitaneus',
            ],
        ],
        [
            ['Spinus', 'psaltria', 'colombiana'],
            [
                'Spinus',
                'psaltria',
                'colombianus',
            ],
        ],
        [
            ['Spizocorys', 'conirostris', 'transiens'],
            [
                'Spizocorys',
                'conirostris',
                'conirostris',
            ],
        ],
        [
            ['Eurystomus', 'orientalis', 'cyanicollis'],
            [
                'Eurystomus',
                'orientalis',
                'cyanocollis',
            ],
        ],
        [
            ['Pyrrhula', 'pyrrhula', 'europoea'],
            [
                'Pyrrhula',
                'pyrrhula',
                'europaea',
            ],
        ],
        [['Spinus', 'notata', 'forreri'], ['Spinus', 'notatus', 'forreri']],
        [
            ['Linaria', 'cannabina', 'nana'],
            [
                'Linaria',
                'cannabina',
                'frigoris',
            ],
        ],
        [
            ['Pycnonotus', 'goiavier', 'yourdini'],
            [
                'Pycnonotus',
                'goiavier',
                'gourdini',
            ],
        ],
        [
            ['Pycnonotus', 'plumosus', 'insularis'],
            [
                'Pycnonotus',
                'plumosus',
                'hachisukae',
            ],
        ],
        [
            ['Cyanocorax', 'heilprini', ''],
            [
                'Cyanocorax',
                'heilprini',
                'heilprini',
            ],
        ],
        [
            ['Spinus', 'psaltria', 'hesperophila'],
            [
                'Spinus',
                'psaltria',
                'hesperophilus',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'hoyi'],
            [
                'Spinus',
                'magellanicus',
                'hoyi',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'icterica'],
            [
                'Spinus',
                'magellanicus',
                'ictericus',
            ],
        ],
        [
            ['Crithagra', 'atrogularis', 'impiger'],
            [
                'Crithagra',
                'atrogularis',
                'impigra',
            ],
        ],
        [
            ['Bernieria', 'madagascariensis', 'inceleber'],
            [
                'Bernieria',
                'madagascariensis',
                'incelebris',
            ],
        ],
        [['Ninox', 'affinis', 'isolata'], ['Ninox', 'scutulata', 'isolata']],
        [
            ['Dendropicos', 'fuscescens', 'centralis'],
            [
                'Dendropicos',
                'fuscescens',
                'loandae',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'longirostris'],
            [
                'Spinus',
                'magellanicus',
                'longirostris',
            ],
        ],
        [
            ['Spinus', 'pinus', 'macroptera'],
            [
                'Spinus',
                'pinus',
                'macropterus',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'magellanica'],
            [
                'Spinus',
                'magellanicus',
                'magellanicus',
            ],
        ],
        [
            ['Carduelis', 'carduelis', 'major'],
            [
                'Carduelis',
                'carduelis',
                'major',
            ],
        ],
        [
            ['Geothlypis', 'trichas', 'riparia'],
            [
                'Geothlypis',
                'trichas',
                'modesta',
            ],
        ],
        [
            ['Sturnella', 'magna', 'quinta'],
            [
                'Sturnella',
                'magna',
                'monticola',
            ],
        ],
        [
            ['Locustella', 'castanea', 'muscula'],
            [
                'Locustella',
                'castanea',
                'musculus',
            ],
        ],
        [
            ['Hypsipetes', 'amaurotis', 'harterti'],
            [
                'Hypsipetes',
                'amaurotis',
                'nagamichii',
            ],
        ],
        [['Spinus', 'notata', 'notata'], ['Spinus', 'notatus', 'notatus']],
        [['Spinus', 'notata', 'oleacea'], ['Spinus', 'notatus', 'oleaceus']],
        [
            ['Sporophila', 'nigricollis', 'vivida'],
            [
                'Sporophila',
                'nigricollis',
                'olivacea',
            ],
        ],
        [
            ['Aerodramus', 'vanikorensis', 'palawanensis'],
            [
                'Aerodramus',
                'vanikorensis',
                'palawanensis',
            ],
        ],
        [
            ['Spinus', 'tristis', 'pallida'],
            [
                'Spinus',
                'tristis',
                'pallidus',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'paula'],
            [
                'Spinus',
                'magellanicus',
                'paulus',
            ],
        ],
        [['Spinus', 'pinus', 'perplexa'], ['Spinus', 'pinus', 'perplexus']],
        [
            ['Spinus', 'magellanica', 'peruana'],
            [
                'Spinus',
                'magellanicus',
                'peruanus',
            ],
        ],
        [
            ['Stelgidopteryx', 'serripennis', 'psammochrous'],
            [
                'Stelgidopteryx',
                'serripennis',
                'psammochroa',
            ],
        ],
        [
            ['Galerida', 'macrorhyncha', 'randoni'],
            [
                'Galerida',
                'macrorhyncha',
                'randonii',
            ],
        ],
        [
            ['Ninox', 'affinis', 'rexpimenti'],
            [
                'Ninox',
                'scutulata',
                'rexpimenti',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'santaecrucis'],
            [
                'Spinus',
                'magellanicus',
                'santaecrucis',
            ],
        ],
        [
            ['Coracina', 'fimbriata', 'schierbrandii'],
            [
                'Coracina',
                'fimbriata',
                'schierbrandi',
            ],
        ],
        [
            ['Cisticola', 'ruficeps', 'scotoptera'],
            [
                'Cisticola',
                'ruficeps',
                'scotopterus',
            ],
        ],
        [
            ['Haemorhous', 'mexicanus', 'rhodopnus'],
            [
                'Haemorhous',
                'mexicanus',
                'sonoriensis',
            ],
        ],
        [
            ['Spinus', 'xanthogastra', 'stejnegeri'],
            [
                'Spinus',
                'xanthogastrus',
                'stejnegeri',
            ],
        ],
        [
            ['Surniculus', 'lugubris', 'stewarti'],
            [
                'Surniculus',
                'dicruroides',
                'stewarti',
            ],
        ],
        [
            ['Crithagra', 'reichardi', 'striatipecta'],
            [
                'Crithagra',
                'reichardi',
                'striatipectus',
            ],
        ],
        [
            ['Acrocephalus', 'stentoreus', 'sumbae'],
            [
                'Acrocephalus',
                'australis',
                'sumbae',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'tucumana'],
            [
                'Spinus',
                'magellanicus',
                'tucumanus',
            ],
        ],
        [
            ['Spinus', 'magellanica', 'urubambensis'],
            [
                'Spinus',
                'magellanicus',
                'urubambensis',
            ],
        ],
        [
            ['Pycnonotus', 'cafer', 'saturatus'],
            [
                'Pycnonotus',
                'cafer',
                'wetmorei',
            ],
        ],
        [
            ['Spinus', 'xanthogastra', 'xanthogastra'],
            [
                'Spinus',
                'xanthogastrus',
                'xanthogastrus',
            ],
        ],
        [
            ['Gymnomyza', 'viridis', 'viridis'],
            [
                'Gymnomyza',
                'viridis',
                null,
            ],
        ],
        [
            ['Gymnomyza', 'viridis', 'brunneirostris'],
            [
                'Gymnomyza',
                'brunneirostris',
                null,
            ],
        ],
        [
            ['Foulehaio', 'carunculatus', 'procerior'],
            [
                'Foulehaio',
                'procerior',
                null,
            ],
        ],
        [
            ['Foulehaio', 'carunculatus', 'carunculatus'],
            [
                'Foulehaio',
                'carunculatus',
                null,
            ],
        ],
        [
            ['Foulehaio', 'carunculatus', 'taviunenis'],
            [
                'Foulehaio',
                'taviunensis',
                null,
            ],
        ],
        [
            ['Mionectes', 'macconnelli', 'roraimae'],
            [
                'Mionectes',
                'roraimae',
                null,
            ],
        ],
        [['Amazona', 'farinosa', 'farinosa'], ['Amazona', 'farinosa', null]],
        [
            ['Amazona', 'farinosa', 'guatemalae'],
            [
                'Amazona',
                'guatemalae',
                'guatemalae',
            ],
        ],
        [
            ['Amazona', 'farinosa', 'virenticeps'],
            [
                'Amazona',
                'guatemalae',
                'virenticeps',
            ],
        ],
        [
            ['Northiella', 'haematogaster', 'narethae'],
            [
                'Northiella',
                'narethae',
                null,
            ],
        ],
        [
            ['Northiella', 'haematogaster', 'haematogaster'],
            [
                'Northiella',
                'haematogaster',
                'haematogaster',
            ],
        ],
        [['Spilornis', 'minimus', null], ['Spilornis', 'cheela', 'minimus']],
        [
            ['Megapodius', 'forsteni', 'forsteni'],
            [
                'Megapodius',
                'freycinet',
                'forsteni',
            ],
        ],
        [
            ['Megapodius', 'forsteni', 'buruensis'],
            [
                'Megapodius',
                'freycinet',
                'buruensis',
            ],
        ],
        [
            ['Paradoxornis', 'polivanovi', null],
            [
                'Paradoxornis',
                'heudei',
                'polivanovi',
            ],
        ],
    ];
$lumps   =
    [

        [['Ploceus', 'capensis', 'capensis'], ['Ploceus', 'capensis', null]]
        ,
        [
            ['Ploceus', 'capensis', 'rubricomus'],
            [
                'Ploceus',
                'capensis',
                null,
            ],
        ]
        ,
        [
            ['Ploceus', 'capensis', 'olivaceus'],
            [
                'Ploceus',
                'capensis',
                null,
            ],
        ],
    ];
