This script requires three files, in this order: 

1. names.xml
2. multilingual.csv
3. changes.php

These files are provided by WP. Compared to the original script by
Jonathon, this updated one:

- uses a header line in the csv file to indicate the language, rather than
an odd order with empty columns.

- provides the option of a 'dry run', to check if all data validates. A dry run (-dr)
is the default. Pass '-r' an 4th argument to the script to truly update the
database.

- provides direct feedback on screen if requested; use -f as a 5th parameter.
Otherwise, feedback is logged to a file. The log file e.g. is used to provide feedback 
when the script is ran from the admin section of the XC site 
(https://xeno-canto.org/admin/classification-update).

- optionally post-processes splits for individual recordings based geography, courtesy of
Rolf de By. Rolf provides two csv files containing the changes. These should be
renamed to splits_fg.csv and splits_bg.csv and stored at the same location as the compulsory files
to post-process foreground and background species respectively.


Start the script from the command line:
cd to XC root directory
php ./tools/taxonomy/update-taxonomy.php /data/minio/classification/names.xml /data/minio/classification/multilingual.csv /data/minio/classification/changes.php -dr -f


To start the script from another PHP script:

    public function dryRun () 
    {
        require_once 'tools/taxonomy/update-taxonomy.php';
        
        $update = new \UpdateTaxonomy();
        $result = $update->run([
            0 => dirname(__FILE__), 
            1 => $this->minioPath . 'names.xml', 
            2 => $this->minioPath . 'multilingual.csv',
            3 => $this->minioPath . 'changes.php',
        ]);
        
        $output = "<h1>Batch Upload</h1><p>" . nl2br($result) . "</p>";
        return $this->template->render($output);
    }




changes.php, what should go where?

1. species to species with a name not yet present in XC -> $renames
2. species to species with a name already present in XC -> $lumps
3. species to ssp of which the species is not yet present in XC -> $renames (species) and $splits (ssp)
4. species to ssp of which the species is already present in XC -> $lumps
5. ssp to species or spp with a name not yet present in XC -> $renames (species) and $splits (ssp)
6. ssp to species or spp with a name not yet present in XC -> $splits

The last two categories are problematic (especially the latter), as we cannot deduce which recording
should be assigned to which 'new' species. This is a manual process that is normally handled based on distribution.


Logic of classification script in plain English

1. empty tables are created for taxonomy, taxonomy_ssp and taxonomy_multilingual,
each with a _next suffix. birdsounds_next and birdsounds_background_next are created as clones
of the original tables. When a real run (as opposed to a dry run) takes place,
the original tables are backed up with a timestamp suffix.

2. taxonomy and taxonomy_ssp are filled by parsing the names.xml file.

3. taxonomy_multilingual is filled by parsing the multilingual.csv file.

(Note: a bit odd is that random species_nrs (actually 6-character strings) are also generated in
ssp and multilingual tables. These are overwritten in step 6, so actually this is unnecessary.)

4. species_nr, extinct and restriced in taxonomy_next are copied from taxonomy by matching a
genus-species combination.

5. renames in change.php are processed. Only species are renamed, subspecies are not considered here.
The data from the 'from' species in taxonomy is copied to the 'to' species in taxonomy_next.
They retain the species_nr from the original taxon, so ultimately only the name is replaced.
Background species therefore do not have to be modified at this step!

6. only now, the species_nrs in taxonomy_ssp and taxonomy_multilingual are updated from taxonomy_next,
again matching on genus-species.

7. English names are crossed-checked between taxonomy and taxonomy_multilingual.
These must match between the xml and csv files to avoid inconsistencies, otherwise the script halts.

8. a check if performed to see if there are taxa in taxonomy that are not present in taxonomy_next
(and if there are any recordings present of those species in birdsounds).
These species are reported on a FYI basis. They are copied over in step 10.

9. taxa from other groups are copied over from taxonomy to taxonomy_next.

10. bird species present in the current taxonomy but not in the next (e.g. undescribed species with
preliminary names) are copied over to taxonomy_next. These have been checked in step 8 and will be listed at
the end of a successful dry run. Check these carefully to ensure that no invalid names are carried over!

11. splits are being processed. These handle:

- renames from one ssp to the other. For ssp that have been renamed, the rename on species level
**should also be present in the rename section**. The background species for such ssp renames
have already been correctly handled in that step, no further action required.

- ssp that have been elevated to species level. Splits does not update birdsounds_background,
because we do not know which recordings should be allocated to which species.
These are post-processed in a separate script by Rolf de By.

12. lumps are being processed. Lumps may or may not include ssp,  as this step also handles species
that are renamed to existing species. This step also updates the species_nr of the background species.

13. the taxonomy_next table now is up-to-date. Next, the data from taxonomy_next is copied to birdsounds_next

14. a check if performed if there are any orphaned species in birdsounds_next.
If so, a report is created with the species name and number of recordings and the script is stopped.

15. for all species in birdsounds_background_next, the "redundant data" (species and English name, family)
is updated by looking up this data by species_nr in taxonomy_next.

16. a check if performed if there are any orphaned species in birdsounds_background_next. If so, a report
is created with the species names and the script is stopped.

17. if the csv files are present, the splits for individual recordings are post-processed.

18. if we get to this point in the script, success is reported and the original tables are
replaced with the next tables. In the dry run, the next tables are not automatically deleted anymore, \
as they can be used to verify data.

