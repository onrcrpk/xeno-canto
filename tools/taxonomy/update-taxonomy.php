<?php

require_once 'class/UpdateTaxonomy.php';

// Run immediately if invoked from command line
if (!http_response_code()) {
    $app = new UpdateTaxonomy();
    $app->run($argv);
}
