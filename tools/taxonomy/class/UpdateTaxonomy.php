<?php

require_once 'xc.lib.php';
require_once 'UpdateAbstract.php';

class UpdateTaxonomy extends UpdateAbstract
{

    public function run($argv)
    {
        if (count($argv) < 4) {
            $this->output($this->usage($argv[0]));
            return $this->buffer;
        }

        // Ruud: before we start, first let's check if all files are present
        foreach (array_slice($argv, 1, 3) as $file) {
            if (!file_exists($file)) {
                $this->output("Cannot locate '$file'!");
                return $this->buffer;
            }
            if (!$this->minioPath) {
                $this->minioPath = dirname($file) . '/';
            }
        }

        if (isset($argv[4]) && $argv[4] == '-r') {
            $this->debug = false;
        }

        if (isset($argv[5]) && $argv[5] == '-f') {
            $this->feedback = true;
        }

        // 'changes' files
        $iocxml = $argv[1];
        include $argv[3];

        if (!isset($renames) || !isset($lumps) || !isset($splits)) {
            $this->abort(
                "Renames, splits and/or lumps arrays cannot be found! Please verify that $argv[3] contains valid PHP and that no short tags (<? instead of <?php) are used."
            );
            return $this->buffer;
        }

        // Check if split files are present; echo message in either case
        if (!file_exists($this->minioPath . 'splits_fg.csv') || !file_exists($this->minioPath . 'splits_bg.csv')) {
            $this->output(
                "INFO: 'splits_fg.csv' and 'splits_bg.csv' files are not present; you may want to add these to post-process individual recordings.\n"
            );
        } else {
            $this->processSplits = true;
            $this->output(
                "INFO: 'splits_fg.csv' and 'splits_bg.csv' files are present; these will be used to post-process individual recordings.\n"
            );
        }

        $this->db = new Database();
        $this->db->setDebugging($this->debug);
        $this->db->setLogging();

        // Set break point when it doesn't make sense to continue
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        $this->output("Verifying species_nrs of background species and fixing if necesssary...");
        $res = $this->db->query(
            'SELECT DISTINCT species_nr, scientific FROM birdsounds_background ORDER BY scientific'
        );

        while ($row = $res->fetch_object()) {
            $stmt = $this->db->prepare(
                'SELECT species_nr, CONCAT(genus, " ", species) FROM taxonomy WHERE species_nr = ?'
            );
            $stmt->bind_param('s', $row->species_nr);
            $error = $taxSpeciesNr = $taxScientific = false;
            if (!$stmt->execute()) {
                $this->abort($stmt->error);
            }
            if (!$stmt->bind_result($taxSpeciesNr, $taxScientific)) {
                $this->abort($stmt->error);
            }
            if (!$stmt->fetch()) {
                $error = 1;
                $this->output("    species number $row->species_nr for $row->scientific does not exist in taxonomy");
            } elseif ($taxScientific != $row->scientific) {
                $error = 2;
                $this->output(
                    "    scientific name for species number $row->species_nr does not match ($row->scientific != $taxScientific)"
                );
            }
            $stmt->close();

            if ($error) {
                [$genus, $species] = explode(' ', $row->scientific);
                $correctNr = $this->db->findSpeciesNumber($genus, $species);
                if ($correctNr) {
                    $this->output("    -> correct species number for $row->scientific is $correctNr, fixing...");
                    if ($error == 1) {
                        $this->db->fixBackgroundByNr($row->species_nr, $correctNr);
                    } else {
                        $this->db->fixBackgroundByScientific($row->scientific, $correctNr);
                    }
                } else {
                    $this->abort(
                        "Aborting. Background species table should be fixed manually before we can continue."
                    );
                }
            }
        }
        $this->output("Creating update database tables...");
        $this->db->createUpdateTables();

        // No need to create backup tables for dry run
        if (!$this->debug) {
            $this->output("Backing up database tables...");
            $this->db->createBackupTables();
        }

        $ctx = new ParseContext($this->db);
        $parser = xml_parser_create('UTF-8');
        xml_set_element_handler($parser, [$ctx, 'handle_element_start'], [$ctx, 'handle_element_end']);
        xml_set_character_data_handler($parser, [$ctx, 'handle_character_data',]);

        $this->output("Parsing master list $iocxml");
        $fp = fopen($iocxml, 'r');
        while (($data = fread($fp, 16384))) {
            $this->output('.', '');
            if (!xml_parse($parser, $data, feof($fp))) {
                $line = xml_get_current_line_number($parser);
                $col = xml_get_current_column_number($parser);
                $this->abort(
                    "Xml parsing error at line $line, column $col: " . xml_error_string(xml_get_error_code($parser))
                );
            }
        }
        $this->output("\n");

        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        $multilingualcsv = $argv[2];
        $this->output("Parsing multilingual list $multilingualcsv...");
        $fp = fopen($multilingualcsv, 'r');
        $line = 0;
        while (($fields = fgetcsv($fp))) {
            // First row contains labels
            if ($line == 0) {
                $this->setMapping($fields, $this->checkMultilanguageFields);
                $line++;
                continue;
            }

            // Check if header was set
            if (is_null($this->getMapping('Scientific name'))) {
                $this->abort("Header line in $multilingualcsv csv file is missing");
                return $this->buffer;
            }

            if (($line++) % 20 == 0) {
                $this->output('.', '');
            }

            $stmt = $this->db->prepare(
                'INSERT INTO taxonomy_multilingual_next (
                    `species_nr`,
                    `genus`,
                    `species`,
                    `english`,
                    `chinese`,
                    `chinese_tw`,
                    `czech`,
                    `danish`,
                    `dutch`,
                    `estonian`,
                    `finnish`,
                    `french`,
                    `german`,
                    `hungarian`,
                    `italian`,
                    `japanese`,
                    `lithuanian`,
                    `norwegian`,
                    `polish`,
                    `portuguese`,
                    `russian`,
                    `slovak`,
                    `spanish`,
                    `swedish`,
                    `afrikaans`,
                    `catalan`,
                    `icelandic`,
                    `indonesian`,
                    `latvian`,
                    `northern_sami`,
                    `slovenian`,
                    `thai`,
                    `ukranian`
                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?)'
            );

            $fields = array_map('trim', $fields);
            $binomial = explode(' ', $fields[$this->getMapping('Scientific name')]);
            if (count($binomial) != 2) {
                $this->abort(
                    "Scientific name '{$fields[$this->getMapping('Scientific name')]}' isn't a binomial on line $line"
                );
            }

            $en = $fields[$this->getMapping('English')];
            $genus = $binomial[0];
            $species = $binomial[1] ?? null;
            $chinese = $fields[$this->getMapping('Chinese')];
            $chinese_tw = $fields[$this->getMapping('Taiwanese')]; // new
            $czech = $fields[$this->getMapping('Czech')];
            $danish = $fields[$this->getMapping('Danish')];
            $dutch = $fields[$this->getMapping('Dutch')];
            $estonian = $fields[$this->getMapping('Estonian')];
            $finnish = $fields[$this->getMapping('Finnish')];
            $french = $fields[$this->getMapping('French')];
            $german = $fields[$this->getMapping('German')];
            $hungarian = $fields[$this->getMapping('Hungarian')];
            $italian = $fields[$this->getMapping('Italian')];
            $japanese = $fields[$this->getMapping('Japanese')];
            $lithuanian = $fields[$this->getMapping('Lithuanian')]; // new
            $norwegian = $fields[$this->getMapping('Norwegian')];
            $polish = $fields[$this->getMapping('Polish')];
            $portuguese = $fields[$this->getMapping('Portuguese')];
            $russian = $fields[$this->getMapping('Russian')];
            $slovak = $fields[$this->getMapping('Slovak')];
            $spanish = $fields[$this->getMapping('Spanish')];
            $swedish = $fields[$this->getMapping('Swedish')];

            // New!
            $afrikaans = $fields[$this->getMapping('Afrikaans')];
            $catalan = $fields[$this->getMapping('Catalan')];
            $icelandic = $fields[$this->getMapping('Icelandic')];
            $indonesian = $fields[$this->getMapping('Indonesian')];
            $latvian = $fields[$this->getMapping('Latvian')];
            $northern_sami = $fields[$this->getMapping('Northern Sami')];
            $slovenian = $fields[$this->getMapping('Slovenian')];
            $thai = $fields[$this->getMapping('Thai')];
            $ukranian = $fields[$this->getMapping('Ukranian')];

            $success = false;
            for ($j = 0; $j < 5; $j++) {
                // in case the new random species number is a duplicate, try several
                // times
                $newNr = $this->db->generateSpeciesNumber();
                if (!$stmt->bind_param(
                    'sssssssssssssssssssssssssssssssss',
                    $newNr,
                    $genus,
                    $species,
                    $en,
                    $chinese,
                    $chinese_tw,
                    $czech,
                    $danish,
                    $dutch,
                    $estonian,
                    $finnish,
                    $french,
                    $german,
                    $hungarian,
                    $italian,
                    $japanese,
                    $lithuanian,
                    $norwegian,
                    $polish,
                    $portuguese,
                    $russian,
                    $slovak,
                    $spanish,
                    $swedish,
                    $afrikaans,
                    $catalan,
                    $icelandic,
                    $indonesian,
                    $latvian,
                    $northern_sami,
                    $slovenian,
                    $thai,
                    $ukranian
                )) {
                    $this->abort($stmt->error);
                }

                if ($stmt->execute() !== false) {
                    $success = true;
                    break;
                }
            }

            if (!$success) {
                $this->abort($stmt->error);
            }
        }
        $this->output("\n");

        // Problems creating both tables?
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        $this->output("Updating species_nrs to retain continuity...");
        $this->db->query(
            'UPDATE taxonomy_next N INNER JOIN taxonomy T USING(genus, species)
             SET N.species_nr = T.species_nr, N.extinct = T.extinct, N.restricted = T.restricted'
        );

        $this->output("Processing renames...");
        $i = 0;
        $renames = $this->removeDuplicates($renames);
        if (!empty($renames)) {
            $stmt = $this->db->prepare(
                'UPDATE taxonomy_next N, taxonomy T SET
                N.species_nr = T.species_nr,
                N.avibase_id = T.avibase_id,
                N.america = T.america,
                N.africa = T.africa,
                N.asia = T.asia,
                N.australia = T.australia,
                N.europe = T.europe,
                N.recordings = T.recordings,
                N.back_recordings = T.back_recordings,
                N.extinct = T.extinct,
                N.restricted = T.restricted
                WHERE T.species_nr = ? AND N.species_nr = ?'
            );
            foreach ($renames as $ren) {
                $from = $ren[0];
                $to = $ren[1];
                $this->output("    renaming $from[0] $from[1] to $to[0] $to[1]");

                // just verify that these are valid taxa
                $fromnr = $this->db->findSpeciesNumber($from[0], $from[1]);
                if (empty($fromnr)) {
                    $this->abort(
                        "Cannot rename $from[0] $from[1] to $to[0] $to[1], $from[0] $from[1] does not exist"
                    );
                }
                $tonr = $this->db->findSpeciesNumberNext($to[0], $to[1]);
                if (empty($tonr)) {
                    $this->abort(
                        "Cannot rename $from[0] $from[1] to $to[0] $to[1], $to[0] $to[1] does not exist"
                    );
                }

                // Ruud: do not die on missing species; skip instead
                if (!empty($fromnr) && !empty($tonr)) {
                    // New name cannot occur in old table!
                    if ($this->db->speciesExists($to[0], $to[1])) {
                        $this->abort(
                            "Cannot rename $from[0] $from[1] to $to[0] $to[1], species already exists"
                        );
                    } else {
                        $stmt->bind_param('ss', $fromnr, $tonr);
                        if (!$stmt->execute()) {
                            $this->abort($stmt->error);
                        } elseif ($stmt->affected_rows == 0) {
                            $this->abort(
                                "Query renaming $from[0] $from[1] to $to[0] $to[1] produced no results"
                            );
                        }
                        $i++;
                    }
                }
            }
            $stmt->close();
            $this->output("Processed $i out of " . count($renames) . " renames");
        }

        // Any problems with renames?
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        $this->output("Setting proper species_nr in ssp and multilingual tables...");
        $this->db->query(
            'UPDATE taxonomy_ssp_next S
            INNER JOIN taxonomy_next T USING(genus, species)
            SET S.species_nr = T.species_nr'
        );
        $this->db->query(
            'UPDATE taxonomy_multilingual_next M
            INNER JOIN taxonomy_next T USING(genus, species)
            SET M.species_nr = T.species_nr'
        );

        // Test if English names in xml and csv match
        $this->output("Cross-checking common names in XML and CSV files...");
        $this->db->verifyEnglishNames();
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        $notInNewClassification = [];
        $res = $this->db->query(
            "SELECT t1.genus, t1.species, count(t3.snd_nr) as nr
            FROM taxonomy as t1
            LEFT JOIN taxonomy_next as t2 on t1.species_nr = t2.species_nr
            LEFT JOIN birdsounds AS t3 on t1.species_nr = t3.species_nr
            WHERE t1.group_id = 1 AND t2.species_nr IS NULL
            GROUP BY t1.genus, t1.species
            ORDER BY t1.genus, t1.species"
        );
        while ($row = $res->fetch_object()) {
            $notInNewClassification[] = "$row->genus $row->species (recordings: $row->nr)";
        }

        $this->output("\nCopying pseudospecies and species from other groups over...");
        $this->db->query(
            'INSERT IGNORE INTO taxonomy_next
            SELECT T.* from taxonomy T
            WHERE T.group_id != 1'
        );

        $this->output("\nCopying extra species over...");
        $this->db->query(
            'INSERT IGNORE INTO taxonomy_next
            SELECT T.* from taxonomy T
            LEFT JOIN taxonomy_multilingual M USING(species_nr)
            WHERE T.group_id = 1 AND M.species_nr IS NULL'
        );

        $i = 0;
        $splits = $this->removeDuplicates($splits);
        $this->output("Processing splits...");
        if (!empty($splits)) {
            $stmt = $this->db->prepare(
                'UPDATE birdsounds_next B, taxonomy_next T
                SET B.species_nr = T.species_nr, B.genus = T.genus, B.species = T.species, B.eng_name = T.eng_name, B.ssp = ?
                WHERE B.species_nr = ? AND B.ssp = ? AND T.species_nr = ? AND B.group_id = 1'
            );
            foreach ($splits as $split) {
                $from = $split[0];
                $to = $split[1];
                $this->output("    splitting $from[0] $from[1] $from[2]");

                // just verify that these are valid taxa
                $fromnr = $this->db->findSpeciesNumber($from[0], $from[1]);
                $tonr = $this->db->findSpeciesNumberNext($to[0], $to[1]);

                if (!empty($fromnr) && !empty($tonr)) {
                    $stmt->bind_param('ssss', $to[2], $fromnr, $from[2], $tonr);
                    if (!$stmt->execute()) {
                        $this->abort($stmt->error);
                    }
                    $i++;
                }
            }
            $stmt->close();
            $this->output("Processed $i out of " . count($splits) . " splits\n");
        }

        // Any problems with splits?
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        $i = 0;
        $lumps = $this->removeDuplicates($lumps);
        $this->output("Processing lumps...");
        if (!empty($lumps)) {
            $stmt1 = $this->db->prepare(
                'UPDATE birdsounds_next B, taxonomy_next T SET
                B.species_nr = T.species_nr, B.ssp = ?
                WHERE B.species_nr = ? AND T.species_nr = ?'
            );
            $stmt2 = $this->db->prepare(
                'UPDATE birdsounds_next B, taxonomy_next T SET
                B.species_nr = T.species_nr, B.ssp = ?
                WHERE B.species_nr = ? AND B.ssp = ? AND T.species_nr = ?'
            );
            $stmt3 = $this->db->prepare(
                'UPDATE birdsounds_background_next SET species_nr = ? WHERE species_nr = ?'
            );

            foreach ($lumps as $lump) {
                $from = $lump[0];
                $to = $lump[1];

                // set ssp to null if not present
                if (!isset($from[2])) {
                    $from[2] = null;
                }

                $this->output("    lumping $from[0] $from[1] $from[2]");

                // also need to lump bg species.  first find species_nr
                $fromnr = $this->db->findSpeciesNumber($from[0], $from[1]);
                $tonr = $this->db->findSpeciesNumberNext($to[0], $to[1]);

                if (!empty($fromnr) && !empty($tonr)) {
                    if (is_null($from[2])) {
                        $stmt1->bind_param('sss', $to[2], $fromnr, $tonr);
                        if (!$stmt1->execute()) {
                            $this->abort($stmt1->error);
                        }
                    } else {
                        $stmt2->bind_param('ssss', $to[2], $fromnr, $from[2], $from[2]);
                        if (!$stmt2->execute()) {
                            $this->abort($stmt2->error);
                        }
                    }
                    $stmt3->bind_param('ss', $tonr, $fromnr);
                    if (!$stmt3->execute()) {
                        $this->abort($stmt3->error);
                    }
                    $i++;
                }
            }
            $stmt1->close();
            $stmt2->close();
            $stmt3->close();
            $this->output("Processed $i out of " . count($lumps) . " lumps\n");
        }

        // ... or lumps?
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        // update all other information that is de-normalized in the birdsounds table...
        $this->output("Updating all de-normalized data in birdsounds...");
        $this->db->query(
            'UPDATE birdsounds_next B
            INNER JOIN taxonomy_next T USING(species_nr)
            SET B.family = T.family, B.order_nr = T.IOC_order_nr, B.genus = T.genus, B.species = T.species, B.eng_name = T.eng_name
            WHERE B.group_id = 1'
        );

        // Do a final check to see if there weren't any problems with the
        // combination of renames, splits and lumps
        $this->output("Checking for orphaned species in birdsounds table...");
        $res = $this->db->query(
            "SELECT TRIM(CONCAT(t1.genus, ' ' , t1.species, ' ' , t1.ssp)) as fullname, COUNT(1) as nr
            FROM birdsounds_next as t1
            LEFT JOIN taxonomy_next as t2 on t1.species_nr = t2.species_nr
            WHERE t1.group_id = 1 AND t1.species_nr != 'mysmys' AND t1.species_nr != 'envrec' AND t2.species_nr IS NULL
            GROUP BY fullname
            ORDER BY fullname"
        );
        while ($row = $res->fetch_object()) {
            $this->abort("$row->fullname in birdsounds not present in taxonomy ($row->nr)");
        }
        // Stop if orphans are found
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        // update background species now
        $this->output("Updating background species...");
        $stmt1 = $this->db->prepare(
            'SELECT CONCAT(genus, " ", species), eng_name, family FROM taxonomy_next WHERE species_nr = ?'
        );
        $stmt2 = $this->db->prepare(
            'UPDATE birdsounds_background_next SET english = ?, scientific = ?, family = ? WHERE species_nr = ?'
        );
        $i = 0;
        $missing = [];
        $res = $this->db->query(
            'SELECT DISTINCT species_nr, scientific AS background_species FROM birdsounds_background_next'
        );
        while ($row = $res->fetch_object()) {
            if (($i++) % 100 == 0) {
                $this->output('.', '');
            }
            $nr = $row->species_nr;
            $english = $scientific = $family = null;
            $stmt1->bind_param('s', $nr);
            $stmt1->execute();
            $stmt1->store_result();
            $stmt1->bind_result($scientific, $english, $family);
            $stmt1->fetch();

            // Background species is missing; break later
            if (empty($scientific)) {
                $missing[$nr] = $row->background_species;
            }

            $stmt2->bind_param('ssss', $english, $scientific, $family, $nr);
            $stmt2->execute();
        }
        $stmt1->close();
        $stmt2->close();
        $this->output("\n");

        foreach ($missing as $nr => $species) {
            if (!in_array($nr, ['envrec', 'mysmys'])) {
                $this->abort(
                    "Background species $species (nr: $nr) not found! Check if it should be added to changes.php:"
                );
            }
        }

        // All background updates OK?
        if ($this->breakOnErrors()) {
            return $this->buffer;
        }

        // Optional splits for individual records
        if ($this->processSplits) {
            $this->clearMapping();
            $foregroundCsv = $this->minioPath . 'splits_fg.csv';
            $this->output("Parsing foreground splits list $foregroundCsv...");
            $fp = fopen($foregroundCsv, 'r');
            $line = 0;

            $stmt = $this->db->prepare(
                'UPDATE birdsounds_next
            SET genus = ?, species = ?, ssp = ?, eng_name = ?, family = ?, order_nr = ?, species_nr = ?
            WHERE snd_nr = ?'
            );

            while (($fields = fgetcsv($fp))) {
                // First row contains labels
                if ($line == 0) {
                    $this->setMapping($fields, $this->checkForegroundFields);
                    $line++;
                    continue;
                }

                // Check if header was set
                if (empty($this->getMapping('xcid'))) {
                    $this->abort("Header line in $foregroundCsv csv file is missing");
                    return $this->buffer;
                }

                $line++;
                $fields = array_map('trim', $fields);

                $sndNr = $fields[$this->getMapping('xcid')];
                $prevGenus = $fields[$this->getMapping('oldgenus')];
                $prevSpecies = $fields[$this->getMapping('oldspecies')];
                $prevSsp = $fields[$this->getMapping('oldsubspecies')];

                $binomial = explode(' ', $fields[$this->getMapping('newsciname')]);
                if (count($binomial) != 2) {
                    $this->abort(
                        "Foreground scientific name '{$fields[$this->getMapping('newsciname')]}' isn't a binomial on line $line"
                    );
                    continue;
                }

                $nextGenus = $binomial[0];
                $nextSpecies = $binomial[1] ?? '';
                $nextSsp = $fields[$this->getMapping('newsspname')];

                // Does the old name match with the current recording?
                $rec = $this->db->getRecording($sndNr);
                if (empty($rec)) {
                    $this->output("WARNING! Recording XC$sndNr on line $line does not exist");
                    continue;
                } elseif (!($prevGenus == $rec->genus && $prevSpecies == $rec->species)) {
                    $this->abort(
                        "Foreground $rec->genus $rec->species in recording does not match $prevGenus $prevSpecies on line $line"
                    );
                    continue;
                } elseif ($prevSsp && strtolower($prevSsp) != 'null' && $prevSsp != $rec->ssp) {
                    $this->output("WARNING! Foreground ssp $prevSsp does not match $rec->ssp on line $line");
                }

                // Does the species to update to actually exist?
                $nextData = $this->db->getSpeciesNext($nextGenus, $nextSpecies);
                if (is_null($nextData)) {
                    $this->abort("Cannot locate next foreground species $nextGenus $nextSpecies in taxonomy");
                    continue;
                }

                if (!$stmt->bind_param(
                    'ssssssss',
                    $nextGenus,
                    $nextSpecies,
                    $nextSsp,
                    $nextData->eng_name,
                    $nextData->order_nr,
                    $nextData->family,
                    $nextData->species_nr,
                    $sndNr
                )) {
                    $this->abort($stmt->error);
                }
                if (!$stmt->execute()) {
                    $this->abort($stmt->error);
                }
            }

            // Problems with foreground species?
            if ($this->breakOnErrors()) {
                return $this->buffer;
            }

            $this->output("Processed $line foreground splits");

            $backgroundCsv = $this->minioPath . 'splits_bg.csv';
            $this->output("\nParsing multilingual list $backgroundCsv...");
            $fp = fopen($backgroundCsv, 'r');
            $this->clearMapping();
            $line = 0;

            $stmt = $this->db->prepare(
                'UPDATE birdsounds_background_next
            SET scientific = ?, english = ?, family = ?, species_nr = ?
            WHERE id = ?'
            );

            while (($fields = fgetcsv($fp))) {
                // First row contains labels
                if ($line == 0) {
                    $this->setMapping($fields, $this->checkBackgroundFields);
                    $line++;
                    continue;
                }

                // Check if header was set
                if (empty($this->getMapping('xcid'))) {
                    $this->abort("Header line in $backgroundCsv csv file is missing");
                    return $this->buffer;
                }

                $line++;
                $fields = array_map('trim', $fields);

                $sndNr = $fields[$this->getMapping('xcid')];
                $backgroundText = $fields[$this->getMapping('bgtextentry')];

                $backData = $this->db->findBackgroundData($backgroundText, $sndNr);
                if (empty($backData)) {
                    $this->abort(
                        "Background recording for $backgroundText not found in birdsounds_background on line $line"
                    );
                    continue;
                }

                $binomial = explode(' ', $fields[$this->getMapping('newsciname')]);
                if (count($binomial) != 2) {
                    $this->abort(
                        "Background scientific name '{$fields[$this->getMapping('newsciname')]}' isn't a binomial on line $line"
                    );
                    continue;
                }

                $nextGenus = $binomial[0];
                $nextSpecies = $binomial[1];

                // Does the species to update to actually exist?
                $nextData = $this->db->getSpeciesNext($nextGenus, $nextSpecies);
                if (is_null($nextData)) {
                    $this->abort("Cannot locate next background species $nextGenus $nextSpecies in taxonomy");
                    continue;
                }
                $scientificNext = "$nextData->genus $nextData->species";

                if (!$stmt->bind_param(
                    'sssss',
                    $scientificNext,
                    $nextData->eng_name,
                    $nextData->family,
                    $nextData->species_nr,
                    $backData->id
                )) {
                    $this->abort($stmt->error);
                }
                if (!$stmt->execute()) {
                    $this->abort($stmt->error);
                }
            }

            // Problems with background species?
            if ($this->breakOnErrors()) {
                return $this->buffer;
            }
            $this->output("Processed $line background splits\n");
        }

        // Ruud: production, actually publish the changes
        if (!$this->debug) {
            // rename new tables
            $this->output("\nCopying new tables over...");
            $this->db->query('DROP TABLE taxonomy');
            $this->db->query('RENAME TABLE taxonomy_next TO taxonomy');
            $this->db->query('DROP TABLE taxonomy_ssp');
            $this->db->query('RENAME TABLE taxonomy_ssp_next TO taxonomy_ssp');
            $this->db->query('DROP TABLE taxonomy_multilingual');
            $this->db->query('RENAME TABLE taxonomy_multilingual_next TO taxonomy_multilingual');
            $this->db->query('DROP TABLE birdsounds');
            $this->db->query('RENAME TABLE birdsounds_next TO birdsounds');
            $this->db->query('DROP TABLE birdsounds_background');
            $this->db->query('RENAME TABLE birdsounds_background_next TO birdsounds_background');
            $this->db->query('DROP TABLE birdsounds_history');
            $this->db->query('RENAME TABLE birdsounds_history_next TO birdsounds_history');
            $this->db->dropTrigger('sound_updated_next');
            $this->db->createTrigger('sound_updated', 'birdsounds', 'birdsounds_history');

            // update statistics
            $this->output("Updating stats...");
            include_once 'tasks/update-daily-stats.php';
            include_once 'tasks/update-hourly-stats.php';
            $this->output("Classification successfully updated!");
            // Dry run, clean up test data
        } else {
            // Report species that may need to be added to changes.php
            if (!empty($notInNewClassification)) {
                $this->output(
                    "INFO: the following species occur in the current taxonomy but not in the next. Please verify they are present in changes.php.\n"
                );
                foreach ($notInNewClassification as $not) {
                    $this->output("$not");
                }
            }
            $this->output("\nDry run passed successfully!");
        }
        return $this->buffer;
    }
}
