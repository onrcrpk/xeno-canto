<?php

require_once 'Database.php';
require_once 'ParseContext.php';
require_once 'Taxon.php';

class UpdateAbstract
{

    // Set to true for dry run; false for production script
    protected bool $debug = true;

    protected $feedback;

    protected $buffer;

    protected $db;

    protected $mapping;

    protected $minioPath;

    protected $checkMultilanguageFields = [
        'English',
        'Chinese',
        'Taiwanese',
        'Czech',
        'Danish',
        'Dutch',
        'Estonian',
        'Finnish',
        'French',
        'German',
        'Hungarian',
        'Italian',
        'Japanese',
        'Lithuanian',
        'Norwegian',
        'Polish',
        'Portuguese',
        'Russian',
        'Slovak',
        'Spanish',
        'Swedish',
        'Scientific name',
        'Afrikaans',
        'Catalan',
        'Icelandic',
        'Indonesian',
        'Latvian',
        'Northern Sami',
        'Slovenian',
        'Thai',
        'Ukranian',
    ];

    protected array $checkForegroundFields = [
        'xcid',
        'oldgenus',
        'oldspecies',
        'oldsubspecies',
        'newsciname',
        'newsspname'
    ];

    protected array $checkBackgroundFields = ['xcid', 'bgtextentry', 'newsciname'];

    protected bool $processSplits = false;


    public function hasErrors()
    {
        return $this->db->hasErrors();
    }

    public function setFeedback($bool = false)
    {
        $this->feedback = (bool)$bool;
    }

    public function dryRun($bool = true)
    {
        $this->debug = (bool)$bool;
    }

    protected function breakOnErrors()
    {
        // Error handling and logging in db class
        if ($this->debug && $this->db->hasErrors()) {
            $this->output(
                "\nScript stopped due to breaking error(s):\n" . file_get_contents($this->db->getLogPath()) . "\n"
            );
            return true;
        }
        return false;
    }

    protected function output($msg, $break = "\n")
    {
        if ($this->feedback) {
            echo $msg . $break;
        } else {
            $this->buffer .= $msg . $break;
        }
    }

    protected function getMapping($col)
    {
        return $this->mapping[strtolower($col)] ?? '';
    }

    protected function setMapping($fields, $checkFields = [])
    {
        if (!$this->mapping) {
            $this->mapping = array_flip(array_map('strtolower', $fields));
            foreach ($checkFields as $col) {
                if (!isset($this->mapping[strtolower($col)])) {
                    $this->abort('Missing column in csv file: ' . $col);
                    return false;
                }
            }
        }
        return $this->mapping;
    }

    protected function abort($msg)
    {
        $this->db->abort($msg . "");
        $this->buffer .= $msg . "\n";
    }

    protected function clearMapping()
    {
        $this->mapping = [];
    }

    protected function usage($scriptname)
    {
        return "Usage: $scriptname PATH/TO/IOC/XML PATH/TO/MULTILINGUAL/CSV PATH/TO/CHANGES.php [-dr|-f]";
    }

    protected function removeDuplicates($array)
    {
        $array = array_map(function ($item) {
            return json_encode($item);
        }, $array);
        $json = '[' . implode(',', array_keys(array_flip($array))) . ']';
        return json_decode($json);
    }

    
}
