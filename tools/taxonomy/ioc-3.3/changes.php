<?php

// format of this file:  There should be 3 variables: $renames, $splits, $lumps.
// Each of these variables should be an array of changes.  Each element of the
// array should also be a two-element array.  The first element is the source
// taxon, the second one the destination.  Each of these taxa is a three-element
// array, specifying genus, species, ssp...

$renames = [
    // accipitridae
    [['Aquila', 'pomarina'], ['Clanga', 'pomarina']],
    [['Aquila', 'hastata'], ['Clanga', 'hastata']],
    [['Aquila', 'clanga'], ['Clanga', 'clanga']],
    [
        ['Leucopternis', 'plumbeus'],
        [
            'Cryptoleucopteryx',
            'plumbea',
        ],
    ],
    [
        ['Leucopternis', 'schistaceus'],
        [
            'Buteogallus',
            'schistaceus',
        ],
    ],
    [
        ['Leucopternis', 'lacernulatus'],
        [
            'Buteogallus',
            'lacernulatus',
        ],
    ],
    [
        ['Harpyhaliaetus', 'solitarius'],
        [
            'Buteogallus',
            'solitarius',
        ],
    ],
    [
        ['Harpyhaliaetus', 'coronatus'],
        [
            'Buteogallus',
            'coronatus',
        ],
    ],
    [['Leucopternis', 'princeps'], ['Morphnarchus', 'princeps']],
    [['Buteo', 'magnirostris'], ['Rupornis', 'magnirostris']],
    [['Buteo', 'leucorrhous'], ['Parabuteo', 'leucorrhous']],
    [['Buteo', 'albicaudatus'], ['Geranoaetus', 'albicaudatus']],
    [['Buteo', 'polyosoma'], ['Geranoaetus', 'polyosoma']],
    [['Leucopternis', 'polionotus'], ['Pseudastur', 'polionotus']],
    [['Leucopternis', 'albicollis'], ['Pseudastur', 'albicollis']],
    [
        ['Leucopternis', 'occidentalis'],
        [
            'Pseudastur',
            'occidentalis',
        ],
    ],
    [
        ['Dryotriorchis', 'spectabilis'],
        [
            'Circaetus',
            'spectabilis',
        ],
    ],
    [['Icthyophaga', 'humilis'], ['Haliaeetus', 'humilis']],
    [
        ['Icthyophaga', 'ichthyaetus'],
        [
            'Haliaeetus',
            'ichthyaetus',
        ],
    ],

    [['Fulica', 'newtoni'], ['Fulica', 'newtonii']],
    [['Thinomis', 'rubricollis'], ['Thinomis', 'cucullatus']],

    // caprimulgidae
    [['Caprimulgus', 'enarratus'], ['Gactomis', 'enarratus']],
    [['Caprimulgus', 'nigrescens'], ['Nyctipolus', 'nigrescens']],
    [
        ['Caprimulgus', 'hirundinaceus'],
        [
            'Nyctipolus',
            'hirundinaceus',
        ],
    ],
    [['Caprimulgus', 'anthonyi'], ['Nyctidromus', 'anthonyi']],
    [['Caprimulgus', 'heterurus'], ['Setopagis', 'heterurus']],
    [['Caprimulgus', 'parvulus'], ['Setopagis', 'parvulus']],
    [['Caprimulgus', 'whitelyi'], ['Setopagis', 'whitelyi']],
    [['Caprimulgus', 'maculosus'], ['Setopagis', 'maculosus']],
    [['Caprimulgus', 'candicans'], ['Eleothreptus', 'candicans']],
    [
        ['Caprimulgus', 'longirostris'],
        [
            'Systellura',
            'longirostris',
        ],
    ],
    [
        ['Caprimulgus', 'cayennensis'],
        [
            'Hydropsalis',
            'cayennensis',
        ],
    ],
    [
        ['Caprimulgus', 'maculicaudus'],
        [
            'Hydropsalis',
            'maculicaudus',
        ],
    ],

    [['Stellula', 'calliope'], ['Selasphorus', 'calliope']],
    [['Anairetes', 'agilis'], ['Uromyias', 'agilis']],
    [['Anairetes', 'agraphia'], ['Uromyias', 'agraphia']],
    [['Myrmotherula', 'gularis'], ['Rhopias', 'gularis']],
    [['Muscicapa', 'dauurica'], ['Muscicapa', 'latirostris']],

    // fringillidae
    [['Eremopsaltria', 'mongolica'], ['Bucanetes', 'mongolicus']],
    [['Carpodacus', 'rubescens'], ['Agraphospiza', 'rubescens']],
    [['Carpodacus', 'nipalensis'], ['Procarduelis', 'nipalensis']],
    [['Haematospiza', 'sipahi'], ['Carpodacus', 'sipahi']],
    [
        ['Chaunoproctus', 'ferreorostris'],
        [
            'Carpodacus',
            'ferreorostris',
        ],
    ],
    [['Koslowia', 'roborowskii'], ['Carpodacus', 'roborowskii']],
    [['Uragus', 'sibiricus'], ['Carpodacus', 'sibiricus']],
    [
        ['Pinicola', 'subhimachalus'],
        [
            'Carpodacus',
            'subhimachalus',
        ],
    ],
    [
        ['Hemignathus', 'sagittirostris'],
        [
            'Viridonia',
            'sagittirostris',
        ],
    ],
    [['Hemignathus', 'obscura'], ['Akialoa', 'obscura']],
    [['Hemignathus', 'ellisiana'], ['Akialoa', 'ellisiana']],
    [['Hemignathus', 'munroi'], ['Hemignathus', 'wilsoni']],
    [['Hemignathus', 'virens'], ['Chlorodrepanis', 'virens']],
    [['Hemignathus', 'flavus'], ['Chlorodrepanis', 'flava']],
    [
        ['Hemignathus', 'kauaiensis'],
        [
            'Chlorodrepanis',
            'stejnegeri',
        ],
    ],
    [['Carpodacus', 'purpureus'], ['Haemorhous', 'purpureus']],
    [['Carpodacus', 'cassinii'], ['Haemorhous', 'cassinii']],
    [['Carpodacus', 'mexicanus'], ['Haemorhous', 'mexicanus']],
    [['Neospiza', 'concolor'], ['Crithagra', 'concolor']],
    [['Carduelis', 'flavirostris'], ['Linaria', 'flavirostris']],
    [['Carduelis', 'cannabina'], ['Linaria', 'cannabina']],
    [['Carduelis', 'yemenensis'], ['Linaria', 'yemenensis']],
    [['Carduelis', 'johannis'], ['Linaria', 'johannis']],
    [['Carduelis', 'flammea'], ['Acanthis', 'flammea']],
    [['Carduelis', 'hornemanni'], ['Acanthis', 'hornemanni']],
    [['Serinus', 'estherae'], ['Chrysocorythus', 'estherae']],
    [['Crithagra', 'alario'], ['Serinus', 'alario']],
    [['Serinus', 'thibetanus'], ['Spinus', 'thibetanus']],
    [['Carduelis', 'lawrencei'], ['Spinus', 'lawrencei']],
    [['Carduelis', 'tristis'], ['Spinus', 'tristis']],
    [['Carduelis', 'psaltria'], ['Spinus', 'psaltria']],
    [['Carduelis', 'spinus'], ['Spinus', 'spinus']],
    [['Carduelis', 'dominicensis'], ['Spinus', 'dominicensis']],
    [['Carduelis', 'pinus'], ['Spinus', 'pinus']],
    [['Carduelis', 'atriceps'], ['Spinus', 'atriceps']],
    [['Carduelis', 'notata'], ['Spinus', 'notata']],
    [['Carduelis', 'barbata'], ['Spinus', 'barbata']],
    [['Carduelis', 'xanthogastra'], ['Spinus', 'xanthogastra']],
    [['Carduelis', 'olivacea'], ['Spinus', 'olivacea']],
    [['Carduelis', 'magellanica'], ['Spinus', 'magellanica']],
    [['Carduelis', 'siemiradzkii'], ['Spinus', 'siemiradzkii']],
    [['Carduelis', 'yarrellii'], ['Spinus', 'yarrellii']],
    [['Carduelis', 'cucullata'], ['Spinus', 'cucullata']],
    [['Carduelis', 'atrata'], ['Spinus', 'atrata']],
    [['Carduelis', 'uropygialis'], ['Spinus', 'uropygialis']],
    [['Carduelis', 'crassirostris'], ['Spinus', 'crassirostris']],
    [['Carduelis', 'spinescens'], ['Spinus', 'spinescens']],
];

$splits = [
    [
        ['Theristicus', 'melanopis', 'branickii'],
        [
            'Theristicus',
            'branickii',
            '',
        ],
    ],
    [
        ['Hypnelus', 'ruficollis', 'bicinctus'],
        [
            'Hypnelus',
            'bicinctus',
            'bicinctus',
        ],
    ],
    [
        ['Hypnelus', 'ruficollis', 'stoicus'],
        [
            'Hypnelus',
            'bicinctus',
            'stoicus',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'stenorhyncha'],
        [
            'Schiffornis',
            'stenorhyncha',
            'stenorhyncha',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'panamensis'],
        [
            'Schiffornis',
            'stenorhyncha',
            'panamensis',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'aenea'],
        [
            'Schiffornis',
            'aenea',
            '',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'veraepacis'],
        [
            'Schiffornis',
            'veraepacis',
            'veraepacis',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'dumicola'],
        [
            'Schiffornis',
            'veraepacis',
            'dumicola',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'rosenbergi'],
        [
            'Schiffornis',
            'veraepacis',
            'rosenbergi',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'acrolophites'],
        [
            'Schiffornis',
            'veraepacis',
            'acrolophites',
        ],
    ],
    [
        ['Schiffornis', 'turdina', 'olivacea'],
        [
            'Schiffornis',
            'olivacea',
            '',
        ],
    ],
    [
        ['Drymophila', 'caudata', 'klagesi'],
        [
            'Drymophila',
            'klagesi',
            '',
        ],
    ],
    [
        ['Drymophila', 'caudata', 'hellmayri'],
        [
            'Drymophila',
            'hellmayri',
            '',
        ],
    ],
    [
        ['Drymophila', 'caudata', 'boliviana'],
        [
            'Drymophila',
            'striaticeps',
            'boliviana',
        ],
    ],
    [
        ['Drymophila', 'caudata', 'peruviana'],
        [
            'Drymophila',
            'striaticeps',
            'peruviana',
        ],
    ],
    [
        ['Drymophila', 'caudata', 'occidentalis'],
        [
            'Drymophila',
            'striaticeps',
            'occidentalis',
        ],
    ],
    [
        ['Drymophila', 'caudata', 'occidentalis'],
        [
            'Drymophila',
            'striaticeps',
            'occidentalis',
        ],
    ],
    [
        ['Myrmeciza', 'immaculata', 'zeledoni'],
        [
            'Myrmeciza',
            'zeledoni',
            'zeledoni',
        ],
    ],
    [
        ['Myrmeciza', 'immaculata', 'macrorhyncha'],
        [
            'Myrmeciza',
            'zeledoni',
            'macrorhyncha',
        ],
    ],
    [
        ['Salpornis', 'spilonotus', 'emini'],
        [
            'Salpornis',
            'salvadori',
            'emini',
        ],
    ],
    [
        ['Salpornis', 'spilonotus', 'erlangeri'],
        [
            'Salpornis',
            'salvadori',
            'erlangeri',
        ],
    ],
    [
        ['Salpornis', 'spilonotus', 'salvadori'],
        [
            'Salpornis',
            'salvadori',
            'salvadori',
        ],
    ],
    [
        ['Salpornis', 'spilonotus', 'xylodromus'],
        [
            'Salpornis',
            'salvadori',
            'xylodromus',
        ],
    ],
    [
        ['Passer', 'domesticus', 'italiae'],
        [
            'Passer',
            'italiae',
            '',
        ],
    ],
    [
        ['Sicalis', 'olivascens', 'mendozae'],
        [
            'Sicalis',
            'mendozae',
            '',
        ],
    ],
];

$lumps = [
    [
        ['Laterallus', 'tuerosi', null],
        [
            'Laterallus',
            'jamaicensis',
            'tuerosi',
        ],
    ],
    [
        ['Corythornis', 'thomensis', null],
        [
            'Corythornis',
            'cristatus',
            'thomensis',
        ],
    ],
    [
        ['Corythornis', 'nais', null],
        [
            'Corythornis',
            'cristatus',
            'nais',
        ],
    ],
];
