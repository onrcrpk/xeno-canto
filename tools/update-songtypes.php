<?php

use function xc\db;

require_once 'xc.lib.php';

class UpdateSoundTypes extends xc\Task
{

    private $soundTypeOptions = [
        'uncertain' => 30,
        'song' => 7,
        'subsong' => 8,
        'call' => 9,
        'alarm call' => 10,
        'flight call' => 11,
        'nocturnal flight call' => 12,
        'begging call' => 13,
        'drumming' => 14,
        'duet' => 15,
        'dawn song' => 16,
        'adult' => 3,
        'juvenile' => 4,
        'hatchling' => 5,
        'nestling' => 5,
        'fledgling' => 5,
        'life stage uncertain' => 29,
        'aberrant' => 17,
        'imitation' => 18,
        'mimicry' => 18,
        'bird in hand' => 25,
        'male' => 1,
        'female' => 2,
        'sex uncertain' => 28,
        'disturbance song' => 22,
        'calling song' => 19,
        'courtship song' => 20,
        'rivalry song' => 21,
        'flight song' => 23,
        'female song' => 33,
        'searching song' => 32
    ];

    private $extracted = [];

    public function __construct()
    {
        parent::__construct('.');
    }

    public function tokenize($str)
    {
        return strtolower(trim($str));
    }

    public function run()
    {
        echo "This script parses song types from the birdsounds table into tokens\n";
        echo "Preparing tables...\n";

        $this->mysqli()->query('CREATE TABLE `_birdsounds_properties_extracted` LIKE  `birdsounds_properties`');
        $this->mysqli()->query('DROP TABLE IF EXISTS `_birdsounds_properties_verbatim`');
        $this->mysqli()->query(
            "CREATE TABLE `_birdsounds_properties_verbatim` (
                `snd_nr` int(11) unsigned NOT NULL,
                `verbatim` varchar(500) DEFAULT NULL,
                PRIMARY KEY (`snd_nr`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
        );

        echo "Creating backup...\n";
        $backupTable = '_birdsounds_properties_backup_' . date('Ymd_His');
        $this->mysqli()->query("CREATE TABLE `$backupTable` LIKE  `_birdsounds_properties_verbatim`");
        $this->mysqli()->query("INSERT INTO `$backupTable` (SELECT `snd_nr`, `songtype` FROM `birdsounds`)");

        echo "Extracting literal strings...\n";
        $i = 0;
        $res = $this->mysqli()->query('SELECT `snd_nr`, `songtype` FROM `birdsounds`');
        while ($row = $res->fetch_object()) {
            if (($i++ % 1000) == 0) {
                echo '.';
            }

            $sndNr = $row->snd_nr;
            $all = array_map([$this, 'tokenize'], explode(',', $row->songtype));

            $extra = $this->soundTypeExtras($all);
            $types = array_diff($all, $extra);

            $verbatim = implode(', ', $extra);
            if ($verbatim) {
                $q = "
                    INSERT INTO `_birdsounds_properties_verbatim` (`snd_nr`, `verbatim`)
                    VALUES ($sndNr, '" . $this->mysqli()->real_escape_string($verbatim) . "')";
                $this->mysqli()->query($q);
            }

            foreach ($types as $type) {
                $propId = $this->soundTypeOptions[$type];
                $property = $this->mysqli()->real_escape_string($this->getProperty($propId)['property']);
                $catId = $this->getProperty($propId)['category_id'];
                $q = "INSERT INTO `_birdsounds_properties_extracted` VALUES ($sndNr, $propId, $catId, '$property', NOW())";
                $this->mysqli()->query($q);
            }
        }

        echo "\n\n";

        // Post-process terms in verbatim field
        // As there may be overlapping terms (flight call vs call),
        // fixedSoundTypes() returns terms in length of term descending.
        // After a hit, the term is removed from verbatim to avoid overlap
        echo "Parsing free-text sound types...\n";
        $res = $this->mysqli()->query('SELECT `snd_nr`, `verbatim` FROM `_birdsounds_properties_verbatim`');
        while ($row = $res->fetch_object()) {
            if (($i++ % 100) == 0) {
                echo '.';
            }
            $verbatim = $row->verbatim;
            foreach ($this->fixedSoundTypes() as $type => $propId) {
                if ($this->termOccurs($verbatim, $type) || $this->termOccurs($verbatim, $type . 's')) {
                    echo "   '$type' parsed from '$verbatim'\n";
                    $property = $this->mysqli()->real_escape_string($this->getProperty($propId)['property']);
                    $catId = $this->getProperty($propId)['category_id'];
                    $q = "INSERT INTO `_birdsounds_properties_extracted` VALUES ($row->snd_nr, $propId, $catId, '$property', NOW())";
                    $this->mysqli()->query($q);
                    // Remove term from verbatim
                    $verbatim = str_replace($type, '', $verbatim);
                }
            }
        }

        echo "Copying parsed sound types...\n";
        $this->mysqli()->query(
            'INSERT IGNORE INTO `birdsounds_properties` (SELECT * FROM `_birdsounds_properties_extracted`)'
        );

        echo "Replacing original sound types with cleaned verbatim strings...\n";
        $this->mysqli()->query("UPDATE `birdsounds` SET `songtype` = ''");
        $res = $this->mysqli()->query('SELECT `snd_nr`, `verbatim` FROM `_birdsounds_properties_verbatim`');
        while ($row = $res->fetch_object()) {
            if (($i++ % 100) == 0) {
                echo '.';
            }
            $verbatim = $this->mysqli()->real_escape_string($row->verbatim);
            $this->mysqli()->query("UPDATE `birdsounds` SET `songtype` = '$verbatim' WHERE snd_nr = $row->snd_nr");
        }

        echo "\nUpdating animal observed and playback used...\n";
        $queries = [
            "UPDATE birdsounds SET `observed` = 1 WHERE snd_nr IN 
                (SELECT snd_nr FROM birdsounds WHERE remarks LIKE '%\n\nbird-seen:yes%')",
            "UPDATE birdsounds SET `observed` = 0 WHERE snd_nr IN 
                (SELECT snd_nr FROM birdsounds WHERE remarks LIKE '%\n\nbird-seen:no%')",
            "UPDATE birdsounds SET `playback` = 1 WHERE snd_nr IN 
                (SELECT snd_nr FROM birdsounds WHERE remarks LIKE '%\n\nplayback-used:yes%')",
            "UPDATE birdsounds SET `playback` = 0 WHERE snd_nr IN 
                (SELECT snd_nr FROM birdsounds WHERE remarks LIKE '%\n\nplayback-used:no%')",
            "UPDATE birdsounds SET remarks = REPLACE(remarks, '\n\nbird-seen:yes', '') 
                WHERE remarks LIKE '%\n\nbird-seen:yes%'",
            "UPDATE birdsounds SET remarks = REPLACE(remarks, '\n\nbird-seen:no', '') 
                WHERE remarks LIKE '%\n\nbird-seen:no%'",
            "UPDATE birdsounds SET remarks = REPLACE(remarks, '\n\nplayback-used:yes', '') 
                WHERE remarks LIKE '%\n\nplayback-used:yes%'",
            "UPDATE birdsounds SET remarks = REPLACE(remarks, '\n\nplayback-used:no', '') 
                WHERE remarks LIKE '%\n\nplayback-used:no%'",
        ];
        foreach ($queries as $q) {
            $this->mysqli()->query($q);
        }
        echo "\n\nReady\n\n";
    }

    private function mysqli()
    {
        return db()->getConnection();
    }

    private function soundTypeExtras($types)
    {
        return array_unique(array_diff($types, array_flip($this->fixedSoundTypes())));
    }

    private function fixedSoundTypes()
    {
        uksort($this->soundTypeOptions, function ($a, $b) {
            $dif = strlen($b) - strlen($a);
            return $dif ?: strcmp($a, $b);
        });
        return $this->soundTypeOptions;
    }

    private function getProperty($id)
    {
        if (!array_key_exists($id, $this->extracted)) {
            $res = $this->mysqli()->query(
                'SELECT `property`, `category_id` FROM `sound_properties` WHERE `id` = ' . $id
            );
            $row = $res->fetch_object();
            $this->extracted[$id] = ['property' => $row->property, 'category_id' => $row->category_id];
        }
        return $this->extracted[$id];
    }

    private function termOccurs($string, $pattern)
    {
        // case insensitive but term cannot be followed by a question mark
        $escape = preg_quote($pattern, '/');
        if (preg_match("/\b({$escape})\b/ui", trim($string), $res) && stripos($string, $pattern . '?') === false) {
            return true;
        }
        return false;
    }


}


function app()
{
    static $task = null;
    if (!$task) {
        $task = new UpdateSoundTypes();
    }
    return $task;
}

app()->run();
