<?php

require_once 'xc.lib.php';

class JDB
{

    public $mysqli;

    public function __construct()
    {
        global $db_host, $db_user, $db_password;

        $this->mysqli = new mysqli(
            $db_host,
            $db_user,
            $db_password,
            'planquedb'
        );

        if ($this->mysqli->connect_error) {
            $this->abort($this->mysqli->connect_error);
        }

        $this->mysqli->set_charset('utf8');
    }

    public function abort($msg)
    {
        $this->mysqli->rollback();
        die("$msg\n");
    }

    public function prepare($sql)
    {
        $stmt = $this->mysqli->prepare($sql);
        if (!$stmt) {
            $this->abort($this->mysqli->error);
        }
        return $stmt;
    }

    public function query($sql)
    {
        $res = $this->mysqli->query($sql);
        if ($res === false) {
            $this->abort($this->mysqli->error);
        }
        return $res;
    }

}

// $checklistFiles should be an associative array mapping from a checklist id
// (e.g. country code) to a filename that defines that checklist
function populateChecklists($checklistFiles, $dbname)
{
    assert($dbname);
    assert(is_array($checklistFiles));

    echo("Populating checklists\n");
    $db = new JDB();

    foreach ($checklistFiles as $code => $filename) {
        echo("Processing $filename...");
        $fh = fopen($filename, 'r');
        $i  = 0;
        while (($line = fgetcsv($fh))) {
            if (($i++ % 20) == 0) {
                echo('.');
            }
            $stmt = $db->prepare(
                "INSERT INTO $dbname SELECT species_nr, ? FROM taxonomy WHERE genus=? AND species=?"
            );
            $stmt->bind_param('sss', $code, $line[0], $line[1]);
            $stmt->execute();
        }
        echo("done\n");
    }
}
