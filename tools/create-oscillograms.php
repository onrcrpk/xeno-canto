<?php

use xc\AudioUtil;

use function xc\db;
use function xc\XC_logger;

require_once 'xc.lib.php';

class UpdateAudio extends xc\Task
{
    private $startNr;

    // We went live the day before, so no need to progress any further
    private $endDate = '2022-10-01';

    public function __construct()
    {
        parent::__construct('.');
    }

    public function run()
    {
        if (!$this->tempTableExists()) {
            $this->mysqli()->query(
                'CREATE TABLE `_audio_update` (
                    `snd_nr` int(11) unsigned NOT NULL AUTO_INCREMENT,
                    `sono` tinyint(1) NOT NULL,
                    `slow` tinyint(1) NOT NULL,
                    PRIMARY KEY (`snd_nr`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4'
            );
        }

        $query = '
            SELECT t1.snd_nr, t2.format
            FROM birdsounds AS t1
            LEFT JOIN audio_info AS t2 ON t1.snd_nr = t2.snd_nr
            WHERE t1.snd_nr >= ' . $this->getStartNr() . ' AND t1.datetime < "' . $this->endDate . '"
            ORDER BY t1.snd_nr';
        $res = $this->mysqli()->query($query);

        while ($row = $res->fetch_object()) {
            $sono = $slow = 1;
            if (!AudioUtil::generateSonos($row->snd_nr, false, true)) {
                XC_logger()->logError("Could not create oscillograms created for XC$row->snd_nr");
                $sono = 0;
            } else {
                XC_logger()->logInfo("Oscillograms created for XC$row->snd_nr");
            }

            if ($row->format == 'wav' && !AudioUtil::createMp3($row->snd_nr)) {
                XC_logger()->logError("Could not generate slowed-down mp3 for XC$row->snd_nr");
                $slow = 0;
            }
            $this->mysqli()->query("INSERT IGNORE INTO _audio_update VALUES ($row->snd_nr, $sono, $slow)");
        }
    }

    private function tempTableExists()
    {
        $res = $this->mysqli()->query('SHOW TABLES LIKE "_audio_update"');
        return $res && $res->num_rows == 1;
    }

    private function mysqli()
    {
        return db()->getConnection();
    }

    private function getStartNr()
    {
        if (!$this->startNr) {
            $res = $this->mysqli()->query("SELECT MAX(snd_nr) as start_nr FROM _audio_update");
            $this->startNr = $res->fetch_object()->start_nr;
            if (!$this->startNr) {
                $res = $this->mysqli()->query("SELECT MIN(snd_nr) as start_nr FROM birdsounds");
                $this->startNr = $res->fetch_object()->start_nr;
            }
        }
        return $this->startNr;
    }

}


function app()
{
    static $task = null;
    if (!$task) {
        $task = new UpdateAudio();
    }
    return $task;
}

app()->run();
