-- Table to store "deleted" users


CREATE TABLE IF NOT EXISTS `users_pruned` (
  `username` varchar(80) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `dir` varchar(20) NOT NULL DEFAULT '',
  `language` varchar(20) NOT NULL DEFAULT '',
  `num_per_page` int(3) NOT NULL DEFAULT 0,
  `view` tinyint(1) NOT NULL DEFAULT 0,
  `notification` tinyint(1) NOT NULL DEFAULT 1,
  `downsample` tinyint(1) NOT NULL DEFAULT 0,
  `blurb` text NOT NULL,
  `region` varchar(20) NOT NULL,
  `license` varchar(255) NOT NULL DEFAULT 'CC-by-nc-sa-4.0',
  `joindate` datetime DEFAULT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT 0,
  `verificationCode` varchar(45) DEFAULT NULL,
  `contactform` tinyint(1) NOT NULL DEFAULT 1,
  `third_party_license` tinyint(1) NOT NULL DEFAULT -1,
  PRIMARY KEY (`dir`),
  UNIQUE KEY `email` (`email`),
  KEY `dir` (`dir`),
  KEY `username` (`username`),
  KEY `third_party_license` (`third_party_license`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
