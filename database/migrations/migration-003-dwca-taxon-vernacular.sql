-- English names can be null
ALTER TABLE `taxonomy` CHANGE `eng_name` `eng_name` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;
ALTER TABLE `taxonomy` CHANGE `family_english` `family_english` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;
ALTER TABLE `taxonomy_multilingual` CHANGE `english` `english` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;


-- Import tables for CoL DarwinCore archives

-- Create syntax for TABLE '_dwca_taxon'
DROP TABLE IF EXISTS `_dwca_taxon`;
CREATE TABLE `_dwca_taxon` (
   `taxonID` varchar(50) NOT NULL DEFAULT '',
   `parentNameUsageID` varchar(50) DEFAULT NULL,
   `acceptedNameUsageID` varchar(50) DEFAULT NULL,
   `originalNameUsageID` varchar(50) DEFAULT NULL,
   `scientificNameID` varchar(50) DEFAULT NULL,
   `datasetID` int(11) DEFAULT NULL,
   `taxonomicStatus` varchar(30) DEFAULT NULL,
   `taxonRank` varchar(20) DEFAULT NULL,
   `scientificName` varchar(200) DEFAULT NULL,
   `scientificNameAuthorship` varchar(150) DEFAULT NULL,
   `genericName` varchar(50) DEFAULT NULL,
   `infragenericEpithet` varchar(50) DEFAULT NULL,
   `specificEpithet` varchar(50) DEFAULT NULL,
   `infraspecificEpithet` varchar(50) DEFAULT NULL,
   `cultivarEpithet` varchar(50) DEFAULT NULL,
   `nameAccordingTo` varchar(100) DEFAULT NULL,
   `namePublishedIn` varchar(1000) DEFAULT NULL,
   `nomenclaturalCode` varchar(50) DEFAULT NULL,
   `nomenclaturalStatus` varchar(20) DEFAULT NULL,
   `taxonRemarks` text DEFAULT NULL,
   `references` varchar(1000) DEFAULT NULL,
   PRIMARY KEY (`taxonID`),
   KEY `parentNameUsageID` (`parentNameUsageID`),
   KEY `genericName` (`genericName`,`specificEpithet`,`infraspecificEpithet`),
   KEY `scientificNameID` (`scientificNameID`),
   KEY `taxonomicStatus` (`taxonomicStatus`),
   KEY `taxonRank` (`taxonRank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Create syntax for TABLE '_dwca_vernacular'
DROP TABLE IF EXISTS `_dwca_vernacular`;
CREATE TABLE `_dwca_vernacular` (
    `taxonID` varchar(50) NOT NULL DEFAULT '',
    `language` varchar(20) DEFAULT NULL,
    `vernacularName` varchar(100) DEFAULT NULL,
    KEY `taxonID` (`taxonID`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- Add indices to multilingual
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`chinese`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`chinese_tw`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`czech`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`danish`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`dutch`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`estonian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`finnish`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`french`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`german`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`hungarian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`italian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`japanese`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`lithuanian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`norwegian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`polish`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`portuguese`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`russian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`slovak`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`spanish`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`swedish`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`afrikaans`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`catalan`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`icelandic`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`indonesian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`latvian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`northern_sami`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`slovenian`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`thai`);
ALTER TABLE `taxonomy_multilingual` ADD INDEX (`ukranian`);
