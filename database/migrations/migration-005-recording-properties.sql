-- New fields in birdsounds
ALTER TABLE `birdsounds` ADD `temperature` DOUBLE DEFAULT NULL;
ALTER TABLE `birdsounds` ADD `collection_specimen` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `birdsounds` ADD `collection_date` DATE DEFAULT NULL;
ALTER TABLE `birdsounds` ADD `observed` TINYINT(1) DEFAULT -1;
ALTER TABLE `birdsounds` ADD `playback` TINYINT(1) DEFAULT -1;
ALTER TABLE `birdsounds` ADD `automatic` TINYINT(1) DEFAULT -1;
ALTER TABLE `birdsounds` ADD `device` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `birdsounds` ADD `microphone` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `birdsounds` ADD `modified` DATETIME DEFAULT NULL;

ALTER TABLE `birdsounds` ADD INDEX (`datetime`);
ALTER TABLE `birdsounds` ADD INDEX (`observed`);
ALTER TABLE `birdsounds` ADD INDEX (`playback`);
ALTER TABLE `birdsounds` ADD INDEX (`automatic`);
ALTER TABLE `birdsounds` ADD INDEX (`temperature`);

UPDATE `birdsounds` SET `modified` = `datetime`;
ALTER TABLE `birdsounds` CHANGE `modified` `modified` DATETIME  NOT NULL  ON UPDATE CURRENT_TIMESTAMP;


-- Update the trash as well :)
ALTER TABLE `birdsounds_trash` ADD `temperature` DOUBLE DEFAULT NULL;
ALTER TABLE `birdsounds_trash` ADD `collection_specimen` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `birdsounds_trash` ADD `collection_date` DATE DEFAULT NULL;
ALTER TABLE `birdsounds_trash` ADD `observed` TINYINT(1) DEFAULT -1;
ALTER TABLE `birdsounds_trash` ADD `playback` TINYINT(1) DEFAULT -1;
ALTER TABLE `birdsounds_trash` ADD `automatic` TINYINT(1) DEFAULT -1;
ALTER TABLE `birdsounds_trash` ADD `device` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `birdsounds_trash` ADD `microphone` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `birdsounds_trash` ADD `modified` DATETIME DEFAULT NULL;

ALTER TABLE `birdsounds` ADD INDEX (`datetime`);
ALTER TABLE `birdsounds` ADD INDEX (`observed`);
ALTER TABLE `birdsounds` ADD INDEX (`playback`);
ALTER TABLE `birdsounds` ADD INDEX (`automatic`);
ALTER TABLE `birdsounds` ADD INDEX (`temperature`);


-- New tables
DROP TABLE IF EXISTS `birdsounds_background`;

CREATE TABLE `birdsounds_background` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `snd_nr` int(11) unsigned NOT NULL,
  `species_nr` varchar(10) NOT NULL,
  `scientific` varchar(100) DEFAULT NULL,
  `english` varchar(50) DEFAULT NULL,
  `family` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `snd_nr` (`snd_nr`,`species_nr`),
  KEY `species_nr` (`species_nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `birdsounds_properties`;

CREATE TABLE `birdsounds_properties` (
    `snd_nr` int(11) unsigned NOT NULL,
    `property_id` int(11) unsigned NOT NULL,
    `category_id` int(11) unsigned NOT NULL,
    `property` varchar(50) DEFAULT NULL,
    `created` timestamp NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`snd_nr`,`property_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



DROP TABLE IF EXISTS `group_sound_properties`;

CREATE TABLE `group_sound_properties` (
    `group_id` int(11) unsigned NOT NULL,
    `property_id` int(11) unsigned NOT NULL,
    `sort_order` tinyint(3) NOT NULL DEFAULT 99,
    UNIQUE KEY `group_id` (`group_id`,`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `group_sound_properties` (`group_id`, `property_id`, `sort_order`)
VALUES
    (1,1,1),
    (1,2,2),
    (1,3,1),
    (1,4,2),
    (1,5,3),
    (1,7,1),
    (1,8,2),
    (1,9,3),
    (1,10,4),
    (1,11,5),
    (1,12,6),
    (1,13,7),
    (1,14,8),
    (1,15,9),
    (1,16,10),
    (1,17,99),
    (1,18,98),
    (1,24,1),
    (1,25,2),
    (1,26,3),
    (1,28,0),
    (1,29,0),
    (1,30,0),
    (1,31,6),
    (2,1,1),
    (2,2,2),
    (2,3,1),
    (2,6,2),
    (2,17,99),
    (2,19,1),
    (2,20,2),
    (2,21,3),
    (2,22,4),
    (2,23,5),
    (2,24,1),
    (2,27,2),
    (2,28,0),
    (2,29,0),
    (2,30,0),
    (2,31,6),
    (2,32,6),
    (2,33,4);


DROP TABLE IF EXISTS `sound_properties`;

CREATE TABLE `sound_properties` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `property` varchar(50) NOT NULL DEFAULT '',
    `category_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `sound_properties` (`id`, `property`, `category_id`)
VALUES
    (1,'male',1),
    (2,'female',1),
    (3,'adult',2),
    (4,'juvenile',2),
    (5,'nestling',2),
    (6,'nymph',2),
    (7,'song',3),
    (8,'subsong',3),
    (9,'call',3),
    (10,'alarm call',3),
    (11,'flight call',3),
    (12,'nocturnal flight call',3),
    (13,'begging call',3),
    (14,'drumming',3),
    (15,'duet',3),
    (16,'dawn song',3),
    (17,'aberrant',3),
    (18,'imitation',3),
    (19,'calling song',3),
    (20,'courtship song',3),
    (21,'rivalry song',3),
    (22,'disturbance song',3),
    (23,'flight song',3),
    (24,'field recording',4),
    (25,'in the hand',4),
    (26,'in net',4),
    (27,'studio recording',4),
    (28,'uncertain',1),
    (29,'uncertain',2),
    (30,'uncertain',3),
    (31,'unknown',4),
    (32,'searching song',3),
    (33,'female song',3);


DROP TABLE IF EXISTS `sound_property_categories`;

CREATE TABLE `sound_property_categories` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `category` varchar(50) NOT NULL DEFAULT '',
    `tag` varchar(15) DEFAULT NULL,
    `multiple` tinyint(1) DEFAULT 1,
    `sort_order` tinyint(3) NOT NULL DEFAULT 99,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `sound_property_categories` (`id`, `category`, `tag`, `multiple`, `sort_order`)
VALUES
    (1,'sex','sex',1,1),
    (2,'life stage','stage',1,2),
    (3,'sound type','type',1,3),
    (4,'recording method','method',0,5);


-- Field recording as default; requires manual corrections...
insert ignore into birdsounds_properties
(snd_nr, property_id, category_id, property)
(select snd_nr, 24, 4, 'field recording' from birdsounds
where snd_nr not in
(select snd_nr from birdsounds_properties where category_id = 4));

-- Not automatic as default; requires manual corrections...
UPDATE birdsounds SET automatic = 0 WHERE automatic = -1;