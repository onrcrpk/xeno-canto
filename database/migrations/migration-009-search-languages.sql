-- Store search preferences for common names in account
CREATE TABLE `user_search_languages` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `userid` varchar(10) NOT NULL,
    `language` varchar(10) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;